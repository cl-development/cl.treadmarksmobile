//
//  TOCMaterialComponentView.swift
//  TreadMarks
//
//  Created by Dennis on 2015-06-15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation

class TOCMaterialComponentView : TOCComponentView {
    
    @IBOutlet weak var materialTypeLabel:UILabel!
    @IBOutlet weak var materialselectedDescription :UILabel!

    var materialTypes:[Transaction_MaterialType]? = []
    private let COMPONENT_WIDTH =   CGFloat(768)    //NSNumber(float:680)
    private let TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON =   CGFloat(175)  //NSNumber(float:150)
    private let TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF =   CGFloat(173)  //NSNumber(float:80)
    private let OFF_SCREEN = false
    private let ON_SCREEN = true
    private var selectedType:Transaction_MaterialType? = nil
    
    
    override init(frame:CGRect) {
        super.init(frame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF))
        self.connectNib()
        self.status = OFF_SCREEN
        self.heightOn  = TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON
        self.heightOff = TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func connectNib() {
        let name:String = self.nameOfClass
        let test:[AnyObject] = NSBundle.mainBundle().loadNibNamed(name, owner: self, options: nil)
        let theView: UIView = test[0] as! UIView
        self.addSubview(theView)
    }

    override func updateUI() ->() {
        self.initData()
        
        if let selType = self.selectedType {
            self.status=ON_SCREEN
            let matType:MaterialType = selType.materialType
            self.materialTypeLabel.text = matType.shortNameKey
            self.materialTypeLabel.font = UIFont.boldSystemFontOfSize(28.0)
            self.materialselectedDescription.text = matType.nameKey
            let label2Font = UIFont(name: "Helvetica-Light", size: 23.0)
            self.materialselectedDescription!.font = label2Font

        }
        else {
            self.status=OFF_SCREEN
        }
        if (self.status==ON_SCREEN) {
            self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,self.frame.size.width,TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON)
            self.heightOn = self.frame.size.height
        }
        if (self.readOnly==true) {
             self.validate()
        }
        else {
            self.validate()
            self.parentPageViewController!.updateUI()
        }
        
    }
    
    
    
    override func validate()->() {
        self.valid = self.selectedType != nil
        self.updateValidateImage()
        
    }
    
    
    
    func materialTypeComponentUpdate()->() {
        self.updateUI()
        self.parentPageViewController!.validate()
    }
    
    // MARK: - data initialization
    /*func initData () {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        if let test : [Transaction_MaterialType] = engineInstance.transactionMaterialTypeForTransactionId(self.transaction?.transactionId) as? [Transaction_MaterialType] {
            for trans_mat:NSManagedObject in test {
                let trans_m:Transaction_MaterialType = trans_mat as! Transaction_MaterialType
                self.materialTypes?.append(trans_m)
                self.selectedType = trans_m
            }
        }
    }
    */
    func initData () {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        if let test : Transaction_MaterialType = engineInstance.transactionMaterialTypeForTransactionId(self.transaction?.transactionId)  {                     self.selectedType = test;
        }
    }

}


