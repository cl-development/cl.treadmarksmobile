//
//  AboutView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+CustomView.h"
@protocol AboutViewControllerDelegate <NSObject>

@required


- (void)aboutCancelAction:(UIButton *)sender;
@end



@interface AboutView : UIView
{
    NSArray *helpTopicArray;
}
@property (weak, nonatomic) IBOutlet UITableView *helpTabelView;
@property (weak, nonatomic) IBOutlet UIView *aboutView;
- (IBAction)helpCancelButton:(id)sender;
@property id<AboutViewControllerDelegate> delegate;


@end
