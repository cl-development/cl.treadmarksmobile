//
//  InfoView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 30/09/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"

@protocol InfoViewControllerDelegate <NSObject>

@required


- (void)cancelAction:(UIButton *)sender;
@end


@interface InfoView : UIView
@property id<InfoViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *businessLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bizAddress2Label;
@property (weak, nonatomic) IBOutlet UILabel *bizAddress3Label;
@property (weak, nonatomic) IBOutlet UILabel *bizPhone;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)infoCancelButton:(id)sender;

@end
