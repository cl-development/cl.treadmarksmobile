//
//  LogoutView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "LogoutView.h"
#import "ModalViewController.h"

@implementation LogoutView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.logoutView createCornerRadius:self.logoutView];
        
    }
    
    return self;
}

#pragma mark - Button Actions
- (IBAction)logoutOkButton:(id)sender
{
    [self.delegate okAction:sender];
    
    
}
- (IBAction)logoutCancelButton:(id)sender
{
    [self.delegate logoutcancelAction:sender];
}

@end
