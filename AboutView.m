//
//  AboutView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "AboutView.h"
#import "ModalViewController.h"
#import "HelpAboutTableViewCell.h"
#import "HelpViewController.h"
#import "Utils.h"
#import "HelpTableViewCell.h"
#import "HelpView.h"

@interface AboutView ()<HelpViewControllerDelegate>

@end
@implementation AboutView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.aboutView createCornerRadius:self.aboutView];
        if([Utils getProcessorDidLogin]==YES)
        {
            helpTopicArray= [[NSArray alloc] initWithObjects:
                             @"About",
                             @"iPad Overview",
                             @"How to complete a PTR transaction", nil];
            
        }
        else
        {
            helpTopicArray= [[NSArray alloc] initWithObjects:
                             @"About",
                             @"iPad Overview",
                             @"How to complete a TCR transaction",
                             @"How to complete a STC transaction",
                             nil];
            
        }
        
    }
    
    return self;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.aboutView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.aboutView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.aboutView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }}

#pragma mark - UITableViewDataSource
- (IBAction)helpCancelButton:(id)sender
{
    [self.delegate aboutCancelAction:sender];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return helpTopicArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /// First row is handled differently
    if (indexPath.row == 0)
    {
        NSString* identifier = NSStringFromClass([HelpAboutTableViewCell class]);
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        
        return cell;
    }
    
    NSString* identifier = @"HelpTableViewCell";
    HelpTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == NULL)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:NULL] forCellReuseIdentifier:identifier];
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    
    cell.helpHeaderLabel.text=@"Manual";
    cell.helpTopicLabel.text=[helpTopicArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0)
    {
        return 200;
    }
    else
    {
        return tableView.rowHeight;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0)
    {
        /// Do nothing for About
    }
    else
    {
        
        HelpViewController *helpViewController=[[HelpViewController alloc]init];
        helpViewController.helpPdfFileName= [helpTopicArray objectAtIndex:indexPath.row];
        //  [self addSubview:helpViewController.view];
        UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
        helpViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [navigationController presentViewController:helpViewController animated:YES completion:nil];
        
    }
}

- (void)helpCancelAction:(UIButton *)sender
{
    [[ModalViewController sharedModalViewController]hideView];
    
}
@end
