

//
//  LocationComponent.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2/19/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCLocationPopOverView.h"

#import "DataEngine.h"
#import "Location.h"
#import "Transaction.h"
#import "TOCPageViewController.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "TOCLocationComponentView.h"
#import "Utils.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Transaction+Transaction.h"
#import "Authorization+Authorization.h"
#import "UIView+CustomView.h"
@interface TOCLocationPopOverView ()
{
    BOOL locationGrabed;
    NSInteger integerTransactionID;
    BOOL locationDisabled;
    BOOL locationAllowed;
}
- (IBAction)settingsButton:(id)sender;
- (IBAction)cancelPopUp:(id)sender;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) Location *locationData;
@property (weak, nonatomic) IBOutlet UIView *disableLocationView;
@property (nonatomic, assign) id currentResponder;
@property (nonatomic,strong)TOCLocationComponentView *tocLocationCV;
@property (weak, nonatomic) IBOutlet UIView *numKeyPadView;

@property (weak, nonatomic) IBOutlet UIView *locationFieldsView;
@property (weak, nonatomic) IBOutlet UILabel *fetchingLocationLabel;

@end

@implementation TOCLocationPopOverView


#define PHONE_TEXT_MAX_LENGTH 12
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    return self;
}

- (void)closeScreen:(NSNotification*)notification {
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }

    
    
   // [self dismissViewControllerAnimated:YES completion:nil];

    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation=[locations lastObject];
    self.latitudetextLabel.alpha=1;
    self.longitudetextLabel.alpha=1;
    self.fetchingLocationLabel.alpha=0;
	
    
    self.latitudetextLabel.text=    [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitudetextLabel.text=[NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    BOOL locationON = [CLLocationManager locationServicesEnabled];
	self.fetchingLocationLabel.text=@"Location Data Not Availaible";
    if([error code] == kCLErrorDenied || !locationON){
		self.locationFieldsView.hidden=YES;
        self.disableLocationView.hidden=NO;
        locationDisabled=YES;
        
    }
    else
        self.locationFieldsView.hidden=NO;
    [Utils log:@"Location error:",[error localizedDescription]];
    [self.locationManager stopUpdatingLocation];
}

-(BOOL)checkForNewTransaction
{
    BOOL isNewTransaction ;
	//
    self.locationData = (Location*)[Location singleById:self.transactionId];
	
	//NSLog(@"LOCATION  id %@",self.locationData.locationId);
    if (self.locationData.locationId!=0)
        
    {
        isNewTransaction = NO;
        //NSLog(@"NOT NEW");
		//Load data from TOC LOcationComponent
		//NSLog(@"Data loading  with LocationID %@",self.locationData.locationId);
        
        if(self.locationData.latitude==NULL)
		{self.latitudetextLabel.text=@"0.000000";
            self.longitudetextLabel.text=@"-0.000000";}
        else{
            self.latitudetextLabel.text=self.locationData.latitude;
            self.longitudetextLabel.text=self.locationData.longitude;}
        
        //remove the if
        if(!locationAllowed)
		{
			self.latitudetextLabel.alpha=0;
			self.longitudetextLabel.alpha=0;
			self.fetchingLocationLabel.alpha=1;
			self.fetchingLocationLabel.text=@"Location Data Not Availaible";
			
            
		}
        else{
            self.locationFieldsView.hidden=NO;
            self.disableLocationView.hidden=YES;
            self.fetchingLocationLabel.text=@"Fetching Location...";
            
        }
        self.groupnameTextfield.text= self.locationData.name;
        //[self.groupnameTextfield becomeFirstResponder];
        self.phonetextTextfield.text= self.locationData.phone;
	}
    else{
        
        isNewTransaction = YES;
        //NSLog(@"NEW");
    }
    
    return isNewTransaction;
}

-(void)saveAllLocationData :(BOOL)SaveAllFields {
	
    self.locationData = (Location*)[Location singleById:self.transactionId];
	
    //NSLog(@"LOCATION  id for new entity %@",self.transactionId);
	
	Transaction* transaction = (Transaction*)[Transaction singleById:self.transactionId];

    if (self.locationData.locationId==NULL)
    {
		//NSLog(@"NEW DATA FOR LOCATION");
        Location *LocationData = (Location*)[[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
		
		/// Need to create a associated STC authorization and Authorization record, if it does not exist
		/// for sync.
		Authorization* authorization = (Authorization*)[Authorization single:@"transaction == %@", transaction];
		if (authorization == nil)
		{
			authorization = [Authorization insertNewObjectForTransaction:transaction];
		}
		STCAuthorization* stcAuthorization = (STCAuthorization*)[STCAuthorization single:@"authorization == %@", authorization];
		if (stcAuthorization == nil)
		{
			stcAuthorization = (STCAuthorization*)[STCAuthorization insertNewObject:[[DataEngine sharedInstance] managedObjectContext]];
			stcAuthorization.authorization = authorization;
		}
		stcAuthorization.location = LocationData;
		
		LocationData.locationId = self.transactionId;
		if([self.latitudetextLabel.text isEqualToString:@"0.000000"])
        { LocationData.latitude =NULL;
			LocationData.longitude=NULL;}
		else{
			LocationData.latitude = self.latitudetextLabel.text;
			LocationData.longitude = self.longitudetextLabel.text;}
        if (SaveAllFields)
        {
            LocationData.name=self.groupnameTextfield.text;
            LocationData.phone=self.phonetextTextfield.text;
        }
        LocationData.syncDate = nil;
    }
    else
    {
        if(locationAllowed)
        {//NSLog(@"LOCATION ALLOWED ");
            self.locationData.latitude = self.latitudetextLabel.text;
            self.locationData.longitude = self.longitudetextLabel.text;
		}
		//NSLog(@"UPDATE DATA FOR NAME & PHONE");
        if (SaveAllFields)
        {
			
			//  self.locationData = [[DataEngine sharedInstance] locationForId:locationIdFromNSDefaults];
			self.locationData.name=self.groupnameTextfield.text;
			self.locationData.phone=self.phonetextTextfield.text;
        }
    }
    self.locationData.syncDate = nil;
	[transaction clearSyncStatus];
    [[DataEngine sharedInstance] saveContext];
}



- (IBAction)doneButton:(id)sender {
    //Save and move to locationcomponentview
    self.groupnameTextfield.text = [self.groupnameTextfield.text  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [self saveAllLocationData:YES];
    
	[self.tocLocationComponentView finishedGrabingLocationNotification];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"Location Grabed" object:self];
	
    //if([self.locationpopoverDelegate respondsToSelector:@selector(finishedGrabingLocation)])
    [self dismissViewControllerAnimated:YES completion:nil];
	
    
	
	//[self dismissViewControllerAnimated:YES completion:nil];
	
}
- (IBAction)backButton:(id)sender {
    
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.locationFieldsView createCornerRadius:self.locationFieldsView];
	self.groupnameTextfield.delegate=self;
    self.phonetextTextfield.delegate=self;
	self.doneButton.enabled=NO;
    [self.doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [[self.phonetextTextfield valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1] forKey:@"insertionPointColor"];
    [[self.groupnameTextfield valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1] forKey:@"insertionPointColor"];
    locationAllowed = [CLLocationManager locationServicesEnabled];
    if (!locationAllowed) {
        self.locationFieldsView.hidden=YES;
        self.disableLocationView.hidden=NO;
        self.fetchingLocationLabel.text=@"Location Data Not Available";
        
    }
    else{
        self.locationFieldsView.hidden=NO;
        self.disableLocationView.hidden=YES;
        self.fetchingLocationLabel.text=@"Fetching Location...";
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
       
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		[self.locationManager requestWhenInUseAuthorization];
    }
    //NSLog(@"Location ID recv from component %@",self.transactionId);
	if([self checkForNewTransaction])
	{
		self.latitudetextLabel.alpha=0;
		self.longitudetextLabel.alpha=0;
		//self.fetchingLocationLabel.text=@"Fetching Location...";
		self.fetchingLocationLabel.alpha=1;
		[self.locationManager startUpdatingLocation];
	}
	else//check for disablelocation
	{
		if (locationAllowed && [self.latitudetextLabel.text isEqualToString:@"0.000000"]  )
			
		{[self.locationManager startUpdatingLocation];
			self.fetchingLocationLabel.alpha=0;
		}
		if (locationAllowed && ![self.latitudetextLabel.text isEqualToString:@"0.000000"]  )
		{[self.locationManager stopUpdatingLocation];
			self.fetchingLocationLabel.alpha=0;}
		//else
		// {[self.locationManager stopUpdatingLocation];}
		
		
    }
    if(![self.latitudetextLabel.text isEqualToString:@"0.000000"])
    {self.fetchingLocationLabel.alpha=0;
        self.latitudetextLabel.alpha=1;
        self.longitudetextLabel.alpha=1;
		
    }
    
    //Resign keyboarb if user tap on screen
	UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnTap:)];
	[singleTap setNumberOfTapsRequired:1];
	[singleTap setNumberOfTouchesRequired:1];
	[self.view addGestureRecognizer:singleTap];
	//[singleTap release];
    
    if([self.groupnameTextfield.text length]>=2 && [self.phonetextTextfield.text length]==12)
	{self.doneButton.enabled=YES;}
    else
	{self.doneButton.enabled=NO;}
	
    if([self.latitudetextLabel.text isEqualToString:@"0.000000"] && !locationAllowed)
    {self.disableLocationView.hidden=NO;
        self.locationFieldsView.hidden=YES;}
    else
    {self.disableLocationView.hidden=YES;
		self.locationFieldsView.hidden=NO;
		[self.groupnameTextfield becomeFirstResponder];
    }
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resignOnTap:(id)iSender {
  //  [self.currentResponder resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
	return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
	self.currentResponder = nil;
    //TBD check for empty field
    //self.doneButton.enabled=YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.currentResponder = textField;
    
    UITextInputAssistantItem* item = [textField inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
    
    if(textField==self.phonetextTextfield){
        if (textField.text.length<PHONE_TEXT_MAX_LENGTH) {
            self.doneButton.enabled=NO;
        }
        else {
            self.doneButton.enabled=YES;
        }
    }
}
-(NSString *)formatGroupName:(NSString *)simpleName deleteLastChar:(BOOL)deleteLastChar {
	
    if (simpleName.length <=2){
        self.doneButton.enabled=NO;
    }
    
    if (simpleName.length >= 2 && deleteLastChar == NO){
        if([self.phonetextTextfield.text length]==12){
            //[self.groupnameTextfield resignFirstResponder]; do no resign the eyboard as user might enter more.
            self.doneButton.enabled=YES;
        }
    }

    return simpleName;
}
-(NSString *) formatPhoneNumber:(NSString *) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    
    if ([simpleNumber length] < 13) {
        self.doneButton.enabled=NO;
    }
	
    if ([simpleNumber length] == 12 && deleteLastChar == NO){
        if ([self.groupnameTextfield.text length]>1) {
			[self.phonetextTextfield resignFirstResponder];
			self.doneButton.enabled=YES;
        }
		//  else
		//    [self.phonetextTextfield resignFirstResponder];
    }
    
    if([simpleNumber length]==0) {
        return @"";
    }
    
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\$\\%\\*\\&\\#\\!\\:\\>\\<\\.\\,\\/\\[\\]\\{\\}\\?\\^\\~\\|\\'\\`\\;\\+\\=\\_\"\\\\@\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if([simpleNumber length] > 10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if([simpleNumber length] < 7) {
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"$1-$2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    }
    
    else {  // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    }
    
    if ([simpleNumber length] == 12) {
        self.doneButton.enabled=YES;
    }
    return simpleNumber;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	
    //NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    UITextPosition *start = [textField positionFromPosition:[textField beginningOfDocument]
                                                     offset:range.location];
    UITextPosition *end = [textField positionFromPosition:start offset:0];
    
    long pos = [textField offsetFromPosition:[textField endOfDocument] toPosition:[[textField selectedTextRange] end]];
    
    if(textField==self.phonetextTextfield) {
        
        NSMutableString *phoneString = [textField.text mutableCopy];
		NSUInteger stringLength = string.length;
        for (NSInteger loopIndex = 0; loopIndex < stringLength; loopIndex++) {
            unichar character = [string characterAtIndex:loopIndex];
            if (character < 48 || character > 57) {
                return NO;
            }
//            if (character > 57) {
//                return NO;
//            }
        }
        
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        if (range.length == 1) {
            [phoneString deleteCharactersInRange:NSMakeRange(range.location, range.length)];
            textField.text = [self formatPhoneNumber:phoneString deleteLastChar:YES];
            [textField setSelectedTextRange:[textField textRangeFromPosition:start toPosition:end]];
        }
        else {
            [phoneString insertString:string atIndex:range.location];
            textField.text = [self formatPhoneNumber:phoneString deleteLastChar:NO];
            UITextPosition *newPos = [textField positionFromPosition:[textField endOfDocument] offset:pos];
            [textField setSelectedTextRange:[textField textRangeFromPosition:newPos toPosition:newPos]];
        }
        return NO;
		// }
    }
    if(textField==self.groupnameTextfield) {
        
        NSMutableString *nameString = [textField.text mutableCopy];
        if (range.location == 0 && [string isEqualToString:@" "]) {
            return NO;
        }
        if (range.length == 1) {//when deleting a character
            [nameString deleteCharactersInRange:NSMakeRange(range.location, range.length)];
            textField.text = [self formatGroupName:nameString deleteLastChar:YES];
            [textField setSelectedTextRange:[textField textRangeFromPosition:start toPosition:end]];
            
        } else {//when entering a character
            [nameString insertString:string atIndex:range.location];
            textField.text = [self formatGroupName:nameString deleteLastChar:NO];
            UITextPosition *newPos = [textField positionFromPosition:[textField endOfDocument] offset:pos];
            [textField setSelectedTextRange:[textField textRangeFromPosition:newPos toPosition:newPos]];
            
        }
        return NO;
    }
    
	return YES;
}

- (IBAction)settingsButton:(id)sender {
    self.disableLocationView.hidden=YES;
    self.locationFieldsView.hidden=NO;
    [self.groupnameTextfield becomeFirstResponder];
}

- (IBAction)cancelPopUp:(id)sender {
    
}

@end

