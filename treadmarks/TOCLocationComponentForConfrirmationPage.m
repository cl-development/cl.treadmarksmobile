//
//  TOCLocationComponentView.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2/13/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCLocationComponentForConfirmationPage.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"
#import "OpenSansRegularTextField.h"
#import "OpenSansLightTextField.h"
#import "OpenSansRegularButton.h"
#import "DataEngine.h"
#import "TOCLocationPopOverView.h"
#import "Transaction.h"
#import "TOCPageViewController.h"
#import "Location.h"
#import "NSManagedObject+NSManagedObject.h"


#define TOC_LOCATION_COMPONENT_HEIGHT_ON 355
#define TOC_LOCATION_COMPONENT_HEIGHT_OFF 80


@interface TOCLocationComponentForConfirmationPage ()
//@property (weak, nonatomic) IBOutlet UIImageView *validateImageView;

@property (nonatomic,strong) Location *locationData;
@property (nonatomic, assign) BOOL isLocationExist ;

@property (nonatomic,strong) IBOutlet UILabel *groupnameLabel;
@property (nonatomic,strong) IBOutlet UILabel *phonetextLabel;
@property (nonatomic,strong) Transaction *tocTransaction;
@property (nonatomic,strong) IBOutlet UILabel *latitudeLabel;
@property (nonatomic,strong) IBOutlet UILabel *longitudeLabel;

@property (weak, nonatomic) IBOutlet UILabel *locationDataLabel;

@end

@implementation TOCLocationComponentForConfirmationPage


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH,TOC_LOCATION_COMPONENT_HEIGHT_ON)];
    if (self) {
        [self connectNib];
    }
    
    [self updateUI];
    
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
        //Register notification
        self.tocTransaction=self.transaction;
     }


-(void)updateUI{
     self.tocTransaction=self.transaction;
     //NSLog(@"Location ID created at UPDATE UI %@",self.tocTransaction.friendlyId);
    [self doLocationDataExist];
    if([self.latitudeLabel.text isEqualToString:@"0.000000"])
    {   self.locationDataLabel.alpha=1;
        self.latitudeLabel.alpha=0;
        self.longitudeLabel.alpha=0;

    }
    else
    {self.locationDataLabel.alpha=0;
        self.latitudeLabel.alpha=1;
        self.longitudeLabel.alpha=1;}
    [self validate];
}

- (BOOL) doLocationDataExist //Check for location data and if exists pouplate the Label.
{
	self.locationData = (Location*)[Location singleById:self.tocTransaction.transactionId];
    //NSLog(@"Location ID for search in db %@",self.tocTransaction.friendlyId);
    //NSLog(@"Location ID from db %@",self.locationData.locationId);
    
    if (self.locationData.locationId==NULL)
    {
        self.isLocationExist = NO;
   }
    else
    {
        self.isLocationExist = YES;
        //if exist preload the data form Location Table
        if(self.locationData.latitude==NULL)
        {self.latitudeLabel.text=@"0.000000";
            self.longitudeLabel.text=@"-0.000000";}
        else{self.latitudeLabel.text=self.locationData.latitude;
            self.longitudeLabel.text=self.locationData.longitude;}
        self.groupnameLabel.text=self.locationData.name;
        self.phonetextLabel.text=self.locationData.phone;
    }
    return self.isLocationExist;
}

-(void)validate{
    self.valid = YES;
    [self updateValidateImage];
    [self.parentPageViewController validate];
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}



@end
