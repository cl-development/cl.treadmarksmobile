//
//  LocationComponent.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2/19/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TOCComponentView.h"

@class TOCLocationComponentView;

@protocol LocationPopOverDelegate <NSObject>

@optional
- (void) finishedGrabingLocation;
@end


@interface TOCLocationPopOverView : UIViewController  <UITextFieldDelegate,CLLocationManagerDelegate,LocationPopOverDelegate>
@property (nonatomic,strong) IBOutlet UILabel *latitudetextLabel;
@property (nonatomic,strong) IBOutlet UILabel *longitudetextLabel;
@property (nonatomic,strong) IBOutlet UITextField *groupnameTextfield;
@property (nonatomic,strong) IBOutlet UITextField *phonetextTextfield;

@property (weak, nonatomic)  IBOutlet UIButton *doneButton;
@property (weak, nonatomic) id<LocationPopOverDelegate> eligibilityDelegate;
@property (nonatomic,strong) NSString* transactionId;
@property (strong) TOCLocationComponentView *tocLocationComponentView;
@end
