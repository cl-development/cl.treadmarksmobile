//
//  OpenSansLightTextField.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-26.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "OpenSansLightTextField.h"

@implementation OpenSansLightTextField

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [UIFont fontWithName:@"OpenSans-Light" size:self.font.pointSize];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"OpenSans-Light" size:self.font.pointSize];
}


@end
