//
//  OpenSansRegularTextField.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-09-07.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "OpenSansRegularTextField.h"

@implementation OpenSansRegularTextField

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [UIFont fontWithName:@"OpenSans" size:self.font.pointSize];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"OpenSans" size:self.font.pointSize];
}

@end
