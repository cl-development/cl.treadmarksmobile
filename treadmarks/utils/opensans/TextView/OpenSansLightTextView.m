//
//  OpenSansLight.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-26.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "OpenSansLightTextView.h"
#import <CoreText/CoreText.h>

@implementation OpenSansLightTextView

#pragma mark - Override methods

- (void)setAttributedText:(NSAttributedString *)attributedText {
	
	NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:attributedText];
	[string enumerateAttribute:NSFontAttributeName
					   inRange:NSMakeRange(0, [string length])
					   options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
					usingBlock:^(UIFont* value, NSRange range, BOOL* stop)
	 {
		 UIFontDescriptor* descriptor = [value fontDescriptor];
		 UIFontDescriptorSymbolicTraits traits = descriptor.symbolicTraits;
		 
		 if (traits & UIFontDescriptorTraitBold)
		 {
			 value = [UIFont fontWithName:@"OpenSans-Semibold" size:value.pointSize];
		 }
		 else
		 {
			 value = [UIFont fontWithName:@"OpenSans-Light" size:value.pointSize];
		 }
		 
		 [string addAttribute:NSFontAttributeName value:value range:range];
	 }];

	[super setAttributedText:string];
}

@end
