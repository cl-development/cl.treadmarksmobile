//
//  OpenSansTextView.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/31/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "OpenSansTextView.h"
#import "Constants.h"

@interface OpenSansTextView() <UITextViewDelegate>

@end

@implementation OpenSansTextView

- (void)awakeFromNib {

	[super awakeFromNib];

	self.delegate = self;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
	
	CGRect line = [textView caretRectForPosition:
				   textView.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
	- ( textView.contentOffset.y + textView.bounds.size.height
	   - textView.contentInset.bottom - textView.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = textView.contentOffset;
        offset.y += overflow + UITEXTVIEW_OVERFLOW_MARGIN; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:UITEXTVIEW_ANIMATION_DURATION animations:^{
            [textView setContentOffset:offset];
        }];
    }
}

@end
