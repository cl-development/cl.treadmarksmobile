//
//  OpenSansRegularTextView.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-09-07.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenSansTextView.h"

@interface OpenSansRegularTextView : OpenSansTextView

@end
