//
//  OpenSansTextView.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/31/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSansTextView : UITextView
- (void)textViewDidChange:(UITextView *)textView;
@end
