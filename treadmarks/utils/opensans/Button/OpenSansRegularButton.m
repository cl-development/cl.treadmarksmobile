//
//  OpenSansRegularButton.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/22/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "OpenSansRegularButton.h"
#import "Constants.h"

@implementation OpenSansRegularButton

- (id) initWithCoder:(NSCoder *)aDecoder {
	
	self = [super initWithCoder:aDecoder];
	
	if(self)
	{
		self.titleLabel.font = [UIFont fontWithName:DEFAULT_REGULAR_FONT size:self.titleLabel.font.pointSize];
	}
	
	return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
