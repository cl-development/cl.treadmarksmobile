//
//  OpenSansBoldButton.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/22/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "OpenSansSemiboldButton.h"
#import "Constants.h"

@implementation OpenSansSemiboldButton

- (id) initWithCoder:(NSCoder *)aDecoder {
	
	self = [super initWithCoder:aDecoder];
	
    if (self)
	{
		self.titleLabel.font = [UIFont fontWithName:DEFAULT_BOLD_FONT size:self.titleLabel.font.pointSize];
    }
	
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
