//
//  OpenSansRegularButton.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/22/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSansRegularButton : UIButton

@end
