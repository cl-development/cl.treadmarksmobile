//
//  OpenSansLightButton.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-08-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "OpenSansLightButton.h"
#import "Constants.h"

@implementation OpenSansLightButton

- (id)initWithCoder:(NSCoder *)aDecoder {
	
	self = [super initWithCoder:aDecoder];
	
    if (self)
	{
		self.titleLabel.font = [UIFont fontWithName:DEFAULT_LIGHT_FONT size:self.titleLabel.font.pointSize];
    }
	
    return self;
}

@end
