//
//  OpenSansLightButton.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-08-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSansLightButton : UIButton

@end
