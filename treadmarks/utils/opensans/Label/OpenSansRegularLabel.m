//
//  OpenSansRegularLabel.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-26.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "OpenSansRegularLabel.h"

@implementation OpenSansRegularLabel

#pragma mark - Override methods

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [UIFont fontWithName:@"OpenSans" size:self.font.pointSize];
    }
    return self;
}
- (void) awakeFromNib {
	
    [super awakeFromNib];
	
	[self applyAttributes];
}

#pragma mark - Accessor methods

/// Need to override because setting the text would make it lose it's attributes
- (void) setText:(NSString *) text {
	
	[super setText:text];
	
	[self applyAttributes];
}

#pragma mark -

- (void) applyAttributes {
	/// Updated the functions so that it works with attributed strings as well
	//	self.font = [UIFont fontWithName:@"OpenSans" size:self.font.pointSize];
	
	NSMutableAttributedString* string = [self.attributedText mutableCopy];
	
	[string enumerateAttribute:NSFontAttributeName
					   inRange:NSMakeRange(0, [string length])
					   options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
					usingBlock:^(UIFont* value, NSRange range, BOOL* stop) {
						
						value = [UIFont fontWithName:@"OpenSans" size:value.pointSize];
						[string addAttribute:NSFontAttributeName value:value range:range];
					}];
	
	[self setAttributedText:string];
}

@end
