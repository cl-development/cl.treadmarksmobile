//
//  OpenSansLight.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-26.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSansLightLabel : UILabel

@end
