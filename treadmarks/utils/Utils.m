//
//  Utils.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "Utils.h"
#import "MessageViewController.h"
#import "Constants.h"
#import "User.h"
#import "Transaction.h"
#import "DataEngine.h"

@implementation Utils

static BOOL SHOULD_LOG=YES;
static BOOL dockOpen;

+ (void)log:(NSString *)formatString, ... {
    if(SHOULD_LOG){
        va_list args;
        va_start(args, formatString);
//        NSString * logMessage = [[NSString alloc] initWithFormat:formatString arguments:args];
        va_end(args);
//        //NSLog(@"%@",logMessage);
    }
}

+(BOOL)isDocOpen{
    return dockOpen;
}

+(void)setDocOpen:(BOOL)isDocOpen{
    dockOpen=isDocOpen;
}

+(NSString*)ordinalSuffix:(NSInteger)number{
    
    switch (number) {
        case 1:
        case 21:
        case 31:
            return @"st";
            break;
        case 2:
        case 22:
            return @"nd";
            break;
        case 3:
        case 23:
            return @"rd";
        default:
            return @"th";
            break;
    }
}

+(UIColor*)colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue{
    UIColor *color = [UIColor colorWithRed:(red/255) green:(green/255) blue:(blue/255) alpha:1];
    return color;
}

//display a message
+(void)showMessage:(UIViewController*)viewController header:(NSString*)header message:(NSString*)message {
	
    /// If viewcontroller has a containing navigationcontoller, use it as the
	/// presenter view controller.
	if(viewController.navigationController != NULL)
	{
		viewController = viewController.navigationController;
	}
	
	//viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    MessageViewController *messageViewController    = [MessageViewController new];
    messageViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    messageViewController.header  = header;
    messageViewController.message = message;
    
    messageViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;

    [viewController presentViewController:messageViewController animated:YES completion:nil];
}
+(void)showMessage:(UIViewController*)viewController
			header:(NSString*)header
		   message:(NSString *)message
		singleline:(NSString*)singleline
	   singlegreen:(NSString*)singlegreen {
	
	[Utils showMessage:viewController
				header:header
			   message:message
		  okButtonText:@"OK"
			singleline:singleline
		   singlegreen:singlegreen
                tobold:@""
			 hideDelay:0
		hideCompletion:NULL];
}
//display a message for a period of time
+(void)showMessage:(UIViewController*)viewController
			header:(NSString*)header
		   message:(NSString*)message
		 hideDelay:(NSTimeInterval)hideDelay {
	
	[Utils showMessage:viewController header:header message:message hideDelay:hideDelay hideCompletion:NULL];
}
+(void)showMessage:(UIViewController*)viewController
			header:(NSString*)header
		   message:(NSString*)message
		 hideDelay:(NSTimeInterval)hideDelay
	hideCompletion:(void (^)(void))hideCompletion {
	
	[Utils showMessage:viewController
				header:header
			   message:message
		  okButtonText:@""
			singleline:@""
		   singlegreen:@""
                tobold:@""
			 hideDelay:hideDelay
		hideCompletion:hideCompletion];
}
+(void)showMessage:(UIViewController*)viewController
			header:(NSString*)header
		   message:(NSString*)message
	  okButtonText:(NSString*)okButtonText
		singleline:(NSString*)singleline
	   singlegreen:(NSString*)singlegreen
            tobold:(NSString*)tobold
		 hideDelay:(NSTimeInterval)hideDelay
	hideCompletion:(void (^)(void))hideCompletion {
	
	for (; viewController.parentViewController != NULL; )
	{
		viewController = viewController.parentViewController;
	}
	
	if(viewController.navigationController)
	{
		viewController = viewController.navigationController;
	}
	
    viewController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    MessageViewController *messageViewController    = [MessageViewController new];
    //messageViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    messageViewController.hideDelay = hideDelay;
    messageViewController.header    = header;
    messageViewController.message   = message;
    messageViewController.okbuttontext = okButtonText;
	messageViewController.singlelinetext = singleline;
	messageViewController.singlegreenlinetext = singlegreen;
    messageViewController.boldtext = tobold;
	messageViewController.hideCompletion = hideCompletion;
    
    messageViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [viewController presentViewController:messageViewController animated:YES completion:NULL];
}

//check if two date are in the same day
+(BOOL)isDate:(NSDate*)date1 sameDayWith:(NSDate*)date2{
    
    NSDate *beginOfDate1 = nil;
    NSDate *beginOfDate2 = nil;
    
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&beginOfDate1 interval:NULL forDate:date1];
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&beginOfDate2 interval:NULL forDate:date2];
    
    BOOL result = ([beginOfDate1 compare:beginOfDate2] == NSOrderedSame);
    
    //[Utils log:@"Comparing %@ with %@ and the result is %d",date1,date2,result];
    return result;
}

//check if two dates are the same month
+(BOOL)isDate:(NSDate *)date1 sameMonthWith:(NSDate *)date2 {
    //if any dates is null, return NO
    if (!date1 || !date2) {
        return NO;
    }
    NSCalendar* calendar = [NSCalendar currentCalendar];
    unsigned int components = NSCalendarUnitMonth | NSCalendarUnitYear;
    NSDateComponents *dateComponents    = [calendar components:components fromDate:date1];
    NSInteger month1   = [dateComponents month];
    NSInteger year1    = [dateComponents year];
    
    dateComponents    = [calendar components:components fromDate:date2];
    NSInteger month2   = [dateComponents month];
    NSInteger year2    = [dateComponents year];
    
    return (month1==month2) && (year1==year2);
}


/* user id */
+(NSNumber*) getUserId{
    NSInteger integer = [[NSUserDefaults standardUserDefaults] integerForKey:USER_ENTITY_NAME];
    return [NSNumber numberWithInteger:integer];
}

+(void) setUserId:(NSNumber*)userId{
    [Utils log:@"setUserId:%@",[userId stringValue]];
    [[NSUserDefaults standardUserDefaults] setInteger:[userId integerValue] forKey:USER_ENTITY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)deleteUserId{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_ENTITY_NAME];
}

/* registrant id */
+(NSNumber*) getRegistrantId{
    NSInteger integer = [[NSUserDefaults standardUserDefaults] integerForKey:REGISTRANT_ENTITY_NAME];
    return [NSNumber numberWithInteger:integer];
}


+(NSNumber*) getRegistrantMetaId{
    NSInteger integer = [[NSUserDefaults standardUserDefaults] integerForKey:REGISTRANT_ENTITY_NAME];
    return [NSNumber numberWithInteger:integer];
}

+(void) setRegistrantId:(NSNumber*)registrantId{
    [Utils log:@"setRegistrantId:%@",[registrantId stringValue]];
    [[NSUserDefaults standardUserDefaults] setInteger:[registrantId integerValue] forKey:REGISTRANT_ENTITY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)deleteRegistrantId{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:REGISTRANT_ENTITY_NAME];
}

/* transaction id */
+(NSString*) getTransactionId{
    NSString* result = [[NSUserDefaults standardUserDefaults] stringForKey:TRANSACTION_ENTITY_NAME];
    return result;
}

+(void) setTransactionId:(NSString*)transactionId{
    [Utils log:@"setTransactionId:%@",transactionId];
    [[NSUserDefaults standardUserDefaults] setObject:transactionId forKey:TRANSACTION_ENTITY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)deleteTransactionId{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:TRANSACTION_ENTITY_NAME];
}

+(User*)getIncomingUser{
    return [[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
}

+(Registrant *)getIncomingRegistrant{
    return [[DataEngine sharedInstance] registrantForId:[Utils getRegistrantId]];
}




+(Transaction*)getTransaction{
    return [[DataEngine sharedInstance] transactionForId:[Utils getTransactionId]];
}
+(Transaction*) sortTransaction:(NSNumber*)friID{
    return [[DataEngine sharedInstance] transactionForFriendlyId:friID];
}
/* login gps id*/
+(NSNumber*) getGPSLogId{
    NSNumber* result = [[NSUserDefaults standardUserDefaults] objectForKey:GPS_LOG_ENTITY_NAME];
    return result;
}

+(void) setGPSId:(NSString*)gpsLogId{
    [Utils log:@"setsetGPSIdId:%@",gpsLogId];
    [[NSUserDefaults standardUserDefaults] setObject:gpsLogId forKey:GPS_LOG_ENTITY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



//generate an unique transaction id
+(NSString*)createTransactionId{
    return [[NSUUID UUID] UUIDString];
}

//generate an unique fiendly transaction id
+(NSNumber*)createFriendlyTransactionId:(TransactionType*)transactionType{
    /*
     9 digits total:
     1: Form Type
     2-8: last 7 digits of unix timestamp
     9: checksum unsing Luhn algorithm
     */
    
    int stringLength = 9;
    NSMutableString *result = [@"" mutableCopy];
    //first digit is the transaction type
    [result appendString:[transactionType.transactionTypeId stringValue]];
    
   // result = [[transactionType.transactionTypeId stringValue]mutableCopy];
    
    
    //2-8: last 7 digits of unix timestamp
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
    int timeIntervalInt = timeInterval;
    int last7Digits = timeIntervalInt % 10000000;
    [result appendString:[NSString stringWithFormat:@"%d",last7Digits]];
    
    //9: checksum unsing Luhn algorithm
    NSString *checksum = [Utils luhnChecksumDigit:result];

    [result appendString:checksum];

    if ([result length] < stringLength) {
        NSInteger diff = stringLength - [result length];
        for (int i = 1; i <= diff; i++) {
            [result insertString:@"0" atIndex:i];
        }
    }
    [Utils log:result];
    return [NSNumber numberWithInteger:[result integerValue]];
}


+ (NSString*) luhnChecksumDigit:(NSString *) string {
    //based on https://github.com/MaxKramer/ObjectiveLuhn
    //more details http://en.wikipedia.org/wiki/Luhn_algorithm
    
    NSMutableString *reversedString = [NSMutableString stringWithCapacity:[string length]];
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:(NSStringEnumerationReverse |NSStringEnumerationByComposedCharacterSequences) usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [reversedString appendString:substring];
    }];
    
    unsigned int odd_sum = 0, even_sum = 0;
    
    for (int i = 0; i < [reversedString length]; i++) {
        NSInteger digit = [[NSString stringWithFormat:@"%C", [reversedString characterAtIndex:i]] integerValue];
        
        if (i % 2 == 1) {
            odd_sum += digit;
        } else {
            even_sum += digit / 5 + ( 2 * digit) % 10;
        }
    }
    NSInteger sum = odd_sum + even_sum;
    
    NSInteger checksum = (sum * 9)%10;
    NSString *result =  [NSString stringWithFormat:@"%ld",(long)checksum];
    [Utils log:@"luhnChecksumDigit: %@ -> %@",string,result];
    return result;
}

#pragma mark - Saving Photos support

+ (NSString *)nameForPhoto {
	
	/*
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    //df.timeStyle = NSDateFormatterShortStyle;
    df.timeStyle = NSDateFormatterMediumStyle;
	NSString *nameFromDate = [df stringFromDate:[NSDate date]];
	 */
    
    return [NSString stringWithFormat:@"%f.png", [[NSDate date] timeIntervalSince1970]];
}

+(NSMutableString *)pathForPhotoWithPhotoName:(NSString *)name {
    NSMutableString *myString = [NSMutableString stringWithString:[self documentsDirectory]];
    myString = [[myString stringByAppendingPathComponent:name] mutableCopy];
    return myString;
}

+(NSString *)documentsDirectory {
    return[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}
+ (UIImage *)findSmallImage:(NSString *)fileName {

	if([fileName rangeOfString:@"_small.png"].location == NSNotFound)
	{
		fileName = [fileName stringByReplacingOccurrencesOfString:@".png"
													   withString:@"_small.png"];
	}
	
	fileName = [fileName lastPathComponent];
	UIImage* image = [UIImage imageWithContentsOfFile:fileName];
	
	if(image == NULL)
	{
		fileName = [Utils pathForPhotoWithPhotoName:fileName];
		
		image = [UIImage imageWithContentsOfFile:fileName];
	}
	
	return image;
}
+ (UIImage *)findLargeImage:(NSString *)fileName {
    
		
	UIImage* image = [UIImage imageWithContentsOfFile:fileName];
	
	if(image == NULL)
	{
		fileName = [Utils pathForPhotoWithPhotoName:fileName];
		
		image = [UIImage imageWithContentsOfFile:fileName];
	}
	
	return image;
}

+ (void) deleteScaleTicketImageFile:(ScaleTicket *)theTicket {
    NSError* error;
    
    NSLog(@"Removing (small): %@", theTicket.photo.fileName);
    NSString *path = [Utils pathForPhotoWithPhotoName:theTicket.photo.fileName];
    [[NSFileManager defaultManager] removeItemAtPath:[path stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"] error:&error];
}

+ (void) validateScaleTickets {
    NSArray *currentScaleTickets = [[DataEngine sharedInstance] entities:SCALE_TICKET_ENTITY_NAME];
    for (ScaleTicket *ticket in currentScaleTickets) {
        if ([self isValidScaleTicket:ticket]==NO) {
            [self deleteScaleTicketImageFile:ticket];
            [[DataEngine sharedInstance] deleteScaleTicket:ticket];
        }
    }
}

+ (BOOL) isValidScaleTicket:(ScaleTicket *)ticket {
    return (ticket.ticketNumber != nil);
}

#pragma mark - Date formatting

+(NSString *) dateToString:(NSDate *)theDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    return[formatter stringFromDate:theDate];
}

+ (NSError *)errorForType:(NSString *)command errorCode:(NSUInteger)code
{
    NSString *description = [NSString stringWithFormat:@"%@ failed.", command];
    
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey : description};
    return [NSError errorWithDomain:command code:code userInfo:userInfo];
}

#pragma mark - processor login

+ (void) setProcessorDidLogin:(BOOL)didLogin {
    [Utils log:@"setProcessorDidLogin:%@",didLogin];
    [[NSUserDefaults standardUserDefaults] setBool:didLogin forKey:PROCESSOR_DID_LOGIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL) getProcessorDidLogin {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:PROCESSOR_DID_LOGIN];
    return result;
}
#pragma mark - feb login
+ (void) setProcessorPtrOnly:(BOOL)febLogin {
    [Utils log:@"setProcessorPtrOnly:%@",febLogin];
    [[NSUserDefaults standardUserDefaults] setBool:febLogin forKey:PROCESSOR_PTR_ONLY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL) getProcessorPtrOnly {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:PROCESSOR_PTR_ONLY];
    return result;
}

+ (void) setShowLastThreeMonths:(BOOL)prevMonth {
    [Utils log:@"setShowLastThreeMonths:%@",prevMonth];
    [[NSUserDefaults standardUserDefaults] setBool:prevMonth forKey:SHOW_PTR_ALSO];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL) getShowLastThreeMonths {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:SHOW_PTR_ALSO];
    return result;
}

#pragma mark - Weight Variance
+(BOOL) transactionHasWeightVarianceComment:(Transaction *)transaction {
    BOOL result = NO;
    NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:transaction];
    if (comments != nil) {
        for (Comment *comment in comments) {
            if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
                result = YES;
                break;
            }
        }
    }
    return result;
}

+(Comment *) weightVarianceCommentForTransaction:(Transaction *)transaction {
    Comment * result = nil;
    NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:transaction];
    if (comments != nil) {
        for (Comment *comment in comments) {
            if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
                result = comment;
                break;
            }
        }
    }
    return result;
}

#pragma mark - Device ID

+(NSString *) deviceID {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}


#pragma mark - flipdate

+ (void) setFlipServer:(BOOL)flipServer {
    [Utils log:@"setFlipServer:%@",flipServer];
    [[NSUserDefaults standardUserDefaults] setBool:flipServer forKey:FLIP_SERVER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL) getFlipServer {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:FLIP_SERVER];
    return result;
}





@end
