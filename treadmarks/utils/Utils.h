//
//  Utils.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#ifndef DLog
#   define DLog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#endif
#ifndef ELog
#   define ELog(err) {if(err) DLog(@"%@", err)}
#endif
#else
#ifndef DLog
#   define DLog(...)
#endif
#ifndef ELog
#   define ELog(err)
#endif
#endif

// ALog always displays output regardless of the DEBUG setting
#ifndef ALog
#define ALog(fmt, ...) {NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);};
#endif

@class User, Registrant, Transaction, TransactionType, ScaleTicket, Comment;

@interface Utils : NSObject

+(void)log:(NSString *)formatString, ...;

+(BOOL)isDocOpen;

+(void)setDocOpen:(BOOL)isDocOpen;

+(NSString*)ordinalSuffix:(NSInteger)number;

+(UIColor*)colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

+(void)showMessage:(UIViewController*)viewController header:(NSString*)header message:(NSString*)message;

+(void)showMessage:(UIViewController*)viewController header:(NSString*)header message:(NSString*)message hideDelay:(NSTimeInterval)hideDelay;
+(void)showMessage:(UIViewController*)viewController
			header:(NSString*)header
		   message:(NSString*)message
		 hideDelay:(NSTimeInterval)hideDelay
	hideCompletion:(void (^)(void))completion;
+(void)showMessage:(UIViewController*)viewController header:(NSString*)header  message:(NSString *)message singleline:(NSString*)singleline  singlegreen:(NSString*)singlegreen;

+(void)showMessage:(UIViewController*)viewController
            header:(NSString*)header
           message:(NSString*)message
      okButtonText:(NSString*)okButtonText
        singleline:(NSString*)singleline
       singlegreen:(NSString*)singlegreen
            tobold:(NSString*)tobold
         hideDelay:(NSTimeInterval)hideDelay
    hideCompletion:(void (^)(void))hideCompletion;

+(BOOL)isDate:(NSDate*)date1 sameDayWith:(NSDate*)date2;

+(BOOL)isDate:(NSDate*)date1 sameMonthWith:(NSDate*)date2;

/* user id */
+(NSNumber*) getUserId;
+(void) setUserId:(NSNumber*)userId;
+(void)deleteUserId;

/* registrant id */
+(NSNumber*) getRegistrantId;
+(void) setRegistrantId:(NSNumber*)registrantId;
+(void)deleteRegistrantId;

/* transaction id */
+(NSString*) getTransactionId;
+(void) setTransactionId:(NSString*)transactionId;
+(void)deleteTransactionId;
+(Transaction*)getTransaction;
+(Transaction*) sortTransaction:(NSNumber*)friID;

+(User*)getIncomingUser;
+(Registrant *)getIncomingRegistrant;
+(NSString*)createTransactionId;

+(NSNumber*)createFriendlyTransactionId:(TransactionType*)transactionType;

/* login gps id*/
+(NSNumber*)getGPSLogId;

+(void)setGPSId:(NSString*)gpsLogId;

// photo saving support:
+(NSString *)nameForPhoto;
+(NSMutableString *)pathForPhotoWithPhotoName:(NSString *)name;
+(NSString *)documentsDirectory;
+ (UIImage*)findSmallImage:(NSString*)fileName;
+ (UIImage*)findLargeImage:(NSString*)fileName;
+ (void) deleteScaleTicketImageFile:(ScaleTicket *)theTicket;
+ (void) validateScaleTickets;
+ (BOOL) isValidScaleTicket:(ScaleTicket *)ticket;

// dates:
+(NSString *) dateToString:(NSDate *)theDate;

// NSError creation
+ (NSError *)errorForType:(NSString *)command errorCode:(NSUInteger)code;

// processor logged in
+ (void) setProcessorDidLogin:(BOOL)didLogin;
+ (BOOL) getProcessorDidLogin;
// processor ptr Only
+ (void) setProcessorPtrOnly:(BOOL)febLogin;
+(BOOL) getProcessorPtrOnly;
//ShowPrevMonth
+ (void) setShowLastThreeMonths:(BOOL)prevMonth;
+(BOOL) getShowLastThreeMonths;

// weight variance
+(BOOL) transactionHasWeightVarianceComment:(Transaction *)transaction;
+(Comment *) weightVarianceCommentForTransaction:(Transaction *)transaction;

// device ID:
+(NSString *) deviceID;

// flipServer

+ (void) setFlipServer:(BOOL)flipDate;
+(BOOL) getFlipServer;



@end
