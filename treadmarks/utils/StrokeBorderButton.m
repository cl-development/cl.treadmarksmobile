//
//  StrokeBorderButton.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-09-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "StrokeBorderButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation StrokeBorderButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.layer setBorderWidth:1.0f];
        [self.layer setBorderColor:[[UIColor blackColor] CGColor]];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.layer setBorderWidth:1.0f];
    [self.layer setBorderColor:[[UIColor blackColor] CGColor]];
}

@end
