//
//  StrokeBorderButton.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-09-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrokeBorderButton : UIButton

@end
