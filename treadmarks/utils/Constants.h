//
//  Constants.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-08-10.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#ifndef TreadMarks_Constants_h

#pragma mark - Syncing URL


//NEW TM WEB SERVICE

//#define	TM_WS_URL	@"http://mobile.treadmarks.ca/TransactionCommunication.svc/rest/"

//UAT Env:
//#define	TM_WS_URL	@"https://mobile-staging.treadmarks.ca/TransactionCommunication.svc/rest/"

//QA Environment:
//#define	TM_WS_URL	@"https://mobile-staging1.treadmarks.ca/TransactionCommunication.svc/rest/"

//PRO QA
//#define    TM_WS_URL    @"https://mobile-qa-pro.treadmarks.ca/TransactionCommunication.svc/rest/"

//PRO UAT
#define    TM_WS_URL    @"https://mobile-uat-pro.treadmarks.ca/TransactionCommunication.svc/rest/"


#pragma mark -

#define OFF_SCREEN 0
#define ON_SCREEN  1

#define TreadMarks_Constants_h

#define REGISTRANT_ID_JSON_KEY    @"r"

#define USER_ID_JSON_KEY          @"w"

#define TIRE_COUNT_DO_NOT_SHOW_KEY @"tireCountDoNotShow"

#define TABLE_TITLE_FOR_DELETE_CONFIRMATION @"Void"

//entities
#define REGISTRANT_ENTITY_NAME                       @"Registrant"
#define REGISTRANT_TYPE_ENTITY_NAME                  @"RegistrantType"
#define LOCATION_ENTITY_NAME                         @"Location"
#define USER_ENTITY_NAME                             @"User"
#define TRANSACTION_TYPE_ENTITY_NAME                 @"TransactionType"
#define TRANSACTION_STATUS_TYPE_ENTITY_NAME          @"TransactionStatusType"
#define TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME     @"TransactionSyncStatusType"
#define TRANSACTION_TIRE_TYPE_ENTITY_NAME            @"Transaction_TireType"
#define TRANSACTION_ENTITY_NAME                      @"Transaction"
#define TIRE_TYPE_ENTITY_NAME                        @"TireType"
#define SCALE_TICKET_ENTITY_NAME                     @"ScaleTicket"
#define PHOTO_PRESELECT_COMMENT_ENTITY_NAME          @"PhotoPreselectComment"
#define PHOTO_ENTITY_NAME                            @"Photo"
#define PHOTO_TYPE_ENTITY_NAME                       @"PhotoType"
#define MODULE_ENTITY_NAME                           @"Module"
#define DOCUMENT_ENTITY_NAME                         @"Document"
#define STCAUTHORIZATION_ENTITY_NAME                 @"STCAuthorization"
#define DOCUMENT_TYPE_ENTITY_NAME                    @"DocumentType"
#define TRANSACTION_TYPE_MODULE_ENTITY_NAME          @"TransactionType_Module"
#define TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME @"TransactionType_RegistrantType"
#define TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME       @"TransactionType_TireType"
#define TRANSACTION_ELIGIBILITY_ENTITY_NAME          @"Transaction_Eligibility"
#define AUTHORIZATION_ENTITY_NAME                    @"Authorization"
#define SCALE_TICKET_TYPE_ENTITY_NAME                @"ScaleTicketType"
#define MESSAGE_ENTITY_NAME                          @"Message"
#define STATUS_TYPE_ENTITY_NAME                      @"StatusType"
#define ELIGIBILITY_ENTITY_NAME                      @"Eligibility"
#define UNIT_TYPE_ENTITY_NAME                        @"UnitType"
#define COMMENT_ENTITY_NAME                          @"Comment"
#define GPS_LOG_ENTITY_NAME                          @"GPSLog"
#define MATERIAL_TYPE_ENTITY_NAME                    @"MaterialType"
#define TRANSACTION_TYPE_MATERIAL_TYPE_ENTITY_NAME   @"TransactionType_MaterialType"
#define TRANSACTION_MATERIAL_TYPE_ENTITY_NAME        @"Transaction_MaterialType"

//qr code constants
#define QR_CODE_REGISTRANT_ID_KEY    @"r"
#define QR_CODE_USER_ID_KEY          @"w"

//#define SHORT_DATE_FORMAT @"yyyy-MM-dd"
#define SHORT_DATE_FORMAT @"EEE, MMM. d"

#define APP_STATIC_DATA_EXTRACT_DATE @"01-03-2018"

// ScaleTicket constants
#define SCALE_TICKET_INBOUND_NAME @"Inbound"
#define SCALE_TICKET_OUTBOUND_NAME @"Outbound"
#define SCALE_TICKET_BOTH_NAME @"Inbound/Outbound"
#define SCALE_TICKET_INBOUND_RECORD_NUMBER 1
#define SCALE_TICKET_OUTBOUND_RECORD_NUMBER 2

#define SCALE_TICKET_TYPE_UNKNOWN	-1
#define SCALE_TICKET_TYPE_INBOUND	1
#define SCALE_TICKET_TYPE_OUTBOUND	2
#define SCALE_TICKET_TYPE_BOTH		3

#define SCALE_TICKET_WEIGHT_MAX_INTEGER_LEN 9
#define SCALE_TICKET_WEIGHT_MAX_DECIMAL_LEN 4
#define SCALE_TICKET_NUMBER_MAX_LEN 11

#define COMMENT_TYPE_DEFAULT 0
#define COMMENT_TYPE_WEIGHT_VARIANCE_REASON 1
#define WEIGHT_VARIANCE_COMMENT_HEADER @"Weight Variance reason:"

#define OFF_SCREEN 0
#define ON_SCREEN  1
#define NEW_TRANSACTION  2

//validation images

#define PAGE_VALIDATION_OK_IMAGE              @"cal-flag-green.png"
#define PAGE_VALIDATION_NOT_OK_IMAGE          @"cal-flag-yellow.png"
#define COMPONENT_VALIDATION_OK_IMAGE         @"trans-checkmark-on.png"
#define COMPONENT_VALIDATION_NOT_OK_IMAGE     @"trans-checkmark-off.png"

//components
#define COMPONENT_WIDTH 768//680
#define BR_IMAGE_HEIGHT 2

// Unit Types
#define UNIT_TYPE_ID_POUND [NSNumber numberWithInteger:1]
#define UNIT_TYPE_ID_KILOGRAM [NSNumber numberWithInteger:2]
#define UNIT_TYPE_ID_TONNE [NSNumber numberWithInteger:3]

// transaction types:
#define TRANSACTION_TYPE_ID_TCR [NSNumber numberWithInteger:1]
#define TRANSACTION_TYPE_ID_DOT [NSNumber numberWithInteger:2]
#define TRANSACTION_TYPE_ID_HIT [NSNumber numberWithInteger:3]
#define TRANSACTION_TYPE_ID_RTR [NSNumber numberWithInteger:4]
#define TRANSACTION_TYPE_ID_STC [NSNumber numberWithInteger:5]
#define TRANSACTION_TYPE_ID_UCR [NSNumber numberWithInteger:6]
#define TRANSACTION_TYPE_ID_PTR [NSNumber numberWithInteger:7]
#define TRANSACTION_TYPE_ID_PIT [NSNumber numberWithInteger:8]

// eligibility types:
#define ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE          [NSNumber numberWithInteger:1]
#define ELIGIBILITY_TYPE_ID_TIRES_GENERATED         [NSNumber numberWithInteger:2]
#define ELIGIBILITY_TYPE_ID_TIRES_CHARGED           [NSNumber numberWithInteger:3]
#define ELIGIBILITY_TYPE_ID_TIRES_GENERATED_PRIOR   [NSNumber numberWithInteger:4]
#define ELIGIBILITY_TYPE_ID_TIRES_NON_REGISTERED    [NSNumber numberWithInteger:5]

// registrant type
/// Changing the IDs based on the IDs coming from TM database.
#define REGISTRANT_TYPE_ID_COLLECTOR                [NSNumber numberWithInteger:1]
#define REGISTRANT_TYPE_ID_HAULER                   [NSNumber numberWithInteger:2]
#define REGISTRANT_TYPE_ID_STEWARD                  [NSNumber numberWithInteger:3]
#define REGISTRANT_TYPE_ID_RPM                      [NSNumber numberWithInteger:4]
#define REGISTRANT_TYPE_ID_PROCESSOR                [NSNumber numberWithInteger:5]
#define REGISTRANT_TYPE_ID_UNREGISTER_COLLECTOR     [NSNumber numberWithInteger:6]

#define LIST_TABLE_CELL_HEIGHT 57

// tire types:
#define TIRE_TYPE_ID_PLT    [NSNumber   numberWithInteger:1]
#define TIRE_TYPE_ID_MT     [NSNumber   numberWithInteger:2]
#define TIRE_TYPE_ID_AGLS   [NSNumber   numberWithInteger:3]
#define TIRE_TYPE_ID_IND    [NSNumber   numberWithInteger:4]
#define TIRE_TYPE_ID_SOTR   [NSNumber   numberWithInteger:5]
#define TIRE_TYPE_ID_MOTR   [NSNumber   numberWithInteger:6]
#define TIRE_TYPE_ID_LOTR   [NSNumber   numberWithInteger:7]
#define TIRE_TYPE_ID_GOTR   [NSNumber   numberWithInteger:8]


//transaction status type

#define TRANSACTION_STATUS_TYPE_ID_ERROR        [NSNumber numberWithInteger:1]
#define TRANSACTION_STATUS_TYPE_ID_INCOMPLETE       [NSNumber numberWithInteger:2]
#define TRANSACTION_STATUS_TYPE_ID_COMPLETE         [NSNumber numberWithInteger:3]
#define TRANSACTION_STATUS_TYPE_ID_DELETED          [NSNumber numberWithInteger:4]
//New status type
//#define TRANSACTION_STATUS_TYPE_ID_INACTIVE_REG     [NSNumber numberWithInteger:5]

// UCR transaction fixed registration number
#define UCR_REGISTRATION_NUMBER [NSNumber numberWithInteger:9999999]

//transaction sync status type

#define TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED           [NSNumber numberWithInteger:1]
#define TRANSACTION_SYNC_STATUS_TYPE_ID_SYNCHRONIZED               [NSNumber numberWithInteger:2]
#define TRANSACTION_SYNC_STATUS_TYPE_ID_SYNCHRONIZED_FAILED        [NSNumber numberWithInteger:3]
#define TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SENT_TO_TM      [NSNumber numberWithInteger:4]
#define TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SYNC_TM         [NSNumber numberWithInteger:5]
#define TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SYNC_FAIL_TM    [NSNumber numberWithInteger:6]


// core data fixed data installed
#define DATABASE_FIXED_DATA_INSTALLED_NAME @"Fixed data installed"
// core data fixed data transaction tire type patch applied
#define DATABASE_FIXED_DATA_PATCH_APPLIED_NAME @"Transaction Tire Type path applied"
#define DATABASE_FIXED_DATA_CURRENT_VERSION @"1.0"
#define DATABASE_FIXED_DATA_INSTALLED_VERSION DATABASE_FIXED_DATA_CURRENT_VERSION
#define DATABASE_FIXED_DATA_INSTALLED_VERSION_NONE @"None"
#define DATABASE_FIXED_DATA_INSTALLED_VERSION_BASE @"Jeff"

//core data qr code patch installed
#define DATABASE_PATCH_INSTALLED_NAME @"QR code data installed"
// core data registrationNumber for Transaction patch applied
#define DATABASE_JTH_REGISTRATION_NUMBER [NSNumber numberWithInteger:3000008]
#define DATABASE_JTH_REGISTRATION_NUMBER_PATCH_APPLIED_NAME @"JTH registrationNumber for Transaction patch applied"

//animation variables
#define COMPONENTS_ANIMATION_DURATION 1
#define DEFAULT_ANIMATION_DURATION 0.25

#pragma mark - UI constants

#define MODAL_BACKGROUND_ALPHA 0.45
#define DEFAULT_MARGIN 40

#pragma mark - Font constants

#define DEFAULT_REGULAR_FONT @"OpenSans"
#define DEFAULT_BOLD_FONT @"OpenSans-Semibold"
#define DEFAULT_LIGHT_FONT @"OpenSans-Light"

#pragma mark - UITextView settings

#define UITEXTVIEW_OVERFLOW_MARGIN 7
#define UITEXTVIEW_ANIMATION_DURATION .2

#define PHOTOTYPE_ID_PHOTO [NSNumber numberWithInteger:1]
#define PHOTOTYPE_ID_SIGNATURE [NSNumber numberWithInteger:2]
#define PHOTOTYPE_ID_SCALE_TICKET [NSNumber numberWithInteger:3]
#define PHOTOTYPE_ID_DOCUMENT [NSNumber numberWithInteger:4]

#define QR_CODE_DECRYPTION_KEY_HEX @"973ac15a09fa425db002077b4d83193876a3e382eb975906e0befbe9f1582d9f"

#pragma mark -

#define OUTGOING_USER 1
#define INCOMING_USER 2

//Custom colors
#define BUTTON_TEXT_COLOR @"UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1"

// MAImagePicker
#define MA_IMAGE_PICKER_CROP_RECT_MIN_WIDTH 400
#define MA_IMAGE_PICKER_CROP_RECT_MIN_HEIGHT 400

/*	PhotoType IDs */
#define PHOTO_TYPE_ID_PHOTO			[NSNumber numberWithInt:1]
#define PHOTO_TYPE_ID_SIGNATURE		[NSNumber numberWithInt:2]
#define PHOTO_TYPE_ID_SCALE_TICKET	[NSNumber numberWithInt:3]
#define PHOTO_TYPE_ID_DOCUMENT		[NSNumber numberWithInt:4]

/* Core Data settings */
#define	SQLITE_STORE_URL			@"TreadMarks"
#define	MANAGED_OBJECT_MODEL_PATH	@"TreadMarks"
#define	FIXED_STORE_PATH			@"TreadMarks-02-03-2018"

#pragma mark - Test Transactions

#define	TEST_TCR_TRANSACTIONS	4
#define	TEST_STC_TRANSACTIONS	2
#define	TEST_PTR_TRANSACTIONS	4
#define TEST_NUMBER_OF_PHOTOS	2
#define RANDOM_MAX_TIRE_QUANTITY	2000
#define IS_RANDOM	NO

#pragma mark - Tire Count component

#define	MAX_TIRE_COUNT_DIGITS	5

#pragma mark - Logging

#define	NSLOG_TO_FILE	NO

#define PROCESSOR_DID_LOGIN @"ProcessorDidLogin"

#define PROCESSOR_PTR_ONLY @"ProcessorPtrOnly"
#define SHOW_PTR_ALSO @"ShowPtrAlso"

#define FLIP_SERVER @"FlipServer"

#endif
