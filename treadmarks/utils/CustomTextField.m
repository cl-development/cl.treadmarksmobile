//
//  CustomTextField.m
//  TreadMarks
//
//  Created by Capris Group on 2013-09-09.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "CustomTextField.h"
#import "OpenSansLabel.h"
#import "OpenSansSemiBoldTextField.h"
#import "OpenSansLightLabel.h"

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawPlaceholderInRect:(CGRect)rect {
    // Set colour and font size of placeholder text
    //[[UIColor whiteColor] setFill];
    //NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    //[style setAlignment:NSTextAlignmentCenter];
    //NSMutableDictionary *strAttribs = [[NSMutableDictionary alloc] init];
    //[strAttribs setObject:style forKey:NSParagraphStyleAttributeName];
    
    
    UIFont *font = [UIFont fontWithName: @"OpenSans-Semibold" size: 26];
    UIColor *color = [UIColor lightGrayColor];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *dict = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName: color };
    
    [[self placeholder] drawInRect:rect withAttributes:dict];
    
    //[[self placeholder] drawInRect:rect withFont:[UIFont fontWithName:@"OpenSans-Semibold" size:26] lineBreakMode:NSLineBreakByTruncatingMiddle alignment:NSTextAlignmentCenter];
    
    
}

//- (CGRect)placeholderRectForBounds:(CGRect)bounds {
//    return CGRectMake(322,32,10,10);//Return your desired x,y position and width,height
//}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
