//
//  SendFTP.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-06-06.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendFTP : NSObject



-(void)sendLogFile;
-(void)startSend:(NSString *)filePath;
-(NSString *)logFilePath;
@end
