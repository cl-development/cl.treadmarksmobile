//
//  TOCMaterialPopoverViewController.swift
//  TreadMarks
//
//  Created by Dennis on 2015-06-15.
//  Copyright (c) 2015 Capris. All rights reserved.
//
// custom table cell issues: cellForRow crashed on dequeueReusableCellWithIdentifier:forIndexPath
// if just call registerNib.
// RegisterClass appears to work but the IBOutlets are nil, implying the nib file wasn't used.
// so here in the custom tablecellclass I create the label etc in code

import Foundation

class TOCMaterialPopoverViewController : TOCPopoverViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    /*
    private var materialTypes: [TM_UAT.MaterialType]?=[]
    private var selectedType:MaterialType? = nil
    private var my_table_cell_id:String =  "TOCMaterialTypeTableViewCell"   //TOCMaterialTypeTableViewCell.nameOfClass
    private let popover_orig_height:CGFloat = 212.0
    */
    
    @IBOutlet weak var materialTypesTableView : UITableView!
    @IBOutlet weak var containerViewBottomMargin: NSLayoutConstraint!
    var transaction : Transaction? = nil
    fileprivate var materialTypes: [MaterialType]?=[]
    fileprivate var selectedType:MaterialType? = nil
    fileprivate var my_table_cell_id:String =  "TOCMaterialTypeTableViewCell"   //TOCMaterialTypeTableViewCell.nameOfClass
    fileprivate let popover_orig_height:CGFloat = 212.0
    @IBOutlet weak var subView : UIView!
    @IBOutlet weak var subViewTitle : UIView!
    var checkedIndexPath : IndexPath?
    var lastSelectedIndexPath = IndexPath(row: -1, section: 0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subView.layer.cornerRadius = 20
        subView.layer.masksToBounds = true
        subView.layer.shadowColor = UIColor.black.cgColor
        subView.layer.shadowOpacity = 0.8
        subView.layer.shadowRadius = 3.0
        subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.materialTypesTableView.register(TOCMaterialTypeTableViewCell.self, forCellReuseIdentifier: my_table_cell_id)
        self.initData()
        self.configureUI()
    }
    
    // update the selected Transaction_MaterialType:
    @IBAction override func doneButtonPressed(_ sender: AnyObject?) {
        super.doneButtonPressed(sender)
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        if let selectedPath = self.materialTypesTableView.indexPathForSelectedRow {
            let mat_type : MaterialType = self.materialTypes![selectedPath.row]
            if let transMatType : Transaction_MaterialType = engineInstance.transactionMaterialType(forTransactionId: self.transaction?.transactionId)  {
                transMatType.materialType = mat_type
            }
            else {
                let transMatType : Transaction_MaterialType = engineInstance.newEntity(TRANSACTION_MATERIAL_TYPE_ENTITY_NAME) as! Transaction_MaterialType
                transMatType.transaction = self.transaction!
                transMatType.materialType = mat_type
                transMatType.transactionMaterialTypeId = UUID().uuidString
            }
            
            engineInstance.saveContext()
            
            if (self.delegate != nil)   {
                if let view : TOCMaterialTypeComponentView = self.delegate as? TOCMaterialTypeComponentView{
                    let aSelector  = Selector(("materialTypeComponentUpdate"))
                    if view.responds(to: aSelector) {
                        view.materialTypeComponentUpdate()
                    }
                }
            }
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesBegan(touches, with:event)
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesMoved(touches, with:event)
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle{
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesEnded(touches, with:event)
        
        
    }

    // MARK: - table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let test = self.materialTypes {
            return test.count
        }
        return Int(0)
    }
    
    func heightForLabel(_ text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var detailHeight: CGFloat = 0
        let font = UIFont(name: "Helvetica-Light", size: 16.0)
//        let cellstr : MaterialType! = self.materialTypes?[indexPath.row]
        if let material = self.materialTypes?[indexPath.row] {
            detailHeight = heightForLabel(material.nameKey, font: font!, width: 550)
        }
        return detailHeight+50;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            /* should never be nil in theory if register nib or register class succeeds */
            let cell = tableView.dequeueReusableCell(withIdentifier: my_table_cell_id, for: indexPath) as? TOCMaterialTypeTableViewCell
            
            let cellstr : MaterialType = self.materialTypes![indexPath.row]
            cell!.labelText! = cellstr.shortNameKey
            cell!.descriptionText! = cellstr.nameKey
            
            let label2Font = UIFont(name: "Helvetica-Light", size: 16.0)
            cell!.descriptionLabel.font = label2Font
            let detailHeight = heightForLabel(cellstr.nameKey, font: label2Font!, width: 550)
            cell!.descriptionLabel.frame = CGRect(x: 19, y: 35, width: 550, height: detailHeight);
            cell!.descriptionLabel.numberOfLines = 0
            cell!.descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell!.customImage = UIImageView(frame: CGRect(x: 0, y: detailHeight+50, width: 600, height: 1))
            let additionalSeparatorThickness = CGFloat(2)
            let additionalSeparator = UIView(frame: CGRect(x: 0,
                y: cell!.frame.size.height - additionalSeparatorThickness,
                width: cell!.frame.size.width,
                height: additionalSeparatorThickness))
            additionalSeparator.backgroundColor = UIColor(red: 206/255, green: 206/255, blue: 206/255, alpha: 1.0)
            cell!.addSubview(additionalSeparator)
            
            cell!.selectionStyle = UITableViewCellSelectionStyle.none
            
            if (self.checkedIndexPath != nil){}
            else
            {
                if let selectedType = self.selectedType {
                    let selectedRow: Int = (self.materialTypes!).index(of: selectedType)!
                    if (selectedRow == indexPath.row)
                    {
                        let checkImage = UIImage(named: "trans-checkmark-on.png")
                        let checkmark = UIImageView(image: checkImage)

                        cell!.accessoryView = checkmark
                        
                    }
                    
                }
            }

            return cell!
    }
    
    
    
    /*func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->
    
    
    let cell = tableView.dequeueReusableCellWithIdentifier(my_table_cell_id, forIndexPath: indexPath) as? TOCMaterialTypeTableViewCell
    let cellstr : Transaction_MaterialType = self.materialTypes![indexPath.row]
    
    let matType:MaterialType = cellstr.materialType as! MaterialType
    cell!.descriptionLabel.text = matType.nameKey;
    cell!.myCellLabel.text = matType.shortNameKey;
    let label2Font = UIFont.systemFontOfSize(26)
    cell!.descriptionLabel.font = label2Font
    
    let detailHeight = heightForLabel(matType.nameKey, font: label2Font, width: 550)
    cell!.descriptionLabel.frame = CGRectMake(19, 21, 550, detailHeight);
    cell!.descriptionLabel.numberOfLines = 0
    cell!.descriptionLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
    cell!.customImage = UIImageView(frame: CGRectMake(0, detailHeight+25, 600, 1))
    let additionalSeparatorThickness = CGFloat(2)
    let additionalSeparator = UIView(frame: CGRectMake(0,
    cell!.frame.size.height - additionalSeparatorThickness,
    cell!.frame.size.width,
    additionalSeparatorThickness))
    additionalSeparator.backgroundColor = UIColor(red: 206/255, green: 206/255, blue: 206/255, alpha: 1.0)
    cell!.addSubview(additionalSeparator)
    
    cell!.selectionStyle = UITableViewCellSelectionStyle.None
    if (self.checkedIndexPath != nil){}
    else
    {
    if let selectedType = self.selectedType {
    let selectedRow: Int = (self.materialTypes!).indexOf(selectedType)!
    if (selectedRow == indexPath.row)
    {
    cell?.accessoryType = .Checkmark
    }
    
    }
    }
    
    return cell!
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        if (self.checkedIndexPath != nil) {
            let cell = tableView.cellForRow(at: self.checkedIndexPath!)
            //cell?.accessoryType = .None
            let checkImage = UIImage(named: "")
            _ = UIImageView(image: checkImage)
            cell?.accessoryView = .none

        }
        else
        {
            if let selectedType = self.selectedType {
                let selectedRow: Int = (self.materialTypes!).index(of: selectedType)!
                if (selectedRow == indexPath.row)
                {
                    //cell?.accessoryType = .Checkmark
                    let checkImage = UIImage(named: "trans-checkmark-on.png")
                    let checkmark = UIImageView(image: checkImage)
                    cell!.accessoryView = checkmark
                    
                }
                let indexPathOld = IndexPath(row: selectedRow, section: 0)
                let cellOld = tableView.cellForRow(at: indexPathOld)
               // cellOld?.accessoryType = .None
                let checkImage = UIImage(named: "")
                let checkmark = UIImageView(image: checkImage)
                cellOld?.accessoryView = checkmark

            }
        }
       // cell?.accessoryType = .Checkmark
        let checkImage = UIImage(named: "trans-checkmark-on.png")
        let checkmark = UIImageView(image: checkImage)
        cell!.accessoryView = checkmark
        self.checkedIndexPath = indexPath
        self.doneButton.isEnabled =  true
    }
    
    // MARK: - data initialization
    func initData () {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        materialTypes = []
        let results = engineInstance.materialTypes(forTransactionTypeId: self.transaction?.transactionType.transactionTypeId)
        results?.forEach({ (materialType) in
            if let materialType = materialType as? MaterialType {
                materialTypes?.append(materialType)
            }
        })
        
//        if let transactionTypeId = self.transaction?.transactionType.transactionTypeId, let test = engineInstance.materialTypes(forTransactionTypeId: transactionTypeId) as? [NSManagedObject] {
//
//            for trans_mat: NSManagedObject in test {
//                if let trans_mat: MaterialType = trans_mat as? MaterialType
//                {
//                    self.materialTypes?.append(trans_mat)
//                }
//            }
//        }
        if let trans_matType:Transaction_MaterialType = engineInstance.transactionMaterialType(forTransactionId: self.transaction?.transactionId) {
            self.selectedType = trans_matType.materialType
        }
    }
    
    override func configureUI()->() {
        // set the text field from the quanity value
        // set the selected row of the table from the transaction_material type found (can only be one)
        self.doneButton.isEnabled =  false
        if let selectedType = self.selectedType {
//            if let selectedRow = self.materialTypes?.index(of: selectedType) as? Int {
            if let selectedRow = materialTypes?.index(where: { $0.materialTypeId == selectedType.materialTypeId } ) {
                self.materialTypesTableView.selectRow(at: IndexPath(row: selectedRow, section: 0), animated: true, scrollPosition: UITableViewScrollPosition.top)
            }
            self.doneButton.isEnabled =  true
        }
    }
    
}
