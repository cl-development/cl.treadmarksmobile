//
//  TOCLocationComponentView.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2/13/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCComponentView.h"
#import "TOCLocationPopOverView.h"
//@class TOCLocationPopOverView;
@interface TOCLocationComponentView :TOCComponentView
//@property (weak, nonatomic) IBOutlet UIView *view;

-(BOOL) doLocationDataExist;
@property (nonatomic,strong) Transaction *tocTransaction;

-(void)finishedGrabingLocationNotification;
-(void)setViewUI;
@end
