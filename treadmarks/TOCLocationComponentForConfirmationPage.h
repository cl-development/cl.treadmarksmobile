//
//  TOCLocationComponentView.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2/13/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "TOCComponentView.h"
#import "ComponentView.h"

@interface TOCLocationComponentForConfirmationPage : ComponentView    //TOCComponentView
@property (strong, nonatomic) IBOutlet UIView *view;

@end
