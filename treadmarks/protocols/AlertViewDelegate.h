//
//  AlertViewDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/11/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AlertViewController;

@protocol AlertViewDelegate <NSObject>

@required

- (void)alertView:(AlertViewController*)alertView
	primaryAction:(UIButton*)button
			 info:(NSDictionary*)info;
- (void)presentAlert:(UIViewController*)viewController info:(NSDictionary*)info;

@optional

- (void)alertView:(AlertViewController*)alertView
	secondaryAction:(UIButton*)button
			 info:(NSDictionary*)info;

- (void)alertView:(AlertViewController*)alertView
  tertiaryAction:(UIButton*)button
             info:(NSDictionary*)info;

- (void)alertView:(AlertViewController*)alertView
   backAction:(UIButton*)button
             info:(NSDictionary*)info;


@end
