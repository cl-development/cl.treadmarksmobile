//
//  SlideDelegate.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SlideViewDelegate <NSObject>

@optional

- (void)voidTransaction;

@end

/*
 //
//  SlideDelegate.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SlideDelegate <NSObject>

//-(void)newTransaction;
-(void)newTransaction:(int) transType;
-(void)showSearchScreen;

@end
*/
