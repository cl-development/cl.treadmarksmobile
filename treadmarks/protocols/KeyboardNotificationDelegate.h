//
//  KeyboardNotificationDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/22/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KeyboardNotificationDelegate <NSObject>

- (void) keyboardWillShow: (NSNotification*) notification;
- (void) keyboardWillHide: (NSNotification*) notification;

@end
