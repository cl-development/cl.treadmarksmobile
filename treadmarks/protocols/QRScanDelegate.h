//
//  QRScanDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/10/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@protocol QRScanDelegate <NSObject>

- (void) proceed: (User*) user;

@optional
- (bool) validateUser: (User*) user;
- (void) backButtonTouchUpInside;

@end
