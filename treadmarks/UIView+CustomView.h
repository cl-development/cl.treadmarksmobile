//
//  UIView+CustomView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 29/07/15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CustomView)
-(UIView *)createCornerRadius:(UIView *)view;

@end
