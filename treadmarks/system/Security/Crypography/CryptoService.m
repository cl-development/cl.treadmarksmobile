//
//  CryptoService.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/6/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "CryptoService.h"
#import <CommonCrypto/CommonCrypto.h>

#import "Convert.h"

@implementation CryptoService

+(NSData*)decrypt:(NSData*)data withKey:(NSData*)key
{
    NSMutableData* _key = [[NSMutableData alloc] initWithData:key];
    if([_key length] < kCCKeySizeAES256 + 1)
    {
        [_key increaseLengthBy:kCCKeySizeAES256 + 1 - [_key length]];
    }
    
    NSUInteger bufferSize = [data length] + kCCBlockSizeAES128;
    void* buffer = malloc(bufferSize);
    size_t numBytesEncrypted = 0;
    
    CCCryptorStatus status = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionECBMode,
                                     [_key bytes], kCCKeySizeAES256,
                                     NULL,
                                     [data bytes], [data length],
                                     buffer, bufferSize, &numBytesEncrypted);
    
    if(status != kCCSuccess)
    {
        free(buffer);
        
//        @throw [NSException exceptionWithName:@"DecryptException"
//                                       reason:@"Unable to decrypt data."
//                                     userInfo:NULL];
        
        return nil;
    }
    
    return [NSData dataWithBytes:buffer length:numBytesEncrypted];
}

@end
