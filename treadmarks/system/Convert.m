//
//  Convert.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/6/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Convert.h"

@implementation Convert

+(NSData*)fromHexStringToData:(NSString*)hexString
{
    NSMutableData* data = [[NSMutableData alloc] init];
    
    char bytes[3] = {'\0', '\0', '\0'};
    for(int i = 0; i < [hexString length] / 2; i++)
    {
        bytes[0] = [hexString characterAtIndex:i * 2];
        bytes[1] = [hexString characterAtIndex:i * 2 + 1];
        
        unsigned char byte = strtol(bytes, NULL, 16);
        
        [data appendBytes:&byte length:1];
    }
    
    return data;
}

@end
