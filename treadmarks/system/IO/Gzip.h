//
//  Gzip.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/9/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gzip : NSObject

+ (NSData*) uncompress:(NSData*) inputData;

@end
