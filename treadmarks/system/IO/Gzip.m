//
//  Gzip.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/9/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Gzip.h"
#import <zlib.h>

@implementation Gzip

+ (NSData*) uncompress:(NSData*) inputData
{
    if ([inputData length])
    {
        z_stream stream;
        stream.zalloc = Z_NULL;
        stream.zfree = Z_NULL;
        stream.avail_in = (uint)[inputData length];
        stream.next_in = (Bytef *)[inputData bytes];
        stream.total_out = 0;
        stream.avail_out = 0;
        
        NSMutableData *data = [NSMutableData dataWithLength:(NSUInteger)([inputData length] * 1.5)];
        if (inflateInit2(&stream, 47) == Z_OK)
        {
            int status = Z_OK;
            while (status == Z_OK)
            {
                if (stream.total_out >= [data length])
                {
                    data.length += [inputData length] * 0.5;
                }
                stream.next_out = (uint8_t *)[data mutableBytes] + stream.total_out;
                stream.avail_out = (uInt)([data length] - stream.total_out);
                status = inflate (&stream, Z_SYNC_FLUSH);
            }
            if (inflateEnd(&stream) == Z_OK)
            {
                if (status == Z_STREAM_END)
                {
                    data.length = stream.total_out;
                    return data;
                }
            }
        }
    }
    return nil;
}

@end
