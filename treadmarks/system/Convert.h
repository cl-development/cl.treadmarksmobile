//
//  Convert.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/6/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Convert : NSObject

+(NSData*)fromHexStringToData:(NSString*)hexString;

@end
