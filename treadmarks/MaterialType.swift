//
//  MaterialType.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-06-22.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation
import CoreData

@objc(MaterialType)
class MaterialType: NSManagedObject {

    @NSManaged var materialTypeId: NSNumber
    @NSManaged var nameKey: String
    @NSManaged var shortNameKey: String
    @NSManaged var sortIndex: NSNumber
    @NSManaged var syncDate: Date
    @NSManaged var effectiveStartDate: Date
    @NSManaged var effectiveEndDate: Date
    @NSManaged var transactionTypeMaterialType: NSSet
    
    
  @objc func setValuesWithDictionarySwift(_ dict:NSDictionary)->(Bool) {
        let myKeys = dict.allKeys as! [NSString]
         for key:NSString in myKeys {
            let theValue = dict.value(forKey: key as String)
            if (key.isEqual(to: "effectiveStartDate") || key.isEqual(to: "syncDate") || key.isEqual(to: "effectiveEndDate")) {
                let theDate:Date = TMSyncEngine.shared().unixDate(toNSDate: theValue as! String)
                self.syncDate = theDate
            }
            else {
                self.setValue(theValue, forKey:key as String)
            }
        }
        return true
    }
}
