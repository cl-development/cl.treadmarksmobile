//
//  CollectorView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"

@protocol CollectorViewControllerDelegate <NSObject>

@required


- (void)primaryAction:(UIButton *)sender;
-(void)secondryAction:(UIButton *)sender;
@end
@interface CollectorView : UIView
@property id<CollectorViewControllerDelegate> delegate;
@property (nonatomic, weak)IBOutlet UIView *collectorView;

//Collector VIEW'S BUTTON
- (IBAction)primaryButton:(id)sender;
- (IBAction)secondryButton:(id)sender;

@end
