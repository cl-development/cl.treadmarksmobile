//
//  DockBarViewController.h
//  TreadMarks
//
//  Created by Adi on 2015-07-28.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "btSimplePopUp.h"
#import "ListViewController.h"
#import "ModalViewController.h"
#import "InfoView.h"
#import "LogoutView.h"
#import "AboutView.h"
#import "PopUpView.h"
#import "SyncViewController.h"
#import "TMSyncEngine.h"

@interface DockBarViewController : UIViewController
@property BOOL newTransactionCreated;
@property BOOL createNewTransaction;
@property (weak, nonatomic) IBOutlet UILabel *lastSyncDay;
@property (weak, nonatomic) IBOutlet UILabel *lastSyncTime;

@end
