//
//  EntityMigrationPolicy-9-10.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface EntityMigrationPolicy_9_10 : NSEntityMigrationPolicy

@end
