//
//  EntityMigrationPolicy-2-3.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-04-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface EntityMigrationPolicy_2_3 : NSEntityMigrationPolicy

- (NSString*)newUUIDString;
- (NSString*)newLocationId:(NSMigrationManager*)manager oldLocationId:(NSNumber*)oldLocationId;

@end
