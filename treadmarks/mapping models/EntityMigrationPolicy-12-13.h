//
//  EntityMigrationPolicy12-13.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-07-10.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface EntityMigrationPolicy_12_13 : NSEntityMigrationPolicy

@end
