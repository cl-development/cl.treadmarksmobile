//
//  EntityMigrationPolicy-9-10.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "EntityMigrationPolicy-9-10.h"
#import "TransactionSyncStatusType.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation EntityMigrationPolicy_9_10

- (BOOL)endEntityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error {
	
	BOOL endEntityMapping = [super endEntityMapping:mapping manager:manager error:error];
	
	NSManagedObjectContext* context = [manager destinationContext];
	
	/// First, update records 1-2
	TransactionSyncStatusType* transactionSyncStatusType;
	
	transactionSyncStatusType = [TransactionSyncStatusType context:context singleById:@1];
	transactionSyncStatusType.fileName = @"list-dot-yellow.png";
	transactionSyncStatusType.internalDescription = @"Mobile transaction not synced to repository as yet";
	transactionSyncStatusType.userMessage = @"Synced";
	
	transactionSyncStatusType = [TransactionSyncStatusType context:context singleById:@2];
	transactionSyncStatusType.internalDescription = @"Mobile transaction synced to repository";
	transactionSyncStatusType.userMessage = @"Not Synced";
	
	/// Insert records 3-6
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy/MM/dd"];

	TransactionSyncStatusType* newSyncStatus;
	newSyncStatus = [TransactionSyncStatusType insertNewObject:context];
	newSyncStatus.transactionSyncStatusTypeId = @3;
	newSyncStatus.syncDate = [dateFormatter dateFromString:@"2014/06/03"];
	newSyncStatus.fileName = @"list-dot-red.png";
	newSyncStatus.internalDescription = @"Mobile transaction failed to sync to repository";
	newSyncStatus.nameKey = @"Device_Sync_Fail_Repository";
	newSyncStatus.userMessage = @"Error During Sync";
	
	newSyncStatus = [TransactionSyncStatusType insertNewObject:context];
	newSyncStatus.transactionSyncStatusTypeId = @4;
	newSyncStatus.syncDate = [dateFormatter dateFromString:@"2014/06/03"];
	newSyncStatus.fileName = @"list-dot-yellow.png";
	newSyncStatus.internalDescription = @"A completed mobile transaction saved successfully to repository and sent to TM for submission (waiting for response from TM which is sent to device)";
	newSyncStatus.nameKey = @"Repository_Sent_To_TM";
	newSyncStatus.userMessage = @"Not Synced";
	
	newSyncStatus = [TransactionSyncStatusType insertNewObject:context];
	newSyncStatus.transactionSyncStatusTypeId = @5;
	newSyncStatus.syncDate = [dateFormatter dateFromString:@"2014/06/03"];
	newSyncStatus.fileName = @"list-dot-green.png";
	newSyncStatus.internalDescription = @"A completed mobile transaction successfully submitted to TM";
	newSyncStatus.nameKey = @"Repository_Sync_TM";
	newSyncStatus.userMessage = @"Synced";
	
	newSyncStatus = [TransactionSyncStatusType insertNewObject:context];
	newSyncStatus.transactionSyncStatusTypeId = @6;
	newSyncStatus.syncDate = [dateFormatter dateFromString:@"2014/06/03"];
	newSyncStatus.fileName = @"list-dot-red.png";
	newSyncStatus.internalDescription = @"A completed mobile transaction failed to sync to TM (see responseComments field for error)";
	newSyncStatus.nameKey = @"Repository_Sync_Fail_TM";
	newSyncStatus.userMessage = @"Error During Sync";
	
	[context save:nil];
	
	return endEntityMapping;
}

@end
