//
//  EntityMigrationPolicy12-13.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-07-10.
//  Copyright (c) 2015 Capris. All rights reserved.
//
//  Create the specific static data entities for Material Type
//  This is attached to the TransactionToTransaction mapping entity.
// NOTE: there may be an issue here. Assignments to the MaterialType of attributes apparently should be
// setKey forValue type as the insertion returns only an NSManagedObject (not the subclass).


#import "EntityMigrationPolicy-12-13.h"
#import "NSManagedObject+NSManagedObject.h"

#import "TreadMarks-Swift.h"

@implementation EntityMigrationPolicy_12_13

- (BOOL)endEntityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError *__autoreleasing *)error {
    
    BOOL endEntityMapping = [super endEntityMapping:mapping manager:manager error:error];
    
    NSManagedObjectContext* context = [manager destinationContext];
    
    // Create the specific static data entities for Material Type
    
    
    
    
    /*
    
    
#define MATERIAL_TYPE_ID_TDP4FF                 [NSNumber   numberWithInteger:1]
#define MATERIAL_TYPE_ID_TDP5FT                 [NSNumber   numberWithInteger:2]
#define MATERIAL_TYPE_ID_TDP5NT                 [NSNumber   numberWithInteger:3]
#define MATERIAL_TYPE_ID_TD6NP                  [NSNumber   numberWithInteger:4]
#define MATERIAL_TYPE_ID_TD6FP                  [NSNumber   numberWithInteger:5]
#define MATERIAL_TYPE_ID_TDP7NT                 [NSNumber   numberWithInteger:6]
#define MATERIAL_TYPE_ID_TDP7FT                 [NSNumber   numberWithInteger:7]
#define MATERIAL_TYPE_ID_Transfer_Fiber_Rubber  [NSNumber   numberWithInteger:8]
#define MATERIAL_TYPE_ID_TDP1                   [NSNumber   numberWithInteger:9]
#define MATERIAL_TYPE_ID_TDP2                   [NSNumber   numberWithInteger:10]
#define MATERIAL_TYPE_ID_TDP3                   [NSNumber   numberWithInteger:11]
#define MATERIAL_TYPE_ID_TDP4FF_PI              [NSNumber   numberWithInteger:12]
    
    

     
     TDP4FF - (Feedstock, Shred)
     ◦
     ◦	TDP5FT - (Feedstock, Off-the-Road Tires)
     ◦	TDP5NT - (Feedstock, On-the-Road Tires)
     ◦	TD6NP - (Partial, On-the- Road Tires)
     ◦	TD6FP - (Partial, Off-the-Road Tires)
     ◦	TDP7NT - (Whole, On- the-Road Tires)
     ◦	TDP7FT - (Whole- Off-the-Road Tires)
     ◦	Transfer- Fiber Rubber
     ◦	TDP1 – On-Road Transfer Rubber from Fiber
     ◦	TDP2 – On-Road Transfer Rubber from Fiber
     ◦	TDP3 – On-Road Transfer Rubber from Fiber
     ◦	TDP4FF - (Feedstock, Shred)
     ◦	TDP4FF - (Feedstock, Shred, No PI Claimed)
     
     
    
    
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *initialDate = [[NSDate alloc] init];
    initialDate = [dateFormatter dateFromString:APP_STATIC_DATA_EXTRACT_DATE];
    TransactionType *transType = [TransactionType context:context singleById:TRANSACTION_TYPE_ID_PIT];
    
    MaterialType *type1 = [MaterialType insertNewObject:context];
    //type1.estimatedWeight = [NSDecimalNumber zero];
    //type1.sortIndex = [NSNumber numberWithInt:0];
    type1.nameKey=@"Feedstock, Off-the-Road Tires";
    type1.shortNameKey=@"TDP5FT";
    type1.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type1.materialTypeId = MATERIAL_TYPE_ID_TDP5FT;
    
    TransactionType_MaterialType *bridge1 = [TransactionType_MaterialType insertNewObject:context];
    bridge1.syncDate = initialDate;
    bridge1.transactionTypeMaterialTypeId = [NSNumber numberWithInt:1];
    bridge1.materialType=type1;
    bridge1.transactionType = transType;
    
    MaterialType *type2 = [MaterialType insertNewObject:context];
    //type2.estimatedWeight = [NSDecimalNumber zero];
    //type2.sortIndex = [NSNumber numberWithInt:0];
    type2.nameKey=@"Feedstock, On-the-Road Tires";
    type2.shortNameKey=@"TDP5NT";
    type2.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type2.materialTypeId = MATERIAL_TYPE_ID_TDP5NT;
    
    TransactionType_MaterialType *bridge2 = [TransactionType_MaterialType insertNewObject:context];
    bridge2.syncDate = initialDate;
    bridge2.transactionTypeMaterialTypeId = [NSNumber numberWithInt:2];
    bridge2.materialType=type2;
    bridge2.transactionType = transType;
    
    MaterialType *type3 = [MaterialType insertNewObject:context];
    //type3.estimatedWeight = [NSDecimalNumber zero];
    //type3.sortIndex = [NSNumber numberWithInt:0];
    type3.nameKey=@"Partial, On-the- Road Tires";
    type3.shortNameKey=@"TD6NP";
    type3.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type3.materialTypeId = MATERIAL_TYPE_ID_TD6NP;
    
    TransactionType_MaterialType *bridge3 = [TransactionType_MaterialType insertNewObject:context];
    bridge3.syncDate = initialDate;
    bridge3.transactionTypeMaterialTypeId = [NSNumber numberWithInt:3];
    bridge3.materialType=type3;
    bridge3.transactionType = transType;
    
    MaterialType *type4 = [MaterialType insertNewObject:context];
    // type4.estimatedWeight = [NSDecimalNumber zero];
    //type4.sortIndex = [NSNumber numberWithInt:0];
    type4.nameKey=@"Partial, Off-the-Road Tires";
    type4.shortNameKey=@"TD6FP";
    type4.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type4.materialTypeId = MATERIAL_TYPE_ID_TD6FP;
    
    TransactionType_MaterialType *bridge4 = [TransactionType_MaterialType insertNewObject:context];
    bridge4.syncDate = initialDate;
    bridge4.transactionTypeMaterialTypeId = [NSNumber numberWithInt:4];
    bridge4.materialType=type4;
    bridge4.transactionType = transType;
    
    MaterialType *type5 = [MaterialType insertNewObject:context];
    //type5.estimatedWeight = [NSDecimalNumber zero];
    //type5.sortIndex = [NSNumber numberWithInt:0];
    type5.nameKey=@"Whole, On- the-Road Tires";
    type5.shortNameKey=@"TDP7NT";
    type5.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type5.materialTypeId = MATERIAL_TYPE_ID_TDP7NT;
    
    TransactionType_MaterialType *bridge5 = [TransactionType_MaterialType insertNewObject:context];
    bridge5.syncDate = initialDate;
    bridge5.transactionTypeMaterialTypeId = [NSNumber numberWithInt:5];
    bridge5.materialType=type5;
    bridge5.transactionType = transType;
    
    MaterialType *type6 = [MaterialType insertNewObject:context];
    //type6.estimatedWeight = [NSDecimalNumber zero];
    //type6.sortIndex = [NSNumber numberWithInt:0];
    type6.nameKey=@"Whole- Off-the-Road Tires";
    type6.shortNameKey=@"TDP7FT";
    type6.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type6.materialTypeId = MATERIAL_TYPE_ID_TDP7FT;
    
    TransactionType_MaterialType *bridge6 = [TransactionType_MaterialType insertNewObject:context];
    bridge6.syncDate = initialDate;
    bridge6.transactionTypeMaterialTypeId = [NSNumber numberWithInt:6];
    bridge6.materialType=type6;
    bridge6.transactionType = transType;
    
    MaterialType *type7 = [MaterialType insertNewObject:context];
    //type7.estimatedWeight = [NSDecimalNumber zero];
    //type7.sortIndex = [NSNumber numberWithInt:0];
    type7.nameKey=@"Transfer- Fiber Rubber";
    type7.shortNameKey=@"Transfer- Fiber Rubber";
    type7.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type7.materialTypeId = MATERIAL_TYPE_ID_Transfer_Fiber_Rubber;
    
    TransactionType_MaterialType *bridge7 = [TransactionType_MaterialType insertNewObject:context];
    bridge7.syncDate = initialDate;
    bridge7.transactionTypeMaterialTypeId = [NSNumber numberWithInt:7];
    bridge7.materialType=type7;
    bridge7.transactionType = transType;
    
    MaterialType *type8 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type8.nameKey=@"On-Road Transfer Rubber from Fiber";
    type8.shortNameKey=@"TDP1";
    type8.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type8.materialTypeId = MATERIAL_TYPE_ID_TDP1;
    
    TransactionType_MaterialType *bridge8 = [TransactionType_MaterialType insertNewObject:context];
    bridge8.syncDate = initialDate;
    bridge8.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge8.materialType=type8;
    bridge8.transactionType = transType;
    
    MaterialType *type9 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type9.nameKey=@"Feedstock, Shred";
    type9.shortNameKey=@"TDP1";
    type9.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type9.materialTypeId = MATERIAL_TYPE_ID_TDP1;
    
    TransactionType_MaterialType *bridge9 = [TransactionType_MaterialType insertNewObject:context];
    bridge9.syncDate = initialDate;
    bridge9.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge9.materialType=type9;
    bridge9.transactionType = transType;
    
    MaterialType *type10 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type10.nameKey=@"On-Road Transfer Rubber from Fiber";
    type10.shortNameKey=@"TDP2";
    type10.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type10.materialTypeId = MATERIAL_TYPE_ID_TDP2;
    
    TransactionType_MaterialType *bridge10 = [TransactionType_MaterialType insertNewObject:context];
    bridge10.syncDate = initialDate;
    bridge10.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge10.materialType=type10;
    bridge10.transactionType = transType;
    
    
    MaterialType *type11 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type11.nameKey=@"On-Road Transfer Rubber from Fiber";
    type11.shortNameKey=@"TDP3";
    type11.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type11.materialTypeId = MATERIAL_TYPE_ID_TDP3;
    
    TransactionType_MaterialType *bridge11 = [TransactionType_MaterialType insertNewObject:context];
    bridge11.syncDate = initialDate;
    bridge11.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge11.materialType=type11;
    bridge11.transactionType = transType;
    
    
    
    MaterialType *type12 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type12.nameKey=@"Feedstock, Shred";
    type12.shortNameKey=@"TDP4FF";
    type12.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type12.materialTypeId = MATERIAL_TYPE_ID_TDP4FF;
    
    TransactionType_MaterialType *bridge12 = [TransactionType_MaterialType insertNewObject:context];
    bridge12.syncDate = initialDate;
    bridge12.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge12.materialType=type12;
    bridge12.transactionType = transType;
    
    MaterialType *type13 = [MaterialType insertNewObject:context];
    //type8.estimatedWeight = [NSDecimalNumber zero];
    //type8.sortIndex = [NSNumber numberWithInt:0];
    type13.nameKey=@"Feedstock, Shred, No PI Claimed";
    type13.shortNameKey=@"TDP4FF";
    type13.syncDate = type1.effectiveEndDate = type1.effectiveStartDate = initialDate;
    type13.materialTypeId = MATERIAL_TYPE_ID_TDP4FF_PI;
    
    TransactionType_MaterialType *bridge13 = [TransactionType_MaterialType insertNewObject:context];
    bridge13.syncDate = initialDate;
    bridge13.transactionTypeMaterialTypeId = [NSNumber numberWithInt:8];
    bridge13.materialType=type13;
    bridge13.transactionType = transType;
    
    
     */
    [context save:nil];
    
    return endEntityMapping;
    
}

@end











