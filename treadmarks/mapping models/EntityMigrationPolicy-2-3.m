//
//  EntityMigrationPolicy-2-3.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-04-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "EntityMigrationPolicy-2-3.h"
#import "Location+Location.h"
#import "Transaction.h"
#import "NSManagedObject+NSManagedObject.h"

#import "DataEngine.h"

@implementation EntityMigrationPolicy_2_3

#pragma mark - Custom attribute mapping methods

- (NSString*)newUUIDString {
	
	return [[NSUUID UUID] UUIDString];
}
- (NSString *)newLocationId:(NSMigrationManager *)manager oldLocationId:(NSNumber *)oldLocationId {

	long newLocationId = [oldLocationId longValue];

	if (newLocationId == 0)
	{
		return [self newUUIDString];
	}
	else if (1000000 <= newLocationId && newLocationId <= 9999999)
	{
		return [Location formatLocationId:[NSNumber numberWithLong:newLocationId]];
	}
	else
	{
		Transaction* transaction = (Transaction*)[Transaction context:[manager destinationContext] single:@"friendlyId == %@", oldLocationId];
		
		return transaction.transactionId;
	}
}

@end
