//
//  DayView.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-30.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import "DataEngine.h"

@interface DayView : UIView

@property (nonatomic,strong) IBOutlet UIView  *view;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic,strong) NSArray *transactionArray;
@property (nonatomic,strong) NSDate *date;
@property (nonatomic)        NSInteger deltaMonths;
@property (nonatomic)        BOOL isCurrentMonth;
@property (nonatomic)        BOOL isCurrentDay;
-(void)updateUI;

@end
