//
//  SimpletableCell.h
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TireCountTableCell : UITableViewCell //<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *tireCountField;

@property (strong, nonatomic) IBOutlet UILabel *tireNameLabel;



@end
