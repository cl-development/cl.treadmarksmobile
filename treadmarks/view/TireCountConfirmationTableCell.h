//
//  TireCountConfirmationTableCell.h
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TireCountConfirmationTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tireCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *tireNameLabel;

@end
