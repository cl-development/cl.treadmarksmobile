//
//  SlideView.h
//  TreadMarks
//
//  Created by Dragos Ionel on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListViewController.h"

@interface SlideView : UIView

#pragma mark - IBOutlets

@property (nonatomic,strong) IBOutlet UIView  *view;

#pragma mark - Properties

@property (strong) UIViewController<SlideViewDelegate>* delegate;
@property BOOL showVoid;
@property BOOL allowNewTransactions;


@property (strong) UIView* backgroundView;
@property BOOL newTransactionCreated;

#pragma mark - IBActions

- (IBAction)togglePressed:(id)sender;

#pragma mark - Methods

- (void)hideAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated completion:(void(^)(void))completion;

@end

/*
 //
 //  SlideView.h
 //  TreadMarks
 //
 //  Created by Dragos Ionel on 11/7/2013.
 //  Copyright (c) 2013 Capris. All rights reserved.
 //
 
 #import <UIKit/UIKit.h>
 #import "ListViewController.h"
 #import "QuartzCore/QuartzCore.h"
 #import "TOCPageViewController.h"
 //#import "PageViewController.h"
 
 
 //@protocol SlideViewVoidDelegate <NSObject>
 //
 //@optional
 //- (void) transactionvoided:(NSNotification *)notification;
 //@end
 
 
 
 @interface  SlideView : UIView
 
 
 @property (nonatomic,strong) IBOutlet UIView  *view;
 @property (weak) ListViewController *parentListViewController;
 
 @property (weak, nonatomic) IBOutlet UIButton *slidetovoidButton;
 
 -(IBAction)togglePressed:(id)sender;
 @property (weak, nonatomic) IBOutlet UILabel *tcrLable;
 @property (weak, nonatomic) IBOutlet UILabel *stcLable;
 
 -(void)hide;
 - (IBAction)voidtransactionButton:(id)sender;
 
 @property (weak, nonatomic) IBOutlet UIView *slidetovoidView;
 -(IBAction)slidetovoid:(UIPanGestureRecognizer *)sender;
 
 @end
*/
