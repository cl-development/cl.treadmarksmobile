//
//  SyncView.m
//  TreadMarks
//
//  Created by Adi on 2015-10-11.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "SyncView.h"
#import "ModalViewController.h"

//for Sync
#import "TMSyncEngine.h"
#import "TMAFCLient.h"
#import "TMTransactionSyncStatus.h"
#import "Reachability.h"
#import "NSArray+NSArray_Slice.h"
#import "CDActivityIndicatorView.h"
#import "Utils.h"
#import "Location.h"
#import "Transaction.h"

//

@implementation SyncView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.syncView createCornerRadius:self.syncView];
        
    }
    
    return self;
}


- (IBAction)BackButton:(id)sender
{
    [self.delegate syncCancelAction:sender];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}
@end