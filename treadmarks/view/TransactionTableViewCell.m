//
//  TransactionTableViewCell.m
//  CalendarPilot
//
//  Created by Dragos Ionel on 2013-07-10.
//  Copyright (c) 2013 Adelante Consulting. All rights reserved.
//

#import "TransactionTableViewCell.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionSyncStatusType.h"
#import "DataEngine.h"
#import "Utils.h"
#import "Location.h"
#import "Authorization.h"
#import "NSManagedObject+NSManagedObject.h"

@interface TransactionTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *registrantIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *weekDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthDaySuffix;

@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *synchStatusImageView;


@end

@implementation TransactionTableViewCell

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])){
        [self addSubview:
         [[[NSBundle mainBundle] loadNibNamed:@"TransactionTableViewCell" owner:self options:nil] objectAtIndex:0]
         ];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:
         [[[NSBundle mainBundle] loadNibNamed:@"TransactionTableViewCell" owner:self options:nil] objectAtIndex:0]
         ];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}



-(void)setTransaction:(Transaction *)transaction{
    _transaction=transaction;
//    User *user = self.transaction.outgoingUser;

    User *user;

    if ([Utils getProcessorDidLogin]==YES) {
        user= self.transaction.incomingUser;
//        NSLog([NSString stringWithFormat:@"set for incomingUser %@:",user.registrant.registrationNumber]);
    }
    else{
        user= self.transaction.outgoingUser;
//    NSLog([NSString stringWithFormat:@"set for outgoingUser %@:",user.registrant.registrationNumber]);
    }
/*
    if ([Utils getProcessorDidLogin]==YES) {
        user= self.transaction.outgoingUser;
        //        NSLog([NSString stringWithFormat:@"set for incomingUser %@:",user.registrant.registrationNumber]);
    }
    else{
        user= self.transaction.incomingUser;
        //    NSLog([NSString stringWithFormat:@"set for outgoingUser %@:",user.registrant.registrationNumber]);
    }//created by w
 */
    
    Registrant  *registrantInfo=user.registrant;
    
    
    
    self.registrantIDLabel.text=[registrantInfo.registrationNumber stringValue];

    self.registrantNameLabel.text=registrantInfo.businessName;
    
    
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR) {
        
        if ([registrantInfo.registrationNumber intValue] == 9999999) {
            Registrant *UnReg = [[DataEngine sharedInstance] registrantIdFromMetaData:[self.transaction.friendlyId stringValue]];
            self.registrantIDLabel.text = @"Unregistered";
            self.registrantNameLabel.text=UnReg.businessName;;
            
        }
        
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    self.numberLabel.text = [transaction.friendlyId stringValue];
   
    [dateFormatter setDateFormat:@"EEE"];
    self.weekDayLabel.text   = [[dateFormatter stringFromDate:transaction.transactionDate]uppercaseString];

    [dateFormatter setDateFormat:@"d"];
    NSString *monthDay = [dateFormatter stringFromDate:transaction.transactionDate];
    self.monthDayLabel.text   = monthDay;
    self.monthDaySuffix.text = [Utils ordinalSuffix:[monthDay integerValue]];

    self.typeLabel.text   = transaction.transactionType.shortNameKey;
    // IF transaction = STC put event number / group name
    if([self.typeLabel.text isEqualToString:@"STC"])
    {
       Authorization   *authorization = [[DataEngine sharedInstance] authorizationForTransaction:transaction];
        
        self.registrantIDLabel.text= authorization.authorizationNumber;
        
         Location *locationData=(Location*)[Location singleById:self.transaction.transactionId];//[[DataEngine sharedInstance] locationForId:transaction.friendlyId];
        self.registrantNameLabel.text=locationData.name;
    
    }
    TransactionStatusType *transactionStatusType = transaction.transactionStatusType;
    

    TransactionSyncStatusType *transactionSyncStatusType = transaction.transactionSyncStatusType;
   //change the hand sign!
    // User *user = self.isIncomingUser?self.transaction.incomingUser:self.transaction.outgoingUser;
    //Registrant *registrant = user.registrant;
    //if ([registrant.isActive integerValue]==0)  // i.e. NO
	//{
		//display an error
    
    User        *userInfo=self.transaction.outgoingUser;
    
    
	registrantInfo=userInfo.registrant;
  /* if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_ERROR )
   {//NEW SPEC
       {self.statusImageView.image = [UIImage imageNamed:@"cal-pop-flag-red.png"];}
     }
    else
    {*/
    self.statusImageView.image = [UIImage imageNamed:transactionStatusType.fileName];
//	}
    self.synchStatusImageView.image = [UIImage imageNamed:transactionSyncStatusType.fileName];
    
    // Hack for completed
  /*  if ([registrantInfo.isActive integerValue]==0 &&transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_COMPLETE)
    { //NEW SPEC
        self.statusImageView.image = [UIImage imageNamed:@"cal-pop-flag-red.png"];
        }*/
    
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
