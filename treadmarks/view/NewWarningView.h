//
//  NewAlertView.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-09-17.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewWarningViewDelegate <NSObject>

@required

- (void)primaryAction:(UIButton *)sender;

@optional

- (void)secondaryAction:(UIButton *)sender;

@end

@interface NewWarningView : UIView

#pragma mark - Properties

@property (nonatomic) NSString *title;
@property (nonatomic) NSMutableAttributedString *text;
@property (nonatomic) NSString *primaryButtonText;
@property id<NewWarningViewDelegate> delegate;
@property (nonatomic) NSString *heading;
@property (nonatomic) NSString *secondaryButtonText;
@property (strong, nonatomic) IBOutlet UILabel *singleLineLabel;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIImageView *singlelineforOkay;
@property (nonatomic,strong)IBOutlet UIImageView *imageViewSingleButton;
@property (weak, nonatomic) IBOutlet UIButton *singleButton;

@property (nonatomic) BOOL setColour;

-(void)setUI;

@end
