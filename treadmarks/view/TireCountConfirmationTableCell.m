//
//  TireCountConfirmationTableCell.m
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TireCountConfirmationTableCell.h"

@implementation TireCountConfirmationTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	if(self)
	{
		[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
														owner:self
													  options:NULL] objectAtIndex:0]];
	}
	
	return self;
}

/*=============
 This is all unnecessary.  Instead of manually creating the controls, use a XIB file
 to layout.
 ==============
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
    if (self)
	{
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.frame = CGRectMake(0, 0, 320, 80);
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
        
        _tireCountField = [[UITextField alloc] initWithFrame:CGRectMake(26.25, 14, 95, 41)];

        [[_tireCountField layer] setMasksToBounds:8.0f];
        //[[_tireCountField layer] setBorderColor:[[UIColor grayColor] CGColor]];
        //[[_tireCountField layer] setBorderWidth:1.0f];
        
        [_tireCountField setRightView:paddingView];
        [_tireCountField setLeftView:paddingView];
        [_tireCountField setRightViewMode:UITextFieldViewModeAlways];
        [_tireCountField setLeftViewMode:UITextFieldViewModeAlways];
        [_tireCountField setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:26]];
        [_tireCountField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [_tireCountField setTextAlignment:NSTextAlignmentRight];
        
        [self.contentView addSubview:_tireCountField];
        
        UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _tireCountField.inputView = dummyView;
        
        _tireNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 14, 470, 40)];
        [_tireNameLabel setBackgroundColor:[UIColor clearColor]];
        [_tireNameLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:24]];
        [_tireNameLabel setNumberOfLines:2];
        [_tireNameLabel setLineBreakMode:NO];
        [self.contentView addSubview:_tireNameLabel];
    }
	
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
 */

@end
