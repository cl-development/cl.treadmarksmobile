//
//  ComponentView.h
//  ComponentsPilot
//  Component View base class
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PageViewController;
@class Transaction;

@protocol ComponentViewDelegate <NSObject>

@optional
- (void)didRemoveComponent:(NSNotification *)notification;
- (void)didAddComponent:(NSNotification *)notification;
@end

@interface ComponentView : UIView

@property (nonatomic,weak) Transaction *transaction;

//database entity related to the component
@property (nonatomic,weak) NSManagedObject *entity;

//if true, all the controls are read only
// e.g., if the owning transaction has been completed, the component cannot be edited
@property BOOL readOnly;

//the page that is including the component
@property (nonatomic,weak) PageViewController *parentPageViewController;

// delegate
@property (nonatomic,weak) id delegate;

//saves all the information to database
-(void)save;

//update the interface
-(void)updateUI;

//validation items

//image for validation
@property (nonatomic,weak) IBOutlet UIImageView *validateImageView;

//true if the component is valid
@property (nonatomic) BOOL valid;

//validates the controls on the page
-(void)validate;

//update the validate image, if any
-(void)updateValidateImage;

@end
