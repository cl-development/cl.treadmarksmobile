//
//  TransactionInformationComponentView.m
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TransactionInformationComponentView.h"
#import "Transaction.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"


//#define TRANSACTION_COMPONENT_WIDTH 640
#define TRANSACTION_COMPONENT_HEIGHT 250

@interface TransactionInformationComponentView ()

@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionDateLabel;

@end

@implementation TransactionInformationComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TRANSACTION_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
    }
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUI{    
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIdLabel.text = [self.transaction.friendlyId stringValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SHORT_DATE_FORMAT];
    self.transactionDateLabel.text = [dateFormatter stringFromDate:self.transaction.createdDate];//Changed from self.transaction.transactionDate
    
    [self validate];
}

@end
