//
//  ComponentView.m
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "ComponentView.h"
#import "DataEngine.h"
#import "Constants.h"
#import "Transaction.h"
#import "Utils.h"

@implementation ComponentView

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    self.transaction = [Utils getTransaction];
    self.readOnly=NO;

    return self;
}

-(void)connectNib {}

-(void)updateValidateImage{
    
    
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}

//validates the controls on the page
-(void)validate{
    self.valid = YES;
    [self updateValidateImage];
}

//saves all the information to database
-(void) save {
    [[DataEngine sharedInstance] saveContext];
}

//update the interface
-(void)updateUI{}

@end
