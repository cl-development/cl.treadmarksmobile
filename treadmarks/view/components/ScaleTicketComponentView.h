//
//  ScaleTicketComponentView.h
//  ComponentsPilot
//
//  Created by Dennis Christopher on 2013-10-24.
//  Copyright (c) 2013 Adelante. All rights reserved.
//  ASSUMPTIONS:
//  Presumably the dataEngine member will be set by the containing page

#import <MobileCoreServices/MobileCoreServices.h>

#import "ComponentView.h"
#import "ScaleTicket.h"
#import "DataEngine.h"

@interface ScaleTicketComponentView : ComponentView <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (id)initWithScaleTicket:(ScaleTicket *)ticket;

@end
