//
//  TireCountMessageComponentView
//  Screen to display the help screen for tire screen
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComponentView.h"

@interface TireCountMessageComponentView : ComponentView

@property (nonatomic,strong) IBOutlet UIView  *view;

//returns YES if the component is displayed
@property (nonatomic) BOOL onScreen;

@end
