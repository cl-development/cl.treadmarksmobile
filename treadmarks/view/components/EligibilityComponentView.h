//
//  EligibilityComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ComponentView.h"


@interface EligibilityComponentView : ComponentView
- (id)init;
@end
