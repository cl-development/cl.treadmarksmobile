//
//  NotesComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-05.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ComponentView.h"
#import "Comment.h"

@interface NotesComponentView : ComponentView <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;
- (id)initWithComment:(Comment *)photo;
- (CGFloat)heightOn;
@property (weak, nonatomic) IBOutlet UILabel *NotesLabel;
@property (nonatomic, assign) NSUInteger type;
@end
