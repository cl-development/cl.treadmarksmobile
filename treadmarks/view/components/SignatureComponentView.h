//
//  SignatureComponentView.h
//  TreadMarks
//
//  Created by Capris Group on 11/19/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ComponentView.h"
#import "AlertViewController.h"
#import <CoreLocation/CoreLocation.h>

@class SignatureLandscapeHelperViewController;

@interface SignatureComponentView : ComponentView

- (id)initWithParticipant:(NSInteger)participantType;
- (void)deleteSignature;

@end
