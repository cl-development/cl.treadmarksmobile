//
//  TireCountComponentNewView
//  
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComponentView.h"

@interface TireCountComponentView : ComponentView <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet UIView  *view;

//bolean to manage the on/off screen for the ruler
@property (nonatomic) BOOL showRuler;

-(void)saveInputs;

@end
