//
//  TireCountComponentNewView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TireCountMessageComponentView.h"
#import "Transaction.h"
#import "Constants.h"
#import "Utils.h"
#import "PageViewController.h"

#define TIRE_COUNT_MESSAGE_COMPONENT_WIDTH 695
#define TIRE_COUNT_MESSAGE_COMPONENT_HEIGHT 925

@interface TireCountMessageComponentView ()

@property (weak, nonatomic) IBOutlet UIButton *doNotShowButton;

@end

@implementation TireCountMessageComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, TIRE_COUNT_MESSAGE_COMPONENT_WIDTH, TIRE_COUNT_MESSAGE_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
    }
    self.valid = YES;
    self.onScreen=YES;
    self.doNotShowButton.selected = YES;
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUI{
    //check if this user selected "do not show" before

    NSArray *userIdArray =  [[NSUserDefaults standardUserDefaults] arrayForKey:TIRE_COUNT_DO_NOT_SHOW_KEY];
    if ([userIdArray containsObject:[Utils getUserId]]) {
        //the user selected do not show before
        self.onScreen=NO;
        return;
    }
}

#define DO_NOT_SHOW_ANIMATION_DURATION 1

- (IBAction)beginButtonPressed:(id)sender {
    //check if option is selected and prevent the screen to show next time

    if (self.doNotShowButton.selected) {
        //update the array of users for which the screen is not to be shown
        NSArray *userIdArray =  [[NSUserDefaults standardUserDefaults] arrayForKey:TIRE_COUNT_DO_NOT_SHOW_KEY];
        
        NSMutableArray *userIdMutableArray = userIdArray?[userIdArray mutableCopy]:[NSMutableArray new];
        
        [userIdMutableArray addObject:[Utils getUserId]];
        
        [[NSUserDefaults standardUserDefaults] setObject:userIdMutableArray forKey:TIRE_COUNT_DO_NOT_SHOW_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    self.onScreen = NO;
    [self.parentPageViewController validate];
}

- (IBAction)doNotShowAgainPressed:(id)sender {
    self.doNotShowButton.selected = 1 - self.doNotShowButton.selected;
}

- (IBAction)tapRecognized:(id)sender {
    [Utils log:@"TireCountMessageComponentView.tapRecognized"];
}

@end
