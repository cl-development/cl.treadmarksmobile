//
//  SignatureComponentView.m
//  TreadMarks
//
//  Created by Capris Group on 11/19/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "SignatureComponentView.h"
#import "NSManagedObject+NSManagedObject.h"
#import "DataEngine.h"
#import "SignatureLandscapeHelperViewController.h"
#import "SignatureHelperNavigationController.h"
#import "PhotoType+PhotoType.h"
//#import "PhotoType.h"
#import "Utils.h"
#import "Transaction+Transaction.h"
#import "AppDelegate.h"

@interface SignatureComponentView() <GLKViewControllerDelegate, CLLocationManagerDelegate, SignatureLandscapeViewDelegate>

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *addSignatureButton;
@property (weak, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (weak, nonatomic) IBOutlet UILabel *signatureLabel;

#pragma mark -

@property NSInteger participantType;
@property (nonatomic, strong)PhotoType *signaturePhotoType;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@end

@implementation SignatureComponentView

#pragma mark - Initializers

- (id)initWithParticipant:(NSInteger)participantType {
	
	UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
												  owner:self
												options:nil] objectAtIndex:0];
	self = [self initWithFrame:view.frame];
	
	if(self)
	{
		[self addSubview:view];
		
		self.valid = NO;
		self.participantType = participantType;
        self.signaturePhotoType = (PhotoType*)[PhotoType singleById:PHOTO_TYPE_ID_SIGNATURE];
	}
	
	return self;
}

#pragma mark - Override methods

- (void)updateUI {
	
	[super updateUI];
	
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
	User* user;
	NSString* message;
	NSString* name;
	Photo* photo;
	
	if(self.participantType == OUTGOING_USER)
	{
		user = self.transaction.outgoingUser;
		message = self.transaction.transactionType.outgoingSignatureKey;
		name = self.transaction.outgoingSignatureName;
		photo = self.transaction.outgoingSignaturePhoto;
	}
	else if(self.participantType == INCOMING_USER)
	{
		user = self.transaction.incomingUser;
		message = self.transaction.transactionType.incomingSignatureKey;
		name = self.transaction.incomingSignatureName;
		photo = self.transaction.incomingSignaturePhoto;
	}

	// TBD: remedy in future by funding from data base for the different participant types
    if (self.participantType == OUTGOING_USER && self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC){
        self.titleLabel.text = @"Event Representative";
    }
    else {
        
        if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
            if (self.participantType == INCOMING_USER) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.titleLabel.text =@"Inbound Hauler";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.titleLabel.text =@"Outbound Hauler";
                
            }
        }
        else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
        {
            if (self.participantType == INCOMING_USER) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.titleLabel.text =@"Outgoing Processor";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.titleLabel.text =@"Incoming Processor";
                
            }
            
        }
        else
        {
        self.titleLabel.text = user.registrant.registrantType.descriptionKey;
        }
        
    }
	self.messageLabel.text = [message stringByReplacingOccurrencesOfString:@"_" withString:@" "];

	if(self.valid)
	{
		self.addSignatureButton.hidden = YES;
		
		@try
		{
			NSString* filePath = photo.fileName;
			
			//NSLog(@"%@", filePath);
			
			UIImage* image = [UIImage imageWithContentsOfFile:filePath];
			
			/// If the entire image path was not stored in the database, then load it using the old way
			if(image == NULL)
			{
				NSArray* components = [filePath componentsSeparatedByString:@"/"];
				
				filePath = [NSString pathWithComponents:@[NSHomeDirectory(),
														  @"Documents",
														  [components lastObject]]];
				
				//NSLog(@"%@", filePath);
				
				image = [UIImage imageWithContentsOfFile:filePath];
			}
			
			self.signatureImageView.image = image;
		}
		@catch (NSException* e)
		{
#if DEBUG
//			@throw e;
#endif
		}
		
		self.signatureLabel.text = name;
	}
	else
	{
		self.addSignatureButton.hidden = NO;
		self.signatureImageView.image = NULL;
		self.signatureLabel.text = NULL;
	}

	[self updateValidateImage];
}
- (BOOL)valid {
	
	NSString* name;
	Photo* photo;
	
	if(self.participantType == OUTGOING_USER)
	{
        if (![self.transaction.outgoingSignaturePhoto.fileName isEqualToString:@"DELETE"]) {
            name = self.transaction.outgoingSignatureName;
            photo = self.transaction.outgoingSignaturePhoto;
        }
	}
	else if(self.participantType == INCOMING_USER)
	{
        if (![self.transaction.incomingSignaturePhoto.fileName isEqualToString:@"DELETE"]) {
            name = self.transaction.incomingSignatureName;
            photo = self.transaction.incomingSignaturePhoto;
        }
	}
	
	return name && photo;
}

#pragma mark -

- (void)removeItemAtPath:(Photo *)signaturePhoto {
	
    if (signaturePhoto.syncDate==nil) {
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSError* error;
        
        //NSLog(@"%@", signaturePhoto.fileName);
        
        if([fileManager removeItemAtPath:[Utils pathForPhotoWithPhotoName:signaturePhoto.fileName] error:&error] == NO)
        {
            NSString* fileName = signaturePhoto.fileName;
            NSArray* components = [fileName componentsSeparatedByString:@"/"];
            
            fileName = [NSString pathWithComponents:@[NSHomeDirectory(),
                                                      @"Documents",
                                                      [components lastObject]]];
            
            if([fileManager removeItemAtPath:fileName error:&error] == NO)
            {
#if DEBUG
                @throw [NSException exceptionWithName:@"Error"
                                               reason:error.description
                                             userInfo:error.userInfo];
#endif
            }
        }
        
        [[DataEngine sharedInstance].managedObjectContext deleteObject:signaturePhoto];
    }
	
}
- (void)deleteSignature {
	
    NSError *error;
	if(self.participantType == OUTGOING_USER)
	{
        [[NSFileManager defaultManager] removeItemAtPath:[Utils pathForPhotoWithPhotoName:self.transaction.outgoingSignaturePhoto.fileName] error:&error];
        if (self.transaction.outgoingSignaturePhoto.syncDate==nil && ![self.transaction.outgoingSignaturePhoto.fileName isEqualToString:@"DELETE" ]) {
            //NSLog(@"The path of the out signature photo is: %@", self.transaction.outgoingSignaturePhoto.fileName);
            //[self removeItemAtPath:self.transaction.outgoingSignaturePhoto];
            self.transaction.outgoingSignatureName = NULL;
            //self.transaction.outgoingSignaturePhoto = NULL;
            [[DataEngine sharedInstance].managedObjectContext deleteObject:self.transaction.outgoingSignaturePhoto];

        } else {
            [[DataEngine sharedInstance] deletePhoto:self.transaction.outgoingSignaturePhoto];
            self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
        }
        
	}
	else if(self.participantType == INCOMING_USER)
	{
        [[NSFileManager defaultManager] removeItemAtPath:[Utils pathForPhotoWithPhotoName:self.transaction.incomingSignaturePhoto.fileName] error:&error];
        if (self.transaction.incomingSignaturePhoto.syncDate==nil && ![self.transaction.incomingSignaturePhoto.fileName isEqualToString:@"DELETE"]) {
             //NSLog(@"The path of the in signature photo is: %@", self.transaction.incomingSignaturePhoto.fileName);
            //[self removeItemAtPath:self.transaction.incomingSignaturePhoto];
            self.transaction.incomingSignatureName = NULL;
            //self.transaction.incomingSignaturePhoto = NULL;
            [[DataEngine sharedInstance].managedObjectContext deleteObject:self.transaction.incomingSignaturePhoto];

        } else {
            [[DataEngine sharedInstance] deletePhoto:self.transaction.incomingSignaturePhoto];
        }

		self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
	}
	
	[[DataEngine sharedInstance] saveContext];
	
	[self updateUI];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation=[locations lastObject];
    [Utils log:@"Login.location: %@",[newLocation description]];
    self.latitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    self.longitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    ////NSLog(@"The latitude is this: %@ abd the lognitaude is that: %@", [NSNumber numberWithDouble:newLocation.coordinate.latitude], [NSNumber numberWithDouble:newLocation.coordinate.longitude]);
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [Utils log:@"Location error:",[error localizedDescription]];
    [self.locationManager stopUpdatingLocation];
}

-(void) restrictRotation:(BOOL) restriction{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}
#pragma mark - IBActions

- (IBAction)addSignature:(UIButton *)sender {
	
    SignatureLandscapeHelperViewController *sigView = [[SignatureLandscapeHelperViewController alloc] initWithNibName:@"SignatureLandscapeHelperViewController" bundle:nil];
	sigView.delegate = self;
	sigView.signatureDelegate = self;
	sigView.buttonTag = self.participantType;
    [self restrictRotation:YES];
    SignatureHelperNavigationController *navigationController = [[SignatureHelperNavigationController alloc] initWithRootViewController:sigView];
    //[self.parentPageViewController presentViewController:navigationController animated:YES completion:nil];
    [self.parentPageViewController presentViewController:navigationController animated:YES];
}

#pragma mark - GLKViewControllerDelegate

- (void)glkViewControllerUpdate:(GLKViewController *)controller {
	/// Not really sure what this is for.
}

#pragma mark - SignatureLandscapeViewDelegate

-(void)imageResult:(UIImage *)image participant:(NSString *)name participantType:(NSInteger)type {
	
    /// Save the image as a file
    //Removed "Signature-" prefix from filePath object since we are now able to save the PhotoType of each image as a separate attribute on the Photo object.
	NSString* fileName = [NSString stringWithFormat:@"%@-%ld.png",
						  self.transaction.transactionId,
						  (long)self.participantType];
	NSString* filePath = [Utils pathForPhotoWithPhotoName:fileName];
	
	if([UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES] == FALSE)
	{
#if DEBUG
		@throw [NSException exceptionWithName:@"Error"
									   reason:@"There was a problem saving the image file."
									 userInfo:NULL];
#endif
	}
	
	/// Update the database
	//NSManagedObjectContext* context = [[DataEngine sharedInstance] managedObjectContext];
	
	Photo* photo = (Photo*)[[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
	NSDate* date = [NSDate date];
	photo.createdDate = date;
	photo.date = date;
	photo.photoId = [[NSUUID UUID] UUIDString];
	photo.transaction = self.transaction;
	photo.fileName = fileName;
	photo.photoType = self.signaturePhotoType;
    photo.latitude = [self.latitude stringValue];
    photo.longitude = [self.longitude stringValue];
    photo.syncDate = nil;
	photo.imageSyncDate = nil;
    
    //NSLog(@"tis is the sig lat: %@ and the sig long %@", photo.latitude, photo.longitude);
	//photo.comments = NULL;
	
	if(self.participantType == OUTGOING_USER)
	{
		self.transaction.outgoingSignatureName = name;
		self.transaction.outgoingSignaturePhoto = photo;
	}
	else if(self.participantType == INCOMING_USER)
	{
		self.transaction.incomingSignatureName = name;
		self.transaction.incomingSignaturePhoto = photo;
	}
	
	[self.transaction clearSyncStatus];
	
	[self save];

	/// Update the UI
	[self updateUI];
	
	/// Validate
	self.valid = YES;
	[self updateValidateImage];
	
	[self.parentPageViewController updateUI];
}

@end
