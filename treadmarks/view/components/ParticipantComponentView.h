//
//  ParticipantComponentView.h
//  Treadmarks
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComponentView.h"
#import "Transaction.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>

@interface ParticipantComponentView : ComponentView <CLLocationManagerDelegate>

@property (nonatomic,strong) IBOutlet UIView  *view;
@property (nonatomic) BOOL isIncomingUser;

- (void) setUser: (User *) user;

@end
