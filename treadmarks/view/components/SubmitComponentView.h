//
//  SubmitComponentView.h
//  TreadMarks
//
//  Created by Dragos Ionel on 11/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ComponentView.h"
#import <QuartzCore/QuartzCore.h>
#import "TOCPageViewController.h"

@interface SubmitComponentView : ComponentView <UIGestureRecognizerDelegate>

@property (nonatomic,strong) IBOutlet UIView  *view;

@property (nonatomic) BOOL enabled;

@end