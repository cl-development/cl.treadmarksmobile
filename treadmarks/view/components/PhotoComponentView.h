//
//  PhotoComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-10-29.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "ComponentView.h"
#import "Photo.h"
#import "DataEngine.h"


@interface PhotoComponentView : ComponentView <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate>
@property (nonatomic, strong) Photo *photo;
@property (nonatomic, strong) DataEngine *dataEngine;

- (id)initWithPhoto:(Photo *)photo;
-(void) updateComponentCanAddStatus:(BOOL)canAdd;

@end
