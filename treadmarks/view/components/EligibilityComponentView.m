//
//  EligibilityComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-11.
//  Copyright (c) 2013 Capris. All rights reserved.
//
//  components job:
//  for each elig found
//  create a trans_elig instance
//  set its ids and properties--set the transaction field of the Trans_Elig to the owning transaction
//  store in model

#import "EligibilityComponentView.h"
#import "DataEngine.h"
#import "Transaction_Eligibility.h"
#import "Transaction.h"
#import "TransactionType.h"
#import "Constants.h"
#import "ConfirmationPageViewController.h"


@interface EligibilityComponentView ()
@property (nonatomic, strong) NSArray *trans_eligibilities;   // these are transaction eligibilities
@property (nonatomic, strong) DataEngine *dataEngine;
@property (nonatomic, weak) IBOutlet UISwitch *tiresEligibleSwitch;
@property (nonatomic, weak) IBOutlet UILabel *tiresEligibleLabel;
@property (nonatomic, weak) IBOutlet UISwitch *generatedSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *chargedSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *generatedPriorSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *pickedUpSwitch;
@property (nonatomic, weak) IBOutlet UILabel *generatedLabel;
@property (nonatomic, weak) IBOutlet UILabel *chargedLabel;
@property (nonatomic, weak) IBOutlet UILabel *generatedPriorLabel;
@property (nonatomic, weak) IBOutlet UILabel *pickedUpLabel;
@property (nonatomic, assign) BOOL tiresEligible;
@end


@implementation EligibilityComponentView

#define ELIGIBILITY_COMPONENT_HEIGHT 320

- (id)init {
    self = [self initWithFrame:CGRectNull];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, ELIGIBILITY_COMPONENT_HEIGHT)];
    if (self) {
        self.dataEngine = [DataEngine sharedInstance];
        [self connectNib];
        self.tiresEligible = YES;
        //self.valid=YES;
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

//-(void) setUpUI {
-(void) updateUI {
    
    self.transaction = self.parentPageViewController.transaction;
    Transaction *owningTransaction = self.transaction;
    self.trans_eligibilities = [self.dataEngine transactionEligibilitiesForTransaction:owningTransaction];
    
    // if found, configure the ui accordingly
   for (Transaction_Eligibility *trans_elig in self.trans_eligibilities) {
        if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE] )   {
            self.tiresEligibleSwitch.on = [trans_elig.value boolValue];
            self.tiresEligibleLabel.text = self.tiresEligibleSwitch.on ? @"Yes" : @"No";
            self.tiresEligible = self.tiresEligibleSwitch.on;
            
        }
        if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_GENERATED] )   {
            self.generatedSwitch.on = [trans_elig.value boolValue];
        }
       if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_CHARGED] )   {
           self.chargedSwitch.on = [trans_elig.value boolValue];
       }
       if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_GENERATED_PRIOR] )   {
           self.generatedPriorSwitch.on = [trans_elig.value boolValue];
       }
       
       
    }
    
//    self.chargedSwitch.enabled = NO;
//    self.generatedPriorSwitch.enabled = NO;
//    self.pickedUpSwitch.enabled = NO;
    

    if ([owningTransaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_TCR]) {
        if (self.tiresEligible==NO) {
           self.generatedSwitch.enabled = YES;
            self.generatedLabel.enabled = YES;
         }
    }
    else if ([owningTransaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_DOT]) {
        if (self.tiresEligible==NO) {
            self.generatedSwitch.enabled = YES;
            self.generatedLabel.enabled = YES;
        }
    }
    else if ([owningTransaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_UCR]) {
        
        if (self.tiresEligible==NO) {
//            self.chargedSwitch.enabled = YES;
//            self.chargedLabel.enabled = YES;
//            self.generatedPriorSwitch.enabled =YES;
//            self.generatedPriorLabel.enabled = YES;
//            self.generatedSwitch.enabled = NO;
//            self.generatedLabel.enabled = NO;
//            self.pickedUpSwitch.enabled = NO;
//            self.pickedUpLabel.enabled = NO;
            self.tiresEligibleSwitch.on = NO;
            self.tiresEligibleLabel.text = self.tiresEligibleSwitch.on ? @"Yes" : @"No";
        }
    }
    if (self.tiresEligible==YES) {  // i.e all off
        [self disableCheckBoxes:YES labelsEnabled:NO];
     }
    
    if (self.readOnly==YES) {
        self.tiresEligibleSwitch.enabled=NO;
        [self disableCheckBoxes:NO labelsEnabled:YES];
        [self validate];
        return;
    }

    [self validate];
}

#pragma mark - IBActions

-(IBAction) eligibilityButtonPressed:(id)sender {
    self.tiresEligible = self.tiresEligibleSwitch.on;
    
    [self saveTransactionEligibity:sender];
    
    [self save];
    
    [self updateUI];
}

-(IBAction) tireReasonButtonPressed:(id)sender {
    [self saveTransactionEligibity:sender];
    
    [self save];
    [self validate];
}


// Private
-(void) disableCheckBoxes:(BOOL)switchOff labelsEnabled:(BOOL)labelsEnabled{
    // Shivangi's suggestion:
    if (switchOff==YES) {
        
        self.generatedSwitch.on=NO;
        self.chargedSwitch.on=NO;
        self.generatedPriorSwitch.on=NO;
        self.pickedUpSwitch.on=NO;
            }

    self.generatedSwitch.enabled=NO;
    self.chargedSwitch.enabled=NO;
    self.generatedPriorSwitch.enabled=NO;
    self.pickedUpSwitch.enabled=NO;
    
    if (labelsEnabled==NO) {
        self.generatedLabel.enabled=NO;
        self.chargedLabel.enabled=NO;
        self.generatedPriorLabel.enabled=NO;
        self.pickedUpLabel.enabled=NO;
    }
}

// modify existing or create new if needed
-(void) saveTransactionEligibity:(UISwitch *)theSender {
    NSNumber *theType;
    if (theSender==self.tiresEligibleSwitch) {
         theType = ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE;
    }
    else if (theSender==self.generatedSwitch) {
        theType = ELIGIBILITY_TYPE_ID_TIRES_GENERATED;
    }
    else if (theSender==self.chargedSwitch) {
        theType = ELIGIBILITY_TYPE_ID_TIRES_CHARGED;
    }
    else if (theSender==self.generatedPriorSwitch) {
        theType = ELIGIBILITY_TYPE_ID_TIRES_GENERATED_PRIOR;
    }
    else if (theSender==self.pickedUpSwitch) {
        theType = ELIGIBILITY_TYPE_ID_TIRES_NON_REGISTERED;
    }

    BOOL found = NO;
    Transaction_Eligibility *theTransElig;
    if ([self.trans_eligibilities count] !=0) {
        for (Transaction_Eligibility *trans_elig in self.trans_eligibilities) {
            if ( trans_elig.eligibility.eligibilityId==theType ) {
                theTransElig = trans_elig;
                found = YES;
                break;
            }
        }
    }
    if (found==NO) {
        theTransElig = (Transaction_Eligibility *)[self.dataEngine newEntity:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
        theTransElig.transactionEligibilityId = [[NSUUID UUID] UUIDString];
    }
    Eligibility *elig = [self.dataEngine eligibilityForId:theType];

    //theTransElig.syncDate = [NSDate date];
    theTransElig.syncDate = nil;
    theTransElig.value = [NSNumber numberWithBool:theSender.on];
    theTransElig.eligibility = elig;
    theTransElig.transaction = self.transaction;
}

-(void)validate{
    self.valid = YES;
    if (self.tiresEligible==NO) {
        self.valid = self.generatedSwitch.on || self.pickedUpSwitch.on || self.generatedPriorSwitch.on
        || self.chargedSwitch.on;
        
    }
    // call page validation
    [self updateValidateImage];
    [self.parentPageViewController validate];
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}

@end
