//
//  NotesComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-05.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "NotesComponentView.h"
#import "UIPlaceHolderTextView.h"
#import "OpenSansRegularTextView.h"
#import "DataEngine.h"
#import "Constants.h"

@interface NotesComponentView ()
@property (nonatomic, strong) Comment *comment;
@property (nonatomic, strong) DataEngine *dataEngine;
@property (nonatomic, assign) float componentHeight;
@property (nonatomic, strong) IBOutlet OpenSansRegularTextView *textView;


@end

@implementation NotesComponentView

#define NOTE_COMPONENT_HEIGHT 200
#define TOC_NOTES_COMPONENT_HEIGHT_OFF 80
#define MIN_COMMENT_TEXTVIEW_HEIGHT 175
#define COMMENT_BOTTOM_MARGIN 45
#define COMMENT_TEXT_MARGIN 10

- (id)initWithComment:(Comment *)comment {
    self.comment = comment;
    self = [self initWithFrame:CGRectNull];
    if (self) {
        self.dataEngine = [DataEngine sharedInstance];
        //self.textView.placeholder = @"Enter comment here.";
    }
    return self;
}
-(Comment *) commentOfType:(NSUInteger) type {
    Comment *result = nil;
    NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
    if (comments != nil) {
        for (Comment *comment in comments) {
            if (type==COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
                    result = comment;
                    break;
                }
            }
            else if (type==COMMENT_TYPE_DEFAULT) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                    result = comment;
                    break;
                }
            }
        }
    }
    return result;
}

-(NSString *)displayText {
    return [self.comment.text stringByReplacingOccurrencesOfString:WEIGHT_VARIANCE_COMMENT_HEADER withString:@""];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, NOTE_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
        //[self setUpUI];
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

/*-(void) setUpUI {
    if (self.comment != nil) {
        self.textView.text = self.comment.text;
    }
    if (self.readOnly==YES) {
        self.textView.editable=NO;
    }
    [self validate];
}*/

-(void) updateUI {
    
    if (self.comment != nil) {
        self.textView.text = [self.comment.text stringByReplacingOccurrencesOfString:WEIGHT_VARIANCE_COMMENT_HEADER withString:@""];
    }
    
    if (self.readOnly==YES) {
        self.textView.editable=NO;
        //NSLog(@"The height of this ffframe is: %f", self.frame.size.height);
        //NSLog(@"what about...%f", self.textView.frame.size.height);
    }
    
    [self validate];
}

- (CGFloat)heightOn {
	
	// Calculate the height of the comment box.
	float height = MIN(MIN_COMMENT_TEXTVIEW_HEIGHT,
					   [self.textView.attributedText boundingRectWithSize:CGSizeMake(self.textView.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
																	  context:NULL].size.height + COMMENT_TEXT_MARGIN);
	float componentHeight = self.textView.frame.origin.y + height;
	componentHeight += DEFAULT_MARGIN;
	componentHeight += BR_IMAGE_HEIGHT;
	//NSLog(@"The Height is: %f", height);
    //NSLog(@"The Component Height is: %f", componentHeight);
    //NSLog(@"The Textview height is: %f", self.textView.frame.origin.y);
    return componentHeight;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView == self.textView) {
        if (self.comment==nil) {
            self.comment = (Comment *)[self.dataEngine newEntity:COMMENT_ENTITY_NAME];
        }
        self.comment.text = textView.text;
        self.comment.transaction = self.transaction;
        self.comment.syncDate = nil;
        [self save];
    }
}

-(void)validate{
    self.valid = YES;
    //[self.parentPageViewController validate];
}





@end
