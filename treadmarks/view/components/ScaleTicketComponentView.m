//
//  ScaleTicketComponentView.m
//  ComponentsPilot
//
//  Created by Dennis Christopher on 2013-10-24.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "ScaleTicketComponentView.h"
#import "Photo.h"
#import "UnitType.h"
#import "Constants.h"
#import "ScaleTicketType.h"
#import "PageViewController.h"
#import "Utils.h"

@interface ScaleTicketComponentView ()
@property (nonatomic, strong) DataEngine *dataEngine;
@property (nonatomic, strong) ScaleTicket *scaleTicket;
@property (nonatomic, weak) IBOutlet UITextField *scaleTicketNumber;
@property (nonatomic, weak) IBOutlet UITextField *scaleticketWeight;
@property (nonatomic, weak) IBOutlet UITextField *date;
@property (nonatomic, weak) IBOutlet UISwitch *inbound;
@property (nonatomic, weak) IBOutlet UILabel *directionLabel;
@property (nonatomic, weak) IBOutlet UIDatePicker *calendar;
@property (nonatomic, weak) IBOutlet UIImageView *scaleTicketImage;
@property (nonatomic, weak) IBOutlet UIButton *addMoreButton;
@property (nonatomic, weak) IBOutlet UIView *calendarMaskView;
@property (nonatomic, weak) IBOutlet UIButton *addPhotoButton;
@property (nonatomic, weak) IBOutlet UISwitch *weightUnitsSwitch;
@property (nonatomic, weak) IBOutlet UILabel *weightUnitsLabel;
@property (strong, nonatomic) IBOutlet UIButton *calendarButton;

@end

@implementation ScaleTicketComponentView

#define SCALE_TICKET_COMPONENT_WIDTH 640
#define SCALE_TICKET_COMPONENT_HEIGHT 380

BOOL calendarVisible;


- (id)initWithScaleTicket:(ScaleTicket *)ticket {
    self.scaleTicket = ticket;
    self.dataEngine = [DataEngine sharedInstance];
    return [self initWithFrame:CGRectNull];
}

    
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, SCALE_TICKET_COMPONENT_WIDTH, SCALE_TICKET_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
        [self updateUI];
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void) updateUI {
    calendarVisible = NO;
    self.calendarMaskView.alpha = 0;
    if (self.scaleTicket != Nil) {
        // assign the image view by getting the file at location
        Photo *thePhoto = self.scaleTicket.photo;
        if (thePhoto != nil) {
            self.addPhotoButton.hidden = YES;
            [self setPhotoImageForScaleTicket:thePhoto];
        }
        // fill in the text fields
        if (self.inbound.on) {
            self.scaleticketWeight.text = [self.scaleTicket.inboundWeight stringValue];
        }
        else {
            self.scaleticketWeight.text = [self.scaleTicket.outboundWeight stringValue];
        }
        self.scaleTicketNumber.text = self.scaleTicket.ticketNumber;
        self.date.text = [self dateToString:self.scaleTicket.date];
        UnitType *theType = self.scaleTicket.unitType;
        NSInteger theWeightMeasure = [theType.unitTypeId integerValue];
        
        // TBD: need a constant here:
        if (theWeightMeasure==0) {
            self.weightUnitsSwitch.on=YES;
            self.weightUnitsLabel.text = @"kgs";
        }
        else {
            self.weightUnitsSwitch.on=NO;
            self.weightUnitsLabel.text = @"lbs";
        }
        ScaleTicketType *directionType = self.scaleTicket.scaleTicketType;
        if (directionType.scaleTicketTypeId == [NSNumber numberWithInt:SCALE_TICKET_INBOUND_RECORD_NUMBER]) {
            self.inbound.on = YES;
            self.directionLabel.text = SCALE_TICKET_INBOUND_NAME;
        }
        else {
            self.inbound.on = NO;
            self.directionLabel.text = SCALE_TICKET_OUTBOUND_NAME;
        }
    }
    
    if (self.readOnly==YES) {
        self.scaleTicketNumber.enabled=NO;
        self.scaleticketWeight.enabled=NO;
        self.weightUnitsSwitch.enabled=NO;
        self.inbound.enabled=NO;
        self.calendarButton.alpha=0;
        self.addMoreButton.alpha=0;
    }
    
    [self validate];
}

#pragma mark - IBActions

-(IBAction)calendarButtonPressed:(id)sender {
    calendarVisible = ! calendarVisible;
    
    if (calendarVisible==YES) {
        self.calendarMaskView.alpha = 1.0;
    }
    else {
        self.calendarMaskView.alpha = 0;
    }
}

// TBD: notify the delegate that another component is to be added to the view.
-(IBAction)addMoreButtonPressed:(id)sender {
    
    ScaleTicketComponentView *view = [[ScaleTicketComponentView alloc] init];
    
    NSNotification *notification = [NSNotification notificationWithName:@"Component Changed" object:view];
    
    if([[self delegate] respondsToSelector:@selector(didAddComponent:)]) {
        [[self delegate] didAddComponent:notification];
    }
}

- (IBAction)dateChanged:(UIDatePicker*)sender {
    self.scaleTicket.date = sender.date;
    
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
	self.date.text = [NSString stringWithFormat:@"%@",[df stringFromDate:sender.date]];
    self.calendarMaskView.alpha = 0;
    calendarVisible = NO;
    [self save];
}

-(IBAction)addPhotoButtonPressed:(id)sender {
    [self startCameraControllerFromViewController: self.parentPageViewController
                                    usingDelegate: self];
}

-(IBAction)deleteScaleTicketButtonPressed:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:@"Component Changed" object:self];
    
    if([[self delegate] respondsToSelector:@selector(didRemoveComponent:)]) {
        [[self delegate] didRemoveComponent:notification];
    }
}

-(IBAction)changeWeightUnits:(id)sender {
    int unitID = 0;
    if (self.weightUnitsSwitch.on) {
        self.weightUnitsLabel.text = @"kgs";
    }
    else {
        self.weightUnitsLabel.text = @"lbs";
        unitID = 1;
    }
    if (self.scaleTicket.unitType == nil) {
        self.scaleTicket.unitType = (UnitType *)[[DataEngine sharedInstance] newEntity:UNIT_TYPE_ENTITY_NAME];
    }
    self.scaleTicket.unitType.unitTypeId = [NSNumber numberWithInt:unitID];
    [self save];
}

-(IBAction)changeDirection:(id)sender {
    ScaleTicketType *desiredType;
    if (self.inbound.on) {
        desiredType = [self.dataEngine scaleTicketTypeForId:[NSNumber numberWithInt:SCALE_TICKET_INBOUND_RECORD_NUMBER]];
        self.scaleTicket.scaleTicketType = desiredType;
        [self save];
        self.directionLabel.text = SCALE_TICKET_INBOUND_NAME;
    }
    else {
        desiredType = [self.dataEngine scaleTicketTypeForId:[NSNumber numberWithInt:SCALE_TICKET_OUTBOUND_RECORD_NUMBER]];
        self.scaleTicket.scaleTicketType = desiredType;
        [self save];
        self.directionLabel.text = SCALE_TICKET_OUTBOUND_NAME;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.scaleTicketNumber) {
        self.scaleTicket.ticketNumber = textField.text;
    }
    else if (textField == self.scaleticketWeight) {
        if (self.inbound.on) {
            self.scaleTicket.inboundWeight = [NSDecimalNumber decimalNumberWithString:textField.text];
        }
        else {
            self.scaleTicket.outboundWeight = [NSDecimalNumber decimalNumberWithString:textField.text];
        }
    }
    [self save];
}

/*moved it to super class
-(void) save {
    [self.dataEngine saveContext];
}
 */


// TBD: Utiility
-(NSString *) dateToString:(NSDate *)theDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    return[formatter stringFromDate:theDate];
}

#pragma mark - ImagePickerDelegate support

- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;
    
    // Handle a still image capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }

        // Save the new image (original ) to the Aap's Document directory
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSMutableString *myString = [NSMutableString stringWithString:[self documentsDirectory]];
        myString = [[myString stringByAppendingPathComponent:[self nameForPhoto]] mutableCopy];
        
        BOOL savedOK = NO;
        savedOK = [fileMgr createFileAtPath:myString contents:UIImagePNGRepresentation(imageToSave) attributes:nil];
        
        if (savedOK==YES) {
            if (self.scaleTicket.photo==nil) {
                self.scaleTicket.photo = (Photo *) [self.dataEngine newEntity:@"Photo"];
            }
            self.scaleTicket.photo.fileName = [self nameForPhoto];
            [self save];
            [self setPhotoImage];
            self.addPhotoButton.hidden=YES;
        }
    }
    [self.parentPageViewController dismissViewControllerAnimated:NO  completion:nil];
}

-(NSString *)nameForPhoto {
    NSString *photoId = self.scaleTicket.photo.photoId;
    Transaction *trans = self.scaleTicket.transaction;
    NSString *transID = @"";
    if (trans != nil) {
        transID = trans.transactionId;
    }
    return [NSString stringWithFormat:@"%@_%@.png", transID, photoId];
}

-(NSString *)documentsDirectory {
    return[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)aPicker{
    NSLog(@"Picker was cancelled");
    
    [[aPicker parentViewController] dismissViewControllerAnimated:NO completion:nil];
}


- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = delegate;
    
    [controller presentViewController:cameraUI animated:NO completion:nil];
    
    return YES;
}

#pragma mark - TBD Utility

-(void) setPhotoImage {
    NSMutableString *myString = [NSMutableString stringWithString:[self documentsDirectory]];
    myString = [[myString stringByAppendingPathComponent:[self nameForPhoto]] mutableCopy];
    self.scaleTicketImage.image = [UIImage imageWithContentsOfFile:myString];
}

-(void) setPhotoImageForScaleTicket:(Photo *)thePhoto {
	
    self.scaleTicketImage.hidden=NO;
    self.scaleTicketImage.image = [Utils findSmallImage:thePhoto.fileName];
}


#pragma mark - Other
-(void) validate {
    BOOL scaleTicketNumberOK = self.scaleTicketNumber.text != nil && [self.scaleTicketNumber.text isEqualToString:@""]==NO;
    BOOL scaleticketWeightOK = self.scaleticketWeight.text != nil && [self.scaleticketWeight.text isEqualToString:@""]==NO;
    
    // TBD: add check for valid data range--or restrict the UIDatePicker
    BOOL dateOK = self.date.text != nil && [self.date.text isEqualToString:@""]==NO;
    BOOL imageOK = self.scaleTicketImage.image != nil;

    self.valid = scaleTicketNumberOK && scaleticketWeightOK && dateOK && imageOK;
    [self updateValidateImage];
}




@end
