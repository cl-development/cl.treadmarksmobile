//
//  TireCountComponentForConfirmationPageView.h
//
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComponentView.h"

@interface TireCountComponentForConfirmationPageView : ComponentView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) IBOutlet UIView  *view;

@end
