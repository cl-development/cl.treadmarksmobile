//
//  TireCountComponentForConfirmationPageView.m
//
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TireCountComponentForConfirmationPageView.h"
#import "Transaction.h"

#import "Constants.h"
#import "Transaction_TireType.h"
#import "TireCountConfirmationTableCell.h"
#import "TireType.h"
#import "DataEngine.h"

#define TOP_AREA_HEIGHT 10
#define HEADER_HEIGHT   50

@interface TireCountComponentForConfirmationPageView ()

	@property (nonatomic,strong) NSArray * transactionTireTypeArray;
	@property (weak, nonatomic) IBOutlet UITableView *tableView;
	
@end

@implementation TireCountComponentForConfirmationPageView
	
	
	
#pragma mark - UIView
	
- (id)initWithFrame:(CGRect)frame{
    //the height will be adjusted depending on the number of tires types
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, 0)];
    if (self) {
        [self connectNib];
    }
    self.valid = YES;
    return self;
}
	
#pragma mark - ComponentView
	
-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}
	
-(void)updateUI{
    self.transactionTireTypeArray = [[DataEngine sharedInstance] transactionTireTypeForTransactionId:self.transaction.transactionId];
    int rowHeight = [self.tableView rowHeight];
    self.frame = CGRectMake(self.frame.origin.x,
                            self.frame.origin.y,
                            self.frame.size.width,
                            TOP_AREA_HEIGHT+HEADER_HEIGHT+rowHeight*[self.transactionTireTypeArray count]+40);
self.validateImageView.frame = CGRectMake(677, 0, 91, self.frame.size.height);
}

	
#pragma mark - UITaleViewDataSource
	
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
	
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [self.transactionTireTypeArray count];
}
	
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    TireCountConfirmationTableCell *cell = (TireCountConfirmationTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[TireCountConfirmationTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Transaction_TireType *crtTransactionTireType = self.transactionTireTypeArray[indexPath.row];
    NSString *tireTypeNameKey = crtTransactionTireType.tireType.nameKey;
    
    cell.tireNameLabel.text = tireTypeNameKey;
    
    cell.tireCountLabel.text = [crtTransactionTireType.quantity stringValue];
    
    return cell;
}
	
	@end
