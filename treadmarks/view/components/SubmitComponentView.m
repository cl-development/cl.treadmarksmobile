//
//  SubmitComponentView.m
//  TreadMarks
//
//  Created by Dragos Ionel on 11/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "SubmitComponentView.h"
#import "Constants.h"
#import "Utils.h"
#import "DataEngine.h"
#import "MessageViewController.h"
#import "Transaction+Transaction.h"
#import "User.h"
#import "Registrant.h"
#import "Location.h"


#define SUBMIT_COMPONENT_HEIGHT 170

@interface SubmitComponentView ()
{CGPoint translation;
    BOOL reachedFinal;
}
@property (weak, nonatomic) IBOutlet UIButton *slidetocompleteButton;
@property (strong, nonatomic) IBOutlet UIImageView *swipeRightImageView;
@property (weak, nonatomic) IBOutlet UILabel *claimPeriodLabel;
@property CGPoint originalCenter;
@property (weak, nonatomic) IBOutlet UIView *slideView;
@property (weak, nonatomic) IBOutlet UIButton *compleButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UIButton *transactionCompleteButton;
@property CGPoint finalPostion;
- (IBAction)completetouchupinsideButton:(id)sender;
@end

@implementation SubmitComponentView
#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, SUBMIT_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
    }
    self.valid = YES;
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
    [self leftImageforButton];
    self.slideView.layer.borderWidth=1.0f;
    self.slideView.layer.borderColor=[[UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1] CGColor];
    self.transactionCompleteButton.layer.borderWidth=1.0f;
    self.transactionCompleteButton.layer.borderColor=[[UIColor colorWithRed:214/255.0 green:214/255.0 blue:214/255.0 alpha:1] CGColor];
   [self.slidetocompleteButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled] ;
    //[self.transactionCompleteButton setTitleColor:[UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1] forState:UIControlStateDisabled] ;
}

-(void)leftImageforButton
{self.slidetocompleteButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.slidetocompleteButton.imageView.frame.size.width-10, 0, -self.slidetocompleteButton.imageView.frame.size.width);
    self.slidetocompleteButton.imageEdgeInsets = UIEdgeInsetsMake(0,self.slidetocompleteButton.titleLabel.frame.size.width , 0, -self.slidetocompleteButton.titleLabel.frame.size.width -35);

    self.transactionCompleteButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.transactionCompleteButton.imageView.frame.size.width-90, 0, -self.transactionCompleteButton.imageView.frame.size.width);
    self.transactionCompleteButton.imageEdgeInsets = UIEdgeInsetsMake(0,self.transactionCompleteButton.titleLabel.frame.size.width , 0, -self.transactionCompleteButton.titleLabel.frame.size.width -5);



}


-(void)setEnabled:(BOOL)enabled{
    _enabled=enabled;
    if (self.transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_COMPLETE) {
        // enable slide   complete button
        self.transactionCompleteButton.alpha=1;
       self.slidetocompleteButton.alpha=0;
        //self.swipeRightImageView.userInteractionEnabled = NO;
       // self.swipeRightImageView.image = [UIImage imageNamed:BTN_SLIDE_DONE];
    } else {
        self.completeButton.enabled=NO;
        //self.swipeRightImageView.userInteractionEnabled = YES;
        
        if (enabled) {
            //self.swipeRightImageView.image = [UIImage imageNamed:BTN_SLIDE_ON];
            self.slideView.userInteractionEnabled=YES;
            self.slidetocompleteButton.enabled=YES;
        } else {
            //self.swipeRightImageView.image = [UIImage imageNamed:BTN_SLIDE_OFF];
            self.slideView.userInteractionEnabled=NO;
             self.slidetocompleteButton.enabled=NO;
        }
    }
}

-(void)updateUI{
    //update the claim period
    //filter transactions
    NSDate *today = self.transaction.createdDate;//transactionDate;
    NSDateComponents* dateComponents = [NSDateComponents new];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    //find the day of the month
    NSUInteger components = NSCalendarUnitDay | NSCalendarUnitMonth;
    dateComponents    = [calendar components:components fromDate:today];
    NSInteger day     = [dateComponents day];
    NSInteger month   = [dateComponents month];

    //find the month name
    NSDateFormatter *df = [NSDateFormatter new];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    
    //finding the last day of the month
    [dateComponents setMonth:1];
    [dateComponents setDay:-day];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    //find the last day of the month
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger lastDayOfTheMonth = [dateComponents day];

    self.claimPeriodLabel.text = [NSString stringWithFormat:@"%@ 1-%ld",monthName,(long)lastDayOfTheMonth];
    
}
//- (IBAction)swipeRightRecognized:(id)sender {
//   
//
//}

-(void)jumpToListScreen{
    UIViewController *targetViewController = nil;
    NSArray *viewControllers = [self.parentPageViewController.navigationController viewControllers];
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[ListViewController class]]) {
            targetViewController = vc;
            break;
        }
    }
    if (targetViewController != nil) {
        [self.parentPageViewController.navigationController popToViewController:targetViewController animated:YES];
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
   
    
    return NO ;//w;hen you reach the frame
}


-(IBAction)slidtocompleteGesture:(UIPanGestureRecognizer *)sender
{
    
    translation = [sender translationInView:self.view];
    ////NSLog(@"x =%f y=%f",translation.x, translation.y);
    // //NSLog(@" AT Startfinal x =%f final y=%f",self.finalPostion.x, self.finalPostion.y);
   
    if(translation.x >= 160)
        
    {
        [UIView animateWithDuration:0.5
                         animations:^
         {
             sender.view.center = CGPointMake(self.originalCenter.x+130, self.originalCenter.y);
         }];
        
        self.finalPostion = CGPointMake(self.originalCenter.x+130,self.originalCenter.y);
        self.completeButton.enabled=TRUE;
        reachedFinal=TRUE;
        return;
    }
    
    
    if ( translation.x<1 && reachedFinal) {
           
        reachedFinal=FALSE;
        self.completeButton.enabled=FALSE;
          // //NSLog(@"x =%f y=%f",translation.x, translation.y);

           [UIView animateWithDuration:0.5
                         animations:^
         {
             sender.view.center = CGPointMake(self.originalCenter.x, self.originalCenter.y);
         }];
       
    }
//
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        self.originalCenter=  sender.view.center;
        
    }
    if(sender.state == UIGestureRecognizerStateChanged)
    {
        CGFloat x = translation.x <0 ? 0 : translation.x;
        
        sender.view.center = CGPointMake(self.originalCenter.x + x, self.originalCenter.y);
    }
    
    if(sender.state == UIGestureRecognizerStateEnded)
    {
   
        if (translation.x>=130) {
            
          /*  if (translation.x>=140) {
            
                //NSLog(@" stop  the gesture");
            
            }*/
            [UIView animateWithDuration:0.5
                             animations:^
             {
                 sender.view.center = CGPointMake(self.originalCenter.x+130, self.originalCenter.y);
             }];
         
          self.finalPostion = CGPointMake(self.originalCenter.x+130,self.originalCenter.y);
          self.completeButton.enabled=TRUE;
          reachedFinal=TRUE;
            return;
      }
       
       else
        {
            self.completeButton.enabled=FALSE;
           [UIView animateWithDuration:0.5
                           animations:^
             {
                sender.view.center = CGPointMake(self.originalCenter.x, self.originalCenter.y);
            }];
       }
//        // [self addAnimation]; IF @ later stage we need to animate the text!
        
    }

}

- (IBAction)completetouchupinsideButton:(id)sender {
    
	[self.parentPageViewController validate];
	
	if (!self.parentPageViewController.valid)
	{
		[Utils showMessage:self.parentPageViewController header:@"Error" message:@"Page is not valid. Transaction cannot be completed" hideDelay:2];
	}
	else
	{
		self.transaction.transactionStatusType = [[DataEngine sharedInstance] transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_COMPLETE];
        User        *user=self.transaction.outgoingUser;
        
        
        if ([self.transaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_TCR]) {
            self.transaction.postalCode1 = [user.registrant.location.postalCode substringToIndex:3];
            self.transaction.postalCode2 = [user.registrant.location.postalCode substringFromIndex:3];
            
            NSLog(@"%@",self.transaction.postalCode1);
            NSLog(@"%@",self.transaction.postalCode2);
            [[DataEngine sharedInstance] saveContext];
        }
       else if ([self.transaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_DOT]) {
            self.transaction.postalCode1 = [user.registrant.location.postalCode substringToIndex:3];
            self.transaction.postalCode2 = [user.registrant.location.postalCode substringFromIndex:3];
            
            NSLog(@"%@",self.transaction.postalCode1);
            NSLog(@"%@",self.transaction.postalCode2);
            [[DataEngine sharedInstance] saveContext];
        }
        // Set  completed  Date for all transaction
       
            self.transaction.transactionDate = [NSDate date];
        
        
        [self.transaction clearSyncStatus];
		
		[self save];
		
        [Utils showMessage:self.parentPageViewController header:@"Thank You" message:@"This transaction is complete and will be transmitted to OTS the next time you sync."  okButtonText:@"OK" singleline:@"Your Transaction ID is" singlegreen:[self.transaction.friendlyId  stringValue] tobold:@"Please sync your device as soon as possible." hideDelay:0 hideCompletion:^
         {
             [self jumpToListScreen];
         }];


	}
}

@end
