//
//  TOCAuthorizationView.h
//  TreadMarks
//
//  Created by Capris Group on 2/12/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "DataEngine.h"
#import "Constants.h"
#import "Authorization.h"
#import "STCAuthorization.h"
#import "TOCAuthorizationPopOverViewController.h"

@class TOCAuthorizationPopOverViewController;

@interface TOCAuthorizationComponentView : TOCComponentView
-(void)setViewUI;

@end
