//
//  TOCAuthorizationView.m
//  TreadMarks
//
//  Created by Capris Group on 2/12/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCAuthorizationComponentView.h"
//#import "DockViewController.h"
#import "PageViewController.h"

#define AUTHORIZATION_COMPONENT_ANIMATION_DURATION 0.70

#define TOC_AUTHORIZATION_COMPONENT_HEIGHT_ON 150
#define TOC_AUTHORIZATION_COMPONENT_HEIGHT_OFF 80

#define EVENT_NUMBER_ON_FRAME  CGRectMake(200, 49, 319, 80);  // (200, 121, 330, 40);
#define EVENT_NUMBER_OFF_FRAME CGRectMake(200, 0, 319, 80);   // 200, 11, 330, 40
#define COMPONENT_VIEW_POSTION_IN_STC 2
#define COMPONENT_VIEW_POSTION_IN_HIT 3
#define COMPONENT_VIEW_POSTION_IN_UCR 3
#define COMPONENT_VIEW_POSTION_IN_DOT 3




@interface TOCAuthorizationComponentView ()

@property (strong, nonatomic) IBOutlet UILabel *eventNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *eventLabel;
@property (strong, nonatomic) IBOutlet UIButton *tapToEnterButton;
@property (strong, nonatomic) IBOutlet UIButton *toggleButton;
//@property (strong, nonatomic) IBOutlet UIImageView *validateImageView;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIImageView *tapImageView;
@property (strong, nonatomic) IBOutlet UILabel *authorizationLabel;
@property (nonatomic, strong) Transaction *owningTransaction;
@property (nonatomic, strong) Authorization *authorization;
@property (nonatomic, strong) NSString *eventNumberValue;

@end

@implementation TOCAuthorizationComponentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_AUTHORIZATION_COMPONENT_HEIGHT_OFF)];
    if (self) {
        // Initialization code
        [self connectNib];
    }
    
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_AUTHORIZATION_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_AUTHORIZATION_COMPONENT_HEIGHT_OFF;
    self.transaction = self.parentPageViewController.transaction;
    
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}
-(void)setViewUI
{
    self.status=OFF_SCREEN;
    self.toggleButton.selected = self.status;
    [UIView animateWithDuration:AUTHORIZATION_COMPONENT_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    
    
}

-(void)updateUI{
    
    self.owningTransaction = self.transaction;
    if ([self.owningTransaction.transactionType.shortNameKey isEqualToString:@"STC"]) {
        self.authorizationLabel.text=@"Event";
        self.eventLabel.text=@"Event Number";
    }
    self.authorization = [[DataEngine sharedInstance] authorizationForTransaction:self.owningTransaction];
    
    if (self.authorization==nil ||
		self.authorization.authorizationNumber == nil)
	{
        self.tapToEnterButton.hidden = NO;
        self.toggleButton.hidden = YES;
        self.editButton.hidden = YES;
        self.eventNumberLabel.text = @"";
        self.eventNumberValue = @"";
        
    }  else {
        
        self.tapToEnterButton.hidden = YES;
        self.tapImageView.hidden = YES;
        self.toggleButton.hidden = NO;
        self.editButton.hidden = YES;
        self.eventNumberLabel.frame = EVENT_NUMBER_OFF_FRAME;
        self.eventNumberLabel.text = self.authorization.authorizationNumber;
        self.eventNumberLabel.textColor = [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1];
        //NSLog(@"The authorization number is: %@", self.authorization.authorizationNumber);
        self.eventNumberValue = self.eventNumberLabel.text;
    }
    
    if (self.readOnly==YES) {
        self.tapToEnterButton.hidden=YES;
        self.toggleButton.hidden = YES;
        self.tapImageView.hidden = YES;
        self.editButton.hidden = YES;
        self.eventNumberLabel.frame = EVENT_NUMBER_OFF_FRAME;
        self.eventNumberLabel.textColor = [UIColor blackColor];
        self.eventNumberLabel.text = self.authorization.authorizationNumber;
        self.eventNumberValue = self.eventNumberLabel.text;
    }
	
    [self validate];
}
- (IBAction)tapToEnterAction:(UIButton *)sender {

    TOCAuthorizationPopOverViewController *viewController = [[TOCAuthorizationPopOverViewController alloc] initWithNibName:@"TOCAuthorizationPopOverViewController" bundle:nil];
    [viewController setAuthorizationDelegate:(id)self];
    viewController.currentTransaction = self.transaction;
    viewController.eventNumber = self.eventNumberLabel.text;
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	
    //UINavigationController *navigationController = self.parentPageViewController.dockViewController.navigationController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.parentPageViewController.navigationController presentViewController:viewController animated:YES completion:nil];
}

- (void) eventAuthorizationComponentUpdate:(NSString *)eventNumber {
    
    self.eventNumberLabel.textColor = [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1];
    self.eventNumberLabel.text = eventNumber;
    
    if (self.tapToEnterButton.hidden==YES) {
        self.status = 1-self.status;
        self.toggleButton.selected = self.status;
    }
    self.tapToEnterButton.hidden = YES;
    self.tapImageView.hidden = YES;
    self.toggleButton.hidden = NO;
    self.editButton.hidden = YES;
    self.eventNumberLabel.frame = EVENT_NUMBER_OFF_FRAME;
    [self validate];
    [self.parentPageViewController updateUI];

    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
}

- (IBAction)toggleAction:(UIButton *)sender {
    
    self.status = 1-self.status;
    self.editButton.hidden = NO;
    self.toggleButton.selected = self.status;
    [UIView animateWithDuration:AUTHORIZATION_COMPONENT_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    //[self updateUI];
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_STC) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];

    }
   else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT];
        
    }
   else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR) {
       [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];
       
   }else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT) {
       [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
       
   }
    
    [self.parentPageViewController updateUI];

}

- (void) updateEditButton {
	
	if (self.status == ON_SCREEN)
	{
		self.editButton.alpha = 1;
        self.eventNumberLabel.frame = EVENT_NUMBER_ON_FRAME;
        self.eventNumberLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1];
        
	}
	else
	{
		self.editButton.alpha = 0;
        self.eventNumberLabel.frame = EVENT_NUMBER_OFF_FRAME;
        self.eventNumberLabel.textColor = [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1];
	}
    [self.parentPageViewController updateUI];
    
}

-(void)validate{
    
    self.valid=YES;
    
    if (!self.tapToEnterButton.hidden) {
        self.valid=NO;
    }
    
    [self updateValidateImage];
    [self.parentPageViewController validate];
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}

@end
