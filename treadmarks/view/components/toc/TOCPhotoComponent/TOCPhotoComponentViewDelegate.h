//
//  TOCPhotoComponentViewDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/20/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AttachPhotoResultViewDelegate.h"

@class PhotoComponentTableViewCell, Photo, CLLocation;

@protocol TOCPhotoComponentViewDelegate <NSObject>

- (void)edit:(PhotoComponentTableViewCell*)cell;
- (void)popoverDone:(Photo *)photo
			comment:(NSString *)comment
			  image:(UIImage *)image
		 resultView:(id<AttachPhotoResultViewDelegate>)resultView
          didRetake:(BOOL)retookPhoto
		   location:(CLLocation *)location;
- (void)cancelAnimated:(BOOL)animated;
- (void)closeCamera;
@end
