//
//  TOCPhotoComponentViewConfirmation.h
//  TreadMarks
//
//  Created by Capris Group on 2014-03-25.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "TOCPhotoComponentViewDelegate.h"
#import "PhotoComponentTableViewCell.h"
#import "Constants.h"
#import "DataEngine.h"
#import "Utils.h"

@interface TOCPhotoComponentViewConfirmation : TOCComponentView <UITableViewDataSource, UITableViewDelegate>

- (CGFloat)heightOn;


@end
