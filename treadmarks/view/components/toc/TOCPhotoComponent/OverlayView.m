//
//  OverlayView.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/19/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "OverlayView.h"

@interface OverlayView ()

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;
@property (weak, nonatomic) IBOutlet UIView *acceptView;
@property (weak, nonatomic) IBOutlet UIView *retakeView;

#pragma mark - Properties

@property (strong) NSDictionary* info;

@end

@implementation OverlayView

#pragma mark - Initializers

- (id)initWithFrame:(CGRect)frame {
	
    self = [super initWithFrame:frame];
	
    if (self)
	{
		[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
														owner:self
													  options:NULL] objectAtIndex:0]];
    }
	
    return self;
}

#pragma mark - IBActions

- (IBAction)shutter:(UIButton *)sender {
	
	[self.viewController.cameraUI takePicture];
	[self.viewController.locationManager stopUpdatingLocation];
}
- (IBAction)cancel:(UIButton *)sender {
	
	[self imagePickerControllerDidCancel:self.viewController.cameraUI];
	[self.viewController.locationManager stopUpdatingLocation];
}
- (IBAction)retake:(UITapGestureRecognizer *)sender {
	
	self.imageView.image = NULL;
	self.imageView.backgroundColor = [UIColor clearColor];
	self.shutterButton.hidden = NO;
	self.acceptView.hidden = YES;
	self.retakeView.hidden = YES;
	[self.viewController.locationManager startUpdatingLocation];
}
- (IBAction)accept:(UITapGestureRecognizer *)sender {
	
	[self.viewController imagePickerController:self.viewController.cameraUI didFinishPickingMediaWithInfo:self.info];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	self.info = info;
	
	UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
	
	self.imageView.image = image;
	self.imageView.backgroundColor = [UIColor blackColor];
	self.shutterButton.hidden = YES;
	self.acceptView.hidden = NO;
	self.retakeView.hidden = NO;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
	[self.viewController imagePickerControllerDidCancel:picker];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
