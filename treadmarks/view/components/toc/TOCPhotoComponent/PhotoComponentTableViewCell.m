//
//  PhotoComponentTableViewCell.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2/3/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "PhotoComponentTableViewCell.h"
#import "Utils.h"
#import "Photo.h"
#import "AttachPhotoViewController.h"
#import "AlertViewController.h"

@interface PhotoComponentTableViewCell() <AttachPhotoResultViewDelegate>

#pragma mark - Properties

@property (weak) Photo* photo;

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
//@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonComment;



@end

@implementation PhotoComponentTableViewCell

#pragma mark - Initializers

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	if(self)
	{
		[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
														owner:self
													  options:NULL] objectAtIndex:0]];
	}
	
	return self;
}

#pragma mark - Override methods

#pragma mark - IBActions

- (IBAction)edit:(UIButton *)sender {
	
	[self.delegate edit:self];
}
- (IBAction)remove:(UIButton *)sender {

	[self.delegate presentAlert:self.parentPageViewController info:@{@"data": self.photo}];
}

#pragma mark - AttachPhotoResultViewDelegate

- (void)updatePhoto:(Photo *)photo {
	
    if (self.readOnly==YES) {
        self.editButton.hidden=YES;
        self.removeButton.hidden=YES;
    }
    
	self.photo = photo;
	UIImage* image = [Utils findSmallImage:photo.fileName];
	self.thumbnailView.image = image;
    self.CommentLabel.text= photo.comments;
    self.CommentLabel.numberOfLines=6;
    self.CommentLabel.frame=CGRectMake(320, 71, 225, 190);
    [self.CommentLabel sizeToFit];
    self.commentTextView.text = photo.comments;
    
    // self.commentLabel.text = photo.comments;
    //[self.commentLabel sizeToFit];
}

#pragma mark - Methods

@end
