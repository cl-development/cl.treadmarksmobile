//
//  TOCPhotoComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "PhotoComponentTableViewCell.h"
#import "AttachPhotoResultViewDelegate.h"
#import <CoreLocation/CoreLocation.h>

@class PhotoComponentTableViewCell;

@interface TOCPhotoComponentView : TOCComponentView

- (void)edit:(PhotoComponentTableViewCell*)cell;
-(void)setViewUI;


@end
