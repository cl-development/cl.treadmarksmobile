//
//  TOCPhotoComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCPhotoComponentView.h"
#import "DataEngine.h"
#import "Utils.h"
#import "UIImage+Resize.h"
#import "PhotoComponentTableViewCell.h"
#import "AttachPhotoResultViewDelegate.h"
#import "AttachPhotoViewController.h"
#import "AlertViewController.h"
#import "NSManagedObject+NSManagedObject.h"
#import "PhotoType+PhotoType.h"
//#import "DockViewController.h"
#import "Photo.h"
#import "ModalViewController.h"

#define TOC_PHOTO_COMPONENT_HEIGHT_OFF 80
#define COMPONENT_VIEW_POSTION_IN_TCR 5
#define COMPONENT_VIEW_POSTION_IN_STC 5
#define COMPONENT_VIEW_POSTION_IN_PTR 5
#define COMPONENT_VIEW_POSTION_IN_HIT 4
#define COMPONENT_VIEW_POSTION_IN_UCR 5
#define COMPONENT_VIEW_POSTION_IN_DOT 7
#define COMPONENT_VIEW_POSTION_IN_DOT_WITHOUT_VARIANCE 6


#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PTR 6

#define COMPONENT_VIEW_POSTION_IN_PIT 5
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PIT 6

@interface TOCPhotoComponentView () <UITableViewDataSource, CLLocationManagerDelegate, AttachPhotoResultViewDelegate,
UITableViewDelegate, TOCPhotoComponentViewDelegate, AlertViewDelegate>
{BOOL    reSizePic;}
#pragma mark - Properties

- (IBAction)tapToCaptureButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *tapToCaptureOutlet;
@property (strong) NSArray* photos;
@property (nonatomic, weak)Photo *currentPhoto;
@property (weak) AttachPhotoViewController* popoverContainerViewController;

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIView *tapToInsertView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *tocbtnPlusImage;

@end

@implementation TOCPhotoComponentView

#pragma mark - Initializers

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:CGRectMake(
                                           frame.origin.x,
                                           frame.origin.y,
                                           COMPONENT_WIDTH,
                                           TOC_PHOTO_COMPONENT_HEIGHT_OFF)];
    
    if(self)
    {
        [self connectNib];
        self.status = OFF_SCREEN;
        self.heightOff = TOC_PHOTO_COMPONENT_HEIGHT_OFF;
        self.valid = YES;
    }
    
    return self;
}

#pragma mark - Override methods

-(void)setViewUI
{
    self.status = OFF_SCREEN;
    [self updateUI];
    self.toggleButton.selected = self.status;
    
}

- (void)updateUI {
    
    [super updateUI];
    
    self.photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
    self.toggleButton.selected = self.status;
    
    if(self.photos == NULL || [self.photos count] == 0)
    {
        self.toggleButton.hidden = YES;
        //self.tapToInsertView.hidden = NO;
        self.tocbtnPlusImage.hidden=NO;
        self.tapToCaptureOutlet.hidden=NO;
        self.validateImageView.hidden = YES;
        self.status = OFF_SCREEN;
    }
    else
    {
        self.toggleButton.hidden = NO;
        //self.tapToInsertView.hidden = YES;
        self.tocbtnPlusImage.hidden=YES;
        self.tapToCaptureOutlet.hidden=YES;
        self.validateImageView.hidden = NO;
        
        CGRect frame = self.containerView.frame;
        frame.size.height = 77 // title minus offset
        + self.tableView.rowHeight * [self.photos count] //tableview
        + 1 // separator
        + 80 // add photo
        + 5; // component separator
        
        self.containerView.frame = frame;
        
        [self.tableView reloadData];
    }
}
- (CGFloat)heightOn {
    
    return self.containerView.frame.size.height;
}

-(void)validate{
    self.valid = YES;
    //[self.parentPageViewController validate];
    if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PTR) {
        
    }
}


#pragma mark - Methods

- (void)connectNib {
    
    [self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                    owner:self
                                                  options:NULL] objectAtIndex:0]];
}
- (void)showPopover:(id<AttachPhotoResultViewDelegate>)resultView {
    
    AttachPhotoViewController* popover = [[AttachPhotoViewController alloc] init];
    
    popover.resultView = resultView;
    popover.photo = [resultView photo];
    //NSLog(@"photo object is: %@", popover.photo);
    popover.containerViewController = self.parentPageViewController;
    popover.delegate = self;
    //	UIImage *image = [UIImage imageWithContentsOfFile:popover.photo.fileName];
    //    //NSLog(@"This image file is what? %@", image.description);//image is null
    self.popoverContainerViewController = popover;
    
    if(self.parentPageViewController == NULL)
    {
        @throw [NSException exceptionWithName:NSObjectNotAvailableException
                                       reason:@"parentPageViewController is not set."
                                     userInfo:NULL];
    }
    
    popover.modalPresentationStyle= UIModalPresentationOverCurrentContext;
    [[ModalViewController sharedModalViewController] presentViewController:self.popoverContainerViewController
                                                                  animated:YES
                                                                completion:nil];
}
- (void)closeCamera
{
    [[ModalViewController sharedModalViewController] dismissViewControllerAnimated:NO completion:nil];
}



- (void)popoverDone:(Photo *)photo
            comment:(NSString *)comment
              image:(UIImage *)image
         resultView:(id<AttachPhotoResultViewDelegate>)resultView
          didRetake:(BOOL)retookPhoto
           location:(CLLocation *)location {
    
    // If photo record does not exist, create one
    BOOL created = NO;
    //dont resize if pic already taken
    reSizePic =retookPhoto;
    //NSManagedObjectContext *context = [DataEngine sharedInstance].managedObjectContext;
    /// If comment record does not exist, create one
    if(photo == NULL)
    {
        photo = (Photo *)[[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
        photo.photoId = [[NSUUID UUID] UUIDString];
        photo.transaction = self.transaction;
        photo.createdDate = [NSDate date];
        created = YES;
        photo.fileName = [Utils nameForPhoto];
        photo.photoType = (PhotoType*)[PhotoType singleById:PHOTO_TYPE_ID_PHOTO];
    }
    //NSLog(@"%@", photo.fileName);
    photo.date = (created==YES) ? photo.createdDate : [NSDate date];
    photo.syncDate = nil;
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    if (created==YES) {
        photo.imageSyncDate = nil;
    }
    else if (retookPhoto==YES) {
        photo.imageSyncDate = nil;
    }
    photo.comments = comment;
    
    if (location != nil)
    {
        photo.latitude = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
        photo.longitude = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    }
    
    //NSLog(@"The latitutde is %@, and the logintude is: %@", photo.latitude, photo.longitude);
    
    self.currentPhoto = photo;
    
    /// In the future, save file only if the image is changed.
    // Dont ReSize the Image if it exist OTSM-1278
    [self saveCurrentImage:image fileName:photo.fileName];
    photo.imageSyncDate = nil;
    
    [self save];
    
    [resultView updatePhoto:photo];
    
    [self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];
}
- (void)saveCurrentImage:(UIImage*)image fileName:(NSString*)fileName {
    UIImage *smallImageToSave= [self downSizeImage:image];
    
    
    // Save the new image to the App's Document directory
    NSString *pathForPhoto = [Utils pathForPhotoWithPhotoName:fileName];
    NSString *smallPathForPhoto = [pathForPhoto stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
    
    // Save a small version of it
    [self asynchSaveImage:smallImageToSave atPath:smallPathForPhoto andWait:YES];
    
    // Save the large version in the background
    //[self asynchSaveImage:[self correntImageOrientation:image] atPath:pathForPhoto andWait:NO];
}
- (UIImage*)downSizeImage:(UIImage*)theImage {
    
    CGSize newSize;
    if (reSizePic==YES)
    {
        newSize= CGSizeMake(theImage.size.width/4, theImage.size.height/4);}
    else
    {newSize= CGSizeMake(theImage.size.width, theImage.size.height);}
    UIImage *resized = [theImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit
                                                      bounds:newSize
                                        interpolationQuality:kCGInterpolationHigh];
    
    return resized;
}
- (NSMutableString*)pathForPhotoWithPhotoName:(NSString *)name {
    
    NSMutableString *myString = [NSMutableString stringWithString:[Utils documentsDirectory]];
    myString = [[myString stringByAppendingPathComponent:name] mutableCopy];
    
    return myString;
}
- (void)asynchSaveImage:(UIImage*)imageToSave atPath:(NSString*)path andWait:(BOOL)wait {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    __block BOOL savedOK = NO;
    
    CFDataRef myRef = CFBridgingRetain(UIImagePNGRepresentation(imageToSave));
    dispatch_data_t data = dispatch_data_create(CFDataGetBytePtr(myRef),CFDataGetLength(myRef),
                                                queue, ^{CFRelease(myRef);});
    
    dispatch_semaphore_t sem = nil;
    if (wait==YES) {
        sem = dispatch_semaphore_create(0);
    }
    
    const char *savePath = [path UTF8String];
    dispatch_fd_t fd = open(savePath, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
    printf("FD: %d\n", fd);
    if (fd==-1) {
        //NSLog(@"Couldn't create file for photo saving.");
        return;
    }
    
    dispatch_write(fd, data, queue,^(dispatch_data_t d, int e) {
        printf("Written %zu bytes!\n", dispatch_data_get_size(data) - (d ? dispatch_data_get_size(d) : 0));
        printf("\tError: %d\n", e);
        if (wait==YES) {
            dispatch_semaphore_signal(sem);
        }
        if (e==0) {
            savedOK = YES;
        }
        close(fd);
    });
    if (wait==YES) {
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    }
}
- (void)cancelAnimated:(BOOL)animated {
    
    [[ModalViewController sharedModalViewController] dismissViewControllerAnimated:animated completion:nil];
}
- (void)edit:(id<AttachPhotoResultViewDelegate>)cell {
    
    [self showPopover:cell];
}
- (UIImage*)correntImageOrientation:(UIImage *)image {
    
    UIImage *sameImage = [image resizedImage:[image size]
                        interpolationQuality:kCGInterpolationNone];
    
    return sameImage;
}

#pragma mark - AttachPhotoResultViewDelegate

- (void)updatePhoto:(Photo*)photo {
    
    [self validate];
    [self updateUI];
    [self.parentPageViewController updateUI];
}

#pragma mark - IBActions

- (IBAction)tapToInsert:(UITapGestureRecognizer *)sender {
    
    [self showPopover:self];
}
- (IBAction)toggleButtonPressed:(UIButton *)sender {
    
    self.status = 1 - self.status;
    self.toggleButton.selected = self.status;
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_TCR) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_STC)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PTR];
        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PTR];
        }
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT];
        
    } else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];

        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT_WITHOUT_VARIANCE];
        }
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
    {
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PIT];
        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PIT];
        }
    }
    
    
    [self.parentPageViewController updateUI];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.photos count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* reusableCellIdentifier = @"PhotoComponentTableViewCell";
    PhotoComponentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
    
    if(cell == NULL)
    {
        [tableView registerClass:NSClassFromString(reusableCellIdentifier)
          forCellReuseIdentifier:reusableCellIdentifier];
        
        cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
    }
    
    cell.parentPageViewController = self.parentPageViewController;
    cell.delegate = self;
    
    [cell updatePhoto:self.photos[indexPath.row]];
    
    return cell;
}

#pragma mark - AttachPhotoViewControllerDelegate

- (Photo *)photo {
    
    return NULL;
}

#pragma mark - AlertViewDelegate

- (void)alertView:(AlertViewController *)alertView
    primaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    Photo* photo = info[@"data"];
    
    /// Delete the files
    
    NSError* error;
    //NSLog(@"the file path is: %@", [Utils pathForPhotoWithPhotoName:photo.fileName]);
    //NSLog(@"the small file path is: %@", [photo.fileName stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"]);
    [[NSFileManager defaultManager] removeItemAtPath:[Utils pathForPhotoWithPhotoName:photo.fileName] error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:[[Utils pathForPhotoWithPhotoName:photo.fileName] stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"] error:&error];
    
    if (photo.syncDate==nil && ![photo.fileName isEqualToString:@"DELETE"]) {
        /// Delete the record
        [[DataEngine sharedInstance].managedObjectContext deleteObject:photo];
    } else {
        [[DataEngine sharedInstance] deletePhoto:photo];
        self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    }
    
    [self save];
    
    [self updateUI];
    [self.parentPageViewController updateUI];
    [self.parentPageViewController.navigationController dismissViewControllerAnimated:YES completion:NULL];
}
- (void)presentAlert:(NSDictionary *)info {
    
    [self presentAlert:NULL info:info];
}
- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    AlertViewController* alert = [[AlertViewController alloc] init];
    
    alert.title = @"Warning";
    alert.heading = @"Are you sure you want to remove this photo?";
    alert.message = @"You will lose the photo and comments that were recorded for this transaction.";
    alert.primaryButtonText = @"Remove";
    alert.primaryInfo = info;
    alert.secondaryButtonText = @"Cancel";
    alert.delegate = self;
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [viewController presentViewController:alert animated:YES completion:NULL];
    

}

- (IBAction)tapToCaptureButton:(id)sender {
    
    [self showPopover:self];
}
@end
