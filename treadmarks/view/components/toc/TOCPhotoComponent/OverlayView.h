//
//  OverlayView.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/19/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttachPhotoViewController.h"
#import "ImagePickerContainerController.h"

@interface OverlayView : UIView <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak) AttachPhotoViewController* viewController;

@end
