//
//  TOCPhotoComponentViewConfirmation.m
//  TreadMarks
//
//  Created by Capris Group on 2014-03-25.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCPhotoComponentViewConfirmation.h"
#import "NSManagedObject+NSManagedObject.h"
#import "PhotoType+PhotoType.h"

#define CONFIRMATION_PHOTO_TICKET_COMPONENT_HEIGHT_ON 941

@interface TOCPhotoComponentViewConfirmation ()
//@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) PhotoType *photoPhotoType;
@end

@implementation TOCPhotoComponentViewConfirmation

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(
										   frame.origin.x,
										   frame.origin.y,
										   COMPONENT_WIDTH,
										   0)];
    if (self) {
        // Initialization code
        self.valid = YES;
        self.photoPhotoType = (PhotoType*)[PhotoType singleById:PHOTO_TYPE_ID_PHOTO];
        [self connectNib];
    }
    return self;
}

- (CGFloat)heightOn {
    [self updateUI];
//    //NSLog(@"the height of this table is %d", CONFIRMATION_PHOTO_TICKET_COMPONENT_HEIGHT_ON * [self.photos count]);
    //NSLog(@"what is the heightof the table? %f", self.tableView.rowHeight);
//    //NSLog(@"what are the number of hotos? %d", [self.photos count]);
	return self.tableView.rowHeight * [self.photos count] + 164;
	//return CONFIRMATION_PHOTO_TICKET_COMPONENT_HEIGHT_ON; //self.frame.size.height;
}

- (void)connectNib {
	
	[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													owner:self
												  options:NULL] objectAtIndex:0]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Override methods

- (void)updateUI {
	
	[super updateUI];
	
    self.photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
	
	if (self.photos == NULL || [self.photos count] == 0)
	{
        /*self.validateImageView.hidden = YES;
         self.status = OFF_SCREEN;*/
        NSError *error = [Utils errorForType:@"Photo Component updateUI" errorCode:0];
        NSLog(@"%@", error.localizedDescription);
	}
	else
	{
		self.validateImageView.hidden = NO;
        
        CGRect frame = self.frame;
		frame.size.height = 77 // title minus offset
        + self.tableView.rowHeight * [self.photos count] //tableview
        + 1 // separator
        + 80 // add photo
        + 5; // component separator
		
		self.frame = frame;
        //NSLog(@"The number of pics are: %lu", (unsigned long)[self.photos count]);
        CGRect oldFrame = self.containerView.frame;
        self.containerView.frame = CGRectMake(oldFrame.origin.x,
                                              oldFrame.origin.y,
                                              oldFrame.size.width,
                                              frame.size.height);
        
        
		[self.tableView reloadData];
	}
    self.validateImageView.frame = CGRectMake(683, 1, 91, self.frame.size.height);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return [self.photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString* reusableCellIdentifier = @"PhotoComponentTableViewCell";
	PhotoComponentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	
	if(cell == NULL)
	{
		[tableView registerClass:NSClassFromString(reusableCellIdentifier)
		  forCellReuseIdentifier:reusableCellIdentifier];
        
		cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	}
	
	cell.parentPageViewController = (UIViewController *)self.parentPageViewController;
	//cell.delegate = self;
	
	cell.readOnly = YES;
    [cell updatePhoto:self.photos[indexPath.row]];
	cell.CommentLabel.hidden = YES;
    cell.commentTextView.hidden = NO;
    cell.editing = NO;
    
	return cell;
}

@end
