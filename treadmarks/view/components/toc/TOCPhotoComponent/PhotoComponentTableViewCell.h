//
//  PhotoComponentTableViewCell.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2/3/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//
#import "AlertViewDelegate.h"
#import "TOCPhotoComponentViewDelegate.h"
#import "Photo.h"

@class TOCPhotoComponentView;

@interface PhotoComponentTableViewCell : UITableViewCell

@property (weak) id<TOCPhotoComponentViewDelegate, AlertViewDelegate> delegate;
@property (weak) UIViewController* parentPageViewController;
@property (nonatomic, assign) BOOL readOnly;
@property (weak, nonatomic) IBOutlet UILabel *CommentLabel;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

- (void)updatePhoto:(Photo *)photo;

@end

/*
#import <UIKit/UIKit.h>
#import "Photo.h"
#import "AlertViewController.h"

@interface PhotoComponentTableViewCell : UITableViewCell
@property(nonatomic, weak) IBOutlet UIImageView *imageView;
@property(nonatomic, weak) IBOutlet UITextView *textView;
@property(nonatomic, strong) Photo *photo;
@property(nonatomic, weak) UITableView *tableView;

-(void)updateUI;
@end
*/
