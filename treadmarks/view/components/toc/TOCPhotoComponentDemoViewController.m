//
//  TOCPhotoComponentDemoViewController.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2/6/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCPhotoComponentDemoViewController.h"



#import "AppDelegate.h"

#import "Utils.h"
#import "Constants.h"

#define TOC_PHOTO_COMPONENT_HEIGHT_OFF 80
#define TOC_PHOTO_COMPONENT_HEIGHT_ON 80


@interface TOCPhotoComponentDemoViewController ()


@end

@implementation TOCPhotoComponentDemoViewController

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_PHOTO_COMPONENT_HEIGHT_OFF)];
    if (self) {
      [self connectNib];
      self.status=OFF_SCREEN;
       self.heightOn  = TOC_PHOTO_COMPONENT_HEIGHT_ON;
        self.heightOff = TOC_PHOTO_COMPONENT_HEIGHT_OFF;
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}
-(void)updateUI{
        [self validate];
}

-(void)validate{
    self.valid=YES;
     [self.parentPageViewController validate];
}

@end

