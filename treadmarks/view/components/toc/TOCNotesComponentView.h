//
//  TOCNotesComponentView.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/29/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"

@interface TOCNotesComponentView : TOCComponentView

@property (nonatomic, assign) NSUInteger type;
@property (nonatomic, assign) BOOL shouldShowPopover;

- (void) popoverDone: (NSString*) commentText;
- (void) cancel;
-(void)setViewUI;

@end
