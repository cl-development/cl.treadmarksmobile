//
//  TOCComponentView.m
//  TreadMarks
//
//  Created by Dragos Ionel on 12/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "Constants.h"

@interface TOCComponentView ()


@end

@implementation TOCComponentView

-(CGFloat)height {
    ////NSLog(@"Base class height: %f",(self.status==ON_SCREEN)?self.heightOn:self.heightOff);
    return (self.status==ON_SCREEN)?self.heightOn:self.heightOff;
    
}

@end
