//
//  TOCPhotoComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCScaleTicketComponentViewConfirmation.h"
#import "DataEngine.h"
#import "Utils.h"
#import "ScaleTicketComponentTableViewCell.h"
#import "TireType.h"
#import "ScaleTicket.h"
#import "ScaleTicketType.h"
#import "TreadMarks-Swift.h"

#define TOC_SCALE_TICKET_COMPONENT_HEIGHT_ON 943
#define TOC_SCALE_TICKET_COMPONENT_HEIGHT_ON_BOTH 554
/*
 frame.size.height = 77 // title minus offset
 + self.tableView.rowHeight (389) * 2 = 778
 + 1 // separator
 + 80 // add photo
 + 5; // component separator
*/

@interface TOCScaleTicketComponentViewConfirmation ()


@property (strong) NSArray* scaleTickets;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UILabel *outboundLabel;
@property (weak, nonatomic) IBOutlet UIView *weightVarianceView;
@property (weak, nonatomic) IBOutlet UILabel *scaleWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightVarianceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBottomLine;

-(BOOL)weightVarianceOkay;
@property (strong)NSDecimalNumber *netWeightDecimal;

@end

@implementation TOCScaleTicketComponentViewConfirmation

#pragma mark - Initializers

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:CGRectMake(
										   frame.origin.x,
										   frame.origin.y,
										   COMPONENT_WIDTH,
										   0)];
	if (self)
	{
		[self connectNib];
		//self.status = ON_SCREEN;
		//self.heightOff = TOC_SCALE_TICKET_COMPONENT_HEIGHT_OFF;
		self.valid = YES;
	}
	
	return self;
}

#pragma mark - Override methods

- (void)updateUI {
	
	[super updateUI];
	
    self.scaleTickets = [[DataEngine sharedInstance] scaleTicketsForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_SCALE_TICKET];
	
	if (self.scaleTickets == NULL || [self.scaleTickets count] == 0)
	{
        NSError *error = [Utils errorForType:@"ScaleTicket Confirmation Component updateUI" errorCode:0];
        NSLog(@"%@", error.localizedDescription);
	}
	else
	{
		self.validateImageView.hidden = NO;

        
        CGRect frame = self.frame;
		if ([self.scaleTickets count] == 1)
        { frame.size.height = 77 // title minus offset
        
        
			+ 389//self.tableView.rowHeight * [self.scaleTickets count] //tableview
			+ 1 // separator
			//+ 80 // add photo
			+ 5; // component separator
            self.frame = frame;
            self.imageViewBottomLine.frame = CGRectMake(0, 300, 768, 2);
            
        
        }
        else
        {frame.size.height = 77 // title minus offset
            
            
            + 778//self.tableView.rowHeight * [self.scaleTickets count] //tableview
            + 1 // separator
            //+ 80 // add photo
            + 5; // component separator
            self.frame = frame;
        }
        //NSLog(@"Scale ticket count %lu",(unsigned long)[self.scaleTickets count]);
        CGRect oldFrame = self.containerView.frame;
        self.containerView.frame = CGRectMake(oldFrame.origin.x,
                                                           oldFrame.origin.y,
                                                           oldFrame.size.width,
                      
                                              frame.size.height);
        if ([self.scaleTickets count] == 1) {
            self.imageViewBottomLine.frame = CGRectMake(0, 445, 768, 2);
 
        }
        else
        {
            self.imageViewBottomLine.frame = CGRectMake(0, 842, 768, 2);
 
        }

        [self.tableView reloadData];
	}
    self.validateImageView.frame = CGRectMake(683, 1, 91, self.frame.size.height);

    [self updateWeightVarianceLabel];
}

- (CGFloat)heightOn {
	
	return TOC_SCALE_TICKET_COMPONENT_HEIGHT_ON; //self.frame.size.height;
}

#pragma mark - Methods

- (void)connectNib {
	
	[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													owner:self
												  options:NULL] objectAtIndex:0]];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
  //  NSLog(@" Total Sclae tickets  %lu",(unsigned long)[self.scaleTickets count]);
	return [self.scaleTickets count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString* reusableCellIdentifier = @"ScaleTicketComponentTableViewCell";
	ScaleTicketComponentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	
	if(cell == NULL)
	{
		[tableView registerClass:NSClassFromString(reusableCellIdentifier)
		  forCellReuseIdentifier:reusableCellIdentifier];
        
		cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	}
	
	cell.parentPageViewController = (UIViewController *)self.parentPageViewController;
	//cell.delegate = self;
	
	cell.readOnly = YES;
    [cell updateScaleTicket:self.scaleTickets[indexPath.row]];
	
	return cell;
}

-(void) updateWeightVarianceLabel {
    float netWeight = [self netWeight];
     double netWeightDecimal=[self.netWeightDecimal doubleValue];
    NSString *zeroValue=[NSString stringWithFormat:@"%.04f",netWeightDecimal];
    // if the netWeight in unavailable either supply a message or hide the variance view
    if (netWeight==INVALID_WEIGHT_VALUE || [zeroValue isEqualToString:@"0.0000"]) {
        self.weightVarianceView.hidden=YES;
    }
  ScaleTicket *firstTicket = [self.scaleTickets objectAtIndex:0];
     UnitType *theType = firstTicket.unitType;
     NSString *weightTypeString = nil;
     if (theType != Nil) {
     // TBD: always KG for net weight display? recall inbound and outbound could be different units
     weightTypeString = (theType.unitTypeId == UNIT_TYPE_ID_KILOGRAM) ? @"KG" : @"LB";
     }
  //  NSString *weightTypeString = @"KG";
   
   self.scaleWeightLabel.text = [NSString stringWithFormat:@"%.04f %@", netWeightDecimal, weightTypeString];
    // self.scaleWeightLabel.text = [NSString stringWithFormat:@"%@  %@", self.netWeightDecimal , weightTypeString];
    NSString *displayString = @"";  //@"Variance not available";
    float weightVariance = [self weightVariance];
    if (weightVariance != INVALID_WEIGHT_VALUE) {
        float varianceAsWholePCT = weightVariance * 100;
        displayString = [NSString stringWithFormat:@"Variance: %.02f%@", varianceAsWholePCT,@"%"];
    }
    self.weightVarianceLabel.text = displayString;
    if ([self weightVarianceOkay:weightVariance]==NO) {
        self.weightVarianceLabel.textColor = [UIColor redColor];
    }
    else {
        self.weightVarianceLabel.textColor = [UIColor greenColor];
    }
    /*if([displayString isEqualToString:@"Variance: 15.0000%"])
    {
    self.weightVarianceLabel.textColor = [UIColor greenColor];
    }*/
}

-(BOOL) weightVarianceOkay {
    float variance = [self weightVariance];
    return [self weightVarianceOkay:variance];
}

-(BOOL) weightVarianceOkay:(float)variance {
    float varianceAsWholePCT = variance * 100;
    if (variance==INVALID_WEIGHT_VALUE) {
        return YES;
    }
    return !(fabsf(varianceAsWholePCT) >= 15);
    //return !(varianceAsWholePCT > 15);
}

-(float) weightVariance {
    float estimatedWeight = [self estimatedWeight];
    float actualWeight = [self netWeight];
    if (estimatedWeight==INVALID_WEIGHT_VALUE || actualWeight==INVALID_WEIGHT_VALUE) {
        return INVALID_WEIGHT_VALUE;
    }
    float variancePCT = (actualWeight-estimatedWeight)/estimatedWeight;
    return variancePCT;
}

// values always in KGs:
-(float) estimatedWeight {
    float estimatedWeight = INVALID_WEIGHT_VALUE;
    NSArray *transactionTireTypeArray = [[DataEngine sharedInstance] transactionTireTypeForTransactionId:self.transaction.transactionId];
    
    for (Transaction_TireType *type in transactionTireTypeArray) {
        if (type.quantity != nil && [type.quantity integerValue]>=0) {
            estimatedWeight += [type.quantity integerValue]* [type.tireType.estimatedWeight integerValue];
        }
    }
        
    return estimatedWeight;
}


#define LBS_TO_KILOGRAMS 0.45359237
// net weight - always in kilograms for comparison

    -(float) netWeight {
        float netWeight = INVALID_WEIGHT_VALUE;
        float grossWeight = -1.0;
        float tareWeight = -1.0;
        BOOL ticketTypeIsBoth = NO;
        BOOL hasInbound = NO;
        BOOL hasOutbound = NO;
        NSDecimalNumber *inboundDecimal;
        NSDecimalNumber *outboundDecimal;
        NSDecimalNumber *conversion = [NSDecimalNumber decimalNumberWithString:@"0.453592"];
        for (ScaleTicket *theTicket in self.scaleTickets) {
            UnitType *theType = theTicket.unitType;
            if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
                tareWeight = [theTicket.outboundWeight    floatValue];
                outboundDecimal=theTicket.outboundWeight;
                if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                    tareWeight *= LBS_TO_KILOGRAMS;
                    outboundDecimal=[outboundDecimal decimalNumberByMultiplyingBy:conversion];
                }
                hasOutbound = YES;
            }
            else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
                grossWeight = [theTicket.inboundWeight floatValue];
                inboundDecimal=theTicket.inboundWeight;
                if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                    grossWeight *= LBS_TO_KILOGRAMS;
                    inboundDecimal=[inboundDecimal decimalNumberByMultiplyingBy:conversion];
                }
                hasInbound = YES;
            }
            else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
                
                netWeight = [theTicket.inboundWeight floatValue] - [theTicket.outboundWeight floatValue] ;
                inboundDecimal=theTicket.inboundWeight;
                outboundDecimal=theTicket.outboundWeight;
                self.netWeightDecimal=[inboundDecimal decimalNumberBySubtracting:outboundDecimal];
                if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                    netWeight *= LBS_TO_KILOGRAMS;
                    self.netWeightDecimal=[ self.netWeightDecimal decimalNumberByMultiplyingBy:conversion];
                }
                ticketTypeIsBoth = YES;
            }
        }
        if (ticketTypeIsBoth==NO && hasOutbound && hasInbound) {
            netWeight = grossWeight-tareWeight;
            
            self.netWeightDecimal=[inboundDecimal decimalNumberBySubtracting:outboundDecimal];
            
            
        }
        return netWeight;
    }




@end

