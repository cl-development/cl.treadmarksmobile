//
//  PhotoComponentTableViewCell.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2/3/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ScaleTicketComponentTableViewCell.h"
#import "Utils.h"
#import "ScaleTicket.h"
#import "ScaleTicketType.h"
#import "UnitType.h"
#import "AttachPhotoViewController.h"
#import "AlertViewController.h"

@interface ScaleTicketComponentTableViewCell() 

@property (nonatomic, weak) IBOutlet UIImageView *thumbnailView;
@property (nonatomic, weak) IBOutlet UILabel *ticketTypeLabel;

@property (nonatomic, weak) IBOutlet UIView *normalView;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *scaleTicketNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *weightLabel;
@property (nonatomic, weak) IBOutlet UILabel *weightGrossOrTareLabel;
@property (nonatomic, weak) IBOutlet UILabel *weightTypeLabel;

@property (nonatomic, weak) IBOutlet UIView *bothView;
@property (nonatomic, weak) IBOutlet UILabel *bothDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *bothScaleTicketNumberLabel;
@property (nonatomic, weak) IBOutlet UILabel *bothGrossWeightLabel;
@property (nonatomic, weak) IBOutlet UILabel *bothTareWeightLabel;
@property (nonatomic, weak) IBOutlet UILabel *bothWeightTypeLabel;


@property (nonatomic, weak) IBOutlet UIButton *editButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

@end

@implementation ScaleTicketComponentTableViewCell

#pragma mark - Initializers

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	if(self)
	{
		[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
														owner:self
													  options:NULL] objectAtIndex:0]];
	}
	
	return self;
}

#pragma mark - Override methods

#pragma mark - IBActions

- (IBAction)edit:(UIButton *)sender {
	//edit  the scale tickec on cancel do not delete
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ScaleTicketCreated"];
    [[NSUserDefaults standardUserDefaults] synchronize];
	[self.delegate edit:self];
}
- (IBAction)remove:(UIButton *)sender {
	
	[self.delegate presentAlert:self.parentPageViewController.navigationController
						   info:@{@"data": self.scaleTicket}];
}

- (void)updateScaleTicket:(ScaleTicket *)ticket {
	
	self.scaleTicket = ticket;
    
    if (self.readOnly==YES) {
        self.editButton.hidden=YES;
        self.deleteButton.hidden=YES;
    }
	
    if ([self.scaleTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
        self.normalView.hidden = NO;
        self.bothView.hidden = YES;
        self.ticketTypeLabel.text = SCALE_TICKET_INBOUND_NAME;
        self.weightGrossOrTareLabel.text = @"GROSS";
        self.weightLabel.text = [self.scaleTicket.inboundWeight stringValue];
    }
    else if ([self.scaleTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND])
    {
        self.normalView.hidden = NO;
        self.bothView.hidden = YES;
        self.ticketTypeLabel.text = SCALE_TICKET_OUTBOUND_NAME;
        self.weightLabel.text = [self.scaleTicket.outboundWeight stringValue];
        self.weightGrossOrTareLabel.text = @"TARE";
    }
    else if ([self.scaleTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
         self.ticketTypeLabel.text = SCALE_TICKET_BOTH_NAME;
        self.normalView.hidden = YES;
        self.bothView.hidden = NO;
        self.bothGrossWeightLabel.text = [self.scaleTicket.inboundWeight stringValue];
        self.bothTareWeightLabel.text = [self.scaleTicket.outboundWeight stringValue];
    }

    if ([self.scaleTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
        self.bothDateLabel.text = [Utils dateToString:self.scaleTicket.date];
        self.bothScaleTicketNumberLabel.text = self.scaleTicket.ticketNumber;
    }
    else {
        self.dateLabel.text = [Utils dateToString:self.scaleTicket.date];
        self.scaleTicketNumberLabel.text = self.scaleTicket.ticketNumber;
    }

    Photo *thePhoto = self.scaleTicket.photo;
    if (thePhoto != nil) {
        UIImage* image = [Utils findSmallImage:thePhoto.fileName];
        self.thumbnailView.image = image;
    }
    UnitType *theType = self.scaleTicket.unitType;
    if (theType != Nil) {
        if ([self.scaleTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
            self.bothWeightTypeLabel.text = (theType.unitTypeId == UNIT_TYPE_ID_KILOGRAM) ? @"KG" : @"LB";
        }
        else {
            self.weightTypeLabel.text = (theType.unitTypeId == UNIT_TYPE_ID_KILOGRAM) ? @"KG" : @"LB";
        }
    }
}


#pragma mark - Methods

@end

