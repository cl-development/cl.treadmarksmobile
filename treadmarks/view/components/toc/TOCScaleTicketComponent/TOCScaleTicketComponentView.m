//
//  TOCScaleTicketComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Version 2 by Dennis Christopher on 11/27/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCScaleTicketComponentView.h"
#import "DataEngine.h"
#import "Utils.h"
#import "UIImage+Resize.h"
#import "PhotoComponentTableViewCell.h"
#import "ScaleTicketComponentTableViewCell.h"
#import "AttachPhotoViewController.h"
#import "AlertViewController.h"
#import "MAImagePickerController.h"
#import "AttachScaleTicketViewController.h"
#import "NSManagedObject+NSManagedObject.h"
#import "PhotoType+PhotoType.h"
//#import "DockViewController.h"
//#import "SlideView.h"
#import "TireType.h"
#import "TOCPageViewController.h"
#import "TreadMarks-Swift.h"


#define TOC_SCALE_TICKET_COMPONENT_HEIGHT_OFF 80
#define COMPONENT_VIEW_POSTION_IN_PTR 4
#define COMPONENT_VIEW_POSTION_IN_DOT 5
#define COMPONENT_VIEW_POSTION_IN_PIT 4


@interface TOCScaleTicketComponentView () 


@property (strong) NSArray* scaleTickets;
@property (weak) AttachScaleTicketViewController* popoverContainerViewController;

@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIView *tapToInsertView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *addOutboundView;
@property (weak, nonatomic) IBOutlet UIView *weightVarianceView;
@property (weak, nonatomic) IBOutlet UILabel *scaleWeightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightVarianceLabel;
@property (nonatomic, weak) IBOutlet UILabel *outboundLabel;
@property (nonatomic, weak) IBOutlet UITapGestureRecognizer *addOutboundTapGestureRecognizer;
@property (strong) CLLocation *location;
@property (strong) CLLocationManager *locationManager;
@property (strong)NSDecimalNumber *netWeightDecimal;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPlus;


@property (weak, nonatomic) IBOutlet UILabel *labelOptional;


@end

@implementation TOCScaleTicketComponentView

#pragma mark - Initializers

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(
										   frame.origin.x,
										   frame.origin.y,
										   COMPONENT_WIDTH,
										   TOC_SCALE_TICKET_COMPONENT_HEIGHT_OFF)];
	if (self)
	{
		[self connectNib];
		self.status = OFF_SCREEN;
		self.heightOff = TOC_SCALE_TICKET_COMPONENT_HEIGHT_OFF;
		self.valid = YES;
	}
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tiresDone:) name:@"TireCountsDone"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidComplete:) name:@"Sync Complete"  object:nil];

	return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TireCountsDone" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Sync Complete" object:nil];
}
-(void)setviewUI
{
    self.status = OFF_SCREEN;
    self.toggleButton.selected = self.status;
    [self updateUI];
    
}


#pragma mark - Override methods
- (void)syncDidComplete:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"Sync Complete"]) {
        [self updateUI];
        [self.parentPageViewController updateUI];
    }
}

- (void)updateUI {
	
	[super updateUI];
    
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT) {
        self.labelOptional.hidden=NO;
        self.imageViewPlus.image =[UIImage imageNamed:@"toc-btn-plus-grey.png"];
        
    }
    else
    {
        self.labelOptional.hidden=YES;
    }
    self.scaleTickets =[[DataEngine sharedInstance] scaleTicketsForTransaction:self.transaction andPhotoType:PHOTOTYPE_ID_SCALE_TICKET];
    
	self.toggleButton.selected = self.status;
	
	if (self.scaleTickets == NULL || [self.scaleTickets count] == 0)
	{
		self.toggleButton.hidden = YES;
		self.tapToInsertView.hidden = NO;
        self.weightVarianceView.hidden = YES;
		self.status = OFF_SCREEN;
        if (self.transaction.transactionType.transactionTypeId ==  TRANSACTION_TYPE_ID_DOT)
        {
        self.validateImageView.hidden = YES;
        }
        

	}
	else
	{
		self.toggleButton.hidden = NO;
		self.tapToInsertView.hidden = YES;
        self.weightVarianceView.hidden = NO;
		self.validateImageView.hidden = NO;

		CGRect frame = self.containerView.frame;
		frame.size.height = 77 // title minus offset
			+ self.tableView.rowHeight * [self.scaleTickets count] //tableview
			+ 1 // separator
			+ 80 // add photo
			+ 5; // component separator
		
		self.containerView.frame = frame;

		[self.tableView reloadData];
	}
    [self updateOutboundLabel];
    [self updateWeightVarianceLabel];
    Comment *varianceComment = [Utils weightVarianceCommentForTransaction:self.transaction];
    if (varianceComment != nil && [self weightVarianceOkay]==YES) {
        [self removeWeightVarianceComment:varianceComment];
    }
    //fix for control center
       [self validate];
}

- (CGFloat)heightOn {
	
	return self.containerView.frame.size.height;
}

- (void)tiresDone:(NSNotification *) notification {
    
 if ([[notification name] isEqualToString:@"TireCountsDone"]) {
        [self updateUI];
     //[self updateWeightVarianceLabel];
    }
      
}
-(void)validate{
    
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT) {
        self.valid=YES;
        
        BOOL valid = YES;
        if ([self hasTicketOfType:@SCALE_TICKET_TYPE_BOTH] ==YES) {
            valid = NO;
            
            valid = [self validTicket:[self.scaleTickets objectAtIndex:0]];
        }
        else {
            if ([self.scaleTickets count]==2) {
                valid = YES;
                
                if ([self hasTicketOfType:@SCALE_TICKET_TYPE_INBOUND] && [self hasTicketOfType:@SCALE_TICKET_TYPE_OUTBOUND]==NO) {
                    valid = NO;
                }
                else {
                    for (ScaleTicket *theTicket in self.scaleTickets) {
                        if ([self validTicket:theTicket]==NO) {
                            valid=NO;
                            break;
                        }
                    }
                }
            }
            if ([self.scaleTickets count]==1) {
                
                valid = NO;
            }
        }
        self.valid = valid;
        [self updateValidateImage];
        [self.parentPageViewController validate];

  
    }
    else
    {
        BOOL valid = NO;
        if ([self hasTicketOfType:@SCALE_TICKET_TYPE_BOTH] ==YES) {
            valid = [self validTicket:[self.scaleTickets objectAtIndex:0]];
        }
        else {
            if ([self.scaleTickets count]==2) {
                valid = YES;
                
                if ([self hasTicketOfType:@SCALE_TICKET_TYPE_INBOUND] && [self hasTicketOfType:@SCALE_TICKET_TYPE_OUTBOUND]==NO) {
                    valid = NO;
                }
                else {
                    for (ScaleTicket *theTicket in self.scaleTickets) {
                        if ([self validTicket:theTicket]==NO) {
                            valid=NO;
                            break;
                        }
                    }
                }
            }
        }
        self.valid = valid;
        [self updateValidateImage];
        [self.parentPageViewController validate];
   }
    
    
    
    
}


#pragma mark - Helper methods

-(BOOL) validTicket:(ScaleTicket *)theTicket {
    BOOL valid = YES;
    if (theTicket.ticketNumber == nil || [theTicket.ticketNumber isEqualToString:@""])
    {
        valid = NO;
    }
    if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
        if (theTicket.inboundWeight==nil || [theTicket.inboundWeight isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            valid = NO;
        }
    }
    else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
        if (theTicket.outboundWeight==nil || [theTicket.outboundWeight isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            valid = NO;
        }
    }
    else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
        if (theTicket.inboundWeight==nil || [theTicket.inboundWeight isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            valid = NO;
        }
        if (theTicket.outboundWeight==nil || [theTicket.outboundWeight isEqualToNumber:[NSNumber numberWithInteger:0]]) {
            valid = NO;
        }
    }
    return valid;
}

-(void) updateOutboundLabel {
    if ([self hasTicketOfType:@SCALE_TICKET_TYPE_BOTH]==YES) {
        self.addOutboundView.hidden=YES;
        //Make height 1
       
    }
    else {
        BOOL active = ([self.scaleTickets count] > 1) ? NO : YES;
        self.addOutboundTapGestureRecognizer.enabled = active;
        self.addOutboundView.hidden = ! active;
        
        if ( [self hasTicketOfType:@SCALE_TICKET_TYPE_OUTBOUND]==YES) {
            self.outboundLabel.text =@"Add Inbound";
        }
        else {
            self.outboundLabel.text =@"Add Outbound";
        }
    }
}


/*-(BOOL) hasOutbound {
    return ;
}*/


-(BOOL) hasTicketOfType:(NSNumber *)typeId {
    BOOL result = NO;
    for (ScaleTicket *theTicket in self.scaleTickets) {
        if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:typeId]) {
            result = YES;
            break;
        }
    }
    return result;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    self.location = [locations lastObject];

    [manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [Utils log:@"Location error:",[error localizedDescription]];
    [manager stopUpdatingLocation];
}

#pragma mark - Weight Variance methods

+(void)refreshVarriance
{
 
}
// displays the net weight, and the variance-green acceptable, red problematic
// for now the Net weight is always shown in kilograms
-(void) updateWeightVarianceLabel {
    float netWeight = [self netWeight];
    double netWeightDecimal =[self.netWeightDecimal doubleValue];
    // if the netWeight in unavailable either supply a message or hide the variance view
    if (netWeight==INVALID_WEIGHT_VALUE || netWeightDecimal==0.0000) {
        self.weightVarianceView.hidden=YES;
    }
    ScaleTicket *firstTicket = [self.scaleTickets objectAtIndex:0];
    UnitType *theType = firstTicket.unitType;
    NSString *weightTypeString = nil;
    if (theType != Nil) {
        // TBD: always KG for net weight display? recall inbound and outbound could be different units
        weightTypeString = (theType.unitTypeId == UNIT_TYPE_ID_KILOGRAM) ? @"KG" : @"LB";
    }
    

    
   // NSString *weightTypeString = @"KG";
    // As the calulaton on TM is of type Decimal
    // we  to have show the Net Weight upto 4Decimal point
    
   
    self.scaleWeightLabel.text = [NSString stringWithFormat:@"%.04f %@", netWeightDecimal, weightTypeString];
    //self.scaleWeightLabel.text = [NSString stringWithFormat:@"%.04f %@", netWeight, weightTypeString];
    

    NSString *displayString = @"";  //@"Variance not available";
    float weightVariance = [self weightVariance];
    if (weightVariance != INVALID_WEIGHT_VALUE) {
        float varianceAsWholePCT = weightVariance * 100;
        displayString = [NSString stringWithFormat:@"Variance: %.02f%@", varianceAsWholePCT,@"%"];
    }
    self.weightVarianceLabel.text = displayString;
    if ([self weightVarianceOkay:weightVariance]==NO) {
        self.weightVarianceLabel.textColor = [UIColor redColor];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Variance1500"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else {
        //remove weightvariance comment
        self.weightVarianceLabel.textColor = [UIColor greenColor];
        Comment *varianceComment = [Utils weightVarianceCommentForTransaction:self.transaction];
        if (varianceComment != nil) {
            [self removeWeightVarianceComment:varianceComment];
        }

    }
    //green color if 15.0000
   /* if([displayString isEqualToString:@"Variance: 15.0000%"]||[displayString isEqualToString:@"Variance: -15.0000%"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Variance1500"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self.weightVarianceLabel.textColor = [UIColor greenColor];
        Comment *varianceComment = [Utils weightVarianceCommentForTransaction:self.transaction];
        if (varianceComment != nil) {
            [self removeWeightVarianceComment:varianceComment];
        }

    }*/

}

-(BOOL) weightVarianceOkay {
    float variance = [self weightVariance];
    return [self weightVarianceOkay:variance];
}

-(BOOL) weightVarianceOkay:(float)variance {
    float varianceAsWholePCT =(float) variance * 100 ;
    if (variance==INVALID_WEIGHT_VALUE) {
        return YES;
    }
    return !(fabsf(varianceAsWholePCT) >= 15);
    //return !(varianceAsWholePCT > 15);
}

-(float) weightVariance {
    float estimatedWeight = [self estimatedWeight] ;
    float actualWeight = [self netWeight];
    if (estimatedWeight==INVALID_WEIGHT_VALUE || actualWeight==INVALID_WEIGHT_VALUE) {
        return INVALID_WEIGHT_VALUE;
    }
    float variancePCT = (actualWeight-estimatedWeight)/estimatedWeight;
    return variancePCT;
}

// values always in KGs:
-(float) estimatedWeight {
    float estimatedWeight = INVALID_WEIGHT_VALUE;
    NSArray *transactionTireTypeArray = [[DataEngine sharedInstance] transactionTireTypeForTransactionId:self.transaction.transactionId];
    
    for (Transaction_TireType *type in transactionTireTypeArray) {
        if (type.quantity != nil && [type.quantity integerValue]>=0) {
            estimatedWeight += [type.quantity integerValue]* [type.tireType.estimatedWeight integerValue];
        }
    }
    
    return estimatedWeight;
}

#define LBS_TO_KILOGRAMS 0.45359237
// net weight - always in kilograms for comparison
-(float) netWeight {
    float netWeight = INVALID_WEIGHT_VALUE;
    float grossWeight = -1.0;
    float tareWeight = -1.0;
    BOOL ticketTypeIsBoth = NO;
    BOOL hasInbound = NO;
    BOOL hasOutbound = NO;
   // As the calulaton on TM is of type Decimal
   // we  to have show the Net Weight in Decimal value
    NSDecimalNumber *inboundDecimal=[NSDecimalNumber decimalNumberWithString:@"0.0"];
    NSDecimalNumber *outboundDecimal=[NSDecimalNumber decimalNumberWithString:@"0.0"];
       NSDecimalNumber *conversion = [NSDecimalNumber decimalNumberWithString:@"0.453592"];
    for (ScaleTicket *theTicket in self.scaleTickets) {
        UnitType *theType = theTicket.unitType;
        if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
            tareWeight = [theTicket.outboundWeight    floatValue];
            outboundDecimal=theTicket.outboundWeight;
            if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                tareWeight *= LBS_TO_KILOGRAMS;
                outboundDecimal=[outboundDecimal decimalNumberByMultiplyingBy:conversion];
            }
            hasOutbound = YES;
        }
        else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
            grossWeight = [theTicket.inboundWeight floatValue];
          inboundDecimal=theTicket.inboundWeight;
            if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                grossWeight *= LBS_TO_KILOGRAMS;
         
                inboundDecimal=[inboundDecimal decimalNumberByMultiplyingBy:conversion];
            }
            hasInbound = YES;
        }
        else if ([theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
           
            netWeight = [theTicket.inboundWeight floatValue] - [theTicket.outboundWeight floatValue] ;
            inboundDecimal=theTicket.inboundWeight;
            outboundDecimal=theTicket.outboundWeight;
            self.netWeightDecimal=[inboundDecimal decimalNumberBySubtracting:outboundDecimal];
            if (theType.unitTypeId == UNIT_TYPE_ID_POUND) {
                netWeight *= LBS_TO_KILOGRAMS;
                self.netWeightDecimal=[self.netWeightDecimal decimalNumberByMultiplyingBy:conversion];// withBehavior:roundUp];
            }
            ticketTypeIsBoth = YES;
        }
    }
    if (ticketTypeIsBoth==NO && hasOutbound && hasInbound) {
        netWeight = grossWeight-tareWeight;
        if(outboundDecimal != nil)
        { self.netWeightDecimal=[inboundDecimal decimalNumberBySubtracting:outboundDecimal];}
        else
        { self.netWeightDecimal=[NSDecimalNumber decimalNumberWithString:@"0.0000"];}//;inboundDecimal ;}

    }
    return netWeight;
}

-(void) removeWeightVarianceComment:(Comment *)comment {
   
    //Dont remove leave it blank and change status to un sync
   /* NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    [context deleteObject:comment];*/
    comment.text=nil;//@"";
    comment.syncDate=nil;
    [self save];
    TOCPageViewController *toc = (TOCPageViewController *)self.parentPageViewController;
    [toc removeWeightVarianceCommentComponent];
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
}

#pragma mark - Methods

- (void)connectNib {
	
	[self addSubview:[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													owner:self
												  options:NULL] objectAtIndex:0]];
  //
}

- (void)showPopover:(id<TOCScaleTicketComponentViewDelegate>)resultView{

    if ([resultView isKindOfClass: [ScaleTicketComponentTableViewCell class]]==YES) {
        ScaleTicket *ticket = ((ScaleTicketComponentTableViewCell *)resultView).scaleTicket;
        [self presentAttachPopover:resultView scaleTicket:ticket];
    }
}

-(void)showCamera:(ScaleTicket *)ticket  {
    [self startCamera:ticket];
}

-(void) startCamera:(ScaleTicket *)scaleTicket {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

		/// Start capturing the location
        self.locationManager =  [[CLLocationManager alloc] init];
		self.locationManager.delegate = self;
		self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		[self.locationManager startUpdatingLocation];

        MAImagePickerController *imagePicker = [[MAImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        imagePicker.scaleTicket = scaleTicket;
        imagePicker.sourceType = MAImagePickerControllerSourceTypeCamera;
        //elf.parentPageViewController.dockViewController.dockViewHidden=YES;
        //self.parentPageViewController.dockViewController.slideView.hidden=YES;
        [self.parentPageViewController.navigationController pushViewController:imagePicker animated:NO];
        
    } else {
        [Utils showMessage:self.parentPageViewController header:@"Error" message:@"Camera not available" hideDelay:1];
    }
}

- (void) popoverRequestsRetake:(ScaleTicket *)scaleTicket
                          resultView:(id<TOCScaleTicketComponentViewDelegate>)resultView {
    
    [self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];
    [self save];
    [self updateUI];
    [self showCamera:scaleTicket];
    
}

- (void)popoverDone:(ScaleTicket *)scaleTicket
			  image:(UIImage *)image
		 resultView:(id<TOCScaleTicketComponentViewDelegate>)resultView {
    
    if (scaleTicket==nil) {
        NSError *error = [Utils errorForType:@"ScaleTicket Component popoverDone" errorCode:0];
        NSLog(@"%@", error.localizedDescription);
    }
	
	[self setLocationForScaleTicket:scaleTicket];
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
	
    [self save];
    [self updateUI];
    
    [self.parentPageViewController updateUI];
    
    [self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];
}

// popover cancelled:
// less than ideal, we need to check the content of the scaleticket record to see if it
// should be deleted: handle the case where user just captured ticket but then cancels
- (void)cancelAnimated:(BOOL)animated resultView:(id)resultView {
    [self.popoverContainerViewController dismissViewControllerAnimated:animated completion:NULL];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ScaleTicketCreated"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ScaleTicketCreated"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    if ([resultView isKindOfClass:[ScaleTicket class]]==YES) {
        ScaleTicket *ticket = (ScaleTicket *)resultView;
        //Not good as it  is  causing lots of issue
       //if (ticket.ticketNumber == nil) {
            [self deleteScaleTicket:ticket];
            [self save];
            [self updateUI];
            
            [self.parentPageViewController updateUI];
       }
    }
}


-(void) presentAttachPopover:(id<TOCScaleTicketComponentViewDelegate>)resultView scaleTicket:(ScaleTicket *)ticket {
   AttachScaleTicketViewController* popover = [[AttachScaleTicketViewController alloc] init];
   
   popover.containerViewController = self.parentPageViewController;
   popover.delegate = self;
   popover.resultView = resultView;
    popover.scaleTicket = ticket;
   
   self.popoverContainerViewController = popover;
   
   if(self.parentPageViewController == NULL)
   {
       @throw [NSException exceptionWithName:NSObjectNotAvailableException
                                      reason:@"parentPageViewController is not set."
                                    userInfo:NULL];
   }
   
   self.popoverContainerViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
   [self.parentPageViewController presentViewController:self.popoverContainerViewController animated:YES completion:NULL];
}

- (void)setLocationForScaleTicket:(ScaleTicket *)scaleTicket {
    
    if (self.location != nil)
    {
        scaleTicket.photo.latitude = [NSString stringWithFormat:@"%f", self.location.coordinate.latitude];
        scaleTicket.photo.longitude = [NSString stringWithFormat:@"%f", self.location.coordinate.longitude];
        self.location = nil;
    }
}

#pragma mark -  MAImagePickerControllerDelegate

- (void)imagePickerDidCancel {
    [self.parentPageViewController.navigationController popViewControllerAnimated:YES];
    // we have inserted a scaleticket at this point, but not yet saved it, so we need to clear the context
    NSManagedObjectContext *moc = [[DataEngine sharedInstance] managedObjectContext];
    [moc rollback];
}


- (void)imagePickerDidChooseImage:(ScaleTicket *)scaleTicket image:(UIImage *)theImage{
    //From 1348
    //[self updateUI];
    [self.parentPageViewController.navigationController popToViewController:self.parentPageViewController animated:NO];
    
    if (scaleTicket==nil) {
        NSError *error = [Utils errorForType:@"ScaleTicket Component imagePickerDidChooseImage" errorCode:0];
        NSLog(@"%@", error.localizedDescription);
    }
    if (theImage != NULL)
    {
        // If photo record does not exist, create one
        if (scaleTicket.photo==nil) {
            Photo *photo = (Photo *)[[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
            photo.transaction = self.transaction;
            photo.createdDate = [NSDate date];
            photo.photoType = (PhotoType*)[PhotoType singleById:PHOTOTYPE_ID_SCALE_TICKET];
            photo.photoId = [[NSUUID UUID] UUIDString];
            photo.fileName = [Utils nameForPhoto];
            photo.syncDate = nil;
			photo.imageSyncDate = nil;
            scaleTicket.photo = photo;
        }
		
		[self setLocationForScaleTicket:scaleTicket];
        
        [self saveCurrentImage:theImage fileName:scaleTicket.photo.fileName];
		scaleTicket.photo.imageSyncDate = nil;
    }
    [self save];
    //from 1348<-------
    [self updateUI];
    [self.parentPageViewController updateUI];
    //------->
    [self presentAttachPopover:nil scaleTicket:scaleTicket];
}


- (void)closeCamera
{
    [self.popoverContainerViewController dismissViewControllerAnimated:NO completion:NULL];
}

#pragma mark - IBActions

- (IBAction)tapToInsert:(UITapGestureRecognizer *)sender {
	//adding scale ticket for first time  son on cancel  delescale ticket
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ScaleTicketCreated"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self presentAlertForScaleTicketCreate:self.parentPageViewController.navigationController info:@{@"data": @"Create"}];
}

- (IBAction)toggleButtonPressed:(UIButton *)sender {

    self.status = 1 - self.status;
    self.toggleButton.selected = self.status;
    
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR) {
        
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PTR];
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PIT];
    }
    // TRY:
    [self updateUI];
    [self.parentPageViewController updateUI];
}

- (IBAction)tapToAddOutboundTicket:(UITapGestureRecognizer *)sender  {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ScaleTicketCreated"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    ScaleTicketType *theType = nil;
    if ([self hasTicketOfType:@SCALE_TICKET_TYPE_OUTBOUND]==YES) {
         theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_INBOUND];
    }
    else {
        theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_OUTBOUND];
    }
    ScaleTicket *ticket = [self createScaleTicketOfType:theType];
    if (ticket != nil) {
        [self showCamera:ticket];
    }
}

#pragma mark - AlertViewDelegate

// Delete scaleticket or create of type "both"
- (void)alertView:(AlertViewController *)alertView
	primaryAction:(UIButton *)button
			 info:(NSDictionary*)info {
     [self enableDock:YES];
    [self.parentPageViewController.navigationController dismissViewControllerAnimated:YES completion:NULL];
    [self enableDock:YES];
    if (info != nil) {
        ScaleTicket* ticket =nil;
        if ([info[@"data"] isKindOfClass:[ScaleTicket class]]==YES) {
            ticket = info[@"data"];
            [self deleteScaleTicket:ticket];
            [self save];
            [self updateUI];
            [self.parentPageViewController updateUI];
        }
        else {
            // "BOTH" TYPE scale ticket is created
            ScaleTicketType *theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_BOTH];
            ScaleTicket *ticket = [self createScaleTicketOfType:theType];
            if (ticket != nil) {
                [self showCamera:ticket];
            }
        }
    }
 }


// i.e. create inbound, or cancel
- (void)alertView:(AlertViewController *)alertView
  secondaryAction:(UIButton *)button
			 info:(NSDictionary*)info {
     [self enableDock:YES];
    [self.parentPageViewController.navigationController dismissViewControllerAnimated:YES completion:NULL];
    
    if ([info[@"data"] isKindOfClass:[ScaleTicket class]]==YES) {   // i.e.e from the table cell remove
        return;
    }
    
    ScaleTicketType *theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_INBOUND];
    ScaleTicket *ticket = [self createScaleTicketOfType:theType];
    if (ticket != nil) {
        [self showCamera:ticket];
    }
}

// i.e. create outbound
- (void)alertView:(AlertViewController *)alertView
  tertiaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    [self.parentPageViewController.navigationController dismissViewControllerAnimated:YES completion:NULL];
     [self enableDock:YES];
    ScaleTicketType *theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_OUTBOUND];
    ScaleTicket *ticket = [self createScaleTicketOfType:theType];
    if (ticket != nil) {
        [self showCamera:ticket];
    }
}
/*- (void)alertView:(AlertViewController *)alertView
   backAction:(UIButton *)button
             info:(NSDictionary*)info {
    [self enableDock:YES];
   // [self.parentPageViewController.navigationController dismissViewControllerAnimated:YES completion:NULL];
    
    }*/

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
     [self enableDock:NO];
	AlertViewController* alert = [[AlertViewController alloc] init];
	
	alert.title = @"Warning";
	alert.heading = @"Are you sure you want to remove your scale ticket?";
	alert.message = @"You will lose your inbound and/or outbound scale ticket and related entries that were recorded for this transaction.";
	alert.primaryButtonText = @"Remove";
	alert.primaryInfo = info;
    alert.secondaryInfo = info;
	alert.secondaryButtonText = @"Cancel";
    alert.tertiaryButtonText = @"";
   
	alert.delegate = self;
	
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
	[viewController presentViewController:alert animated:YES completion:NULL];
}
-(void)enableDock :(bool)value
{
   // self.parentPageViewController.dockViewController.slideView.userInteractionEnabled=value;
    //self.parentPageViewController.dockViewController.toggleButton.userInteractionEnabled=value;
}
- (void)presentAlertForScaleTicketCreate:(UIViewController *)viewController info:(NSDictionary *)info {
    
    [self enableDock:NO];
    AlertViewController* alert = [[AlertViewController alloc] init];
    
    alert.title = @"Capture Scale Ticket";
    alert.heading = @"Is this scale ticket image for Inbound, Outbound or Both?";
    alert.message = @"Choose both if your single scale ticket contains both the gross (inbound) and tare (outbound) weights.";
    alert.primaryButtonText = @"Both";
    alert.primaryInfo = info;
    alert.secondaryInfo = info;
    alert.secondaryButtonText = @"Inbound";
    alert.tertiaryButtonText = @"Outbound";
    alert.isPopOverDismissOnTouch=YES;
    alert.showBack=YES;
    
    alert.delegate = self;
    
    
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [viewController presentViewController:alert animated:YES completion:NULL];
}


#pragma mark -  ScaleTicket record support

-(ScaleTicket *) createScaleTicketOfType:(ScaleTicketType *)type {
    ScaleTicket *scaleTicket = nil;
    scaleTicket = (ScaleTicket*)[ScaleTicket insertNewObject:[[DataEngine sharedInstance] managedObjectContext]];
    scaleTicket.scaleTicketId = [[NSUUID UUID] UUIDString];
    scaleTicket.transaction = self.transaction;
    scaleTicket.date = [NSDate date];
    scaleTicket.inboundWeight = nil;
    scaleTicket.outboundWeight = nil;
    scaleTicket.scaleTicketType = type;
    scaleTicket.ticketNumber = nil;
    
    return scaleTicket;
}

- (void)edit:(id<TOCScaleTicketComponentViewDelegate>)cell {
    [self showPopover:cell];
}



/*-(ScaleTicket *)outboundTicketForTicket:(ScaleTicket *)ticket {
    ScaleTicket *outboundTicket = nil;
    if ([self.scaleTickets count] >1) {
        for (ScaleTicket *theTicket in self.scaleTickets) {
            if (theTicket != ticket && [theTicket.scaleTicketType.scaleTicketTypeId isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
                outboundTicket = theTicket;
                break;
            }
        }
    }
    return outboundTicket;
}*/

// Delete data object and optionally the associated image files
// if scale ticket has corresponding weight variance comment, delete it as well
-(void) deleteScaleTicket:(ScaleTicket *)theTicket{
    NSError* error;
    if (theTicket.photo.syncDate==nil && ![theTicket.photo.fileName isEqualToString:@"DELETE"]) {
        NSString *photoPath = [Utils pathForPhotoWithPhotoName:theTicket.photo.fileName];
        [[NSFileManager defaultManager] removeItemAtPath:[photoPath stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"] error:&error];
        [[DataEngine sharedInstance].managedObjectContext deleteObject:theTicket.photo];
        [[DataEngine sharedInstance].managedObjectContext deleteObject:theTicket];
    }
    else {
        // Delete the record
        
            [Utils deleteScaleTicketImageFile:theTicket];
            [[DataEngine sharedInstance] deletePhoto:theTicket.photo];
       
        [[DataEngine sharedInstance].managedObjectContext deleteObject:theTicket];
        self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    }

    
    
    Comment *varianceComment = [Utils weightVarianceCommentForTransaction:self.transaction];
    if (varianceComment != nil) {
        [self removeWeightVarianceComment:varianceComment];
    }
    
}

- (UIImage*)correntImageOrientation:(UIImage *)image {
	
    UIImage *sameImage = [image resizedImage:[image size]
						interpolationQuality:kCGInterpolationNone];
	
    return sameImage;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return [self.scaleTickets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString* reusableCellIdentifier = @"ScaleTicketComponentTableViewCell";
	ScaleTicketComponentTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	
	if(cell == NULL)
	{
		[tableView registerClass:NSClassFromString(reusableCellIdentifier)
		  forCellReuseIdentifier:reusableCellIdentifier];
        
		cell = [tableView dequeueReusableCellWithIdentifier:reusableCellIdentifier];
	}
	
	cell.parentPageViewController = self.parentPageViewController;
	cell.delegate = self;
	
    ScaleTicket *ticket = self.scaleTickets[indexPath.row];
    [cell updateScaleTicket:ticket];
	
	return cell;
}

#pragma mark -  File saving support
// TBD: refactor class or place in utility
// Just save the small image it is adequate.
- (void)saveCurrentImage:(UIImage*)image fileName:(NSString*)fileName {
	
    UIImage *smallImageToSave = [self downSizeImage:image];
    
    // Save the new image to the App's Document directory
    NSString *pathForPhoto = [Utils pathForPhotoWithPhotoName:fileName];
    NSString *smallPathForPhoto = [pathForPhoto stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
    
    // Save a small version of it
    [self asynchSaveImage:smallImageToSave atPath:smallPathForPhoto andWait:YES];
}


- (UIImage*)downSizeImage:(UIImage*)theImage {
	
    CGSize newSize = CGSizeMake(theImage.size.width/4, theImage.size.height/4);
    UIGraphicsBeginImageContext( newSize );
    
    
    //  UIImage *resized = [theImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit
    //								  bounds:newSize
    //					interpolationQuality:kCGInterpolationHigh];
    [theImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resized;
}

- (void)asynchSaveImage:(UIImage*)imageToSave atPath:(NSString*)path andWait:(BOOL)wait {
	
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    __block BOOL savedOK = NO;
    
    CFDataRef myRef = CFBridgingRetain(UIImagePNGRepresentation(imageToSave));
    dispatch_data_t data = dispatch_data_create(CFDataGetBytePtr(myRef),CFDataGetLength(myRef),
                                                queue, ^{CFRelease(myRef);});
    
    dispatch_semaphore_t sem = nil;
    if (wait==YES) {
        sem = dispatch_semaphore_create(0);
    }
    
    const char *savePath = [path UTF8String];
    dispatch_fd_t fd = open(savePath, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG | S_IRWXO);
    printf("FD: %d\n", fd);
    if (fd==-1) {
        NSLog(@"Couldn't create file for photo saving.");
        return;
    }
    
    dispatch_write(fd, data, queue,^(dispatch_data_t d, int e) {
        printf("Written %zu bytes!\n", dispatch_data_get_size(data) - (d ? dispatch_data_get_size(d) : 0));
        printf("\tError: %d\n", e);
        if (wait==YES) {
            dispatch_semaphore_signal(sem);
        }
        if (e==0) {
            savedOK = YES;
        }
        close(fd);
    });
    if (wait==YES) {
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    }
}


@end

