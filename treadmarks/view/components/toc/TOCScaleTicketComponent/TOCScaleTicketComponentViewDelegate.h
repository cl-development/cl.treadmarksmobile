//
//  TOCScaleTicketComponentViewDelegate.h
//  TreadMarks
//
//  Created by Dennis Christopher on 3/7/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScaleTicket.h"

@class ScaleTicketComponentTableViewCell;

@protocol TOCScaleTicketComponentViewDelegate <NSObject>

- (void)edit:(ScaleTicketComponentTableViewCell*)cell;
- (void)popoverDone:(ScaleTicket *)scaleTicket
			  image:(UIImage *)image
		 resultView:(id)resultView;
- (void)cancelAnimated:(BOOL)animated resultView:(id)resultView;

- (void)popoverRequestsRetake:(ScaleTicket *)scaleTicket
		 resultView:(id)resultView;
-(void)closeCamera;

/*- (void)popoverRequestsDelete:(ScaleTicket *)scaleTicket
                   resultView:(id)resultView;*/

@end
