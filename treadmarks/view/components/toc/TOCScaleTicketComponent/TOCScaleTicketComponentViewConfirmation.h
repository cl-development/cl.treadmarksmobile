//
//  TOCScaleTicketComponentViewConfirmation.h
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
//#import "ComponentView.h"
#import "TOCScaleTicketComponentViewDelegate.h"


@interface TOCScaleTicketComponentViewConfirmation : TOCComponentView <UITableViewDataSource, UITableViewDelegate>

- (CGFloat)heightOn;

@end
