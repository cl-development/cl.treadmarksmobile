//
//  TOCScaleTicketComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "PhotoComponentTableViewCell.h"
#import "TOCScaleTicketComponentViewDelegate.h"
#import "MAImagePickerController.h"
#import <CoreLocation/CoreLocation.h>

@class PhotoComponentTableViewCell;

@interface TOCScaleTicketComponentView : TOCComponentView <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, MAImagePickerControllerDelegate,TOCScaleTicketComponentViewDelegate, AlertViewDelegate>

- (void)popoverDone:(ScaleTicket*)scaleTicket
			  image:(UIImage*)image
		 resultView:(id<TOCScaleTicketComponentViewDelegate>)resultView;
- (void)edit:(PhotoComponentTableViewCell*)cell;
-(void) updateWeightVarianceLabel;
-(BOOL)weightVarianceOkay;
+(void)refreshVarriance;
-(void)setviewUI;

@end
