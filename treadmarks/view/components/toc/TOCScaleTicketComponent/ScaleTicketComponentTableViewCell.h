//
//  ScaleTicketComponentTableViewCell.h
//  TreadMarks
//
//  Created by Dennis Christopher on 3/4/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//
#import "AlertViewDelegate.h"
#import "TOCScaleTicketComponentViewDelegate.h"
#import "Photo.h"
#import "ScaleTicket.h"

@interface ScaleTicketComponentTableViewCell : UITableViewCell

@property (weak) id<TOCScaleTicketComponentViewDelegate, AlertViewDelegate> delegate;
@property (weak) UIViewController* parentPageViewController;
@property (nonatomic, weak) ScaleTicket* scaleTicket;
@property (nonatomic, assign) BOOL readOnly;

- (void)updateScaleTicket:(ScaleTicket *)scaleTicket;

@end
