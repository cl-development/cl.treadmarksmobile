//
//  ScaleTicketWrapper.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-03-26.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScaleTicket.h"

@interface ScaleTicketWrapper : NSObject
@property (nonatomic, strong) ScaleTicket *scaleTicket;
//@property (nonatomic, assign) BOOL isOutbound;
@property (nonatomic, assign) BOOL isCopy;

- (id)initWithScaleTicket:(ScaleTicket *)ticket;

@end
