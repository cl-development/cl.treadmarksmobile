//
//  ScaleTicketWrapper.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-03-26.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ScaleTicketWrapper.h"

@implementation ScaleTicketWrapper

- (id)initWithScaleTicket:(ScaleTicket *)ticket
{
    self = [super init];
    if (self) {
        // Initialization code
        self.scaleTicket = ticket;
        //self.isOutbound = NO;
        self.isCopy = NO;
    }
    return self;
}

- (void)setIsCopy:(BOOL)copy {
    _isCopy=copy;
}

@end
