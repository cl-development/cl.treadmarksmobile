//
//  TOCEligibilityPopoverViewController.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-05-29.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation

class TOCEligibilityPopoverViewController : TOCPopoverViewController {
    @objc var transaction : Transaction? = nil
    var trans_eligibilities : [Transaction_Eligibility]? = []
    var eligibilities :  [NSNumber] = [2,3,4,5]
    @IBOutlet var tiresEligibleLabel : UILabel!
    @IBOutlet var tiresEligibleSwitch : UISwitch!
    @IBOutlet var labelCollection: [UILabel]!
    @IBOutlet var switchCollection: [UISwitch]!
    fileprivate let TRANSACTION_TYPE_ID_UCR : NSNumber = NSNumber(value: 6 as Int32)
    @IBOutlet weak var subView : UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subView.layer.cornerRadius = 20
        subView.layer.masksToBounds = true
        subView.layer.shadowColor = UIColor.black.cgColor
        subView.layer.shadowOpacity = 0.8
        subView.layer.shadowRadius = 3.0
        subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        
        self.tiresEligibleSwitch.isEnabled=self.defaultMasterSwitchEnabled()
        self.tiresEligibleSwitch.isOn=self.defaultMasterSwitchOn()
        self.tiresEligibleLabel.text = self.tiresEligibleSwitch.isOn ? "Yes" : "No"
        self.doneButton.isEnabled = self.defaultDoneButton()
        
        for sw: UISwitch in self.switchCollection {
            sw.isEnabled=false
        }
        
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        if let test : [Transaction_Eligibility] = engineInstance.transactionEligibilities(for: self.transaction) as? [Transaction_Eligibility] {
            for trans_elig in test {
                self.trans_eligibilities!.append(trans_elig)
            }
            if ((self.trans_eligibilities!).count != 0) {
                let master_elig = self.masterEligibility(test)
                self.tiresEligibleSwitch.isOn = master_elig.value as! Bool
                self.tiresEligibleLabel.text = self.tiresEligibleSwitch.isOn ? "Yes" : "No"
                var disableMasterSwitch:Bool = false
                for sw: UISwitch in self.switchCollection {
                    if let trans_elig : Transaction_Eligibility =
                        self.correspondingTransactionEligibility(sw.tag as NSNumber) {
                        sw.isOn = trans_elig.value as! Bool
                    }
                    sw.isEnabled = self.shouldEnableTireReasonSwitch(sw.tag)
                    if (sw.isEnabled==true) {
                        disableMasterSwitch = true
                    }
                }
                if (disableMasterSwitch==true) {
                    self.tiresEligibleSwitch.isEnabled=false
                }
            }
        }
        else {
            for sw: UISwitch in self.switchCollection  {
                sw.isEnabled = self.shouldEnableTireReasonSwitch(sw.tag)
                if (sw.tag == 5){
                    sw.isEnabled = false
                }
            }
        }
    }
    
    func defaultMasterSwitchEnabled()->Bool {
        return self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR ? false : true
    }
    
    func defaultMasterSwitchOn()->Bool {
        return self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR ? false : true
    }
    
    func defaultDoneButton()->Bool {
        return self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR ? false : true
    }
    
    
    func masterEligibility(_ trans:[Transaction_Eligibility])->Transaction_Eligibility {
        let ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE =  NSNumber(value: 1 as Int)
        var result : Transaction_Eligibility? = nil
        for trans_elig : Transaction_Eligibility in trans {
            if (trans_elig.eligibility.eligibilityId==ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE) {
                result = trans_elig
                break
            }
        }
        return result!
    }
    
    
    
    @IBAction override func cancelButtonPressed(_ sender: AnyObject?) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction override func doneButtonPressed(_ sender: AnyObject?) {
        super.doneButtonPressed(sender)
        self.saveTransactionEligibilities()
        if (self.delegate != nil)   {
            if let view : TOCEligibilityComponentView = self.delegate as? TOCEligibilityComponentView {
                if view.responds(to: #selector(TOCEligibilityComponentView.popoverDone)) {
                    view.popoverDone()
                }
            }
        }
    }
    
    func saveTransactionEligibilities() {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        self.saveMasterElligibility()
        
        for sw: UISwitch in self.switchCollection {
            let valueToSet : NSNumber = NSNumber(value: sw.isOn as Bool)
            let elig : Eligibility = engineInstance.eligibility(forId: sw.tag as NSNumber)
            
            if sw.isEnabled == true {
                if let trans_elig : Transaction_Eligibility = self.correspondingTransactionEligibility(sw.tag as NSNumber) {
                    self.setTransEligibilityValues(trans_elig, value: valueToSet)
                }
                else {
                    let trans_elig: Transaction_Eligibility = (Transaction_Eligibility.insertNewObject(engineInstance.managedObjectContext, for:self.transaction, andEligibility:elig) as? Transaction_Eligibility!)!
                    self.setTransEligibilityValues(trans_elig, value: valueToSet)
                }
            }
            else {
                if let trans_elig : Transaction_Eligibility = self.correspondingTransactionEligibility(sw.tag as NSNumber) {
                    self.setTransEligibilityValues(trans_elig, value: valueToSet)
                }
            }
        }
        self.transaction?.clearSyncStatus()
        engineInstance.saveContext()
    }
    
    func saveMasterElligibility()->() {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        let elig : Eligibility = engineInstance.eligibility(forId: self.tiresEligibleSwitch.tag as NSNumber)
        let valueToSet : NSNumber = NSNumber(value: self.tiresEligibleSwitch.isOn as Bool)
        if let trans_elig : Transaction_Eligibility = self.correspondingTransactionEligibility(self.tiresEligibleSwitch.tag as NSNumber) {
            self.setTransEligibilityValues(trans_elig, value: valueToSet)
        }
        else {
            let trans_elig: Transaction_Eligibility = (Transaction_Eligibility.insertNewObject(engineInstance.managedObjectContext, for:self.transaction, andEligibility:elig) as? Transaction_Eligibility!)!
            self.setTransEligibilityValues(trans_elig, value: valueToSet)
        }
    }
    
    func correspondingTransactionEligibility(_ theTag:NSNumber)->Transaction_Eligibility? {
        var transactionEligibility : Transaction_Eligibility?
        for trans_elig:Transaction_Eligibility in self.trans_eligibilities! {
            if let elig: Eligibility = trans_elig.eligibility {
                if elig.eligibilityId.isEqual(to: theTag) {
                    transactionEligibility = trans_elig
                    break
                }
            }
        }
        return transactionEligibility
    }
    
    
    func setTransEligibilityValues(_ trans_elig: Transaction_Eligibility, value:NSNumber) {
        trans_elig.value = value;
        trans_elig.syncDate = nil;
        trans_elig.transaction = self.transaction
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesBegan(touches, with:event)
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesMoved(touches, with:event)
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
            print("\(touch)")
            
        }
        super.touchesEnded(touches, with:event)
        
        
    }
    
    
    @IBAction func tiresEligibleSwitchAction(_ sw : AnyObject)->() {
        self.tiresEligibleLabel.text = self.tiresEligibleSwitch.isOn ? "Yes" : "No"
        
        for sw: UISwitch in self.switchCollection  {
            sw.isEnabled = shouldEnableTireReasonSwitch(sw.tag)
            if (sw.tag == 5){
                sw.isEnabled = false
            }
            
        }
        
        var enableDoneButton : Bool = false
        if (self.tiresEligibleSwitch.isOn==true) {
            enableDoneButton = true
        }
        else {
            if self.tireReasonSwitchOn()==true {
                enableDoneButton = true
            }
            else {
                enableDoneButton = false
            }
        }
        self.doneButton.isEnabled = enableDoneButton
    }
    
    // is any tireReason switch on?
    func tireReasonSwitchOn()->Bool {
        var switchOn:Bool = false
        for sw: UISwitch in self.switchCollection  {
            if sw.isOn {
                switchOn = true
                break
            }
            if (sw.tag == 5){
                sw.isEnabled = false
            }
            
        }
        return switchOn
    }
    
  
    @IBAction func radioButtonPressed(_ button : UISwitch)->() {
        self.tiresEligibleSwitch.isOn = false
        var enableDoneButton : Bool = false
        for sw: UISwitch in self.switchCollection  {
            if sw.tag != button.tag {
                sw.isOn = false
                if (sw.tag == 5){
                    sw.isEnabled = false
                }
                
            }
            else {
                
                if self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR {
                    self.tiresEligibleSwitch.isEnabled = false

                }
                else
                {
                    self.tiresEligibleSwitch.isEnabled = !sw.isOn

                }
                enableDoneButton = sw.isOn
            }
            
        }
        self.doneButton.isEnabled = enableDoneButton
    }
    
    func shouldEnableTireReasonSwitch(_ tag: Int)->Bool {
        let TRANSACTION_TYPE_ID_TCR : NSNumber = NSNumber(value: 1 as Int32)
        let TRANSACTION_TYPE_ID_UCR : NSNumber = NSNumber(value: 6 as Int32)
        let TRANSACTION_TYPE_ID_DOT : NSNumber = NSNumber(value: 2 as Int32)
        var result : Bool = false
        if self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR {
            switch (tag) {
            case 2:
                result = self.tiresEligibleSwitch.isOn ? false : true
            case 3:
                result = false
            case 4:
                result = false
            case 5:
                result = false
            default:
                result = false
            }
        }
        else if self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR {
            switch (tag) {
            case 2:
                result = false
            case 3:
                result = self.tiresEligibleSwitch.isOn ? false : true
            case 4:
                result = self.tiresEligibleSwitch.isOn ? false : true
            case 5:
                result = false
            default:
                result = false
            }
        }
        else if self.transaction?.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_DOT {
            switch (tag) {
            case 2:
                result = self.tiresEligibleSwitch.isOn ? false : true
            case 3:
                result = false
            case 4:
                result = false
            case 5:
                result = false
            default:
                result = false
            }
        }
        
        return result
    }
}
