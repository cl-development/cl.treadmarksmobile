//
//  TOCEligibilityComponentView.h
//  TreadMarks
//
//  Created by Capris Group on 1/15/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"
#import "Constants.h"
#import "AlertViewController.h"

@class TOCEligibilityPopOverViewController;

@interface TOCEligibilityComponentView : TOCComponentView
-(void)setViewUI;
-(void)popoverDone;

@end
