//
//  TOCTransactionInformationComponentView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCComponentView.h"
#import "AlertViewController.h"

@interface TOCTransactionInformationComponentView : TOCComponentView

//@property (nonatomic,weak) IBOutlet UIView  *view;
-(void)setViewUI;
@end
