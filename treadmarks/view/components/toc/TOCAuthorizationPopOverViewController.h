//
//  TOCAuthorizationPopOverViewController.h
//  TreadMarks
//
//  Created by Capris Group on 2/12/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import "Authorization.h"
#import "STCAuthorization.h"
#import "DataEngine.h"
#import "UIView+CustomView.h"


@protocol AuthorizationDelegate <NSObject>

@optional
- (void) eventAuthorizationComponentUpdate:(NSString *)eventNumber;
@end


@interface TOCAuthorizationPopOverViewController : UIViewController <UITextFieldDelegate ,AuthorizationDelegate>

@property (weak, nonatomic) id<AuthorizationDelegate> authorizationDelegate;
@property (nonatomic, strong) Transaction *currentTransaction;
@property (nonatomic, strong) NSString *eventNumber;

@end
