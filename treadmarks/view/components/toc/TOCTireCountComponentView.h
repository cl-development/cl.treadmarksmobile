//
//  TOCTireCountComponentView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 1/23/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCComponentView.h"

@protocol TireCountComponentViewDelegate <NSObject>

@optional
- (void)didFinishEnteringTireCounts:(NSNotification *)notification;
@end

@interface TOCTireCountComponentView : TOCComponentView <UITableViewDelegate>
-(void)setViewUI;

@end
