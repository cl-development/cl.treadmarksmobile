//
//  TOCEligibilityComponentView.m
//  TreadMarks
//
//  Created by Capris Group on 1/15/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCEligibilityComponentView.h"
#import "PageViewController.h"
#import "TreadMarks-Swift.h"


#define TOC_ELIGIBILITY_COMPONENT_HEIGHT_ON 218
#define TOC_ELIGIBILITY_COMPONENT_HEIGHT_ADJUSTED 175
#define TOC_ELIGIBILITY_COMPONENT_HEIGHT_OFF 80
#define COMPONENT_VIEW_POSTION_IN_TCR 4
#define COMPONENT_VIEW_POSTION_IN_UCR 4
#define COMPONENT_VIEW_POSTION_IN_DOT 4



@interface TOCEligibilityComponentView ()
//@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIButton *tapToConfirm;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UILabel *eligibilityConfirmLabel;
@property (weak, nonatomic) IBOutlet UILabel *eligibileTireLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tapToConfirmImageView;
@property (weak, nonatomic) IBOutlet UILabel *eligibilityConfirmLabelTwo;
//@property (weak, nonatomic) IBOutlet UIImageView *validateImageView;
@property (nonatomic, weak) NSArray *trans_eligibilities;   // these are transaction eligibilities
@property (nonatomic, weak) Transaction *owningTransaction;
@property (nonatomic, assign) float componentHeight;
@end

@implementation TOCEligibilityComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_ELIGIBILITY_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_ELIGIBILITY_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_ELIGIBILITY_COMPONENT_HEIGHT_OFF;
    self.transaction = self.parentPageViewController.transaction;
    return self;
}

#pragma mark - ComponentView
-(void)setViewUI
{
    self.status=OFF_SCREEN;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    self.toggleButton.selected = self.status;
    
    
}
-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(CGFloat)heightOn {
	
	
    if ([self.eligibileTireLabel.text isEqualToString:@"Yes"]) {
         self.componentHeight = TOC_ELIGIBILITY_COMPONENT_HEIGHT_ADJUSTED;
    }  else {
        self.componentHeight = TOC_ELIGIBILITY_COMPONENT_HEIGHT_ON;
    }
    
	return self.componentHeight;
}
-(void)popoverDone {
    [self updateUI];
}


-(void)updateUI{
    
    self.owningTransaction = self.transaction;
    self.trans_eligibilities = [[DataEngine sharedInstance] transactionEligibilitiesForTransaction:self.owningTransaction];
    //NSLog(@"the count of the array is: %lu", (unsigned long)[self.trans_eligibilities count]);
    self.eligibilityConfirmLabelTwo.text = nil;
    self.eligibilityConfirmLabel.text =nil;
    for (Transaction_Eligibility *trans_elig in self.trans_eligibilities) {
       
        
        
        NSLog(@"%@",self.eligibilityConfirmLabel.text);
        NSLog(@"%@",self.eligibilityConfirmLabelTwo.text);
        
        if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE] )   {
            if ([trans_elig.value isEqualToNumber:[NSNumber numberWithInt:1]]) {
                self.eligibileTireLabel.text = @"Yes";
                self.eligibilityConfirmLabel.text = @"";
                            }
            
        }
        else if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_GENERATED] )   {
            if ([trans_elig.value isEqualToNumber:[NSNumber numberWithInt:1]]) {
                self.eligibileTireLabel.text = @"No";
                self.eligibilityConfirmLabel.text = @"Generated Tires";
            }
        }
        else if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_CHARGED] )   {
            if ([trans_elig.value isEqualToNumber:[NSNumber numberWithInt:1]]) {
                self.eligibileTireLabel.text = @"No";
                if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR) {
                    if ([self.eligibilityConfirmLabel.text length] == 0 || [self.eligibilityConfirmLabel.text isEqualToString:@""]) {
                        self.eligibilityConfirmLabel.text = @"Charged a disposal fee";
                        
                        
                    }else
                    {
                        self.eligibilityConfirmLabelTwo.text = @"Charged a disposal fee";
                    }
   
                    
                }
                else
                {
                      self.eligibilityConfirmLabel.text = @"Charged a disposal fee";
                    
                }
                            }}
            else if ( [trans_elig.eligibility.eligibilityId isEqualToNumber:ELIGIBILITY_TYPE_ID_TIRES_GENERATED_PRIOR] )   {
                NSLog(@"%@",trans_elig.value);
                NSLog(@"%@",trans_elig);
                if ([trans_elig.value isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    self.eligibileTireLabel.text = @"No";
                    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR) {
                        if ([self.eligibilityConfirmLabel.text length] ==0 || [self.eligibilityConfirmLabel.text isEqualToString:@""]) {
                            self.eligibilityConfirmLabel.text = @"Generated prior to Sept. 1st, 2009";
                            
                            
                        }else
                        {
                            self.eligibilityConfirmLabelTwo.text = @"Generated prior to Sept. 1st, 2009";
                        }

                    }
                    else
                    {
                        self.eligibilityConfirmLabel.text = @"Generated prior to Sept. 1st, 2009";
                        
                    }
                    
                }}
        

    }
    

    if ([self.trans_eligibilities count] > 0) {
        self.tapToConfirm.hidden = YES;
        self.toggleButton.hidden = NO;
        self.tapToConfirmImageView.hidden = YES;
 
    }  else {
        self.tapToConfirm.hidden = NO;
        self.tapToConfirmImageView.hidden = NO;
    }
 
    
    CGRect frame = self.view.frame, newFrame;
    if (self.status==ON_SCREEN) {
        if ([self.eligibileTireLabel.text isEqualToString:@"Yes"]) {
            newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_ELIGIBILITY_COMPONENT_HEIGHT_ADJUSTED);
        }  else {
            newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_ELIGIBILITY_COMPONENT_HEIGHT_ON);
        }
        
        
    } else {
        newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_ELIGIBILITY_COMPONENT_HEIGHT_OFF);
    }
    
    self.view.frame = newFrame;
    [self validate];
}

/// Should deprecate this function
- (void) eligibilityComponentUpdate: (NSString *)eligibilityText tap:(BOOL)tapToConfirm toggle:(BOOL)toggleState edit:(BOOL)editState {
    
    if ([eligibilityText isEqualToString:@""]) {
        self.eligibileTireLabel.text = @"Yes";
    }  else
        self.eligibileTireLabel.text = @"No";
    
    self.eligibilityConfirmLabel.text = eligibilityText;
    self.tapToConfirm.hidden = tapToConfirm;
    self.tapToConfirmImageView.hidden = tapToConfirm;
    self.toggleButton.hidden = toggleState;
    self.editButton.hidden = editState;
    
    [self validate];
    [self.parentPageViewController updateUI];
    //NSLog(@"hello?");
}

//- (IBAction)displayPopoverView:(UIButton *)sender {
//    
//    TOCEligibilityPopOverViewController *viewController = [[TOCEligibilityPopOverViewController alloc] initWithNibName:@"TOCEligibilityPopOverViewController" bundle:nil];
//    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    viewController.currentTransaction = self.owningTransaction;
//    viewController.eligible = self.eligibileTireLabel.text;
//    viewController.confirm = self.eligibilityConfirmLabel.text;
//    [viewController setEligibilityDelegate:(id)self];
//    //NSLog(@"The button pressed is: %@", sender.titleLabel.text);
//    
//    if ([sender.titleLabel.text isEqualToString:@"Edit"]) {
//        viewController.isEdit = YES;
//    }  else  {
//        viewController.isEdit = NO;
//    }
//    
//    if ([self.eligibilityConfirmLabel.text isEqualToString:@"Yes"] ){
//        viewController.tiresEligibleSwitch.on = YES;
//    }  else {
//        viewController.tiresEligibleSwitch.on = NO;
//    }
//    
//    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
//    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
//    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    [navigationController presentViewController:viewController animated:YES completion:nil];
//}

-(IBAction) showPopover:(id)sender {
    TOCEligibilityPopoverViewController *popover = [[TOCEligibilityPopoverViewController alloc] initWithNibName:@"TOCEligibilityPopOverViewController" bundle:nil];
    
    popover.delegate = self;
    popover.transaction = self.owningTransaction;
    
    if (self.parentPageViewController == NULL)
    {
        @throw [NSException exceptionWithName:NSObjectNotAvailableException
                                       reason:@"parentPageViewController is not set."
                                     userInfo:NULL];
    }
    
    UINavigationController *navigationController = self.parentPageViewController.navigationController;
    popover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:popover animated:YES completion:nil];
    
}

- (IBAction)toggleButtonPressed:(id)sender { 
    self.status = 1-self.status;
    self.editButton.hidden = NO;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    self.toggleButton.selected = self.status;
    
    //    if (self.status==ON_SCREEN) {
    //        self.editButton.hidden = NO;
    //    }  else {
    //        self.editButton.hidden = YES;
    //    }
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_TCR) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR];

    }
    else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];

    }else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
        
    }
    [self.parentPageViewController updateUI];

}

- (void) updateEditButton {
	
	if (self.status == ON_SCREEN)
	{
		self.editButton.alpha = 1;
	}
	else
	{
		self.editButton.alpha = 0;
	}
    [self.parentPageViewController updateUI];
//    if (self.status==ON_SCREEN) {
//        self.editButton.hidden = NO;
//    }  else {
//        self.editButton.hidden = YES;
//    }
}
-(void)validate{
    self.valid= YES;
    
    if (!self.tapToConfirm.hidden) {
        self.valid=NO;
    }

    [self updateValidateImage];
    [self.parentPageViewController validate];
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}
@end
