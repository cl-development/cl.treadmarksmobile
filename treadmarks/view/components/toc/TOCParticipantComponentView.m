//
//  TOCParticipantComponentView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TOCParticipantComponentView.h"
#import "Transaction.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"
#import "RegistrantType.h"
#import "User.h"
#import "Registrant.h"
#import "Location.h"
#import "DataEngine.h"
#import "Constants.h"
#import "Utils.h"
#import "PageViewController.h"
#import "LoginViewController.h"

#define TOC_PARTICIPANT_COMPONENT_HEIGHT_OFF 80
#define TOC_PARTICIPANT_COMPONENT_HEIGHT_ON 290

#define BUSINESS_NAME_ON_FRAME  CGRectMake(300, 117, 330, 80);
#define BUSINESS_NAME_OFF_FRAME CGRectMake(300, 0, 330, 80);
#define COMPONENT_VIEW_POSTION_IN_TCR_INCOMING_USER 1
#define COMPONENT_VIEW_POSTION_IN_TCR_OUTGOING_USER 2
#define COMPONENT_VIEW_POSTION_IN_STC 1
#define COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_YES_INCOMING_USER 2
#define COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_YES_OUTGOING_USER 1

#define COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_NO_INCOMING_USER 2
#define COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_NO_OUTGOING_USER 1

#define COMPONENT_VIEW_POSTION_IN_HIT_INCOMING_USER 1
#define COMPONENT_VIEW_POSTION_IN_HIT_OUTGOING_USER 2

#define COMPONENT_VIEW_POSTION_IN_UCR_INCOMING_USER 1
#define COMPONENT_VIEW_POSTION_IN_UCR_OUTGOING_USER 2

#define COMPONENT_VIEW_POSTION_IN_DOT_INCOMING_USER 1
#define COMPONENT_VIEW_POSTION_IN_DOT_OUTGOING_USER 2

#define COMPONENT_VIEW_POSTION_IN_PIT_INCOMING_USER 2
#define COMPONENT_VIEW_POSTION_IN_PIT_OUTGOING_USER 1

@interface TOCParticipantComponentView () <QRScanDelegate>

@property (weak, nonatomic) IBOutlet UILabel *registrantTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantBusinessNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantPhoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *registrantTypeImageView;

@property (weak, nonatomic) IBOutlet UILabel *companyHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactHeaderLabel;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIView *scanButtonView;
@property (weak, nonatomic) IBOutlet UIButton *rescanButton;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tempLoginGesture;
@property (weak, nonatomic) IBOutlet UIButton *tapToScanButton;

@property (strong) LoginViewController* login;
@property (weak, nonatomic) IBOutlet UIImageView *tocPlusImage;

@end

@implementation TOCParticipantComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_PARTICIPANT_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_PARTICIPANT_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_PARTICIPANT_COMPONENT_HEIGHT_OFF;
    return self;
}
-(void)setViewUI
{
    //self.status = !self.status;
    self.status=OFF_SCREEN;
    
    CGRect frame = self.view.frame, newFrame;
    self.toggleButton.selected = self.status;
    newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_PARTICIPANT_COMPONENT_HEIGHT_OFF);
    self.toggleButton.selected = self.status;
    if (self.isIncomingUser) {
        
        [self updateUIAnimated];
        
    }
    
    // [self updateUIAnimated];
    
}


#pragma mark - ComponentView

- (id)initWithUser:(User *)user expectedRegistrantType:(RegistrantType *)expectedRegType {
    if ((self = [super init]) != nil) {
        _user = user;
        _expectedRegistrantType = expectedRegType;
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUIAnimated{
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateUI];
                     }];
}

-(void)updateUI{
    
    //populate the values on the screen
    if ((self.isIncomingUser || self.transaction.outgoingUser) && (self.user != nil)) {
        // set up normal view
        //self.scanButtonView.hidden = YES;
        self.tocPlusImage.hidden=YES;
        self.tapToScanButton.hidden=YES;
        self.toggleButton.hidden = NO;
        
        Registrant *registrant = self.user.registrant;
        if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
            if (self.isIncomingUser) {
               // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.registrantTypeLabel.text =@"Inbound Hauler";
            }
            else if (self.transaction.outgoingUser) {
               // self.registrantTypeLabel.text = @"Hauler Taking";
                self.registrantTypeLabel.text =@"Outbound Hauler";
                
            }
        }
        else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
        {
            if (self.isIncomingUser) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.registrantTypeLabel.text =@"Outgoing Processor";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.registrantTypeLabel.text =@"Incoming Processor";
                
            }

        }
        else {
            self.registrantTypeLabel.text = registrant.registrantType.descriptionKey;
        }
        self.registrantIdHeaderLabel.text = [NSString stringWithFormat:@"%@ ID", registrant.registrantType.descriptionKey];
        self.registrantIdLabel.text = [NSString stringWithFormat:@"#%d", [registrant.registrationNumber intValue]];
        self.registrantBusinessNameLabel.text = registrant.businessName;
        self.registrantPhoneLabel.text = registrant.location.phone;
        self.registrantTypeImageView.image = [UIImage imageNamed:registrant.registrantType.fileName];
        self.registrantBusinessNameLabel.alpha=1;
        
        
        
        
        if (self.status==ON_SCREEN && self.isIncomingUser) {
            self.registrantIdHeaderLabel.alpha=1;
            self.registrantIdLabel.alpha=1;
            self.registrantBusinessNameLabel.frame = BUSINESS_NAME_ON_FRAME;
            [self.registrantBusinessNameLabel setFont:[UIFont fontWithName:@"OpenSans" size:26]];
            self.registrantBusinessNameLabel.textColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1];
            self.contactHeaderLabel.alpha=1;
            self.companyHeaderLabel.alpha=1;
            self.registrantPhoneLabel.alpha=1;
        }
        else {
            self.registrantIdHeaderLabel.alpha=0;
            self.registrantIdLabel.alpha=0;
            self.registrantBusinessNameLabel.frame = BUSINESS_NAME_OFF_FRAME;
            //            [self.registrantBusinessNameLabel setFont:[UIFont fontWithName:@"OpenSans" size:28]];
            /// For now keep it 26 so that there is no "jump"
            [self.registrantBusinessNameLabel setFont:[UIFont fontWithName:@"OpenSans" size:26]];
            self.registrantBusinessNameLabel.textColor = [UIColor colorWithRed:86/255.0 green:86/255.0 blue:86/255.0 alpha:1];
            self.contactHeaderLabel.alpha=0;
            self.companyHeaderLabel.alpha=0;
            self.registrantPhoneLabel.alpha=0;
        }
        
    } else {
        // set up ui for outgoing user case where scan is needed
        //self.scanButtonView.hidden = NO;
        self.tapToScanButton.hidden=NO;
        self.tocPlusImage.hidden=NO;
        self.toggleButton.hidden = YES;
         if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
        {
            if (self.isIncomingUser) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.registrantTypeLabel.text =@"Outgoing Processor";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.registrantTypeLabel.text =@"Incoming Processor";
                
            }
            
        }
else
{
        self.registrantTypeLabel.text = self.expectedRegistrantType.descriptionKey;
}
        self.rescanButton.hidden=YES;
        self.registrantIdLabel.alpha=0;
        self.companyHeaderLabel.alpha=0;
        self.registrantBusinessNameLabel.alpha=0;
        self.contactHeaderLabel.alpha=0;
        self.registrantPhoneLabel.alpha=0;
        self.registrantIdHeaderLabel.alpha=0;
    }
    [self validate];
    
    if (self.isIncomingUser==NO && self.transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_ERROR)
    {
    }
    else
    {
        [self updateValidateImage];
    }
    [self updateEditButton];
    
#if DEBUG
    if(self.isIncomingUser == NO)
    {
        self.tempLoginGesture.enabled = YES;
    }
#endif
}
- (void)updateEditButton {

	/// Until OTSJ-454 is approved, rescan will not be available.
	if(self.status == ON_SCREEN &&
	   self.isIncomingUser == FALSE &&
	   NO)
	{
		self.rescanButton.alpha = 1;
	}
	else
	{
		self.rescanButton.alpha = 0;
	}
}
- (IBAction)toggleButtonPressed:(id)sender {
    self.status = !self.status;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    
    self.toggleButton.selected = self.status;
    
    [self updateUIAnimated];
    // [self.parentPageViewController updateUI];
    
    
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR) {
        
        if (self.isIncomingUser) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR_INCOMING_USER];
        }
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        if([Utils getProcessorPtrOnly]==YES)
        {
            if (self.isIncomingUser) {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_YES_INCOMING_USER];
            }
        }
        else
        {
            if (self.isIncomingUser) {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PROESSOR_PTR_NO_INCOMING_USER];
            }
            
        }
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
        
        if (self.isIncomingUser) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT_INCOMING_USER];
        }
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR) {
        
        if (self.isIncomingUser) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR_INCOMING_USER];
        }
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_DOT) {
        
        if (self.isIncomingUser) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT_INCOMING_USER];
        }
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT) {
        
        if (self.isIncomingUser) {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PIT_INCOMING_USER];
        }
        
    }

    
    [self.parentPageViewController updateUI];

}

-(void)validate{
    self.valid = self.user != nil;  //self.isIncomingUser || self.transaction.outgoingUser;
   
}

#pragma mark - ParticipantComponentView

- (IBAction)tempLoginPressed:(id)sender {
    // Dennis--assign Jeff McHatter for a temp test;
    [self setUser:[[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:10932]]];
}

- (IBAction)scanButtonPressed:(id)sender {
	
    if(self.login == NULL)
    {
        self.login = [[LoginViewController alloc] init];
        
    }
    self.login.shouldEnableCameraOnAppear = YES;
    self.login.slideToLogin = YES;
    self.login.isSlideToLoginInvisible = YES;
    self.login.delegate = self;
    self.login.hideBackButton = false;
	
    UINavigationController* navigationController = self.parentPageViewController.navigationController;
    
    
    [navigationController pushViewController:self.login animated:YES];

}

-(void)setUser:(User *)user{
    _user = user;
    if(self.isIncomingUser){
        self.transaction.incomingUser = user;
        self.transaction.incomingRegistrant = user.registrant;
    } else {
        // TEMP: SEE if we can get away with skipping this check:
        self.transaction.outgoingUser = user;
        self.transaction.outgoingRegistrant = user.registrant;
        
         //this means a user was found using the QR Code. In this case we need to find the location
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
        

        self.status = OFF_SCREEN;
        [self saveLocation];
    }
    [self save];
    
    [self updateUI];
    
    // needed to add this call to get the company name label to appear (??):
   // [self.parentPageViewController updateUI];
    
    [self.parentPageViewController validate];
}

-(void)saveLocation {
    [Utils log:@"TOCParticipantComponentView.saveLocation"];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation=[locations lastObject];
    [Utils log:@"TOCParticipantComponentView.location: %@",[newLocation description]];
    GPSLog *gpsLog = (GPSLog*)[[DataEngine sharedInstance] newEntity:GPS_LOG_ENTITY_NAME];
    gpsLog.gpsLogId = [[NSUUID UUID] UUIDString];
    gpsLog.timestamp = [NSDate date];
    gpsLog.latitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    gpsLog.longitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    gpsLog.syncDate = nil;
    [[DataEngine sharedInstance] saveContext];
    self.transaction.outgoingGPSLog=gpsLog;
    [self save];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [Utils log:@"TOCParticipantComponentView.Location error:",[error localizedDescription]];
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - QRScanDelegate Methods

- (bool) validateUser: (User*) user {
    
    NSLog(@"%@",[Utils getRegistrantId]);
    NSLog(@"%@",user.registrant.registrationNumber);
    
    if ([user.registrant.registrationNumber  isEqualToNumber:[Utils getRegistrantId]]) {
        if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT) {
            [Utils showMessage:self.login
                        header:@"Invalid QR Code"
                       message:[NSString stringWithFormat:@"Error: %@",@"Different Processor Expected"]
                     hideDelay:2
                hideCompletion:^
             {
                 [self.login enableCamera];
             }];
            
        }
        else if (self.parentPageViewController.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
            [Utils showMessage:self.login
                        header:@"Invalid QR Code"
                       message:[NSString stringWithFormat:@"Error: %@",@"Different Hauler Expected"]
                     hideDelay:2
                hideCompletion:^
             {
                 [self.login enableCamera];
             }];
            
        }
        else
        {
            [Utils showMessage:self.login
                        header:@"Invalid QR Code"
                       message:[NSString stringWithFormat:@"Error: Expected %@",self.expectedRegistrantType.descriptionKey]
                     hideDelay:2
                hideCompletion:^
             {
                 [self.login enableCamera];
             }];
        }
        
        return false;
        
    }
    if (![self.expectedRegistrantType.registrantTypeId isEqualToNumber:user.registrant.registrantType.registrantTypeId])
    {
        [Utils showMessage:self.login
                    header:@"Invalid QR Code"
                   message:[NSString stringWithFormat:@"Error: Expected %@",self.expectedRegistrantType.descriptionKey]
                 hideDelay:2
            hideCompletion:^
         {
             [self.login enableCamera];
         }];
        
        return false;
    }
    
    Registrant *registrant = user.registrant;
    if ([registrant.isActive integerValue]==0)  // i.e. NO
    {
        //display an error
        // Dennis: was self.parentPageViewController
        [Utils showMessage:self.login
                    header:@"Warning"
                   message:@"Registrant is not active"
                 hideDelay:2
            hideCompletion:^{
                
                [self.login enableCamera];
                
            }];
        
        return false;
    }
    
    return true;
}

 - (void) proceed: (User *) user {

	[self setUser:user];
    
    if ([self.transaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_TCR]) {
        self.transaction.postalCode1 = [user.registrant.location.postalCode substringToIndex:3];
        self.transaction.postalCode2 = [user.registrant.location.postalCode substringFromIndex:3];
        [[DataEngine sharedInstance] saveContext];
    }
	
     /*[Utils showMessage:self.parentPageViewController header:@"QR CODE" message:@"Scan successful" hideDelay:1];
     [self dismissViewController];*/

     [Utils showMessage:self.parentPageViewController header:@"QR CODE" message:@"Scan successful" hideDelay:1 hideCompletion:^
	{
		[self dismissViewController];
	}];
	
}

- (void) backButtonTouchUpInside {

	[self dismissViewController];
}

#pragma mark - 

- (void)dismissViewController {
	
	
    UINavigationController* navigationController = self.parentPageViewController.navigationController;
    [navigationController popViewControllerAnimated:YES];
    /*
    
    UINavigationController* navigationController = self.parentPageViewController.navigationController.navigationController;
    [navigationController popViewControllerAnimated:YES];*/
}

@end
