//
//  TOCParticipantPopoverViewController.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-05-20.
//  Copyright (c) 2015 Capris. All rights reserved.
//
// TBD: check input values from user are complete
// NOTE: at dialog close time, the argument to proceed is a dummy user object
// set up with Registrant pointing to the Registrant created here and saved.
// or better: set up a base class protocol popoverDidComplete, popoverDidCancel
// and call these on the delegate at dialog close time

import Foundation
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}



class TOCParticipantPopoverViewController : TOCPopoverViewController, UITextFieldDelegate  {
     @IBOutlet weak var registrantBusinessNameTextField:UITextField!
     @IBOutlet weak var registrantPhoneTextField:UITextField!
     @IBOutlet weak var registrantAddressTextField:UITextField!
     @IBOutlet weak var registrantAddress2TextField:UITextField!
     @IBOutlet weak var registrantCityTextField:UITextField!
     @IBOutlet weak var registrantPostalTextField:UITextField!
     @IBOutlet weak var subView : UIView!
     @IBOutlet weak var subViewTitle : UIView!
    let locationManager = CLLocationManager()
    var locValue = CLLocationCoordinate2D()
    let regNumber : NSNumber = 9999999 // UCR_REGISTRATION_NUMBER from constants.h
    var prospectiveTextPhone = String()
    var prospectiveTextPostal = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // print(Int(rand()));
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.registrantBusinessNameTextField.becomeFirstResponder()
        
        subView.layer.cornerRadius = 20
        subView.layer.masksToBounds = true
        subView.layer.shadowColor = UIColor.black.cgColor
        subView.layer.shadowOpacity = 0.8
        subView.layer.shadowRadius = 3.0
        subView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        if (registrantBusinessNameTextField.text?.isEmpty) == false && (registrantPostalTextField.text?.isEmpty) == false && (registrantAddressTextField.text?.isEmpty) == false && (registrantCityTextField.text?.isEmpty) == false && (registrantPhoneTextField.text?.isEmpty) == false
        {
            doneButton.isEnabled=true
            doneButton.alpha = 1.0
        }
        else
        {
            doneButton.isEnabled=false
            doneButton.alpha = 0.5
        }        
    }
    
    func addManagedObject(_ dict : NSMutableDictionary, typeName:String) ->NSManagedObject? {
        var ok : Bool = false
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        let newEntity : NSManagedObject? = engineInstance.newEntity(typeName)
        if let entity : NSManagedObject = newEntity {
            ok = entity.setValuesWith(dict as! [AnyHashable: Any])
            let context : NSManagedObjectContext = engineInstance.managedObjectContext
            if (ok==false) {
                context.delete(entity)
                NSLog("Insert new object of type \(typeName) failed.")
            }
            else {
                return entity
            }
        }
        return nil
    }
 
    func locationManager(_ manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let userLocation:CLLocation = locations[0] as! CLLocation
        _ = userLocation.coordinate.longitude;
        _ = userLocation.coordinate.latitude;
        locValue = manager.location!.coordinate
       // print(locValue);
        
        //Do What ever you want with it
    }
    
    @IBAction override func doneButtonPressed(_ sender: AnyObject?) {
        super.doneButtonPressed(sender)
            let locDict : NSMutableDictionary = self.getLocationAsDictionary()
            let uuid : String = UUID().uuidString
            let defaults = UserDefaults.standard
            let metaTrans = defaults.string(forKey: "UCRTransaction")
            locDict.setValue(uuid, forKey: "locationId")
            if self.addManagedObject(locDict, typeName:LOCATION_ENTITY_NAME) != nil {
                
                
                let regDict : NSMutableDictionary = self.getRegistrantAsDictionary()
                regDict.setValue(uuid, forKey: "locationId")
                regDict.setValue(metaTrans, forKey:"metaData")
                if self.addManagedObject(regDict, typeName:REGISTRANT_ENTITY_NAME) != nil
                {
                    let userDict : NSMutableDictionary = self.getUserAsDictionary()
                    if let added : User = (self.addManagedObject(userDict, typeName:USER_ENTITY_NAME) as? User) {
                        if (self.delegate != nil)   {
                            if let view : TOCOutgoingComponentView = self.delegate as? TOCOutgoingComponentView{
                                let aSelector : Selector = #selector(TOCOutgoingComponentView.proceed(_:))
                                if view.responds(to: aSelector) {
                                    view.proceed(added)
                                }
                            }
                        }
                    }
                }
            }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
           // print("\(touch)")
            
        }
        super.touchesBegan(touches, with:event)
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
           // print("\(touch)")
            
        }
        super.touchesMoved(touches, with:event)
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {
            if touch.view == subView || touch.view == subViewTitle {
                
            }
            else
            {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
           // print("\(touch)")
            
        }
        super.touchesEnded(touches, with:event)
        
        
    }
    /*
     let userDefaults = NSUserDefaults.standardUserDefaults()
     userDefaults.setValue(highscore, forKey: "highscore")
     userDefaults.synchronize() // don't forget this!!!!
     
     
     */

 
    // MARK: - Helpers
    func getRegistrantAsDictionary ()->NSMutableDictionary {
        // why needed? constants.h in bridging header
        let REGISTRANT_TYPE_ID_COLLECTOR : NSNumber =  NSNumber(value: 1 as Int)
        
       // let metaTrans = defaults.stringForKey("UCRTransaction")
        let dict : NSMutableDictionary = NSMutableDictionary()
        
        dict.setValue(self.registrantBusinessNameTextField?.text, forKey: "businessName")
             dict.setValue(regNumber, forKey:"registrationNumber")
        // setTransactionnumber  as meta data
        //dict.setValue(, forKey:"metaData")
        
        
       // }
        dict.setValue(REGISTRANT_TYPE_ID_COLLECTOR, forKey:"registrantTypeId")
        
        return dict
    }
    
    func getUserAsDictionary ()->NSMutableDictionary {
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(1, forKey: "userId")
        dict.setValue(regNumber, forKey:"registrationNumber")
        return dict
    }
    
    func getLocationAsDictionary ()->NSMutableDictionary {
        let dict : NSMutableDictionary = NSMutableDictionary()
        
        if registrantAddress2TextField.text?.count == 0
        {
            if let fldString : NSString = self.registrantAddressTextField.text as NSString? {
                dict.setValue(fldString, forKey:"address1")
            }
            
        }
        
        else
        {
        if let fldString : NSString = self.registrantAddressTextField.text! + self.registrantAddress2TextField.text! as? NSString {
            dict.setValue(fldString, forKey:"address1")
        }
        }
        
        if let fldString : NSString = self.registrantPhoneTextField.text as NSString? {
            dict.setValue(fldString, forKey:"phone")
        }
        if let fldString : NSString = self.registrantPostalTextField.text as NSString? {
            dict.setValue(fldString, forKey:"postalCode")
        }
        if let fldString : NSString = self.registrantCityTextField.text as NSString? {
            dict.setValue(fldString, forKey:"city")
        }
        return dict
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField!) {    //delegate method
//        if ( registrantBusinessNameTextField.text?.characters.count >= 2 && registrantPostalTextField.text?.characters.count >= 5 && registrantAddressTextField.text?.characters.count >= 2 && registrantCityTextField.text?.characters.count >= 2 && registrantPhoneTextField.text?.characters.count >= 12)
//        {
//            doneButton.enabled=true
//            doneButton.alpha = 1.0
//        }
//        else
//        {
//            doneButton.enabled=false
//            doneButton.alpha = 0.5
//        }
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField!) -> Bool {  //delegate method
//        if ( registrantBusinessNameTextField.text?.characters.count >= 2 && registrantPostalTextField.text?.characters.count >= 5 && registrantAddressTextField.text?.characters.count >= 2 && registrantCityTextField.text?.characters.count >= 2 && registrantPhoneTextField.text?.characters.count >= 12)
//        {
//            doneButton.enabled=true
//            doneButton.alpha = 1.0
//        }
//        else
//        {
//            doneButton.enabled=false
//            doneButton.alpha = 0.5
//        }
        
       return true
        
    }
    

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
/*
        if textField == registrantPhoneTextField
        {
        
        let currentTextPhone = registrantPhoneTextField.text ?? ""
        let prospectiveTextPhone = (currentTextPhone as NSString).stringByReplacingCharactersInRange(range, withString: string)

        print(prospectiveTextPhone)
        if (prospectiveTextPhone.characters.count > 11)
        {
            doneButton.enabled=true
            doneButton.alpha = 1.0

        }
        else
        {
            doneButton.enabled=false
            doneButton.alpha = 0.5
        }
        
        }
        if textField == registrantPostalTextField
        {
            
            let currentTextPostal = registrantPostalTextField.text ?? ""
            let prospectiveTextPostal = (currentTextPostal as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            print(prospectiveTextPostal)
            if (prospectiveTextPostal.characters.count > 5)
            {
                doneButton.enabled=true
                doneButton.alpha = 1.0
                
            }
            else
            {
                doneButton.enabled=false
                doneButton.alpha = 0.5
            }
            
        }
*/
        
        if textField == registrantPhoneTextField
        {
            
            let currentTextPhone = registrantPhoneTextField.text ?? ""
            prospectiveTextPhone = (currentTextPhone as NSString).replacingCharacters(in: range, with: string)
            
        }
        
        if textField == registrantPostalTextField
        {
            
            let currentTextPostal = registrantPostalTextField.text ?? ""
            prospectiveTextPostal = (currentTextPostal as NSString).replacingCharacters(in: range, with: string)

        }
        
           // print(prospectiveTextPhone)
        
        //print(prospectiveTextPostal)
 
            if (prospectiveTextPhone.count > 11 && prospectiveTextPostal.count > 5 && registrantBusinessNameTextField.text?.count >= 2 && registrantPostalTextField.text?.count >= 5 && registrantAddressTextField.text?.count >= 2)
            {
                doneButton.isEnabled=true
                doneButton.alpha = 1.0
                
            }
            else
            {
                doneButton.isEnabled=false
                doneButton.alpha = 0.5
            }
        
        if textField == registrantPhoneTextField
        {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
       let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.count
        let decimalStr = decimalString as NSString
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
        {
            let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
            
            return (newLength > 10) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        textField.text = formattedString as String
        return false
    }

        if textField == registrantPostalTextField
        {
        
         let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
            
          //  print(newLength)
            switch textField
            {
            case registrantPostalTextField:
                if string == ""
                {
                    print("Backspace pressed");
                    return true;
                }
               if  string == "E" || string == "e"
                {
                    if newLength == 1 ||  newLength == 3||newLength == 5
                    {return true}
                    else{return false }
                
                }
                else
                {
                if string.isNumeric()
                {
                   if newLength == 1
                    {return false}
                    if newLength == 2
                    {return true}
                    if newLength == 3
                    {return false}
                    if newLength == 4
                    {return true}
                    if newLength == 5
                    {return false}
                    if newLength == 6
                    {return true}
                    //if newLength == 2 ||  newLength == 4||newLength == 6
                    //{return true}
                    
                }
                else
                    {
                    
                    let currentText = textField.text ?? ""
                    let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
                   //
                    if newLength == 1
                        
                    {return prospectiveText.doesNotContainCharactersIn("?@#$%^&*()<>,./|{}[]")}
                        
                    if newLength == 2
                        
                    {
                        return false}
                    if newLength == 3
                    {return prospectiveText.doesNotContainCharactersIn("?@#$%^&*()<>,./|{}[]")}
                    if newLength == 4
                    {return false}
                    if newLength == 5
                    {return prospectiveText.doesNotContainCharactersIn("?@#$%^&*()<>,./|{}[]")}
                    if newLength == 6
                    {return false}
                    }
                }
                
                default:
                return true
                
            }
         }
        
        if string.count == 0 {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        switch textField {
            
        case registrantBusinessNameTextField:
            return prospectiveText.doesNotContainCharactersIn("?@#$%^&*()<>,./|{}[]") &&
                prospectiveText.count <= 50
            
            // Allow only digits in this field,
            // and limit its contents to a maximum of 3 characters.
        case registrantAddressTextField:
            return prospectiveText.count <= 50
            
            // Allow only values that evaluate to proper numeric values in this field,
            // and limit its contents to a maximum of 7 characters.
        case registrantCityTextField:
            return prospectiveText.count <= 12
            
            // In this field, allow only values that evalulate to proper numeric values and
            // do not contain the "-" and "e" characters, nor the decimal separator character
            // for the current locale. Limit its contents to a maximum of 5 characters.
        case registrantPostalTextField:
            
            return
                prospectiveText.count <= 6
            
            // Do not put constraints on any other text field in this view
            // that uses this class as its delegate.
        default:
            return true
        }
        
    }
    
    
    
}

    // Dismiss the keyboard when the user taps the "Return" key or its equivalent
    // while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }


       
