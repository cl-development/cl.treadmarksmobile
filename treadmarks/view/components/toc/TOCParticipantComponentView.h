//
//  TOCParticipantComponentView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCComponentView.h"
#import "RegistrantType.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>

@interface TOCParticipantComponentView : TOCComponentView <CLLocationManagerDelegate>

@property (nonatomic, assign) BOOL isIncomingUser;
@property (nonatomic, strong) User *user;
@property (strong, nonatomic) RegistrantType *expectedRegistrantType;

- (id)initWithUser:(User *)user expectedRegistrantType:(RegistrantType *)expectedRegType;
-(void)setViewUI;
- (void) proceed: (User *) user;

@end
