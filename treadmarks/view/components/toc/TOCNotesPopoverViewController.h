//
//  TOCNotesPopoverViewController.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/21/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardNotificationDelegate.h"
#import "Comment.h"
#import "TOCNotesComponentView.h"
#import "UIView+CustomView.h"


@interface TOCNotesPopoverViewController : UIViewController <UITextViewDelegate>

@property (weak) UIViewController* containerViewController;
@property (weak) TOCNotesComponentView* componentView;
@property (weak) NSString* commentText;

@property (nonatomic, assign) NSUInteger type;

@end
