//
//  TransactionInformationComponentView.m
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TOCNotesComponentView.h"
#import "Transaction.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"
#import "OpenSansRegularTextView.h"
#import "DataEngine.h"
#import "OpenSansRegularButton.h"
#import "TOCNotesPopoverViewController.h"
//#import "DockViewController.h"
#import "PageViewController.h"
#import "Utils.h"
#import "TOCScaleTicketComponentView.h"
#import "TOCPageViewController.h"

#define TOC_NOTES_COMPONENT_HEIGHT_OFF 80
#define MIN_COMMENT_TEXTVIEW_HEIGHT 175
#define COMMENT_BOTTOM_MARGIN 45
#define COMMENT_TEXT_MARGIN 10
#define COMPONENT_VIEW_POSTION_IN_TCR 6
#define COMPONENT_VIEW_POSTION_IN_STC 6
#define COMPONENT_VIEW_POSTION_IN_PTR 6
#define COMPONENT_VIEW_POSTION_IN_HIT 5
#define COMPONENT_VIEW_POSTION_IN_UCR 6
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PTR 5
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_PTR 7
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_DOT 6
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_DOT 8
#define COMPONENT_VIEW_POSTION_IN_DOT 7

#define COMPONENT_VIEW_POSTION_IN_PIT 6

#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PIT 5
#define COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_PIT 7


@interface TOCNotesComponentView ()

@property (weak, nonatomic) IBOutlet OpenSansRegularTextView *commentLabel;
@property (weak, nonatomic) IBOutlet OpenSansRegularLabel *componentTitle;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIView *tapToInsertView;
@property (weak, nonatomic) IBOutlet OpenSansRegularButton *editButton;
@property (strong) Comment* comment;
@property (weak) UIViewController* popoverContainerViewController;
@property (weak, nonatomic) IBOutlet UIImageView *greyBarImageView;

@end

@implementation TOCNotesComponentView

//NSInteger status;

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_NOTES_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOff = TOC_NOTES_COMPONENT_HEIGHT_OFF;
    return self;
}

#pragma mark - ComponentView

-(CGFloat)heightOn {
	
	// Calculate the height of the comment box.
	float height = MIN(MIN_COMMENT_TEXTVIEW_HEIGHT,
					   [self.commentLabel.attributedText boundingRectWithSize:CGSizeMake(self.commentLabel.frame.size.width, CGFLOAT_MAX)
																	  options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
																	  context:NULL].size.height + COMMENT_TEXT_MARGIN);
	float componentHeight = self.commentLabel.frame.origin.y + height;
	componentHeight += DEFAULT_MARGIN;
	componentHeight += BR_IMAGE_HEIGHT;
    return componentHeight;
}
-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}
-(void)setViewUI
{
    self.status=OFF_SCREEN;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    
    self.toggleButton.selected = self.status;
    
}
-(void)updateUI{
    if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
        self.componentTitle.text = @"Weight Variance Comment";
        //self.editButton.hidden = YES;
        self.comment = [self commentOfType:COMMENT_TYPE_WEIGHT_VARIANCE_REASON];
    }
    else {
        self.comment = [self commentOfType:COMMENT_TYPE_DEFAULT];
    }
	
	self.toggleButton.selected = self.status;
	
	if(self.comment == NULL ||
	   self.comment.text == nil)
	{
		self.toggleButton.hidden = YES;
		self.tapToInsertView.hidden = NO;
		self.validateImageView.hidden = YES;
	}
	else
	{
		self.toggleButton.hidden = NO;
		self.tapToInsertView.hidden = YES;
        self.commentLabel.text = [self displayText];
		self.validateImageView.hidden = NO;
	}

	[self updateEditButton];
    
    if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON && self.shouldShowPopover==YES) {
        [self showPopover];
    }
}

- (IBAction)toggleButtonPressed:(id)sender {

    self.status = 1-self.status;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    
    self.toggleButton.selected = self.status;
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_TCR) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_STC)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
        {
            if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PTR];}
            else
            {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_PTR];
            }
            
        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PTR];
        }
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
        {
            if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_DOT];}
            else
            {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_DOT];
            }
            
        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
        }

        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
    {
        
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
        {
            if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_PIT];}
            else
            {
                [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_WEIGHT_VARIANCE_SECOND_PIT];
            }
            
        }
        else
        {
            [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PIT];
        }
        
    }

    [self.parentPageViewController updateUI];

}
- (void) updateEditButton {
	
	if (self.status == ON_SCREEN)
	{
		self.editButton.alpha = 1;
	}
	else
	{
		self.editButton.alpha = 0;
	}
}
- (IBAction)edit:(id)sender {
	
	[self showPopover];
}

- (void) showPopover {
    
    TOCNotesPopoverViewController* popover = [[TOCNotesPopoverViewController alloc] init];
    popover.componentView = self;
    if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
        popover.type=self.type;
    }
    
    if(self.comment)
    {
        popover.commentText = self.comment.text;
    }
    
    popover.containerViewController = self.parentPageViewController;
    
    self.popoverContainerViewController = popover;
    
    if (self.parentPageViewController == NULL)
    {
        @throw [NSException exceptionWithName:NSObjectNotAvailableException
                                       reason:@"parentPageViewController is not set."
                                     userInfo:NULL];
    }
    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    
    //UINavigationController *navigationController = self.parentPageViewController.dockViewController.navigationController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    popover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:popover animated:YES completion:nil];
    //[self.parentPageViewController presentViewController:self.popoverContainerViewController
    //'									animated:YES
    //								  completion:NULL];
}
/*
- (void) showPopover {
	
	TOCNotesPopoverViewController* popover = [[TOCNotesPopoverViewController alloc] init];
	popover.componentView = self;
    if (self.type == COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
        popover.type=self.type;
    }
	
	if(self.comment)
	{
		popover.commentText = self.comment.text;
	}
	
	popover.containerViewController = self.parentPageViewController;
	
	self.popoverContainerViewController = popover;
    //popover.view.backgroundColor=[UIColor clearColor];
   // popover.view.alpha =0.85;
    
	
	if (self.parentPageViewController == NULL)
	{
		@throw [NSException exceptionWithName:NSObjectNotAvailableException
									   reason:@"parentPageViewController is not set."
									 userInfo:NULL];
	}
	
    //self.parentPageViewController.view.backgroundColor =[UIColor clearColor];
    
	[self.parentPageViewController presentViewController:self.popoverContainerViewController
												animated:YES
											  completion:NULL];
}
 */


- (void) popoverDone: (NSString*) commentText {
	
    /// If comment entity exists but the text is cleared, we are going to delete the object
    /// Note: Popover controller is responsible for trimming whitespace.
    if(self.comment && [commentText isEqualToString:@""])
    {
        /// Set commentText to nil, which will be equivalent to comment record being removed
        commentText = nil;
        //TOCPageViewController *toc = (TOCPageViewController *)self.parentPageViewController;
        //[toc removeWeightCommentComponent];
        
    }
    /// If comment record does not exist, create one
    else if(self.comment == NULL && [commentText isEqualToString:@""] == NO)
    {
        Comment* comment = (Comment *)[[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
        comment.transaction = self.transaction;
        comment.commentId = [[NSUUID UUID] UUIDString];
        comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
        self.comment = comment;
    }
    
    /// If comment entity exists, then validate it and update UI
    if (self.comment)
    {
        /// Populate comment entity
        self.comment.createdDate = [NSDate date];
        self.comment.text = commentText;
        self.comment.syncDate = nil;
        self.comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
        self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
        [self save];
        
        //[self validate];
    }
    
    if (self.comment == nil || self.comment.text == nil)
    {
        self.status = OFF_SCREEN;
       
        
    }
    
    [self updateUI];
    
    [self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];
    
    self.commentLabel.text = [self displayText];
    
    [self.parentPageViewController updateUI];
    
    
    
    /* 1345 changes commented!
	/// If comment entity exists but the text is cleared, we are going to delete the object
	/// Note: Popover controller is responsible for trimming whitespace.
	if(self.comment && [commentText isEqualToString:@""])
	{
		/// Set commentText to nil, which will be equivalent to comment record being removed
		commentText = nil;

	}
	/// If comment record does not exist, create one
	else if(self.comment == NULL && [commentText isEqualToString:@""] == NO)
	{
		Comment* comment = (Comment *)[[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
        comment.transaction = self.transaction;
        comment.commentId = [[NSUUID UUID] UUIDString];
		comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
		self.comment = comment;
	}
	
	/// If comment entity exists, then validate it and update UI
	if (self.comment)
	{
		
        {
            //if comment object exists, but commentText value is empty or nil, then delete Comment object altogether
            if ([commentText length] == 0) {
                
              //  [[DataEngine sharedInstance] deleteComment:self.comment];
                
            }  else {
                
                /// Populate comment entity
                self.comment.createdDate = [NSDate date];
                self.comment.text = commentText;
                self.comment.syncDate = nil;
                self.comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
                self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
                
            }
            
            [self save];
            
            [self validate];
        }

        
        
        /// Populate comment entity
		self.comment.createdDate = [NSDate date];
		self.comment.text = commentText;
        self.comment.syncDate = nil;
        self.comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
        self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
		[self save];

		//[self validate];
	}
	
	if (self.comment == nil || self.comment.text == nil)
	{
		self.status = OFF_SCREEN;
	}
	
	[self updateUI];

	[self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];

	self.commentLabel.text = [self displayText];
	
	[self.parentPageViewController updateUI];*/
}
- (void) expand {
	
	self.status = ON_SCREEN;
	
	[self updateEditButton];
	
	self.tapToInsertView.hidden = YES;
	self.toggleButton.hidden = NO;
	self.toggleButton.selected = self.status;

	[self.parentPageViewController updateUI];
}

// if user cancels on first load of dialog, must remove the Weight comment record
- (void) cancel {
    
    Comment *weightVarianceComment = [self commentOfType:COMMENT_TYPE_WEIGHT_VARIANCE_REASON];
    if (weightVarianceComment != nil && [weightVarianceComment.text isEqualToString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
        [self removeWeightVarianceComment:weightVarianceComment];
    }

	[self.popoverContainerViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)tapToInsert:(UITapGestureRecognizer *)sender {
	
	[self showPopover];
}

- (BOOL)valid {
	
	return YES;
}


/*-(void)validate{
    self.valid = YES;
}*/

#pragma mark - Weight Variance helpers

-(Comment *) commentOfType:(NSUInteger) type {
    Comment *result = nil;
    NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
    if (comments != nil) {
        for (Comment *comment in comments) {
            if (type==COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
                    result = comment;
                    break;
                }
            }
            else if (type==COMMENT_TYPE_DEFAULT) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                    result = comment;
                    break;
                }
            }
        }
    }
    return result;
}

-(NSString *)displayText {
    return [self.comment.text stringByReplacingOccurrencesOfString:WEIGHT_VARIANCE_COMMENT_HEADER withString:@""];
}

-(void) removeWeightVarianceComment:(Comment *)comment {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    [context deleteObject:comment];
    [self save];
    // set trnsaction to unsync once variance is deleted
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    TOCPageViewController *toc = (TOCPageViewController *)self.parentPageViewController;
    [toc removeWeightVarianceCommentComponent];
}



@end
