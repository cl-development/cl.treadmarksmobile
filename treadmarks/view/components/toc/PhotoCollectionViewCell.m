//
//  PhotoCollectionViewCell.m
//  TreadMarks
//
//  Created by Dennis Christopher on 1/8/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "PhotoCollectionViewCell.h"

@implementation PhotoCollectionViewCell

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIView *bgView = [[UIView alloc] initWithFrame:self.backgroundView.frame];
        bgView.backgroundColor = [UIColor blueColor];
        bgView.layer.borderColor = [[UIColor whiteColor] CGColor];
        bgView.layer.borderWidth = 4;
        self.selectedBackgroundView = bgView;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    const CGRect bounds = self.bounds;
    self.imageView.frame = CGRectMake(20.0f, 40.0f, bounds.size.width - 40.0f, bounds.size.height - 60.0f);
    //self.imageView.frame = CGRectMake(20.0f, 20.0f, bounds.size.width - 40.0f, bounds.size.height - 30.0f);
}

-(void) setPhoto:(Photo *)thePhoto {
    if (_photo != thePhoto)
        _photo = thePhoto;

    NSMutableString *pathForPhoto = [NSMutableString stringWithString:[self documentsDirectory]];
    pathForPhoto = [[pathForPhoto stringByAppendingPathComponent:thePhoto.fileName] mutableCopy];
    NSString *pathForSmallPhoto = [self nameForSmallPhoto:pathForPhoto];
    
    //self.imageView.image = [UIImage imageWithContentsOfFile:pathForSmallPhoto];
    [self.imageView setImage: [UIImage imageWithContentsOfFile:pathForSmallPhoto]];
}

-(NSString *)documentsDirectory {
    return[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

-(NSString *)nameForSmallPhoto:(NSString*)photoName {
    NSString *result = [photoName stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
    //[Utils log:@"small photo name for %@ is %@",photoName,result];
    return result;
}


@end
