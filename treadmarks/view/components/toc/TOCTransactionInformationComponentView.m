//
//  TransactionInformationComponentView.m
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "TOCTransactionInformationComponentView.h"
#import "Transaction.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"
#import "PageViewController.h"

#define TOC_TRANSACTION_COMPONENT_HEIGHT_ON 185
#define TOC_TRANSACTION_COMPONENT_HEIGHT_OFF 80
#define COMPONENT_VIEW_POSTION_IN_TCR 0
#define COMPONENT_VIEW_POSTION_IN_STC 0
#define COMPONENT_VIEW_POSTION_IN_PTR 0
#define COMPONENT_VIEW_POSTION_IN_HIT 0
#define COMPONENT_VIEW_POSTION_IN_UCR 0
#define COMPONENT_VIEW_POSTION_IN_DOT 0
#define COMPONENT_VIEW_POSTION_IN_PIT 0

@interface TOCTransactionInformationComponentView ()

@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;

@end

@implementation TOCTransactionInformationComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_TRANSACTION_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_TRANSACTION_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_TRANSACTION_COMPONENT_HEIGHT_OFF;
    return self;
}

#pragma mark - ComponentView
-(void)setViewUI
{
    self.status=OFF_SCREEN;
    CGRect frame = self.view.frame, newFrame;
    self.toggleButton.selected = self.status;
    newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_TRANSACTION_COMPONENT_HEIGHT_OFF);
    
}


-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUI{
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIdLabel.text = [self.transaction.friendlyId stringValue];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:SHORT_DATE_FORMAT];
    self.transactionDateLabel.text = [dateFormatter stringFromDate:self.transaction.createdDate];
    CGRect frame = self.view.frame, newFrame;
    if (self.status==ON_SCREEN) {
        newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_TRANSACTION_COMPONENT_HEIGHT_ON);
        
    } else {
        newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_TRANSACTION_COMPONENT_HEIGHT_OFF);
    }
    
    self.view.frame = newFrame;
    
    [self validate];
}

- (IBAction)toggleButtonPressed:(id)sender {
    self.status = 1-self.status;
    self.toggleButton.selected = self.status;
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_TCR) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR];
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_STC)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PTR];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PIT];
        
    }
    
    [self.parentPageViewController updateUI];
    

}

@end
