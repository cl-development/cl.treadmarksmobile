//
//  TOCNotesPopoverViewController.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 1/21/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//
//  Dennis December 2014:
//  If this is a weight variance comment, we set the title to a special case text,
//  and assign that text as prefix to the actual entered comment

#import "TOCNotesPopoverViewController.h"
#import "Constants.h"
#import "KeyboardNotificationDelegate.h"
#import "Transaction.h"
#import "Utils.h"

@interface TOCNotesPopoverViewController () <NSFetchedResultsControllerDelegate>

#pragma mark IBOutlet properties

@property (weak, nonatomic) IBOutlet UITextView *commentsTextView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomMargin;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *commentsTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okayButtonTrailingSpaceContraint;


#pragma mark -

@end

@implementation TOCNotesPopoverViewController

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
	{
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:NULL];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:NULL];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeScreen:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeScreen:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
		
		self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
	
    return self;
}

- (void)closeScreen:(NSNotification*)notification {
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        //if blank variance delete  comment
        if([self.commentText isEqualToString:WEIGHT_VARIANCE_COMMENT_HEADER])
        {[self cancelAndDismiss];}
        else
        {[self dismissViewControllerAnimated:YES completion:nil];}
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
  /*
    if([self.commentText isEqualToString:WEIGHT_VARIANCE_COMMENT_HEADER])
    {[self cancelAndDismiss];}
    
    //else
    //{[self dismissViewControllerAnimated:YES completion:nil];}*/
    
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void) viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
}

- (void) viewDidLoad {
	
    [super viewDidLoad];
	
    
	//self.backgroundView.alpha = MODAL_BACKGROUND_ALPHA;
    [self.backgroundView createCornerRadius:self.backgroundView];
    
	
	[self dataBind];

	[self.commentsTextView becomeFirstResponder];
    if (self.type==YES) {
        self.commentsTitle.text = @"Weight Variance reason";//WEIGHT_VARIANCE_COMMENT_HEADER;
       
        self.doneButton.titleLabel.text=@"Confirm";
        
        [self.view layoutIfNeeded];
        self.commentsTextView.delegate = self;
        [self updateDoneButton:self.commentsTextView];
    }
	self.commentsTextView.selectedRange = NSMakeRange(0, 0);
}

- (void) didReceiveMemoryWarning {
	
    [super didReceiveMemoryWarning];
}

#pragma mark IBAction methods

- (IBAction) cancel: (UIButton *)sender {
    [self cancelAndDismiss];
}

- (IBAction) done: (UIButton *)sender {
	
	/// When entering data is done -- trim any white spaces
	NSString* comment = [self.commentsTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (self.type==YES) {
        NSString *fixedComment = [NSString stringWithFormat:@"%@%@", WEIGHT_VARIANCE_COMMENT_HEADER, comment];
        [self.componentView popoverDone:fixedComment];
    }
    else {
        [self.componentView popoverDone:comment];
    }
}

#pragma mark Keyboard notification methods

- (void) keyboardWillShow: (NSNotification*) notification {
	
	NSDictionary* userInfo = notification.userInfo;
	CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^{
						 self.containerViewBottomMargin.constant = keyboardFrame.size.height;
						 //[self.view layoutIfNeeded];
					 }];
}
- (void) keyboardWillHide: (NSNotification*) notification {
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^{
						 self.containerViewBottomMargin.constant = 0;
						 [self.view layoutIfNeeded];
					 }];
}

#pragma mark -

- (void) dataBind {

	if(self.commentText)
	{
		self.commentsTextView.text = [self displayText];
	}
}
- (void) cancelAndDismiss {
	
	[self.componentView cancel];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    UITextInputAssistantItem* item = [textView inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
}

-(void) textViewDidChange:(UITextView *)textView {
    
    UITextInputAssistantItem* item = [textView inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
    [self updateDoneButton:textView];
}

- (void)updateDoneButton:(UITextView *)textView {
    
    NSString* textViewText = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.doneButton.enabled = [textViewText length] >= 3;
}


#pragma mark helper methods
-(NSString *)displayText {
    return [self.commentText stringByReplacingOccurrencesOfString:WEIGHT_VARIANCE_COMMENT_HEADER withString:@""];
}


@end
