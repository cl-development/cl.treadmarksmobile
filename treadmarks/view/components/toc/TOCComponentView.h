//
//  TOCComponentView.h
//  TreadMarks
//
//  Created by Dragos Ionel on 12/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ComponentView.h"

@interface TOCComponentView : ComponentView

@property (nonatomic,weak) IBOutlet UIView  *view;
@property (nonatomic, assign) BOOL status;
@property (nonatomic) CGFloat heightOn;
@property (nonatomic) CGFloat heightOff;

-(CGFloat)height;

@end
