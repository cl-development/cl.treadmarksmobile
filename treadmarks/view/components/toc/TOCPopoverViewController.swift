//
//  TOCPopoverViewController.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-05-20.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation
import CoreLocation


class TOCPopoverViewController : UIViewController ,CLLocationManagerDelegate {
    @IBOutlet weak var doneButton:UIButton!
    @IBOutlet weak var cancelButton:UIButton!
    @objc var delegate : AnyObject?
    
    required init?(coder aDecoder: NSCoder) {
        self.delegate = nil
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    @IBAction func doneButtonPressed(_ sender: AnyObject?) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject?) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    func configureUI () {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
