//
//  TOCAuthorizationPopOverViewController.m
//  TreadMarks
//
//  Created by Capris Group on 2/12/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#define STCAUTHORIZATION_CHARACTER_INPUT @"0123456789"

#import "TOCAuthorizationPopOverViewController.h"
#import "Transaction+Transaction.h"
#import "Utils.h"
#import "Authorization+Authorization.h"

@interface TOCAuthorizationPopOverViewController ()
@property (strong, nonatomic) IBOutlet UITextField *eventNumberTextField;
@property (nonatomic, strong) Authorization *authorization;
@property (nonatomic, strong) STCAuthorization *stcAuthorization;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UIButton *backSpaceButton;
@property (nonatomic, strong)IBOutlet UIView *viewBackground;


@end

@implementation TOCAuthorizationPopOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.viewBackground createCornerRadius:self.viewBackground];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];

    
    // Do any additional setup after loading the view from its nib.
    self.eventNumberTextField.text = self.eventNumber;
    
    if ([self.eventNumberTextField.text length] == 5) {
        self.doneButton.enabled = YES;
    }  else {
        self.doneButton.enabled = NO;
    }
    
}
- (void)closeScreen:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
    [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.eventNumberTextField becomeFirstResponder];
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.eventNumberTextField setInputView:dummyView];
}

//ensure only alpha numeric characters are included here

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UITextInputAssistantItem* item = [textField inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	//    NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
	//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
	//
	//    if (newLength > 0) {
	//        self.doneButton.enabled = YES;
	//    }
	//    else self.doneButton.enabled = NO;
	//
	//    return (newLength > 45 && [string rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound) ? NO : YES;
    
    
    BOOL canEdit=NO;
    //NSCharacterSet *myCharSet = [NSCharacterSet alphanumericCharacterSet];
   // UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:@"Incorrect Input" message:@"The character you are trying to enter is not valid" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[alert addButtonWithTitle:@"Error"];
    //NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:STCAUTHORIZATION_CHARACTER_INPUT];
    //NSCharacterSet *myCharSet = [NSCharacterSet alphanumericCharacterSet];
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if (![myCharSet characterIsMember:c]) {
            //[alert show];
            return NO;
        }
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (newLength > 0) {
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                canEdit=NO;
                //[alert show];
                self.doneButton.enabled = NO;
            }
            else
            {
                canEdit=YES;
                self.doneButton.enabled = YES;
            }
        }
        
    }  else  self.doneButton.enabled = NO;
    
    return (newLength > 9 && canEdit) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return NO;
}

- (IBAction)doneAction:(UIButton *)sender {
    
    self.stcAuthorization = nil;
    self.authorization = nil;
    self.authorization = [[DataEngine sharedInstance] authorizationForTransaction:self.currentTransaction];
    NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setNumberStyle:NSNumberFormatterNoStyle];
    
    if (self.authorization!=nil) {
        
        self.stcAuthorization = [[DataEngine sharedInstance] stcAuthorizationForAuthorization:self.authorization];
        //NSLog(@"The authorization num is %@:", self.eventNumberTextField.text);
        self.authorization.authorizationNumber = self.eventNumberTextField.text;
        
    }  else {
        
//        NSTimeInterval today = [[NSDate date] timeIntervalSince1970];
//        NSString *intervalString = [NSString stringWithFormat:@"%f", today];
//        NSString *stringName = [intervalString stringByReplacingOccurrencesOfString:@"." withString:@""];
//        //NSLog(@"The id is: %@", stringName);
//        NSString *authId = [[NSUUID UUID] UUIDString];
//        //NSLog(@"The numformat ID is: %@", authId);
        
        self.authorization = [Authorization insertNewObjectForTransaction:self.currentTransaction];
        self.authorization.authorizationNumber = self.eventNumberTextField.text;
		
        self.stcAuthorization = (STCAuthorization *)[[DataEngine sharedInstance] newEntity:STCAUTHORIZATION_ENTITY_NAME];
    }
    
    self.authorization.syncDate = nil;
    self.stcAuthorization.authorization = self.authorization;
	[self.currentTransaction clearSyncStatus];
    [[DataEngine sharedInstance] saveContext];
	
    //NSLog(@"The authorization num is: %@", self.stcAuthorization.authorization.authorizationNumber);
    
    [self.authorizationDelegate eventAuthorizationComponentUpdate:self.eventNumberTextField.text];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)cancelAction:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backAction:(id)sender {
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"transaction voided" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)buttonPressed:(id)sender {
    
    UIButton *button = (UIButton *) sender;
    
    [[UIDevice currentDevice] playInputClick];
    
    if (button == self.backSpaceButton) {
        
        [self.eventNumberTextField deleteBackward];
        
    } else {
        //a digit was pressed
        
        if ([self.eventNumberTextField.text length] < MAX_TIRE_COUNT_DIGITS) {
            [self.eventNumberTextField replaceRange:self.eventNumberTextField.selectedTextRange withText:button.titleLabel.text];
        }
        
    }
    
    if ([self.eventNumberTextField.text length] == 5) {
        self.doneButton.enabled = YES;
    } else {
        self.doneButton.enabled = NO;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
