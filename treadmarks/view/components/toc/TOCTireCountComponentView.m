//
//  TOCTireCountComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 1/23/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCTireCountComponentView.h"
#import "Constants.h"
#import "DataEngine.h"
#import "Transaction_TireType.h"
#import "TireType.h"
#import "TireCountConfirmationTableCell.h"
#import "MaterialQuantityPageViewController.h"
#import "TOCScaleTicketComponentView.h"



#define TOC_TIRE_COUNT_COMPONENT_HEIGHT_ON 370
#define TOC_TIRE_COUNT_COMPONENT_HEIGHT_OFF 80

#define TOP_AREA_HEIGHT 60  //80
#define HEADER_HEIGHT   50

#define COMPONENT_VIEW_POSTION_IN_TCR 3
#define COMPONENT_VIEW_POSTION_IN_STC 4
#define COMPONENT_VIEW_POSTION_IN_HIT 3
#define COMPONENT_VIEW_POSTION_IN_PTR 3
#define COMPONENT_VIEW_POSTION_IN_UCR 3
#define COMPONENT_VIEW_POSTION_IN_DOT 3


@interface TOCTireCountComponentView ()

@property (nonatomic, strong) NSArray * transactionTireTypeArray;
@property (nonatomic) NSInteger tireTypeCount;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *tapToEnterPlusImage;
@property (weak, nonatomic) IBOutlet UIView *tapToEnterView;
@property (assign) BOOL userEnteredCounts;
@property (nonatomic, assign)int numEmpty;

@end

@implementation TOCTireCountComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_TIRE_COUNT_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_TIRE_COUNT_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_TIRE_COUNT_COMPONENT_HEIGHT_OFF;
    
    self.userEnteredCounts = NO;
    
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    
    [self addSubview:
    
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TireCountsEdited"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)setViewUI
{
    self.status=OFF_SCREEN;
    self.toggleButton.selected = self.status;
    [self updateUIAnimated];
    
}


-(void)updateUIAnimated{
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateUI];
                     }];
}

-(void)updateUI{
    if ( self.transactionTireTypeArray==nil) {
        self.transactionTireTypeArray = [[DataEngine sharedInstance] transactionTireTypeForTransactionId:self.transaction.transactionId];
        
    }
    self.tireTypeCount = [self.transactionTireTypeArray count];
    self.userEnteredCounts = [self userHasTireCounts];
    int rowHeight = [self.tableView rowHeight];
    /*self.frame = CGRectMake(self.frame.origin.x,
	 self.frame.origin.y,
	 self.frame.size.width,
	 TOP_AREA_HEIGHT+HEADER_HEIGHT+(rowHeight*self.tireTypeCount));
	 
	 self.heightOn  = self.frame.size.height;*/
    
    if (self.userEnteredCounts == NO) {
        self.status=OFF_SCREEN;
    }
    
    if (self.status==ON_SCREEN) {
        
        // TRY:
        self.frame = CGRectMake(self.frame.origin.x,
                                self.frame.origin.y,
                                self.frame.size.width,
                                TOP_AREA_HEIGHT+HEADER_HEIGHT+(rowHeight*self.tireTypeCount));
        
        self.heightOn  = self.frame.size.height;
        
        // end TRY
        self.toggleButton.alpha=1;
        self.tableView.alpha=1;
        self.editButton.alpha=1;
        self.tapToEnterView.alpha=0;
        self.tapToEnterPlusImage.alpha=0;
		
    } else {
        self.editButton.alpha=0;
        if (self.userEnteredCounts==NO) {
            self.toggleButton.alpha=0;
            self.tableView.alpha=0;
            self.tapToEnterView.alpha=1;
            self.tapToEnterPlusImage.alpha=1;
        }
        else {
            self.toggleButton.alpha=1;
            self.tableView.alpha=1;
            self.tapToEnterView.alpha=0;
            self.tapToEnterPlusImage.alpha=0;
        }
    }
    
    [self validate];
   //[[NSNotificationCenter defaultCenter] postNotificationName:@"TireCountsDone" object:nil];

}

/*-(CGFloat)height {
 //return self.view.frame.size.height;
 return (self.status==OFF_SCREEN) ? self.heightOff : self.heightOn;
 }*/
//
#pragma mark - TiewCount ComponentView

-(BOOL) userHasTireCounts {
    BOOL result = NO;
    for (Transaction_TireType *type in self.transactionTireTypeArray) {
        if (type.quantity != nil && [type.quantity integerValue]>=0) {
            result = YES;
            break;
        }
    }
    return result;
}


-(BOOL) userHasEmptyTireFields {
    BOOL result = NO;
    for (Transaction_TireType *type in self.transactionTireTypeArray) {
        if (type.quantity == nil) {
            result = YES;
            break;
        }
    }
    return result;
}

-(void) listenForTiresComplete {
   

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    [center addObserverForName:@"TireCountsComplete" object:nil
						 queue:mainQueue usingBlock:^(NSNotification *note) {
							 
							 [self stopListeningForTiresComplete];
                             //NSLog(@"Got Material Entry notification");
							 [self updateUIAnimated];
							 [self.tableView reloadData];
                             [self.parentPageViewController updateUI];
                           
                             // this is a hack to deal with case where user deletes all entries
                             if ([self userHasTireCounts]==NO) {
                                 self.toggleButton.selected = NO;
                             }
						 }];


}

-(void)validate{
    self.valid=YES;
    int totalTireCount = 0;
    for (Transaction_TireType *type in self.transactionTireTypeArray) {
        if (type.quantity == nil) {
            self.valid=NO;
            break;
        }  else {
            totalTireCount += [type.quantity intValue];
        }
    }
    
    if (totalTireCount > 0 && ![self userHasEmptyTireFields]) {
        self.valid=YES;
        
    }  else {
        self.valid=NO;
    }
    [self updateValidateImage];
    [self.parentPageViewController validate];
    

}

-(void) stopListeningForTiresComplete {
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TireCountsComplete" object:nil];
    
}


- (IBAction)toggleButtonPressed:(id)sender {
    self.status = !self.status;
    self.toggleButton.selected = self.status;
    [self updateUIAnimated];
    if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_TCR) {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_TCR];
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_STC)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_PTR];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_HIT];
        
    }else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_UCR];
        
    }
    else if (self.parentPageViewController.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_DOT];
        
    }

    [self.parentPageViewController updateUI];

}

- (IBAction)editButtonPressed:(id)sender {
    // refresh variance label activate
    

    [self listenForTiresComplete];
    MaterialQuantityPageViewController *tiresView = [[MaterialQuantityPageViewController alloc] init];
    [self.parentPageViewController.navigationController pushViewController:tiresView animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return  [self.transactionTireTypeArray count];
    return self.tireTypeCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    TireCountConfirmationTableCell *cell = (TireCountConfirmationTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[TireCountConfirmationTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.backgroundColor = [UIColor grayColor];
    }
    
    Transaction_TireType *crtTransactionTireType = self.transactionTireTypeArray[indexPath.row];
    NSString *tireTypeNameKey = crtTransactionTireType.tireType.nameKey;
    
    cell.tireNameLabel.text = tireTypeNameKey;
    
    cell.tireCountLabel.text = [crtTransactionTireType.quantity stringValue];
    
    return cell;
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}



@end
