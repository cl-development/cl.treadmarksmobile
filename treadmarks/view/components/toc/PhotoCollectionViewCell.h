//
//  PhotoCollectionViewCell.h
//  TreadMarks
//
//  Created by Dennis Christopher on 1/8/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Photo.h"

@interface PhotoCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) Photo *photo;

@end
