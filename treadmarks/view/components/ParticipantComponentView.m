//
//  ParticipantComponentView.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "ParticipantComponentView.h"
#import "User.h"
#import "Registrant.h"
#import "RegistrantType.h"
#import "DataEngine.h"
#import "Constants.h"
#import "Location.h"
#import "Utils.h"
#import "PageViewController.h"
#import "LoginViewController.h"

//#define PARTICIPANT_COMPONENT_WIDTH 640
#define PARTICIPANT_COMPONENT_HEIGHT 276

@interface ParticipantComponentView ()<QRScanDelegate>

@property (weak, nonatomic) IBOutlet UIView  *loginView;
@property (weak, nonatomic) IBOutlet UILabel *loginRegistrantTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *loginMessageLabel;
@property (weak, nonatomic) IBOutlet UIView *inactiveView;

@property (weak, nonatomic) IBOutlet UIView  *userView;
@property (weak, nonatomic) IBOutlet UILabel *registrantTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantBusinessNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantPhoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *registrantTypeImageView;
@property (nonatomic,strong) CLLocationManager *locationManager;

@property (strong,nonatomic) RegistrantType *expectedRegistrantType;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (strong) LoginViewController* login;

@end

@implementation ParticipantComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, PARTICIPANT_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
        _isIncomingUser=YES;
    }
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)setParentPageViewController:(PageViewController *)parentPageViewController{
    [super setParentPageViewController:parentPageViewController];
    if ([self.transaction.transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_TCR]) {
        self.expectedRegistrantType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_COLLECTOR];
    }
    
}

-(void)updateUI{
    CGRect insideRect  = CGRectMake(0, 0, COMPONENT_WIDTH, PARTICIPANT_COMPONENT_HEIGHT);
    CGRect outsideRect = CGRectMake(COMPONENT_WIDTH*2, 0, COMPONENT_WIDTH, PARTICIPANT_COMPONENT_HEIGHT);
    
    
    if(self.isIncomingUser || self.transaction.outgoingUser){
        
        User *user = self.isIncomingUser?self.transaction.incomingUser:self.transaction.outgoingUser;
        self.loginView.frame = outsideRect;
        self.userView.frame  = insideRect;
        
        //populate the values on the screen
        Registrant *registrant = user.registrant;
        if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
            if (self.isIncomingUser) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.registrantTypeLabel.text =@"Inbound Hauler";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.registrantTypeLabel.text =@"Outbound Hauler";
                
            }
        }
        else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT)
        {
            if (self.isIncomingUser) {
                // self.registrantTypeLabel.text = @"Hauler Transfering";
                self.registrantTypeLabel.text =@"Outgoing Processor";
            }
            else if (self.transaction.outgoingUser) {
                // self.registrantTypeLabel.text = @"Hauler Taking";
                self.registrantTypeLabel.text =@"Incoming Processor";
                
            }
            
        }
        else {
            self.registrantTypeLabel.text = registrant.registrantType.descriptionKey;
        }
        
        if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR) {
            
            if ([registrant.registrationNumber intValue] == [UCR_REGISTRATION_NUMBER integerValue]) {
                Registrant *UnReg = [[DataEngine sharedInstance] registrantIdFromMetaData:[self.transaction.friendlyId stringValue]];
                self.registrantIdLabel.text = @"Unregistered";
                // comapny name and phone
                self.registrantBusinessNameLabel.text = UnReg.businessName;
                
                self.registrantPhoneLabel.text = UnReg.location.phone;
            }
            else
            {
                self.registrantIdLabel.text = [NSString stringWithFormat:@"#%d", [registrant.registrationNumber intValue]];
                self.registrantBusinessNameLabel.text = registrant.businessName;
                
                self.registrantPhoneLabel.text = registrant.location.phone;

            }
            
        }
        else
        {
            self.registrantIdLabel.text = [NSString stringWithFormat:@"#%d", [registrant.registrationNumber intValue]];
        

        //self.registrantTypeLabel.text = registrant.registrantType.descriptionKey;
        self.registrantIdHeaderLabel.text = [NSString stringWithFormat:@"%@ ID", registrant.registrantType.descriptionKey];
        //self.registrantIdLabel.text = [NSString stringWithFormat:@"#%d", [registrant.registrationNumber intValue]];
        self.registrantBusinessNameLabel.text = registrant.businessName;
        
        self.registrantPhoneLabel.text = registrant.location.phone;
            
        }
        self.registrantTypeImageView.image = [UIImage imageNamed:registrant.registrantType.fileName];
    } else {
        self.loginView.frame = insideRect;
        self.userView.frame  = outsideRect;
        
        self.loginRegistrantTypeLabel.text = self.expectedRegistrantType.descriptionKey;
        self.loginMessageLabel.text = [NSString stringWithFormat:@"%@ badge scan required", self.expectedRegistrantType.descriptionKey];
        self.registrantTypeImageView.image = [UIImage imageNamed:self.expectedRegistrantType.fileName];
        #ifdef DEBUG
        self.tapGestureRecognizer.enabled = true;
#endif
    }
    
    [self validate];
}

-(void)validate{
    self.valid = self.isIncomingUser || self.transaction.outgoingUser;
    
           [self updateValidateImage];
    
}

#pragma mark - ParticipantComponentView

- (void) setUser: (User *) user {
	
    if(self.isIncomingUser)
	{
        self.transaction.incomingUser = user;
    }
	else
	{
		self.transaction.outgoingUser = user;

        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        [self.locationManager startUpdatingLocation];
        

        [self saveLocation];
    }
	
    [self save];
    [self updateUI];
    [self.parentPageViewController validate];
}

-(void)saveLocation {
    [Utils log:@"ParticipantComponentView.saveLocation"];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *newLocation=[locations lastObject];
    [Utils log:@"ParticipantComponentView.location: %@",[newLocation description]];
    GPSLog *gpsLog = (GPSLog*)[[DataEngine sharedInstance] newEntity:GPS_LOG_ENTITY_NAME];
    gpsLog.gpsLogId = [[NSUUID UUID] UUIDString];
    gpsLog.timestamp = [NSDate date];
    gpsLog.latitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    gpsLog.longitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    gpsLog.syncDate = nil;
    [[DataEngine sharedInstance] saveContext];
    self.transaction.outgoingGPSLog=gpsLog;
    [self save];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [Utils log:@"ParticipantComponentView.Location error:",[error localizedDescription]];
    [self.locationManager stopUpdatingLocation];
}

// NOT USED
- (IBAction)scanButtonPressed:(id)sender
{
   /* if(self.login == NULL)
    {
        self.login = [[LoginViewController alloc] init];
        self.login.slideToLogin = false;
        self.login.delegate = self;
        self.login.hideBackButton = false;
    }
    
    [self.parentPageViewController presentViewController:self.login animated:true completion:NULL];
 */
}

- (IBAction)tempLoginPressed:(id)sender {
    // Dennis--adapt to CSV data [self setUser:[[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]]];
    [self setUser:[[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:10932]]];
}

#pragma mark - QRScanDelegate Methods

- (bool) validateUser: (User*) user {
	
	if (![self.expectedRegistrantType.registrantTypeId isEqualToNumber:user.registrant.registrantType.registrantTypeId])
	{
		//display an error
		[Utils showMessage:self.parentPageViewController.presentedViewController
					header:@"Invalid QR Code"
				   message:[NSString stringWithFormat:@"Error: Expected %@",self.expectedRegistrantType.descriptionKey]];
		
		return false;
	}
	
	return true;
}
- (void) proceed: (User *) user {

    [self setUser:user];

	[self.parentPageViewController dismissViewControllerAnimated:true completion:NULL];
}
- (void) backButtonTouchUpInside {
    [self.parentPageViewController dismissViewControllerAnimated:true completion:NULL];
}

@end
