//
//  PhotoComponentView.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-10-29.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "PhotoComponentView.h"
#import "AppDelegate.h"
#import "Transaction.h"
#import "Utils.h"
#import "Constants.h"
#import "UIImage+Resize.h"
#import "PageViewController.h"

#define PHOTO_COMPONENT_HEIGHT 340


@interface PhotoComponentView ()
@property (nonatomic, weak) IBOutlet UILabel *commentLabel;
@property (nonatomic, weak) IBOutlet UITextView *commentTextView;
@property (nonatomic, weak) IBOutlet UIImageView *photoImageView;
@property (nonatomic, weak) IBOutlet UIButton *addMoreButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@property (nonatomic, weak) IBOutlet UIButton *addPhotoButton;
@property (nonatomic, weak) IBOutlet UILabel *applicableLabel;
@property (nonatomic, weak) IBOutlet UILabel *useCameraLabel;

-(IBAction) takeAPhoto:(id)sender;
@end

@implementation PhotoComponentView

- (id)initWithPhoto:(Photo *)photo {
    self.photo = photo;
    return [self initWithFrame:CGRectNull];
}


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, PHOTO_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
        //[self setUpUI];
    }
    
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

#pragma mark - IBActions

-(IBAction) takeAPhoto:(id)sender {
    BOOL okay = [self startCameraControllerFromViewController: self.parentPageViewController
												usingDelegate: self];
    if (okay==NO) {
        [Utils showMessage:self.parentPageViewController header:@"Error" message:@"Camera not available" hideDelay:1];
        [self updateUI];
    }
}

#pragma mark - ImagePickerDelegate

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)aPicker{
    [self.parentPageViewController dismissViewControllerAnimated:NO completion:nil];
}


- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = delegate;
    
    [controller presentViewController:cameraUI animated:NO completion:nil];
    
    return YES;
}

-(UIImage *) changeImageOrientation:(UIImage *)theImage {
    CGSize newSize = CGSizeMake(theImage.size.height, theImage.size.width);
    UIImage *resized = [theImage resizedImage:newSize interpolationQuality:kCGInterpolationHigh];
    return resized;
}

-(IBAction)addMoreButtonPressed:(id)sender {
    NSNotification *notification = [NSNotification notificationWithName:@"Component Changed" object:self];
    
    if([[self delegate] respondsToSelector:@selector(didAddComponent:)]) {
        [[self delegate] didAddComponent:notification];
    }
}

-(IBAction)deletePhotoButtonPressed:(id)sender {
    [self deletePhotoObject];
    
    NSNotification *notification = [NSNotification notificationWithName:@"Component Changed" object:self];
    
    if([[self delegate] respondsToSelector:@selector(didRemoveComponent:)]) {
        [[self delegate] didRemoveComponent:notification];
    }
}

#pragma mark -  UITextFieldDelegate, UITextViewDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView == self.commentTextView) {
        self.photo.comments = textView.text;
        [self save];
    }
}

/*-(void) save {
 [self.dataEngine saveContext];
 }*/

#pragma mark - UI Support, Utitlity

-(void) updateUI {
    self.dataEngine = [DataEngine sharedInstance];
    if (self.photo != Nil) {
        // assign the image view by getting the file at location
        Photo *thePhoto = self.photo;
        if (thePhoto != nil) {
            [self setPhotoImageForPhoto:thePhoto];
        }
        // fill in the text fields
        self.addPhotoButton.hidden=YES;
        self.commentTextView.text = thePhoto.comments;
        self.addMoreButton.hidden= NO;
        self.applicableLabel.hidden = YES;
        self.useCameraLabel.hidden = YES;
        self.commentLabel.hidden = NO;
        self.commentTextView.hidden = NO;
    }
    else {
        self.addMoreButton.hidden=YES;
        //self.deleteButton.hidden=YES;
        self.addPhotoButton.hidden=NO;
        //self.addPhotoButton.enabled=YES;
        self.applicableLabel.hidden = NO;
        self.useCameraLabel.hidden = NO;
        self.commentLabel.hidden = YES;
        self.commentTextView.hidden = YES;
    }
    if (self.readOnly==YES) {
        self.addPhotoButton.enabled=NO;
        self.commentTextView.editable=NO;
        //self.addMoreButton.enabled=NO;
        self.addMoreButton.hidden=YES;
        //self.deleteButton.enabled=NO;
        self.deleteButton.hidden=YES;
    }
	
    [self validate];
}


-(void) updateComponentCanAddStatus:(BOOL)canAdd {
    if (self.photo==nil) {
        self.addMoreButton.hidden=YES;
    }
    else {
        self.addMoreButton.hidden=!canAdd;
    }
}

// TBD: Utiility
-(NSString *) dateToString:(NSDate *)theDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    return[formatter stringFromDate:theDate];
}

- (void)setPhotoImage {

    NSString *pathForPhoto = [self pathForPhoto];
    NSString *pathForSmallPhoto = [self nameForSmallPhoto:pathForPhoto];
	
    self.photoImageView.hidden=NO;
    self.addPhotoButton.hidden=YES;
    self.photoImageView.image = [UIImage imageWithContentsOfFile:pathForSmallPhoto];
}

-(void) setPhotoImageForPhoto:(Photo *)thePhoto {
	
    self.photoImageView.hidden=NO;
    self.addPhotoButton.hidden=YES;
    self.photoImageView.image = [Utils findSmallImage:thePhoto.fileName];
}

#pragma mark - ImagePickerDelegate support


-(NSString *)nameForPhoto {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    df.timeStyle = NSDateFormatterShortStyle;
	NSString *nameFromDate = [df stringFromDate:[NSDate date]];
    
    return [NSString stringWithFormat:@"%@.png", nameFromDate];
}

-(NSString *)nameForSmallPhoto:(NSString*)photoName {
    NSString *result = [photoName stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
    [Utils log:@"small photo name for %@ is %@",photoName,result];
    return result;
}

-(NSString *)documentsDirectory {
    return[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
}

-(void) deletePhotoObject {
    if (self.photo != nil) {
        [self.dataEngine deletePhoto:self.photo];
        //[self deletePhotoFile];
    }
}

-(void) deletePhotoFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
	
    NSMutableString *myString = [NSMutableString stringWithString:[self documentsDirectory]];
    //  WHY does this fail??
    myString = [[myString stringByAppendingPathComponent:self.photo.fileName] mutableCopy];
	
    if ([fileManager removeItemAtPath:myString error:&error]==NO) {
        //NSLog(@"Delete file error: %@", [error localizedDescription]);
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
        if(detailedErrors != nil && [detailedErrors count] > 0)
		{
			/*
            for(NSError* detailedError in detailedErrors)
			{
                //NSLog(@"  DetailedError: %@", [detailedError userInfo]);
            }
			 */
        }
        else {
            //NSLog(@"  %@", [error userInfo]);
        }
    }
}

-(NSMutableString *)pathForPhoto {
    NSMutableString *myString = [NSMutableString stringWithString:[self documentsDirectory]];
    NSString *photoName = [self nameForPhoto];
    if (self.photo==nil) {
        myString = [[myString stringByAppendingPathComponent:photoName] mutableCopy];
    }
    else {
        myString = [[myString stringByAppendingPathComponent:self.photo.fileName] mutableCopy];
    }
    return myString;
}

- (UIImage*)resizeImageWithImage:(UIImage*)image {
	CGSize newSize;
	newSize.height = image.size.height/4;
	newSize.width = image.size.width/4;
	
	UIGraphicsBeginImageContext( newSize );
	[image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return newImage;
}

#pragma mark - Other

-(void) AttachPhotoViewDidComplete:(NSNotification *)notification {
    // need to save the photo transaction link here:
    self.photo.transaction = self.parentPageViewController.transaction;
    //[self setUpUI];
    [self updateUI];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)validate{
    self.valid=YES;
    /* The photo is optional for JTH release.
     self.valid = self.photoImageView.image != nil;
	 [self updateValidateImage];
	 [self.parentPageViewController validate];*/
}


@end
