//
//  TireCountComponentNewView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//  April 16, 2014: syncing: syncDate sent to nil for tire counts

#import "TireCountComponentView.h"
#import "Transaction.h"
#import "TransactionType.h"
#import "Constants.h"
#import "DataEngine.h"
#import "TireCountTableCell.h"
#import "Transaction_TireType.h"
#import "TireType.h"
#import "Utils.h"
#import "TOCTireCountComponentView.h"
#import "PageViewController.h"
#import "TOCScaleTicketComponentView.h"

#define TRANSACTION_COUNT_COMPONENT_WIDTH 695
#define TRANSACTION_COUNT_COMPONENT_HEIGHT 936

//assign input fields tags over 50
#define INPUT_FIELDS_TAG_DELTA 50

@interface TireCountComponentView ()

//table view for the tire types
@property (strong, nonatomic) IBOutlet UITableView *tireTableView;

@property (strong, nonatomic) IBOutlet UIImageView *rullerImageView;
@property (nonatomic, strong) UITableViewCell *selectedCell;
@property (nonatomic, assign) CGPoint startLocationPoint;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) TireCountTableCell *cell;
@property (nonatomic, strong) UILabel *rulerLabel;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (nonatomic, assign) BOOL isTextFieldSelected;
@property (nonatomic, assign) BOOL isAnimating;
@property (strong, nonatomic) IBOutlet UIView *keypadView;
@property (nonatomic, strong) NSMutableDictionary *dict;

@property (nonatomic,strong) NSArray * transactionTireTypeArray;
@property (nonatomic) NSInteger tireTypeCount;
@property (weak, nonatomic) IBOutlet UIButton *backspaceButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;

@end

@implementation TireCountComponentView

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, TRANSACTION_COUNT_COMPONENT_WIDTH, TRANSACTION_COUNT_COMPONENT_HEIGHT)];
    if (self) {
        [self connectNib];
    }
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUI{
    
    self.transactionTireTypeArray = [[DataEngine sharedInstance] transactionTireTypeForTransactionId:self.transaction.transactionId];
    self.tireTypeCount = [self.transactionTireTypeArray count];
    
    self.doneButton.enabled = NO;
    self.rullerImageView.alpha = 0;
    
    NSString *tireTypeNameKey;
    if ([self.transactionTireTypeArray count]>0) {
        
        Transaction_TireType *crtTransactionTireType = self.transactionTireTypeArray[self.indexPath.row];
         tireTypeNameKey= crtTransactionTireType.tireType.nameKey;
    }
    

    self.rulerLabel = [[UILabel alloc] initWithFrame:CGRectMake(158, 15, 450, 40)];
    [self.rulerLabel setNumberOfLines:1];
    [self.rulerLabel setText:tireTypeNameKey];
    [self.rulerLabel setBackgroundColor:[UIColor clearColor]];
    [self.rulerLabel setTextColor:[UIColor whiteColor]];
    [self.rulerLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:24]];
    
    [self.rullerImageView addSubview:self.rulerLabel];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
    panGesture.maximumNumberOfTouches = 1;
    panGesture.minimumNumberOfTouches = 1;
    panGesture.delegate = self;
    [self.rullerImageView addGestureRecognizer:panGesture];
    
    [self.tireTableView setScrollEnabled:NO];
    self.tireTableView.alwaysBounceVertical = NO;
    
     [self validate];
    
}

#pragma mark - TireCountComponentView

#define RULER_ANIMATION 1
-(void)setShowRuler:(BOOL)showRuler{
    if (showRuler==_showRuler) {
        //nothing to do
        return;
    }
    _showRuler = showRuler;
    [UIView animateWithDuration:RULER_ANIMATION animations:^{
        self.rullerImageView.alpha = showRuler;
    }];
    if (showRuler) {
        NSIndexPath *firstCellPath = [NSIndexPath indexPathForRow:0 inSection:0];
        TireCountTableCell *myCell = (TireCountTableCell *)[self.tireTableView cellForRowAtIndexPath:firstCellPath];
        [self.rulerLabel setText:myCell.tireNameLabel.text];
        [myCell.tireCountField becomeFirstResponder];
        [self moveRulerToRow:0];
    }
}

#pragma mark - UITaleViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [self.transactionTireTypeArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    static NSString *cellIdentifier = @"Cell";
    
    self.cell = (TireCountTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (self.cell == nil) {
        self.cell = [[TireCountTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Transaction_TireType *crtTransactionTireType = self.transactionTireTypeArray[indexPath.row];
    NSString *tireTypeNameKey = crtTransactionTireType.tireType.nameKey;

    self.cell.tireNameLabel.text = tireTypeNameKey;
    
    self.cell.tireCountField.tag = indexPath.row + INPUT_FIELDS_TAG_DELTA;
    self.cell.tireCountField.delegate = self;
    self.cell.tireCountField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.cell.tireCountField setTextAlignment:NSTextAlignmentRight];
    [self.cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    self.cell.tireCountField.text = [crtTransactionTireType.quantity stringValue];
    self.selectedCell = self.cell;
    
    return self.cell;
}

#define ANIMATION_DURATION 0.3

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[Utils log:@"Table row %d has been tapped", indexPath.row];
    
    if (self.isAnimating) {
        return;
    }
    self.isAnimating = YES;
    
    CGRect rect = [self.view convertRect:[tableView rectForRowAtIndexPath:indexPath] fromView:tableView];
    CGFloat floatx = self.rullerImageView.frame.origin.x - rect.origin.x;
    self.cell = (TireCountTableCell *)[self.tireTableView cellForRowAtIndexPath:indexPath];

    //first hide the text on the ruller
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.rulerLabel.alpha=0;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:ANIMATION_DURATION
                                          animations:^{
                                              self.rullerImageView.frame = CGRectMake(rect.origin.x + floatx, rect.origin.y, self.rullerImageView.frame.size.width, self.rullerImageView.frame.size.height);                                          }
                                          completion:^(BOOL finished) {
                                              [self.rulerLabel setText:self.cell.tireNameLabel.text];
                                              self.rulerLabel.alpha=1;
                                              self.isAnimating = NO;
                                          }];
                     }];
    
     [self.cell.tireCountField performSelector:@selector(becomeFirstResponder) withObject:self afterDelay:0.25 ];

 }

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer {
    
    self.startLocationPoint = [recognizer locationInView:self.rullerImageView];
    //NSLog(@"The point is: %@", NSStringFromCGPoint(self.startLocationPoint));
    
    NSIndexPath *indexPath;
    UITableViewCell *cell;
    
    //int selectedRow = [self.tireTableView indexPathForSelectedRow].row;
    int rowHeight = [self.tireTableView rowHeight];
    
    CGPoint newCenter  = self.rullerImageView.center;
    newCenter.y = [recognizer locationInView:[self.rullerImageView superview]].y;
    
    //this is what limits the movement of the slider above the table
    if (newCenter.y - self.rullerImageView.frame.size.height / 2 < self.tireTableView.frame.origin.y) {
        
        newCenter.y = self.tireTableView.frame.origin.y + self.rullerImageView.frame.size.height / 2;

    } else if (newCenter.y + self.rullerImageView.frame.size.height / 2 > self.tireTableView.frame.origin.y + self.tireTableView.frame.size.height) {
        
        newCenter.y = self.tireTableView.frame.origin.y + self.tireTableView.frame.size.height - self.rullerImageView.frame.size.height / 2;
        
    } else if (newCenter.y - self.rullerImageView.frame.size.height / 2 > self.tireTableView.frame.origin.y + self.tireTypeCount * rowHeight) {
        
        newCenter.y = self.tireTableView.frame.origin.y + self.tireTypeCount * rowHeight + self.rullerImageView.frame.size.height / 2;

    }
    
    self.rullerImageView.center = newCenter;
    
    if ([recognizer state] == UIGestureRecognizerStateChanged) {
        
        //this is what gets called when the slider moves above the first row
        if (newCenter.y == self.tireTableView.frame.origin.y + self.rullerImageView.frame.size.height / 2) {

            CGPoint topPoint = [recognizer locationInView:self.tireTableView];
            NSIndexPath *nextRow = [self.tireTableView indexPathForRowAtPoint:topPoint];
            NSInteger currentRow = nextRow.row;
            
            if (currentRow != 0) {
                
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [self.tireTableView selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
            }
        } else if (newCenter.y == self.tireTableView.frame.origin.y + self.tireTableView.frame.size.height - self.rullerImageView.frame.size.height / 2) {
            
            CGPoint bottomPoint = [recognizer locationInView:self.tireTableView];
            NSIndexPath *nextRow = [self.tireTableView indexPathForRowAtPoint:bottomPoint];
            NSInteger currentRow = nextRow.row;
            
            if (currentRow != self.tireTypeCount-1) {
                
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [self.tireTableView selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
                
            }
            
        } else if (newCenter.y > self.tireTableView.frame.origin.y + self.tireTableView.frame.size.height) {
            
            CGPoint bottomPoint = [recognizer locationInView:self.tireTableView];
            NSIndexPath *nextRow = [self.tireTableView indexPathForRowAtPoint:bottomPoint];
            NSInteger currentRow = nextRow.row;
            
            if (currentRow != self.tireTypeCount-1) {
                
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [self.tireTableView selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
                
            }
            
            
        }
        
        [self.rulerLabel setText:@""];
        
        
    //Once user has stopped moving the UIImageView/scrollbar, if the UIImageView/scrollbar is in between two rows, link to the row that the UIImageView/scrollbar is closest to.
    } else if ([recognizer state] == UIGestureRecognizerStateEnded) {
        
        CGPoint touchPoint = [recognizer locationInView:self.tireTableView];

        //Be sure the point is inside the table
        touchPoint = CGPointMake(1, touchPoint.y);
        
        [Utils log:@"end touch point = %@", NSStringFromCGPoint(touchPoint)];
        
        indexPath = [self.tireTableView indexPathForRowAtPoint:touchPoint];
        
        if(indexPath == nil) {
            
            if(touchPoint.y < 0) {
            
                indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
            } else if (touchPoint.y > self.tireTypeCount * rowHeight) {
                
                indexPath = [NSIndexPath indexPathForRow:self.tireTypeCount-1 inSection:0];
                
            } else {

                indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            
        }
        
        cell = [self.tireTableView cellForRowAtIndexPath:indexPath];
        
        [UIView animateWithDuration:.3 animations:^{
            [self.tireTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
            CGRect rect = [self.view convertRect:[self.tireTableView rectForRowAtIndexPath:indexPath] fromView:self.tireTableView];
            
            CGFloat floatx = self.rullerImageView.frame.origin.x - rect.origin.x;
            self.rullerImageView.frame = CGRectMake(rect.origin.x + floatx, rect.origin.y, self.rullerImageView.frame.size.width, self.rullerImageView.frame.size.height);
        }];
        
        self.cell = (TireCountTableCell *)[self.tireTableView cellForRowAtIndexPath:indexPath];
        [self.rulerLabel setText:self.cell.tireNameLabel.text];
        [self.cell.tireCountField becomeFirstResponder];
        
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField.tag >= INPUT_FIELDS_TAG_DELTA) {
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else
        return YES;
}

//check if all the inputs are entered. If yes, enable the Done button
-(void)validate {
    
    NSInteger rows =  [self.tireTableView numberOfRowsInSection:0];
    int totalTireCount = 0;
    BOOL isValid = YES;
    
    for (int row = 0; row < rows; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        TireCountTableCell *cell = (TireCountTableCell *)[self.tireTableView cellForRowAtIndexPath:indexPath];
        totalTireCount += [cell.tireCountField.text intValue];
        NSString *text = cell.tireCountField.text;
        
        isValid = isValid && ([text length]>0);
    }
    
    if (isValid && totalTireCount == 0) {
        self.doneButton.enabled = YES;
        self.valid = NO;
    }  else {
        self.valid = self.doneButton.enabled = isValid;
    }
    
    [self updateValidateImage];
    [self.parentPageViewController validate];
   //
}

//check if all the inputs are entered. If yes, enable the Done button
-(void)saveInputs {
    [Utils log:@"saveInputs"];
    NSInteger rows =  [self.tireTableView numberOfRowsInSection:0];
    
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    
    for (int row = 0; row < rows; row++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        TireCountTableCell *cell = (TireCountTableCell *)[self.tireTableView cellForRowAtIndexPath:indexPath];
        
        NSString *text   = cell.tireCountField.text;
        Transaction_TireType *transactionTireType = self.transactionTireTypeArray[row];
        
        if ([text length]==0) {
            [Utils log:@"saveInputs: Empty row=%d empty (%@)",row, cell.textLabel];
            transactionTireType.quantity = nil;
        }
        else {
            transactionTireType.quantity = [NSNumber numberWithInteger:[text integerValue]];
        }
        //[Utils log:@"row: %d, value: %@",row,text];
        transactionTireType.syncDate = nil;
        [self save];
    }
   
    // let TOC Component know tires complete:
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TireCountsComplete" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TireCountsDone" object:nil];
    }


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    // Dennis OMIT
    /*UITextInputAssistantItem* item = [textField inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];*/
    
    self.isTextFieldSelected = YES;
    NSInteger selectedRow = textField.tag - INPUT_FIELDS_TAG_DELTA;
    
    [self moveRulerToRow:selectedRow];
    return YES;
}


//up or down button pressed on the keyboard
- (IBAction)arrowButtonTapped:(UIButton*)sender {
    
    NSInteger selectedRow = [self.tireTableView indexPathForSelectedRow].row;
    
    if(sender==self.upButton) {
        selectedRow --;
    } else if(sender==self.downButton) {
        selectedRow++;
    }
    //if below or before the table, adjust
    selectedRow = (selectedRow+self.tireTypeCount) % self.tireTypeCount;
    
    [self moveRulerToRow:selectedRow];
    
}

-(void)moveRulerToRow:(NSInteger)rowIndex{
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:rowIndex inSection:0];
    [self.tireTableView selectRowAtIndexPath:path animated:YES scrollPosition:UITableViewScrollPositionNone];
    [self.tireTableView.delegate tableView:self.tireTableView didSelectRowAtIndexPath:path];
}

- (IBAction)buttonPressed:(id)sender {
    
    UIButton *button = (UIButton *) sender;
    
	[[UIDevice currentDevice] playInputClick];
    
    if (button == self.backspaceButton) {
		[self.cell.tireCountField deleteBackward];
	} else if (button == self.doneButton) {
        if (self.doneButton.enabled) {
            [self saveInputs];
             
            // For TOC version:
            //[self.parentPageViewController loadNextPage];
           
            [self.parentPageViewController.navigationController popViewControllerAnimated:YES];
            
                       }
	} else {
        //a digit was pressed
        
        if ([self.cell.tireCountField.text length] < MAX_TIRE_COUNT_DIGITS) {
            [self.cell.tireCountField replaceRange:self.cell.tireCountField.selectedTextRange withText:button.titleLabel.text];
        }
        
	}
    
    [self validate];
}

@end
