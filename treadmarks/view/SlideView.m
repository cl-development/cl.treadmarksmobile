//
//  SlideView.m
//  TreadMarks
//
//  Created by Dragos Ionel on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "SlideView.h"
#import "Constants.h"
#import "Utils.h"
#import "DataEngine.h"
#import "TOCPageViewController.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Transaction+Transaction.h"

#define X_OFF_SCREEN -225
#define ROTATION 2./5. * 2*M_PI
#define SLIDE_TIME .6


@interface SlideView ()

#pragma mark - IBOutlets


@property (weak, nonatomic) IBOutlet UIButton *tcrButton;
@property (weak, nonatomic) IBOutlet UIButton *stcButton;
@property (weak, nonatomic) IBOutlet UIButton *ptrButton;
@property (weak, nonatomic) IBOutlet UIButton *pitButton;
@property (weak, nonatomic) IBOutlet UIButton *voidButton;
@property (weak, nonatomic) IBOutlet UIButton *slideToVoidButton;
@property (weak, nonatomic) IBOutlet UIView *voidView;
@property (weak, nonatomic) IBOutlet UIButton *wheelButton;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainerView;
@property (weak, nonatomic) TOCPageViewController *TOCView;
@property (weak, nonatomic) IBOutlet UIButton *testButton;

#pragma mark - Properties

@property CGPoint startingOrigin;
@property BOOL thresholdPassed;
@property NSInteger rollStatus;	//status of the roll

@end

@implementation SlideView


//true if the slide is animating
BOOL animating;

#pragma mark - Initializers

- (id)init {

	self = [super init];
	
	if(self)
	{
		UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													  owner:self
													options:nil] objectAtIndex:0];
		self.frame = view.frame;
		[self addSubview:view];

		self.rollStatus = ON_SCREEN;
        animating = NO;
		
		self.showVoid = NO;
		self.allowNewTransactions = YES;
		self.thresholdPassed = NO;
		
#if DEBUG
		self.testButton.hidden = NO;
#endif
	}
	 
	return self;
}

#pragma mark - Override methods
/*- (void)syncDidComplete:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"Sync Complete"]) {
        Registrant *currentRegistrant = [Utils getIncomingRegistrant];
        BOOL activeRegistrant = [currentRegistrant.isActive integerValue]==1 ? YES:NO;
        self.allowNewTransactions = activeRegistrant;
        [self layoutSubviews];
    }
}*/

- (void)layoutSubviews {
	
	[super layoutSubviews];
	
	self.voidView.hidden = !self.showVoid;
    if ([Utils getProcessorDidLogin]==YES) {
        self.tcrButton.enabled = NO;
        self.stcButton.enabled = NO;
        self.ptrButton.enabled =self.allowNewTransactions;// YES;
        //self.pitButton.enabled = YES;
    }
    else {
        self.tcrButton.enabled = self.allowNewTransactions;
        self.stcButton.enabled = self.allowNewTransactions;
        self.ptrButton.enabled = NO;//self.allowNewTransactions;
        //self.pitButton.enabled = NO;
    }
   /* if([Utils getProcessorPtrOnly]==YES && [Utils getProcessorDidLogin]==NO)
    {self.ptrButton.enabled = NO;}*/
    if (self.showVoid== YES)
    {
        self.tcrButton.enabled = NO;
        self.stcButton.enabled = NO;
        self.ptrButton.enabled = NO;
    }
}

#pragma mark - Methods

- (void)hideAnimated:(BOOL)animated {
	
	[self hideAnimated:animated completion:NULL];
}
- (void)hideAnimated:(BOOL)animated completion:(void (^)(void))completion {
	
	if(self.rollStatus != OFF_SCREEN)
	{
		self.rollStatus = OFF_SCREEN;
		
		[self.wheelButton setTransform:CGAffineTransformMakeRotation(0)];
		
		float slideTime = animated ? SLIDE_TIME : 0;
		
		[UIView animateWithDuration:slideTime
						 animations:^
		 {
			 [self.wheelButton setTransform:CGAffineTransformMakeRotation(-ROTATION)];
			 
			 self.frame = CGRectMake(-self.buttonsContainerView.frame.size.width - 1,
									 self.frame.origin.y,
									 self.frame.size.width,
									 self.frame.size.height);
			 
			 self.backgroundView.alpha = 0;
		 }
						 completion:^(BOOL finished)
		 {
			 if (finished)
			 {
				 animating = NO;
				 
				 if(completion != NULL)
				 {
					 completion();
				 }
			 }
		 }];
		
		[self returnSlideToVoid:self.slideToVoidButton];
	}
	else
	{
		if (completion != NULL)
		{
			completion();
		}
	}
}
- (void)returnSlideToVoid:(UIView *)view {
	
	float x = 0;
	
	self.voidButton.enabled = NO;
	
	if(view.frame.origin.x + view.frame.size.width < self.voidButton.frame.origin.x)
	{
		x = self.voidButton.frame.origin.x - view.frame.size.width;
		
		self.voidButton.enabled = YES;
	}
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^
	 {
		 view.frame = CGRectMake(x,
								 view.frame.origin.y,
								 view.frame.size.width,
								 view.frame.size.height);
	 }];
}

- (void)newTransaction:(NSInteger)transType {
    
    self.tcrButton.enabled = NO;
    self.stcButton.enabled = NO;
    self.ptrButton.enabled = NO;
    

	self.newTransactionCreated=YES;
    //create a new transaction based on transType, which is the tagValue of the button that was selected by the user.  The tag value corresponds to the TransactionTypeId, which allows for the creation of the correct TransactionType dynamically.
    [Utils log:@"New Transaction creation"];
    
    TransactionType *transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInteger:transType]];
    //create a new transaction
    Transaction *transaction     = (Transaction*)[[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
	transaction.deviceName = [[UIDevice currentDevice] name];
    // TEMP append device id for now
//    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//    transaction.deviceName = [NSString stringWithFormat:@"%@/%@",transaction.deviceName,deviceID];
//    transaction.deviceIDFV = deviceID;
   // if a processor logs in, the user values are reversed:
    User *loggedInUser = [Utils getIncomingUser];
    BOOL processorDidLogin = loggedInUser.registrant.registrantType.registrantTypeId==REGISTRANT_TYPE_ID_PROCESSOR;
    
    if (processorDidLogin==YES) {
        transaction.incomingUser     = nil;
        transaction.incomingRegistrant = nil;
        transaction.outgoingUser     = [Utils getIncomingUser];
       // transaction.createdUser      = [Utils  getIncomingRegistrant];
        transaction.outgoingRegistrant = [Utils getIncomingRegistrant];
    }
    else {
        transaction.incomingUser     = [Utils getIncomingUser];
        transaction.incomingRegistrant = [Utils getIncomingRegistrant];
        
        transaction.outgoingUser     = nil;
        transaction.outgoingRegistrant = nil;
    }
    transaction.transactionType  = transactionType;
    transaction.transactionId    = [Utils createTransactionId];
    transaction.friendlyId       = [Utils createFriendlyTransactionId:transactionType];
    transaction.createdDate      = [NSDate date];
    transaction.transactionDate  = [NSDate date];
    transaction.createdUser = [Utils getIncomingUser];
    NSString *appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *versionBuildString = [NSString stringWithFormat:@"Version %@(%@)", appVersionString, appBuildString];
    transaction.versionBuild = versionBuildString;
    transaction.transactionStatusType       = [[DataEngine sharedInstance] transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_INCOMPLETE];
    
    transaction.transactionSyncStatusType   = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    
    //find the gps location of the login
    NSNumber *gpsLogId = [Utils getGPSLogId];
    GPSLog *incomingGPSLog = [[DataEngine sharedInstance] gpsLogForId:gpsLogId];
    transaction.incomingGpsLog = incomingGPSLog;
    
    //create the transaction_transactiontype objects
    NSArray *tireTypeArray = [[DataEngine sharedInstance] tireTypesForTransactionTypeId:transactionType.transactionTypeId];
    for (TireType *tireType in tireTypeArray) {
        Transaction_TireType *transactionTireType = (Transaction_TireType*)[[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
        transactionTireType.transaction = transaction;
        transactionTireType.tireType = tireType;
        transactionTireType.transactionTireTypeId = [[NSUUID UUID] UUIDString];
    }
    
    [Utils setTransactionId:transaction.transactionId];
    
	[[DataEngine sharedInstance] saveContext];
    
    TOCPageViewController *viewController = [[TOCPageViewController alloc] init];
	 self.TOCView.newTransaction=OFF_SCREEN;
	if(self.delegate == NULL)
	{
		@throw [[NSException alloc] initWithName:@"Exception"
										  reason:@"delegate must be specified."
										userInfo:NULL];
	}
	
    viewController.rootViewController = self.delegate;
    //self.TOCView.newTransaction=OFF_SCREEN;
    [self.delegate.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - IBActions

- (IBAction)togglePressed:(UIButton *)sender {
    
    
    if ( [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)    {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Location  Access Required"
                                                                            message: @"TreadMarks Mobile needs to use the iPad's location services so transaction can be created. Please enable it in Settings."
                                                                     preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Settings"
                                                              style: UIAlertActionStyleDestructive
                                                            handler: ^(UIAlertAction *action) {
                                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];;
                                                            }];
        
        [controller addAction: alertAction];
        
        [self.delegate.navigationController   presentViewController: controller animated: YES completion: nil];
    }

    [self.window endEditing:YES];
    if (animating)
	{
        return;
    }
	
	animating = YES;
	
	if (self.rollStatus == OFF_SCREEN)
	{
		self.rollStatus = ON_SCREEN;
		
		[sender setTransform:CGAffineTransformMakeRotation(0)];
		
		[UIView animateWithDuration:SLIDE_TIME
						 animations:^
		 {
			 [sender setTransform:CGAffineTransformMakeRotation(ROTATION)];
			 
			 self.frame = CGRectMake(0,
									 self.frame.origin.y,
									 self.frame.size.width,
									 self.frame.size.height);
			 
			 self.backgroundView.alpha = MODAL_BACKGROUND_ALPHA;

		 }
						 completion:^(BOOL finished)
		 {
			 if (finished)
			 {
				 animating = NO;
			 }
		 }];
	}
	else
	{
		[self hideAnimated:YES];
    }
	
	/*
	[self.superview bringSubviewToFront:self];
	
    animating=YES;
    [self.wheelImageView setTransform:CGAffineTransformMakeRotation(0)];
    
    if (self.rollStatus==OFF_SCREEN)
	{
        self.rollStatus=ON_SCREEN;
        [self.wheelImageView setTransform:CGAffineTransformMakeRotation(0)];
        [UIView animateWithDuration:SLIDE_TIME
                         animations:^
		 {
			 self.widgetView.frame = CGRectMake(0,
												self.widgetView.frame.origin.y,
												self.widgetView.frame.size.width,
												self.widgetView.frame.size.height);
			 [self.wheelImageView setTransform:CGAffineTransformMakeRotation(ROTATION)];
			 
			 self.grayLayerView.alpha=.5;
			 
			 self.clippingContainerView.frame = CGRectMake(0,
														   250,
														   self.clippingContainerView.frame.size.width,
														   self.clippingContainerView.frame.size.height);
			 
		 }
						 completion:^(BOOL finished)
		 {
			 animating = NO;s
		 }];
    }
	 */
}
- (IBAction)tcr:(UIButton *)sender {
	
    [self newTransaction:[TRANSACTION_TYPE_ID_TCR intValue]];
}

- (IBAction)stc:(UIButton *)sender {
	
	[self newTransaction:[TRANSACTION_TYPE_ID_STC intValue]];
}


- (IBAction)ptr:(UIButton *)sender {

	[self newTransaction:[TRANSACTION_TYPE_ID_PTR intValue]];
}

- (IBAction)pit:(UIButton *)sender {
    
	[self newTransaction:[TRANSACTION_TYPE_ID_PIT intValue]];
}

- (IBAction)slideToVoid:(UIPanGestureRecognizer *)sender {
	
	UIView* view = sender.view;
	CGPoint translation = [sender translationInView:view];
	float x = 0;
	
	if(sender.state == UIGestureRecognizerStateBegan)
	{
		self.startingOrigin = view.frame.origin;
	}
	else if(sender.state == UIGestureRecognizerStateChanged)
	{
		x = self.startingOrigin.x + translation.x;
		
		if(x > 0)
		{
			x = 0;
		}
		
		view.frame = CGRectMake(x, self.startingOrigin.y, view.frame.size.width, view.frame.size.height);
	}
	else if(sender.state == UIGestureRecognizerStateEnded)
	{
		[self returnSlideToVoid:view];
	}
}
- (IBAction)slideToVoidTouchUpInside:(UIButton *)sender {

	[self returnSlideToVoid:sender];
}
- (IBAction)voidTransaction:(UIButton *)sender {
	
	Transaction *transaction = [Utils getTransaction];

    if (transaction.outgoingSignatureName)
    {
		[Utils showMessage:self.delegate header:@"Warning" message:@"Transaction with outgoing signature cannot be voided" hideDelay:1];
        
		[self hideAnimated:YES];
        
        return ;
    }
    

    [self.delegate voidTransaction];
}
- (IBAction)test:(UIButton *)sender {
	
	/*
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^
				   {
					   [Transaction loadTestTransactions:[Utils getIncomingUser]];
				   });
	 */
	[Transaction loadTestTransactions:[Utils getIncomingUser]];
}

@end
