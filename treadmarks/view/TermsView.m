//
//  TermsView.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-04.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TermsView.h"
#import "User.h"
#import "Registrant.h"
#import "UIView+CustomView.h"

@implementation TermsView

#pragma mark - Initializers

- (id)init {
	
	self = [super init];
	
	if (self)
	{
		UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													  owner:self
													options:nil] objectAtIndex:0];
		self.frame = view.frame;
		[self addSubview:view];
        [view createCornerRadius:view];

	}
	
	return self;
}

#pragma mark - IBActions

- (IBAction)accept:(UIButton *)sender {
	
	[self.delegate acceptTerms:self.user];

}
- (IBAction)decline:(UIButton *)sender {
	
	[self.delegate declineTerms];
}

@end
