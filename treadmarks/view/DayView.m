//
//  DayView.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-30.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "DayView.h"
#import "Utils.h"
#import "Constants.h"
#import "TransactionStatusType.h"

#define TransactionStatusError 0
#define TransactionStatusIncomplete 1
#define TransactionStatusComplete 2

@interface DayView ()

@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *syncStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;

@property (weak, nonatomic) IBOutlet UIImageView *bottomBar;

@end

@implementation DayView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self connectNib];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])){
        //[self connectNib];
    }
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
}

-(void)updateUI{
    //populate the day
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned int components = NSCalendarUnitDay ;
    NSDateComponents *dateComponents    = [calendar components:components fromDate:self.date];
    NSInteger day     = [dateComponents day];
    
    self.dayLabel.text=[NSString stringWithFormat:@"%ld",(long)day];
    
    // Don't filter
    NSArray *filteredArray = [self filterTransactionsArray];
    if ([filteredArray count]==0) {
        // we just show all green transactions
    }
    else {
        self.transactionArray = filteredArray;
    }
    if(self.isCurrentDay){
    self.dayLabel.textColor = [Utils colorWithRed:255 green:1 blue:12];
    }

    if(self.isCurrentMonth){
        self.dayLabel.textColor = [Utils colorWithRed:125 green:125 blue:125];
       //neede to be fixed
        // if ([self.dayLabel.text isEqualToString:[@ (Currday) stringValue]]&& month==CurrMonth)
        //{self.view.backgroundColor=[Utils colorWithRed:245 green:245 blue:245];
        //}
            
        //self.button.enabled = YES;
       // self.bottomBar.hidden=NO;
    } else {
        self.dayLabel.textColor = [Utils colorWithRed:206 green:206 blue:206];
       // self.bottomBar.hidden=YES;
    }
    
    if ([self.transactionArray count]==0) {
        self.statusImageView.alpha=0;
        self.syncStatusImageView.alpha=0;
        self.countLabel.alpha=0;
    } else {
        self.statusImageView.alpha=1;
        self.syncStatusImageView.alpha=1;
        self.countLabel.alpha=1;
    }
    
    if ([self.transactionArray count]!=0) {
        NSUInteger dayTransactionStatus=TransactionStatusComplete;
        int transactionCompleteCount=0; int transactionIncompleteCount=0; int transactionHasIssuesCount=0;
        for (Transaction *transaction in self.transactionArray) {
            if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_ERROR) {
                dayTransactionStatus=TransactionStatusError;
                transactionHasIssuesCount = transactionHasIssuesCount + 1;
            }
            else if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_INCOMPLETE) {
                dayTransactionStatus=TransactionStatusIncomplete;
                transactionIncompleteCount = transactionIncompleteCount + 1;
            }
            else if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_COMPLETE) {
                transactionCompleteCount = transactionCompleteCount + 1;
            }
            

        }
        switch (dayTransactionStatus) {
            case TransactionStatusIncomplete:
                self.statusImageView.image = [UIImage imageNamed:@"cal-flag-yellow.png"];
                //self.dayLabel.textColor = [Utils colorWithRed:184 green:174 blue:0];
                self.countLabel.text = [NSString stringWithFormat:@"%d",transactionIncompleteCount];
                break;
            case TransactionStatusComplete:
                self.statusImageView.image = [UIImage imageNamed:@"cal-flag-green.png"];
                //self.dayLabel.textColor = [Utils colorWithRed:23 green:135 blue:69];
                self.countLabel.text = [NSString stringWithFormat:@"%d",transactionCompleteCount];
                break;
            case TransactionStatusError:
                self.statusImageView.image = [UIImage imageNamed:@"cal-flag-red.png"];
                //self.dayLabel.textColor = [Utils colorWithRed:218 green:47 blue:58];
                self.countLabel.text = [NSString stringWithFormat:@"%d",transactionHasIssuesCount];
                break;
            default:
                break;
        }
    }
}

-(NSMutableArray *) filterTransactionsArray {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (Transaction *trans in self.transactionArray) {
        if (trans.transactionStatusType.transactionStatusTypeId!=TRANSACTION_STATUS_TYPE_ID_COMPLETE) {
            [tempArray addObject:trans];
        }
    }
    return tempArray;
}

@end
