//
//  NewAlertView.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-09-17.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "NewAlertView.h"
#import "UIView+CustomView.h"


@interface NewAlertView()

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIView *compoundLabelView;
@property (weak, nonatomic) IBOutlet UILabel *singleTextLabel;
@property (weak, nonatomic) IBOutlet UIView *twoButtonView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIButton *primaryButton;
@property (weak, nonatomic) IBOutlet UIButton *secondaryButton;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (nonatomic,strong)IBOutlet UIView *viewBackground;

@property (nonatomic, strong)IBOutlet UIVisualEffectView *visualView;
@property (nonatomic, strong)IBOutlet UIView *viewVisualEffect;


@end

@implementation NewAlertView

#pragma mark - Initializers

- (id)init {
	
	self = [super init];
	
	if (self)
	{
		UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
													  owner:self
													options:nil] objectAtIndex:0];
		self.frame = view.frame;
		[self addSubview:view];
       [self.viewBackground createCornerRadius:self.viewBackground];
        self.singlelineforOkay.hidden=YES;
		self.headingLabel.text = nil;
		[self.secondaryButton setTitle:nil forState:UIControlStateNormal];
}
	
	return self;
}

#pragma mark - Property Getter/Setters

- (void)setTitle:(NSString *)title {
	
	self.titleLabel.text = title;
}
- (void)setText:(NSString *)text {
	
	self.singleTextLabel.text = text;
	self.textLabel.text = text;
}
- (void)setPrimaryButtonText:(NSString *)primaryButtonText {
	
	[self.singleButton setTitle:primaryButtonText forState:UIControlStateNormal];
	[self.primaryButton setTitle:primaryButtonText forState:UIControlStateNormal];
}
- (void)setHeading:(NSString *)heading {
	
	self.headingLabel.text = heading;
}
- (void)setSecondaryButtonText:(NSString *)secondaryButtonText {
	
	[self.secondaryButton setTitle:secondaryButtonText forState:UIControlStateNormal];
}

#pragma mark - Override methods

//- (void)drawRect:(CGRect)rect {
//	
//	
//     if ([self.headingLabel.text isEqualToString: @"Weight variance detected"])  {
//        self.compoundLabelView.hidden = YES;
//        self.singleTextLabel.hidden = YES;
//        self.twoButtonView.hidden = YES;
//        self.singleButton.hidden = YES;
//        self.imageViewSingleButton.hidden=YES;
//        self.twoButtonView.hidden =YES;
//         self.singlelineforOkay.hidden=NO;
//        
//    }
//    
//    if (self.headingLabel.text == nil) {
//		
//		self.compoundLabelView.hidden = YES;
//		self.singleTextLabel.hidden = NO;
//		
//	} else {
//		
//		self.compoundLabelView.hidden = NO;
//        self.singlelineforOkay.hidden=YES;
//		self.singleTextLabel.hidden = YES;
//	}
//	
//	if ([self.secondaryButton titleForState:UIControlStateNormal] == nil ) {
//		
//		self.twoButtonView.hidden = YES;
//		self.singleButton.hidden = NO;
//        self.imageViewSingleButton.hidden=NO;
//
//		
//	} else {
//		
//		self.twoButtonView.hidden = NO;
//		self.singleButton.hidden = YES;
//        self.imageViewSingleButton.hidden=YES;
//
//	}
//		
//	
//	[super drawRect:rect];
//}
-(void)setUI
{
    
    if (self.setColour == YES) {
        self.imageViewSingleButton.backgroundColor =[UIColor colorWithRed:10.0/255.0 green:96.0/255.0 blue:254.0/255.0 alpha:1.0];
        [self.singleButton setTitleColor:[UIColor colorWithRed:10.0/255.0 green:96.0/255.0 blue:254.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
        [self.singleButton setTitleColor:[UIColor colorWithRed:10.0/255.0 green:96.0/255.0 blue:254.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
    if ([self.headingLabel.text isEqualToString: @"Weight variance detected"])  {
        self.compoundLabelView.hidden = YES;
        self.singleTextLabel.hidden = YES;
        self.twoButtonView.hidden = YES;
        self.singleButton.hidden = YES;
        self.imageViewSingleButton.hidden=YES;
        self.twoButtonView.hidden =YES;
        self.singlelineforOkay.hidden=NO;
        
    }
    
    if (self.headingLabel.text == nil) {
        
        self.compoundLabelView.hidden = YES;
        self.singleTextLabel.hidden = NO;
        
    } else {
        
        self.compoundLabelView.hidden = NO;
        self.singlelineforOkay.hidden=YES;
        self.singleTextLabel.hidden = YES;
    }
    
    if ([self.secondaryButton titleForState:UIControlStateNormal] == nil ) {
        
        self.twoButtonView.hidden = YES;
        self.singleButton.hidden = NO;
        self.imageViewSingleButton.hidden=NO;
        
        
    } else {
        
        self.twoButtonView.hidden = NO;
        self.singleButton.hidden = YES;
        self.imageViewSingleButton.hidden=YES;
        
    }
    
    
}
#pragma mark - IBActions

- (IBAction)primaryAction:(UIButton *)sender {
	
	[self.delegate primaryAction:sender];
}
- (IBAction)secondaryAction:(UIButton *)sender {
	
	[self.delegate secondaryAction:sender];
}

@end
