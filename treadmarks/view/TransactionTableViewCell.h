//
//  TransactionTableViewCell.h
//  CalendarPilot
//
//  Created by Dragos Ionel on 2013-07-10.
//  Copyright (c) 2013 Adelante Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

@interface TransactionTableViewCell : UITableViewCell

@property (nonatomic,strong) Transaction *transaction;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end
