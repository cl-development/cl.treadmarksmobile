//
//  TermsView.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-04.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;


@protocol TermsViewDelegate <NSObject>

- (void)declineTerms;
- (void)acceptTerms:(User*)user;

@end

@interface TermsView : UIView

@property (strong) id<TermsViewDelegate> delegate;
@property (strong) User* user;

@end
