//
//  SimpletableCell.m
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TireCountTableCell.h"

@implementation TireCountTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.frame = CGRectMake(0, 0, 320, 80);
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
        
        _tireCountField = [[UITextField alloc] initWithFrame:CGRectMake(26.25, 14, 95, 41)];

        [[_tireCountField layer] setMasksToBounds:8.0f];
        [[_tireCountField layer] setBorderColor:[[UIColor grayColor] CGColor]];
        [[_tireCountField layer] setBorderWidth:1.0f];
        
        [_tireCountField setRightView:paddingView];
        [_tireCountField setLeftView:paddingView];
        [_tireCountField setRightViewMode:UITextFieldViewModeAlways];
        [_tireCountField setLeftViewMode:UITextFieldViewModeAlways];
        [_tireCountField setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:26]];
        [_tireCountField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        //[_tireCountField setDelegate:self];
        
        [self.contentView addSubview:_tireCountField];
        
        UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _tireCountField.inputView = dummyView;
        
        _tireNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 14, 470, 40)];
        [_tireNameLabel setBackgroundColor:[UIColor clearColor]];
        //[_tireNameLabel setFont:[UIFont systemFontOfSize:24]];
        [_tireNameLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:24]];
        [_tireNameLabel setNumberOfLines:2];
        [_tireNameLabel setLineBreakMode:NO];
        UITextInputAssistantItem* item = [_tireCountField inputAssistantItem];
        item.leadingBarButtonGroups = @[];
        item.trailingBarButtonGroups = @[];
        
        [self.contentView addSubview:_tireNameLabel];
        
    }
    return self;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSUInteger newLength = [textField.text length] + [string length] - range.length;
//    return (newLength > 6) ? NO : YES;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
