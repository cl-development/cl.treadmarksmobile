//
//  SyncView.h
//  TreadMarks
//
//  Created by Adi on 2015-10-11.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"
@protocol SyncViewControllerDelegate <NSObject>

@required


- (void)syncCancelAction:(UIButton *)sender;
@end

@interface SyncView : UIView
@property id<SyncViewControllerDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIView *syncView;
- (IBAction)BackButton:(id)sender;

@end
