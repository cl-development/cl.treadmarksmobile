//
//  MHWCoreDataController.m
//  BookMigration
//
//  Created by Martin Hwasser on 8/26/13.
//  Copyright (c) 2013 Martin Hwasser. All rights reserved.
//

#import "MHWCoreDataController.h"
#import "NSFileManager+MHWAdditions.h"
#import "MHWMigrationManager.h"
#import "NSManagedObjectModel+MHWAdditions.h"

@interface MHWCoreDataController ()

@property (nonatomic, readwrite, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, readwrite, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, readwrite, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@end

@implementation MHWCoreDataController

+ (MHWCoreDataController *)sharedInstance
{
    static MHWCoreDataController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [MHWCoreDataController new];
    });
    return sharedInstance;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
   // _managedObjectContext = [[NSManagedObjectContext alloc] init];
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _managedObjectContext.persistentStoreCoordinator = coordinator;

    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }

    NSString *momPath = [[NSBundle mainBundle] pathForResource:MANAGED_OBJECT_MODEL_PATH
                                                        ofType:@"momd"];

    if (!momPath) {
        momPath = [[NSBundle mainBundle] pathForResource:MANAGED_OBJECT_MODEL_PATH
                                                  ofType:@"mom"];
    }

    NSURL *url = [NSURL fileURLWithPath:momPath];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:url];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSError *error = nil;

    NSManagedObjectModel *mom = [self managedObjectModel];

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:[self sourceStoreType]
                                                   configuration:nil
                                                             URL:[self sourceStoreURL]
                                                         options:nil
                                                           error:&error]) {

        //NSLog(@"error: %@", error);
        NSFileManager *fileManager = [NSFileManager new];
        [fileManager removeItemAtPath:[self sourceStoreURL].path error:nil];

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Ouch"
                                     message:error.localizedDescription
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your ok button action here
                                    }];
    
        
        [alert addAction:okButton];
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:^{
        }];
        
       /* [[[UIAlertView alloc] initWithTitle:@"Ouch"
                                    message:error.localizedDescription
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];*/
    }

    return _persistentStoreCoordinator;
}

- (NSURL *)sourceStoreURL
{
    return [[NSFileManager urlToDocumentDirectory] URLByAppendingPathComponent:SQLITE_STORE_URL];
}

- (NSString *)sourceStoreType
{
    return NSSQLiteStoreType;
}

- (NSDictionary *)sourceMetadata:(NSError **)error
{
    return [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:[self sourceStoreType] URL: [self sourceStoreURL] options:nil error:error];
   // return [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:[self sourceStoreType]
                                                                    //  URL:[self sourceStoreURL]
                                                                   // error:error];
}

- (BOOL)isMigrationNeeded
{
    NSError *error = nil;

    // Check if we need to migrate
    NSDictionary *sourceMetadata = [self sourceMetadata:&error];
    BOOL isMigrationNeeded = NO;

    if (sourceMetadata != nil) {
        NSManagedObjectModel *destinationModel = [self managedObjectModel];
        // Migration is needed if destinationModel is NOT compatible
        isMigrationNeeded = ![destinationModel isConfiguration:nil
                                   compatibleWithStoreMetadata:sourceMetadata];
        
        /*NSDictionary *dict = destinationModel.entityVersionHashesByName;
        NSLog(@"isMigrationNeeded: %d %@", isMigrationNeeded, dict);*/
    }
    //NSLog(@"isMigrationNeeded: %d", isMigrationNeeded);
    return isMigrationNeeded;
}

- (BOOL)migrate:(NSError *__autoreleasing *)error
{
    // Enable migrations to run even while user exits app
    __block UIBackgroundTaskIdentifier bgTask;
    bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];

    MHWMigrationManager *migrationManager = [MHWMigrationManager new];
    migrationManager.delegate = self;
    
    BOOL OK = [migrationManager progressivelyMigrateURL:[self sourceStoreURL]
                                                 ofType:[self sourceStoreType]
                                                toModel:[self managedObjectModel]
                                                  error:error];
    if (OK) {
        //NSLog(@"migration complete");
    }

    // Mark it as invalid
    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
    return OK;
}


#pragma mark -
#pragma mark - MHWMigrationManagerDelegate

- (void)migrationManager:(MHWMigrationManager *)migrationManager migrationProgress:(float)migrationProgress
{
    //NSLog(@"migration progress: %f", migrationProgress);
}

- (NSArray *)migrationManager:(MHWMigrationManager *)migrationManager
  mappingModelsForSourceModel:(NSManagedObjectModel *)sourceModel
{
    NSMutableArray *mappingModels = [@[] mutableCopy];
    NSString *modelName = [sourceModel mhw_modelName];
	
	/// Hardcoding some cases because the correct file name conventions were not in place
	NSString* mappingModelName;
	if ([modelName isEqualToString:@"TreadMarks 2"])
	{
		mappingModelName = @"Model-2";
	}
	else if ([modelName isEqualToString:@"TreadMarks 3"])
	{
		mappingModelName = @"Model-3";
	}
	else
	{
		mappingModelName = [modelName stringByReplacingOccurrencesOfString:@" " withString:@"-"];
	}
	
        // Migrating to Model3
        NSArray *urls = [[NSBundle bundleForClass:[self class]]
                         URLsForResourcesWithExtension:@"cdm"
                         subdirectory:nil];
        for (NSURL *url in urls) {
            if ([url.lastPathComponent rangeOfString:[NSString stringWithFormat:@"%@-", mappingModelName]].length != 0) {
                NSMappingModel *mappingModel = [[NSMappingModel alloc] initWithContentsOfURL:url];
                    [mappingModels addObject:mappingModel];
            }
        }

    return mappingModels;
}

@end
