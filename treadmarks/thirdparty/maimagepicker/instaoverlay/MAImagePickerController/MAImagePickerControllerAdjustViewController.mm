//
//  MAImagePickerControllerAdjustViewController.m
//  instaoverlay
//
//  Created by Maximilian Mackh on 11/5/12.
//  Copyright (c) 2012 mackh ag. All rights reserved.
//

#import "MAImagePickerControllerAdjustViewController.h"
#import "MAImagePickerController.h"

#import "MAOpenCV.h"
#import "UIImageView+ContentFrame.h"
#import "CDActivityIndicatorView.h"
#import <QuartzCore/QuartzCore.h>

@interface MAImagePickerControllerAdjustViewController ()
{
    UIButton *newConfirmButton;
}

@end

@implementation MAImagePickerControllerAdjustViewController

@synthesize sourceImageView = _sourceImageView;
@synthesize adjustToolBar = _adjustToolBar;
@synthesize sourceImage = _sourceImage;
@synthesize adjustedImage = _adjustedImage;
@synthesize adjustRect = _adjustRect;
@synthesize Close=_close;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeCamera:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeCamera:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    UIView *savingDialog   = [[UIView alloc] initWithFrame:CGRectMake(286, 442, 197, 142)  ];
    [savingDialog setBackgroundColor:[UIColor greenColor]];
    //savingDialog.alpha=0;
    [self.view addSubview:savingDialog];
    [self.view bringSubviewToFront:savingDialog];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    [backgroundView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:backgroundView];
    
    _sourceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - kCameraToolBarHeight)];
    [_sourceImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    _adjustedImage = _sourceImage;
    
    [_sourceImageView setImage:_adjustedImage];
    [self.view addSubview:_sourceImageView];
    
    _adjustRect= [[MADrawRect alloc] initWithFrame:_sourceImageView.contentFrame];
    [self.view addSubview:_adjustRect];
    
    
    //new design (ui toolbar)
   
#define iOS7_Add_Pixels 20
    UIImageView *barImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comp-cap-ftr-bar.png"]];
    CGRect barImageViewFrame = CGRectMake(0, 1024-80, 768, 80);
    barImageView.frame = barImageViewFrame;
    /*CGRectMake(0, 1004-54+iOS7_Add_Pixels, 768, 54);*//* 20 for iOS7 change */
    [self.view addSubview:barImageView];

    newConfirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [newConfirmButton setImage:[UIImage imageNamed:@"comp-cap-check.png"] forState:UIControlStateNormal];
     [newConfirmButton setTitle:@"Done" forState:UIControlStateNormal];
      newConfirmButton.titleLabel.font= [UIFont fontWithName:@"OpenSans-Semibold" size:26];
    [newConfirmButton addTarget:self action:@selector(confirmedImage) forControlEvents:UIControlEventTouchUpInside];
    newConfirmButton.frame = CGRectMake(305,1024-80,178,80);
    [self leftImageforButton:newConfirmButton:60];
    [self.view addSubview:newConfirmButton];
    
    UIButton *newCancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [newCancelButton setImage:[UIImage imageNamed:@"crop-cap-btn-cancel.png"] forState:UIControlStateNormal];
    [newCancelButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    newCancelButton.frame = CGRectMake(20, 1024-80, 90, 80);
    [self.view addSubview:newCancelButton];
    
    
    //cap-btn-retake.png
    UIButton *retakeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [retakeButton setImage:[UIImage imageNamed:@"comp-cap-retake-blue.png"] forState:UIControlStateNormal];
   [retakeButton setTitle:@"Retake" forState:UIControlStateNormal];
   retakeButton.titleLabel.font=[UIFont fontWithName:@"OpenSans" size:19];
    [retakeButton addTarget:self action:@selector(retakeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self leftImageforButton:retakeButton :140];
    retakeButton.frame = CGRectMake(768-20-85, 1024-80, 128, 80);
    [self.view addSubview:retakeButton];

    //end of new design
    
    [self detectEdges];
    // For hiding the Notification Center /Control Center
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
  //  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    self.view.clipsToBounds  =YES;
    self.view.multipleTouchEnabled=NO;
    
}
// For hiding the Notification Center /Control Center

- (void)closeCamera:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
         [self.delegate dismissMAImagePickerController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)leftImageforButton :(UIButton*)buttons :(NSInteger)gap

{
   buttons.titleEdgeInsets = UIEdgeInsetsMake(0, - buttons.imageView.frame.size.width-5, 0, -buttons.imageView.frame.size.width);
   buttons.imageEdgeInsets = UIEdgeInsetsMake(0,buttons.titleLabel.frame.size.width , 0, - buttons.titleLabel.frame.size.width -15);
    
   buttons.titleEdgeInsets = UIEdgeInsetsMake(0, - buttons.imageView.frame.size.width-gap, 0, -buttons.imageView.frame.size.width);
   buttons.imageEdgeInsets = UIEdgeInsetsMake(0,buttons.titleLabel.frame.size.width , 0, -buttons.titleLabel.frame.size.width -5);
    
    
    
}




- (void)viewWillAppear:(BOOL)animated
{
    if ([_adjustRect frameEdited])
    {
        _adjustedImage = _sourceImage;
        [_sourceImageView setImage:_adjustedImage];
    }
}

- (void)popCurrentViewController
{    
    
    [self.navigationController popViewControllerAnimated:NO];
    
    _adjustRect = nil;
    _adjustToolBar = nil;
    _adjustedImage = nil;
    _sourceImage = nil;
    
    if (self.delegate != nil) {
        [self.delegate dismissMAImagePickerController];
    }
}
-(void) retakeButtonAction:(id) sender {
    NSArray *vcs = self.navigationController.viewControllers;
    for (UIViewController *vc in vcs) {
        if ([vc isKindOfClass:[MAImagePickerController class]]) {
            [self.navigationController popToViewController:vc animated:NO];
            break;
        }
    }
}

- (void)resetRectFrame
{
    _adjustedImage = _sourceImage;
    [_sourceImageView setImage:_adjustedImage];
    [_adjustRect setHidden:NO];
    [_adjustRect resetFrame];
}

- (void)detectEdges
{
    cv::Mat original = [MAOpenCV cvMatFromUIImage:_adjustedImage];
    CGSize targetSize = _sourceImageView.contentSize;
    cv::resize(original, original, cvSize(targetSize.width, targetSize.height));
    
    cv::vector<cv::vector<cv::Point>>squares;
    cv::vector<cv::Point> largest_square;
    
    find_squares(original, squares);
    find_largest_square(squares, largest_square);

    if (largest_square.size() == 4)
    {
        
        // Manually sorting points, needs major improvement. Sorry.
        
        NSMutableArray *points = [NSMutableArray array];
        NSMutableDictionary *sortedPoints = [NSMutableDictionary dictionary];
        
        for (int i = 0; i < 4; i++)
        {
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithCGPoint:CGPointMake(largest_square[i].x, largest_square[i].y)], @"point" , [NSNumber numberWithInt:(largest_square[i].x + largest_square[i].y)], @"value", nil];
            [points addObject:dict];
        }
        
        int min = [[points valueForKeyPath:@"@min.value"] intValue];
        int max = [[points valueForKeyPath:@"@max.value"] intValue];
        
        int minIndex;
        int maxIndex;
        
        int missingIndexOne;
        int missingIndexTwo;
        
        for (int i = 0; i < 4; i++)
        {
            NSDictionary *dict = [points objectAtIndex:i];
            
            if ([[dict objectForKey:@"value"] intValue] == min)
            {
                [sortedPoints setObject:[dict objectForKey:@"point"] forKey:@"0"];
                minIndex = i;
                continue;
            }
            
            if ([[dict objectForKey:@"value"] intValue] == max)
            {
                [sortedPoints setObject:[dict objectForKey:@"point"] forKey:@"2"];
                maxIndex = i;
                continue;
            }
            
            //NSLog(@"MSSSING %i", i);
            
            missingIndexOne = i;
        }
        
        for (int i = 0; i < 4; i++)
        {
            if (missingIndexOne != i && minIndex != i && maxIndex != i)
            {
                missingIndexTwo = i;
            }
        }
        
        
        if (largest_square[missingIndexOne].x < largest_square[missingIndexTwo].x)
        {
            //2nd Point Found
            [sortedPoints setObject:[[points objectAtIndex:missingIndexOne] objectForKey:@"point"] forKey:@"3"];
            [sortedPoints setObject:[[points objectAtIndex:missingIndexTwo] objectForKey:@"point"] forKey:@"1"];
        }
        else
        {
            //4rd Point Found
            [sortedPoints setObject:[[points objectAtIndex:missingIndexOne] objectForKey:@"point"] forKey:@"1"];
            [sortedPoints setObject:[[points objectAtIndex:missingIndexTwo] objectForKey:@"point"] forKey:@"3"];
        }
        
        // Check for degenerate rect that is either negative (over-lapped points) or too small to be a document:
        CGPoint ULpt = [(NSValue *)[sortedPoints objectForKey:@"0"] CGPointValue];
        CGPoint URpt = [(NSValue *)[sortedPoints objectForKey:@"1"] CGPointValue];
        CGPoint LLpt = [(NSValue *)[sortedPoints objectForKey:@"3"] CGPointValue];
        CGPoint LRpt = [(NSValue *)[sortedPoints objectForKey:@"2"] CGPointValue];
        int width = MAX(URpt.x, LRpt.x) - MAX(LLpt.x, ULpt.x);
        int height =  MAX(LLpt.y, LRpt.y) - MAX(ULpt.y, URpt.y);
        
        if (width < MA_IMAGE_PICKER_CROP_RECT_MIN_WIDTH || height < MA_IMAGE_PICKER_CROP_RECT_MIN_HEIGHT) {
            //NSLog(@"Resized rect width:%d, height:%d",width,height);
            [self setDefaultCropRect];
        }
        else {
            [_adjustRect topLeftCornerToCGPoint:[(NSValue *)[sortedPoints objectForKey:@"0"] CGPointValue]];
            [_adjustRect topRightCornerToCGPoint:[(NSValue *)[sortedPoints objectForKey:@"1"] CGPointValue]];
            [_adjustRect bottomRightCornerToCGPoint:[(NSValue *)[sortedPoints objectForKey:@"2"] CGPointValue]];
            [_adjustRect bottomLeftCornerToCGPoint:[(NSValue *)[sortedPoints objectForKey:@"3"] CGPointValue]];
        }
    } else {
        [self setDefaultCropRect];
    }
    /*else {
        CGFloat BORDER_SIZE=30;
        [_adjustRect bottomLeftCornerToCGPoint: CGPointMake(_adjustRect.pointA.center.x+BORDER_SIZE,   _adjustRect.pointA.center.y-2*BORDER_SIZE)];
        [_adjustRect bottomRightCornerToCGPoint:CGPointMake(_adjustRect.pointB.center.x-2*BORDER_SIZE, _adjustRect.pointB.center.y-2*BORDER_SIZE)];
        [_adjustRect topRightCornerToCGPoint:   CGPointMake(_adjustRect.pointC.center.x-2*BORDER_SIZE, _adjustRect.pointC.center.y+BORDER_SIZE)];
        [_adjustRect topLeftCornerToCGPoint:    CGPointMake(_adjustRect.pointD.center.x+BORDER_SIZE,   _adjustRect.pointD.center.y+BORDER_SIZE)];
    }*/

    original.release();

    /*
    [_sourceImageView setNeedsDisplay];
    [_sourceImageView setImage:[MAOpenCV UIImageFromCVMat:original]];
    [_sourceImageView setContentMode:UIViewContentModeScaleAspectFit];
     */
}

-(void) setDefaultCropRect {
    CGFloat BORDER_SIZE=30;
    [_adjustRect bottomLeftCornerToCGPoint: CGPointMake(_adjustRect.pointA.center.x+BORDER_SIZE,   _adjustRect.pointA.center.y-2*BORDER_SIZE)];
    [_adjustRect bottomRightCornerToCGPoint:CGPointMake(_adjustRect.pointB.center.x-2*BORDER_SIZE, _adjustRect.pointB.center.y-2*BORDER_SIZE)];
    [_adjustRect topRightCornerToCGPoint:   CGPointMake(_adjustRect.pointC.center.x-2*BORDER_SIZE, _adjustRect.pointC.center.y+BORDER_SIZE)];
    [_adjustRect topLeftCornerToCGPoint:    CGPointMake(_adjustRect.pointD.center.x+BORDER_SIZE,   _adjustRect.pointD.center.y+BORDER_SIZE)];
}

- (void)confirmedImage
{
      //   [newConfirmButton setTitle:@"Saving..." forState:UIControlStateHighlighted];
  
    // UIImage *CroppedImage;
  
    //cap-btn-retake.png
    
  UILabel *lblBck = [[UILabel alloc] initWithFrame:CGRectMake(0,0,768,1024)];
     lblBck.backgroundColor=[UIColor darkGrayColor];
        lblBck.userInteractionEnabled=NO;
    lblBck.alpha=0.6;
    [self.view addSubview:lblBck];

    
    
    
            CDActivityIndicatorView * activityIndicatorView = [[CDActivityIndicatorView alloc] initWithImage:[UIImage imageNamed:@"slide-icon-tire.png"]];
        
        activityIndicatorView.center = self.view.center;
        
        [self.view addSubview:activityIndicatorView];
        
        [activityIndicatorView startAnimating];
    
    
    UILabel *lblSaving = [[UILabel alloc] initWithFrame:CGRectMake(activityIndicatorView.frame.origin.x-50,activityIndicatorView.frame.origin.y+50,250,200)];
       lblSaving.textColor = [UIColor blackColor];
   // [lbl1 setFrame:position];
    lblSaving.backgroundColor=[UIColor clearColor];
    lblSaving.textColor=[UIColor whiteColor];
    lblSaving.font=[UIFont fontWithName:@"OpenSans" size:22];
    lblSaving.userInteractionEnabled=NO;
   lblSaving.text= @"Saving & Optimizing Image";
    [self.view addSubview:lblSaving];
    
   /*
    UIButton *retakeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [retakeButton setImage:[UIImage imageNamed:@"comp-cap-retake.png"] forState:UIControlStateNormal];
    [retakeButton setTitle:@"Retake" forState:UIControlStateNormal];
    retakeButton.titleLabel.font=[UIFont fontWithName:@"OpenSans" size:19];
    [retakeButton addTarget:self action:@selector(retakeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self leftImageforButton:retakeButton :140];
    retakeButton.frame = CGRectMake(768-20-85, 1024-80, 128, 80);
    //retakeButton.frame = CGRectMake(768-20-128, 1024-80, 128, 80);
    [self.view addSubview:retakeButton];
    
    
     
     [self leftImageforButton:retakeButton :140];
     retakeButton.frame = CGRectMake(768-20-85, 1024-80, 128, 80);
     */
    [self performSelector:@selector(cropNAttach) withObject:nil afterDelay:0.01];

 
  
}

-(void) cropNAttach{
BOOL edited;
    if ([_adjustRect frameEdited])
    {
        //cv::GaussianBlur(original, original, cvSize(11,11), 0);
        //cv::adaptiveThreshold(original, original, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 5, 2);
        
        CGFloat scaleFactor =  [_sourceImageView contentScale];
        CGPoint ptBottomLeft = [_adjustRect coordinatesForPoint:1 withScaleFactor:scaleFactor];
        CGPoint ptBottomRight = [_adjustRect coordinatesForPoint:2 withScaleFactor:scaleFactor];
        CGPoint ptTopRight = [_adjustRect coordinatesForPoint:3 withScaleFactor:scaleFactor];
        CGPoint ptTopLeft = [_adjustRect coordinatesForPoint:4 withScaleFactor:scaleFactor];
        
        CGFloat w1 = sqrt( pow(ptBottomRight.x - ptBottomLeft.x , 2) + pow(ptBottomRight.x - ptBottomLeft.x, 2));
        CGFloat w2 = sqrt( pow(ptTopRight.x - ptTopLeft.x , 2) + pow(ptTopRight.x - ptTopLeft.x, 2));
        
        CGFloat h1 = sqrt( pow(ptTopRight.y - ptBottomRight.y , 2) + pow(ptTopRight.y - ptBottomRight.y, 2));
        CGFloat h2 = sqrt( pow(ptTopLeft.y - ptBottomLeft.y , 2) + pow(ptTopLeft.y - ptBottomLeft.y, 2));
        
        CGFloat maxWidth = (w1 < w2) ? w1 : w2;
        CGFloat maxHeight = (h1 < h2) ? h1 : h2;
        
        cv::Point2f src[4], dst[4];
        src[0].x = ptTopLeft.x;
        src[0].y = ptTopLeft.y;
        src[1].x = ptTopRight.x;
        src[1].y = ptTopRight.y;
        src[2].x = ptBottomRight.x;
        src[2].y = ptBottomRight.y;
        src[3].x = ptBottomLeft.x;
        src[3].y = ptBottomLeft.y;
        
        dst[0].x = 0;
        dst[0].y = 0;
        dst[1].x = maxWidth - 1;
        dst[1].y = 0;
        dst[2].x = maxWidth - 1;
        dst[2].y = maxHeight - 1;
        dst[3].x = 0;
        dst[3].y = maxHeight - 1;
        
            

        cv::Mat undistorted = cv::Mat(cvSize(maxWidth,maxHeight), CV_8UC1);//CV_8UC1);
        cv::Mat original = [MAOpenCV cvMatFromUIImage:_sourceImage];
        cv::warpPerspective(original, undistorted, cv::getPerspectiveTransform(src, dst), cvSize(maxWidth, maxHeight));
        original.release();
        
       _adjustedImage = [MAOpenCV UIImageFromCVMat:undistorted];
        undistorted.release();
        edited = YES;
        

    }
    else
    {
        edited = NO;
    }
   
    // Adjust View is the final view now
    /*MAImagePickerFinalViewController *finalView = [[MAImagePickerFinalViewController alloc] init];
    finalView.sourceImage =  _adjustedImage;
    finalView.imageFrameEdited = edited;
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.4;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
     
    [self.navigationController pushViewController:finalView animated:NO];*/
   
    
    [self.navigationController popViewControllerAnimated:NO];

       NSNotification *nofification = [NSNotification notificationWithName:@"MAIPCSuccessInternal" object:_adjustedImage];
    [[NSNotificationCenter defaultCenter] postNotification:nofification];

        
   
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate
{
    return NO;
}



// http://stackoverflow.com/questions/8667818/opencv-c-obj-c-detecting-a-sheet-of-paper-square-detection
void find_squares(cv::Mat& image, cv::vector<cv::vector<cv::Point>>&squares) {
    
    // blur will enhance edge detection
    cv::Mat blurred(image);
    medianBlur(image, blurred, 9);
    
    cv::Mat gray0(blurred.size(), CV_8U), gray;
    cv::vector<cv::vector<cv::Point> > contours;
    
    // find squares in every color plane of the image
    for (int c = 0; c < 3; c++)
    {
        int ch[] = {c, 0};
        mixChannels(&blurred, 1, &gray0, 1, ch, 1);
        
        // try several threshold levels
        const int threshold_level = 2;
        for (int l = 0; l < threshold_level; l++)
        {
            // Use Canny instead of zero threshold level!
            // Canny helps to catch squares with gradient shading
            if (l == 0)
            {
                Canny(gray0, gray, 10, 20, 3); //
                
                // Dilate helps to remove potential holes between edge segments
                dilate(gray, gray, cv::Mat(), cv::Point(-1,-1));
            }
            else
            {
                gray = gray0 >= (l+1) * 255 / threshold_level;
            }
            
            // Find contours and store them in a list
            findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
            
            // Test contours
            cv::vector<cv::Point> approx;
            for (size_t i = 0; i < contours.size(); i++)
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
                approxPolyDP(cv::Mat(contours[i]), approx, arcLength(cv::Mat(contours[i]), true)*0.02, true);
                
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                if (approx.size() == 4 &&
                    fabs(contourArea(cv::Mat(approx))) > 1000 &&
                    isContourConvex(cv::Mat(approx)))
                {
                    double maxCosine = 0;
                    
                    for (int j = 2; j < 5; j++)
                    {
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }
                    
                    if (maxCosine < 0.3)
                        squares.push_back(approx);
                }
            }
        }
    }
}

void find_largest_square(const cv::vector<cv::vector<cv::Point> >& squares, cv::vector<cv::Point>& biggest_square)
{
    if (!squares.size())
    {
        // no squares detected
        return;
    }
    
    int max_width = 0;
    int max_height = 0;
    size_t max_square_idx = 0;
    
    for (size_t i = 0; i < squares.size(); i++)
    {
        // Convert a set of 4 unordered Points into a meaningful cv::Rect structure.
        cv::Rect rectangle = boundingRect(cv::Mat(squares[i]));
        
        //        cout << "find_largest_square: #" << i << " rectangle x:" << rectangle.x << " y:" << rectangle.y << " " << rectangle.width << "x" << rectangle.height << endl;
        
        // Store the index position of the biggest square found
        if ((rectangle.width >= max_width) && (rectangle.height >= max_height))
        {
            max_width = rectangle.width;
            max_height = rectangle.height;
            max_square_idx = i;
        }
    }
    
    biggest_square = squares[max_square_idx];
}


double angle( cv::Point pt1, cv::Point pt2, cv::Point pt0 ) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

cv::Mat debugSquares( std::vector<std::vector<cv::Point> > squares, cv::Mat image ){
    
    //NSLog(@"DEBUG!/?!");
    for ( unsigned int i = 0; i< squares.size(); i++ ) {
        // draw contour
        
        //NSLog(@"LOOP!");
        
        cv::drawContours(image, squares, i, cv::Scalar(255,0,0), 1, 8, std::vector<cv::Vec4i>(), 0, cv::Point());
        
        // draw bounding rect
        cv::Rect rect = boundingRect(cv::Mat(squares[i]));
        cv::rectangle(image, rect.tl(), rect.br(), cv::Scalar(0,255,0), 2, 8, 0);
        
        // draw rotated rect
        cv::RotatedRect minRect = minAreaRect(cv::Mat(squares[i]));
        cv::Point2f rect_points[4];
        minRect.points( rect_points );
        for ( int j = 0; j < 4; j++ ) {
            cv::line( image, rect_points[j], rect_points[(j+1)%4], cv::Scalar(0,0,255), 1, 8 ); // blue
        }
    }
    
    return image;
}

@end
