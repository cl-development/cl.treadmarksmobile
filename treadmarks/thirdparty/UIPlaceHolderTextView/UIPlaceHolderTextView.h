//
//  UIPlaceHolderTextView.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-27.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>

@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end