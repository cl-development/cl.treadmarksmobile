//
//  NSDictionary+NSDictionary_keys.h
//  AFNetworkingUploadTest
//
//  Created by Dennis Christopher on 2013-12-12.
//  Copyright (c) 2013 Dennis Christopher. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Transaction, Photo, Transaction_TireType, Comment, Transaction_Eligibility, GPSLog, STCAuthorization, ScaleTicket, Transaction_MaterialType;

@interface NSDictionary (NSDictionary_keys)
+(NSDictionary *) dictionaryWithPropertiesOfObject:(id) obj;
+(NSDictionary *) dictionaryForTransaction:(Transaction *)theTransaction;
+(NSDictionary *) dictionaryForPhotoUpload:(Photo *)photo useName:(BOOL)useName;
+(NSDictionary *) dictionaryForTransaction_TireType:(Transaction_TireType *)tireType;
+(NSDictionary *) dictionaryForTransaction_MaterialType:(Transaction_MaterialType *)transMatType;

+(NSDictionary *) dictionaryForCommentUpload:(Comment *)comment;
+(NSDictionary *) dictionaryForTransaction_Eligibility:(Transaction_Eligibility *)transElig;
+(NSDictionary *) dictionaryForTransactionLocation:(GPSLog *)userLog;
+(NSDictionary *) dictionaryForTransaction_STCAuthorization:(STCAuthorization *)stcAuthorization;
+(NSDictionary *) dictionaryForScaleTicket:(ScaleTicket *)scaleTicket;

+ (id)unixTimeStampFromDate:(NSDate *)theDate;
+ (id)unformattedUnixTimeStampFromDate:(NSDate *)theDate;
@end
