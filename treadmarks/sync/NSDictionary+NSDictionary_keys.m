//
//  NSDictionary+NSDictionary_keys.m
//  AFNetworkingUploadTest
//
//  Created by Dennis Christopher on 2013-12-12.
//  Copyright (c) 2013 Dennis Christopher. All rights reserved.
//

#import "NSDictionary+NSDictionary_keys.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Transaction.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "TransactionSyncStatusType.h"
#import "Transaction_TireType.h"
#import "TireType.h"
#import "User.h"
#import "Registrant.h"
#import "GPSLog.h"
#import "Location.h"
#import "Photo.h"
#import "PhotoType.h"
#import "DataEngine.h"
#import "Authorization.h"
#import <objc/runtime.h>

#import "TreadMarks-Swift.h"


@interface NSDictionary(dictionaryWithObject)

+(NSDictionary *) dictionaryWithPropertiesOfObject:(id) obj;

@end
@implementation NSDictionary (NSDictionary_keys)

+(NSDictionary *) dictionaryForCommentUpload:(Comment *)comment
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (comment.commentId != nil) {
        [dict setObject:comment.commentId forKey:@"commentId"];
    }
    if (comment.createdDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:comment.createdDate] forKey:@"createdDate"];
    }
    if (comment.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:comment.syncDate] forKey:@"syncDate"];
    }
    if (comment.text != nil) {
        [dict setObject:comment.text forKey:@"text"];
    }
    if (comment.transaction != nil) {
        [dict setObject:comment.transaction.transactionId forKey:@"transactionId"];
    }
   /* if (comment.user != nil) {
        [dict setObject:comment.user.userId forKey:@"syncDate"];
    }*/
    if (comment.user != nil) {
        [dict setObject:comment.user.userId forKey:@"userId"];
    }

   // //NSLog(@"Comment dict: %@", dict);
    return dict;
}

// use name YES means send json for Photo object
/// Technically, it should not matter whether it's json or multi-part data
/// the content of the record should still be the same.
+(NSDictionary *) dictionaryForPhotoUpload:(Photo *)photo useName:(BOOL)useName
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
	/// Hack -- photoId should be assigned
	if (photo.photoId == nil)
	{
		photo.photoId = [[NSUUID UUID] UUIDString];
	}
	[dict setObject:photo.photoId forKey:@"photoId"];
	
    if (photo.comments != nil) {
        [dict setObject:photo.comments forKey:@"comments"];
    }
    if (photo.createdDate != nil) {
        if (useName==YES) {
            [dict setObject:[self unixTimeStampFromDate:photo.createdDate] forKey:@"createdDate"];
        }
        else {
            [dict setObject:[self unformattedUnixTimeStampFromDate:photo.createdDate] forKey:@"createdDate"];
        }
    }
    if (photo.date != nil) {
        if (useName==YES) {
            [dict setObject:[self unixTimeStampFromDate:photo.date] forKey:@"date"];
        }
        else {
            [dict setObject:[self unformattedUnixTimeStampFromDate:photo.date] forKey:@"date"];
        }
    }
    /* passed with the form data in part 2 so not needed */
	/*
    if (useName==YES) {
	 */
        if (photo.fileName != nil) {
            [dict setObject:photo.fileName forKey:@"fileName"];
         }
    //}
    if (photo.latitude != nil) {
        [dict setObject:photo.latitude forKey:@"latitude"];
    }
    if (photo.longitude != nil) {
        [dict setObject:photo.longitude forKey:@"longitude"];
    }
    if (photo.syncDate != nil) {
        if (useName==YES) {
           [dict setObject:[self unixTimeStampFromDate:photo.syncDate] forKey:@"syncDate"];
        }
        else {
            [dict setObject:[self unformattedUnixTimeStampFromDate:photo.syncDate] forKey:@"syncDate"];
        }
    }
    if (photo.transaction.transactionId != nil) {
        [dict setObject:photo.transaction.transactionId forKey:@"transactionId"];
    }

    if (photo.photoType != nil) {
        [dict setObject:photo.photoType.photoTypeId forKey:@"photoTypeId"];
    }
	
	////NSLog(@"PhotoUpload dictionary:\r\n%@", dict);
    
    return dict;
}

// Feb. 2016: Detect the UCR case and add "nested object" as required

+(NSDictionary *) dictionaryForTransaction:(Transaction *)theTransaction
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if (theTransaction.createdDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:theTransaction.createdDate] forKey:@"createdDate"];
    }
    if (theTransaction.friendlyId != nil) {
        [dict setObject:theTransaction.friendlyId forKey:@"friendlyId"];
    }
    if (theTransaction.incomingSignatureName != nil) {
        [dict setObject:theTransaction.incomingSignatureName forKey:@"incomingSignatureName"];
    }
    if (theTransaction.modifiedDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:theTransaction.modifiedDate] forKey:@"modifiedDate"];
    }
    if (theTransaction.outgoingSignatureName != nil) {
        [dict setObject:theTransaction.outgoingSignatureName forKey:@"outgoingSignatureName"];
    }
    if (theTransaction.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:theTransaction.syncDate] forKey:@"syncDate"];
    }
    if (theTransaction.trailerNumber != nil) {
        [dict setObject:theTransaction.trailerNumber forKey:@"trailerNumber"];
    }
    if (theTransaction.transactionDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:theTransaction.transactionDate] forKey:@"transactionDate"];
    }
        
    if (theTransaction.transactionId != nil) {
        [dict setObject:theTransaction.transactionId forKey:@"transactionId"];
    }
    if (theTransaction.createdUser != nil) {
        [dict setObject:theTransaction.createdUser.userId forKey:@"createdUserId"];
    }
    if (theTransaction.incomingRegistrant != nil) {
        [dict setObject:theTransaction.incomingRegistrant.registrationNumber forKey:@"registrationNumber"];
    }
    if (theTransaction.incomingUser != nil) {
        [dict setObject:theTransaction.incomingUser.userId forKey:@"incomingUserId"];
    }
    if (theTransaction.modifiedUser != nil) {
        [dict setObject:theTransaction.modifiedUser.userId forKey:@"modifiedUserId"];
    }
    if (theTransaction.outgoingUser != nil) {
        [dict setObject:theTransaction.outgoingUser.userId forKey:@"outgoingUserId"];
    }
    if (theTransaction.trailerLocation != nil) {
        [dict setObject:theTransaction.trailerLocation.locationId forKey:@"locationId"];
    }
    if (theTransaction.transactionStatusType != nil) {
        [dict setObject:theTransaction.transactionStatusType.transactionStatusTypeId forKey:@"transactionStatusTypeId"];
    }
    if (theTransaction.transactionSyncStatusType != nil) {
        [dict setObject:theTransaction.transactionSyncStatusType.transactionSyncStatusTypeId forKey:@"transactionSyncStatusTypeId"];
    }
    if (theTransaction.transactionType != nil) {
        [dict setObject:theTransaction.transactionType.transactionTypeId forKey:@"transactionTypeId"];
    }
    if (theTransaction.outgoingGPSLog != nil) {
        [dict setObject:theTransaction.outgoingGPSLog.gpsLogId forKey:@"outgoingGpsLogId"];
    }
    if (theTransaction.incomingGpsLog != nil) {
        [dict setObject:theTransaction.incomingGpsLog.gpsLogId forKey:@"incomingGpsLogId"];
    }
    if (theTransaction.postalCode1 != nil) {
         [dict setObject:theTransaction.postalCode1 forKey:@"postalCode1"];
    }
    if (theTransaction.postalCode2 != nil) {
        [dict setObject:theTransaction.postalCode2 forKey:@"postalCode2"];
    }
	if (theTransaction.incomingSignaturePhoto != nil)
	{
		[dict setObject:theTransaction.incomingSignaturePhoto.photoId forKey:@"incomingSignaturePhotoId"];
	}
	
	if (theTransaction.outgoingSignaturePhoto != nil)
	{
		[dict setObject:theTransaction.outgoingSignaturePhoto.photoId forKey:@"outgoingSignaturePhotoId"];
	}
	
	if (theTransaction.deviceName != nil)
	{
		[dict setObject:theTransaction.deviceName forKey:@"deviceName"];
	}
/*
    if (theTransaction.deviceIDFV != nil)
    {
        [dict setObject:theTransaction.deviceIDFV forKey:@"deviceIDFV"];
    }
*/
	if (theTransaction.outgoingRegistrant != nil)
	{
		[dict setObject:theTransaction.outgoingRegistrant.registrationNumber forKey:@"outgoingRegistrationNumber"];
	}
    
	if (theTransaction.versionBuild != nil)
	{
		[dict setObject:theTransaction.versionBuild forKey:@"versionBuild"];
	}
    if ([theTransaction.outgoingRegistrant.registrationNumber integerValue]==[UCR_REGISTRATION_NUMBER integerValue]) {
        [self addUCRRegistrant:dict forTransaction:theTransaction];
    }
    
    return dict;
}


+(NSDictionary *) dictionaryForTransaction_TireType:(Transaction_TireType *)tireType
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (tireType.quantity != nil) {
        [dict setObject:tireType.quantity forKey:@"quantity"];
    }
    if (tireType.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:tireType.syncDate] forKey:@"syncDate"];
    }
    if (tireType.transactionTireTypeId != nil) {
        [dict setObject:tireType.transactionTireTypeId forKey:@"transactionTireTypeId"];
    }
    if (tireType.tireType != nil) {
        [dict setObject:tireType.tireType.tireTypeId forKey:@"tireTypeId"];
    }
    if (tireType.transaction != nil) {
        [dict setObject:tireType.transaction.transactionId forKey:@"transactionId"];
    }
    
    return dict;
}

+(NSDictionary *)dictionaryForTransaction_MaterialType:(Transaction_MaterialType *)transMatType {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (transMatType.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:transMatType.syncDate] forKey:@"syncDate"];
    }
    if (transMatType.transactionMaterialTypeId != nil) {
        [dict setObject:transMatType.transactionMaterialTypeId forKey:@"transactionMaterialTypeId"];
    }
    if (transMatType.materialType != nil) {
        MaterialType *matType = (MaterialType *)transMatType.materialType;
        [dict setObject:matType.materialTypeId forKey:@"materialTypeId"];
    }
    if (transMatType.transaction != nil) {
        [dict setObject:transMatType.transaction.transactionId forKey:@"transactionId"];
    }
    
    return dict;

}

+(NSDictionary *) dictionaryForTransactionLocation:(GPSLog *)userLog {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (userLog.gpsLogId != nil) {
        [dict setObject:userLog.gpsLogId forKey:@"gpsLogId"];
    }
    if (userLog.latitude != nil) {
        [dict setObject:userLog.latitude forKey:@"latitude"];
    }
    if (userLog.longitude != nil) {
        [dict setObject:userLog.longitude forKey:@"longitude"];
    }
    if (userLog.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:userLog.syncDate] forKey:@"syncDate"];
    }
    if (userLog.timestamp != nil) {
        [dict setObject:[self unixTimeStampFromDate:userLog.timestamp] forKey:@"timestamp"];
    }
    return dict;
}

+(NSDictionary *) dictionaryForTransaction_Eligibility:(Transaction_Eligibility *)transElig
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (transElig.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:transElig.syncDate] forKey:@"syncDate"];
    }
    if (transElig.transactionEligibilityId != nil) {
        [dict setObject:transElig.transactionEligibilityId forKey:@"transactionEligibilityId"];
    }
    if (transElig.value != nil) {
        [dict setObject:transElig.value forKey:@"value"];
    }
    if (transElig.eligibility != nil) {
        [dict setObject:transElig.eligibility.eligibilityId forKey:@"eligibilityId"];
    }
    if (transElig.transaction != nil) {
        [dict setObject:transElig.transaction.transactionId forKey:@"transactionId"];
    }
    
    return dict;
}

// the location is not assigned to the stcAuthorization by the app, so we look it up here
+(NSDictionary *) dictionaryForTransaction_STCAuthorization:(STCAuthorization *)stcAuthorization
{    
    Authorization *authorization = stcAuthorization.authorization;
    Location *location = (Location*)[Location singleById:authorization.transaction.transactionId];
    
    NSMutableDictionary *locDict = nil;
    if (location != nil) {
        locDict = [NSMutableDictionary dictionary];
        if (location.locationId != nil) {
            [locDict setObject:location.locationId forKey:@"locationId"];
        }
        if (location.address1 != nil) {
            [locDict setObject:location.address1 forKey:@"address1"];
        }
        if (location.address2 != nil) {
            [locDict setObject:location.address2 forKey:@"address2"];
        }
        if (location.address3 != nil) {
            [locDict setObject:location.address3 forKey:@"address3"];
        }
        if (location.city != nil) {
            [locDict setObject:location.city forKey:@"city"];
        }
        if (location.country != nil) {
            [locDict setObject:location.country forKey:@"country"];
        }
        if (location.fax != nil) {
            [locDict setObject:location.fax forKey:@"fax"];
        }
        if (location.latitude != nil) {
            [locDict setObject:location.latitude forKey:@"latitude"];
        }
        if (location.longitude != nil) {
            [locDict setObject:location.longitude forKey:@"longitude"];
        }
        if (location.name != nil) {
            [locDict setObject:location.name forKey:@"name"];
        }
        if (location.phone != nil) {
            [locDict setObject:location.phone forKey:@"phone"];
        }
        if (location.postalCode != nil) {
            [locDict setObject:location.postalCode forKey:@"postalCode"];
        }
        if (location.province != nil) {
            [locDict setObject:location.province forKey:@"province"];
        }
        if (location.syncDate != nil) {
            [locDict setObject:[self unixTimeStampFromDate:location.syncDate] forKey:@"syncDate"];
        }
    }
    
    NSMutableDictionary *authDict = nil;
    if (authorization != nil) {
        authDict = [NSMutableDictionary dictionary];
        if (authorization.authorizationId != nil) {
            [authDict setObject:authorization.authorizationId forKey:@"authorizationId"];
        }
        if (authorization.authorizationNumber != nil) {
            [authDict setObject:authorization.authorizationNumber forKey:@"authorizationNumber"];
        }
        if (authorization.expiryDate != nil) {
            [authDict setObject:[self unixTimeStampFromDate:authorization.expiryDate] forKey:@"expiryDate"];
        }
        if (authorization.syncDate != nil) {
            [authDict setObject:[self unixTimeStampFromDate:authorization.syncDate] forKey:@"syncDate"];
        }
        if (authorization.transaction != nil)
		{
            [authDict setObject:authorization.transaction.transactionId forKey:@"transactionId"];
            [authDict setObject:authorization.transaction.transactionType.transactionTypeId forKey:@"transactionTypeId"];
        }
        // required only for sync:
        //authorization.user = authorization.transaction.outgoingUser;
        if (authorization.user != nil) {
            [authDict setObject:authorization.user.userId forKey:@"userId"];
        }
    }
    
   NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    
    if (locDict != nil) {
        [resultDict setObject:locDict forKey:@"location"];
    }
    
    if (authDict != nil) {
        [resultDict setObject:authDict forKey:@"authorization"];
    }
    
    return resultDict;
}

// Added NaN check Dec. 2014.
+(NSDictionary *) dictionaryForScaleTicket:(ScaleTicket *)scaleTicket {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (scaleTicket.date) {
        [dict setObject:[self unixTimeStampFromDate:scaleTicket.date] forKey:@"date"];
    }
    if (scaleTicket.inboundWeight != nil) {
        if ([scaleTicket.inboundWeight isEqualToNumber:[NSDecimalNumber notANumber]]==NO) {
            [dict setObject:scaleTicket.inboundWeight forKey:@"inboundWeight"];
        }
    }
    if (scaleTicket.outboundWeight != nil) {
        if ([scaleTicket.outboundWeight isEqualToNumber:[NSDecimalNumber notANumber]]==NO) {
            [dict setObject:scaleTicket.outboundWeight forKey:@"outboundWeight"];
        }
    }
	
	/// Hack - scaleticketid should always be set
	if (scaleTicket.scaleTicketId == nil)
	{
		scaleTicket.scaleTicketId = [[NSUUID UUID] UUIDString];
	}
	[dict setObject:scaleTicket.scaleTicketId forKey:@"scaleTicketId"];

    if (scaleTicket.sortIndex) {
        [dict setObject:scaleTicket.sortIndex forKey:@"sortIndex"];
    }
    if (scaleTicket.syncDate) {
        [dict setObject:[self unixTimeStampFromDate:scaleTicket.syncDate] forKey:@"syncDate"];
    }
    if (scaleTicket.ticketNumber) {
        [dict setObject:scaleTicket.ticketNumber forKey:@"ticketNumber"];
    }
	
    if (scaleTicket.photo)
	{
		/// Hack so that photoId is assigned to scale ticket photos
		if (scaleTicket.photo.photoId == nil)
		{
			scaleTicket.photo.photoId = [[NSUUID UUID] UUIDString];
			[scaleTicket.managedObjectContext save:nil];
		}

        [dict setObject:scaleTicket.photo.photoId forKey:@"photoId"];
    }
    if (scaleTicket.scaleTicketType) {
        [dict setObject:scaleTicket.scaleTicketType.scaleTicketTypeId forKey:@"scaleTicketTypeId"];
    }
    if (scaleTicket.transaction) {
        [dict setObject:scaleTicket.transaction.transactionId forKey:@"transactionId"];
    }
    if (scaleTicket.unitType) {
        [dict setObject:scaleTicket.unitType.unitTypeId forKey:@"unitTypeId"];
    }
    
    return dict;
}

// NOTE: be careful not to call with mutually referring objects!
+(NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
 
        if ([self isOurCustomClass:key]==YES) {
            id subObj = [self dictionaryWithPropertiesOfObject:[obj valueForKey:key]];
            [dict setObject:subObj forKey:key];
        }
        else if ([key isEqualToString:@"createdDate"] || [key isEqualToString:@"modifiedDate"] || [key isEqualToString:@"syncDate"] || [key isEqualToString:@"transactionDate"] || [key isEqualToString:@"timestamp"] || [key isEqualToString:@"lastAccessDate"]
                 || [key isEqualToString:@"effectiveStartDate"] || [key isEqualToString:@"effectiveEndDate"]
                 || [key isEqualToString:@"date"]) {
            id value = [self unixTimeStampFromDate:[obj valueForKey:key]];
            if(value) [dict setObject:value forKey:key];
        }
        // an exception for photo type
        else if ([key isEqualToString:@"photos"]) {
            [dict setValue:nil forKey:key];
        }
        else
        {
            id value = [obj valueForKey:key];
            if(value) [dict setObject:value forKey:key];
        }
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

#pragma mark - Helpers

+(BOOL)isOurCustomClass:(NSString *)key {
    BOOL result = NO;
    if ([key isEqualToString:@"transactionStatusType"]
        || [key isEqualToString:@"transactionType"]
        || [key isEqualToString:@"transactionSyncStatusType"]
        || [key isEqualToString:@"incomingUser"]
        || [key isEqualToString:@"outgoingUser"]
        || [key isEqualToString:@"createdUser"]
        || [key isEqualToString:@"modifiedUser"]
        || [key isEqualToString:@"incomingRegistrant"]
        || [key isEqualToString:@"incomingSignaturePhoto"]
        || [key isEqualToString:@"outgoingSignaturePhoto"]
        || [key isEqualToString:@"incomingGpsLog"]
        || [key isEqualToString:@"outgoingGPSLog"]
        || [key isEqualToString:@"trailerLocation"]
        /* sub-type member names: */
        || [key isEqualToString:@"location"]
        || [key isEqualToString:@"registrantType"]
        || [key isEqualToString:@"photoType"]
        || [key isEqualToString:@"registrant"]
            ) {
            result = YES;
    }
    return result;
}

+(BOOL)isOurCustomClass2:(id)object {
    BOOL result = NO;
    if ([object isKindOfClass:[TransactionStatusType class]]
        || [object isKindOfClass:[TransactionType class]]
        || [object isKindOfClass:[TransactionSyncStatusType class]]
        || [object isKindOfClass:[User class]]
        || [object isKindOfClass:[Registrant class]]
        || [object isKindOfClass:[Photo class]]
        || [object isKindOfClass:[GPSLog class]]
        || [object isKindOfClass:[Location class]])
    {
        result = YES;
    }
    return result;
}

// convert the date to local time then get its time stamp (date is in GMT by default)
+ (id)unixTimeStampFromDate:(NSDate *)theDate {
    if (theDate==nil) {
        return nil; //
        //[NSNull null];
    }
    
    /* to convert to local time: NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd HH:mm:ss Z"];
    NSTimeZone *localTimeZone = [NSTimeZone systemTimeZone];
    [df setTimeZone: localTimeZone];
    NSString *targetDateString = [df stringFromDate:theDate];
    NSDate *targetDate = [df dateFromString:targetDateString];
    NSString *dateString = [NSString stringWithFormat:@"%.0f", [targetDate timeIntervalSince1970]*1000];*/
    
    NSString *dateString = [NSString stringWithFormat:@"%.0f", [theDate timeIntervalSince1970]*1000];
    NSString *resultStr =  [NSString stringWithFormat:@"/Date(%@)/",dateString];
    return resultStr;
}

+ (id)unformattedUnixTimeStampFromDate:(NSDate *)theDate {
    if (theDate==nil) {
        return nil; //
        //[NSNull null];
    }
    NSString *dateString = [NSString stringWithFormat:@"%.0f", [theDate timeIntervalSince1970]*1000];
    return dateString;
}

#pragma mark - UCR transaction Helpers

+(void)addUCRRegistrant:(NSMutableDictionary *)theDict forTransaction:(Transaction *)theTransaction {
    [theDict setObject:[self dictionaryForRegistrant:theTransaction.outgoingRegistrant] forKey:@"companyInfo"];
}

+(NSDictionary *) dictionaryForRegistrant:(Registrant *)registrant {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (registrant.activationDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.activationDate] forKey:@"activationDate"];
    }
    if (registrant.activeStateChangeDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.activeStateChangeDate] forKey:@"activeStateChangeDate"];
    }
    if (registrant.createdDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.createdDate] forKey:@"createdDate"];
    }
    if (registrant.lastUpdatedDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.lastUpdatedDate] forKey:@"lastUpdatedDate"];
    }
    if (registrant.modifiedDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.modifiedDate] forKey:@"modifiedDate"];
    }
    if (registrant.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:registrant.syncDate] forKey:@"syncDate"];
    }
    if (registrant.businessName != nil) {
        [dict setObject:registrant.businessName forKey:@"businessName"];
    }
    if (registrant.metaData != nil) {
        [dict setObject:registrant.metaData forKey:@"metaData"];
    }
    if (registrant.isActive != nil) {
        [dict setObject:registrant.isActive forKey:@"isActive"];
    }
    if (registrant.registrationNumber != nil) {
        [dict setObject:registrant.registrationNumber forKey:@"registrationNumber"];
    }
    if (registrant.location != nil) {
        [dict setObject:[self dictionaryForLocation:registrant.location] forKey:@"location"];
    }
    if (registrant.registrantType != nil) {
        [dict setObject:registrant.registrantType.registrantTypeId forKey:@"registrantTypeId"];
    }
    return dict;
}


+(NSDictionary *) dictionaryForLocation:(Location *)location {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (location.address1 != nil) {
        [dict setObject:location.address1 forKey:@"address1"];
    }
    if (location.address2 != nil) {
        [dict setObject:location.address2 forKey:@"address2"];
    }
    if (location.address3 != nil) {
        [dict setObject:location.address3 forKey:@"address3"];
    }
    if (location.city != nil) {
        [dict setObject:location.city forKey:@"city"];
    }
    if (location.country != nil) {
        [dict setObject:location.country forKey:@"country"];
    }
    if (location.fax != nil) {
        [dict setObject:location.fax forKey:@"fax"];
    }
    if (location.latitude != nil) {
        [dict setObject:location.latitude forKey:@"latitude"];
    }
    if (location.locationId != nil) {
        [dict setObject:location.locationId forKey:@"locationId"];
    }
    if (location.longitude != nil) {
        [dict setObject:location.longitude forKey:@"longitude"];
    }
    if (location.name != nil) {
        [dict setObject:location.name forKey:@"name"];
    }
    if (location.phone != nil) {
        [dict setObject:location.phone forKey:@"phone"];
    }
    if (location.postalCode != nil) {
        [dict setObject:location.postalCode forKey:@"postalCode"];
    }
    if (location.province != nil) {
        [dict setObject:location.province forKey:@"province"];
    }
    if (location.syncDate != nil) {
        [dict setObject:[self unixTimeStampFromDate:location.syncDate] forKey:@"location"];
    }
    
    return dict;
}


/*+(NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
 {
 NSMutableDictionary *dict = [NSMutableDictionary dictionary];
 
 unsigned count;
 objc_property_t *properties = class_copyPropertyList([obj class], &count);
 
 for (int i = 0; i < count; i++) {
 NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
 [dict setObject:[obj valueForKey:key] forKey:key];
 }
 
 free(properties);
 
 return [NSDictionary dictionaryWithDictionary:dict];
 }*/

/*+(NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
 {
 NSMutableDictionary *dict = [NSMutableDictionary dictionary];
 
 unsigned count;
 objc_property_t *properties = class_copyPropertyList([obj class], &count);
 
 for (int i = 0; i < count; i++) {
 NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
 Class classObject = NSClassFromString([key capitalizedString]);
 if (classObject) {
 id subObj = [self dictionaryWithPropertiesOfObject:[obj valueForKey:key]];
 [dict setObject:subObj forKey:key];
 }
 else
 {
 id value = [obj valueForKey:key];
 if(value) [dict setObject:value forKey:key];
 }
 }
 
 free(properties);
 
 return [NSDictionary dictionaryWithDictionary:dict];
 }*/



@end
