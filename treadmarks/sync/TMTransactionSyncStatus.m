//
//  TMTransactionSyncStatus.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-10.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TMTransactionSyncStatus.h"

@interface TMTransactionSyncStatus ()


@end

@implementation TMTransactionSyncStatus

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.hasTireCounts = NO;
        self.hasComments = NO;
        self.hasEligibilities = NO;

        self.transactionRecordSynced = NO;
        self.transactionRecordError = nil;
        self.transactionPhotosSynced = NO;
        self.transactionImagesError = nil;
        self.transactionTireCountsSynced = NO;
        self.transactionTireCountsError = nil;
        self.transactionCommentsSynced = NO;
        self.transactionCommentsError = nil;
        self.transactionEligibilitiesSynced = NO;
        self.transactionEligibilitiesError = nil;
        self.transactionGPSlogsSynced = NO;
        self.transactionGPSlogsError = nil;
    }
    return self;
}

-(BOOL)isSynced {
    BOOL synced = self.transactionRecordSynced;
    if (self.hasTireCounts==YES) {
        synced &= self.transactionTireCountsSynced;
    }
    if (self.hasComments==YES) {
        synced &= self.transactionCommentsSynced;
    }
    if (self.hasEligibilities==YES) {
        synced &= self.transactionEligibilitiesSynced;
    }
    return synced;
}

@end
