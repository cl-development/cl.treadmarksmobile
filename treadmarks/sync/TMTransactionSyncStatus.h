//
//  TMTransactionSyncStatus.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-10.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TMTransactionSyncStatus : NSObject

@property (nonatomic, assign) BOOL hasTireCounts;
@property (nonatomic, assign) BOOL hasComments;
@property (nonatomic, assign) BOOL hasEligibilities;

@property (nonatomic, assign) BOOL transactionRecordSynced;
@property (nonatomic, assign) BOOL transactionPhotosSynced;
@property (nonatomic, assign) BOOL transactionTireCountsSynced;
@property (nonatomic, assign) BOOL transactionCommentsSynced;
@property (nonatomic, assign) BOOL transactionEligibilitiesSynced;
@property (nonatomic, assign) BOOL transactionGPSlogsSynced;
@property (nonatomic, assign) BOOL transactionSTCAuthSynced;

@property (nonatomic, strong) NSError *transactionRecordError;
@property (nonatomic, strong) NSError *transactionTireCountsError;
@property (nonatomic, strong) NSError *transactionCommentsError;
@property (nonatomic, strong) NSError *transactionEligibilitiesError;
@property (nonatomic, strong) NSError *transactionImagesError;
@property (nonatomic, strong) NSError *transactionGPSlogsError;
@property (nonatomic, strong) NSError *transactionSTCAuthError;

-(BOOL)isSynced;
@end
