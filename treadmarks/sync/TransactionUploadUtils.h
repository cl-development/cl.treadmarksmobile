//
//  TransactionUploadUtils.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Transaction, Comment, STCAuthorization, Transaction_MaterialType;

@interface TransactionUploadUtils : NSObject
+(NSMutableArray *)tireCountsForTransaction:(Transaction *)transaction;
+(Transaction_MaterialType *)transactionMaterialTypeForTransaction:(Transaction *)transaction;
//+(Comment *)commentForTransaction:(Transaction *)transaction;
+(NSArray *)commentsForTransaction:(Transaction *)transaction;
+(NSArray *)eligibilitiesForTransaction:(Transaction *)transaction;
+(STCAuthorization *)stcAuthorizationForTransaction:(Transaction *)transaction;
+(NSArray *)scaleTicketsForTransaction:(Transaction *)transaction;
@end
