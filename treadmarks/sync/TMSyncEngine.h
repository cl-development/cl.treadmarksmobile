//
//  TMSyncEngine.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-02.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Transaction, TMTransactionSyncStatus;

typedef enum {
    TMObjectSynced = 0,
    TMObjectCreated,
    TMObjectDeleted
} TMObjectSyncStatus;

// don't modify this unless you know what you are doing:
#define TRANSACTION_FETCH_LIMIT_SIZE 1  //6

#define CORE_DATA_SAVE_ERROR -5000

@protocol TMSyncEngineDelegate <NSObject>

@required

-(void) transactionDidUpload:(Transaction *)transaction withStatuses:(TMTransactionSyncStatus *)status;
-(void) transactionUploadDidFail:(Transaction *)transaction withError:(NSError *)error;
-(void) syncDidComplete:(NSUInteger)syncedObjectCount withError:(NSError *)error;
-(void) syncDates:(NSArray *)arrayOfDates;

@optional

@end


@interface TMSyncEngine : NSObject
@property (atomic, readonly) BOOL syncInProgress;
@property (nonatomic, weak) id delegate;


+ (TMSyncEngine *)sharedEngine;

// "Sync back"
- (void)registerNSManagedObjectClassesToSync:(NSArray *)classes;
- (void)startSync;
- (void)pullData;

// "Sync up"
-(void)uploadTransaction:(Transaction *)theTransaction;
-(void) cancel;
-(NSDate *)unixDateToNSDate:(NSString *)unixDateString;

// accessing NSUserDefaults
-(NSDate *) getLastUpdatedDate:(NSString *)className;
-(void)setLastUpdatedDate:(NSDate *)updatedDate forClass:(NSString *)className;

@end
