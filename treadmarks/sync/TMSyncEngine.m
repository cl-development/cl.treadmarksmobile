//
//  TMSyncEngine.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-02.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import "TMSyncEngine.h"
#import "TMAFCLient.h"
#import "TransactionUploadUtils.h"
#import "Transaction.h"
#import "DataEngine.h"
#import "NSDictionary+NSDictionary_keys.h"
#import "NSManagedObject+JSON.h"
#import "NSManagedObject+NSManagedObject.h"
#import "TMTransactionSyncStatus.h"
#import "PhotoType.h"
#import "Location.h"
#import "TireType.h"
#import "Utils.h"
#import "Authorization.h"
#import "TransactionType_TireType.h"
#import "TransactionType_RegistrantType.h"
#import "Constants.h"

#import "TreadMarks-Swift.h"

NSString * const kTMSyncEngineInitialCompleteKey = @"TMSyncEngineInitialSyncCompleted";
NSString * const kTMSyncEngineSyncCompletedNotificationName = @"TMSyncEngineSyncCompleted";
NSInteger kTM_Response_Success = 1;
NSInteger kTM_OPERATION_CANCELLED = -999;


@interface TMSyncEngine ()
@property (nonatomic, strong) NSMutableArray *registeredClassesToSync;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic) dispatch_queue_t saveOperationQueue;

//@property (nonatomic, strong) TMTransactionSyncStatus *syncStatus;

#pragma mark - Locks

@property dispatch_semaphore_t userOperationSuccessLock;
@property dispatch_semaphore_t registrantOperationSuccessLock;

@end

@interface TMSyncEngine()
{
    BOOL _cancelRequested;
    BOOL _pullInProgress;
    AFHTTPRequestOperation *_currentOperation;
}
@end
@implementation TMSyncEngine

+ (TMSyncEngine *)sharedEngine {
    static TMSyncEngine *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[TMSyncEngine alloc] init];
        sharedEngine->_saveOperationQueue = dispatch_queue_create("save_data_operation", DISPATCH_QUEUE_SERIAL);
    });
    
    return sharedEngine;
}

#pragma mark - Upload transactions et al

-(void)uploadTransaction:(Transaction *)theTransaction {
    
    //self.syncStatus = [[TMTransactionSyncStatus alloc] init];
    NSMutableArray *photos = [self fetchTransactionImages:theTransaction];
    if ([photos count]>0) {
        [self uploadTransactionImages:photos];
    }
    else {
        [self enqueueTransactionAndObjects:theTransaction];
    }
}
-(void)pullData {
    [self enqueuePullData];
}

-(void) enqueueTransactionAndObjects:(Transaction *)theTransaction {
    NSMutableArray *operations = [NSMutableArray array];
    //    __block NSError *operationError = nil;
    
    // 1 create the request for the transaction  record upload
    AFHTTPRequestOperation *transactionOperation = [self transactionRecordUploadOperation:theTransaction];
    
    if (theTransaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT) {
        AFHTTPRequestOperation *matTypeOperation = [self transactionMaterialTypeUploadOperation:theTransaction];
        if (matTypeOperation != nil) {
            [transactionOperation addDependency:matTypeOperation];
            [operations addObject:matTypeOperation];
        }
    }
    else {
        // 2 get the tire counts request operation and add it
        AFHTTPRequestOperation *tireCountOperation = [self tireCountsUploadOperation:theTransaction];
        if (tireCountOperation != nil) {
            [transactionOperation addDependency:tireCountOperation];
            [operations addObject:tireCountOperation];
        }
    }
    
    // 3 get the (optional) comment request operation and add it
    AFHTTPRequestOperation *commentUploadOperation = [self commentsUploadOperation:theTransaction];
    if (commentUploadOperation != nil) {
        [transactionOperation addDependency:commentUploadOperation];
        [operations addObject:commentUploadOperation];
    }
    
    // 4 get the transaction eligibilities operation and add it
    AFHTTPRequestOperation *eligUploadOperation = [self eligibilitiesUploadOperation:theTransaction];
    if (eligUploadOperation != nil) {
        [transactionOperation addDependency:eligUploadOperation];
        [operations addObject:eligUploadOperation];
    }
    
    // 5 get the transaction GPSLogUploadOperation and add it
    AFHTTPRequestOperation *gpsLogUploadOperation = [self GPSLogUploadOperation:theTransaction];
    if (gpsLogUploadOperation != nil) {
        [transactionOperation addDependency:gpsLogUploadOperation];
        [operations addObject:gpsLogUploadOperation];
    }
    
    // 6 get the transaction STCAuthorization operation and add it
    AFHTTPRequestOperation *stcAuthUploadOperation = [self STCAuthorizationUploadOperation:theTransaction];
    if (stcAuthUploadOperation != nil) {
        [transactionOperation addDependency:stcAuthUploadOperation];
        [operations addObject:stcAuthUploadOperation];
    }
    
    // 7 get the ScaleTicket operation and add it
    
    AFHTTPRequestOperation *scaleTicketUploadOperation = [self scaleTicketUploadOperation:theTransaction];
    if (scaleTicketUploadOperation) {
        [transactionOperation addDependency:scaleTicketUploadOperation];
        [operations addObject:scaleTicketUploadOperation];
    }
    
    
    [operations addObject:transactionOperation];
    
    // Moved to enqueuePullData
    // 7 Tack on the SyncDatesList operation, ensuring it executes last
    /*AFHTTPRequestOperation *syncDatesListOperation =  [self getSyncDatesList];
     if (syncDatesListOperation != nil) {
     [syncDatesListOperation addDependency:transactionOperation];
     [operations addObject:syncDatesListOperation];
     }*/
    
    // enqueue the operations
    // Pass off operations array to the sharedClient so that they are all executed
    [[TMAFCLient sharedClient] enqueueBatchOfHTTPRequestOperations:operations progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
        ////NSLog(@"Completed %lu of %lu create operations.", (unsigned long)numberOfCompletedOperations, (unsigned long)totalNumberOfOperations);
    } completionBlock:^(NSArray *operations) {
        if ([operations count] > 0) {
            //self.syncStatus.transactionRecordSynced = YES;
        }
    }];
}
-(void) enqueuePullData {
    
    NSMutableArray *operations = [NSMutableArray array];
    
    // 7 Tack on the SyncDatesList operation, ensuring it executes last
    AFHTTPRequestOperation *syncDatesListOperation =  [self getSyncDatesList];
    if (syncDatesListOperation != nil) {
        //        [syncDatesListOperation addDependency:nil];
        [operations addObject:syncDatesListOperation];
    }
    
    // enqueue the operations
    // Pass off operations array to the sharedClient so that they are all executed
    [[TMAFCLient sharedClient] enqueueBatchOfHTTPRequestOperations:operations progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
        NSLog(@"Completed %lu of %lu create operations.", (unsigned long)numberOfCompletedOperations, (unsigned long)totalNumberOfOperations);
    } completionBlock:^(NSArray *operations) {
        if ([operations count] > 0) {
            //self.syncStatus.transactionRecordSynced = YES;
        }
    }];
}

// types: photos, scale ticket photos, signatures photos
-(void)uploadTransactionImages:(NSMutableArray *)photosArray {
    NSMutableArray *operations = [NSMutableArray array];
    __block NSError *operationError = nil;
    
    // Iterate over all photos to upload
    for (Photo *photo in photosArray) {
        // Create a request using TMAFCLient POST method
        NSMutableURLRequest *request = nil;
        if (photo.imageSyncDate==nil)    {
            request = [[TMAFCLient sharedClient] POSTRequestForTransactionImage:@"FileUpload" photo:photo];
        }
        else { // just upload the photo dict
            request = [[TMAFCLient sharedClient] POSTRequestForTransactionPhoto:@"SyncPhoto" photo:photo];
        }
        
        /// If request is nil, no operation required.
        if (request == nil)
        {
            continue;
        }
        
        AFHTTPRequestOperation *operation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (photo.imageSyncDate!=nil) {
                NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncPhotoResult"];
                // HACK for case of copy scale ticket (same as inbound) having
                // the same photo record and imageSyncDate being changed in success block
                // handle same as image upload case
                if (responseDictionary==nil) {
                    NSDictionary *responseDictionary = responseObject;
                    NSInteger responseCode =  [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
                    if (responseCode==kTM_Response_Success) {
                        photo.syncDate = photo.imageSyncDate = [self extractDate:responseDictionary];
                        //NSLog(@"Success photo+image upload operation at: %@", photo.syncDate);
                    }
                    else {
                        //NSLog(@"Failed creating operation for upload photo+image: %ld",(long)responseCode);
                        operationError = [Utils errorForType:@"Upload Photo+Image" errorCode:responseCode];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
                    }
                }
                else {
                    NSInteger responseCode =  [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
                    if (responseCode==kTM_Response_Success) {
                        photo.syncDate = [self extractDate:responseDictionary];
                        //NSLog(@"Success photo upload operation at: %@", photo.syncDate);
                    }
                    else {
                        //NSLog(@"Failed creating operation for upload photo: %ld",(long)responseCode);
                        operationError = [Utils errorForType:@"Upload Photo" errorCode:responseCode];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
                    }
                }
            }
            else {
                NSDictionary *responseDictionary = responseObject;
                NSInteger responseCode =  [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
                if (responseCode==kTM_Response_Success) {
                    //self.syncStatus.transactionPhotosSynced = YES;
                    photo.syncDate = photo.imageSyncDate = [self extractDate:responseDictionary];
                    //NSLog(@"Success photo+image upload operation at: %@", photo.syncDate);
                }
                else {
                    
                    //NSLog(@"Failed creating operation for upload photo+image: %ld",(long)responseCode);
                    operationError = [Utils errorForType:@"Upload Photo+Image" errorCode:responseCode];
                    //self.syncStatus.transactionPhotosSynced = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
                }
            }
        }
                                                                                               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                                                   //NSLog(@"Failed creating operation for upload photo/image: %@", error);
                                                                                                   //self.syncStatus.transactionPhotosSynced = NO;
                                                                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
                                                                                                   operationError = error;
                                                                                               }];
        // Add all operations to the operations NSArray
        [operations addObject:operation];
    }
    
    // Pass off photo operations array to the sharedClient so that they are all executed
    [[TMAFCLient sharedClient] enqueueBatchOfHTTPRequestOperations:operations progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
        ////NSLog(@"Completed %lu of %lu create operations for photo upload", (unsigned long)numberOfCompletedOperations, (unsigned long)totalNumberOfOperations);
    } completionBlock:^(NSArray *operations) {
        // Set the completion block to do something
        if ([operations count] > 0) {
            // Do something
        }
        // Invoke executeSyncCompletionOperations as this is now the final step of the sync engine's flow
        if (operationError != nil) {
            if (self.delegate != nil) {
                [self.delegate transactionUploadDidFail:nil withError:operationError];
                //self.syncStatus.transactionImagesError = operationError;
            }
        }
        else {
            Photo *aPhoto = [photosArray objectAtIndex:0];
            Transaction *theTransaction = aPhoto.transaction;
            [self enqueueTransactionAndObjects:theTransaction];
        }
    }];
    
}


// create the request for the ScaleTicket upload
- (AFHTTPRequestOperation *)scaleTicketUploadOperation:(Transaction *)theTransaction {
    NSArray *scaleTickets = [TransactionUploadUtils scaleTicketsForTransaction:theTransaction];
    if (!scaleTickets || [scaleTickets count]==0) {
        return nil;
    }
    else {
        NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForScaleTicket:@"SyncScaleTicket" scaleTickets:scaleTickets];
        __block NSError *operationError;
        
        AFHTTPRequestOperation *scaleTicketOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncScaleTicketResult"];
            if ([[responseDictionary valueForKey:@"ResponseStatus"] integerValue]==1) {
                NSDate *syncedDate = [self extractDate:responseDictionary];
                NSLog(@"Success for ScaleTicket operation at: %@", syncedDate);
                for (ScaleTicket *scaleTicket in scaleTickets) {
                    scaleTicket.syncDate = syncedDate;
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Failed creating ScaleTicket  operation for upload: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }];
        
        return scaleTicketOperation;
    }
}
-(void) cancel {
    _cancelRequested = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_currentOperation cancel];
        [[TMAFCLient sharedClient].operationQueue cancelAllOperations];
        _syncInProgress = NO;
    });
}

#pragma mark - Transaction operations

-(AFHTTPRequestOperation *) transactionRecordUploadOperation:(Transaction *)theTransaction {
    __block NSError *operationError = nil;
    
    // create the request for the transaction  record upload
    NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForTransaction:@"SyncTransaction" transaction:theTransaction];
    
    AFHTTPRequestOperation *transactionOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncTransactionResult"];
        if ([[responseDictionary valueForKey:@"ResponseStatus"] integerValue]==kTM_Response_Success) {
            //self.syncStatus.transactionRecordSynced = YES;
            NSDate *syncedDate = [self extractDate:responseDictionary];
            theTransaction.syncDate = syncedDate;
            NSLog(@"Success for transaction operation at: %@", syncedDate);
            
            NSNumber *syncStatusNum = [responseDictionary valueForKey:@"TransactionSyncStatusTypeId"];
            [self executeSyncCompletedOperations:theTransaction withStatus:syncStatusNum];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload transaction.");
        }
        else {
            //added on 18thjune
            NSNumber *syncStatusNum = TRANSACTION_SYNC_STATUS_TYPE_ID_SYNCHRONIZED_FAILED;
            //[self executeSyncFinishedMethod:theTransaction statusType:syncStatusNum];
            [self executeSyncCompletedOperations:theTransaction withStatus:syncStatusNum];
            //NSLog(@"Failed creating operation for upload transaction: %@", error);
            operationError = error;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
        }
    }];
    return transactionOperation;
}

//  get the eligibilities request operation
-(AFHTTPRequestOperation *) eligibilitiesUploadOperation:(Transaction *)theTransaction {
    NSArray *eligs = [TransactionUploadUtils eligibilitiesForTransaction:theTransaction];
    if (eligs==nil || [eligs count]==0) {
        return nil;
    }
    
    NSMutableURLRequest *eligRequest = [[TMAFCLient sharedClient] POSTRequestForTransactionEligibilities:@"SyncTransaction_Eligibility" eligibilities:eligs];
    __block NSError *operationError = nil;
    
    AFHTTPRequestOperation *eligOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:eligRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject objectForKey:@"SyncTransaction_EligibilityResult"];
        NSInteger responseCode = [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
        if (responseCode==kTM_Response_Success) {
            //self.syncStatus.transactionEligibilitiesSynced = YES;
            NSDate *syncedDate = [self extractDate:responseDictionary];
            //NSLog(@"Success for eligibilities operation at: %@", syncedDate);
            for (Transaction_Eligibility *elig in eligs){
                elig.syncDate = syncedDate;
            }
            //self.syncStatus.transactionEligibilitiesSynced = YES;
        }
        else {
            //NSLog(@"Failed creating operation for upload eligibilities: %ld", (long)responseCode);
            operationError = [Utils errorForType:@"Upload Eligibility" errorCode:responseCode];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload eligibilities.");
        }
        else {
            //NSLog(@"Failed creating operation for upload eligibilities: %@", error);
            operationError = error;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
        }
        //self.syncStatus.transactionEligibilitiesSynced = NO;
        //self.syncStatus.transactionEligibilitiesError = error;
    }];
    return eligOperation;
}

//  get the tire counts request operation
-(AFHTTPRequestOperation *) tireCountsUploadOperation:(Transaction *)theTransaction {
    NSArray *tireCounts = [TransactionUploadUtils tireCountsForTransaction:theTransaction];
    if (tireCounts==nil || [tireCounts count]==0) {
        return nil;
    }
    
    NSMutableURLRequest *tireCountRequest = [[TMAFCLient sharedClient] POSTRequestForTransactionTireCounts:@"SyncTransaction_TireType" tireCounts:tireCounts];
    __block NSError *operationError = nil;
    
    AFHTTPRequestOperation *tireCountOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:tireCountRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject objectForKey:@"SyncTransaction_TireTypeResult"];
        NSInteger responseCode = [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
        if (responseCode==kTM_Response_Success) {
            //self.syncStatus.transactionTireCountsSynced = YES;
            NSDate *syncedDate = [self extractDate:responseDictionary];
            NSLog(@"Success for tireCounts operation at: %@", syncedDate);
            for (Transaction_TireType *tireType in tireCounts){
                tireType.syncDate = syncedDate;
            }
            //self.syncStatus.transactionTireCountsSynced = YES;
        }
        else {
            //NSLog(@"Failed creating operation for upload tireCounts: %ld", (long)responseCode);
            //self.syncStatus.transactionTireCountsSynced = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = [Utils errorForType:@"Upload tireCounts" errorCode:responseCode];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload tire counts.");
        }
        else {
            //NSLog(@"Failed creating operation for upload tire counts: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }
    }];
    return tireCountOperation;
}

//  get the tire counts request operation
-(AFHTTPRequestOperation *) transactionMaterialTypeUploadOperation:(Transaction *)theTransaction {
    Transaction_MaterialType *transMatType = [TransactionUploadUtils transactionMaterialTypeForTransaction:theTransaction];
    if (transMatType==nil) {
        return nil;
    }
    NSMutableURLRequest *transMatTypeRequest = [[TMAFCLient sharedClient] POSTRequestForTransactionMaterialType:@"SyncTransaction_MaterialType" transMatType:transMatType];
    __block NSError *operationError = nil;
    
    AFHTTPRequestOperation *transMatTypeOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:transMatTypeRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject objectForKey:@"SyncTransaction_MaterialTypeResult"];
        NSInteger responseCode = [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
        if (responseCode==kTM_Response_Success) {
            NSDate *syncedDate = [self extractDate:responseDictionary];
            NSLog(@"Success for transMatType operation at: %@", syncedDate);
            transMatType.syncDate = syncedDate;
        }
        else {
            NSLog(@"Failed creating operation for upload transMatType: %ld", (long)responseCode);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = [Utils errorForType:@"Upload transMatType" errorCode:responseCode];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload tire counts.");
        }
        else {
            NSLog(@"Failed creating operation for upload transMatType: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }
    }];
    return transMatTypeOperation;
}


// get the comment request operation
-(AFHTTPRequestOperation *) commentsUploadOperation:(Transaction *)theTransaction {
    NSArray *comments = [TransactionUploadUtils commentsForTransaction:theTransaction];
    if (comments==nil) {
        return nil;
    }
    
    NSMutableArray *commentsDictionaryArray = [NSMutableArray array];
    for (Comment *comment in comments) {
        NSDictionary *commentDictionary = [NSDictionary dictionaryForCommentUpload:comment];
        [commentsDictionaryArray addObject:commentDictionary];
    }
    
    NSMutableURLRequest *commentRequest = [[TMAFCLient sharedClient] //POSTRequestForTransactionComments:@"SyncComment" comment:commentsDictionaryArray];
                                           POSTRequestForTransactionComments:@"SyncComments" comment:commentsDictionaryArray];
    __block NSError *operationError = nil;
    
    AFHTTPRequestOperation *commentOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:commentRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncCommentResult"];
        NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncCommentsResult"];
        NSInteger responseCode = [[responseDictionary valueForKey:@"ResponseStatus"] integerValue];
        if (responseCode==kTM_Response_Success) {
            NSLog(@"Successful operation for upload comments: %ld", (long)responseCode);
            //self.syncStatus.transactionCommentsSynced = YES;
            NSDate *syncedDate = [self extractDate:responseDictionary];
            for (Comment *comment in comments) {
                comment.syncDate = syncedDate;
            }
        }
        else {
            NSLog(@"Error creating operation for upload comments: %ld", (long)responseCode);
            //self.syncStatus.transactionCommentsSynced = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = [Utils errorForType:@"Upload comments" errorCode:responseCode];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload comments.");
        }
        else {
            //NSLog(@"Failed creating operation for upload comments: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }
    }];
    return commentOperation;
}

// create the request for the GPSlog records upload
-(AFHTTPRequestOperation *) GPSLogUploadOperation:(Transaction *)theTransaction {
    __block NSError *operationError = nil;
    
    NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForGPSlog:@"SyncGPSLog" transaction:theTransaction];
    if (request==nil) {
        return nil;
    }
    
    AFHTTPRequestOperation *gpsLogsOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncGpsLogResult"];
        if ([[responseDictionary valueForKey:@"ResponseStatus"] integerValue]==kTM_Response_Success) {
            NSDate *syncedDate = [self extractDate:responseDictionary];
            //NSLog(@"Success for gpsLogsOperation operation at: %@", syncedDate);
            //self.syncStatus.transactionGPSlogsSynced = YES;
            
            if (theTransaction.incomingGpsLog != nil)
            {
                theTransaction.incomingGpsLog.syncDate = syncedDate;
            }
            if (theTransaction.outgoingGPSLog != nil)
            {
                theTransaction.outgoingGPSLog.syncDate = syncedDate;
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload gpsLog.");
        }
        else {
            //NSLog(@"Failed creating operation for gpsLog: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }
    }];
    return gpsLogsOperation;
}

// create the request for the STCAuthorization upload
-(AFHTTPRequestOperation *) STCAuthorizationUploadOperation:(Transaction *)theTransaction {
    STCAuthorization *auth = [TransactionUploadUtils stcAuthorizationForTransaction:theTransaction];
    if (auth==nil) {
        return nil;
    }
    __block NSError *operationError = nil;
    
    NSLog(@"what is the userId for authorization? %@", auth.authorization.user.userId);
    
    Authorization *authorization = auth.authorization;
    Location *location = auth.location;
    
    NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForAuthorization:@"SyncSTCAuthorization" authorization:auth];
    
    AFHTTPRequestOperation *authOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject valueForKey:@"SyncSTCAuthorizationResult"];
        if ([[responseDictionary valueForKey:@"ResponseStatus"] integerValue]==kTM_Response_Success) {
            NSDate *syncedDate = [self extractDate:responseDictionary];
            NSLog(@"Success for STCAuthorization operation at: %@", syncedDate);
            
            /// This needs to update the sync date for STCAuthorization, Authorization and Location
            /*Authorization* authorization = (Authorization*)[Authorization single:@"transaction == %@", theTransaction];
             STCAuthorization* stcAuthorization = [STCAuthorization single:@"authorization == %@", authorization];
             Location* location = stcAuthorization.location;
             authorization.syncDate = syncedDate;
             location.syncDate = syncedDate;*/
            
            authorization.syncDate = syncedDate;
            location.syncDate = syncedDate;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for upload stc suthorization.");
        }
        else {
            //NSLog(@"Failed creating operation for stc suthorization: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            
            operationError = error;
        }
    }];
    return authOperation;
}

- (void)executeSyncCompletedOperations:(Transaction *)theTransaction withStatus:(NSNumber *)theStatus {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = NO;
        [self didChangeValueForKey:@"syncInProgress"];
        // save to record the sync date and status changes
        theTransaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:theStatus];
        [[DataEngine sharedInstance] saveContext];
        if (self.delegate != nil) {
            [self.delegate transactionDidUpload:theTransaction withStatuses:nil];
        }
    });
}

- (void)executeGetSyncDatesCompletedOperations:(NSArray *)datesArray {
    if (self.delegate != nil) {
        [self.delegate syncDates:datesArray];
    }
}


-(NSMutableArray *)fetchTransactionImages:(Transaction *)transaction {
    NSMutableArray *photosArray = [NSMutableArray array];
    
    Photo *incoming = transaction.incomingSignaturePhoto;
    if (incoming != nil) {
        [photosArray addObject:incoming];
    }
    Photo *outgoing = transaction.outgoingSignaturePhoto;
    if (outgoing != nil) {
        [photosArray addObject:outgoing];
    }
    NSArray *transPhotosArray = [self safeGetTransactionPhotos:transaction];
    for (Photo *photo in transPhotosArray) {
        [photosArray addObject:photo];
    }
    NSArray *scaleTicketArray = [self safeGetTransactionScaleTickets:transaction];
    for (ScaleTicket *ticket in scaleTicketArray) {
        if (ticket.photo != nil) {
            [photosArray addObject:ticket.photo];
        }
    }
    return photosArray;
}

-(NSArray *)safeGetTransactionPhotos:(Transaction *)transaction {
    __block NSArray *photosArray = nil;
    
    if ([NSThread isMainThread]==YES) {
        photosArray = [[DataEngine sharedInstance] photosForSync:transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
    }
    else {
        /*dispatch_sync(dispatch_get_main_queue(), ^{
         photosArray = [[DataEngine sharedInstance] photosForSync:transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
         });*/
        NSManagedObjectContext *main = [transaction managedObjectContext];
        [main performBlockAndWait:^{
            photosArray = [[DataEngine sharedInstance] photosForSync:transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        }];
        
    }
    return photosArray;
}

-(NSArray *)safeGetTransactionScaleTickets:(Transaction *)transaction {
    __block NSArray *scaleTicketArray = nil;
    
    if ([NSThread isMainThread]==YES) {
        scaleTicketArray = [[DataEngine sharedInstance] scaleTicketsForTransaction:transaction];
    }
    else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            scaleTicketArray = [[DataEngine sharedInstance] scaleTicketsForTransaction:transaction];
        });
    }
    return scaleTicketArray;
}


#pragma mark - getSyncDatesList

-(AFHTTPRequestOperation *) getSyncDatesList {
    NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForSyncDatesList];
    __block NSError *operationError = nil;
    
    AFHTTPRequestOperation *getSyncListOperation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *responseDictionary = [responseObject valueForKey:@"GetSyncDatesListResult"];
        if ([[responseDictionary valueForKey:@"ResponseStatus"] integerValue]==kTM_Response_Success) {
            NSArray *syncedDatesArray = [responseDictionary valueForKey:@"SyncDatesList"];
            //NSLog(@"Success for getSyncListOperation operation.");
            [self executeGetSyncDatesCompletedOperations:[self getSyncDatesFromArray:syncedDatesArray]];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            //NSLog(@"Cancelled operation for get sync date list.");
        }
        else {
            //NSLog(@"Failed creating operation for for get sync date list: %@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
            operationError = error;
        }
    }];
    return getSyncListOperation;
}

#pragma mark - Get fixed data methods
#pragma mark _simplified_sync

- (void)startSync {
    if (!self.syncInProgress) {
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = YES;
        [self didChangeValueForKey:@"syncInProgress"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self downloadDataForRegisteredObjects:YES toDeleteLocalRecords:NO];
        });
    }
}

// NOTE: this relies on the server always sending the correct sets - e.g. Location must be there is Registrant is
- (void)downloadDataForRegisteredObjects:(BOOL)useUpdatedAtDate toDeleteLocalRecords:(BOOL)toDelete{
    @try {
        
        BOOL registeredClassesContainRegistrant = [self.registeredClassesToSync containsObject:REGISTRANT_ENTITY_NAME];
        BOOL registeredClassesContainUser = [self.registeredClassesToSync containsObject:USER_ENTITY_NAME];
        BOOL registeredClassesContainLocation = [self.registeredClassesToSync containsObject:LOCATION_ENTITY_NAME];

        NSString *lastObjectClassName = nil;
        _cancelRequested = NO;
        _pullInProgress = YES;
        // we have to process the classes in order if we have dependencies.
        
        if (registeredClassesContainUser==YES) {
            _currentOperation = [self downloadOperationFor:LOCATION_ENTITY_NAME isLast:NO completion:^(bool success) {
                if (success && !_cancelRequested)
                {
                    _currentOperation = [self downloadOperationFor:REGISTRANT_ENTITY_NAME isLast:NO completion:^(bool success) {
                        if (success && !_cancelRequested)
                        {
                            _currentOperation = [self downloadOperationFor:USER_ENTITY_NAME isLast:YES completion:^(bool success) {
                                _pullInProgress = NO;
                                _currentOperation = nil;
                            }];
                        }
                        else {
                            _pullInProgress = NO;
                            _currentOperation = nil;
                        }
                    }];
                }
                else {
                    _pullInProgress = NO;
                    _currentOperation = nil;
                }
            }];
        }
        else if (registeredClassesContainRegistrant==YES) {
            
            _currentOperation = [self downloadOperationFor:LOCATION_ENTITY_NAME isLast:NO completion:^(bool success) {
                if (success && !_cancelRequested)
                {
                    _currentOperation = [self downloadOperationFor:REGISTRANT_ENTITY_NAME isLast:YES completion:^(bool success) {
                        _pullInProgress = NO;
                        _currentOperation = nil;
                    }];
                }
                else {
                    _pullInProgress = NO;
                    _currentOperation = nil;
                }
            }];
        }
        else if (registeredClassesContainLocation==YES) {
            _currentOperation = [self downloadOperationFor:LOCATION_ENTITY_NAME isLast:YES completion:^(bool success) {
                _pullInProgress = NO;
                _currentOperation = nil;
            }];
        }
        else {  // designate an arbitrary "last synced item" so we will know when pull is complete.
            lastObjectClassName = [self.registeredClassesToSync lastObject];
            if (lastObjectClassName != nil) {
                _currentOperation = [self downloadOperationFor:lastObjectClassName isLast:YES completion:^(bool success) {
                    _pullInProgress = NO;
                    _currentOperation = nil;
                }];
            }
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"Faill to connect with  server");
    }
    
    @catch (NSException *exception) {
        //NSLog(@"Error from server");
    }
}

- (void)processData:(BOOL)isLastOperation className:(NSString *)className responseObject:(id)responseObject {
    
    NSLog(@"processData: %@", className);
    
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSString *resultsKey = [NSString stringWithFormat:@"Get%@ListResult",className];
        NSDictionary *resultsDict = [responseObject objectForKey:resultsKey];
        NSInteger responseCode = [[resultsDict valueForKey:@"ResponseStatus"] integerValue];
        if (responseCode==kTM_Response_Success) {
            resultsKey = [NSString stringWithFormat:@"%@List",className];
            NSArray *resultsArray = [resultsDict objectForKey:resultsKey];
            if ((id)resultsArray != [NSNull null]) {
                NSLog(@"%lu static updates downloaded for class %@.", (unsigned long)[resultsArray count], className);
                [self writeJSONResponse:resultsArray toDiskForClassWithName:className];
            }
            else {
                //NSLog(@"No static updates to download for class %@.", className);
            }
            if (isLastOperation == YES) {
                [self executePullCompletedOperations];
            }
        }
        else {
            //NSLog(@"Failed creating operation for get static updates for class %@.", className);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
        }
    }
}

-(AFHTTPRequestOperation *)downloadOperationFor:(NSString *)className isLast:(bool)last completion:(void (^)(bool success))completion
{
    NSDate *mostRecentUpdatedDate = [self getLastUpdatedDate:className];
    NSMutableURLRequest *request = [[TMAFCLient sharedClient] POSTRequestForChangedRecordsOfClass:className updatedAfterDate:mostRecentUpdatedDate];
    
    AFHTTPRequestOperation *operation = [[TMAFCLient sharedClient] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success: %@", className);
        
//        dispatch_async(_saveOperationQueue, ^{
            [self processData:last className:className responseObject:responseObject];
            if (last)
            {
                _syncInProgress = NO;
            }
            completion(!_cancelRequested);
        //        });
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == kTM_OPERATION_CANCELLED) {
            NSLog(@"Cancelled operation to get static updates for class %@.", className);
        }
        else {
            //NSLog(@"Failed creating operation for get static updates for class %@.", className);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Error" object:nil];
        }
        _syncInProgress = NO;
        completion(NO);
    }];
    [[TMAFCLient sharedClient] enqueueBatchOfHTTPRequestOperations:@[operation] progressBlock:^(NSUInteger numberOfCompletedOperations, NSUInteger totalNumberOfOperations) {
    } completionBlock:^(NSArray *operations) {
    }];
    
    return operation;
}


-(void) executePullCompletedOperations {
    [self processJSONDataRecordsIntoCoreData];
}

-(void) registerNSManagedObjectClassesToSync:(NSArray *)classes {
    self.registeredClassesToSync = [NSMutableArray array];
    for (Class aClass in classes) {
        [self.registeredClassesToSync addObject:NSStringFromClass(aClass)];
    }
    [self sortRegisteredClassesArray];
}

// these two classes must be processed first
-(void) sortRegisteredClassesArray {
    NSMutableArray *result = [NSMutableArray array];
    
    if ([self.registeredClassesToSync containsObject:LOCATION_ENTITY_NAME]) {
        [result addObject:LOCATION_ENTITY_NAME];
    }
    if ([self.registeredClassesToSync containsObject:REGISTRANT_ENTITY_NAME]) {
        [result addObject:REGISTRANT_ENTITY_NAME];
    }
    for (NSString *theClass in self.registeredClassesToSync) {
        if ([theClass isEqualToString:LOCATION_ENTITY_NAME]==NO && [theClass isEqualToString:REGISTRANT_ENTITY_NAME]==NO)
            [result addObject:theClass];
    }
    self.registeredClassesToSync = result;
}

- (BOOL)initialSyncComplete {
    return [[[NSUserDefaults standardUserDefaults] valueForKey:kTMSyncEngineInitialCompleteKey] boolValue];
}

- (void)setInitialSyncCompleted {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kTMSyncEngineInitialCompleteKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Helpers

-(NSDate *)extractDate:(NSDictionary *)responseDictionary {
    NSString *date = [responseDictionary valueForKey:@"SyncDate"];
    return [self unixDateToNSDate:date];
    
}
// the date passed back is microseconds, so we divide by 1000 to figure the interval
-(NSDate *)unixDateToNSDate:(NSString *)unixDateString {
    NSString *temp = [unixDateString stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
    NSMutableString *result1 = [[temp stringByReplacingOccurrencesOfString:@")/"withString:@""]
                                mutableCopy];
    double dubResult =[result1 doubleValue];
    dubResult = dubResult / 1000;
    NSDate *result =  [NSDate dateWithTimeIntervalSince1970:dubResult];
    ////NSLog(@"result date:%@",result);
    return result;
}

-(NSDate *) getLastUpdatedDate:(NSString *)className {
    NSString *staticClassKey = [NSString stringWithFormat:@"%@lastUpdated",className];
    return [[NSUserDefaults standardUserDefaults] objectForKey:staticClassKey];
}

- (void)setLastUpdatedDate:(NSDate *)updatedDate forClass:(NSString *)className {
    NSString *staticClassKey = [NSString stringWithFormat:@"%@lastUpdated",className];
    [[NSUserDefaults standardUserDefaults] setObject:updatedDate forKey:staticClassKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSMutableArray *) getSyncDatesFromArray:(NSArray *)syncedDatesArray {
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSDictionary *dict in syncedDatesArray) {
        NSMutableDictionary  *resultDict = [NSMutableDictionary dictionary];
        
        NSString *theDate = [dict valueForKey:@"lastUpdated"];
        if ((id)theDate==[NSNull null]) {
            continue;
        }
        NSString *className = [dict valueForKey:@"entityName"];
        [resultDict setObject:[self unixDateToNSDate:theDate] forKey:@"lastUpdated"];
        [resultDict setObject:className forKey:@"entityName"];
        [resultArray addObject:resultDict];
    }
    return resultArray;
}

#pragma mark - File and Core Data management

// pull out the nulls and save the record array
-(void)writeJSONResponse:(NSArray *)responseArray toDiskForClassWithName:(NSString *)className {
    
    NSMutableArray *nullFreeRecords = [NSMutableArray array];
    for (NSDictionary *record in responseArray) {
        NSMutableDictionary *nullFreeRecord = [NSMutableDictionary dictionaryWithDictionary:record];
        [record enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([obj isKindOfClass:[NSNull class]]) {
                [nullFreeRecord setValue:nil forKey:key];
            }
        }];
        [nullFreeRecords addObject:nullFreeRecord];
    }
    
    __block NSUInteger syncedObjectCount = 0;
    __block NSError *error = nil;
    
    NSUInteger newObjectsCreated = 0, objectsUpdated = 0;
    NSArray *downloadedRecords = nullFreeRecords;
    
    if ([downloadedRecords lastObject]) {
        if ([className isEqualToString:USER_ENTITY_NAME]==YES) {
            [self processUserRecords:downloadedRecords new:&newObjectsCreated updated:&objectsUpdated];
        }
        else {
            [self processDownloadedRecords:downloadedRecords forClass:className new:&newObjectsCreated updated:&objectsUpdated];
        }
        syncedObjectCount+=objectsUpdated+newObjectsCreated;
        
        if ([[[DataEngine sharedInstance] managedObjectContext] save:&error] == NO)
        {
            @throw [NSException exceptionWithName:[error localizedDescription]
                                           reason:[error localizedFailureReason]
                                         userInfo:[error userInfo]];
        }
        
        NSLog(@"%lu new and %lu objects updated for %@",(unsigned long)newObjectsCreated,(unsigned long)objectsUpdated,className);
    }
}
- (NSURL *)JSONDataRecordsDirectory{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL URLWithString:@"JSONRecords/" relativeToURL:[self applicationCacheDirectory]];
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:[url path]]) {
        [fileManager createDirectoryAtPath:[url path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return url;
}

- (NSURL *)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void) processJSONDataRecordsIntoCoreData {
    
    // sync (pull) complete:
    [self willChangeValueForKey:@"syncInProgress"];
    _syncInProgress = NO;
    [self didChangeValueForKey:@"syncInProgress"];
    
    if (self.delegate != nil) {
        [self.delegate syncDidComplete:0 withError:nil];
    }
}

// TBD: return the error?
-(void) processDownloadedRecords:(NSArray *)downloadedRecords forClass:(NSString *)className new:(NSUInteger*)newObjectsCreated updated:(NSUInteger*) objectsUpdated {
    NSError *error = nil;
    // Determine if the record is new or has been updated.
    NSString *idNameForClass = [self idNameForClass:className];
    
    // ..fetch all records stored in Core Data whose objectId matches those from the JSON response.
    NSArray *idArray = [self idArrayFromRecordArray:downloadedRecords usingField:idNameForClass];
    NSArray *storedRecords = [[DataEngine sharedInstance] managedObjectsForClass:className sortedByKey:idNameForClass usingArrayOfIds:idArray inArrayOfIds:YES];
    
    int currentIndex = 0;
    NSArray *sortedDownloadedRecords = [downloadedRecords sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:idNameForClass ascending:YES]]];
    for (NSDictionary *record in sortedDownloadedRecords) {
        if (_cancelRequested)
        {
            break;
        }
        NSManagedObject *storedManagedObject = nil;
        NSNumber *currentId = [idArray objectAtIndex:currentIndex];
        storedManagedObject = [self matchingObj:storedRecords withID:currentId forName:idNameForClass];
        if (storedManagedObject != nil) {
            if ([self updateManagedObject:storedManagedObject withRecord:record]) {
                (*objectsUpdated)++;
            }
            else {
                error = [Utils errorForType:@"CoreData new object" errorCode:CORE_DATA_SAVE_ERROR];
            }
        }
        else {
            if ([self newManagedObjectWithClassName:className forRecord:record]) {
                (*newObjectsCreated)++;
            }
            else {
                error = [Utils errorForType:@"CoreData update object" errorCode:CORE_DATA_SAVE_ERROR];
            }
        }
        currentIndex++;
    }
}

// TBD: return the error?
-(void) processUserRecords:(NSArray *)downloadedRecords new:(NSUInteger *)newObjectsCreated updated:(NSUInteger *)objectsUpdated {
    
    NSError *error = nil;
    for (NSDictionary *record in downloadedRecords) {
        if (_cancelRequested)
        {
            break;
        }
        NSNumber *registrationNumber = [record valueForKey:@"registrationNumber"];
        NSNumber *userId = [record valueForKey:@"userId"];
        
        Registrant *registrant = [[DataEngine sharedInstance] registrantForId:registrationNumber];
        // handle registrant is nil
        if (registrant==nil)    { // ref integrity error
            NSLog(@"registrationNumber  %@",registrationNumber);
            error = [Utils errorForType:@"CoreData update/new object" errorCode:CORE_DATA_SAVE_ERROR];
            NSLog(@"Referential integrity (no registrant) failed for Core Data new User object.");
            continue;
        }
        NSManagedObject *storedManagedObject = nil;
        storedManagedObject = [[DataEngine sharedInstance] userManagedObject:userId WithRegistrant:registrant];
        if (storedManagedObject != nil) {
            if ([self updateManagedObject:storedManagedObject withRecord:record]) {
                (*objectsUpdated)++;
            }
            else {
                error = [Utils errorForType:@"CoreData new object" errorCode:CORE_DATA_SAVE_ERROR];
            }
        }
        else {
            if ([self newManagedObjectWithClassName:USER_ENTITY_NAME forRecord:record]) {
                (*newObjectsCreated)++;
            }
            else {
                error = [Utils errorForType:@"CoreData update object" errorCode:CORE_DATA_SAVE_ERROR];
            }
        }
    }
}

-(NSManagedObject *) matchingObj:(NSArray *)storedRecords withID:(NSNumber *)currentId forName:(NSString *)idName{
    @autoreleasepool {
        id matchingObj = nil;
        for (id obj in storedRecords) {
            id value = [obj valueForKey:idName];
            if ([value isKindOfClass:[NSString class]]) {
                if ([value isEqualToString:(NSString*)currentId]) {
                    matchingObj = obj;
                    break;
                }
            }
            else {
                NSNumber *num = [obj valueForKey:idName];
                if ([num isEqualToNumber:currentId]) {
                    matchingObj = obj;
                    break;
                }
            }
        }
        return matchingObj;
    }
}


-(NSArray *) idArrayFromRecordArray:(NSArray *)inputRecords usingField:(NSString *)fieldName {
    NSMutableArray *resultArray = [NSMutableArray array];
    for (id record in inputRecords) {
        [resultArray addObject:[record valueForKey:fieldName]];
    }
    return resultArray;
}

// removing all records on disc for one class
- (void)deleteJSONDataRecordsForClassWithName:(NSString *)className {
    NSURL *url = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    NSError *error = nil;
    BOOL deleted = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
    if (!deleted) {
        //NSLog(@"Unable to delete JSON Records at %@, reason: %@", url, error);
    }
}

- (NSDictionary *)JSONDictionaryForClassWithName:(NSString *)className {
    NSURL *fileURL = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    return [NSDictionary dictionaryWithContentsOfURL:fileURL];
}

- (NSArray *)JSONDataRecordsForClass:(NSString *)className sortedByKey:(NSString *)key {
    NSDictionary *JSONDictionary = [self JSONDictionaryForClassWithName:className];
    NSArray *records = [JSONDictionary objectForKey:@"results"];
    return [records sortedArrayUsingDescriptors:[NSArray arrayWithObject:
                                                 [NSSortDescriptor sortDescriptorWithKey:key ascending:YES]]];
}

- (BOOL)newManagedObjectWithClassName:(NSString *)className forRecord:(NSDictionary *)record {
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:className
                                                                      inManagedObjectContext:[[DataEngine sharedInstance] managedObjectContext]];
    BOOL ok = [newManagedObject setValuesWithDictionary:record];
    if (ok==NO) {
        [self deleteManagedObject:newManagedObject];
        NSLog(@"Referential integrity failed for Core Data new object of type %@.",className);
        return NO;
    }
    return YES;
}

- (BOOL)updateManagedObject:(NSManagedObject *)managedObject withRecord:(NSDictionary *)record {
    // need to hack this to get around swift method rules:
    BOOL ok = NO;
    if ([managedObject isKindOfClass:[MaterialType class]]) {
        MaterialType *matType = (MaterialType*) managedObject;
        ok = [matType setValuesWithDictionarySwift:record];
    }
    else  if ([managedObject isKindOfClass:[TransactionType_MaterialType class]]) {
        TransactionType_MaterialType *transTypeMatType = (TransactionType_MaterialType*) managedObject;
        ok = [transTypeMatType setValuesWithDictionarySwift:record];
    }
    else {
        ok = [managedObject setValuesWithDictionary:record];
        if (ok==NO) {
            NSLog(@"Referential integrity failed for Core Data update.");
            return NO;
        }
    }
    return ok;
}

-(void) deleteManagedObject:(NSManagedObject *)managedObject  {
    NSManagedObjectContext *backgroundContext = [[DataEngine sharedInstance] managedObjectContext];
    [backgroundContext deleteObject:managedObject];
}

-(NSString *)idNameForClass:(NSString *)className {
    NSString *fieldName = nil;
    if ([className isEqualToString:NSStringFromClass([Registrant class])]==YES) {
        fieldName = @"registrationNumber";
    }
    else if ([className isEqualToString:NSStringFromClass([User class])]==YES) {
        fieldName = @"userId";
    }
    else if ([className isEqualToString:NSStringFromClass([Location class])]==YES) {
        fieldName = @"locationId";
    }
    else if ([className isEqualToString:NSStringFromClass([DocumentType class])]==YES) {
        fieldName = @"documentTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([RegistrantType class])]==YES) {
        fieldName = @"registrantTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([Eligibility class])]==YES) {
        fieldName = @"eligibilityId";
    }
    else if ([className isEqualToString:NSStringFromClass([TireType class])]==YES) {
        fieldName = @"tireTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionType class])]==YES) {
        fieldName = @"transactionTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionStatusType class])]==YES) {
        fieldName = @"transactionStatusTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionSyncStatusType class])]==YES) {
        fieldName = @"transactionSyncStatusTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([ScaleTicketType class])]==YES) {
        fieldName = @"scaleTicketTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([PhotoPreselectComment class])]==YES) {
        fieldName = @"photoPreselectCommentId";
    }
    else if ([className isEqualToString:NSStringFromClass([PhotoType class])]==YES) {
        fieldName = @"photoTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([UnitType class])]==YES) {
        fieldName = @"unitTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionType_TireType class])]==YES) {
        fieldName = @"transactionTypeTireTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionType_RegistrantType class])]==YES) {
        fieldName = @"transactionTypeRegistrantTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([MaterialType class])]==YES) {
        fieldName = @"materialTypeId";
    }
    else if ([className isEqualToString:NSStringFromClass([TransactionType_MaterialType class])]==YES) {
        fieldName = @"transactionTypeMaterialTypeId";
    }
    
    
    
    return fieldName;
}


@end
