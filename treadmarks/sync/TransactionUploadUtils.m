//
//  TransactionUploadUtils.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionUploadUtils.h"
#import "DataEngine.h"

@implementation TransactionUploadUtils

+(NSArray *)scaleTicketsForTransaction:(Transaction *)transaction {
    __block NSArray *tickets = nil;
    
    //ACTIVE INACTIVE  status to be handled
    if ([NSThread isMainThread]==YES) {
        tickets = [[DataEngine sharedInstance] scaleTicketsForTransaction:transaction];
        
    }
    else {
        //dispatch_sync(dispatch_get_main_queue(), ^{
            tickets = [[DataEngine sharedInstance] scaleTicketsForTransaction:transaction];
       // });
    }
    return tickets;
}

// TBD: get trans_MatTypes not synced?
+(Transaction_MaterialType *)transactionMaterialTypeForTransaction:(Transaction *)transaction {
    __block Transaction_MaterialType *transMatType = nil;
    
    if ([NSThread isMainThread]==YES) {
        transMatType = [[DataEngine sharedInstance] transactionMaterialTypeForTransactionId:transaction.transactionId];
    }
    else {
        transMatType = [[DataEngine sharedInstance] transactionMaterialTypeForTransactionId:transaction.transactionId];
    }
    return transMatType;
}

+(NSArray *)tireCountsForTransaction:(Transaction *)transaction {
    __block NSArray *tireCounts = nil;
    
    if ([NSThread isMainThread]==YES) {
        tireCounts = [[DataEngine sharedInstance] transactionTireTypeNotSyncedForTransactionId:transaction.transactionId];

    }
    else {
        //dispatch_sync(dispatch_get_main_queue(), ^{
            tireCounts = [[DataEngine sharedInstance] transactionTireTypeNotSyncedForTransactionId:transaction.transactionId];
         //});
    }
    return tireCounts;
}

+(STCAuthorization *)stcAuthorizationForTransaction:(Transaction *)transaction {
    __block STCAuthorization *stcAuth;
    __block Authorization *auth = nil;
    
    if ([NSThread isMainThread]==YES) {
        auth = [[DataEngine sharedInstance] authorizationForTransaction:transaction];
        stcAuth = [[DataEngine sharedInstance] stcAuthorizationForAuthorization:auth];
    }
    else {
        //dispatch_sync(dispatch_get_main_queue(), ^{
            auth = [[DataEngine sharedInstance] authorizationForTransaction:transaction];
            stcAuth = [[DataEngine sharedInstance] stcAuthorizationForAuthorization:auth];
            
        //});
    }
    return stcAuth;
}

+(NSArray *)eligibilitiesForTransaction:(Transaction *)transaction {
    __block NSArray *eligs = nil;
    
    if ([NSThread isMainThread]==YES) {
        eligs = [[DataEngine sharedInstance] transactionEligibilitiesNotSyncedForTransaction:transaction];
    }
    else {
        //dispatch_sync(dispatch_get_main_queue(), ^{
        eligs = [[DataEngine sharedInstance] transactionEligibilitiesNotSyncedForTransaction:transaction];
        //});
    }
    return eligs;
}

+(NSArray *)commentsForTransaction:(Transaction *)transaction {
    __block NSArray *commentsArray = nil;
    
    if ([NSThread isMainThread]==YES) {
        commentsArray = [[DataEngine sharedInstance] commentsNotSyncedForTransactionId:transaction];
    }
    else {
        commentsArray = [[DataEngine sharedInstance] commentsNotSyncedForTransactionId:transaction];
    }
    return commentsArray;
}

/*+(Comment *)commentForTransaction:(Transaction *)transaction {
    __block Comment *comment = nil;
    
    if ([NSThread isMainThread]==YES) {
        comment = [[DataEngine sharedInstance] commentNotSyncedForTransactionId:transaction];
    }
    else {
        //dispatch_sync(dispatch_get_main_queue(), ^{
            comment = [[DataEngine sharedInstance] commentNotSyncedForTransactionId:transaction];
        //});
    }
    return comment;
}*/


@end
