//
//  TMAFCLient.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-02.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TMAFCLient.h"
#import "AFJSONRequestOperation.h"
#import "NSDictionary+NSDictionary_keys.h"
#import "Transaction.h"
#import "Photo.h"
#import "PhotoType.h"
#import "Utils.h"
#import "GPSLog.h"

static NSString *TM_Username = @"ignat";
static NSString *TM_Username_Key = @"username";
static NSString *TM_Password = @"kurzalevski";
static NSString *TM_Password_Key = @"password";

@implementation TMAFCLient





+ (TMAFCLient *)sharedClient {
    static TMAFCLient *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[TMAFCLient alloc] initWithBaseURL:[NSURL URLWithString:[Utils getFlipServer]?TM_WS_URL:TM_WS_URL]];    });
    
    return sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        [self setDefaultHeader:@"Accept" value:@"application/json"];
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setParameterEncoding:AFJSONParameterEncoding];
    }
    
    return self;
}

- (NSMutableURLRequest *)POSTRequestForChangedRecordsOfClass:(NSString *)className updatedAfterDate:(NSDate *)updatedDate {
    NSMutableURLRequest *request = nil;
    NSString *dateString = [NSDictionary unixTimeStampFromDate:updatedDate];
    NSDictionary* dateDictionary = [NSDictionary dictionaryWithObject:dateString forKey:@"lastUpdated"];
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:dateDictionary,@"dateInfo",credentials, @"credentials",nil];
    
    ////NSLog(@"Pull dictionary:%@", finalDict);
    
    request = [self requestWithMethod:@"POST" path:[NSString stringWithFormat:@"Get%@List", className] parameters:finalDict];
    return request;
}

/*- (NSMutableURLRequest *)GETRequestForChangedRecordsOfClass:(NSString *)className updatedAfterDate:(NSDate *)updatedDate {
    NSMutableURLRequest *request = nil;
    NSDictionary *parameters = nil;
    
    NSString *dateString = [NSDictionary unformattedUnixTimeStampFromDate:updatedDate];
    parameters = [NSDictionary dictionaryWithObject:dateString forKey:@"lastUpdated"];
    
    request = [self requestWithMethod:@"GET" path:[NSString stringWithFormat:@"Get%@List", className] parameters:parameters];
    return request;
}*/


- (NSMutableURLRequest *)POSTRequestForSyncDatesList {
    NSMutableURLRequest *request = nil;
    
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObject:credentials forKey: @"credentials"];
    ////NSLog(@"SycDateList dictionary:%@", finalDict);
    
    request = [self requestWithMethod:@"POST" path:[NSString stringWithFormat:@"GetSyncDatesList"] parameters:finalDict];
    return request;
}


- (NSMutableURLRequest *)POSTRequestForTransaction:(NSString *)directoryName transaction:(Transaction *)theTransaction {
    NSMutableURLRequest *request = nil;
    NSDictionary* transDict = [NSDictionary dictionaryForTransaction:theTransaction];
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:transDict,@"transactionInfo",credentials, @"credentials",nil];
	
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}


- (NSMutableURLRequest *)POSTRequestForGPSlog:(NSString *)directoryName transaction:(Transaction *)theTransaction {
    NSMutableURLRequest *request = nil;
    
    NSMutableArray *gpsLogsArray = [NSMutableArray array];
	
	if (theTransaction.incomingGpsLog.syncDate == nil)
	{
		NSDictionary *incomingDict = [NSDictionary dictionaryForTransactionLocation:theTransaction.incomingGpsLog];
		// this should never happen:
		if (incomingDict != nil && [incomingDict count]>0) {
			[gpsLogsArray addObject: incomingDict];
		}
	}
	
	if (theTransaction.outgoingGPSLog.syncDate == nil)
	{
		NSDictionary *outgoingDict = [NSDictionary dictionaryForTransactionLocation:theTransaction.outgoingGPSLog];
		if (outgoingDict != nil && [outgoingDict count]>0) {
			[gpsLogsArray addObject: outgoingDict];
		}
	}
	
    if ([gpsLogsArray count]==0) {
        return nil;
    }
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:gpsLogsArray,@"gpsLogInfo",
                               credentials, @"credentials",nil];
	// //NSLog(@"transaction GPSLog record: %@", finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForAuthorization:(NSString *)directoryName authorization:(STCAuthorization *)auth {
    NSMutableURLRequest *request = nil;
    
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *stcAuthDictionary = [NSDictionary dictionaryForTransaction_STCAuthorization:auth];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:stcAuthDictionary,@"authorizationInfo",credentials, @"credentials",nil];
    
    ////NSLog(@"stc authorization json:%@",finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
	
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionTireCounts:(NSString *)directoryName tireCounts:(NSArray *)tireCounts {
    NSMutableURLRequest *request = nil;
    NSDictionary* credentials = [self credentialsDictionary];
    NSMutableArray *tireCountsDictionaryArray = [NSMutableArray array];
    
    for (Transaction_TireType *tireType in tireCounts) {
        NSDictionary *dict = [NSDictionary dictionaryForTransaction_TireType:tireType];
        [tireCountsDictionaryArray addObject:dict];
    }
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:tireCountsDictionaryArray,@"tiresInfo",credentials, @"credentials",nil];
    
    ////NSLog(@"tirecount json:%@",finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionMaterialType:(NSString *)directoryName transMatType:(Transaction_MaterialType *)transMatType {
    NSMutableURLRequest *request = nil;
    NSDictionary* credentials = [self credentialsDictionary];
    
    NSDictionary *dict = [NSDictionary dictionaryForTransaction_MaterialType:transMatType];
    // TEMP
    //NSArray *matTypesArray = [NSArray arrayWithObjects:dict, nil];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:dict,@"materialsInfo",credentials, @"credentials",nil];
   // NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:matTypesArray,@"materialsInfo",credentials, @"credentials",nil];
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionEligibilities:(NSString *)directoryName eligibilities:(NSArray *)eligibilities {
    NSMutableURLRequest *request = nil;
    NSDictionary* credentials = [self credentialsDictionary];
    NSMutableArray *eligibilitiesDictionaryArray = [NSMutableArray array];
    
    for (Transaction_Eligibility *elig in eligibilities) {
        NSDictionary *dict = [NSDictionary dictionaryForTransaction_Eligibility:elig];
        [eligibilitiesDictionaryArray addObject:dict];
    }
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:eligibilitiesDictionaryArray,@"transaction_EligibilityInfo",credentials, @"credentials",nil];
    
    // //NSLog(@"eligibilities json:%@",finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionImage:(NSString *)directoryName photo:(Photo *)thePhoto {
    
    NSDictionary* photoDict = [NSDictionary dictionaryForPhotoUpload:thePhoto useName:NO];
    NSDictionary* credentials = [self credentialsDictionary];
    
    NSMutableDictionary *finalDict = [NSMutableDictionary dictionaryWithDictionary:photoDict];
    [finalDict setObject:[credentials objectForKey:TM_Username_Key] forKey:TM_Username_Key];
    [finalDict setObject:[credentials objectForKey:TM_Password_Key] forKey:TM_Password_Key];

    ////NSLog(@"%@", finalDict);
        
    UIImage *image = nil;
    // scale ticket stores the photo filename differently
    if (thePhoto.photoType.photoTypeId == PHOTOTYPE_ID_SCALE_TICKET) {
        NSString *path = [thePhoto.fileName stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
        image = [UIImage imageWithContentsOfFile:path];
		if (image == nil)
		{
			path = [Utils pathForPhotoWithPhotoName:path];
			image = [UIImage imageWithContentsOfFile:path];
		}
    }
    else {
        NSString *originalPath = [Utils pathForPhotoWithPhotoName:thePhoto.fileName];
        NSString *path = [originalPath stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
        image = [UIImage imageWithContentsOfFile:path];
        // hedge for signature file:
        if (image==nil) {
            image = [UIImage imageWithContentsOfFile:originalPath];
        }
    }
    NSData *imgData = UIImagePNGRepresentation(image);
    NSString *photoFileName = [self uploadNameForPhoto:thePhoto];   //[path lastPathComponent];
    
    return [self multipartFormRequestWithMethod:@"POST"
                                           path:directoryName
									 parameters:finalDict
                      constructingBodyWithBlock:^(id <AFMultipartFormData>formData)
            {
                [formData appendPartWithFileData:imgData name:@"image" fileName:photoFileName mimeType:@"image/png"];
            }
            ];
}

- (NSMutableURLRequest *)POSTRequestForTransactionPhoto:(NSString *)directoryName photo:(Photo *)thePhoto {
    NSMutableURLRequest *request = nil;
    NSDictionary* photoDict = [NSDictionary dictionaryForPhotoUpload:thePhoto useName:YES];
    NSDictionary* credentials = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:credentials, @"credentials", photoDict,@"photoInfo",nil];
    
    ////NSLog(@"%@", finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionComment:(NSString *)directoryName comment:(Comment *)theComment {
    NSMutableURLRequest *request = nil;
    NSDictionary* credentialsDict = [self credentialsDictionary];
    NSDictionary *commentDict = [NSDictionary dictionaryForCommentUpload:theComment];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:credentialsDict, @"credentials", commentDict,@"commentInfo",nil];
    
    ////NSLog(@"comment json:%@",finalDict);
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}

- (NSMutableURLRequest *)POSTRequestForTransactionComments:(NSString *)directoryName comment:(NSArray *)comments {
    NSMutableURLRequest *request = nil;
    NSDictionary* credentialsDict = [self credentialsDictionary];
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:credentialsDict, @"credentials", comments,@"commentInfo",nil];
    
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    return request;
}


- (NSMutableURLRequest *)POSTRequestForScaleTicket:(NSString *)directoryName scaleTickets:(NSArray *)tickets {
    
    NSMutableURLRequest *request;
    NSDictionary *credentials = [self credentialsDictionary];
    NSMutableArray *scaleTicketDictionaryArray = [NSMutableArray array];
    
    for (ScaleTicket *sTicket in tickets) {
        NSDictionary *scaleTicketDictionary = [NSDictionary dictionaryForScaleTicket:sTicket];
        [scaleTicketDictionaryArray addObject:scaleTicketDictionary];
    }
    
    NSDictionary *finalDict = [NSDictionary dictionaryWithObjectsAndKeys:scaleTicketDictionaryArray, @"scaleTicketInfo",credentials, @"credentials", nil];
    request = [self requestWithMethod:@"POST" path:directoryName parameters:finalDict];
    
    return request;
}

-(NSDictionary *)credentialsDictionary {
    return [NSDictionary dictionaryWithObjectsAndKeys:TM_Username,TM_Username_Key,TM_Password,TM_Password_Key ,nil];
}

-(NSString *)uploadNameForPhoto:(Photo *)photo {
    if (photo.photoType.photoTypeId == PHOTOTYPE_ID_SCALE_TICKET) {
        return [photo.fileName lastPathComponent];
    }
    //return photo.fileName;
    return [photo.fileName stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
}

@end
