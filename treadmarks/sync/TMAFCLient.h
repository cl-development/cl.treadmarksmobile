//
//  TMAFCLient.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-04-02.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "AFHTTPClient.h"
#import "Transaction.h"
@class Comment, STCAuthorization, ScaleTicket, Transaction_MaterialType;

//extern NSString * const NetworkingReachabilityDidChangeNotification;

@interface TMAFCLient : AFHTTPClient
+ (TMAFCLient *)sharedClient;

- (NSMutableURLRequest *)POSTRequestForSyncDatesList;
//- (NSMutableURLRequest *)GETRequestForChangedRecordsOfClass:(NSString *)className updatedAfterDate:(NSDate *)updatedDate;
- (NSMutableURLRequest *)POSTRequestForChangedRecordsOfClass:(NSString *)className updatedAfterDate:(NSDate *)updatedDate;

- (NSMutableURLRequest *)POSTRequestForTransaction:(NSString *)directoryName transaction:(Transaction *)theTransaction;
- (NSMutableURLRequest *)POSTRequestForTransactionImage:(NSString *)directoryName photo:(Photo *)thePhoto;
- (NSMutableURLRequest *)POSTRequestForTransactionPhoto:(NSString *)directoryName photo:(Photo *)thePhoto;
- (NSMutableURLRequest *)POSTRequestForTransactionTireCounts:(NSString *)directoryName tireCounts:(NSArray *)tireCounts;
- (NSMutableURLRequest *)POSTRequestForTransactionMaterialType:(NSString *)directoryName transMatType:(Transaction_MaterialType *)transMatType;
- (NSMutableURLRequest *)POSTRequestForTransactionComments:(NSString *)directoryName comment:(NSArray *)comments;
- (NSMutableURLRequest *)POSTRequestForTransactionEligibilities:(NSString *)directoryName eligibilities:(NSArray *)eligibilities;
- (NSMutableURLRequest *)POSTRequestForGPSlog:(NSString *)directoryName transaction:(Transaction *)theTransaction;
- (NSMutableURLRequest *)POSTRequestForAuthorization:(NSString *)directoryName authorization:(STCAuthorization *)auth;
- (NSMutableURLRequest *)POSTRequestForScaleTicket:(NSString *)directoryName scaleTickets:(NSArray *)tickets;
@end
