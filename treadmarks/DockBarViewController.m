//
//  DockBarViewController.m
//  TreadMarks
//
//  Created by Adi on 2015-07-28.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "DockBarViewController.h"
#import "Utils.h"
#import "Transaction+Transaction.h"
#import "DataEngine.h"
#import "TOCPageViewController.h"
#import "AppDelegate.h"
#import "ContactView.h"
#import "TreadMarks-Swift.h"
#import "ListViewController.h"
#import "NewWarningView.h"

@interface DockBarViewController ()<InfoViewControllerDelegate,LogoutViewControllerDelegate,ContactViewControllerDelegate,AboutViewControllerDelegate,PopUpViewControllerDelegate, NewWarningViewDelegate>
{
    IBOutlet UIButton *buttonMyInfo;
    IBOutlet UIButton *buttonContact;
    IBOutlet UIButton *buttonLogout;
    IBOutlet UIButton *buttonHelp;
    IBOutlet UIButton *buttonSynch;
    IBOutlet UIButton *buttonNew;
    
}
@property (weak, nonatomic) TOCPageViewController *TOCView;
@property(nonatomic, retain) btSimplePopUP *popUp;
@property (nonatomic,strong) NSMutableAttributedString *alertmessage;
- (IBAction)myInfo:(id)sender;
-(IBAction)buttonContact:(id)sender;
-(IBAction)buttonLogout:(id)sender;
-(IBAction)buttonHelp:(id)sender;
-(IBAction)buttonSynch:(id)sender;
-(IBAction)buttonNew:(id)sender;


@end

@implementation DockBarViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidComplete:) name:@"Sync Complete"  object:nil];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *dateString = [standardUserDefaults stringForKey:@"lastPushDate"];
    NSArray *components = [dateString componentsSeparatedByString:@"  "];
    NSString *date = components[0];
    NSString *time = components[1];
    self.lastSyncDay.text =[date uppercaseString];
    self.lastSyncTime.text=time;

   
}

- (void)syncDidComplete:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"Sync Complete"]) {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *dateString = [standardUserDefaults stringForKey:@"lastPushDate"];
        NSArray *components = [dateString componentsSeparatedByString:@"  "];
        NSString *date = components[0];
        NSString *time = components[1];
        self.lastSyncDay.text =[date uppercaseString];
        self.lastSyncTime.text=time;

    }
}

#pragma mark - Memory Management
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated
{
   }
#pragma mark - Button Actions
- (IBAction)myInfo:(id)sender {
    InfoView *infoView =[[InfoView alloc]init];
    infoView.delegate=self;
    [[ModalViewController sharedModalViewController]showView:infoView];
    
    
}
-(IBAction)buttonContact:(id)sender
{
    ContactView *contactView =[[ContactView alloc]init];
    contactView.delegate=self;
    [[ModalViewController sharedModalViewController]showView:contactView];
}
-(IBAction)buttonLogout:(id)sender
{
    LogoutView *logoutView =[[LogoutView alloc]init];
    logoutView.delegate=self;
    [[ModalViewController sharedModalViewController]showView:logoutView];
}
-(IBAction)buttonHelp:(id)sender
{
    AboutView *aboutView =[[AboutView alloc]init];
    aboutView.delegate=self;
    [[ModalViewController sharedModalViewController]showView:aboutView];
    
}
-(IBAction)buttonSynch:(id)sender
{
    SyncViewController *viewController = [[SyncViewController alloc] initWithNibName:@"SyncViewController" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:viewController animated:YES completion:nil];


}
-(IBAction)buttonNew:(id)sender
{
    //   // [[NSNotificationCenter defaultCenter]
    //     postNotificationName:@"showBtPopUp"
    //     object:self];
    ListViewController *listview = [[ListViewController alloc]init];
    NSInteger *UnsyncLastMonth = listview.countUnsyncedTransactions;
    NSString *LastMonth = [listview getClaimMonth:2];
    NSString *Month = [listview getClaimMonth:1];
    NSNumber *Year = [listview getClaimCurrentYear];
    NSNumber *LastYear;
    
    if ([LastMonth isEqualToString:@"Dec"]){
        LastYear = [NSNumber numberWithInt:([Year intValue]-1)];
    } else {
        LastYear = Year;
    }
    
//            UnsyncLastMonth = 2;
    
    if (UnsyncLastMonth > 0) {
        
        NSString *st1 = @"You must ";
        NSString *stbold1 = [NSString stringWithFormat:@"complete, void, or sync any incomplete or unsynced transactions in %@. %@ before", LastMonth, LastYear];
        NSString *st2 = @" you can create new transactions in";
        NSString *stbold2 = [NSString stringWithFormat:@" %@. %@", Month, Year];
        NSString *text = [NSString stringWithFormat:@"%@%@%@%@",st1, stbold1, st2, stbold2];
        
        NSDictionary *attribs = @{
                                  NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:26]
                                  };
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
        
        UIFont *boldFont = [UIFont fontWithName:@"Helvetica-Bold" size:26];
        NSRange range1 = [text rangeOfString:stbold1];
        NSRange range2 = [text rangeOfString:stbold2];
        //        NSRange range = NSUnionRange(range1, range2);
        [attributedText setAttributes:@{NSFontAttributeName:boldFont} range:range1];
        [attributedText setAttributes:@{NSFontAttributeName:boldFont} range:range2];
        
        self.alertmessage = attributedText;
        
        [self presentAlert:nil info:NULL];
        
    } else {
    
    if (self.createNewTransaction)
    {
        PopUpView *popupView =[[PopUpView alloc]init];
        popupView.delegate=self;
        [[ModalViewController sharedModalViewController]showView:popupView];
    }
    else
    {
        [self startShake:buttonNew];
        
        
    }
    }
}

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    NewWarningView *newWarningView = [[NewWarningView alloc] init];
    //    newAlertView.frame = CGRectMake(floor((self.view.frame.size.width - newAlertView.frame.size.width) / 2),
    //                                    floor((self.view.frame.size.height - newAlertView.frame.size.height) / 2),
    //                                    newAlertView.frame.size.width,
    //                                    newAlertView.frame.size.height);
    /*
     NSRange boldedRange = NSMakeRange(10, 24);
     
     NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self.alertmessage];
     
     UIFont *font_regular = [UIFont fontWithName:@"Helvetica" size:20.0f];
     UIFont *font_bold = [UIFont fontWithName:@"Helvetica-Bold" size:20.0f];
     
     [attrString addAttribute:NSFontAttributeName value:font_bold range:boldedRange];
     */
    //    [newAlertView setText: self.alertmessage];
    newWarningView.title = @"Warning";
    newWarningView.text = self.alertmessage;
    newWarningView.primaryButtonText = @"OK";
    newWarningView.delegate = self;
    newWarningView.singlelineforOkay.hidden=YES;
    
    newWarningView.setColour =YES;
    [newWarningView setUI];
//    [self addSubview:newAlertView];
        [[ModalViewController sharedModalViewController] showView:newWarningView];

}

- (void)primaryAction:(UIButton *)sender {
    
    [[ModalViewController sharedModalViewController] hideView];
}


- (void) startShake:(UIButton*)button
{
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(-5, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(5, 0);
    
    button.transform = leftShake;  // starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void * _Nullable)(button)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    
    button.transform = rightShake; // end here & auto-reverse
    
    [UIView commitAnimations];
}

- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([finished boolValue]) {
        UIView* item = (__bridge UIView *)context;
        item.transform = CGAffineTransformIdentity;
    }
}



#pragma mark - Delegates
-(void)cancelAction:(UIButton *)sender
{
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)logoutcancelAction:(UIButton *)sender
{
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)okAction:(UIButton *)sender
{
    [((AppDelegate *)[[UIApplication sharedApplication] delegate]) logout:YES];
    
}
-(void)contactCancelAction:(UIButton *)sender
{
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)aboutCancelAction:(UIButton *)sender
{
    [[ModalViewController sharedModalViewController]hideView];
}
-(void)ptrPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_PTR intValue]];
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)stcPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_STC intValue]];
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)tcrPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_TCR intValue]];
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)extraAlertAction
{
    
}
-(void)PitPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_PIT intValue]];
    [[ModalViewController sharedModalViewController]hideView];
    
    
}
-(void)hitPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_HIT intValue]];
    [[ModalViewController sharedModalViewController]hideView];
    
}
-(void)ucrPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_UCR intValue]];
    [[ModalViewController sharedModalViewController]hideView];
}
-(void)dotPerformAction
{
    [self newTransaction:[TRANSACTION_TYPE_ID_DOT intValue]];
    [[ModalViewController sharedModalViewController]hideView];
}
- (void)newTransaction:(NSInteger)transType {
    self.newTransactionCreated=YES;
    //create a new transaction based on transType, which is the tagValue of the button that was selected by the user.  The tag value corresponds to the TransactionTypeId, which allows for the creation of the correct TransactionType dynamically.
    [Utils log:@"New Transaction creation"];
    
    TransactionType *transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInteger:transType]];
    //create a new transaction
    Transaction *transaction     = (Transaction*)[[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
//    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *deviceName = [[UIDevice currentDevice] name];
    transaction.deviceName = deviceName;
//    transaction.deviceIDFV = deviceID;
    
//    transaction.deviceName = [NSString stringWithFormat:@"[%@][%@]",deviceID, deviceName];
    transaction.deviceName = deviceName;
//    NSLog([NSString stringWithFormat:@"DEVICENAMENAMENAME: %@",transaction.deviceName]);

    // if a processor logs in, the user values are reversed:
    User *loggedInUser = [Utils getIncomingUser];
    BOOL processorDidLogin = loggedInUser.registrant.registrantType.registrantTypeId==REGISTRANT_TYPE_ID_PROCESSOR;
    
    if (processorDidLogin==YES) {
        transaction.incomingUser     = nil;
        transaction.incomingRegistrant = nil;
        transaction.outgoingUser     = [Utils getIncomingUser];
        // transaction.createdUser      = [Utils  getIncomingRegistrant];
        transaction.outgoingRegistrant = [Utils getIncomingRegistrant];
    }
    else {
        transaction.incomingUser     = [Utils getIncomingUser];
        transaction.incomingRegistrant = [Utils getIncomingRegistrant];
        
        transaction.outgoingUser     = nil;
        transaction.outgoingRegistrant = nil;
    }
    transaction.transactionType  = transactionType;
    transaction.transactionId    = [Utils createTransactionId];
    transaction.friendlyId       = [Utils createFriendlyTransactionId:transactionType];
    //save this in the case of UCR along with created date
    if(transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR)
    { NSString *ucrTrans=[transaction.friendlyId stringValue] ;
        NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setObject:ucrTrans forKey:@"UCRTransaction"];
        [standardUserDefaults synchronize];
}
    
    // chage to Nill if OTS need it Back!
    NSDate *startDate =[NSDate date];
    transaction.createdDate      = [NSDate date];
    transaction.transactionDate  = startDate;//nil;//[NSDate date];
    transaction.createdUser = [Utils getIncomingUser];
    NSString *appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *versionBuildString = [NSString stringWithFormat:@"Version %@(%@)", appVersionString, appBuildString];
    transaction.versionBuild = versionBuildString;
    transaction.transactionStatusType       = [[DataEngine sharedInstance] transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_INCOMPLETE];
    
    transaction.transactionSyncStatusType   = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    
    //find the gps location of the login
    NSNumber *gpsLogId = [Utils getGPSLogId];
    GPSLog *incomingGPSLog = [[DataEngine sharedInstance] gpsLogForId:gpsLogId];
    transaction.incomingGpsLog = incomingGPSLog;
        
    if (transactionType.transactionTypeId!=TRANSACTION_TYPE_ID_PIT)  {
        // Note: the data engine returns the transactionType_tireType bridge type here, not the type in the name of the call
        NSArray *tireTypeArray = [[DataEngine sharedInstance] tireTypesForTransactionTypeId:transactionType.transactionTypeId];
        for (TireType *tireType in tireTypeArray) {
            Transaction_TireType *transactionTireType = (Transaction_TireType*)[[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
            transactionTireType.transaction = transaction;
            transactionTireType.tireType = tireType;
            transactionTireType.transactionTireTypeId = [[NSUUID UUID] UUIDString];
        }
    }

    [Utils setTransactionId:transaction.transactionId];
    
    [[DataEngine sharedInstance] saveContext];
    
    TOCPageViewController *viewController = [[TOCPageViewController alloc] init];
    
    [self.navigationController pushViewController:viewController animated:YES];
}


@end

