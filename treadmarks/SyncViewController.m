//
//  SyncViewController.m
//  TreadMarks
//
//  Created by Adi on 2015-10-12.
//  Copyright © 2015 Capris. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>
#import "SyncViewController.h"
#import "TMSyncEngine.h"
#import "TMAFCLient.h"
#import "TMTransactionSyncStatus.h"
#import "Reachability.h"
#import "NSArray+NSArray_Slice.h"
#import "CDActivityIndicatorView.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "Location.h"
#import "DataEngine.h"
#import "Transaction.h"
#import "STKSpinnerView.h"
#import "UIView+CustomView.h"
#import "ASProgressPopUpView.h"
#import "DockBarViewController.h"


@interface SyncViewController ()<ASProgressPopUpViewDataSource>
{
    UITapGestureRecognizer *singleTapGestureRecognizer;
    NSUInteger completedTransaction;
    NSUInteger nonSyncTransaction;
    NSUInteger totalTransactiontobeSync;
    NSUInteger syncToServerTransactionCount;
    NSUInteger chunkCycleCount;
    NSUInteger chunkCycle;
    float progressBarCount;
    CDActivityIndicatorView * activityIndicatorView;
    Reachability *reachability;
    BOOL animating;
    BOOL pullIsOn;
    BOOL disableNotification;
    NSTimer *cancelSyncTimer;
    float time;
    NSTimer *timer;
}

#pragma mark - IBOutlets
@property (weak, nonatomic) IBOutlet KAProgressLabel *pLabel;
@property (weak, nonatomic) IBOutlet UIView *dockView;
@property (weak, nonatomic) IBOutlet UIButton *backgroundButton;
@property (weak, nonatomic) IBOutlet UIButton *syncButton;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastSyncLabel;
@property (weak, nonatomic) IBOutlet UIView *backGroundView;

//Cancel/Retry Sync
@property (strong, nonatomic) IBOutlet UIView *CancelSyncTimerView;
@property (weak, nonatomic) IBOutlet UILabel *cancellingSyncTimerLabel;
@property (weak, nonatomic) IBOutlet ASProgressPopUpView *cancellinProgressBar;

//ERROR ALERT
@property (weak, nonatomic) IBOutlet UIView *errorAlertView;
@property (weak, nonatomic) IBOutlet UILabel *transactionLeftLabel;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertHeader;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertViewLineOne;
- (IBAction)errorOkButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertVilewLineThird;
@property (strong, nonatomic) IBOutlet UILabel *ErrorBatteryLableOne;
@property (strong, nonatomic) IBOutlet UILabel *errorBatteryLabelTwo;

//SYNC IN PROGRESS VIEW
@property (weak, nonatomic) IBOutlet UIImageView *syncAnimImage;
@property (weak, nonatomic) IBOutlet UILabel *syncProgressCountLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *syncProgressBar;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncButton;
- (IBAction)syncCancelButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *syncViewHeaderLabel;
- (IBAction)cancelSyncYesButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *allTransactionSyncLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncYesButtonOutlet;
@property (weak, nonatomic) IBOutlet UILabel *recevingDataLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncNoButtonOutlet;
- (IBAction)cancelSyncNoButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *allSyncOkButton;
- (IBAction)allSyncOK:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *cancelSyncConfirmLabel;
//WIFI ALERT
@property bool syncInProgress;
- (IBAction)wifiAlertSettingButton:(id)sender;
- (IBAction)wifiAlertOkButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *wifiAlertView;
@property (nonatomic, strong) NSArray *allTransactionsArray;
@property (nonatomic,strong) NSArray *transactionToBeSyncedArray;
@property (nonatomic, assign) BOOL canSync;
@property (nonatomic, strong) NSArray *syncDatesArray;

#pragma mark - Properties

@property int state;
@property BOOL firstAppearance;
@property NSUInteger transactionsToBeSyncedCount;
- (IBAction)Synctest:(id)sender;

// performance testing:
@property (nonatomic, strong) NSDate *startTime;

// static sync
@property (nonatomic, strong) NSArray *classesToSync;
@property (nonatomic, assign) NSUInteger fetchTransactionsOffset;
@property (weak, nonatomic) IBOutlet STKSpinnerView *spinnerView;

@end

@implementation SyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.syncInProgressView createCornerRadius:self.syncInProgressView];
    [self.CancelSyncTimerView   createCornerRadius:self.CancelSyncTimerView];
    [self.wifiAlertView   createCornerRadius:self.wifiAlertView ];
    [self.errorAlertView createCornerRadius:self.errorAlertView ];
    self.cancellinProgressBar.popUpViewCornerRadius = 12.0;
    self.cancellinProgressBar.font  = [UIFont fontWithName:@"OpenSans" size:15];
    [self.cancellinProgressBar showPopUpViewAnimated:YES];
    self.cancellinProgressBar.popUpViewAnimatedColors = @[[UIColor redColor], [UIColor orangeColor], [UIColor greenColor]];
    self.cancellinProgressBar.dataSource =self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkStatusChanged:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    [reachability startNotifier];
}

- (void)networkStatusChanged:(NSNotification*)note
{
    Reachability* reach = [note object];
    NetworkStatus networkStatus = reach.currentReachabilityStatus;
    NSString* textToShow;
    if (networkStatus == NotReachable)
    {
        textToShow = @"Network problems.  No Syncing possible";
        self.canSync = NO;
        [self wifiError];
    }
    if (networkStatus==ReachableViaWiFi||networkStatus==ReachableViaWWAN)
    {
        textToShow= @"Network found.  Try sync";
        self.canSync = YES;
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [ self syncStart];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark New ProgressBar

- (NSString *)progressView:(ASProgressPopUpView *)progressView stringForProgress:(float)progress
{
    NSString *s;
    if (progress < 0.2) {
        s = @"Sending Request.";
    } else if (progress > 0.4 && progress < 0.6) {
        s = @"Processing.";
    } else if (progress > 0.75 && progress < 1.0) {
        s = @"Saving Data.";
    } else if (progress >= 1.0) {
        s = @"Disconnected ";
    }
    return s;
}

// by default ASProgressPopUpView precalculates the largest popUpView size needed
// it then uses this size for all values and maintains a consistent size
// if you want the popUpView size to adapt as values change then return 'NO'
- (BOOL)progressViewShouldPreCalculatePopUpViewSize:(ASProgressPopUpView *)progressView;
{
    return NO;
}

#pragma mark - Sync Process

-(void) syncStart
{
    [self hideAllSubview];
    disableNotification=NO;
    //disable the timeout
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];

    // catch any corrupt scale ticket records caused by lock screen
    [Utils validateScaleTickets];
    // uncheck
    if (self.canSync == NO)
    {
        [self wifiError];
        return;
    }
    chunkCycleCount=0;
    progressBarCount=0;
    self.syncProgressBar.progress=0;
    // update  UI .
    
    self.syncInProgress=YES;
    activityIndicatorView = [[CDActivityIndicatorView alloc] initWithImage:[UIImage imageNamed:@"slide-icon-tire.png"]];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    self.syncInProgressView.hidden=NO;
    syncToServerTransactionCount=0;
    self.syncProgressCountLabel.text=@"Preparing data for upload";
    self.syncProgressBar.alpha=0;
    self.fetchTransactionsOffset = 0;
    self.allTransactionsArray = nil;
    
    //Put in Array copy in seprate thread.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        self.allTransactionsArray = [[DataEngine sharedInstance] transactionsNotSynced];
        totalTransactiontobeSync  = [self.allTransactionsArray count];
        chunkCycle=totalTransactiontobeSync/TRANSACTION_FETCH_LIMIT_SIZE;
        [self syncTransactions];
    });
}

-(void)doPull
{
    NSLog(@"DO PULL ACTIVATED");
    dispatch_async(dispatch_get_main_queue(), ^{
        self.recevingDataLabel.hidden      = NO;
        self.recevingDataLabel.alpha        =1;
    });

    [self pullUI:NO];
    [TMSyncEngine sharedEngine].delegate = self;
    [[TMSyncEngine sharedEngine] pullData];
    return;
}

-(void)syncTransactions {
    //CHANGE COUPLE OF UI VALUE
    if (totalTransactiontobeSync!=0)
    {
        progressBarCount=(100/(float)totalTransactiontobeSync);
        progressBarCount=progressBarCount/100;
    }
    else
    {
        [self doPull];
    }
    
    self.transactionToBeSyncedArray = nil;
    self.transactionToBeSyncedArray = [self.allTransactionsArray arrayBySlicingFrom:self.fetchTransactionsOffset to:self.fetchTransactionsOffset+TRANSACTION_FETCH_LIMIT_SIZE];
    self.transactionsToBeSyncedCount = [self.transactionToBeSyncedArray count];
    self.fetchTransactionsOffset += TRANSACTION_FETCH_LIMIT_SIZE;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.syncInProgressView.hidden=NO;
    });
    
    
    self.startTime = [NSDate date];
    [TMSyncEngine sharedEngine].delegate = self;
    
    for (Transaction *transaction in self.transactionToBeSyncedArray)
    {
        //  [self startSpin];
        if (self.canSync==NO)
        {
            [self wifiError];
            break;
        }
        [[TMSyncEngine sharedEngine] uploadTransaction:transaction];
    }
}

#pragma mark  - TMSyncEngineDelegate

-(void) transactionDidUpload:(Transaction *)transaction withStatuses:(TMTransactionSyncStatus *)transactionStatuses {
    
    //Change UI
    self.syncProgressBar.alpha=1;

    NSDate*lastsyncDate = transaction.syncDate;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d MMM  hh:mm a"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:[dateFormat stringFromDate:lastsyncDate] forKey:@"lastPushDate"];
    [standardUserDefaults synchronize];
    
    self.transactionsToBeSyncedCount--;
    syncToServerTransactionCount++;
    
    //Animate the blue bar
    self.syncProgressBar.progress+=progressBarCount;
    
    self.syncProgressCountLabel.text=([NSString stringWithFormat:@"Transaction %lu of %lu ", (unsigned long)syncToServerTransactionCount,(unsigned long)totalTransactiontobeSync ]);
    
    //Once all the transaction  sync using the  chunking Do Pull!
    if(syncToServerTransactionCount==totalTransactiontobeSync)
    {
        // All Trnsaction synced. Notify and Do Pull
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
        self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
        
        //Show Pull message
        pullIsOn=YES;
        NSLog(@"Pull Has started");
        [self pullUI:NO];
        [[TMSyncEngine sharedEngine ]pullData];
        return;
    }
    // CHECK IF IT NEED THE NEXT CHUNK
    if (self.transactionsToBeSyncedCount==0)
    {
        [self syncTransactions];
    }
}

-(void) transactionUploadDidFail:(Transaction *)transaction withError:(NSError *)error {
    
    NSString *msg = [error localizedDescription];
    NSLog(@"transaction %@ upload did fail with error: %@",transaction.friendlyId, msg);
    [[TMSyncEngine sharedEngine]cancel];
    
    self.syncButton.enabled=YES;
}

-(void) syncDidComplete:(NSUInteger)syncedObjectCount withError:(NSError *)error {
    self.syncInProgress=NO;
    if (error != nil) {
        NSString *msg = [error localizedDescription];
        NSLog(@"Sync (pull) completed with error: %@",msg);
        //[self wifiError];
        [self syncErrorUI];
        self.transactionLeftLabel.text=@"";
        return;
    }
    else {
        // Pull  finished ! change UI
        // Add notification  for the Battery staus changed
        NSLog(@"Sync (pull) completed for %lu entities",(unsigned long)syncedObjectCount);
        
        // now store off the new dates
        for (NSDictionary *dict in self.syncDatesArray){
            NSDate *syncDate = [dict valueForKey:@"lastUpdated"];
            NSString *className = [dict valueForKey:@"entityName"];
            [[TMSyncEngine sharedEngine] setLastUpdatedDate:syncDate forClass:className];
        }
        self.syncDatesArray = nil;
        [self pullUI:YES];
    }
    NSDate *dateB = [NSDate date];
    NSDate *dateA = self.startTime;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitMinute|NSCalendarUnitSecond
                                               fromDate:dateA
                                                 toDate:dateB
                                                options:0];
    NSLog(@"Sync Succeeded. elapsed time for upload: %li minutes and %li seconds.", (long)components.minute, (long)components.second);
    // should change UI
    NSLog(@"FORCING THE NEW UI");
    
}

// Compare these dates with stored values, and initiate pull request if required
-(void) syncDates:(NSArray *)dateArray {
    // check at last transaction completed time
    if (self.transactionsToBeSyncedCount == 0) {
        NSMutableArray *classesToSync  = [NSMutableArray array];
        for (NSDictionary *dict in dateArray){
            NSDate *syncDate = [dict valueForKey:@"lastUpdated"];
            NSString *className = [dict valueForKey:@"entityName"];
            if ([className isEqualToString:@"PhotoPreSelectComment"]) {
                className = @"PhotoPreselectComment";
            }
            NSDate *currentDate = [[TMSyncEngine sharedEngine] getLastUpdatedDate:className];
            
            //  NSLog(@" LAST UPDATE DATE %@",currentDate);
            if ( [syncDate compare:currentDate]==NSOrderedDescending) {
                Class theClass = NSClassFromString(className);
                [classesToSync addObject:theClass];
            }
        }
        
        self.classesToSync = [self orderClassesToSync:classesToSync];
        
        // now store off the new dates in tempArray
        self.syncDatesArray = dateArray;
        
        // initiate sync if necessary
        // PUll if OFF for testing
        if ([self.classesToSync count] != 0) {
            [[TMSyncEngine sharedEngine] registerNSManagedObjectClassesToSync:self.classesToSync];
            [TMSyncEngine sharedEngine].delegate = self;
            [[TMSyncEngine sharedEngine] startSync];
        } else {
            [self pullUI:YES];
        }
    }
}

-(NSArray *) orderClassesToSync:(NSArray *)classesToSync {
    NSMutableArray *array = [NSMutableArray array];
    if ([classesToSync containsObject:[Location class]]==YES) {
        [array addObject:[Location class]];
    }
    if ([classesToSync containsObject:[Registrant class]]==YES) {
        [array addObject:[Registrant class]];
    }
    for (Class aClass in classesToSync) {
        if (aClass != [Location class] && aClass != [Registrant class]) {
            [array addObject:aClass];
        }
    }
    
    return [array copy];
}

-(void)pullUI :(BOOL)stop
{
    //stop the spin for pull
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [activityIndicatorView stopAnimating];
        
        if (!stop)
        {
            self.recevingDataLabel.hidden      = NO;
            self.recevingDataLabel.alpha       = 1;
            self.syncProgressBar.hidden        = YES;
            self.syncProgressCountLabel.hidden = YES;
            pullIsOn=YES;
        } else {
            [self.recevingDataLabel setAlpha:0.0];
            self.recevingDataLabel.hidden      = YES;
            pullIsOn=NO;
            //hide cancel
            [self cancelSyncUIChange:NO];
            [self allTransactionSyncUI];
        }
    });
}


-(void) cancelSyncUIChange :(bool)status
{
    self.syncProgressBar.hidden=status;
    self.syncProgressCountLabel.hidden=status;
    self.cancelSyncButton.hidden=status;
    self.cancelSyncConfirmLabel.hidden = !status;
    self.cancelSyncNoButtonOutlet.hidden = !status;
    self.cancelSyncYesButtonOutlet.hidden = !status;
}

-(void)wifiError
{
    [self hideAllSubview];
    self.wifiAlertView.hidden=NO;
}

-(void)hideAllSubview
{
    self.syncInProgressView.hidden=YES;
    self.wifiAlertView.hidden=YES;
    self.errorAlertView.hidden=YES;
    [activityIndicatorView stopAnimating];
    self.CancelSyncTimerView.hidden=YES;
}

-(void)syncServerError: (NSNotification *)notification
{
    NSLog(@" Sync Error");
    [[TMSyncEngine sharedEngine] cancel];
    
    if ([[notification name] isEqualToString:@"Sync Error"]) {
        if (disableNotification)
        {//Do not show Sync error if cancel by user
            return;
        }
        else
        {[self syncErrorUI];}
    }
}

-(void)syncErrorUI
{
    [self hideAllSubview];
    //if any error stop sync
    [[TMSyncEngine sharedEngine] cancel];
    
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
    self.errorAlertHeader.text=@"Sync Alert";
    self.backGroundView.hidden=NO;
    self.errorAlertViewLineOne.hidden=NO;
    self.errorAlertVilewLineThird.hidden=NO;
    self.transactionLeftLabel.hidden=NO;
    self.ErrorBatteryLableOne.hidden=YES;
    self.errorBatteryLabelTwo.hidden=YES;
    self.transactionLeftLabel.text=syncToServerTransactionCount==0?@"No transaction uploaded.":([NSString stringWithFormat:@"Transaction %lu of %lu uploaded", (unsigned long)syncToServerTransactionCount,(unsigned long)totalTransactiontobeSync ]);
    self.errorAlertView.hidden=NO;
}

-(void)hideCancelingView
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    self.CancelSyncTimerView.hidden=YES;
    [ self dismissViewControllerAnimated:YES completion:nil];
}

-(void)allTransactionSyncUI
{
    [activityIndicatorView stopAnimating];
    // FORCE THE CANCEL SYN LABEL AND BUTTONS
    self.syncInProgressView.hidden=NO;
    self.cancelSyncConfirmLabel.hidden=YES;
    self.cancelSyncYesButtonOutlet.hidden=YES;
    self.cancelSyncNoButtonOutlet.hidden=YES;
    self.recevingDataLabel.hidden=YES;
    [self cancelSyncUIChange:NO];
    
    self.syncProgressBar.hidden=YES;
    self.syncProgressCountLabel.hidden=YES;
    self.cancelSyncButton.hidden=YES;
    
    self.allTransactionSyncLabel.hidden=NO;
    self.allSyncOkButton.hidden=NO;
    self.syncViewHeaderLabel.text=@"Sync Complete";
    self.syncInProgressView.hidden=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    
    //Enable the timeout.
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (IBAction)allSyncOK:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    [self hideAllSubview];
    [ self dismissViewControllerAnimated:YES completion:nil];
}

// SYNC UI BUTTONS
- (IBAction)syncCancelButton:(id)sender {
    NSLog(@" Cancel Sync Manual");
    //Do we have to stop progressbar and sync?
    [self cancelSyncUIChange:YES];
    [activityIndicatorView stopAnimating];
    if(pullIsOn)
    {
        NSLog(@" Pull is on hide cancel");
        self.cancelSyncConfirmLabel.hidden=NO;
        self.recevingDataLabel.alpha=0;
    }
}
- (IBAction)errorOkButton:(id)sender {
    [self hideAllSubview];
    if(!self.syncInProgress)
    {
        //Hide the error coz  user has not done the sync
        // [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
        self.CancelSyncTimerView.hidden=YES;
        
        return;
    }
    [[TMSyncEngine sharedEngine] cancel];
    
    // singleTapGestureRecognizer.enabled=NO;
    self.CancelSyncTimerView.hidden=NO;
    //NSTimer *autoTimer;
    
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(hideCancelingView) userInfo:nil repeats:NO];
    self.cancellinProgressBar.progress = 0;
    // Put progress bar on the  cancelling
    time = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.1f
                                             target: self
                                           selector: @selector(updateTimer)
                                           userInfo: nil
                                            repeats: YES];
    
}
- (IBAction)cancelSyncYesButton:(id)sender {
    NSLog(@" Cancel Sync  YES BUUTON Manual");
    disableNotification=YES;
    [activityIndicatorView stopAnimating];
    [[TMSyncEngine sharedEngine] cancel];
    //UI CHANGE
    //Change the sync date
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
    
    [self cancelSyncUIChange:NO];
    self.recevingDataLabel.alpha=0;
    self.syncInProgressView.hidden=YES;
    [self hideAllSubview];
    
    self.CancelSyncTimerView.hidden=NO;
    
    self.cancellinProgressBar.progress  = 0;
    
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(hideCancelingView) userInfo:nil repeats:NO];
    //[self hideDock];
    self.cancellinProgressBar.progress = 0;
    time = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.1f
                                             target: self
                                           selector: @selector(updateTimer)
                                           userInfo: nil
                                            repeats: YES];
}



-(void) updateTimer
{
    if(time == 1)
    {
        //Invalidate timer when time reaches 0
        [timer invalidate];
    }
    else
    {
        time += 0.005;
        self.cancellinProgressBar.progress = time;
    }
}


- (IBAction)cancelSyncNoButton:(id)sender {
    [activityIndicatorView startAnimating];
    //[self stopSpin];
    [self cancelSyncUIChange:NO];
    if(pullIsOn)
    {
        self.syncProgressBar.hidden=YES;
        self.syncProgressCountLabel.hidden=YES;
        self.cancelSyncConfirmLabel.hidden=YES;
        self.recevingDataLabel.alpha=1;
    }
}

- (IBAction)wifiAlertSettingButton:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (IBAction)wifiAlertOkButton:(id)sender {
    [self hideAllSubview];
    [ self dismissViewControllerAnimated:YES completion:nil];
}

-(void)synInProgress
{
    [self hideAllSubview];
    self.syncInProgressView.hidden=NO;
    singleTapGestureRecognizer.enabled=NO;
}

- (IBAction)Synctest:(id)sender {
}
@end
