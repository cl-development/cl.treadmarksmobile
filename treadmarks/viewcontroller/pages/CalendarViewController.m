//
//  CalendarViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-20.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "CalendarViewController.h"
#import "ListViewController.h"
#import "DayView.h"
//#import "DockViewController.h"
#import "Utils.h"
#import "CalendarPopoverViewController.h"
#import "ParticipantsPageViewController.h"
#import "TransactionStatusType.h"
#import "Constants.h"
#import "ConfirmationPageViewController.h"
#import "TOCPageViewController.h"
#import "DockBarViewController.h"


#define X0  (0)
#define Y0 (147+10)
#define DAY_WIDTH 108
#define DAY_HEIGHT 128
#define SEPARATOR_SIZE 1

#define DAY_LABEL_WIDTH  40
#define DAY_LABEL_HEIGHT 40
#define DAYS_PER_WEEK 7
#define WEEKS_IN_CALENDAR 6

@interface CalendarViewController () <SlideViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong,nonatomic) NSMutableArray* dayLabelArray;
//@property (strong,nonatomic) DockViewController *dockViewController;
@property (nonatomic,strong) NSMutableArray *dayViewArray;
@property (nonatomic, strong) IBOutlet UILabel *transactionCountLabel;

@end

@implementation CalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //update the UI
    self.dayLabelArray = [NSMutableArray new];
    self.dayViewArray  = [NSMutableArray new];
    
    for (int i=0; i<WEEKS_IN_CALENDAR; i++) {
        for (int j=0; j<DAYS_PER_WEEK; j++) {
            CGRect rect = CGRectMake(X0+j*(DAY_WIDTH+SEPARATOR_SIZE), Y0+i*(DAY_HEIGHT+SEPARATOR_SIZE), DAY_WIDTH, DAY_HEIGHT);
            DayView *dayView = [[DayView alloc] initWithFrame:rect];
            [dayView.button addTarget:self action:@selector(showDayDetails:) forControlEvents:UIControlEventTouchDown];
            [self.view addSubview:dayView];
            [self.dayViewArray addObject:dayView];
        }
    }
    
    [self updateCalendar];
    
    DockBarViewController *dockBarView = [[DockBarViewController alloc] initWithNibName:@"DockBarViewController" bundle:nil];
    dockBarView.view.frame = CGRectMake(0,938,768,89);
    dockBarView.createNewTransaction=YES;
    [self.view addSubview:dockBarView.view];
    [self addChildViewController:dockBarView];
    [dockBarView didMoveToParentViewController:self];
}


-(void)viewWillAppear:(BOOL)animated {
	
    [super viewWillAppear:animated];
    
    [self updateCalendar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidComplete:) name:@"Sync Complete"  object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 Every month fills the 'grid' of 6 (rows) x 7 (columns) of the view: 42 day views.
 self.deltaMonths is the integer that changes as the user selects Next and Prev months
 
 */




- (void)syncDidComplete:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"Sync Complete"]) {
        [self updateUI];
    }
}

-(void)updateCalendar {
    //self.transactionArray = [[DataEngine sharedInstance] transactionsNotDeletedSort:@"transactionDate" ascending:NO];
    self.transactionArray = [[DataEngine sharedInstance] transactionsForRegistrant:[Utils getRegistrantId] AndUser:[Utils getUserId] NotDeletedSort:@"createdDate" ascending:NO filterSync:NO];
    NSDate *today = [NSDate date];
    
    
    //add the months delta
    NSDateComponents* dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    unsigned int components = NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth | NSCalendarUnitYear;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger day     = [dateComponents day];   // this is the ordinal day of the month, e.g. 1 or 30
    NSInteger weekday = [dateComponents weekday];   // day of the week by number, e.g. Thursday is 5
    NSInteger month   = [dateComponents month];
    NSInteger year    = [dateComponents year];
    
    //NSLog(@"day = %ld, weekday = %ld, month = %ld, year = %ld", (long)day, (long)weekday, (long)month, (long)year);
    
    //populate the month and year
    NSDateFormatter *df = [NSDateFormatter new];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    
    NSString *newMonthLabel = [NSString stringWithFormat:@"%@ %ld", monthName, (long)year];
    //if first time, do not animate
    if ([self.monthLabel.text isEqualToString:@""]) {
        self.monthLabel.text = newMonthLabel;
    } else if (![newMonthLabel isEqualToString:self.monthLabel.text]) {
        //animate the months
        [UIView animateWithDuration:0.1
                         animations:^{
                             self.monthLabel.alpha=0;
                         }
                         completion:^(BOOL animation) {
                             [UIView animateWithDuration:.2
                                              animations:^{
                                                  self.monthLabel.alpha=1;
                                                  self.monthLabel.text = newMonthLabel;
                                                  
                                              }];
                         }];
    }
    
    // derive the first day that will be displayed in the calendar:
    NSInteger deltaDays  = weekday-2;
    NSInteger mondayDay = day - deltaDays;
    while (mondayDay>1) {
        mondayDay-=7;
        deltaDays+=7;
    }
    dateComponents = [NSDateComponents new];
    [dateComponents setDay:-deltaDays];
    NSDate* firstDay = [calendar dateByAddingComponents:dateComponents toDate:newDate options:0];
    //NSLog(@"First date to display: %@", firstDay);
    
    int noOfTransactionInTheCurrentMonth=0;
    
    // note that this series typically will start before and end after the current month:
    for (int i=0; i<6; i++) {
        for (int j=0; j<7; j++) {
            int deltaCrtDay = i*7 + j;
            dateComponents = [NSDateComponents new];
            [dateComponents setDay:deltaCrtDay];
            
            // derive the date for the current day and assign it to the DayView:
            NSDate* currentDaysDate = [calendar dateByAddingComponents:dateComponents toDate:firstDay options:0];
            
            dateComponents = [NSDateComponents new];
            components = NSCalendarUnitDay | NSCalendarUnitMonth;
            dateComponents       = [calendar components:components fromDate:currentDaysDate];
            NSInteger crtMonth   = [dateComponents month];
                        DayView *dayView = [self.dayViewArray objectAtIndex:deltaCrtDay];
            [Utils log:@"Adding day %d for date %@", deltaCrtDay, currentDaysDate];
            dayView.date = currentDaysDate;
            dayView.isCurrentMonth = (crtMonth==month);
                       // find the number of transactions for the day
            NSMutableArray *crtDayTransactionArray=[NSMutableArray new];
           
            // assign only the transactions in the current month
            //.transactionDate
            if ([Utils isDate:currentDaysDate sameMonthWith:newDate]) {
                for (Transaction *transaction in self.transactionArray) {
                    if([Utils isDate:transaction.createdDate sameDayWith:currentDaysDate]){
                        TransactionStatusType *transactionType=   transaction.transactionStatusType;
                                               if([transactionType.transactionStatusTypeId integerValue]!=4)
                        {
                            //If Processor load all trnsaction
                            if([Utils getProcessorDidLogin])
                            {[crtDayTransactionArray addObject:transaction];}
                            
                            else //Hauler login
                            {
                             if([Utils getShowLastThreeMonths] )//show for jan feb dec
                                {
                                    [crtDayTransactionArray addObject:transaction];
                                }
                            else
                                if(transaction.transactionType.transactionTypeId!=TRANSACTION_TYPE_ID_PTR)
                                {
                                    [crtDayTransactionArray addObject:transaction];
}
                                }
                            //if(transaction.incomingRegistrant!=[Utils getIncomingRegistrant]||transaction.incomingUser!=[Utils getIncomingUser])
                           
                        }
                        noOfTransactionInTheCurrentMonth++;
                        if(transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_DELETED){
                           // if(transaction.incomingRegistrant!=[Utils getIncomingRegistrant]||transaction.incomingUser!=[Utils getIncomingUser])
                            noOfTransactionInTheCurrentMonth--;
                        }
                    }
                }
            }
            dayView.transactionArray = crtDayTransactionArray;
            dayView.button.tag = deltaCrtDay;
            
            [dayView updateUI];
            
        }
    }
    self.transactionCountLabel.text = [NSString stringWithFormat:@"%d",noOfTransactionInTheCurrentMonth];
	
}

-(void)showDayDetails:(DayView*)sender{
	
    DayView *theDayView = [self.dayViewArray objectAtIndex:sender.tag];
	
    if ([theDayView.transactionArray count] > 0)
	{
        self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        CalendarPopOverViewController *viewController = [[CalendarPopOverViewController alloc] init];
        viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        viewController.transactionsArray = theDayView.transactionArray;
        viewController.date = theDayView.date;
        viewController.delegate = self;
		
		//self.dockViewController.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
        viewController.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        [self.navigationController presentViewController:viewController animated:YES completion:nil];
    }
}


#pragma mark - IBActions

- (IBAction)listButtonPressed:(id)sender {
   // self.listViewController.searchStatus=OFF_SCREEN;
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)prevMonthPressed:(id)sender {
    self.deltaMonths--;
    //self.listViewController.deltaMonths=self.deltaMonths;
    [self updateCalendar];
}

- (IBAction)nextMonthPressed:(id)sender {
    self.deltaMonths++;
    self.listViewController.deltaMonths=self.deltaMonths;
    [self updateCalendar];
}

#pragma mark - CalendarPopupViewController Delegate

- (void)tableDidSelectRow:(NSNotification *)notification {
    Transaction *transaction = notification.object;
    [Utils setTransactionId:transaction.transactionId];
    
    if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_COMPLETE){
        ConfirmationPageViewController *viewController = [ConfirmationPageViewController new];
        [self.navigationController pushViewController:viewController animated:YES];
    } else if (transaction.transactionStatusType.transactionStatusTypeId!=TRANSACTION_STATUS_TYPE_ID_COMPLETE && ([transaction.incomingSignatureName length]!=0 || [transaction.outgoingSignatureName length]!=0)) {
        ConfirmationPageViewController *confViewController = [ConfirmationPageViewController new];
        TOCPageViewController *tocViewController = [TOCPageViewController new];
        tocViewController.rootViewController = self;
//        [self.navigationController pushViewController:tocViewController animated:NO];
//        [self.navigationController pushViewController:confViewController animated:YES];
        
        //We're modifying the stack, and committing the changes in one motion so that the movement to the last view controller is seamless.
        NSMutableArray *vcStack = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcStack addObject:tocViewController];
        [vcStack addObject:confViewController];
        [self.navigationController setViewControllers:vcStack animated:YES];
        
    } else {
        TOCPageViewController *viewController = [[TOCPageViewController alloc] init];
        viewController.rootViewController = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
}


@end

