//
//  ConfirmationTestPageViewController.m
//  TreadMarks
//
//  Created by Capris Group on 11/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ConfirmationTestPageViewController.h"
#import "DataEngine.h"
#import "EligibilityComponentView.h"
#import "SignatureComponentView.h"
#import "Constants.h"

@interface ConfirmationTestPageViewController ()
@property (nonatomic, strong)  DataEngine *dataEngine;
@end

@implementation ConfirmationTestPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.componentViewArray = [NSMutableArray new];
    
    // query the eligs where transactionType = our transaction type
    self.dataEngine = [DataEngine sharedInstance];
    
    self.transaction = [[self.dataEngine entities:TRANSACTION_ENTITY_NAME] objectAtIndex:0];
    
    EligibilityComponentView * eligibilityView = [[EligibilityComponentView alloc] init];
    eligibilityView.parentPageViewController = self;
    eligibilityView.delegate = self;
    eligibilityView.transaction = self.transaction;
    [self.componentViewArray addObject:eligibilityView];
    
    NSString *participant1 = self.transaction.outgoingUser.registrant.registrantType.shortDescriptionKey;
    NSString *participant2 = self.transaction.incomingUser.registrant.registrantType.shortDescriptionKey;
    NSString *type = self.transaction.transactionType.shortNameKey;
    
    NSLog(@"Participant 1 is: %@", participant1);
    NSLog(@"Participant 2 is: %@", participant2);
    NSLog(@"Transaction Type is: %@", type);
    
    SignatureComponentView *signatureView1 = [[SignatureComponentView alloc] initWithParticipant:1];
    signatureView1.parentPageViewController = self;
    signatureView1.delegate = self;
    signatureView1.transaction = self.transaction;
    [self.componentViewArray addObject:signatureView1];
    
    SignatureComponentView *signatureView2 = [[SignatureComponentView alloc] initWithParticipant:2];
    signatureView2.parentPageViewController = self;
    signatureView2.delegate = self;
    signatureView2.transaction = self.transaction;
    [self.componentViewArray addObject:signatureView2];
    
    [self updateUI];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI {
    CGFloat offset=0;
    for (ComponentView *componentView in self.componentViewArray) {
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  componentView.frame.size.height);
        [self.scrollView addSubview:componentView];
        [componentView updateUI];
        offset+=componentView.frame.size.height;
    }
    [self.scrollView setContentSize:CGSizeMake(640, offset * 1.25)];
}


@end
