//
//  ConfirmationTestPageViewController.h
//  TreadMarks
//
//  Created by Capris Group on 11/20/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "PageViewController.h"
#import "OpenSansLabel.h"
#import "NICSignatureView.h"
#import <QuartzCore/QuartzCore.h>

@class SignatureLandscapeViewController;

@interface ConfirmationTestPageViewController : PageViewController

@property (nonatomic, strong) Transaction *myTransaction;

@end
