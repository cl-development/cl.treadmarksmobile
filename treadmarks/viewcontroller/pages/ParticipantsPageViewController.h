//
//  ParticipantsPageViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-19.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageViewController.h"
#import "ComponentView.h"
#import "Transaction.h"

@interface ParticipantsPageViewController : PageViewController

@end
