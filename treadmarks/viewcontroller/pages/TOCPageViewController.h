//
//  TOCPageViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-19.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageViewController.h"
#import "ComponentView.h"
#import "Transaction.h"
#import "TOCEligibilityComponentView.h"
#import "TOCAuthorizationComponentView.h"
#import "TOCLocationComponentView.h"
#import "SlideView.h"
#import "TOCScaleTicketComponentView.h"
#import "AlertViewController.h"
#import "TOCOutgoingComponentView.h"

typedef NS_ENUM(int, TRANSACTION_TYPE) {
    TRANSACTION_TYPE_TCR = 1,
    TRANSACTION_TYPE_DOT = 2,
    TRANSACTION_TYPE_HIT = 3,
    TRANSACTION_TYPE_RTR = 4,
    TRANSACTION_TYPE_STC = 5,
    TRANSACTION_TYPE_UCR = 6,
    TRANSACTION_TYPE_PTR = 7,
    TRANSACTION_TYPE_PIT = 8
    
    
};

@interface TOCPageViewController : PageViewController <AlertViewDelegate>
@property int *newTransaction;
@property (nonatomic,weak) ListViewController *listViewController;
-(void)updateUI;
-(void) addComponentsForTransactionType;


-(TOCScaleTicketComponentView *)scaleTicketComponentView;
-(void)removeWeightVarianceCommentComponent;



@end
