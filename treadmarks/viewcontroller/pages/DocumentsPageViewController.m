//
//  DocumentsViewController.m
//  ComponentsPilot
//
//  Created by Dennis Christopher on 2013-10-24.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "DocumentsPageViewController.h"

@interface DocumentsPageViewController()

@end

@implementation DocumentsPageViewController

@end

/*
#import "DocumentsPageViewController.h"
#import "Constants.h"
#import "ComponentView.h"
#import "DataEngine.h"
#import "ScaleTicket.h"
#import "PhotoComponentView.h"
#import "ScaleTicketComponentView.h"
#import "NotesComponentView.h"
#import "EligibilityComponentView.h"
#import "ConfirmationPageViewController.h"

@interface DocumentsPageViewController ()
- (void)updateUI;
@property (nonatomic, weak) IBOutlet UIButton *continueButton;
@property (nonatomic, weak) IBOutlet UILabel *transactionTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel *transactionIDLabel;
@end

@implementation DocumentsPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.componentViewArray = [NSMutableArray new];
    
    DataEngine *dataEngine = [DataEngine sharedInstance];
    
    // get the photos related to the transaction
    Comment *comment = [dataEngine commentForTransaction:self.transaction];

    NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
    notesView.parentPageViewController = self;
    notesView.delegate = self;
    notesView.transaction = self.transaction;
    [self.componentViewArray addObject:notesView];
    
    // get the photos related to the transaction; always insure there is at least one photo component
    NSArray *photos = [dataEngine photosForTransaction:self.transaction];
    if ([photos count]==0)  {
         PhotoComponentView * photoView1 = [[PhotoComponentView alloc] initWithPhoto:nil];
         photoView1.parentPageViewController = self;
         photoView1.delegate = self;
         photoView1.transaction = self.transaction;
         [self.componentViewArray addObject:photoView1];
    }
    else {
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }
    }
    EligibilityComponentView * eligibilityView = [[EligibilityComponentView alloc] init];
    eligibilityView.parentPageViewController = self;
    //eligibilityView.transaction = self.transaction;
    eligibilityView.delegate = self;
    [self.componentViewArray addObject:eligibilityView];

    [self updateUI];
    [self updateComponentCanAddStatus];
    
    self.nextPageViewController = [ConfirmationPageViewController new];
    self.nextPageViewController.rootViewController = self.rootViewController;
}

- (void)updateUI {
    //update the screen items
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIDLabel.text = [self.transaction.friendlyId stringValue];

    BOOL pageIsReadOnly = NO;
    TransactionStatusType *theType = [[DataEngine sharedInstance] transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_COMPLETE];
    if (self.transaction.transactionStatusType==theType) {
        pageIsReadOnly = YES;
    }
    
    CGFloat offset=0;
    for (ComponentView *componentView in self.componentViewArray) {
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  componentView.frame.size.height);
        [self.scrollView addSubview:componentView];
        offset+=componentView.frame.size.height;
        componentView.readOnly = pageIsReadOnly;
        [componentView updateUI];
        
        //gray horizontal bar
        UIImageView *brImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trans-panel-br.png"]];
        brImageView.frame = CGRectMake(0, offset, COMPONENT_WIDTH, BR_IMAGE_HEIGHT);
        [self.scrollView addSubview:brImageView];
        offset += BR_IMAGE_HEIGHT;
    }
    //[self.scrollView setContentSize:CGSizeMake(640, offset * 1.25)];
    [self.scrollView setContentSize:CGSizeMake(COMPONENT_WIDTH, offset)];
    [self validate];
}

-(void) updateComponentCanAddStatus {
    ComponentView *lastPhotoView = nil;
    for (ComponentView *view in self.componentViewArray) {
        if ([view isKindOfClass:[PhotoComponentView class]]) {
            lastPhotoView = view;
        }
    }
    // tell the components to update
    for (PhotoComponentView *view in self.componentViewArray) {
        if ([view isKindOfClass:[PhotoComponentView class]] ) {
            if (view == lastPhotoView) {
                [view updateComponentCanAddStatus:YES];
            }
            else {
                [view updateComponentCanAddStatus:NO];
            }
        }
    }
}

- (IBAction)nextTempPressed:(id)sender {
    [self.navigationController pushViewController:self.nextPageViewController animated:YES];
}

- (IBAction)continueButtonPressed:(id)sender {
    [self.navigationController pushViewController:self.nextPageViewController animated:YES];
}



#pragma mark - ComponentViewDelegate
// concerned only with PhotoComponentViews
- (void)didRemoveComponent:(NSNotification *)notification {
    PhotoComponentView *view = notification.object;
    BOOL shouldAddPhotoComponent = [self isLastPhotoComponent];
    
    // if islast add an empty component
    if (shouldAddPhotoComponent==YES) {
         //[self.componentViewArray addObject:photoView1];
        [self insertPhotoComponentAfterComponent:view];
    }
    [self.componentViewArray removeObject:view];
    [view removeFromSuperview];
    [self updateUI];
    [self updateComponentCanAddStatus];
}

- (void)didAddComponent:(NSNotification *)notification {
    ComponentView *view = notification.object;
    //[self.componentViewArray addObject:view];
    [self insertPhotoComponentAfterComponent:view];
    [self updateUI];
    // tell components to update delete button etc
    [self updateComponentCanAddStatus];
}

// helpers:
-(void) insertPhotoComponentAfterComponent:(ComponentView *)thePhotoView {
    int theIndex = -1;
    theIndex = [self.componentViewArray indexOfObject:thePhotoView];
    if (theIndex != -1) {
        PhotoComponentView *newView = [[PhotoComponentView alloc] init];
        newView.parentPageViewController = self;
        newView.delegate = self;
        [self.componentViewArray insertObject:newView atIndex:theIndex+1];
    }
}

- (BOOL)isLastPhotoComponent {
     BOOL isLast = NO;
     int count = 0;
     
     for (ComponentView *view in self.componentViewArray) {
         if ([view isKindOfClass:[PhotoComponentView class]]) {
             count = count + 1;
         }
    }
     if (count == 1) {
         isLast = YES;
     }
     return isLast;
}


#pragma mark - Validation

-(void)validate{
    [super validate];
    if (self.valid) {
        [self.continueButton setImage:[UIImage imageNamed:@"trans-btn-cont-on.png"] forState:UIControlStateNormal];
        self.continueButton.enabled = YES;
    } else {
        [self.continueButton setImage:[UIImage imageNamed:@"trans-btn-cont-off.png"] forState:UIControlStateNormal];
        self.continueButton.enabled = NO;
    }
}


@end
*/
