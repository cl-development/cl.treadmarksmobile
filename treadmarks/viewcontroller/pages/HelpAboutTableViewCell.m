//
//  HelpAboutTableViewCell.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "HelpAboutTableViewCell.h"

@interface HelpAboutTableViewCell ()

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *DeviceNameLabel;

@end

@implementation HelpAboutTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.appVersionLabel.text = [NSString stringWithFormat:@"TreadMarks Mobile %@ (%@) UAT",
//                                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
//                                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
	
    self.appVersionLabel.text = [NSString stringWithFormat:@"TreadMarks Mobile %@ (%@)",
                                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];

//    self.appVersionLabel.text = [NSString stringWithFormat:@"TreadMarks Mobile %@ (%@) QA",
//								 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
//								 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
	self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *deviceName = [[UIDevice currentDevice] name];
    //self.DeviceNameLabel.text = [NSString stringWithFormat:@"Device ID: %@",deviceName];
    self.DeviceNameLabel.text = deviceName;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
