//
//  TempMaterialQuantityPageViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 11/19/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageViewController.h"

@interface MaterialQuantityPageViewController : PageViewController

@end
