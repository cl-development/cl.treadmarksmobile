//
//  StartViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-17.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "StartViewController.h"
#import "LoginViewController.h"
#import "ParticipantsPageViewController.h"
#import "ViewCoreDataViewController.h"
#import "DocumentsPageViewController.h"
#import "Utils.h"
#import "ConfirmationPageViewController.h"
#import "SendFTP.h"

@interface StartViewController ()
@property(nonatomic, assign) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //push the login screen
  //  [self loginButtonPressed:nil];
}

- (IBAction)loginButtonPressed:(id)sender {
    LoginViewController *viewController = [LoginViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)viewDatabaseButtonPressed:(id)sender {
    ViewCoreDataViewController *viewController = [ViewCoreDataViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}


- (IBAction)participantButtonPresses:(id)sender {
    ParticipantsPageViewController *viewController = [ParticipantsPageViewController new];
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)sendDebugLog:(id)sender {
    SendFTP *sftp = [[SendFTP alloc] init];
    NSString *pathToLogFile = [sftp logFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToLogFile]==NO) {
        self.statusLabel.text = @"No log file found.";
        return;
    }
    [sftp startSend:pathToLogFile];
}

/*- (IBAction)documentsButtonPressed:(id)sender {
    DocumentsPageViewController *viewController = [DocumentsPageViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}*/

- (IBAction)confirmationViewButtonPressed:(id)sender {
    ConfirmationPageViewController *viewController = [ConfirmationPageViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

/*- (IBAction)confirmationTestViewButtonPressed:(id)sender {
    ConfirmationTestPageViewController *viewController = [ConfirmationTestPageViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}*/

- (IBAction)tireCountButtonPressed:(id)sender {
    /*
    MaterialQuantityPageViewController *viewController = [MaterialQuantityPageViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
    */
}

/*- (IBAction)preloadData:(id)sender {
    [TestData populateDatabase];
}*/


/*- (IBAction)preloadCSVData:(id)sender {
    
    CSVData *testD = [[CSVData alloc] init];
    [testD addData];
    [testD addUser];
    
}*/



- (IBAction)showAMessagePressed:(id)sender {
//    [Utils showMessage:self header:@"this is a header" message:@"and this is a message"];
}

- (IBAction)showAMessageWithDelayPressed:(id)sender {
    [Utils showMessage:self header:@"this is a header" message:@"and this is a message that will go away in 5 seconds" hideDelay:5];
}


- (IBAction)clearDoNotShowPressed:(id)sender {
    [Utils log:@"Clearing Do Not Show status for Tire Screen"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:TIRE_COUNT_DO_NOT_SHOW_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
