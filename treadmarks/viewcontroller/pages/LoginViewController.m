//
//  LoginViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 10/30/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>
#import "Location+Location.h"
#import "LoginViewController.h"
#import "ListViewController.h"
#import "Utils.h"
#import "User.h"
#import "Registrant.h"
#import "DataEngine.h"
#import "Constants.h"
#import "GPSLog.h"
#import "DataEngine.h"
#import "MessageViewController.h"
#import "CryptoService.h"
#import "Convert.h"
#import "ZXingObjC.h"
#import "Gzip.h"
//#import "DockViewController.h"
#import "Transaction+Transaction.h"
#import "MHWCoreDataController.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Transaction_Eligibility+Transaction_Eligibility.h"
#import "PhotoPreselectComment.h"
#import "Comment+Comment.h"
#import "Authorization+Authorization.h"
#import "STCAuthorization+STCAuthorization.h"
#import "ScaleTicket+ScaleTicket.h"
#import "NSManagedObject+NSManagedObject.h"
#import "ModalViewController.h"
#import "TermsView.h"
#import "NewAlertView.h"
#import "TCViewController.h"

@interface LoginViewController () <ZXCaptureDelegate, QRScanDelegate, TermsViewDelegate>

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIImageView *crossHairImageView;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *slideToLoginContainerView;
@property (weak, nonatomic) IBOutlet UIView *shutterView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *loadTestTransactionsButton;
@property (weak, nonatomic) IBOutlet UIPanGestureRecognizer *slideToLoginPanGesture;
@property (weak, nonatomic) IBOutlet UIButton *loginNoQRCodeTempButton;
@property (weak, nonatomic) IBOutlet UIView *slideToLoginWrapperView;
@property (weak, nonatomic) IBOutlet UILabel *VerNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *DeviceNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *AcceptTermsView;
@property (weak, nonatomic) IBOutlet UIButton *DeclineTermsView;
- (IBAction)DeclineTermsAction:(id)sender;
- (IBAction)AcceptTermsAction:(id)sender;

#pragma mark - Properties

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) ZXCapture* capture;
@property CGPoint originalCenter;
@property float animationValue;
@property (strong) CALayer* maskLayer;
@property BOOL isAnimating;

@property (weak, nonatomic) IBOutlet UIView *termsview;
@end

@implementation LoginViewController

#pragma mark - Override Methods

#define LOCATION_SERVICES_REQUIRED_MESSAGE @"TreadMarks Mobile needs to use the iPad's location services so transactions can be created. Please enable the App Settings. NOTE: If Location services are off for the device please enable. Settings --> Privacy --> Location Services"

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	
    if (self)
    {
        self.slideToLogin = true;
        self.delegate = self;
        self.hideBackButton = YES;
		
#if DEBUG
        self.hideBackButton = YES;   //true;
#endif
		self.shouldEnableCameraOnAppear = false;
		self.isAnimating = NO;
    }
    
    return self;
}
- (void) viewDidLoad {
	
    /// New code for OTSJ-408
    [self initSlideToLogin];
    
    [super viewDidLoad];
    // SHOW THE VERSION
//    self.VerNoLabel.text = [NSString stringWithFormat:@"Version: %@ QA",
//                                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
//                                 ];
//    self.VerNoLabel.text = [NSString stringWithFormat:@"Version: %@ UAT",
//                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
//    self.VerNoLabel.text = [NSString stringWithFormat:@"Version: %@ PROD",
//                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
//                            ];
    
    
    self.VerNoLabel.text = [NSString stringWithFormat:@"Version: %@ PRO UAT",
                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
                            ];

    NSString *deviceName = [[UIDevice currentDevice] name];
    self.DeviceNameLabel.text = [NSString stringWithFormat:@"Device: %@",deviceName];
    //self.DeviceNameLabel.text = @"Device ID: ASDF56ASDGDE0";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableCamera) name:@"LoginViewControllerShouldShowCamera" object:nil];

	
#if DEBUG
	self.loginNoQRCodeTempButton.hidden = NO;
	//self.backButton.enabled = YES;
	//self.loadTestTransactionsButton.hidden = NO;
#endif
    
   /* if (self.slideToLogin==YES) {
        self.loginNoQRCodeTempButton.hidden = YES;
    }*/
    
    if (self.isSlideToLoginInvisible == YES) {
        self.slideToLoginImageView.hidden = YES;
        self.slideToLoginView.hidden = YES;
        self.cameraView.hidden = YES;
        self.crossHairImageView.hidden = YES;
        self.loginImageView.hidden = YES;
    }
        self.backButton.hidden = self.hideBackButton;
	self.originalCenter = self.slideToLoginWrapperView.center;
	[self initialDatabasePopulate];
   // [self UpdateScaleTicketRecords];
    
    
    //CHECK IF FLIP DATE
    NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:1973];
    [dateComponents setMonth:5];
    [dateComponents setDay:23];
    NSDate* LiveDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    if ([[NSDate date] compare:LiveDate] == NSOrderedDescending ||[[NSDate date] compare:LiveDate] == NSOrderedSame )
    {
        [Utils setFlipServer:YES];    }
    else
    {
        [Utils setFlipServer:NO];
    }

    
}
-(void)UpdateScaleTicketRecords
{
#define LBS_TO_KGS 0.453592
    
    
    
    
    // code to run once: thin out the scale ticket records to the new style
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"kScaleTicketStepUp"])
    {
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                       {
                           // get only the PTRs
                           NSArray *transactions =[[DataEngine sharedInstance] transactionsOfType:TRANSACTION_TYPE_ID_PTR];
                           
                           for (Transaction *transaction in transactions) {
                               // we only care about the paired tickets
                               NSArray *pair =[[DataEngine sharedInstance] scaleTicketsForTransaction:transaction andPhotoType:PHOTOTYPE_ID_SCALE_TICKET];
                               
                               if ([pair count]==2) {
                                   ScaleTicket *inboundTicket = nil;
                                   ScaleTicket *outboundTicket = nil;
                                   ScaleTicket *temp = (ScaleTicket *)[pair objectAtIndex:0];
                                   if ([temp.scaleTicketType.scaleTicketTypeId isEqual:@SCALE_TICKET_TYPE_INBOUND]) {
                                       inboundTicket = temp;
                                   }
                                   else {
                                       if ([temp.scaleTicketType.scaleTicketTypeId isEqual:@SCALE_TICKET_TYPE_OUTBOUND]) {
                                           outboundTicket = temp;
                                       }
                                   }
                                   temp = (ScaleTicket *)[pair objectAtIndex:1];
                                   if ([temp.scaleTicketType.scaleTicketTypeId isEqual:@SCALE_TICKET_TYPE_INBOUND]) {
                                       inboundTicket = temp;
                                   }
                                   else {
                                       if ([temp.scaleTicketType.scaleTicketTypeId isEqual:@SCALE_TICKET_TYPE_OUTBOUND]) {
                                           outboundTicket = temp;
                                       }
                                   }
                                   if (inboundTicket != nil && outboundTicket != nil) {
                                       if ([inboundTicket.ticketNumber isEqualToString:outboundTicket.ticketNumber]==YES) {
                                           ScaleTicket *ticketOfTypeBoth = ( ScaleTicket *)[[DataEngine sharedInstance] newEntity:SCALE_TICKET_ENTITY_NAME];
                                           ScaleTicketType *theType = [[DataEngine sharedInstance] scaleTicketTypeForId:@SCALE_TICKET_TYPE_BOTH];
                                           ticketOfTypeBoth.scaleTicketType = theType;
                                           ticketOfTypeBoth.date = inboundTicket.date; // this is arbitrary-could use outbound
                                           ticketOfTypeBoth.transaction = transaction;
                                           
                                           // the old system allowed different unit types which we may have to convert to KGs
                                           if (inboundTicket.unitType != outboundTicket.unitType) {
                                               float calculatedWeight = 0.0;
                                               NSNumber *calculatedNumber = nil;
                                               if (inboundTicket.unitType.unitTypeId==UNIT_TYPE_ID_POUND) {
                                                   calculatedWeight = [inboundTicket.inboundWeight floatValue] * LBS_TO_KGS;
                                                   calculatedNumber = [NSNumber numberWithFloat:calculatedWeight];
                                                   inboundTicket.inboundWeight = [NSDecimalNumber decimalNumberWithString:[calculatedNumber stringValue]];
                                               }
                                               else if (outboundTicket.unitType.unitTypeId==UNIT_TYPE_ID_POUND) {
                                                   calculatedWeight = [outboundTicket.outboundWeight floatValue] * LBS_TO_KGS;
                                                   calculatedNumber = [NSNumber numberWithFloat:calculatedWeight];
                                                   outboundTicket.outboundWeight = [NSDecimalNumber decimalNumberWithString:[calculatedNumber stringValue]];
                                               }
                                           }
                                           // if scaleticket has same unit and is type of both Convert
                                           
                                           else {
                                               float calculatedWeight = 0.0;
                                               NSNumber *calculatedNumber = nil;
                                               if (inboundTicket.unitType.unitTypeId==UNIT_TYPE_ID_POUND)
                                               {
                                                   calculatedWeight = [inboundTicket.inboundWeight floatValue] * LBS_TO_KGS;
                                                   calculatedNumber = [NSNumber numberWithFloat:calculatedWeight];
                                                   inboundTicket.inboundWeight = [NSDecimalNumber decimalNumberWithString:[calculatedNumber stringValue]];
                                                   calculatedWeight = [outboundTicket.outboundWeight floatValue] * LBS_TO_KGS;
                                                   calculatedNumber = [NSNumber numberWithFloat:calculatedWeight];
                                                   outboundTicket.outboundWeight = [NSDecimalNumber decimalNumberWithString:[calculatedNumber stringValue]];
                                               }
                                           }
                                           
                                           ticketOfTypeBoth.inboundWeight = inboundTicket.inboundWeight;
                                           ticketOfTypeBoth.outboundWeight = outboundTicket.outboundWeight;
                                           // assign the photo that goes with inbound scale ticket
                                           ticketOfTypeBoth.photo = inboundTicket.photo;
                                           ticketOfTypeBoth.ticketNumber = inboundTicket.ticketNumber;
                                           ticketOfTypeBoth.unitType = [[DataEngine sharedInstance] unitTypeForId:UNIT_TYPE_ID_KILOGRAM];
                                           
                                           ticketOfTypeBoth.scaleTicketId = [[NSUUID UUID] UUIDString];
                                           // syncDate left at nil
                                           
                                           // clean up:
                                           [self deleteScaleTicket:inboundTicket];
                                           [self deleteScaleTicket:outboundTicket];
                                       }
                                   }
                               }
                           }
                           [[DataEngine sharedInstance] saveContext];
                           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"kScaleTicketStepUp"];
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                           
                           
                           [self dismissPopulatingDatabaseMessage];
                       });
        
        [self presentPopulatingDatabaseMessage];
    }
    
    
    
    
    
}

-(void)deleteScaleTicket:(ScaleTicket *)ticket {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    [context deleteObject:ticket];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self addAnimation];
	
	if(self.shouldEnableCameraOnAppear)
	{
		[self enableCamera];
	}
}
- (void) viewDidUnload {
	
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*- (void) viewWillAppear:(BOOL)animated {
	
    [super viewWillAppear:animated];
    
    /// If sliding in not required, then run the camera immediately
    if(self.slideToLogin == false)
    {
        [self enableCamera];
    }
}*/

- (void) viewWillDisappear:(BOOL)animated {

	///NSLog(@"viewWillDisappear");

    [super viewWillDisappear:animated];
    
    [self disableCamera];
	
	[self removeAllAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidDisappear:(BOOL)animated
{
    //NSLog(@"viewDidDisappear");
  
}
#pragma mark - UI related methods

- (void)addAnimation {

	if (self.isAnimating == NO)
	{
		self.maskLayer.backgroundColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25] CGColor];
		
		CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"position.x"];
		animation.byValue = [NSNumber numberWithFloat:self.animationValue];
		animation.repeatCount = HUGE_VALF;
		animation.duration = 1.5;
		
		[self.maskLayer addAnimation:animation forKey:NULL];
		
		self.isAnimating = YES;
		
		//NSLog(@"Animation added");
	}
}
- (void)removeAllAnimations {

    self.maskLayer.backgroundColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:1] CGColor];
    
    [self.maskLayer removeAllAnimations];
	
	self.isAnimating = NO;

	//NSLog(@"Animation removed");
}
- (void) initSlideToLogin {

    if(self.slideToLogin)
    {
        UIImage* textImage = [UIImage imageNamed:@"login-slidtetologin-text"];
        UIImage* maskImage = [UIImage imageNamed:@"Mask"];
        float maskWidth = textImage.size.width * 2 + maskImage.size.width;
        
        self.maskLayer = [CALayer layer];
        self.maskLayer.contents = (id)[maskImage CGImage];
        self.maskLayer.contentsGravity = kCAGravityCenter;
        self.maskLayer.frame = CGRectMake(-maskWidth + textImage.size.width, 0, maskWidth, textImage.size.height);
        
        self.animationValue = textImage.size.width + maskImage.size.width;
        
        CALayer* textLayer = [CALayer layer];
        textLayer.contents = (id)[textImage CGImage];
        textLayer.frame = CGRectMake(0, 0, textImage.size.width, textImage.size.height);
        textLayer.mask = self.maskLayer;
        
        [self.slideToLoginContainerView.layer addSublayer:textLayer];
    }
}

#pragma mark

- (void)presentPopulatingDatabaseMessage {
	
	//NSLog(@"Populating current version.");
	//show a message view
	self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
	
	MessageViewController *messageViewController    = [MessageViewController new];
	messageViewController.closeOnTap = NO;
	messageViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	
	messageViewController.header    = @"Database";
	messageViewController.message   = @"Loading default data\nplease wait...";
	
	[self.navigationController presentViewController:messageViewController animated:NO completion:nil];
}
- (void)dismissPopulatingDatabaseMessage {
	
	//to be revisited
	/*
	[self performSelector:@selector(hideLoadingMessage) withObject:nil afterDelay:10];
	 */
	[self hideLoadingMessage];
}
-(void)initialDatabasePopulate {
	
	if ([[MHWCoreDataController sharedInstance] isMigrationNeeded])
	{
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
					   {
						   if ([[MHWCoreDataController sharedInstance] migrate:nil] == NO)
						   {
#if DEBUG
							   @throw [NSException exceptionWithName:@"Data migration error" reason:nil userInfo:nil];
#endif
						   }

						   [self dismissPopulatingDatabaseMessage];
					   });
			
		[self presentPopulatingDatabaseMessage];
	}
}
-(void)hideLoadingMessage{
    
   
    [Utils log:@"hideLoadingMessage"];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
}
- (void)resetSlider:(UIView *)view {
	
	[UIView animateWithDuration:0.5
					 animations:^{
						 
						 view.center = self.originalCenter;
		 }];

	[self addAnimation];
}
- (void)reset {

	NSLog(@"reset");

	self.slideToLoginPanGesture.enabled = YES;
	self.cameraView.hidden = YES;
	[self disableCamera];
	[self resetSlider:self.slideToLoginPanGesture.view];
    
    [Utils setProcessorDidLogin:NO];
}

#pragma mark - Camera Methods

- (void) enableCamera {

    self.cameraView.hidden = false;

    self.capture = [[ZXCapture alloc] init];

    AVAuthorizationStatus authStatus;
    authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    //!!!! Will work on iOS8 and above only!!!!!
    //If no access goto setting page for app
    if (authStatus == AVAuthorizationStatusDenied)
    {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Camera Access Required"
                                                                            message: @"TreadMarks Mobile needs to use the iPad's camera so it can scan your QR Code. Please enable it in Settings."
                                                                     preferredStyle: UIAlertControllerStyleAlert];

        UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Settings"
                                                              style: UIAlertActionStyleDestructive
                                                            handler: ^(UIAlertAction *action) {
                                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString

                                                                                                            ]];;
                                                            }];

        [controller addAction: alertAction];
        
        [self presentViewController: controller animated: YES completion: nil];
    }

	NSLog(@"enableCamera");
	
    	self.capture.hints.encoding = NSISOLatin1StringEncoding;
	self.capture.rotation = 90.0f;
	// Use the back camera
	self.capture.camera = self.capture.back;
	self.capture.layer.frame = self.shutterView.bounds;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

		//NSLog(@"self.capture.delegate = self;");
		self.capture.delegate = self;
		
		dispatch_async(dispatch_get_main_queue(), ^{
			
			NSLog(@"[self.shutterView.layer addSublayer:self.capture.layer];");
			[self.shutterView.layer addSublayer:self.capture.layer];
		});
	});
	
	self.shouldEnableCameraOnAppear = false;
}
- (void) disableCamera {
    
	//NSLog(@"disableCamera");
	
	if (self.capture == nil)
	{
		return;
	}
	
	NSLog(@"[self.capture.layer removeFromSuperlayer];");
	[self.capture.layer removeFromSuperlayer];

	NSLog(@"self.capture.delegate = nil;");
	self.capture.delegate = nil;
	
	/*
	NSLog(@"[self.capture stop];");
	[self.capture stop];
	 */
	
	NSLog(@"self.capture = nil;");
	self.capture = nil;
}

#pragma mark -

/// Authenticates jsonString with CoreData and delegate.
///	If successful, return the user instance,
/// else, returns null.
- (User*) authenticate:(NSDictionary*)jsonData {
    
    NSInteger userIdFromJson = [[jsonData objectForKey:QR_CODE_USER_ID_KEY] integerValue];
    NSInteger registrantIdFromJson = [[jsonData objectForKey:QR_CODE_REGISTRANT_ID_KEY] integerValue];
    
    if (!userIdFromJson || !registrantIdFromJson) {
        //NSLog(@"parseAndUpdateControls:error: userId=%ld, registrantId=%ld", (long)userIdFromJson, (long)registrantIdFromJson);
        [Utils showMessage:self header:@"Error" message:@"Wrong QR code format" hideDelay:2 hideCompletion:^
         {
             [self enableCamera];
         }];
        
        return false;
    }
    
    

    
    NSNumber *userIdNumber = [NSNumber numberWithInteger:userIdFromJson];
    [Utils log:@"userIdNumber = %@", [userIdNumber stringValue]];
    
    NSNumber *registrantIdNumber = [NSNumber numberWithInteger:registrantIdFromJson];
    [Utils log:@"registrantIdNumber = %@", [registrantIdNumber stringValue]];
    
    User *crtUser = [[DataEngine sharedInstance] userForId:userIdNumber andRegistrantId:registrantIdNumber];
	
	if(crtUser == NULL)
	{
        [Utils showMessage:self header:@"LOGIN" message:@"Login error. Try again!" hideDelay:2 hideCompletion:^
         {
             [self enableCamera];
         }];
        
		[Utils log:@"Login error. Try again!"];
		
		return NULL;
	}
	
	// If validateUser is not implemented, then
	if([self.delegate respondsToSelector:@selector(validateUser:)] &&
	   [self.delegate validateUser:crtUser] == false)
	{
		return NULL;
	}
	
	return crtUser;
}

//load the next screen
-(void)loadTransactionList {
    
    [self saveLocation];
    [[ModalViewController sharedModalViewController] hideView];
	ListViewController *listViewController = [[ListViewController alloc] init];
	//UINavigationController* navigationController = [[UINavigationController alloc] init];
	//navigationController.navigationBarHidden = YES;
	[self.navigationController pushViewController:listViewController animated:NO];
	
	/*DockViewController* dockViewController = [[DockViewController alloc] init];
	[dockViewController addChildViewController:navigationController];
	[dockViewController.view addSubview:navigationController.view];
	[dockViewController.view sendSubviewToBack:navigationController.view];
	
	[self.navigationController pushViewController:dockViewController animated:YES];*/
}
-(void)saveLocation {

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
	[self.locationManager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
	
    CLLocation *newLocation=[locations lastObject];
    [Utils log:@"Login.location: %@",[newLocation description]];
    GPSLog *gpsLog = (GPSLog*)[[DataEngine sharedInstance] newEntity:GPS_LOG_ENTITY_NAME];
    gpsLog.gpsLogId = [[NSUUID UUID] UUIDString];
    gpsLog.timestamp = [NSDate date];
    gpsLog.latitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    gpsLog.longitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    gpsLog.syncDate = nil;
    [[DataEngine sharedInstance] saveContext];
    [Utils setGPSId:gpsLog.gpsLogId];
	
    [manager stopUpdatingLocation];
	
	self.locationManager = nil;
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [Utils log:@"Location error:",[error localizedDescription]];
   
    if ( [CLLocationManager authorizationStatus] ==  kCLAuthorizationStatusDenied)    {
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Location  Access Required"
                                                                            message:LOCATION_SERVICES_REQUIRED_MESSAGE
                                         
                                                                     preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Settings"
                                                              style: UIAlertActionStyleDestructive
                                                            handler: ^(UIAlertAction *action) {
                                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];;
                                                            }];
        
        [controller addAction: alertAction];
        
        [self presentViewController: controller animated: YES completion: nil];
    }

        
    

    [manager stopUpdatingLocation];

	self.locationManager = nil;
    

}
/*
- (void)stopUpdatingLocation:(NSString *)state {
	
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
}
 */

#pragma mark - ZXCaptureDelegate Methods

- (void) captureResult:(ZXCapture*)capture result:(ZXResult*)result {

	NSLog(@"captureResult");

    if (result)
    {
		[self disableCamera];
		
		/// "Valid" == QR code is readable
		BOOL isQRCodeValid = YES;
		
        // We got a result. Display information about the result onscreen.
        NSData* encryptedData = [result.text dataUsingEncoding:NSISOLatin1StringEncoding];
        NSData* key = [Convert fromHexStringToData:QR_CODE_DECRYPTION_KEY_HEX];
        NSData* decryptedData = [CryptoService decrypt:encryptedData withKey:key];
        NSData* uncompressedData = [Gzip uncompress:decryptedData];
		NSDictionary* jsonDictionary = nil;
		NSError* error;
		
		if (uncompressedData != nil)
		{
			jsonDictionary = [NSJSONSerialization JSONObjectWithData:uncompressedData
															 options:kNilOptions
															   error:&error];
		}
        
		if (jsonDictionary == nil)
		{
			/// Before July 1, 2014, plain-text QR codes can be used
			NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
			[dateComponents setYear:2014];
			[dateComponents setMonth:7];
			[dateComponents setDay:1];
			NSDate* ineffectiveDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
			if ([[NSDate date] compare:ineffectiveDate] == NSOrderedAscending)
			{
				jsonDictionary = [NSJSONSerialization JSONObjectWithData:[result.text dataUsingEncoding:NSUTF8StringEncoding]
																 options:kNilOptions
																   error:&error];
				
				isQRCodeValid = jsonDictionary != nil;
			}
			else
			{
				isQRCodeValid = NO;
			}
		}
		
		if (isQRCodeValid)
		{
			User* user = [self authenticate:jsonDictionary];
			
			if(user)
			{
				[self.delegate proceed:user];
				
				return;
			}
		}
		else
		{
            [Utils showMessage:self header:@"Error" message:@"Wrong QR code format" hideDelay:2 hideCompletion:^
             {
                 [self enableCamera];
             }];
		}
		
		/// There might be a message view that's on top so only
		/// enable the camera when it is the visible view.
		if(self.isBeingPresented)
		{
			[self enableCamera];
		}
		else
		{
			/// Else, just set a flag so that when the view will become visible,
			/// it will turn the camera on
			self.shouldEnableCameraOnAppear = true;
		}
	}
}
- (void) captureSize:(ZXCapture*)capture width:(NSNumber*)width height:(NSNumber*)height {
}

#pragma mark - QRScanDelegate Methods

- (void) proceed: (User*) user {
    
    [Utils setUserId:user.userId];
	[Utils setRegistrantId:user.registrant.registrationNumber];
    
	NSString *welcomeText = [NSString stringWithFormat:@"Welcome, %@", user.registrant.businessName];
	//[Utils showMessage:self header:@"LOGIN" message:@"Login successful" hideDelay:1];
	[Utils showMessage:self header:@"LOGIN SUCCESSFUL" message:welcomeText hideDelay:2
	 hideCompletion:^
	 {
		 /// If user has never accepted the terms or it has been greater than 24 hours
		 if (user.termsAcceptedDate == nil ||
			 [[NSDate date] timeIntervalSinceDate:user.termsAcceptedDate] / 60.0 / 60.0 > 24.0)
		 {
			 TermsView *termsView = [[TermsView alloc] init];
			 termsView.frame = CGRectMake(49, 267, termsView.frame.size.width, termsView.frame.size.height);
			 termsView.delegate = self;
			 termsView.user = user;
			 
             /*TCViewController *tcView=[[TCViewController alloc]init];
             
             self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
             tcView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
             
             [self.navigationController pushViewController:tcView animated:NO];*/
             
			 [[ModalViewController sharedModalViewController] showView:termsView];
             
             //[self.view addSubview:self.termsview   ];
             //self.termsview.hidden=NO;
		 }
		 else
		 {
			 /// Proceed as usual
			 [self loadTransactionList];
		 }
		 
	 }];
}
- (bool)validateUser:(User *)user {
	
    // Modify to allow processor login:
    NSNumber *registrantType = user.registrant.registrantType.registrantTypeId;
	if(registrantType != REGISTRANT_TYPE_ID_HAULER && registrantType != REGISTRANT_TYPE_ID_PROCESSOR)
	{
        //[Utils showMessage:self header:@"LOGIN" message:@"Invalid registrant type." hideDelay:2];
		[Utils log:@"Login error. Invalid registrant type."];
        
        [Utils showMessage:self
                    header:@"LOGIN"
                   message:@"Invalid registrant type."
                 hideDelay:2
            hideCompletion:^
         {
             [self enableCamera];
         }];

		
		return NO;
	}
    // TEMPORARY till full Processor login, create transaction etc.
   
        // PTR by processor only
    if(registrantType == REGISTRANT_TYPE_ID_PROCESSOR) {
        [Utils setProcessorDidLogin:YES];
        
        
    }
    else {
        [Utils setProcessorDidLogin:NO];
    }
	
	return YES;
}

#if DEBUG
- (void)backButtonTouchUpInside {
    
    [self backButtonPressed:nil];
}
#endif

#pragma mark - IBAction Methods

- (IBAction) backButtonTouchUpInside:(id)sender {
    [self disableCamera];
    [self.delegate backButtonTouchUpInside];
}
- (IBAction) slideToLogin:(UIPanGestureRecognizer *)sender {
        //Restructure to fix slide Animation
;

    if ( [CLLocationManager authorizationStatus] ==  kCLAuthorizationStatusDenied)    {

        UIAlertController *controller = [UIAlertController alertControllerWithTitle: @"Location  Access Required"
                                                                            message:LOCATION_SERVICES_REQUIRED_MESSAGE
                                                                     preferredStyle: UIAlertControllerStyleAlert];

        UIAlertAction *alertAction = [UIAlertAction actionWithTitle: @"Settings"
                                                              style: UIAlertActionStyleDestructive
                                                            handler: ^(UIAlertAction *action) {
                                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];;
                                                            }];

        [controller addAction: alertAction];

        [self presentViewController: controller animated: YES completion: nil];
    }


    UIView* view = sender.view;

    if(sender.state == UIGestureRecognizerStateBegan)
    {
        self.originalCenter = view.center;

        [self removeAllAnimations];
    }
    else if(sender.state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [sender translationInView:view];

        if(translation.x >= view.bounds.size.width)
        {
            sender.enabled = false;

            [self enableCamera];
        }

        CGFloat x = translation.x < 0 ? 0 : translation.x;

        view.center = CGPointMake(self.originalCenter.x + x, self.originalCenter.y);
    }
    else if(sender.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:0.5
                         animations:^
         {
             view.center = CGPointMake(self.originalCenter.x, self.originalCenter.y);
         }];

        [self addAnimation];
    }





}
- (IBAction) backButtonPressed:(id)sender {
    [self disableCamera];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction) loginNoQRCodePressed:(id)sender {
	
	User* user = [User single:@"userId == %@ && registrant.registrationNumber == %@", @5, @3000018];
    
    //remove before release
    NSDateComponents* dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:2015];
    [dateComponents setMonth:02];
    [dateComponents setDay:1];
    NSDate* processorPtrDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    if ([[NSDate date] compare:processorPtrDate] == NSOrderedDescending ||[[NSDate date] compare:processorPtrDate] == NSOrderedSame )
    {
        [Utils setProcessorPtrOnly:YES];
    }
    else
    {
        [Utils setProcessorPtrOnly:NO];
    }
//////////////////
	[self proceed:user];
}

#pragma mark - TermsViewDelegate

- (void)acceptTerms:(User *)user {

   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        user.termsAcceptedDate = [NSDate date];
        [user.managedObjectContext save:nil];
        
        [[ModalViewController sharedModalViewController] dismissView];

        

   // });
    
    		[self loadTransactionList];
    
}
	- (void)declineTerms {
	// [[ModalViewController sharedModalViewController] removeview];
	//[[ModalViewController sharedModalViewController] hideView];
	 [[ModalViewController sharedModalViewController] dismissView];
        [self reset];
}
- (IBAction)loadTestTransactions:(UIButton *)sender {
	
	[Transaction loadTestTransactions:nil];
}

- (IBAction)DeclineTermsAction:(id)sender {
}

- (IBAction)AcceptTermsAction:(id)sender {
    
   // user.termsAcceptedDate = [NSDate date];
    //[user.managedObjectContext save:nil];
    //[self loadTransactionList];

}
@end
