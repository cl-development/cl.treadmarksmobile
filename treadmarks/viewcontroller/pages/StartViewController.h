//
//  StartViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-17.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SignatureLandscapeHelperViewController.h"
#import "SignatureHelperNavigationController.h"

@interface StartViewController : UIViewController


@property(nonatomic,weak) IBOutlet UILabel *statusLabel;

@end
