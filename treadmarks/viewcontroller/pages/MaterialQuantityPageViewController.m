//
//  TempMaterialQuantityPageViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 11/19/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "MaterialQuantityPageViewController.h"
#import "TransactionType.h"
#import "TireCountComponentView.h"
#import "TireCountMessageComponentView.h"
//#import "DockViewController.h"
//#import "SlideView.h"

@interface MaterialQuantityPageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (nonatomic, weak) IBOutlet UIImageView *transTypeImageView;


@end

@implementation MaterialQuantityPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.componentViewArray = [NSMutableArray new];
    TireCountComponentView * tireCountComponentView = [TireCountComponentView new];
    tireCountComponentView.parentPageViewController = self;
    [self.componentViewArray addObject:tireCountComponentView];

    TireCountMessageComponentView * tireCountMessageComponentView = [TireCountMessageComponentView new];
    tireCountMessageComponentView.parentPageViewController = self;
    [self.componentViewArray addObject:tireCountMessageComponentView];

    [self updateUI];
    
    /*self.nextPageViewController = [DocumentsPageViewController new];
    self.nextPageViewController.rootViewController = self.rootViewController;*/
}
-(void)viewDidDisappear:(BOOL)animated
{
   

}
- (void)updateUI {
    //update the screen items
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIdLabel.text = [self.transaction.friendlyId stringValue];
    
    CGFloat offset=0;
    for (ComponentView *componentView in self.componentViewArray) {
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  componentView.frame.size.height);
        [self.scrollView addSubview:componentView];
        [componentView updateUI];
        
        //keeping this code here to show that on this page the components are on the top of each other
        //offset+=componentView.frame.size.height;
    }
    [self validate];

}

//overwrite the parent method to manage the logic of hiding the ruler when the message screen is on
-(void)validate{
    [super validate];
    TireCountComponentView * tireCountComponentView = self.componentViewArray[0];
    TireCountMessageComponentView * tireCountMessageComponentView = self.componentViewArray[1];
    
    if (!tireCountMessageComponentView.onScreen) {
        tireCountMessageComponentView.alpha=0;
    }
    
    tireCountComponentView.showRuler = !tireCountMessageComponentView.onScreen;
    
}


//load previous page in the flow
-(IBAction)backButtonPressed:(id)sender{
    TireCountComponentView * tireCountComponentView = self.componentViewArray[0];
    [tireCountComponentView saveInputs];
    [super backButtonPressed:sender];
}

#pragma mark - Override methods

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

@end
