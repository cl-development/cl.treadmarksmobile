//
//  DockViewController.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSyncEngine.h"

@class SlideView;

@interface DockViewController : UIViewController <TMSyncEngineDelegate>

@property (nonatomic) BOOL dockViewHidden;
@property (strong) SlideView* slideView;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@end
