//
//  TireViewController.h
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "SimpleTableCell.h"
#import "PageViewController.h"

@interface TireViewController : PageViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate>



@property (nonatomic, strong) NSMutableArray *tableData;
@property (nonatomic, strong) NSArray *tireCode;
@property (nonatomic, strong) NSArray *tireName;

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (nonatomic, assign) int cellCount;
@property (nonatomic, strong) NSArray *tableCells;
@property (nonatomic, strong) UITableViewCell *selectedCell;
@property (nonatomic, assign) CGPoint startLocation;
@property (nonatomic, strong) NSIndexPath *indexPath5;
@property (nonatomic, strong) SimpleTableCell *cell;
@property (nonatomic, strong) UILabel *rulerLabel;

@property (nonatomic, assign) BOOL selectedTextField;
@property (nonatomic, assign) BOOL isAnimating;
@property (nonatomic, assign) int check;

@property (nonatomic, strong) NSMutableDictionary *dict;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property (strong, nonatomic) IBOutlet UIButton *backButton;


- (IBAction)buttonPressed:(id)sender;




@property (strong, nonatomic) IBOutlet UIButton *upButton;
@property (strong, nonatomic) IBOutlet UIButton *downButton;

- (IBAction)buttonClicked:(id)sender;





@end
