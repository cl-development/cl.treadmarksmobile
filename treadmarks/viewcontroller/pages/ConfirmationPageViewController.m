//
//  ConfirmationPageViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-12.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ConfirmationPageViewController.h"
#import "SignatureComponentView.h"
#import "ParticipantComponentView.h"
#import "TransactionInformationComponentView.h"
#import "SubmitComponentView.h"
#import "NotesComponentView.h"
#import "PhotoComponentView.h"
#import "Constants.h"
#import "TireCountComponentForConfirmationPageView.h"
#import "EligibilityComponentView.h"
#import "AlertViewController.h"
#import "DataEngine.h"
#import "TOCAuthorizationComponentView.h"
#import "TOCLocationComponentForConfirmationPage.h"
//#import "DockViewController.h"
#import "TOCScaleTicketComponentViewConfirmation.h"
#import "TOCPhotoComponentViewConfirmation.h"
#import "NewAlertView.h"
#import "ModalViewController.h"
#import "TOCNotesComponentView.h"
#import "TOCOutgoingComponentView.h"
#import "TreadMarks-Swift.h"


@interface ConfirmationPageViewController () <NewAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (strong,nonatomic) SubmitComponentView *submitComponentView;
@property (strong) SignatureComponentView* signatureComponent1;
@property (strong) SignatureComponentView* signatureComponent2;
@property (weak, nonatomic) IBOutlet UIImageView *transTypeImageView;



@end

#define SUBMIT_VIEW_HEIGHT 170

@implementation ConfirmationPageViewController

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];

	//////////////////////////////////
	//
	//	You cannot call this method on viewWillAppear or else it will reallocate/initialize
	//	all the components everytime the view loads.
	//
	//////////////////////////////////
	/*
	 // the comment status may have changed:
    self.componentViewArray = [NSMutableArray new];
	
    [self configurePageComponents];
	 */
	
	//self.dockViewController.dockViewHidden = YES;
	//self.dockViewController.slideView.hidden = YES;
}
- (void)viewDidLoad {

    [super viewDidLoad];

    // the comment status may have changed:
    self.componentViewArray = [NSMutableArray new];
	
	[self configurePageComponents];
    [self setTransTypeImageView];
}
-(Comment *) commentOfType:(NSUInteger) type {
    Comment *result = nil;
    NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
    if (comments != nil) {
        for (Comment *comment in comments) {
            if (type==COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
                    result = comment;
                    break;
                }
            }
            else if (type==COMMENT_TYPE_DEFAULT) {
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                    result = comment;
                    break;
                }
            }
        }
    }
    return result;
}

-(void) configurePageComponents {
    TransactionInformationComponentView * transactionView = [[TransactionInformationComponentView alloc] init];
    transactionView.parentPageViewController = self;
    [self.componentViewArray addObject:transactionView];
    
    if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = NO;
        participantComponentView.parentPageViewController = self;
        // UI for inactive trnsaction
        User        *user=self.transaction.outgoingUser;
        Registrant  *registrant=user.registrant;
        
        if ([registrant.isActive integerValue]==0 && registrant !=nil )
        {    //   participantComponentView.validateImageView.image= [UIImage imageNamed: @"cal-pop-flag-red.png"];
          //NEW SPEC
            //  self.validateImageView.image= [UIImage imageNamed: @"cal-pop-flag-red.png"];
        }

        [self.componentViewArray addObject:participantComponentView];
        
        
        EligibilityComponentView * eligibilityView = [[EligibilityComponentView alloc] init];
        eligibilityView.parentPageViewController = self;
        eligibilityView.delegate = self;
        [self.componentViewArray addObject:eligibilityView];
        
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
        
        // get the photos related to the transaction; always insure there is at least one photo component
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }

        
        Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        if (comment != nil && comment.text != nil && [comment.text isEqualToString:@""]==NO) {
            NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        
        [self.componentViewArray addObject:participantComponentView];

        TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
        authorizationView.parentPageViewController = self;
        [self.componentViewArray addObject:authorizationView];
        
        TOCLocationComponentForConfirmationPage *locationView = [[TOCLocationComponentForConfirmationPage alloc] init];
        locationView.parentPageViewController = self;
        [self.componentViewArray addObject:locationView];

       
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
       
        
        // get the photos related to the transaction; always insure there is at least one photo component
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }
        
        Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        if (comment != nil && comment.text != nil && [comment.text isEqualToString:@""]==NO) {
            NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }

    }
    
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PTR) {
        
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        if ([Utils getProcessorPtrOnly]==YES && [Utils getProcessorDidLogin]==YES)
        {
            
            participantComponentView.isIncomingUser = NO;
            participantComponentView.parentPageViewController = self;
            [self.componentViewArray addObject:participantComponentView];
            participantComponentView = [[ParticipantComponentView alloc] init];
            participantComponentView.isIncomingUser = YES;
            participantComponentView.parentPageViewController = self;
            [self.componentViewArray addObject:participantComponentView];
        }
        else
        {
       
            participantComponentView.isIncomingUser = NO;
            participantComponentView.parentPageViewController = self;
            [self.componentViewArray addObject:participantComponentView];
            participantComponentView = [[ParticipantComponentView alloc] init];
            participantComponentView.isIncomingUser = YES;
            participantComponentView.parentPageViewController = self;
            [self.componentViewArray addObject:participantComponentView];

        // UI for inactive trnsaction
        
        
        }
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
        
        /*NSArray *scaleTickets = [[DataEngine sharedInstance] scaleTicketsForTransaction:self.transaction];
        for (ScaleTicket *scaleTicket in scaleTickets) {
            ScaleTicketComponentView *scaleTicketComponentView = [[ScaleTicketComponentView alloc] initWithScaleTicket:scaleTicket];
            scaleTicketComponentView.parentPageViewController = self;
            scaleTicketComponentView.delegate = self;
            scaleTicketComponentView.transaction=self.transaction;
            [self.componentViewArray addObject:scaleTicketComponentView];
        }*/
        TOCScaleTicketComponentViewConfirmation *scaleTicketComponentView = [[TOCScaleTicketComponentViewConfirmation alloc] init];
        scaleTicketComponentView.parentPageViewController = self;
        scaleTicketComponentView.delegate = self;
        [self.componentViewArray addObject:scaleTicketComponentView];

        /*
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }
        */
        Comment *weightVarianceComment;
        Comment *NormalComment;
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
        {
            
            // = [self commentOfType:COMMENT_TYPE_WEIGHT_VARIANCE_REASON];
            //if (weightVarianceComment != nil && [weightVarianceComment.text isEqualToString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
            
            NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
            if (comments != nil)
            {
                for (Comment *comment in comments)
                {
                    if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES)
                    {
                        weightVarianceComment = comment;
                        break;
                        
                    }
                   

                }
            }
            
            
            NotesComponentView *tocWeightVarianceNoteView = [[NotesComponentView alloc] initWithComment:weightVarianceComment];
            tocWeightVarianceNoteView.parentPageViewController = self;
            tocWeightVarianceNoteView.NotesLabel.text = @"Weight Variance Comment";
            [self.componentViewArray addObject:tocWeightVarianceNoteView];
            
        }

        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }

       
     
        
        /*TOCPhotoComponentViewConfirmation *photoComponentView = [[TOCPhotoComponentViewConfirmation alloc] init];
        photoComponentView.parentPageViewController = self;
        photoComponentView.delegate = self;
        [self.componentViewArray addObject:photoComponentView]; */
        //Add Weight Variance Note
        
        //Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        //if ([NormalComment.text isEqualToString:@""]==NO) {
           
        
            
            NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
            if (comments != nil)
            {
                for (Comment *comment in comments)
                {
                
                    if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                        NormalComment = comment;
                        break;
                    }
                    
                }
            }
        
            if (NormalComment != nil && NormalComment.text != nil && [NormalComment.text isEqualToString:@""]==NO)
            {
                NotesComponentView *notesView = [[NotesComponentView alloc] initWithComment:NormalComment];
                notesView.parentPageViewController = self;
                notesView.delegate = self;
                [self.componentViewArray addObject:notesView];
            }
        }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_DOT) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = NO;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
//        TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
//        authorizationView.parentPageViewController = self;
//        [self.componentViewArray addObject:authorizationView];
        
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
        
        EligibilityComponentView * eligibilityView = [[EligibilityComponentView alloc] init];
        eligibilityView.parentPageViewController = self;
        eligibilityView.delegate = self;
        [self.componentViewArray addObject:eligibilityView];
        
        TOCScaleTicketComponentViewConfirmation *scaleTicketComponentView = [[TOCScaleTicketComponentViewConfirmation alloc] init];
        scaleTicketComponentView.parentPageViewController = self;
        scaleTicketComponentView.delegate = self;
        [self.componentViewArray addObject:scaleTicketComponentView];
        
        Comment *weightVarianceComment;
        Comment *NormalComment;
        if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
        {
            
            // = [self commentOfType:COMMENT_TYPE_WEIGHT_VARIANCE_REASON];
            //if (weightVarianceComment != nil && [weightVarianceComment.text isEqualToString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES) {
            
            NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
            if (comments != nil)
            {
                for (Comment *comment in comments)
                {
                    if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==YES)
                    {
                        weightVarianceComment = comment;
                        break;
                        
                    }
                    
                    
                }
            }
            
            
            NotesComponentView *tocWeightVarianceNoteView = [[NotesComponentView alloc] initWithComment:weightVarianceComment];
            tocWeightVarianceNoteView.parentPageViewController = self;
            tocWeightVarianceNoteView.NotesLabel.text = @"Weight Variance Comment";
            [self.componentViewArray addObject:tocWeightVarianceNoteView];
            
        }

        
        
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }

        /*
        Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        if (comment != nil && comment.text != nil && [comment.text isEqualToString:@""]==NO) {
            NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }
        */
        
        NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
        if (comments != nil)
        {
            for (Comment *comment in comments)
            {
                
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                    NormalComment = comment;
                    break;
                }
                
            }
        }
        
        if (NormalComment != nil && NormalComment.text != nil && [NormalComment.text isEqualToString:@""]==NO)
        {
            NotesComponentView *notesView = [[NotesComponentView alloc] initWithComment:NormalComment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }

        // get the photos related to the transaction; always insure there is at least one photo component
        
       
        
        
        
       
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_HIT) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = NO;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        /*
        TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
        authorizationView.parentPageViewController = self;
        [self.componentViewArray addObject:authorizationView];
         */
        
        
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
        
        // get the photos related to the transaction; always insure there is at least one photo component
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }

        
        Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        if (comment != nil && comment.text != nil && [comment.text isEqualToString:@""]==NO) {
            NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }
        
        
        
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_UCR) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = NO;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
//        TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
//        authorizationView.parentPageViewController = self;
//        [self.componentViewArray addObject:authorizationView];
        
        
        
        TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
        tireCountComponentForConfirmationPageView.parentPageViewController = self;
        tireCountComponentForConfirmationPageView.delegate = self;
        [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
        
        EligibilityComponentView * eligibilityView = [[EligibilityComponentView alloc] init];
        eligibilityView.parentPageViewController = self;
        eligibilityView.delegate = self;
        [self.componentViewArray addObject:eligibilityView];
        
        // get the photos related to the transaction; always insure there is at least one photo component
        NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
        for (Photo *photo in photos) {
            PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
            photoView.parentPageViewController = self;
            photoView.delegate = self;
            photoView.transaction=self.transaction;
            [self.componentViewArray addObject:photoView];
        }

        Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        if (comment != nil && comment.text != nil && [comment.text isEqualToString:@""]==NO) {
            NotesComponentView * notesView = [[NotesComponentView alloc] initWithComment:comment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }

        
       
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT) {
        ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = NO;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        participantComponentView = [[ParticipantComponentView alloc] init];
        participantComponentView.isIncomingUser = YES;
        participantComponentView.parentPageViewController = self;
        [self.componentViewArray addObject:participantComponentView];
        
        TOCMaterialComponentView *materialTypeView = [[TOCMaterialComponentView alloc] init];
        materialTypeView.parentPageViewController = self;
        [self.componentViewArray addObject:materialTypeView];
        
        TOCScaleTicketComponentViewConfirmation *scaleTicketComponentView = [[TOCScaleTicketComponentViewConfirmation alloc] init];
        scaleTicketComponentView.parentPageViewController = self;
        scaleTicketComponentView.delegate = self;
        [self.componentViewArray addObject:scaleTicketComponentView];
        
        
         NSArray *photos = [[DataEngine sharedInstance] photosForTransaction:self.transaction andPhotoType:PHOTO_TYPE_ID_PHOTO];
         for (Photo *photo in photos) {
         PhotoComponentView * photoView = [[PhotoComponentView alloc] initWithPhoto:photo];
         photoView.parentPageViewController = self;
         photoView.delegate = self;
         photoView.transaction=self.transaction;
         [self.componentViewArray addObject:photoView];
         }
        
        
        Comment *NormalComment;
        
      /*  TOCPhotoComponentViewConfirmation *photoComponentView = [[TOCPhotoComponentViewConfirmation alloc] init];
        photoComponentView.parentPageViewController = self;
        photoComponentView.delegate = self;
        [self.componentViewArray addObject:photoComponentView];*/
        //Add Weight Variance Note
        
        //Comment *comment = [[DataEngine sharedInstance] commentForTransaction:self.transaction];
        //if ([NormalComment.text isEqualToString:@""]==NO) {
        
        
        
        NSArray *comments = [[DataEngine sharedInstance] commentsForTransaction:self.transaction];
        if (comments != nil)
        {
            for (Comment *comment in comments)
            {
                
                if ([comment.text containsString:WEIGHT_VARIANCE_COMMENT_HEADER]==NO) {
                    NormalComment = comment;
                    break;
                }
                
            }
        }
        
        if (NormalComment != nil && NormalComment.text != nil && [NormalComment.text isEqualToString:@""]==NO)
        {
            NotesComponentView *notesView = [[NotesComponentView alloc] initWithComment:NormalComment];
            notesView.parentPageViewController = self;
            notesView.delegate = self;
            [self.componentViewArray addObject:notesView];
        }
        
        
    }

        //ScaleTicketComponentView*
        //self.scaleTickets = [[DataEngine sharedInstance] scaleTicketsForTransaction:self.transaction];
        //
    //}
    
//    TireCountComponentForConfirmationPageView *tireCountComponentForConfirmationPageView = [TireCountComponentForConfirmationPageView new];
//    tireCountComponentForConfirmationPageView.parentPageViewController = self;
//    tireCountComponentForConfirmationPageView.delegate = self;
//    [self.componentViewArray addObject:tireCountComponentForConfirmationPageView];
    
    if ([Utils getProcessorPtrOnly]==YES && [Utils getProcessorDidLogin]==YES)
    {   self.signatureComponent1 = [[SignatureComponentView alloc] initWithParticipant:OUTGOING_USER];
        self.signatureComponent1.parentPageViewController = self;
        self.signatureComponent1.delegate = self;
        self.signatureComponent1.transaction = self.transaction;
        [self.componentViewArray addObject:self.signatureComponent1];
        
        self.signatureComponent2 = [[SignatureComponentView alloc] initWithParticipant:INCOMING_USER];
        self.signatureComponent2.parentPageViewController = self;
        self.signatureComponent2.delegate = self;
        self.signatureComponent2.transaction = self.transaction;
        [self.componentViewArray addObject:self.signatureComponent2];

        }
    else{
        self.signatureComponent1 = [[SignatureComponentView alloc] initWithParticipant:INCOMING_USER];
        self.signatureComponent1.parentPageViewController = self;
        self.signatureComponent1.delegate = self;
        self.signatureComponent1.transaction = self.transaction;
        [self.componentViewArray addObject:self.signatureComponent1];
        
        self.signatureComponent2 = [[SignatureComponentView alloc] initWithParticipant:OUTGOING_USER];
        self.signatureComponent2.parentPageViewController = self;
        self.signatureComponent2.delegate = self;
        self.signatureComponent2.transaction = self.transaction;
        [self.componentViewArray addObject:self.signatureComponent2];

    
    }
    self.submitComponentView = [SubmitComponentView new];
    self.submitComponentView.parentPageViewController = self;
    [self.componentViewArray addObject:self.submitComponentView];
    
    [self updateUI];
}

- (void)updateUI {
    
    CGFloat offset=0;
    for (ComponentView *componentView in self.componentViewArray) {
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  componentView.frame.size.height);
        
        //a better solution needs to be found
        if ([componentView isKindOfClass: [TOCScaleTicketComponentViewConfirmation class]]==YES) {
            TOCScaleTicketComponentViewConfirmation *confirm = (TOCScaleTicketComponentViewConfirmation *)componentView;
            confirm.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [confirm heightOn]);
        }
        else if ([componentView isKindOfClass: [TOCPhotoComponentViewConfirmation class]]==YES) {
            TOCPhotoComponentViewConfirmation *confirm = (TOCPhotoComponentViewConfirmation *)componentView;
            confirm.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [confirm heightOn]);
             //NSLog(@"The height of photo frame is: %f", [confirm heightOn]);
        }
        else if ([componentView isKindOfClass: [NotesComponentView class]]==YES) {
            NotesComponentView *confirm = (NotesComponentView *)componentView;
            confirm.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [confirm heightOn]);
            //NSLog(@"The height of the frame is: %f", confirm.view.frame.size.height);
        }
        else if ([componentView isKindOfClass: [TOCComponentView class]]==YES) {
            TOCComponentView *toc = (TOCComponentView *)componentView;
            toc.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [toc height]);
        } 
        
        [self.scrollView addSubview:componentView];
        componentView.readOnly=YES;
        [componentView updateUI];
        offset+=componentView.frame.size.height;
        
        // temporary solution till we refactor classes:
        // toc-components have their own lower bar, so don't draw it.|| ([componentView isKindOfClass: [TOCScaleTicketComponentViewConfirmation class]]==YES))
        /*
        if ([componentView isKindOfClass: [TOCPhotoComponentViewConfirmation class]]==YES)  {
            //gray horizontal bar
            UIImageView *brImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trans-panel-br.png"]];
            brImageView.frame = CGRectMake(0, offset, COMPONENT_WIDTH, BR_IMAGE_HEIGHT);
            [self.scrollView addSubview:brImageView];
            offset += BR_IMAGE_HEIGHT;
            //NSLog(@"The component is: %@", componentView.description);
        }
         */
        if (([componentView isKindOfClass: [TOCComponentView class]]==NO) && ([componentView isKindOfClass: [TOCLocationComponentForConfirmationPage class]]==NO)) {
            //gray horizontal bar
            UIImageView *brImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trans-panel-br.png"]];
            brImageView.frame = CGRectMake(0, offset, COMPONENT_WIDTH, BR_IMAGE_HEIGHT);
            [self.scrollView addSubview:brImageView];
            offset += BR_IMAGE_HEIGHT;
        }
    }
    
    [self.scrollView addSubview:self.submitView];
    self.submitView.frame = CGRectMake(0, offset, self.submitView.frame.size.width, self.submitView.frame.size.height);
    offset+=self.submitView.frame.size.height;
    
    self.scrollView.contentSize = CGSizeMake(COMPONENT_WIDTH, offset);
    
    [self validate];
}

// TBD: Utility method?
-(void) setTransTypeImageView {
    if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-tcr.png"];
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-stc.png"];
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PTR) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-ptr.png"];
    }
}


#pragma mark - IBActions

- (IBAction)backButtonPressed:(id)sender {
    
	if(self.transaction.transactionStatusType.transactionStatusTypeId != TRANSACTION_STATUS_TYPE_ID_COMPLETE &&
	   (self.signatureComponent1.valid || self.signatureComponent2.valid))
	{
		[self presentAlert:self info:NULL];
	}
	else
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)validate{
    [super validate];
    self.submitComponentView.enabled = self.valid;
}

#pragma mark - AlertViewDelegate

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {

	NewAlertView *alert = [[NewAlertView alloc] init];
//	alert.frame = CGRectMake(floor((self.view.frame.size.width - alert.frame.size.width) / 2),
//							 floor((self.view.frame.size.height - alert.frame.size.height) / 2),
//							 alert.frame.size.width,
//							 alert.frame.size.height);
	alert.title = @"Warning";
	alert.heading = @"Are you sure you want to go back?";
	alert.text = @"Navigating back will clear the signature(s) entered and must be signed again in order to complete this transaction.";
	alert.primaryButtonText = @"Confirm";
	alert.secondaryButtonText = @"Cancel";
	alert.delegate = self;
    [alert setUI];

	[[ModalViewController sharedModalViewController] showView:alert];
}

#pragma mark - NewAlertViewDelegate

- (void)primaryAction:(UIButton *)sender {
	
	if(self.signatureComponent1.valid)
	{
		[self.signatureComponent1 deleteSignature];
	}
	
	if(self.signatureComponent2.valid)
	{
		[self.signatureComponent2 deleteSignature];
	}

	[self.navigationController popViewControllerAnimated:YES];

	[[ModalViewController sharedModalViewController] hideView];
}
- (void)secondaryAction:(UIButton *)sender {
	
	[[ModalViewController sharedModalViewController] hideView];
}

@end
