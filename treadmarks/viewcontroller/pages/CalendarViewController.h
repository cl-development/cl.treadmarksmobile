//
//  CalendarViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-20.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListViewController.h"
#import "Transaction.h"
#import "CalendarPopOverViewController.h"
#import "PageViewController.h"

@interface CalendarViewController : PageViewController <CalendarPopoverViewControllerDelegate>

@property (nonatomic)        NSInteger deltaMonths;
@property (nonatomic,weak) ListViewController *listViewController;
@property (nonatomic,strong) NSArray *transactionArray;


@end
