//
//  ListViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 11/2/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideViewDelegate.h"
#import "AlertViewController.h"
#import "PageViewController.h"
#import "CMPopTipView.h"
//extern NSInteger countforLastMonth;

@class AlertViewController;

@interface ListViewController : PageViewController <UITableViewDelegate, UITableViewDataSource, SlideViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,CMPopTipViewDelegate>


//delta between current month and the month to be displayed
@property (nonatomic) NSInteger deltaMonths;
@property int searchStatus;
@property int isTransactionNewOrVoid;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(NSString *)getClaimMonth:(int) monthValue;
-(NSNumber *)getClaimCurrentYear;
-(NSString *)getMonthFromDate:(NSDate *)checkDate;
-(NSInteger*)countUnsyncedTransactions;

@end
