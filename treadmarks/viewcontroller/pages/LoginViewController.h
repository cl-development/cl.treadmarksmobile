//
//  LoginViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 10/30/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "QRScanDelegate.h"

@interface LoginViewController : UIViewController <CLLocationManagerDelegate,UIGestureRecognizerDelegate>

@property BOOL shouldEnableCameraOnAppear;
@property (weak) id<QRScanDelegate> delegate;
@property bool hideBackButton;
@property BOOL slideToLogin;
@property (nonatomic, assign) BOOL isSlideToLoginInvisible;
@property (strong, nonatomic) IBOutlet UIImageView *slideToLoginImageView;
@property (strong, nonatomic) IBOutlet UIView *slideToLoginView;
@property (strong, nonatomic) IBOutlet UIImageView *loginImageView;

- (void)reset;
- (void) enableCamera;
-(void)disableCamera;
@end

