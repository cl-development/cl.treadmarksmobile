//
//  ListViewController.m
//  TreadMarks
//
//  Created by Capris on 11/2/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ListViewController.h"
#import "TransactionWrapper.h"
#import "TransactionTableViewCell.h"
//#import "SlideView.h"
#import "Utils.h"
//#import "DockViewController.h"
#import "SearchViewController.h"
#import "DataEngine.h"
#import "Constants.h"
#import "ParticipantsPageViewController.h"
#import "CalendarViewController.h"
#import "ConfirmationPageViewController.h"
#import "TOCPageViewController.h"
#import "AlertViewController.h"
#import "ModalViewController.h"
#import "NewAlertView.h"
#import "AMWaveTransition.h"
#import "DockBarViewController.h"
#import "CMPopTipView.h"
#import "Reachability.h"
#import "MPGNotification.h"

#define DELETE_ALERT_TAG 1

//NSInteger countforLastMonth;

@interface ListViewController () <TransactionDelegate, AlertViewDelegate, NewAlertViewDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet AMWaveTransition *interactive;


@property (weak, nonatomic) TOCPageViewController *TOCView;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionFindCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionFindLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionTotalLabel;

@property (nonatomic,strong) NSArray *transactionArray;
@property (nonatomic,strong) NSArray *syncTransactionArray;
@property (nonatomic,strong) NSMutableArray *filteredTransactionArray;
@property (nonatomic,strong) NSMutableArray *nonPTRTransactionArray;
@property (nonatomic,strong) NSMutableArray *searchTransactionArray;
@property (nonatomic,strong) NSString *alertMessage;
@property (nonatomic, strong) NSString *tranClaimOneText;
@property (nonatomic, strong) NSString *tranClaimTwoText;

@property (nonatomic, strong)NSMutableArray *arrayPtr;
@property (nonatomic, strong)NSMutableArray *arrayStc;
@property (nonatomic, strong)NSMutableArray *arrayTcr;
@property (nonatomic, strong)NSMutableArray *arrayTransaction;
@property (weak, nonatomic) IBOutlet UISearchBar *listviewSearch;
- (IBAction)searchButton:(id)sender;
@property NSInteger searchTextLenght;

@property (nonatomic,strong) NSIndexPath *deletedRowIndexPath;

@property (weak, nonatomic) IBOutlet UIButton *searchButtonOutlet;

-(IBAction)buttonTransactionsTotalAction:(id)sender;

#pragma mark - Properties

@property BOOL isUnsyncedTransactionsChecked;

@end

@implementation ListViewController
{Reachability *reachability;
MPGNotification *notification;
}

#pragma mark - UIViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    self.deltaMonths = 0;
    self.searchTextLenght=0;
    [self customSearchbar];
    self.searchStatus=OFF_SCREEN;
    self.listviewSearch.autocapitalizationType= UITextAutocapitalizationTypeNone;
    [self.listviewSearch   setShowsCancelButton:YES animated:YES];
    
    //For more than 20  search records
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.isUnsyncedTransactionsChecked = NO;
    
    //[self.tableView setBackgroundColor:[UIColor clearColor]];
    // _interactive = [[AMWaveTransition alloc] init];
    
    DockBarViewController *dockBarView = [[DockBarViewController alloc] initWithNibName:@"DockBarViewController" bundle:nil];
    dockBarView.view.frame = CGRectMake(0,938,768,89);
    dockBarView.createNewTransaction=YES;
    [self.view addSubview:dockBarView.view];
    [self addChildViewController:dockBarView];
    [dockBarView didMoveToParentViewController:self];
    
    
    




[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkStatusChanged:) name:kReachabilityChangedNotification object:nil];

reachability = [Reachability reachabilityWithHostName:@"www.google.com"];

[reachability startNotifier];

//Format the Date for Last sync 

   // NSLog(@"LIST VIEW LOADED");

}
- (BOOL)prefersStatusBarHidden {
    
    return NO;
}
- (void)networkStatusChanged:(NSNotification*)note
{
    Reachability* reach = [note object];
    NetworkStatus networkStatus = reach.currentReachabilityStatus;
   
    if (networkStatus == NotReachable)
    {
       /* // Add MS USER DEFAULT
               //NSLog(@"NETWORK PROBLEM. NO SYNC POSSIBLE");
        notification = [MPGNotification notificationWithTitle:@"No Wi-Fi !" subtitle:@"WiFi is not connected. Syncronization with server is not possible. \rCheck Wi-Fi settings" backgroundColor:[UIColor colorWithRed:(226./255)
                                                               green:(60./255)
                                                               blue:(51./255)
                                                               alpha:1]
                                                               iconImage:[UIImage imageNamed:@"wifi-off"] ];
        
        notification.duration = 4.0;
        notification.swipeToDismissEnabled = NO;
        notification.fullWidthMessages=YES;
        notification.animationType=MPGNotificationAnimationTypeDrop;
        [notification setTitleColor:[UIColor whiteColor]];
        [notification setSubtitleColor:[UIColor whiteColor]];
        notification.backgroundTapsEnabled = YES;
       // [notification setButtonConfiguration:MPGNotificationButtonConfigrationOneButton withButtonTitles:@[@"Settings"]];
        [notification show];
        }*/
        
       // NSLog(@"NETWORK NOT FOUND ");
       }
    if (networkStatus==ReachableViaWiFi||networkStatus==ReachableViaWWAN)
    {
             //   NSLog(@"NETWORK FOUND. TRY SYNC");
    }
    

}



- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation != UINavigationControllerOperationNone) {
        return [AMWaveTransition transitionWithOperation:operation andTransitionType:AMWaveTransitionTypeNervous];
    }
    return nil;
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    //NSLog(@"viewWilldisappear");
    self.tableView.editing = NO;
}



- (void)dealloc
{
    [self.navigationController setDelegate:nil];
    
}

-(void)keyboardWillShow:(NSNotification *)theNotification

{
    
    CGSize keyboardSize = [[[theNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets;
    
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
        
    } else {
        
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
        
    }
    
    
    
    self.tableView.contentInset = contentInsets;
    
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    //[self.tableView scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

//more than 20 search result

- (void)keyboardWillHide:(NSNotification *)notification

{
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
}




-(void)customSearchbar
{
    UITextField *txfSearchField = [self.listviewSearch valueForKey:@"_searchField"];
    [txfSearchField setBackgroundColor:[UIColor whiteColor]];
    [txfSearchField setBorderStyle:UITextBorderStyleRoundedRect ];
    CGRect frameRect = txfSearchField.frame;
    frameRect.size.height = 88;
    txfSearchField.frame=frameRect;
    [txfSearchField setFont:[UIFont fontWithName:@"OpenSans-Light" size:26]];
    txfSearchField.layer.borderColor = [UIColor clearColor].CGColor;
    [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"list-search-img-2.png"]forState:UIControlStateNormal];
    [[UISearchBar appearance]setImage:[UIImage imageNamed:@"list-search-img-3.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
}
- (void)viewWillAppear:(BOOL)animated {
   // NSLog(@"LIST VIEW APPEAR");
    [super viewWillAppear:animated];
    //[self.navigationController setDelegate:self];
    
    if (self.listviewSearch.hidden==NO && self.searchStatus==OFF_SCREEN) {
        [self.listviewSearch resignFirstResponder];
    }
    
/*    NSLog(@"what is the search status: %d", self.searchStatus);
     NSLog(@"is the searchview hidden? %d", self.listviewSearch.hidden);*/
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidComplete:) name:@"Sync Complete"  object:nil];
    [self updateUI];
    
}
-(void )viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self searchUiUpdate:self.searchStatus];
    [self searchCountUiUpdate:self.searchStatus];
    self.searchButtonOutlet.selected=self.searchStatus;
    
    if (self.isUnsyncedTransactionsChecked == NO) {
        
        [self checkUnsyncedTransactions];
    }
    
    
    
}
#pragma mark - ListViewController //transactionDate

- (void)updateUI {
    
    // see  both transactions
    self.transactionArray = [[DataEngine sharedInstance] transactionsForRegistrant:[Utils getRegistrantId] AndUser:[Utils getUserId] NotDeletedSort:@"createdDate" ascending:NO filterSync:NO];
   // NSLog(@"TRANSACTION COUNT   %lu",(unsigned long)[self.transactionArray count]);
    //filter transactions
    NSDate *today = [NSDate date];
    //add the months delta
    NSDateComponents* dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    self.filteredTransactionArray = [NSMutableArray new];
    //Dont show PTR for Haulers
    
    
    //NSString *Dec = [self getClaimMonth:3];
    //NSString *Jan = [self getClaimMonth:2];
    NSString *currentMonth = [self getClaimMonth:1];
    NSNumber *currentYear = [self getClaimCurrentYear];
    
    if([currentMonth isEqualToString:@"Jan"] || [currentMonth isEqualToString:@"Feb"])
    {
        if ([currentYear intValue] == 2015 )
        {
            [Utils setShowLastThreeMonths:YES];
        }
    }
    //transactionDate
    if([Utils getProcessorDidLogin])
    {
        for (Transaction *transaction in self.transactionArray)
        {
            if ([Utils isDate:newDate sameMonthWith:transaction.createdDate] && transaction.transactionStatusType.transactionStatusTypeId !=TRANSACTION_STATUS_TYPE_ID_DELETED)
            {
                [self.filteredTransactionArray addObject:transaction];
            }
        }
    }
    else
    {
        if([Utils getShowLastThreeMonths])// && [Utils getProcessorDidLogin]==NO)
        {
            for (Transaction *transaction in self.transactionArray)
            {
                if ([Utils isDate:newDate sameMonthWith:transaction.createdDate] && transaction.transactionStatusType.transactionStatusTypeId !=TRANSACTION_STATUS_TYPE_ID_DELETED)
                {
                    [self.filteredTransactionArray addObject:transaction];
                }
            }
        }
        
        else
        {
            for (Transaction *transaction in self.transactionArray)
            {
                if ([Utils isDate:newDate sameMonthWith:transaction.createdDate]   && transaction.transactionStatusType.transactionStatusTypeId !=TRANSACTION_STATUS_TYPE_ID_DELETED && transaction.transactionType.transactionTypeId!=TRANSACTION_TYPE_ID_PTR)
                {
                    [self.filteredTransactionArray addObject:transaction];
                }
            }
        }
    }
    //show  all the transaction for  last 3 month from feb & Processor
    
    
    
    if(self.searchStatus==OFF_SCREEN){
        
        [self searchUiUpdate:OFF_SCREEN];
    }
    
    if(self.searchStatus==ON_SCREEN){
        
        self.searchTransactionArray = [NSMutableArray new];
        [self searchBar:self.listviewSearch textDidChange:self.listviewSearch.text];
        
        self.searchButtonOutlet.selected=YES;
        [self searchUiUpdate:ON_SCREEN];
        //[self.tableView reloadData];
    }
    [self.tableView reloadData];
    
    //update the UI
    [self updateDisplayedMonth];
    self.transactionCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.filteredTransactionArray.count];
}


- (void)syncDidComplete:(NSNotification *) theNotification {
    if ([[theNotification name] isEqualToString:@"Sync Complete"]) {
        [self updateUI];
    }
}

- (void)setDeltaMonths:(NSInteger)deltaMonths{
    _deltaMonths=deltaMonths;
    [self updateUI];
}
- (IBAction)prevMonthPressed:(id)sender {
    self.deltaMonths--;
    self.searchStatus=OFF_SCREEN;
    
    [self searchUiUpdate:OFF_SCREEN];
    [self searchCountUiUpdate:OFF_SCREEN];
    self.searchButtonOutlet.selected=NO;
    
    [self updateUI];
}
- (IBAction)nextMonthPressed:(id)sender {
    self.deltaMonths++;
    self.searchStatus=OFF_SCREEN;
    self.searchButtonOutlet.selected=NO;
    
    [self searchUiUpdate:OFF_SCREEN];
    [self searchCountUiUpdate:OFF_SCREEN];
    [self updateUI];
}

- (NSString *)getClaimCurrentMonth {
    NSDate *today = [NSDate date];
    
    //add the months delta
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    unsigned int components = NSCalendarUnitMonth | NSCalendarUnitDay;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger month   = [dateComponents month];
    NSInteger day     = [dateComponents day];
    
    //populate the month and year
    NSDateFormatter *df = [NSDateFormatter new];
    
    NSString *monthName;
    
    if (day>3) {
        monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    } else {
        monthName = [[df monthSymbols] objectAtIndex:(month-2)];
    }
    
    monthName = [monthName substringToIndex:3];
    
    return monthName;
}

- (NSNumber *) getClaimCurrentYear {
    NSDate *today = [NSDate date];
    
    //add the months delta
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    unsigned int components = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger year    = [dateComponents year];
    
    NSNumber *yearValue = [NSNumber numberWithLong:year];
    
    return yearValue;
}

- (NSString *)getClaimMonth:(int) monthValue {
    NSDate *today = [NSDate date];
    int numMonths = 12;
    NSString *monthName;
    
    //add the months delta
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    unsigned int components = NSCalendarUnitMonth | NSCalendarUnitDay;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger month   = [dateComponents month];
    
    //populate the month and year
    NSDateFormatter *df = [NSDateFormatter new];
    
    if ((month - monthValue)<0) {
        int monthNum = 0;
        monthNum = (int)month - monthValue + numMonths;
        monthName = [[df monthSymbols] objectAtIndex:(monthNum)];
    }  else {
        monthName = [[df monthSymbols] objectAtIndex:(month-monthValue)];
    }
    
    if ([monthName isEqualToString:@"September"]) {
        monthName = [monthName substringToIndex:4];
    } else {
        monthName = [monthName substringToIndex:3];
    }
    
    return monthName;
}

- (NSString *)getMonthFromDate:(NSDate *)checkDate {
    //add the months delta
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:checkDate options:0];
    
    unsigned int components = NSCalendarUnitMonth | NSCalendarUnitDay;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger month   = [dateComponents month];
    
    //populate the month and year
    NSDateFormatter *df = [NSDateFormatter new];
    
    NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    
    if ([monthName isEqualToString:@"September"]) {
        monthName = [monthName substringToIndex:4];
    } else {
        monthName = [monthName substringToIndex:3];
    }
    
    return monthName;
}

- (NSNumber *) getYearFromDate:(NSDate *)checkDate {
    //add the months delta
    NSDateComponents *dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:checkDate options:0];
    
    unsigned int components = NSCalendarUnitYear;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger year    = [dateComponents year];
    
    NSNumber *yearFromDate = [NSNumber numberWithInteger:year];
    
    return yearFromDate;
}


- (void)updateDisplayedMonth{
    NSDate *today = [NSDate date];
    
    //add the months delta
    NSDateComponents* dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    unsigned int components = NSCalendarUnitMonth | NSCalendarUnitYear;
    dateComponents    = [calendar components:components fromDate:newDate];
    NSInteger month   = [dateComponents month];
    NSInteger year    = [dateComponents year];
    
    //populate the month and year
    NSDateFormatter *df = [NSDateFormatter new];
    NSString *monthName = [[df monthSymbols] objectAtIndex:(month-1)];
    
    NSString *newMonthLabel = [NSString stringWithFormat:@"%@ %ld", monthName, (long)year];
    if (![newMonthLabel isEqualToString:self.monthLabel.text]) {
        //animate the months
        [UIView animateWithDuration:0.1
                         animations:^{
                             self.monthLabel.alpha=0;
                         }
                         completion:^(BOOL animation) {
                             [UIView animateWithDuration:.2
                                              animations:^{
                                                  self.monthLabel.alpha=1;
                                                  self.monthLabel.text = newMonthLabel;
                                              }];
                         }];
    }
}
- (IBAction)showCalendarPressed:(id)sender {
    [self searchUiUpdate:OFF_SCREEN];
    self.searchStatus=OFF_SCREEN;
    CalendarViewController *viewController = [[CalendarViewController alloc] init];
    viewController.listViewController = self;
    viewController.deltaMonths = self.deltaMonths;
    //viewController.transactionArray = self.transactionArray;
    
    [self.navigationController pushViewController:viewController animated:NO];
}
- (IBAction)backToLoginPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark SEARCH BAR UI & DELEGATES


- (void) searchDisplayControllerDidEndSearch:(UISearchController *)controller
{
    controller.searchBar.showsCancelButton = YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [self searchCountUiUpdate :ON_SCREEN];
    //lowercase the text for case insenstive search
    searchText=[searchText lowercaseString ];
    
    //if empty string show all the records and exit
    
    if(searchText.length == 0 )
    {
        // self.transactionFindCountLabel.text=@"";
        self.searchStatus=OFF_SCREEN;
        [self searchCountUiUpdate :OFF_SCREEN];
        [self.tableView reloadData];
        return;
    }
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.searchStatus=ON_SCREEN;
    self.searchTransactionArray = [NSMutableArray new];
    NSDate *today = [NSDate date];
    NSDateComponents* dateComponents = [NSDateComponents new];
    [dateComponents setMonth:self.deltaMonths];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:today options:0];
    
    //Use all transaction array instead of FilterArray for future power search
    // User *userInfo  =[Utils getProcessorDidLogin]? self.transaction.incomingUser:self.transaction.outgoingUser;
    
    
    for (Transaction *transaction in self.filteredTransactionArray)//  .transactionArray)
    {
        if ([Utils isDate:newDate sameMonthWith:transaction.createdDate])
        {
            
            //  User *userInfo;
            
            User *userInfo  =[Utils getProcessorDidLogin]? transaction.incomingUser:transaction.outgoingUser;
         //   User *userInfo  =[Utils getProcessorDidLogin]? transaction.outgoingUser:transaction.incomingUser;//created by w
            
            //User        *userInfo=transaction.outgoingUser;
            
            Registrant  *registrantInfo=  userInfo.registrant;
            
            NSRange rangeTrans = [[transaction.friendlyId stringValue] rangeOfString:searchText];
            
            NSRange rangeTransType= [[transaction.transactionType.shortNameKey lowercaseString ]rangeOfString:searchText];
            
            NSRange rangedevicename = [[transaction.deviceName lowercaseString] rangeOfString:searchText];
//            NSLog([NSString stringWithFormat:@"snknsknknsknknksnkdevicename: %@", [transaction.deviceName lowercaseString]]);
            
            //Check for Nill Value of range for Processor
            NSString    *regNumber =[[registrantInfo.registrationNumber stringValue]lowercaseString ]==nil?@"":[[registrantInfo.registrationNumber stringValue]lowercaseString ] ;
            
            NSString    *regBizName=[registrantInfo.businessName lowercaseString]==nil?@"":[registrantInfo.businessName lowercaseString];
            
//            NSLog([NSString stringWithFormat:@"snknsknknsknknksnkID: %@, lkjlsninfiName: %@", regNumber,regBizName]);
            NSRange rangeRegistrantID= [regNumber rangeOfString:searchText];
            
            NSRange rangeRegistrantName= [ regBizName rangeOfString:searchText];
            
            if (rangeTrans.location != NSNotFound || rangeTransType.location  != NSNotFound
                ||rangeRegistrantID.location!= NSNotFound||rangeRegistrantName.location!=NSNotFound||rangedevicename.location!=NSNotFound)
            {
                NSNumber* friendlyIDFound=transaction.friendlyId;
                Transaction* srTrans=[[DataEngine sharedInstance] singleTransactionForFriendlyId:friendlyIDFound:searchText];
                [self.searchTransactionArray addObject:srTrans];
            }
        }
    }
    
    self.transactionFindCountLabel.text= [NSString stringWithFormat:@"%lu",(unsigned long) self.searchTransactionArray.count];
    [self.tableView reloadData];
    self.searchTextLenght=searchText.length;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    dispatch_async(dispatch_get_main_queue(), ^{
        __block __weak void (^weakEnsureCancelButtonRemainsEnabled)(UIView *);
        void (^ensureCancelButtonRemainsEnabled)(UIView *);
        weakEnsureCancelButtonRemainsEnabled = ensureCancelButtonRemainsEnabled = ^(UIView *view) {
            for (UIView *subview in view.subviews) {
                if ([subview isKindOfClass:[UIControl class]]) {
                    [(UIControl *)subview setEnabled:YES];
                }
                weakEnsureCancelButtonRemainsEnabled(subview);
            }
        };
        
        ensureCancelButtonRemainsEnabled(searchBar);
    });
}

-(void) searchBarTextDidBeginEditing:(nonnull UISearchBar *)searchBar
{
    
    UITextInputAssistantItem* item = [searchBar inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
    
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    
    self.searchButtonOutlet.selected=NO;
    [self searchUiUpdate:OFF_SCREEN];
    [self searchCountUiUpdate :OFF_SCREEN];
    self.searchStatus=OFF_SCREEN;
    [self.tableView reloadData];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    
    [self searchBar:self.listviewSearch textDidChange:searchBar.text];
    
}

-(void) searchCountUiUpdate :(int)status
{
    
    if(status==OFF_SCREEN)
    {
        self.transactionTotalLabel.hidden=NO;
        self.transactionCountLabel.hidden=NO;
        self.transactionFindCountLabel.hidden=YES;
        self.transactionFindLabel.hidden=YES;
        
    }
    if(status==ON_SCREEN)
    {
        self.transactionTotalLabel.hidden=YES;
        self.transactionCountLabel.hidden=YES;
        self.transactionFindCountLabel.hidden=NO;
        self.transactionFindLabel.hidden=NO;
        
    }
    
}

-(void) searchUiUpdate :(int)status
{
    
    if(status==OFF_SCREEN)
    {
        [self.listviewSearch resignFirstResponder];
        
        [self.searchButtonOutlet setImage:[UIImage imageNamed:@"list-icon-filter-off.png"] forState:UIControlStateNormal];
        self.listviewSearch.hidden=YES;
        [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^() {
            self.tableView.frame=CGRectMake(0, 90, 768,844);
            
        }];
    }
    if(status==ON_SCREEN)
    {
        [self.listviewSearch becomeFirstResponder];
        [self.searchButtonOutlet setImage:[UIImage imageNamed:@"list-icon-filter-on.png"] forState:UIControlStateNormal];
        self.listviewSearch.hidden=NO;
        [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^() {
            self.tableView.frame=CGRectMake(0, 140, 768,794);
            
            
        }];
    }
    
}

- (BOOL) isMonthEnd {
    
    NSDate *now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitDay fromDate:now];
    
    NSRange range = [cal rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:now];
    
    return (range.length - components.day) < 3;
}

- (BOOL) isMonthBeginning {
    
    NSDate *now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:NSCalendarUnitDay fromDate:now];
    
    return components.day < 4;
    
}

- (IBAction)searchButton:(id)sender {
    
    self.searchButtonOutlet.selected=! self.searchButtonOutlet.selected;
    self.searchTextLenght=0;
    
    if ( self.searchButtonOutlet.selected)
    {
        //NSLog(@"Search ON");
        [self searchUiUpdate:ON_SCREEN];
        
    }
    else{
        // //NSLog(@"Search OFF");
        [self searchUiUpdate:OFF_SCREEN];
        self.searchStatus=OFF_SCREEN;
        [self searchCountUiUpdate :OFF_SCREEN];
        [self.tableView reloadData];
        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.searchStatus==ON_SCREEN?[self.searchTransactionArray count]:[self.filteredTransactionArray count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //TransactionTableViewCell *cell;
    static NSString *CellIdentifier = @"TransactionCell";
    //static NSString *FoundTrans=@"FoundCell";
    Transaction *transaction;
    if (self.searchStatus==ON_SCREEN)
        
    {transaction=[self.searchTransactionArray objectAtIndex:indexPath.row];}
    else
    {transaction=[self.filteredTransactionArray objectAtIndex:indexPath.row];}
    //Do not add the deleted trans to filter & Search array
    // if (transaction.transactionStatusType.transactionStatusTypeId!=TRANSACTION_STATUS_TYPE_ID_DELETED)
    //{ //NSLog(@"Del trans %@",transaction.friendlyId);
    //}
    
    TransactionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[TransactionTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"TransactionCell"];
    }
    cell.transaction = transaction;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LIST_TABLE_CELL_HEIGHT;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //add code here for when you hit delete
        Transaction *transaction;
        
        if(self.searchStatus==ON_SCREEN)
        {transaction = [self.searchTransactionArray objectAtIndex:indexPath.row];}
        else
        {transaction = [self.filteredTransactionArray objectAtIndex:indexPath.row];}
        
        if (transaction.transactionStatusType.transactionStatusTypeId == TRANSACTION_STATUS_TYPE_ID_COMPLETE)
        {
            
            [Utils showMessage:self header:@"Warning" message:@"Completed transaction cannot be voided" hideDelay:2];
            
            [self.tableView setEditing:NO];
            
            return ;
        }
        if ([Utils getProcessorDidLogin]==YES)
        {
            if (transaction.incomingSignatureName)
            {[Utils showMessage:self header:@"Warning" message:@"Transaction with incoming signature cannot be voided" hideDelay:2];
                return ;}
        }
        else{
            if (transaction.outgoingSignatureName)
            {
                [Utils showMessage:self header:@"Warning" message:@"Transaction with outgoing signature cannot be voided" hideDelay:2];
                return ;
            }
        }
        
        self.deletedRowIndexPath = indexPath;
/*        if ([self getMonthFromDate:transaction.createdDate] == [self getClaimMonth:2]){
            countforLastMonth--;
        }
 */       
        TransactionWrapper* wrapper = [[TransactionWrapper alloc] initWithTransaction:transaction];
        [wrapper voidTransaction:self info:NULL];
    }
    //[self.tableView reloadData];
}

#pragma mark - Table view delegate
//here
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Transaction *transaction;
    if (self.searchStatus==ON_SCREEN)
    {
        transaction=[self.searchTransactionArray objectAtIndex:indexPath.row];
    }
    else
    {
        transaction=[self.filteredTransactionArray objectAtIndex:indexPath.row];
    }
    
    [Utils setTransactionId:transaction.transactionId];
    
    if (transaction.transactionStatusType.transactionStatusTypeId==TRANSACTION_STATUS_TYPE_ID_COMPLETE){
        ConfirmationPageViewController *viewController = [ConfirmationPageViewController new];
        [self.navigationController pushViewController:viewController animated:YES];
    }  else if (transaction.transactionStatusType.transactionStatusTypeId!=TRANSACTION_STATUS_TYPE_ID_COMPLETE && ([transaction.incomingSignatureName length]!=0 || [transaction.outgoingSignatureName length]!=0)) {
        ConfirmationPageViewController *confViewController = [ConfirmationPageViewController new];
        TOCPageViewController *tocViewController = [TOCPageViewController new];
        tocViewController.rootViewController = self;
        //        [self.navigationController pushViewController:tocViewController animated:NO];
        //        [self.navigationController pushViewController:confViewController animated:YES];
        
        //We're modifying the stack, and committing the changes in one motion so that the movement to the last view controller is seamless.
        NSMutableArray *vcStack = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [vcStack addObject:tocViewController];
        [vcStack addObject:confViewController];
        [self.navigationController setViewControllers:vcStack animated:YES];
    }  else {
        //ParticipantsPageViewController *viewController = [ParticipantsPageViewController new];
        
        TOCPageViewController *viewController = [TOCPageViewController new];
        viewController.rootViewController = self;
        //here dismiss the firstresponder.
        if (self.listviewSearch.hidden==NO && self.searchStatus==OFF_SCREEN) {
            //[self searchBarCancelButtonClicked:self.listviewSearch];
            [self.listviewSearch resignFirstResponder];
            self.listviewSearch.hidden = YES;
            NSLog(@"catch");
        }
        // NSLog(@"search status value is: %d", self.searchStatus);
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    [self.tableView reloadData];
}

- (NSString*)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return TABLE_TITLE_FOR_DELETE_CONFIRMATION;
}

#pragma mark - Methods

//saves all the information to database
-(void) save {
    [[DataEngine sharedInstance] saveContext];
}

-(NSInteger*)countUnsyncedTransactions{
    
        self.syncTransactionArray = [[DataEngine sharedInstance] transactionsForRegistrant:[Utils getRegistrantId] AndUser:[Utils getUserId] NotDeletedSort:@"createdDate" ascending:NO filterSync:YES];
    
//    Registrant  *registrantInfo=[[DataEngine sharedInstance]  registrantForId:[Utils getRegistrantId]];
    
//    NSLog([NSString stringWithFormat:@"Registrant Id: %@, User ID: %d !!!!",[[Utils getRegistrantId] stringValue],[Utils getUserId]]);
        if ([self.syncTransactionArray count]>0) {

            NSString *lastMonth = [self getClaimMonth:2];
            NSNumber *currentYear = [self getClaimCurrentYear];
            
            NSInteger *lastMonthCount=0;
            
            NSString *RegistrantId = [[Utils getRegistrantId] stringValue];
         
            for (Transaction *test in self.syncTransactionArray) {
                NSString *testRegistrantId1 = [test.outgoingRegistrant.registrationNumber stringValue];
                NSString *testRegistrantId2 = [test.incomingRegistrant.registrationNumber stringValue];
//                NSLog([NSString stringWithFormat:@"Registrant ID: %@, transaction ID: %d!!",[[Utils getRegistrantId] stringValue],testRegistrantId]);
                if ([testRegistrantId1 isEqualToString:RegistrantId] || [testRegistrantId2 isEqualToString:RegistrantId] ) {
                if ([lastMonth isEqualToString:[self getMonthFromDate:test.createdDate]]) {
                        if ([lastMonth isEqualToString:@"Dec"]) {
                            if (([[self getYearFromDate:test.createdDate] intValue]+1) == [currentYear intValue]) {
                                lastMonthCount++;
                            }
                        }  else {
                            if ([[self getYearFromDate:test.createdDate] isEqualToNumber:currentYear]) {
                                lastMonthCount++;
                            }
                        }
                };
                };
            }
//            NSLog([NSString stringWithFormat:@"the count is ::%d !!!!", lastMonthCount]);
            return lastMonthCount;
        } else {
            return 0;
        }
    
}

- (void)checkUnsyncedTransactions {
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    
    self.syncTransactionArray = [[DataEngine sharedInstance] transactionsForRegistrant:[Utils getRegistrantId] AndUser:[Utils getUserId] NotDeletedSort:@"createdDate" ascending:NO filterSync:YES];
    if ([self.syncTransactionArray count]>0) {
        
        NSString *twoMonthsAgo = [self getClaimMonth:3];
        NSString *lastMonth = [self getClaimMonth:2];
        NSString *currentMonth = [self getClaimMonth:1];
        
        NSNumber *firstClaimYear;
        NSNumber *secondClaimYear;
        NSNumber *currentYear = [self getClaimCurrentYear];

        int twoMonthCount=0;
        int lastMonthCount=0;
        int currentMonthCount=0;
        
        if (![self isMonthEnd]) {
            
            //iterate array and see if any transaction from two months ago
            for (Transaction *test in self.syncTransactionArray) {
                if ([twoMonthsAgo isEqualToString:[self getMonthFromDate:test.createdDate]]) {
                    if ([twoMonthsAgo isEqualToString:@"Dec"] || [twoMonthsAgo isEqualToString:@"Nov"]) {
                        if (([[self getYearFromDate:test.createdDate] intValue]+1) == [currentYear intValue]) {
                            twoMonthCount++;
                        }
                    }  else {
                        if ([[self getYearFromDate:test.createdDate] isEqualToNumber:currentYear]) {
                            twoMonthCount++;
                        }
                    }
                    
                }  else if ([lastMonth isEqualToString:[self getMonthFromDate:test.createdDate]]) {
                    if ([lastMonth isEqualToString:@"Dec"]) {
                        if (([[self getYearFromDate:test.createdDate] intValue]+1) == [currentYear intValue]) {
                            lastMonthCount++;
                        }
                    }  else {
                        if ([[self getYearFromDate:test.createdDate] isEqualToNumber:currentYear]) {
                            lastMonthCount++;
                        }
                    }
                }
            }
            
            
            
            if (twoMonthCount > 0 && lastMonthCount > 0) {
                //display message for both months
                if (twoMonthCount == 1) {
                    self.self.tranClaimTwoText = @"transaction";
                }  else {
                    self.self.tranClaimTwoText = @"transactions";
                }
                
                if (lastMonthCount == 1) {
                    self.tranClaimOneText = @"transaction";
                }  else {
                    self.tranClaimOneText = @"transactions";
                }
                
                if ([twoMonthsAgo isEqualToString:@"Dec"] || [twoMonthsAgo isEqualToString:@"Nov"]) {
                    firstClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    firstClaimYear = currentYear;
                }
                
                if ([lastMonth isEqualToString:@"Dec"]) {
                    secondClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    secondClaimYear = currentYear;
                }
                self.alertMessage = [NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period, and %d unsynced or incomplete %@ from %@. %@ claim period on this iPad.", twoMonthCount, self.tranClaimTwoText, twoMonthsAgo, firstClaimYear, lastMonthCount, self.tranClaimOneText, lastMonth, secondClaimYear];
                
                
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
                
            } else if (twoMonthCount==0 && lastMonthCount > 0) {
                //display message for last month
                if (lastMonthCount == 1) {
                    self.tranClaimOneText = @"transaction";
                }  else {
                    self.tranClaimOneText = @"transactions";
                }
                
                if ([lastMonth isEqualToString:@"Dec"]) {
                    secondClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    secondClaimYear = currentYear;
                }
                self.alertMessage =[NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period on this iPad.", lastMonthCount, self.tranClaimOneText, lastMonth, secondClaimYear];
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
                
                
            } else if (twoMonthCount > 0 && lastMonthCount == 0){
                //display message for two months ago
                if (twoMonthCount == 1) {
                    self.tranClaimTwoText = @"transaction";
                }  else {
                    self.tranClaimTwoText = @"transactions";
                }
                
                if ([twoMonthsAgo isEqualToString:@"Dec"] || [twoMonthsAgo isEqualToString:@"Nov"]) {
                    firstClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    firstClaimYear = currentYear;
                }
                
                self.alertMessage = [NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period on this iPad.", twoMonthCount, self.tranClaimTwoText, twoMonthsAgo, firstClaimYear];
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
            }
            
            if ([self.alertMessage length]>0) {
                [self presentAlert:nil info:NULL];
                
            }
            
            
            
            
            
        } else if ([self isMonthEnd]) {
            
            lastMonthCount = 0;
            currentMonthCount = 0;
            
            for (Transaction *test in self.syncTransactionArray) {
                if ([lastMonth isEqualToString:[self getMonthFromDate:test.createdDate]]) {
                    if ([lastMonth isEqualToString:@"Dec"]) {
                        if (([[self getYearFromDate:test.createdDate] intValue]+1) == [currentYear intValue]) {
                            lastMonthCount++;
                        }
                    }  else {
                        if ([[self getYearFromDate:test.createdDate] isEqualToNumber:currentYear]) {
                            lastMonthCount++;
                        }
                    }
                }  else if ([currentMonth isEqualToString:[self getMonthFromDate:test.createdDate]]) {
                    if ([[self getYearFromDate:test.createdDate] isEqualToNumber:currentYear]) {
                        currentMonthCount++;
                    }
                }
            }
            
            
            
            if (lastMonthCount > 0 && currentMonthCount > 0) {
                //display message for both months
                if (lastMonthCount == 1) {
                    self.tranClaimOneText = @"transaction";
                }  else {
                    self.tranClaimOneText = @"transactions";
                }
                if (currentMonthCount == 1) {
                    self.tranClaimTwoText = @"transaction";
                }  else {
                    self.tranClaimTwoText = @"transactions";
                }
                
                if ([lastMonth isEqualToString:@"Dec"]) {
                    firstClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    firstClaimYear = currentYear;
                }
                self.alertMessage = [NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period, and %d unsynced or incomplete %@ from %@. %@ claim period on this iPad.", lastMonthCount, self.tranClaimOneText, lastMonth, firstClaimYear, currentMonthCount, self.tranClaimTwoText, currentMonth, currentYear];
                
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
                
            } else if (lastMonthCount == 0 && currentMonthCount > 0) {
                //display message for current month
                if (currentMonthCount == 1) {
                    self.tranClaimTwoText = @"transaction";
                }  else {
                    self.tranClaimTwoText = @"transactions";
                }
                self.alertMessage =[NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period on this iPad.", currentMonthCount, self.tranClaimTwoText, currentMonth, currentYear];
                
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
                
            } else if (lastMonthCount > 0 && currentMonthCount == 0) {
                //display message for last month
                if (lastMonthCount == 1) {
                    self.tranClaimOneText = @"transaction";
                }  else {
                    self.tranClaimOneText = @"transactions";
                }
                
                if ([lastMonth isEqualToString:@"Dec"]) {
                    firstClaimYear =  [NSNumber numberWithInt:([currentYear intValue]-1)];
                }  else {
                    firstClaimYear = currentYear;
                }
                self.alertMessage =[NSString stringWithFormat:@"You have %d unsynced or incomplete %@ from the %@. %@ claim period on this iPad.", lastMonthCount, self.tranClaimOneText, lastMonth, firstClaimYear];
                
                
                [defaults setObject:self.alertMessage forKey:@"message"];
                [defaults synchronize];
                
            }
            
           
            
            if ([self.alertMessage length]>0) {
                [self presentAlert:nil info:NULL];
                
                
            }
            
            
            
            
            
        }
        
 //        countforLastMonth = lastMonthCount;
        
    }
    
   
    self.isUnsyncedTransactionsChecked = YES;
}


#pragma mark - TransactionDelegate

- (void)transactionDidVoid {
    if(self.searchStatus==ON_SCREEN)
    {
        
        
        //grab Index for Filter array
        NSInteger delRowAtFilterArray = [self.filteredTransactionArray indexOfObject:[self.searchTransactionArray objectAtIndex:self.deletedRowIndexPath.row]];
        
        [self.searchTransactionArray removeObjectAtIndex:self.deletedRowIndexPath.row];
        
        //remove the same row  from filter array.
        [self.filteredTransactionArray removeObjectAtIndex:delRowAtFilterArray];
        
        //update the find count*/
        self.transactionFindCountLabel.text= [NSString stringWithFormat:@"%lu",(unsigned long) self.searchTransactionArray.count];
        
    }
    else
    {
        [self.filteredTransactionArray removeObjectAtIndex:self.deletedRowIndexPath.row];
    }
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[self.deletedRowIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    [self updateUI];
}

- (void) cancelVoidProcess {
    
    [self.tableView setEditing:NO animated:YES];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if(self.searchStatus==ON_SCREEN)
    {
        return;
    }
    if (self.tableView.contentOffset.y < -70)
    {
        self.searchButtonOutlet.selected = YES;
        [self searchUiUpdate:ON_SCREEN];
    }
    
}

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    NewAlertView *newAlertView = [[NewAlertView alloc] init];
//    newAlertView.frame = CGRectMake(floor((self.view.frame.size.width - newAlertView.frame.size.width) / 2),
//                                    floor((self.view.frame.size.height - newAlertView.frame.size.height) / 2),
//                                    newAlertView.frame.size.width,
//                                    newAlertView.frame.size.height);
    newAlertView.title = @"Attention";
    newAlertView.text = self.alertMessage;
    newAlertView.primaryButtonText = @"OK";
    newAlertView.delegate = self;
    newAlertView.singlelineforOkay.hidden=YES;
    
    newAlertView.setColour =YES;
    [newAlertView setUI];
    [[ModalViewController sharedModalViewController] showView:newAlertView];
    /*
     AlertViewController *alert = [[AlertViewController alloc] init];
     alert.title = @"Attention";
     alert.heading = @"";
     alert.message = self.alertMessage;
     alert.primaryButtonText = @"Confirm";
     alert.secondaryButtonText = @"Cancel";
     
     if (info == NULL) {
     
     dispatch_async(dispatch_get_main_queue(), ^{
     alert.primaryButton.hidden = YES;
     alert.secondaryButton.hidden = YES;
     alert.okButton.hidden = NO;
     
     //  if ([alert.message length]>100) {
     //  [alert.messageLabel setFont:[UIFont fontWithName:@"OpenSans-Light" size:19]];
     //}
     
     });
     
     }
     
     alert.delegate = self;
     [alert presentAlert:viewController];
     */
}

- (void)alertView:(AlertViewController *)alertView primaryAction:(UIButton *)button info:(NSDictionary *)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - NewAlertViewDelegate

- (void)primaryAction:(UIButton *)sender {
    
    [[ModalViewController sharedModalViewController] hideView];
}

-(IBAction)buttonTransactionsTotalAction:(id)sender
{
   /* Removing for 2.0
    
    if ([Utils getProcessorDidLogin]==YES)
    {
        return;
    }
    self.arrayPtr =[[NSMutableArray alloc]init];
    self.arrayStc =[[NSMutableArray alloc]init];
    self.arrayTcr =[[NSMutableArray alloc]init];
    self.arrayTransaction =[[NSMutableArray alloc]init];
    
    if (self.searchStatus == ON_SCREEN) {
        
        if ([self.searchTransactionArray count]>0) {
            [self.arrayTransaction addObjectsFromArray:self.searchTransactionArray];

        }
        
    }
    else
    {
        if ([self.filteredTransactionArray count]>0) {
          [self.arrayTransaction addObjectsFromArray:self.filteredTransactionArray];
        }
        
    }
    
        for (int intcount =0; [self.arrayTransaction count]>intcount; intcount++) {
            
            Transaction *transaction;
            transaction=[self.arrayTransaction objectAtIndex:intcount];
            if ([transaction.transactionType.shortNameKey isEqualToString:@"PTR"]) {
                
                [self.arrayPtr  addObject:transaction.transactionType.shortNameKey];
                NSLog(@"%@",self.arrayPtr);
                
            }
            else if ([transaction.transactionType.shortNameKey isEqualToString:@"STC"])
            {
                [self.arrayStc  addObject:transaction.transactionType.shortNameKey];
                NSLog(@"%@",self.arrayStc);
            }
            else if ([transaction.transactionType.shortNameKey isEqualToString:@"TCR"])
            {
                [self.arrayTcr  addObject:transaction.transactionType.shortNameKey];
                NSLog(@"%@",self.arrayTcr);
            }
            

            
        }
    
   // NSLog(@"%lu",(unsigned long)[self.arrayPtr count]);
    //NSLog(@"%lu",(unsigned long)[self.arrayStc count]);
    //NSLog(@"%lu",(unsigned long)[self.arrayTcr count]);
    //[NSString stringWithFormat:@"TCR %lu\r STC %lu",(@"%lu",(unsigned long)[self.arrayTcr count]), (@"%lu",(unsigned long)[self.arrayStc count])];
    CMPopTipView *popTipView;
    
    //not needed in case of  Processor.
    popTipView = [[CMPopTipView alloc] initWithTitle:@"  COUNT    " message:[NSString stringWithFormat:@"TCR %lu\rSTC %lu",((unsigned long)[self.arrayTcr count]), ((unsigned long)[self.arrayStc count])]];
    popTipView.delegate = self;
    
    
    //popTipView.disableTapToDismiss = YES;
    popTipView.preferredPointDirection = PointDirectionUp;
    popTipView.hasGradientBackground = NO;
    popTipView.has3DStyle=NO;
    popTipView.animation=CMPopTipAnimationPop;
    popTipView.backgroundColor = [UIColor colorWithRed:(90./255)
                                                  green:(147./255)
                                                   blue:(33./255)
                                                  alpha:1];
    //popTipView.maxWidth=20;
    //popTipView.cornerRadius = 2.0;
    //popTipView.sidePadding = 30.0f;
    //popTipView.topMargin = 20.0f;
    //popTipView.pointerSize = 50.0f;
    popTipView.hasShadow = YES;
    
    
    //popTipView.animation = arc4random() % 2;
    //popTipView.has3DStyle = (BOOL)(arc4random() % 2);
    
    popTipView.dismissTapAnywhere = YES;
    [popTipView autoDismissAnimated:YES atTimeInterval:3.0];
    
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *button = (UIButton *)sender;
        [popTipView presentPointingAtView:button inView:self.view animated:YES];
    }

    */
}- (void)popTipViewWasDismissedByUser:(CMPopTipView *)popTipView
{
   // [self.visiblePopTipViews removeObject:popTipView];
    //self.currentPopTipViewTarget = nil;
}


@end
