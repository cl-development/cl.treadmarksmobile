//
//  DocumentsViewController.h
//  ComponentsPilot
//
//  Created by Dennis Christopher on 2013-10-24.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "PageViewController.h"
//#import "ScaleTicketComponentView.h"


@interface DocumentsPageViewController : PageViewController <ComponentViewDelegate>

@end
