//
//  PageViewController.h
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"

@class DockViewController;

@interface PageViewController : UIViewController

@property (nonatomic,weak) Transaction *transaction;

@property (nonatomic,strong) NSMutableArray* componentViewArray;
@property (nonatomic,strong) PageViewController *nextPageViewController;

@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;

//the view controller that started the transaction (list or calendar)
//used to load this screen after a transaction is completed
@property (nonatomic,weak) UIViewController *rootViewController;

//load next page in the flow
-(IBAction)loadNextPage;

//load previous page in the flow
-(IBAction)backButtonPressed:(id)sender;

//validation items

//validate image
@property (nonatomic,weak) IBOutlet UIImageView *validateImageView;

@property (nonatomic) BOOL valid;

#pragma mark - Properties

@property (weak, nonatomic) DockViewController* dockViewController;

//validates the controls on the page
-(void)validate;

//update the validate image
-(void)updateValidateImage;

-(void)updateUI;

// for signature use only:
- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag;

-(void)getComponentsView :(int)selectedViewindex;

@end
