//
//  ParticipantsPageViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-19.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ParticipantsPageViewController.h"
#import "TransactionInformationComponentView.h"
#import "ParticipantComponentView.h"
#import "DataEngine.h"
#import "Constants.h"
#import "TransactionType.h"
#import "MaterialQuantityPageViewController.h"

@interface ParticipantsPageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *continueImageView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *nextPageGestureRecognizer;

@end

@implementation ParticipantsPageViewController

#pragma mark - UIViewController

- (void)viewDidLoad{
    [super viewDidLoad];
 
    //init the components
    self.componentViewArray = [NSMutableArray new];

    TransactionInformationComponentView * transactionView = [[TransactionInformationComponentView alloc] init];
    transactionView.parentPageViewController = self;
    [self.componentViewArray addObject:transactionView];
    
    ParticipantComponentView *participantComponentView = [[ParticipantComponentView alloc] init];
    participantComponentView.isIncomingUser = YES;
    participantComponentView.parentPageViewController = self;
    [self.componentViewArray addObject:participantComponentView];
    
    participantComponentView = [[ParticipantComponentView alloc] init];
    participantComponentView.isIncomingUser = NO;
    participantComponentView.parentPageViewController = self;
    [self.componentViewArray addObject:participantComponentView];    

    [self updateUI];
    
    self.nextPageViewController = [MaterialQuantityPageViewController new];
    self.nextPageViewController.rootViewController = self.rootViewController;
    
}

#pragma mark - PageViewController

- (void)updateUI {
    //update the screen items
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIdLabel.text = [self.transaction.friendlyId stringValue];

    CGFloat offset=0;
    for (ComponentView *componentView in self.componentViewArray) {
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  componentView.frame.size.height);
        [self.scrollView addSubview:componentView];
        [componentView updateUI];
        offset+=componentView.frame.size.height;
        
        //gray horizontal bar
        UIImageView *brImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"trans-panel-br.png"]];
        brImageView.frame = CGRectMake(0, offset, COMPONENT_WIDTH, BR_IMAGE_HEIGHT);
        [self.scrollView addSubview:brImageView];
        offset += BR_IMAGE_HEIGHT;
    }
    [self validate];
}

-(void)validate{
    [super validate];
    if (self.valid) {
        self.continueImageView.image = [UIImage imageNamed:@"trans-btn-cont-on.png"];
    } else {
        self.continueImageView.image = [UIImage imageNamed:@"trans-btn-cont-off.png"];
    }
}

@end
