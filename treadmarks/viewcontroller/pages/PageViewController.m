//
//  PageViewController.m
//  ComponentsPilot
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import "PageViewController.h"
#import "ComponentView.h"
#import "Constants.h"
#import "Utils.h"
//#import "DockViewController.h"

@interface PageViewController ()

@end

@implementation PageViewController

#pragma mark - navigation

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    self.transaction = [Utils getTransaction];
    self.valid = YES;
    return self;
}

-(void)updateUI{}

//load next page in the flow
-(void)loadNextPage{
    if (self.valid) {
        [self.navigationController pushViewController:self.nextPageViewController animated:YES];
    } else {
        [Utils showMessage:self header:@"Warning" message:@"Page is not valid. Cannot continue" hideDelay:2];
    }
}

//load previous page in the flow
-(IBAction)backButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - validation

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:PAGE_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:PAGE_VALIDATION_NOT_OK_IMAGE];
    }
}

//validate the page
-(void)validate{
    BOOL validated=YES;
    for (ComponentView *componentView in self.componentViewArray) {
        ////NSLog(@"%@ - valid: %@", NSStringFromClass(componentView.class), componentView.valid?@"YES":@"NO");
        validated = validated && componentView.valid;
    }
    self.valid=validated;
    [self updateValidateImage];
}

#pragma mark - Accessors

- (DockViewController *)dockViewController {
	
	if(_dockViewController == NULL)
	{
		_dockViewController = (DockViewController*)self.parentViewController.parentViewController;
	}
	
	return _dockViewController;
}

#pragma mark - Override methods

/*- (void)presentViewController:(UIViewController *)viewControllerToPresent
					 animated:(BOOL)flag
				   completion:(void (^)(void))completion NS_AVAILABLE_IOS(5_0) {
	
	UINavigationController* navigationController = self.dockViewController.navigationController;
	navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
	[navigationController presentViewController:viewControllerToPresent
									   animated:flag
									 completion:completion];
}*/

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag {
	
	//UINavigationController* navigationController = self.dockViewController.navigationController;
	//navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
	[self.navigationController presentViewController:viewControllerToPresent animated:flag completion:NULL];
 }

/* suggested by Josh Tidsbury Apple Canada DTS. */
- (void)presentViewController:(UIViewController *)viewControllerToPresent
                     animated:(BOOL)flag
                   completion:(void (^)(void))completion NS_AVAILABLE_IOS(5_0) {
    
    //UINavigationController* navigationController = self.dockViewController.navigationController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    //viewControllerToPresent.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.navigationController presentViewController:viewControllerToPresent
                                       animated:flag
                                     completion:completion];
}


@end
