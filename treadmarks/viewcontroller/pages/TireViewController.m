//
//  TireViewController.m
//  TreadMarks
//
//  Created by Capris Group on 11/7/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TireViewController.h"

@interface TireViewController ()

@end

@implementation TireViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark - UITaleViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  [self.tireCode count];
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isKindOfClass:[SimpleTableCell class]]) {
        
        //  SimpleTableCell *tableCell = (SimpleTableCell *)cell;
        
        if (indexPath.row == [self.tireCode count]-1) {
            if([tableView indexPathForSelectedRow] == nil && [self.tireCode count] > 0)
            {
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                _cell = (SimpleTableCell*)cell;
                [_rulerLabel setText:_cell.tireNameLabel.text];
                [_cell.tireCodeField becomeFirstResponder];
            }
            
            
        }
        
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView indexPathForSelectedRow] == nil) {
        
        [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];
        NSIndexPath *rowToBeSelected = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [UIView animateWithDuration:.3 animations:^{
            [_table selectRowAtIndexPath:rowToBeSelected animated:NO scrollPosition:UITableViewScrollPositionBottom];
            CGRect rect = [self.view convertRect:[_table rectForRowAtIndexPath:rowToBeSelected] fromView:_table];
            NSLog(@"The next index is: %d", rowToBeSelected.row);
            
            CGFloat floatx = _imageView.frame.origin.x - rect.origin.x;
            _imageView.frame = CGRectMake(rect.origin.x + floatx, rect.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
        }];
        
    }
    
    static NSString *cellIdentifier = @"Cell";
    
    _cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (_cell == nil) {
        
        _cell = [[SimpleTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    _cell.tireNameLabel.text = [_tireName objectAtIndex:indexPath.row];
    //[_rulerLabel setText:[_tireName objectAtIndex:indexPath.row]];
    _cell.tireCodeField.tag = indexPath.row + 50;
    _cell.tireCodeField.delegate = self;
    _cell.tireCodeField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [_cell.tireCodeField setTextAlignment:NSTextAlignmentRight];
    //[_cell.tireCodeField becomeFirstResponder];
    [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    _selectedCell = _cell;
    
    return _cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath == NULL) {
        
        indexPath = indexPath;
        
    }
    
    [UIView animateWithDuration:.3 animations:^{
        
        CGRect rect = [self.view convertRect:[tableView rectForRowAtIndexPath:indexPath] fromView:tableView];
        
        CGFloat floatx = _imageView.frame.origin.x - rect.origin.x;
        _imageView.frame = CGRectMake(rect.origin.x + floatx, rect.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
        
    }];
    
    _cell = (SimpleTableCell *)[self.table cellForRowAtIndexPath:indexPath];
    
    [_rulerLabel setText:_cell.tireNameLabel.text];
    NSLog(@"Table row %d has been tapped", indexPath.row);
    
    if (_selectedTextField) {
        
        _selectedTextField = NO;
    }
    else {
        
        [_cell.tireCodeField performSelector:@selector(becomeFirstResponder) withObject:self afterDelay:0.25 ];
        
    }
    
    
    
}


-(BOOL)isRowVisible:(NSIndexPath *)nextIndex :(int) tagValue{
    
    NSArray *indexes = [_table indexPathsForVisibleRows];
    
    for (NSIndexPath *index in indexes) {
        
        NSLog(@"The index here is: %d", index.row);
        
    }
    
    
    for (NSIndexPath *index in indexes) {
        
        NSLog(@"%d", index.row);
        NSLog(@"We are on row: %d", nextIndex.row);
        
        if (tagValue == 1) {
            
            if (nextIndex.row - 1 == index.row) {
                
                return YES;
                
            }
            
        }
        
        else if (tagValue == 2) {
            
            if (nextIndex.row + 1 == index.row) {
                
                return YES;
                
            }
            
        }
        
    }
    
    return NO;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
    
}

- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer {
    
    NSLog(@"Do I get called?");
    _startLocation = [recognizer locationInView:_imageView];
    NSLog(@"The point is: %@", NSStringFromCGPoint(_startLocation));
    
    NSIndexPath *indexPath;
    UITableViewCell *cell;
    //NSCharacterSet *invalid = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    int selectedRow = [_table indexPathForSelectedRow].row;
    int rowHeight = [_table rowHeight];
    
    NSLog(@"The current row is: %d", selectedRow);
    NSLog(@"The height of the row is: %d", rowHeight);
    
    
    CGPoint newCenter  = _imageView.center;
    newCenter.y = [recognizer locationInView:[_imageView superview]].y;
    
    NSLog(@"The y coordinate of the center of imageView is: %f", newCenter.y);
    NSLog(@"This is 1/2 of the height of imageView: %f", _imageView.frame.size.height);
    NSLog(@"This is the y coordinate of the origin of the table %f", self.table.frame.origin.y);
    
    //this is what limits the movement of the slider above the table
    if (newCenter.y - _imageView.frame.size.height / 2 < self.table.frame.origin.y) {
        
        newCenter.y = self.table.frame.origin.y + _imageView.frame.size.height / 2;
        NSLog(@"abc");
        int tableOrigin = self.table.frame.origin.y;
        int tableHeight = self.table.frame.size.height;
        int newCentre = newCenter.y;
        
        NSLog(@"the origin is: %d, and the height of the table is %d, and the center is: %d", tableOrigin, tableHeight, newCentre);
    }
    
    else if (newCenter.y + _imageView.frame.size.height / 2 > self.table.frame.origin.y + self.table.frame.size.height) {
        
        newCenter.y = self.table.frame.origin.y + self.table.frame.size.height - _imageView.frame.size.height / 2;
        NSLog(@"123");
    }
    //this is what prevents the slider from going below the table
    else if (newCenter.y - _imageView.frame.size.height / 2 > self.table.frame.origin.y + [_tireName count] * rowHeight) {
        
        newCenter.y = self.table.frame.origin.y + [_tireName count] * rowHeight + _imageView.frame.size.height / 2;
        int jim = self.table.frame.origin.y + [_tireName count] * rowHeight + _imageView.frame.size.height / 2;
        NSLog(@"tire count %d", [_tireName count]);
        NSLog(@"xyz %d", jim);
        
    }
    
    
    _imageView.center = newCenter;
    
    
    if ([recognizer state] == UIGestureRecognizerStateChanged) {
        
        NSLog(@"what what what?");
        //this is what gets called when the slider moves above the first row
        if (newCenter.y == self.table.frame.origin.y + _imageView.frame.size.height / 2) {
            int bob = self.table.frame.origin.y;
            int rob = _imageView.frame.size.height;
            int boo = self.table.frame.origin.y + _imageView.frame.size.height / 2;
            NSLog(@"Origin: %d", bob);
            NSLog(@"frame height: %d", rob);
            NSLog(@"When do I get called? Answer is %d:", boo);
            
            CGPoint topPoint = [recognizer locationInView:_table];
            NSIndexPath *nextRow = [_table indexPathForRowAtPoint:topPoint];
            int currentRow = nextRow.row;
            
            if (currentRow != 0) {
                
                NSLog(@"do i get called at all?");
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [_table selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
                
            }
            
        }
        
        else if (newCenter.y == self.table.frame.origin.y + self.table.frame.size.height - _imageView.frame.size.height / 2) {
            
            NSLog(@"does thing ever call?");
            CGPoint bottomPoint = [recognizer locationInView:_table];
            NSIndexPath *nextRow = [_table indexPathForRowAtPoint:bottomPoint];
            int currentRow = nextRow.row;
            
            if (currentRow != [self.tireCode count]-1) {
                
                NSLog(@"me too?");
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [_table selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
                
            }
            
        }
        //below is for bug fix
        /*
         else if (newCenter.y > ([_tireName count] - 1) * rowHeight + rowHeight / 2) {
         
         newCenter.y = ([_tireName count] - 1) * rowHeight + rowHeight / 2;
         int jim = ([_tireName count] - 1) * rowHeight + rowHeight / 2;
         NSLog(@"tire count %d", [_tireName count]);
         NSLog(@"xyz %d", jim);
         }
         */
        else if (newCenter.y > self.table.frame.origin.y + self.table.frame.size.height) {
            
            NSLog(@"Does fayyaz get called?");
            
            CGPoint bottomPoint = [recognizer locationInView:_table];
            NSIndexPath *nextRow = [_table indexPathForRowAtPoint:bottomPoint];
            int currentRow = nextRow.row;
            
            if (currentRow != [self.tireCode count]-1) {
                
                NSLog(@"do i get called ever?");
                nextRow = [NSIndexPath indexPathForRow:currentRow-- inSection:0];
                
                [UIView animateWithDuration:.3 animations:^{
                    [_table selectRowAtIndexPath:nextRow animated:NO scrollPosition:UITableViewScrollPositionBottom];
                    
                }];
                
            }
            
            
        }
        
        
        [_rulerLabel setText:@""];
        
    }
    
    
    
    //Once user has stopped moving the UIImageView/scrollbar, if the UIImageView/scrollbar is in between two rows, link to the row that the UIImageView/scrollbar is closest to.
    else if ([recognizer state] == UIGestureRecognizerStateEnded) {
        
        CGPoint touchPoint = [recognizer locationInView:_table];
        
        indexPath = [_table indexPathForRowAtPoint:touchPoint];
        
        if(indexPath == nil) {
            
            if(touchPoint.y < 0) {
                int wow = touchPoint.y;
                NSLog(@"curly: %d", wow);
                indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            
            //else if (touchPoint.y > _table.frame.size.height - _table.frame.origin.y){
            else if (touchPoint.y > [_tireName count] * rowHeight) {
                
                int wow = touchPoint.y;
                NSLog(@"larry: %d", wow);
                indexPath = [NSIndexPath indexPathForRow:[_tireCode count]-1 inSection:0];
                
            }
            
            else {
                //this is why the ruler scrolls all the way to the top
                int wow = touchPoint.y;
                int bee = _table.frame.size.height;
                int origin = _table.frame.origin.y;
                NSLog(@"tochpoint: %d", wow);
                NSLog(@"table height: %d", bee);
                NSLog(@"table origin: %d", origin);
                indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            
        }
        
        cell = [_table cellForRowAtIndexPath:indexPath];
        
        [UIView animateWithDuration:.3 animations:^{
            [_table selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
            CGRect rect = [self.view convertRect:[_table rectForRowAtIndexPath:indexPath] fromView:_table];
            
            CGFloat floatx = _imageView.frame.origin.x - rect.origin.x;
            _imageView.frame = CGRectMake(rect.origin.x + floatx, rect.origin.y, _imageView.frame.size.width, _imageView.frame.size.height);
        }];
        
        _cell = (SimpleTableCell *)[self.table cellForRowAtIndexPath:indexPath];
        [_rulerLabel setText:_cell.tireNameLabel.text];
        [_cell.tireCodeField becomeFirstResponder];
        
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag >= 50)
    {
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    else
        return YES;
}


- (IBAction)backButtonPressed:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
}


- (IBAction)buttonPressed:(id)sender {
}
@end
