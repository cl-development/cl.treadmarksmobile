//
//  TOCPageViewController
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-19.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TOCPageViewController.h"
#import "TOCTransactionInformationComponentView.h"
#import "DataEngine.h"
#import "Constants.h"
#import "TransactionType.h"
#import "RegistrantType.h"
#import "MaterialQuantityPageViewController.h"
#import "Utils.h"
#import "TOCParticipantComponentView.h"
#import "TOCPhotoComponentView.h"
#import "TOCTireCountComponentView.h"
#import "TOCNotesComponentView.h"
#import "ConfirmationPageViewController.h"
#import "TransactionWrapper.h"
#import "TOCScaleTicketComponentView.h"
#import "ListViewController.h"
#import "ModalViewController.h"
#import "NewAlertView.h"
#import "DockBarViewController.h"
#import "TreadMarks-Swift.h"


#define X_OFF_SCREEN -225
#define ROTATION 2./5. * 2*M_PI
#define SLIDE_TIME .6


@interface TOCPageViewController () <SlideViewDelegate, TransactionDelegate,NewAlertViewDelegate>
#pragma mark - IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView *tocIcon;

@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *continueImageView;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *nextPageGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIImageView *transTypeImageView;
@property (weak, nonatomic) IBOutlet UIButton *testConfirmationPageButton;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *voidButton;
@property (weak, nonatomic) IBOutlet UIButton *slideToVoidButton;
@property (weak, nonatomic) IBOutlet UIView *voidView;
@property CGPoint startingOrigin;
@property BOOL thresholdPassed;
@property (weak, nonatomic) IBOutlet UIButton *confirmSubmit;

@end

@implementation TOCPageViewController

#pragma mark - UIViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    //init the components
    
    self.tocIcon.layer.cornerRadius = self.tocIcon.frame.size.width /2
    ;
    self.tocIcon.clipsToBounds = YES;
    self.tocIcon.layer.borderWidth = 1.0f;
    self.tocIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    
    
    self.componentViewArray = [NSMutableArray new];
    [self addComponentsForTransactionType];
    DockBarViewController *dockBarView = [[DockBarViewController alloc] initWithNibName:@"DockBarViewController" bundle:nil];
    dockBarView.view.frame = CGRectMake(0,938,768,89);
    dockBarView.createNewTransaction=NO;
    [self.view addSubview:dockBarView.view];
    [self addChildViewController:dockBarView];
    [dockBarView didMoveToParentViewController:self];
    
    //New Submit Button
    self.confirmSubmit.backgroundColor = [UIColor clearColor];
    self.confirmSubmit.layer.cornerRadius = 8;
    self.confirmSubmit.layer.borderWidth = 1;
    self.confirmSubmit.layer.borderColor = self.view.tintColor.CGColor;//[UIColor colorWithRed:0 green:122 blue:255 alpha:1].CGColor;
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    
    
}

- (void)returnSlideToVoid:(UIView *)view {
    
    float x = 0;
    
    self.voidButton.enabled = NO;
    
    if(view.frame.origin.x + view.frame.size.width < self.voidButton.frame.origin.x)
    {
        x = self.voidButton.frame.origin.x - view.frame.size.width;
        
        self.voidButton.enabled = YES;
    }
    
    [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
                     animations:^
     {
         view.frame = CGRectMake(x,
                                 view.frame.origin.y,
                                 view.frame.size.width,
                                 view.frame.size.height);
     }];
}

-(void)viewDidAppear:(BOOL)animated{
    /*self.dockViewController.dockViewHidden = NO;
     self.dockViewController.slideView.hidden = NO;
     self.dockViewController.slideView.delegate = self;
     self.dockViewController.slideView.showVoid = YES;
     self.dockViewController.slideView.allowNewTransactions = NO;
     [self.dockViewController.slideView hideAnimated:YES
     completion:^
     {
     [self.dockViewController.slideView layoutSubviews];
     }];
     */
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


#pragma mark - TOCPageViewController

-(void) addComponentsForTransactionType {
    
    // [self.componentViewArray removeAllObjects];
    
    
    TOCTransactionInformationComponentView * transactionView = [TOCTransactionInformationComponentView new];
    transactionView.parentPageViewController = self;
    [self.componentViewArray addObject:transactionView];
    int var =[self.transaction.transactionType.transactionTypeId intValue];
    
    switch (var) {
        case TRANSACTION_TYPE_TCR:
        {
            // first component is always the incomingUser of the transaction
            User *incomingUser = self.transaction.incomingUser;
            RegistrantType *expectedRegType = nil;
            
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            // second component is always the outgoingUser of the transaction
            expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_COLLECTOR];
            TOCOutgoingComponentView  *participantView2 = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            participantView2.isIncomingUser = NO;
            participantView2.parentPageViewController = self;
            
            [self.componentViewArray addObject:participantView2];
            
            
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            // next Elligibility
            TOCEligibilityComponentView *eligibilityView = [[TOCEligibilityComponentView alloc] init];
            eligibilityView.parentPageViewController = self;
            [self.componentViewArray addObject:eligibilityView];
            
            // next Photos
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
        }
            break;
        case TRANSACTION_TYPE_STC:
        {
            User *incomingUser = self.transaction.incomingUser;
            RegistrantType *expectedRegType = nil;
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
            authorizationView.parentPageViewController = self;
            [self.componentViewArray addObject:authorizationView];
            
            TOCLocationComponentView *locationView = [[TOCLocationComponentView alloc] init];
            locationView.parentPageViewController = self;
            [self.componentViewArray addObject:locationView];
            
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
        }
            break;
        case TRANSACTION_TYPE_PTR:
        {
            RegistrantType *expectedRegType = nil;
            User *incomingUser = self.transaction.incomingUser;
            // detect processor login
            //
            // after Feb change the order
            
            if (incomingUser==nil) {
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
            }
            else if (incomingUser.registrant.registrantType.registrantTypeId==REGISTRANT_TYPE_ID_PROCESSOR) {
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
            }
            else {
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_PROCESSOR];
            }
            
            TOCParticipantComponentView *participantView;
            TOCOutgoingComponentView *outgoingView;
            if([Utils getProcessorPtrOnly]==YES)
            {
                // JUst flip th component  no change in DB for incoming and ougoing user
                //Has to be checked on server side to flip the values in TM
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_PROCESSOR];
                outgoingView = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
                outgoingView.isIncomingUser = NO;
                outgoingView.parentPageViewController = self;
                
                [self.componentViewArray addObject:outgoingView];
                
                
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
                participantView= [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
                participantView.isIncomingUser = YES;
                participantView.parentPageViewController = self;
                
                [self.componentViewArray addObject:participantView];
                
                
            }
            else
            {
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_PROCESSOR];
                outgoingView = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
                outgoingView.isIncomingUser = NO;
                outgoingView.parentPageViewController = self;
                
                [self.componentViewArray addObject:outgoingView];
                
                
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
                participantView= [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
                participantView.isIncomingUser = YES;
                participantView.parentPageViewController = self;
                
                [self.componentViewArray addObject:participantView];
/*
                participantView= [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
                participantView.isIncomingUser = YES;
                participantView.parentPageViewController = self;
                
                [self.componentViewArray addObject:participantView];
                
                // second component is always the outgoingUser of the transaction
                expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_PROCESSOR];//[self expectedRegistrantTypeFromTransactionType];
                
                outgoingView = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
                outgoingView.isIncomingUser = NO;
                outgoingView.parentPageViewController = self;
                
                [self.componentViewArray addObject:outgoingView];
 */
                
                
            }
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            TOCScaleTicketComponentView *scaleTicketView = [[TOCScaleTicketComponentView alloc] init];
            scaleTicketView.parentPageViewController = self;
            [self.componentViewArray addObject:scaleTicketView];
            
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
            
            
            // do we have a weight variance reason comment?
            
            
            if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
            {
                TOCNotesComponentView *tocWeightVarianceNoteView = [[TOCNotesComponentView alloc] init];
                tocWeightVarianceNoteView.parentPageViewController = self;
                tocWeightVarianceNoteView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
                [self.componentViewArray  insertObject:tocWeightVarianceNoteView atIndex:5];
            }
            
            
            
        }
            break;
        case TRANSACTION_TYPE_PIT:
        {
            /* NSLog(@"%d",self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT);
             NSLog(@"%d",self.transaction.transactionType.transactionTypeId==[NSNumber numberWithInteger:18]);
             NSLog(@"%d",[self.transaction.transactionType.transactionTypeId integerValue]==18);*/
            // first component is always the incomingUser of the transaction
            // for PIT expected type is always processor
            
            User *incomingUser = self.transaction.incomingUser;

            RegistrantType *expectedRegType = nil;
            

            expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_PROCESSOR];;

            // second component is always the outgoingUser of the transaction
            
            TOCOutgoingComponentView *participantView2;
            participantView2 = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            participantView2.isIncomingUser = NO;
            participantView2.parentPageViewController = self;
            [self.componentViewArray addObject:participantView2];
            
            
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            

            
           

            
            
           
        
            
            
            TOCMaterialTypeComponentView *materialTypeView = [[TOCMaterialTypeComponentView alloc] init];
            materialTypeView.parentPageViewController = self;
            [self.componentViewArray addObject:materialTypeView];
            
            
            
            
            TOCScaleTicketComponentView *scaleTicketView = [[TOCScaleTicketComponentView alloc] init];
            scaleTicketView.parentPageViewController = self;
            [self.componentViewArray addObject:scaleTicketView];
            
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
            
            if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
            {
                TOCNotesComponentView *tocWeightVarianceNoteView = [[TOCNotesComponentView alloc] init];
                tocWeightVarianceNoteView.parentPageViewController = self;
                tocWeightVarianceNoteView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
                [self.componentViewArray  insertObject:tocWeightVarianceNoteView atIndex:5];
            }
            
            
        }
            break;
        case TRANSACTION_TYPE_UCR:
        {
            // first component is always the incomingUser of the transaction
            User *incomingUser = self.transaction.incomingUser;
            RegistrantType *expectedRegType = nil;
            
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            // second component is always the outgoingUser of the transaction
            expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_COLLECTOR];
            TOCOutgoingComponentView  *participantView2 = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            participantView2.isIncomingUser = NO;
            participantView2.parentPageViewController = self;
            
            [self.componentViewArray addObject:participantView2];
            
            //            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            //            tireCountView.parentPageViewController = self;
            //            [self.componentViewArray addObject:tireCountView];
            
//            TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
//            authorizationView.parentPageViewController = self;
//            [self.componentViewArray addObject:authorizationView];
            
            
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            // next Elligibility
            TOCEligibilityComponentView *eligibilityView = [[TOCEligibilityComponentView alloc] init];
            eligibilityView.parentPageViewController = self;
            [self.componentViewArray addObject:eligibilityView];
            
            // next Photos
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
            
        }
            break;
            
        case TRANSACTION_TYPE_HIT:
        {
            // first component is always the incomingUser of the transaction
            User *incomingUser = self.transaction.incomingUser;
            RegistrantType *expectedRegType = nil;
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            /*
             // second component is always the outgoingUser of the transaction
             expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
             participantView = [[TOCParticipantComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
             participantView.isIncomingUser = NO;
             participantView.parentPageViewController = self;
             
             [self.componentViewArray addObject:participantView];
             
             */
            
            expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_HAULER];
            TOCOutgoingComponentView  *participantView2 = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            participantView2.isIncomingUser = NO;
            participantView2.parentPageViewController = self;
            [self.componentViewArray addObject:participantView2];
            
            
            /*
            TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
            authorizationView.parentPageViewController = self;
            [self.componentViewArray addObject:authorizationView];
            */
            
            
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            // next Photos
            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
        }
            break;
        case TRANSACTION_TYPE_DOT:
        {
            // first component is always the incomingUser of the transaction
            User *incomingUser = self.transaction.incomingUser;
            RegistrantType *expectedRegType = nil;
            TOCParticipantComponentView *participantView = [[TOCParticipantComponentView alloc] initWithUser:incomingUser expectedRegistrantType:expectedRegType];
            participantView.isIncomingUser = YES;
            participantView.parentPageViewController = self;
            [self.componentViewArray addObject:participantView];
            
            // second component is always the outgoingUser of the transaction
            expectedRegType = [[DataEngine sharedInstance] registrantTypeForId:REGISTRANT_TYPE_ID_COLLECTOR];
            //            participantView = [[TOCParticipantComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            //            participantView.isIncomingUser = NO;
            //            participantView.parentPageViewController = self;
            //
            //            [self.componentViewArray addObject:participantView];
            
            TOCOutgoingComponentView  *participantView2 = [[TOCOutgoingComponentView alloc] initWithUser:self.transaction.outgoingUser expectedRegistrantType:expectedRegType];
            participantView2.isIncomingUser = NO;
            participantView2.parentPageViewController = self;
            
            [self.componentViewArray addObject:participantView2];
            
            
            
//            TOCAuthorizationComponentView *authorizationView = [[TOCAuthorizationComponentView alloc] init];
//            authorizationView.parentPageViewController = self;
//            [self.componentViewArray addObject:authorizationView];
            
            TOCTireCountComponentView *tireCountView = [[TOCTireCountComponentView alloc] init];
            tireCountView.parentPageViewController = self;
            [self.componentViewArray addObject:tireCountView];
            
            // next Elligibility
            TOCEligibilityComponentView *eligibilityView = [[TOCEligibilityComponentView alloc] init];
            eligibilityView.parentPageViewController = self;
            [self.componentViewArray addObject:eligibilityView];
            
            // next Photos
            
            TOCScaleTicketComponentView *scaleTicketView = [[TOCScaleTicketComponentView alloc] init];
            scaleTicketView.parentPageViewController = self;
            [self.componentViewArray addObject:scaleTicketView];
            
        

            TOCPhotoComponentView *photoView = [[TOCPhotoComponentView alloc] init];
            photoView.parentPageViewController = self;
            [self.componentViewArray addObject:photoView];
            
            
            // next comments
            TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
            tocNotesView.parentPageViewController = self;
            [self.componentViewArray addObject:tocNotesView];
            
            
            if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES)
            {
                TOCNotesComponentView *tocWeightVarianceNoteView = [[TOCNotesComponentView alloc] init];
                tocWeightVarianceNoteView.parentPageViewController = self;
                tocWeightVarianceNoteView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
                [self.componentViewArray  insertObject:tocWeightVarianceNoteView atIndex:6];
            }

            
            
        }
            break;
        default:
            break;
    }
    
}

- (void)createUI {
    //update the screen items
    self.transactionTypeLabel.text = self.transaction.transactionType.shortNameKey;
    self.transactionIdLabel.text = [self.transaction.friendlyId stringValue];
    [self setTransTypeImageView];
    
    CGFloat offset=0;
    for (TOCComponentView *componentView in self.componentViewArray) {
        
        componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  [componentView height]);
        //a better solution needs to be found
        componentView.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [componentView height]);
        
        [self.scrollView addSubview:componentView];
        [componentView updateUI];
        offset+=componentView.frame.size.height;
    }
    [self.scrollView setNeedsDisplay];
    [self validate];
}

-(void)updateUI{
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         CGFloat offset=0;
                         for (TOCComponentView *componentView in self.componentViewArray) {
                             componentView.frame = CGRectMake(0, offset, componentView.frame.size.width,  [componentView height]);
                             //a better solution needs to be found
                             componentView.view.frame = CGRectMake(0, 0, componentView.frame.size.width,  [componentView height]);
                             offset+=componentView.frame.size.height;
                         }
                         [self.scrollView setContentSize:CGSizeMake(680,offset)];
                     }];
    
}

-(void) setTransTypeImageView {
    if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-tcr.png"];
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-stc.png"];
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PTR) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-ptr.png"];
    }
    else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PIT) {
        self.transTypeImageView.image = [UIImage imageNamed:@"toc-label-pit.png"];
    }
}

-(void)validate{
    [super validate];
    if (self.valid) {
        
        self.confirmSubmit.enabled=true;
        
        self.confirmSubmit.layer.borderColor = self.view.tintColor.CGColor;    }
    else {
        
        self.confirmSubmit.enabled=false;
        self.confirmSubmit.layer.borderColor =   [UIColor grayColor].CGColor;
        //[self.confirmSubmit titleColorForState:UIControlStateDisabled].CGColor ;
    }
    
}
- (IBAction)testConfirmationPage:(UIButton *)sender {
    
    [self loadNextPage];
}

/// Overridin because I want to reallocate the next page every time.
- (void)loadNextPageIfValid {
    
    if (self.valid)
    {
        // changes for weight variance check:
        /* if
         1. scaleTicketComponent exists and reports an issue
         2. AND a comment for it has not already been entered, then prompt user for reason
         
         Move it TOC scale ticket and ask user if variance */
        
        
//        if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT) {
//            [self loadNextPage];
//
//        }
//        
//        else
//        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Variance1500"])
            {
                
                [self loadNextPage];
                return;
            }
            
            
            
            TOCScaleTicketComponentView *compView = [self scaleTicketComponentView];
            if (compView != nil) {
                if ([Utils transactionHasWeightVarianceComment:self.transaction]==NO && compView.weightVarianceOkay==NO) {
                    [self presentAlert:self info:nil];
                    // [self showWeightcomment];
                    return;
                }
            }
            
            [self loadNextPage];

            
        }
          //  }
    else
    {
        [Utils showMessage:self header:@"Warning" message:@"Page is not valid. Cannot continue" hideDelay:2];
    }
}

- (void)loadNextPage {
    
    ConfirmationPageViewController* confirmationPage = [[ConfirmationPageViewController alloc] init];
    confirmationPage.rootViewController = self.rootViewController;
    
    [self.navigationController pushViewController:confirmationPage animated:YES];
}

#pragma mark - Weight Variance check

-(TOCScaleTicketComponentView *)scaleTicketComponentView {
    TOCScaleTicketComponentView *scaleTicketComponent = nil;
    
    for (TOCComponentView *view in self.componentViewArray) {
        if ([view isKindOfClass:[TOCScaleTicketComponentView class]]==YES) {
            scaleTicketComponent = (TOCScaleTicketComponentView *)view;
        }
    }
    return scaleTicketComponent;
}


-(void)removeWeightVarianceCommentComponent {
    TOCComponentView *viewToRemove = nil;
    for (TOCComponentView *componentView in self.componentViewArray) {
        if ([componentView isKindOfClass: [TOCNotesComponentView class]]==YES) {
            TOCNotesComponentView *notes = (TOCNotesComponentView *)componentView;
            if (notes.type==COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                viewToRemove = componentView;
                break;
            }
            
        }
    }
    [self.componentViewArray removeObject:viewToRemove];
    // refesh the page to ensure it removes this new component
    [self.scrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    [self createUI];
}

-(void)removeCommentComponent {
    TOCComponentView *viewToRemove = nil;
    for (TOCComponentView *componentView in self.componentViewArray) {
        if ([componentView isKindOfClass: [TOCNotesComponentView class]]==YES) {
            TOCNotesComponentView *notes = (TOCNotesComponentView *)componentView;
            if (notes.type!=COMMENT_TYPE_WEIGHT_VARIANCE_REASON) {
                viewToRemove = componentView;
                break;
            }
            
        }
    }
    [self.componentViewArray removeObject:viewToRemove];
    // refesh the page to ensure it removes this new component
    [self.scrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    [self createUI];
}

#pragma mark - SlideDelegate

- (void)voidTransaction {
    
    TransactionWrapper* wrapper = [[TransactionWrapper alloc] initWithTransaction:self.transaction];
    [wrapper voidTransaction:self info:NULL];
}

#pragma mark - TransactionDelegate

- (void)transactionDidVoid {
    
    [self.navigationController dismissViewControllerAnimated:YES
                                                  completion:^
     {
         // for maintaing the the current animation and reloading if voided by search
         for (UIViewController *controller in self.navigationController.viewControllers) {
             if ([controller isKindOfClass:[ ListViewController class]]) {
                 //off the search
                 
                 [self.navigationController popToViewController:controller
                                                       animated:YES];
                 
                 break;
             }
         }
         //[self.navigationController popViewControllerAnimated:YES];
     }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelVoidProcess {
    
    //  [self.dockViewController.slideView togglePressed:nil];
}

- (IBAction)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - AlertViewDelegate
-(void) showWeightcomment
{
    Comment* comment = (Comment *)[[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    comment.text = @"Weight Variance reason";//WEIGHT_VARIANCE_COMMENT_HEADER;
    comment.transaction = self.transaction;
    comment.commentId = [[NSUUID UUID] UUIDString];
    comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
    [[DataEngine sharedInstance] saveContext];
    
    // ...and then tell the TOC (self) to register a Weight Variance Note Component
    TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
    tocNotesView.parentPageViewController = self;
    tocNotesView.shouldShowPopover = YES;
    tocNotesView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
    [self.componentViewArray addObject:tocNotesView];
    // [self.componentViewArray  insertObject:tocNotesView atIndex:5];
    
    // modify TOC Note component to allow it to "auto display" a popover
    
    // refesh the page to ensure it loads this new component
    [self.scrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    [self createUI];
    tocNotesView.shouldShowPopover = NO;
    
    
    
}
- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    
    NewAlertView *newAlertView = [[NewAlertView alloc] init];
//    newAlertView.frame = CGRectMake(floor((self.view.frame.size.width - newAlertView.frame.size.width) / 2),
//                                    floor((self.view.frame.size.height - newAlertView.frame.size.height) / 2),
//                                    newAlertView.frame.size.width,
//                                    274);
    newAlertView.title = @"Weight variance detected";
    newAlertView.text = @"";//@"Please enter a reason explaining this PTR's weight variance.";//self.alertMessage;
    newAlertView.singleLineLabel.hidden=NO;
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT) {
        newAlertView.singleLineLabel.text =@"Please enter a reason explaining this PIT's weight variance.";

    }
    else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PTR)
    {
        newAlertView.singleLineLabel.text =@"Please enter a reason explaining this PTR's weight variance.";

    }
    else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT)
    {
        newAlertView.singleLineLabel.text =@"Please enter a reason explaining this DOT's weight variance.";

    }
    
    
    newAlertView.singleLineLabel.frame = CGRectMake(40, 100, 395, 198);
    newAlertView.primaryButtonText = @"OK";
    newAlertView.delegate = self;
    newAlertView.singlelineforOkay.hidden=YES;
    newAlertView.imageViewSingleButton.backgroundColor =[UIColor blueColor];
    [newAlertView.singleButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    newAlertView.delegate = self;
    [newAlertView setUI];
    [[ModalViewController sharedModalViewController] showView:newAlertView];
}

#pragma mark - NewAlertViewDelegate

- (void)primaryAction:(UIButton *)sender {
    
    [[ModalViewController sharedModalViewController] hideView];
    Comment* comment = (Comment *)[[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    comment.text = WEIGHT_VARIANCE_COMMENT_HEADER;
    comment.transaction = self.transaction;
    comment.commentId = [[NSUUID UUID] UUIDString];
    comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
    [[DataEngine sharedInstance] saveContext];
    
    // ...and then tell the TOC (self) to register a Weight Variance Note Component
    TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
    tocNotesView.parentPageViewController = self;
    tocNotesView.shouldShowPopover = YES;
    tocNotesView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
    //[self.componentViewArray addObject:tocNotesView];
    if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT) {
        [self.componentViewArray  insertObject:tocNotesView atIndex:6];

        
    }
    else
    {
        [self.componentViewArray  insertObject:tocNotesView atIndex:5];

    }
    // modify TOC Note component to allow it to "auto display" a popover
    
    // refesh the page to ensure it loads this new component
    [self.scrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    [self createUI];
    tocNotesView.shouldShowPopover = NO;
    
}

- (void)alertView:(AlertViewController *)alertView
    primaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    // create a note...
    Comment* comment = (Comment *)[[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    comment.text = WEIGHT_VARIANCE_COMMENT_HEADER;
    comment.transaction = self.transaction;
    comment.commentId = [[NSUUID UUID] UUIDString];
    comment.user=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
    [[DataEngine sharedInstance] saveContext];
    
    // ...and then tell the TOC (self) to register a Weight Variance Note Component
    TOCNotesComponentView *tocNotesView = [[TOCNotesComponentView alloc] init];
    tocNotesView.parentPageViewController = self;
    tocNotesView.shouldShowPopover = YES;
    tocNotesView.type = COMMENT_TYPE_WEIGHT_VARIANCE_REASON;
    //[self.componentViewArray addObject:tocNotesView];
    [self.componentViewArray  insertObject:tocNotesView atIndex:5];
    // modify TOC Note component to allow it to "auto display" a popover
    
    // refesh the page to ensure it loads this new component
    [self.scrollView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    [self createUI];
    tocNotesView.shouldShowPopover = NO;
}

- (void)alertView:(AlertViewController *)alertView
  secondaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// SLIDE TO VOID
- (IBAction)slideToVoid:(UIPanGestureRecognizer *)sender {
    
    UIView* view = sender.view;
    CGPoint translation = [sender translationInView:view];
    float x = 0;
    
    if(sender.state == UIGestureRecognizerStateBegan)
    {
        self.startingOrigin = view.frame.origin;
    }
    else if(sender.state == UIGestureRecognizerStateChanged)
    {
        x = self.startingOrigin.x + translation.x;
        
        if(x > 0)
        {
            x = 0;
        }
        
        view.frame = CGRectMake(x, self.startingOrigin.y, view.frame.size.width, view.frame.size.height);
    }
    else if(sender.state == UIGestureRecognizerStateEnded)
    {
        [self returnSlideToVoid:view];
    }
}
- (IBAction)slideToVoidTouchUpInside:(UIButton *)sender {
    
    [self returnSlideToVoid:sender];
}
- (IBAction)voidTransaction:(UIButton *)sender {
    
    Transaction *transaction = [Utils getTransaction];
    
    if (transaction.outgoingSignatureName)
    {
        [Utils showMessage:self header:@"Warning" message:@"Transaction with outgoing signature cannot be voided" hideDelay:1];
        
        //[self hideAnimated:YES];
        
        return ;
    }
    
    
    [self voidTransaction];
}

- (IBAction)confirmSubmit:(id)sender {
    
    [self loadNextPageIfValid ];
}
#pragma mark - To set the expanad collapse
-(void)getComponentsView :(int)selectedViewindex
{
    for (int intCount =0; [self.componentViewArray count]>intCount; intCount++) {
        if (intCount == selectedViewindex) {}
        else
        {
            if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_TCR) {
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCOutgoingComponentView *ParticipantView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                    [ParticipantView setViewUI];
                }
                if (intCount == 3) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 4) {
                    TOCEligibilityComponentView *eligibiltyView = (TOCEligibilityComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [eligibiltyView setViewUI];
                }
                if (intCount == 5) {
                    TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [photoView setViewUI];
                }
                if (intCount == 6) {
                    TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [notesView setViewUI];
                }
            }
            else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_STC) {
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCAuthorizationComponentView *componentView =(TOCAuthorizationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [componentView setViewUI];
                }
                if (intCount == 3) {
                    TOCLocationComponentView *locationView =(TOCLocationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [locationView setViewUI];
                }
                if (intCount == 4) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 5) {
                    TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [photoView setViewUI];
                }
                if (intCount == 6) {
                    TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [notesView setViewUI];
                }
            }
            else if (self.transaction.transactionType.transactionTypeId==TRANSACTION_TYPE_ID_PTR){
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if([Utils getProcessorPtrOnly]==YES)
                {
                    if (intCount == 1) {
                        TOCOutgoingComponentView *outgoingView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [outgoingView setViewUI];
                    }
                    if (intCount == 2) {
                        TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                        [ParticipantView setViewUI];
                    }
                    
                }
                else
                {
                    if (intCount == 1) {
                        TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [ParticipantView setViewUI];
                    }
                    if (intCount == 2) {
                        TOCOutgoingComponentView *outgoingView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [outgoingView setViewUI];
                        
                    }
                    
                }
                if (intCount == 3) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 4) {
                    TOCScaleTicketComponentView *scaleTicketView =(TOCScaleTicketComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [scaleTicketView setviewUI];
                }
                if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
                    
                    if (intCount == 5) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                        
                    }
                    if (intCount == 6) {
                        TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [photoView setViewUI];
                    }
                    if (intCount == 7) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                    }
                    
                    
                }
                else
                {
                    if (intCount == 5) {
                        TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [photoView setViewUI];
                    }
                    if (intCount == 6) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                    }
                }
                
                
            }
            else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_HIT)
            {
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCOutgoingComponentView *ParticipantView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                    [ParticipantView setViewUI];
                }
                if (intCount == 3) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 4) {
                    TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [photoView setViewUI];
                }
                if (intCount == 5) {
                    TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [notesView setViewUI];
                }
                
            }
            else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_UCR)
            {
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCOutgoingComponentView *ParticipantView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                    [ParticipantView setViewUI];
                }
//                if (intCount == 3) {
//                    TOCAuthorizationComponentView *componentView =(TOCAuthorizationComponentView *)[self.componentViewArray objectAtIndex:intCount];
//                    [componentView setViewUI];
//                }
                if (intCount == 3) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 4) {
                    TOCEligibilityComponentView *eligibiltyView = (TOCEligibilityComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [eligibiltyView setViewUI];
                }
                if (intCount == 5) {
                    TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [photoView setViewUI];
                }
                if (intCount == 6) {
                    TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [notesView setViewUI];
                }
                
            }
            else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_DOT){
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCOutgoingComponentView *ParticipantView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                    [ParticipantView setViewUI];
                }
//                if (intCount == 3) {
//                    TOCAuthorizationComponentView *componentView =(TOCAuthorizationComponentView *)[self.componentViewArray objectAtIndex:intCount];
//                    [componentView setViewUI];
//                }
                if (intCount == 3) {
                    TOCTireCountComponentView *tireView = (TOCTireCountComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [tireView setViewUI];
                }
                if (intCount == 4) {
                    TOCEligibilityComponentView *eligibiltyView = (TOCEligibilityComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [eligibiltyView setViewUI];
                }
                if (intCount == 5) {
                    TOCScaleTicketComponentView *scaleTicketView =(TOCScaleTicketComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [scaleTicketView setviewUI];
                }
                 if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
                 
                 if (intCount == 6) {
                 TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                 [notesView setViewUI];
                 
                 }
                 if (intCount == 7) {
                 TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                 [photoView setViewUI];
                 }
                 if (intCount == 8) {
                 TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                 [notesView setViewUI];
                 }
                 
                 
                 }
                 else
                 {
                 if (intCount == 6) {
                 TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                 [photoView setViewUI];
                 }
                 if (intCount == 7) {
                 TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                 [notesView setViewUI];
                 }
                 }
                

                 
                /*
                
                
                if (intCount == 7) {
                    TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [photoView setViewUI];
                }
                
                if (intCount == 8) {
                    TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [notesView setViewUI];
                }
                 */
            
                
            }
            else if (self.transaction.transactionType.transactionTypeId == TRANSACTION_TYPE_ID_PIT){
                if (intCount == 0) {
                    TOCTransactionInformationComponentView *transactionView = (TOCTransactionInformationComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [transactionView setViewUI];
                }
                if (intCount == 1) {
                    
                    TOCOutgoingComponentView *ParticipantView = (TOCOutgoingComponentView *)[self.componentViewArray objectAtIndex:intCount];;
                    [ParticipantView setViewUI];
                }
                if (intCount == 2) {
                    TOCParticipantComponentView *ParticipantView = (TOCParticipantComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [ParticipantView setViewUI];
                    
                }
                
                
                if (intCount == 3) {
                    
                    TOCMaterialTypeComponentView *materialTypeView = (TOCMaterialTypeComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [materialTypeView setViewUI];
                }
                
                if (intCount == 4) {
                    TOCScaleTicketComponentView *scaleTicketView =(TOCScaleTicketComponentView *)[self.componentViewArray objectAtIndex:intCount];
                    [scaleTicketView setviewUI];
                }
                if ([Utils transactionHasWeightVarianceComment:self.transaction]==YES) {
                    
                    if (intCount == 5) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                        
                    }
                    if (intCount == 6) {
                        TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [photoView setViewUI];
                    }
                    if (intCount == 7) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                    }
                    
                    
                }
                else
                {
                    if (intCount == 5) {
                        TOCPhotoComponentView *photoView = (TOCPhotoComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [photoView setViewUI];
                    }
                    if (intCount == 6) {
                        TOCNotesComponentView *notesView = (TOCNotesComponentView *)[self.componentViewArray objectAtIndex:intCount];
                        [notesView setViewUI];
                    }
                }
                    
                    
                }
        }
        
        
    }
    
}

@end
