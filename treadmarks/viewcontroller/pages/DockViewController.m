//
//  DockViewController.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <Accelerate/Accelerate.h>
#import "DockViewController.h"
#import "SlideView.h"
#import "Utils.h"
#import "Location.h"
#import "DataEngine.h"
#import "Transaction.h"
#import "HelpAboutTableViewCell.h"
#import "ListViewController.h"
#import "LoginViewController.h"
#import "HelpTableViewCell.h"
#import "HelpViewController.h"
#import "TMSyncEngine.h"
#import "TMAFCLient.h"
#import "TMTransactionSyncStatus.h"
#import "Reachability.h"
#import "NSArray+NSArray_Slice.h"
#import "CDActivityIndicatorView.h"
#import "AppDelegate.h"
#import "btSimplePopUP.h"

#define OFFSET 80

@interface DockViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITapGestureRecognizer *singleTapGestureRecognizer;
    NSUInteger completedTransaction;
    NSUInteger nonSyncTransaction;
    NSUInteger totalTransactiontobeSync;
     NSUInteger syncToServerTransactionCount;
    NSUInteger chunkCycleCount;
    NSUInteger chunkCycle;
    float progressBarCount;
    CDActivityIndicatorView * activityIndicatorView;
    /*
	 For Phase 1  we have only 4 PDF for help. So we can use NS Array for that.
     but later we have to use DB coz of  diffrent type of helptopic i.e Manual,Video etc.
     will be used
     */
    NSArray *helpTopicArray;
   Reachability *reachability;
    BOOL animating;
    BOOL pullIsOn;
    BOOL disableNotification;
    NSTimer *cancelSyncTimer;
    float time;
    NSTimer *timer;
    

}


#pragma mark - IBOutlets
@property (weak, nonatomic) IBOutlet UIButton *wheelMenu;
- (IBAction)wheelMenuAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *dockView;
@property (weak, nonatomic) IBOutlet UIButton *backgroundButton;

@property (weak, nonatomic) IBOutlet UIButton *syncButton;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastSyncLabel;

@property (weak, nonatomic) IBOutlet UIView *backGroundView;
@property (weak, nonatomic) IBOutlet UILabel *syncServerLabel;

//VIEW'S IN DOCK
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dockViewBottom;
@property (weak, nonatomic) IBOutlet UIView *ContactView;
@property (weak, nonatomic) IBOutlet UIView *LogoutView;
@property (weak, nonatomic) IBOutlet UIView *helpView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UIView *syncInProgressView;
@property (weak, nonatomic) IBOutlet UIView *wifiAlertView;

//MY INFO VIEW'S LABEL'S

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;


@property (weak, nonatomic) IBOutlet UILabel *businessLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bizAddress2Label;
@property (weak, nonatomic) IBOutlet UILabel *bizAddress3Label;
@property (weak, nonatomic) IBOutlet UILabel *bizPhone;
- (IBAction)infoCancelButton:(id)sender;

//HELP VIEW
@property (weak, nonatomic) IBOutlet UITableView *helpTabelView;
- (IBAction)helpCancelButton:(id)sender;

//LOGOUT VIEW'S BUTTON
- (IBAction)logoutOkButton:(id)sender;
- (IBAction)logoutCancelButton:(id)sender;


//Cancel/Retry Sync
@property (strong, nonatomic) IBOutlet UIView *CancelSyncTimerView;
@property (weak, nonatomic) IBOutlet UILabel *cancellingSyncTimerLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *cancellinProgressBar;



//ERROR ALERT
@property (weak, nonatomic) IBOutlet UIView *errorAlertView;
@property (weak, nonatomic) IBOutlet UILabel *transactionLeftLabel;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertHeader;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertViewLineOne;
- (IBAction)errorOkButton:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *errorAlertVilewLineThird;
@property (strong, nonatomic) IBOutlet UILabel *ErrorBatteryLableOne;
@property (strong, nonatomic) IBOutlet UILabel *errorBatteryLabelTwo;

//SYNC IN PROGRESS VIEW
@property (weak, nonatomic) IBOutlet UIImageView *syncAnimImage;
@property (weak, nonatomic) IBOutlet UILabel *syncProgressCountLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *syncProgressBar;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncButton;
- (IBAction)syncCancelButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *syncViewHeaderLabel;
- (IBAction)cancelSyncYesButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *allTransactionSyncLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncYesButtonOutlet;
@property (weak, nonatomic) IBOutlet UILabel *recevingDataLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelSyncNoButtonOutlet;
- (IBAction)cancelSyncNoButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *allSyncOkButton;
- (IBAction)allSyncOK:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *cancelSyncConfirmLabel;
//WIFI ALERT
@property bool syncInProgress;
- (IBAction)wifiAlertSettingButton:(id)sender;
- (IBAction)wifiAlertOkButton:(id)sender;

@property(strong, nonatomic) btSimplePopUP *popUp;

- (IBAction)contact:(id)sender;


- (IBAction)BackButton:(id)sender;

@property (nonatomic, strong) NSArray *allTransactionsArray;
@property (nonatomic,strong) NSArray *transactionToBeSyncedArray;
@property (nonatomic, assign) BOOL canSync;
@property (nonatomic, strong) NSArray *syncDatesArray;

#pragma mark - Properties

@property int state;
@property BOOL firstAppearance;
@property NSUInteger transactionsToBeSyncedCount;

// performance testing:
@property (nonatomic, strong) NSDate *startTime;

// static sync
@property (nonatomic, strong) NSArray *classesToSync;
@property (nonatomic, assign) NSUInteger fetchTransactionsOffset;

@end

@implementation DockViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	
    if (self)
	{
		self.state = ON_SCREEN;
		self.firstAppearance = YES;
    }
	
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
	
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncServerError:) name:@"Sync Error"  object:nil];
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    /*   Not neededfor 1.4 release will keep it for future release
     ===============================================================
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryError:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
         self.syncInProgress=NO;
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryError:) name:UIDeviceBatteryLevelDidChangeNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncServerError:) name:@"Sync Error"  object:nil];
     =======================================================================
     */


    /*#if DEBUG
    self.syncServerLabel.hidden=NO;
    NSString *string = TM_WS_BASE_URL;
    if ([string containsString:@"staging"]) {
        self.syncServerLabel.textColor=[UIColor colorWithRed:0 green:255 blue:0 alpha:1.0];
       self.syncServerLabel.text=@"POINTING TO STAGING SAFE TO SYNC";
    } else {
        self.syncServerLabel.textColor=[UIColor colorWithRed:255 green:0 blue:0 alpha:1.0];
        self.syncServerLabel.text=@"POINTING TO PRODUCTION DO NOT TO SYNC";
    }
    
#endif
*/
    self.slideView = [[SlideView alloc] init];
	self.slideView.frame = CGRectMake(0,
									 350,
									  self.slideView.frame.size.width,
									  self.slideView.frame.size.height);
	self.slideView.backgroundView = self.backgroundButton;
   // [self.view addSubview:self.slideView];
    
	//[self.view insertSubview:self.slideView belowSubview:self.dockView];
	NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"d"];
	self.dayLabel.text = [formatter stringFromDate:[NSDate date]];
	
	self.lastSyncLabel.text = @"Never";
    self.canSync = YES;
    self.transactionsToBeSyncedCount = 0;
    
    singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
								  initWithTarget:self
								  action:@selector(handleSingleTap:)];
    
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [singleTapGestureRecognizer  setCancelsTouchesInView:NO];
    [self.backGroundView addGestureRecognizer:singleTapGestureRecognizer];
    
    
    
    
    //change the topics for processor
    
    if([Utils getProcessorDidLogin]==YES)
    {
    helpTopicArray= [[NSArray alloc] initWithObjects:
					 @"About",
                     @"iPad Overview",
                     @"How to complete a PTR transaction", nil];
        
    }
    else
    {
        helpTopicArray= [[NSArray alloc] initWithObjects:
                         @"About",
                         @"iPad Overview",
                         @"How to complete a TCR transaction",
                         @"How to complete a STC transaction",
                         nil];

    }
    self.dockView.userInteractionEnabled=YES;
    self.backGroundView.userInteractionEnabled=YES;
    self.dockView.alpha=1;
    self.syncDatesArray = nil;
   /* [[TMAFCLient sharedClient] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
		if (status == AFNetworkReachabilityStatusNotReachable) {
			// Not reachable
			self.canSync = NO;
		}
		else if (status == AFNetworkReachabilityStatusReachableViaWiFi) {
			// On wifi
			self.canSync = YES;
		}
	}];*/

   /*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkStatusChanged:) name:kReachabilityChangedNotification object:nil];
    
    reachability = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    [reachability startNotifier];
    
    //Format the Date for Last sync
    
    */
    
    
}


- (void)networkStatusChanged:(NSNotification*)note
{
    Reachability* reach = [note object];
    NetworkStatus networkStatus = reach.currentReachabilityStatus;
    NSString* textToShow;
    if (networkStatus == NotReachable)
    {
        textToShow = @"Network problems.  No Syncing possible";
        //NSLog(@"NETWORK PROBLEM. NO SYNC POSSIBLE");
        self.canSync = NO;
        [self wifiError];
    }
    if (networkStatus==ReachableViaWiFi||networkStatus==ReachableViaWWAN)
    {
        textToShow= @"Network found.  Try sync";
        //NSLog(@"NETWORK FOUND. TRY SYNC");
        self.canSync = YES;
        [self wifiAlertOkButton:self];
    }
	// UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Status" message:textToShow delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //[alertView show];}
}

- (void) HideDockView :(BOOL)on
{
 if (on)
 {[self hideDock];}
}


- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	}
-(void)viewDidAppear:(BOOL)animated
{
  
    if (self.firstAppearance )
	{
        self.firstAppearance = NO;
        if(self.canSync==YES)
        {
            self.dockView.userInteractionEnabled=NO;
            [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(hideDock) userInfo:nil repeats:NO];
            // [self performSelector:@selector(hideDock) withObject:nil afterDelay:1.5];
        }
        else
        {
            self.backGroundView .hidden=NO;
            self.slideView.userInteractionEnabled=NO;
            [self.slideView hideAnimated:YES];
            self.slideView.alpha=0.45;

            //[self hideDock];
            [self wifiError];
        }
    }
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];

    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
    }
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
	
    self.backGroundView.hidden=YES;
    
    [self hideDock];
    
}
#pragma mark - Accessors

- (void)setDockViewHidden:(BOOL)dockViewHidden {
	
	self.dockView.hidden = dockViewHidden;
}
-(void)hideAllSubview
{
    
    self.dockView.userInteractionEnabled=YES;
    singleTapGestureRecognizer.enabled=YES;
    self.dockView.alpha=1;
    self.backGroundView.userInteractionEnabled=YES;
    self.ContactView.hidden=YES;
    self.LogoutView.hidden=YES;
    self.helpView.hidden=YES;
    self.infoView.hidden=YES;
    
    
    self.syncInProgressView.hidden=YES;
    self.wifiAlertView.hidden=YES;
    self.errorAlertView.hidden=YES;
   // self.dockView.userInteractionEnabled=YES;
    singleTapGestureRecognizer.enabled=YES;
    self.dockView.alpha=1;
    [activityIndicatorView stopAnimating];
    self.CancelSyncTimerView.hidden=YES;
    
}
#pragma mark - IBActions

- (IBAction)backgroundButtonTouchUpInside:(UIButton *)sender {
	
	[self.slideView hideAnimated:YES];
}
- (IBAction)toggle:(UIButton *)sender {
	
	if (self.state == ON_SCREEN)
	{
		[self hideDock];
	}
	else
	{
		
        self.state = ON_SCREEN;
        self.backGroundView .hidden=NO;
        self.slideView.userInteractionEnabled=NO;
        [self.slideView hideAnimated:YES];
        self.slideView.alpha=0.45;
		[self.view layoutIfNeeded];
		[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^
		 {
			 self.dockViewBottom.constant = 0;
			 
			 [self.view layoutIfNeeded];
			 
		 }
						 completion:^(BOOL finished)
		 {
			 if (finished)
			 {
				 UIImage* image = [UIImage imageNamed:@"dock-arrow-dn"];
				 [self.toggleButton setImage:image forState:UIControlStateNormal];
				 [self.toggleButton setImage:image forState:UIControlStateHighlighted];
			 }
		 }];
	}
}
- (IBAction)myInfo:(UIButton *)sender {
    
    
	[self hideAllSubview];
    self.infoView.hidden=NO;
    [self populateMyInfo];
	
}
- (IBAction)logout:(UIButton *)sender {
	[self hideAllSubview];
    self.LogoutView.hidden=NO;
	self.dockView.userInteractionEnabled=NO;
    self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;
	
	
}
- (IBAction)help:(UIButton *)sender {
    UIDevice *myDevice = [UIDevice currentDevice];
    //[myDevice setBatteryMonitoringEnabled:YES];
    double batLeft = (float)[myDevice batteryLevel];
    NSLog(@"BAT LEFT %f",batLeft);
    [self hideAllSubview];
       self.helpView.hidden=NO;
    
	
	
}
#pragma mark - Sync Button UI


-(void)allTransactionSyncUI
{
    [activityIndicatorView stopAnimating];
    self.syncProgressBar.hidden=YES;
    self.syncProgressCountLabel.hidden=YES;
    self.cancelSyncButton.hidden=YES;
    self.allTransactionSyncLabel.hidden=NO;
    self.allSyncOkButton.hidden=NO;
    self.syncViewHeaderLabel.text=@"Sync Alert";
    self.syncInProgressView.hidden=NO;
     [self stopSpin];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    /* layout slideview for active/deactive
        Registrant *currentRegistrant = [Utils getIncomingRegistrant];
        BOOL activeRegistrant = [currentRegistrant.isActive integerValue]==1 ? YES:NO;
        self.slideView.allowNewTransactions = activeRegistrant;
        [self.slideView  layoutSubviews];*/
    
   //Enable the timeout.
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

-(void)pullUI :(BOOL)stop
{
    //stop the spill for pull
    //This  has be fixed
    [activityIndicatorView stopAnimating];
   // [self stopSpin];
    if (!stop)
    {
   
    self.recevingDataLabel.hidden      = NO;
    self.recevingDataLabel.alpha       = 1;
        
    self.syncProgressBar.hidden        = YES;
    self.syncProgressCountLabel.hidden = YES;
    //[self.recevingDataLabel setAlpha:1.0];
        }
    if(stop)
    {
       [self cancelSyncUIChange:NO];
        [self.recevingDataLabel setAlpha:0.0];
        self.recevingDataLabel.hidden      = YES;
        pullIsOn=NO;
        
        [self allTransactionSyncUI];
    }

}


-(void) cancelSyncUIChange :(bool)status
{
    self.syncProgressBar.hidden=status;
    self.syncProgressCountLabel.hidden=status;
    self.cancelSyncButton.hidden=status;
    self.cancelSyncConfirmLabel.hidden=status==YES?NO:YES;
    self.cancelSyncNoButtonOutlet.hidden=status==YES?NO:YES;
    self.cancelSyncYesButtonOutlet.hidden=status==YES?NO:YES;
}

-(void)wifiError
{
    //Call when wifi is not availlable
   
    //tbd when wifi is off on listview disable the slideview dock
    [self stopSpin];
    [self hideAllSubview];
    self.wifiAlertView.hidden=NO;
	self.dockView.userInteractionEnabled=NO;
    self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;
    self.backGroundView.hidden=NO;

    
}
-(void)batteryError:(NSNotification *)notification
{
    UIDevice *myDevice = [UIDevice currentDevice];
    //[myDevice setBatteryMonitoringEnabled:YES];
    double batLeft = (float)[myDevice batteryLevel];
    UIDeviceBatteryState batteryState = [UIDevice currentDevice].batteryState;
   // if (self.syncInProgress){
        //if less than 15% and not charging  Stop the Sync
        if ( batLeft<0.41000 && batteryState ==UIDeviceBatteryStateUnplugged )
        {
        //stop sync
        [[TMSyncEngine sharedEngine] cancel];
        [self batteryErrorUI];
        }
        //if less than 16% and unpulug  Stop the Sync
    if (batteryState ==UIDeviceBatteryStateUnplugged )
    {

        if(batLeft<0.41000)
        {//stop sync
        [[TMSyncEngine sharedEngine] cancel];
        [self batteryErrorUI];
        }
    }
   // }

}
-(void)syncServerError: (NSNotification *)notification
{
    [[TMSyncEngine sharedEngine] cancel];

    if ([[notification name] isEqualToString:@"Sync Error"]) {
       if (disableNotification)
       {//Do not show Sync error if cancel by user
          
           return;
       }
        else
        {[self syncErrorUI];}

    }
}

-(void)batteryErrorUI
{
    /*Title-* Low Battery
     *Message Body-*  Sync cannot be performed if your battery is less than 15%. Please plug in your device in order to Sync.*/

    [self hideAllSubview];

    [self stopSpin];
    [[TMSyncEngine sharedEngine] cancel];
    [self stopSpin];

    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];

    self.backGroundView.hidden=NO;
    self.errorAlertViewLineOne.hidden=YES;
    self.errorAlertVilewLineThird.hidden=YES;
    self.transactionLeftLabel.hidden=YES;
    self.ErrorBatteryLableOne.hidden=NO;
    self.errorBatteryLabelTwo.hidden=NO;

    self.errorAlertHeader.text=@"Low Battery";

    self.errorAlertView.hidden=NO;
    self.dockView.userInteractionEnabled=NO;
    self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;


}


-(void)syncErrorUI
{
    //if any error stop sync
    [self hideAllSubview];

    [self stopSpin];
    [[TMSyncEngine sharedEngine] cancel];
    [self stopSpin];
    
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
     self.errorAlertHeader.text=@"Sync Alert";
    self.backGroundView.hidden=NO;
    self.errorAlertViewLineOne.hidden=NO;
    self.errorAlertVilewLineThird.hidden=NO;
    self.transactionLeftLabel.hidden=NO;
    self.ErrorBatteryLableOne.hidden=YES;
    self.errorBatteryLabelTwo.hidden=YES;
    self.transactionLeftLabel.text=syncToServerTransactionCount==0?@"No transaction uploaded.":([NSString stringWithFormat:@"Transaction %lu of %lu uploaded", (unsigned long)syncToServerTransactionCount,(unsigned long)totalTransactiontobeSync ]);
    self.errorAlertView.hidden=NO;
	self.dockView.userInteractionEnabled=NO;
    self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;
    
}
-(void)hideCancelingView
{
    [self stopSpin];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
        self.CancelSyncTimerView.hidden=YES;
        self.dockView.userInteractionEnabled=YES;
        singleTapGestureRecognizer.enabled=YES;
        [self hideDock];
}

- (IBAction)allSyncOK:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    [self cancelSyncUIChange:NO];
    self.allSyncOkButton.hidden=YES;
    self.allTransactionSyncLabel.hidden=YES;
    self.syncViewHeaderLabel.text=@"Sync in Progress";
    [self hideAllSubview];
    self.dockView.userInteractionEnabled=YES;
    singleTapGestureRecognizer.enabled=YES;
    [self hideDock];
}

// SYNC UI BUTTONS
- (IBAction)syncCancelButton:(id)sender {
    //Do we have to stop progressbar and sync?
    [self cancelSyncUIChange:YES];
     [activityIndicatorView stopAnimating];
   // [self stopSpin];
    if(pullIsOn)
    {
            self.cancelSyncConfirmLabel.hidden=NO;
        self.recevingDataLabel.alpha=0;
        
    }
}
- (IBAction)errorOkButton:(id)sender {
    [self hideAllSubview];
    if(!self.syncInProgress)
    {
        //Hide the error coz  user has not done the sync
        [self stopSpin];
       // [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
        self.CancelSyncTimerView.hidden=YES;
        self.dockView.userInteractionEnabled=YES;
        singleTapGestureRecognizer.enabled=YES;
        [self hideDock];
        return;
    }
    [[TMSyncEngine sharedEngine] cancel];
    self.dockView.userInteractionEnabled=NO;
    //self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;
    self.CancelSyncTimerView.hidden=NO;
    //NSTimer *autoTimer;
    
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(hideCancelingView) userInfo:nil repeats:NO];
    self.cancellinProgressBar.progress = 0;
    // Put progress bar on the  cancelling
    time = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.1f
                                             target: self
                                           selector: @selector(updateTimer)
                                           userInfo: nil
                                            repeats: YES];

}
- (IBAction)cancelSyncYesButton:(id)sender {
     [self stopSpin];
    disableNotification=YES;
    [activityIndicatorView stopAnimating];
    [[TMSyncEngine sharedEngine] cancel];
    //UI CHANGE
    //Change the sync date
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];
    
    [self cancelSyncUIChange:NO];
    self.recevingDataLabel.alpha=0;
    self.syncInProgressView.hidden=YES;
    [self hideAllSubview];
    self.CancelSyncTimerView.hidden=NO;
    
    
    
    self.dockView.userInteractionEnabled=NO;
    
    singleTapGestureRecognizer.enabled=NO;

    self.cancellinProgressBar.progress  = 0;
    
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(hideCancelingView) userInfo:nil repeats:NO];
        //[self hideDock];
    self.cancellinProgressBar.progress = 0;
    time = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.1f
                                             target: self
                                           selector: @selector(updateTimer)
                                           userInfo: nil
                                            repeats: YES];
}
    -(void) updateTimer
    {
        if(time == 1)
        {
            //Invalidate timer when time reaches 0
            [timer invalidate];
        }
        else
        {
            time += 0.005;
           self.cancellinProgressBar.progress = time;
        }
    }


- (IBAction)cancelSyncNoButton:(id)sender {
    [activityIndicatorView startAnimating];
    //[self stopSpin];
    [self cancelSyncUIChange:NO];
    if(pullIsOn)
    {
        self.syncProgressBar.hidden=YES;
        self.syncProgressCountLabel.hidden=YES;
        
        self.cancelSyncConfirmLabel.hidden=YES;
        self.recevingDataLabel.alpha=1;
        
    }//status==YES?YES:NO;
    
}

- (IBAction)wifiAlertSettingButton:(id)sender {
    //For Phase 1 setting button also  behave as ok button
    [self wifiAlertOkButton:self];
    
}

- (IBAction)wifiAlertOkButton:(id)sender {
    [self hideAllSubview];
    self.dockView.userInteractionEnabled=YES;
    singleTapGestureRecognizer.enabled=YES;
    [self hideDock];
}

-(void)synInProgress
{
    [self hideAllSubview];
    self.syncInProgressView.hidden=NO;
	self.dockView.userInteractionEnabled=NO;
    self.dockView.alpha=0.45;
    singleTapGestureRecognizer.enabled=NO;
    
}
//Spining icon
// Magic. Do not touch.

- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.5f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         self.syncAnimImage.transform = CGAffineTransformRotate(self.syncAnimImage.transform, M_PI / 2);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}

- (void) startSpin {
   // self.syncAnimImage.hidden=NO;
 self.syncAnimImage.alpha=1;
   [self.syncButton setImage:[UIImage imageNamed:@"dock-sync-blank.png"] forState:UIControlStateNormal];
    if (!animating) {
        animating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }
   
    
}

- (void) stopSpin {
    self.syncAnimImage.alpha=0;
    [self.syncButton setImage:[UIImage imageNamed:@"dock-icon-sync"] forState:UIControlStateNormal];
    animating = NO;
}

#pragma mark - Sync Process
- (IBAction)sync:(UIButton *)sender {
    [self hideAllSubview];
    //Enable notification when sync start!
    disableNotification=NO;
   //disable the timeout


    UIDevice *myDevice = [UIDevice currentDevice];
   // [myDevice setBatteryMonitoringEnabled:YES];
    double batLeft = (float)[myDevice batteryLevel];

    if ( batLeft<0.15000) {
        self.syncInProgress=NO;
          [self batteryErrorUI];
        return;

        //0.160000
    }

    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    // catch any corrupt scale ticket records caused by lock screen
    [Utils validateScaleTickets];
    
    if (! self.canSync)
    {
        [activityIndicatorView stopAnimating];
        [self wifiError];
        return;
    }
    chunkCycleCount=0;
    progressBarCount=0;
    self.syncProgressBar.progress=0;
     // update  UI .
    [self startSpin];
    self.syncInProgress=YES;
    activityIndicatorView = [[CDActivityIndicatorView alloc] initWithImage:[UIImage imageNamed:@"slide-icon-tire.png"]];
    
    activityIndicatorView.center = self.view.center;
    
    [self.view addSubview:activityIndicatorView];
    
    [activityIndicatorView startAnimating];
    
    self.syncInProgressView.hidden=NO;
      [self startSpin];       
    self.dockView.userInteractionEnabled=NO;
   // self.cancelSyncButton.hidden=YES;
    singleTapGestureRecognizer.enabled=NO;
            
    self.dockView.alpha=1;//
            
    syncToServerTransactionCount=0;
            
    self.syncProgressCountLabel.text=@"Preparing data for upload";
    
   // self.syncProgressBar.alpha=0;

    self.fetchTransactionsOffset = 0;
    
    self.allTransactionsArray = nil;
    
    //Put in Array copy in seprate thread.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
       [self startSpin];
        
        self.allTransactionsArray = [[DataEngine sharedInstance] transactionsNotSynced];
        totalTransactiontobeSync  = [self.allTransactionsArray count];
        chunkCycle=totalTransactiontobeSync/TRANSACTION_FETCH_LIMIT_SIZE;
      // dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self syncTransactions];
 
        //       });
    });

}

-(void)doPull
{
   
    
     self.recevingDataLabel.hidden      = NO;
    self.recevingDataLabel.alpha        =1;
    [activityIndicatorView stopAnimating ];
    [self synInProgress];
    [self pullUI:NO];
    //self.syncProgressCountLabel.text=@"Preparing data for download";
    //[self synInProgress];
    //[self pullUI:NO];
    //[[TMSyncEngine sharedEngine ]pullData ];
    
    //[self allTransactionSyncUI];
    [TMSyncEngine sharedEngine].delegate = self;
    [[TMSyncEngine sharedEngine] pullData];
    return;
 
}

-(void)syncTransactions {
    //CHANGE COUPLE OF UI VALUE
    [self startSpin];
    //self.cancelSyncButton.hidden=NO;
    if (totalTransactiontobeSync!=0)
    {
        
        
        progressBarCount=(100/(float)totalTransactiontobeSync);
        progressBarCount=progressBarCount/100;
    }
    else
    {
        [self doPull];
    }

    self.transactionToBeSyncedArray = nil;
    // //NSLog(@"Syncing from %d to %d transaction set", self.fetchTransactionsOffset,self.fetchTransactionsOffset+TRANSACTION_FETCH_LIMIT_SIZE);
    self.transactionToBeSyncedArray = [self.allTransactionsArray arrayBySlicingFrom:self.fetchTransactionsOffset to:self.fetchTransactionsOffset+TRANSACTION_FETCH_LIMIT_SIZE];
    self.transactionsToBeSyncedCount = [self.transactionToBeSyncedArray count];
    self.fetchTransactionsOffset += TRANSACTION_FETCH_LIMIT_SIZE;
    //NSLog(@"fech %lu",(unsigned long)self.fetchTransactionsOffset);
    //Show Progress View
   // self.syncProgressCountLabel.text=([NSString stringWithFormat:@"Transaction %lu of %lu ",(unsigned long)syncToServerTransactionCount,(unsigned long)[self.transactionToBeSyncedArray count ]]);
    self.syncInProgressView.hidden=NO;
    
    self.startTime = [NSDate date];
    [TMSyncEngine sharedEngine].delegate = self;
    
    for (Transaction *transaction in self.transactionToBeSyncedArray)
    {
       [self startSpin];
        if (self.canSync==NO)
        {
            [self wifiError];
            break;
        }
        [[TMSyncEngine sharedEngine] uploadTransaction:transaction];
    }
}


#pragma mark  - TMSyncEngineDelegate

-(void) transactionDidUpload:(Transaction *)transaction withStatuses:(TMTransactionSyncStatus *)transactionStatuses {
    
    //Change UI 
    [activityIndicatorView stopAnimating ];
    //self.syncProgressBar.alpha=1;
    
   /* NSDate *dateB = [NSDate date];
    NSDate *dateA = self.startTime;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	
    NSDateComponents *components = [calendar components:NSMinuteCalendarUnit|NSSecondCalendarUnit
                                               fromDate:dateA
                                                 toDate:dateB
                                                options:0];
	 */
//    //NSLog(@"Transaction %@ Succeeded. elapsed time for upload: %li minutes and %li seconds.", transaction.friendlyId, (long)components.minute, (long)components.second);
    NSDate*lastsyncDate = transaction.syncDate;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d MMM  hh:mm a"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:[dateFormat stringFromDate:lastsyncDate] forKey:@"lastPushDate"];
    [standardUserDefaults synchronize];
    
  //  self.syncButton.enabled=YES;
    
    self.transactionsToBeSyncedCount--;
    syncToServerTransactionCount++;

    //Animate the blue bar
    self.syncProgressBar.progress+=progressBarCount;

    self.syncProgressCountLabel.text=([NSString stringWithFormat:@"Transaction %lu of %lu ", (unsigned long)syncToServerTransactionCount,(unsigned long)totalTransactiontobeSync ]);
    
    //Once all the transaction  sync using the  chunking Do Pull!
    if(syncToServerTransactionCount==totalTransactiontobeSync)
    {
        
         // All Trnsaction synced. Notify and Do Pull
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
        self.lastSyncLabel.text = [standardUserDefaults stringForKey:@"lastPushDate"];

       /* // When Pull is on remove the alltarnsactionsyncUI  & Stopspin call
        [self allTransactionSyncUI];
        [self stopSpin];
        
       
        //Show last sync date
        
        // [date descriptionWithCalendarFormat:nil timeZone:tz locale:nil]);
        // self.lastSyncLabel.text=[dateFormat stringFromDate:lastsyncDate];
       */
        //Show Pull message
        pullIsOn=YES;
        
        [self pullUI:NO];
        [[TMSyncEngine sharedEngine ]pullData ];
        return;
       
         }
   // CHECK IF IT NEED THE NEXT CHUNK
    if (self.transactionsToBeSyncedCount==0)
    
    {
//         //NSLog(@"Transaction set with %@ completed. elapsed time: %li minutes and %li seconds.", transaction.friendlyId, (long)components.minute, (long)components.second);

        [self syncTransactions];
    }
    
   }



-(void) transactionUploadDidFail:(Transaction *)transaction withError:(NSError *)error {
   
    NSString *msg = [error localizedDescription];
    NSLog(@"transaction %@ upload did fail with error: %@",transaction.friendlyId, msg);
    [[TMSyncEngine sharedEngine]cancel];
  
   // self.syncButton.enabled=YES;
   // [self wifiError];
    
    //self.transactionsToBeSyncedCount--;
    // TBD: need to tell the ListViewController to refresh to see the changed sync status icons
    
}

-(void) syncDidComplete:(NSUInteger)syncedObjectCount withError:(NSError *)error {
    self.syncInProgress=NO;
    if (error != nil) {
        NSString *msg = [error localizedDescription];
        NSLog(@"Sync (pull) completed with error: %@",msg);
        //[self wifiError];
        [self syncErrorUI];
         self.transactionLeftLabel.text=@"";
        return;
    }
    else {
        // Pull  finished ! change UI
        [self pullUI:YES];
        [self stopSpin];
        // Add notification  for the Battery staus changed
        //NSLog(@"Sync (pull) completed for %lu entities",(unsigned long)syncedObjectCount);
        // now store off the new dates
        for (NSDictionary *dict in self.syncDatesArray){
            NSDate *syncDate = [dict valueForKey:@"lastUpdated"];
            NSString *className = [dict valueForKey:@"entityName"];
            [[TMSyncEngine sharedEngine] setLastUpdatedDate:syncDate forClass:className];
        }
        self.syncDatesArray = nil;
      
    }
}

// Compare these dates with stored values, and initiate pull request if required
-(void) syncDates:(NSArray *)dateArray {
    // check at last transaction completed time
    if (self.transactionsToBeSyncedCount == 0) {
        NSMutableArray *classesToSync  = [NSMutableArray array];
        for (NSDictionary *dict in dateArray){
            NSDate *syncDate = [dict valueForKey:@"lastUpdated"];
            NSString *className = [dict valueForKey:@"entityName"];
            if ([className isEqualToString:@"PhotoPreSelectComment"]) {
                className = @"PhotoPreselectComment";
            }
            NSDate *currentDate = [[TMSyncEngine sharedEngine] getLastUpdatedDate:className];
            if ( [syncDate compare:currentDate]==NSOrderedDescending) {
                Class theClass = NSClassFromString(className);
                [classesToSync addObject:theClass];
            }
        }
        
        self.classesToSync = [self orderClassesToSync:classesToSync];
        
        // now store off the new dates in tempArray
        self.syncDatesArray = dateArray;
       
               // initiate sync if necessary
        // PUll if OFF for testing
        if ([self.classesToSync count] != 0) {
            
            [[TMSyncEngine sharedEngine] registerNSManagedObjectClassesToSync:self.classesToSync];
            [TMSyncEngine sharedEngine].delegate = self;
         
            [[TMSyncEngine sharedEngine] startSync];
        }
        else
        {
            [self pullUI:YES];
            [self stopSpin];
           
        }
    
    
    }
}

-(NSArray *) orderClassesToSync:(NSArray *)classesToSync {
    NSMutableArray *array = [NSMutableArray array];
    if ([classesToSync containsObject:[Location class]]==YES) {
        [array addObject:[Location class]];
    }
    if ([classesToSync containsObject:[Registrant class]]==YES) {
        [array addObject:[Registrant class]];
    }
    for (Class aClass in classesToSync) {
        if (aClass != [Location class] && aClass != [Registrant class]) {
            [array addObject:aClass];
        }
    }

    return [array copy];
}

#pragma mark - Methods

- (void)hideDock {
    
    self.state = OFF_SCREEN;
	self.backGroundView.hidden=YES;
	[self hideAllSubview];
    self.slideView.userInteractionEnabled=YES;
   
    [self.view layoutIfNeeded];
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^
	 {
		 //self.dockViewBottom.constant = -OFFSET;
		 
		 [self.view layoutIfNeeded];
		 /*
		  self.dockView.frame = CGRectMake(self.dockView.frame.origin.x,
		  self.dockView.frame.origin.y + OFFSET,
		  self.dockView.frame.size.width,
		  self.dockView.frame.size.height);
		  */
	 }
					 completion:^(BOOL finished)
	 {
		 if (finished)
		 {
			 UIImage* image = [UIImage imageNamed:@"dock-arrow-up"];
			 [self.toggleButton setImage:image forState:UIControlStateNormal];
			 [self.toggleButton setImage:image forState:UIControlStateHighlighted];
		 }
	 }];
    self.slideView.userInteractionEnabled=YES;
    self.slideView.alpha=1;
    self.dockView.userInteractionEnabled=YES;
    
    singleTapGestureRecognizer.enabled=YES;
    self.dockView.alpha=1;
   // [cancelSyncTimer invalidate];

}
- (IBAction)BackButton:(id)sender {
    self.ContactView.hidden=YES;
}
- (IBAction)logoutOkButton:(id)sender {

	[((AppDelegate *)[[UIApplication sharedApplication] delegate]) logout:YES];
}

- (IBAction)logoutCancelButton:(id)sender {
  
    self.LogoutView.hidden=YES;

    
    self.dockView.userInteractionEnabled=YES;
        singleTapGestureRecognizer.enabled=YES;
    self.dockView.alpha=1;
}


- (IBAction)contact:(id)sender {
    [self hideAllSubview];
    self.ContactView.hidden=NO;
	
}


- (IBAction)helpCancelButton:(id)sender {
    self.helpView.hidden =YES;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
	return helpTopicArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	/// First row is handled differently
	if (indexPath.row == 0)
	{
		NSString* identifier = NSStringFromClass([HelpAboutTableViewCell class]);
		UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		if (cell == nil)
		{
			[tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
			
			cell = [tableView dequeueReusableCellWithIdentifier:identifier];
		}
		
		return cell;
	}

	NSString* identifier = @"HelpTableViewCell";
	HelpTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	
	if (cell == NULL)
	{
		[tableView registerNib:[UINib nibWithNibName:identifier bundle:NULL] forCellReuseIdentifier:identifier];
		
		cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	}
	
	cell.helpHeaderLabel.text=@"Manual";
	cell.helpTopicLabel.text=[helpTopicArray objectAtIndex:indexPath.row];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.row == 0)
	{
		return 113;
	}
	else
	{
		return tableView.rowHeight;
	}
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.row == 0)
	{
		/// Do nothing for About
	}
	else
	{
		
		// [tableView deselectRowAtIndexPath:indexPath animated:YES];
		HelpViewController *helpViewController=[[HelpViewController alloc]init];
		helpViewController.helpPdfFileName= [helpTopicArray objectAtIndex:indexPath.row];
		//[self.navigationController presentViewController:helpViewController animated:YES completion:NULL];
		[self.navigationController pushViewController:helpViewController animated:YES];
	}
}

-(void)populateMyInfo
{
    User        *userInfo=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
    
  
    Registrant  *registrantInfo=[[DataEngine sharedInstance]  registrantForId:[Utils getRegistrantId]];
    Location    *locationInfo=registrantInfo.location;
	
    self.nameLabel.text=    userInfo.name;
    //NSLog(@"%@",self.nameLabel.text);
    self.emailLabel.text = userInfo.email==NULL?@"":userInfo.email;
    self.businessLabel.text=registrantInfo.businessName;
    self.registrantIdLabel.text=[registrantInfo.registrationNumber stringValue];
    self.bizPhone.text=locationInfo.phone==NULL?@"":locationInfo.phone;
    
    self.addressLabel.text=locationInfo.address1==NULL?@"":locationInfo.address1 ;
    self.bizAddress2Label.text=[[NSString stringWithFormat:@ "%@ %@ %@",locationInfo.address2==NULL?@"":locationInfo.address2,
								 locationInfo.address3==NULL?@"":locationInfo.address3,
								 locationInfo.city==NULL?@"":locationInfo.city ] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.bizAddress3Label.text=[[NSString stringWithFormat:@ "%@ %@",locationInfo.province==NULL?@"":locationInfo.province,
								 locationInfo.postalCode==NULL?@"":locationInfo.postalCode
								 ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
}
-(void)progressStatus
{

}
- (IBAction)infoCancelButton:(id)sender {
    self.infoView.hidden=YES;
}
- (IBAction)wheelMenuAction:(id)sender {
   [self hideAllSubview];
    self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[
                                                      [UIImage imageNamed:@"settings.png"],
                                                     [UIImage imageNamed:@"settings.png"],
                                                      [UIImage imageNamed:@"settings.png"],
                                                      
                                                      ]
                                          andTitles:    @[
                                                          @"TCR", @"PTR",@"STC"
                                                          ]
             
                                     andActionArray:@[
                                                      ^{
        
        __block UIViewController *temp = [[UIViewController alloc]init];
        temp.view.backgroundColor = [UIColor blueColor];
        [self.navigationController presentViewController:temp
                                                animated:YES
                                              completion:^{
                                                  
                                                  [temp dismissViewControllerAnimated:YES completion:nil];
                                              }];
    },
                                                       ^{
        [self showAlert];
    },
                                                       ^{
        [self showAlert];
    },
                                                       
                                                       ^{
        [self showAlert];
    }]
                                addToViewController:self];
    [self.view addSubview:self.popUp];
    [self.popUp setPopUpStyle:BTPopUpStyleDefault];
    [self.popUp setPopUpBorderStyle:BTPopUpBorderStyleDefaultNone];
    //[popUp setPopUpBackgroundColor:[UIColor colorWithRed:0.1 green:0.2 blue:0.6 alpha:0.7]];
    [self.popUp show:BTPopUPAnimateNone];

}




-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"PopItem" message:@" iAM from Block" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
    [alert show];
}


@end
