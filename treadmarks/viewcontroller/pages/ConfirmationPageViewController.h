//
//  ConfirmationPageViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-12.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "PageViewController.h"
#import "AlertViewController.h"

@class AlertViewController;

@interface ConfirmationPageViewController : PageViewController

@end
