//
//  ParticipantComponentHelperViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 10/29/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ParticipantComponentHelperViewController.h"
#import "Utils.h"
#import "DataEngine.h"
#import "ParticipantComponentView.h"
#import "Constants.h"

@interface ParticipantComponentHelperViewController ()

@property (weak,   nonatomic) IBOutlet UIImageView *shutterImageView;

@end

@implementation ParticipantComponentHelperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self performSelector:@selector(addZbar) withObject:self afterDelay:.2];
}

-(void)addZbar{
    //present a barcode reader that scans from the camera feed
    /*
    self.zBarReaderViewController = [CaprisZBarReaderViewController new];
    self.zBarReaderViewController.readerDelegate = self;
    self.zBarReaderViewController.supportedOrientationsMask = ZBarOrientationMaskAll;
    self.zBarReaderViewController.showsZBarControls = NO;
    ZBarImageScanner *scanner = self.zBarReaderViewController.scanner;
    
    //disable rarely used I2/5 to improve performance
    [scanner setSymbology:ZBAR_I25 config:ZBAR_CFG_ENABLE to:0];
    
    CGSize shutterSize = self.shutterImageView.frame.size;
    self.zBarReaderViewController.view.frame = CGRectMake(0, 0, shutterSize.width, shutterSize.height);
    [self.shutterImageView addSubview:self.zBarReaderViewController.view];
     */
}

//called after an image is picked
- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info {
    //clean the screen
    
    // ADD: get the decode results
    /*
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    // grab the first barcode
    for(symbol in results)
        break;
    
    NSString *jsonString = symbol.data;
    DLog(@"qr code returned \n%@",jsonString);
    
    //checking the decoded value
    NSError* error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* jsonDictionary = [NSJSONSerialization
                                    JSONObjectWithData:jsonData
                                    options:kNilOptions
                                    error:&error];
    
    if (error) {
        DLog(@"parseAndUpdateControls: error = %@", [error localizedDescription]);
        [Utils showMessage:self header:@"Error" message:@"Wrong QR code format" hideDelay:2];
        return;
    }
    
    NSInteger userIdFromJson = [[jsonDictionary objectForKey:QR_CODE_USER_ID_KEY] integerValue];
    NSInteger registrantIdFromJson = [[jsonDictionary objectForKey:QR_CODE_REGISTRANT_ID_KEY] integerValue];
    
    if (!userIdFromJson || ! registrantIdFromJson) {
        DLog(@"error: userId=%ld, registrantId=%ld", (long)userIdFromJson, (long)registrantIdFromJson);
        [Utils showMessage:self header:@"Error" message:@"Wrong QR code format" hideDelay:2];
        return;
    }

    NSNumber *userIdNumber = [NSNumber numberWithInteger:userIdFromJson];
    DLog(@"userIdNumber = %@", [userIdNumber stringValue] );

    User *crtUser = [[DataEngine sharedInstance] userForId:userIdNumber andRegistrantId:[NSNumber numberWithInteger:registrantIdFromJson]];
    
    BOOL loginOK;
    
    if (crtUser) {
        loginOK=YES;
    } else {
        loginOK=NO;
    }
   
    
    if (loginOK) {
        self.zBarReaderViewController.readerDelegate = nil;
        [self.zBarReaderViewController.view removeFromSuperview];
        self.zBarReaderViewController = nil;
        self.shutterImageView.image =  [info objectForKey: UIImagePickerControllerOriginalImage];
        
        [self.parentParticipantComponentView setUser:crtUser];
        
        // TBD:check this works
        self.parentParticipantComponentView.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
        
        [Utils showMessage:self.parentViewController header:@"QR CODE" message:@"Scan successful" hideDelay:1];
        [self performSelector:@selector(backButtonPressed:) withObject:nil afterDelay:1];
    } else {
        DLog(@"crtUser.registrant.registrantType.registrantTypeId = %@", [crtUser.registrant.registrantType.registrantTypeId description]);
        DLog(@"crtUser.registrant.registrantType.registrationNumber = %@", [crtUser.registrant.registrationNumber description]);
        DLog(@"crtUser.userId = %@", [crtUser.userId description]);
        [Utils showMessage:self header:@"LOGIN" message:@"Login error. Try again!" hideDelay:2];
        [Utils log:@"Login error. Try again!"];
    }
     */
}


- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
