//
//  CalendarPopupViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-20.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarPopupViewControllerDelegate <NSObject>

@optional
- (void)tableDidSelectRow:(NSNotification *)notification;
@end

@interface CalendarPopupViewController : UITableViewController
@property(nonatomic, strong) NSArray *transactionsArray;
@property(nonatomic, strong) UIPopoverController *dayCellPopoverController;
@property(nonatomic, strong) NSDate *date;
@property(nonatomic, strong) id delegate;
@end
