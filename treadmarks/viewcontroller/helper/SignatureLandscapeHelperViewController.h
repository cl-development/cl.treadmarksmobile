//
//  SignatureLandscapeViewController.h
//  SignaturePad
//
//  Created by Capris Group on 2013-08-23.
//  Copyright (c) 2013 Capris Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "ConfirmationPageViewController.h"
#import "NICSignatureView.h"
#import "CustomTextField.h"
#import "Photo.h"

@protocol SignatureLandscapeViewDelegate <NSObject>

@optional
-(void)imageResult:(UIImage *)image participant:(NSString *)name participantType:(NSInteger)type;

@end

@interface SignatureLandscapeHelperViewController : GLKViewController <UITextFieldDelegate, SignatureLandscapeViewDelegate>

@property (nonatomic, assign) NSInteger buttonTag;
@property (weak, nonatomic) id<SignatureLandscapeViewDelegate> signatureDelegate;

@end
