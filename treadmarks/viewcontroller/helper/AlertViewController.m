//
//  AlertViewController.m
//  TreadMarks
//
//  Created by Capris Group on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//  Dennis December 2014 - added methods to create a simpler Error message dialog:


#import "AlertViewController.h"
#import "Constants.h"

@interface AlertViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewHorizontal;
@property (weak, nonatomic)IBOutlet UIImageView *imageViewVerticalLine1;
@property (weak, nonatomic)IBOutlet UIImageView *imageViewVerticalLine2;
@property (weak, nonatomic)IBOutlet UIImageView *imageViewVerticalLine3;




@end

@implementation AlertViewController

#pragma mark - Overridden methods
-(id) init {
    return [self initWithNibName:@"AlertViewController" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if (self)
	{
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    
	return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad {
	
	[super viewDidLoad];
    
    [self.mainView createCornerRadius:self.mainView];
	self.titleLabel.text = self.title;
	self.headingLabel.text = self.heading;
	self.messageLabel.text = self.message;
    self.buttonSecondry.hidden=YES;
    self.buttonPrimary.hidden=YES;
    
    if (self.okButtonText != nil && [self.okButtonText isEqualToString:@""]==NO) {
        [self setButtonTitle:self.okButton title:self.okButtonText];
        self.imageViewVerticalLine1.hidden=YES;
        self.imageViewVerticalLine2.hidden=YES;
        self.imageViewVerticalLine3.hidden=YES;

        
    }
    else {
        self.okButton.hidden=YES;
    }
    if (self.primaryButtonText != nil && [self.primaryButtonText isEqualToString:@""]==NO) {
        [self setButtonTitle:self.primaryButton title:self.primaryButtonText];
        [self setButtonTitle:self.buttonPrimary title:self.primaryButtonText];

        if([self.primaryButtonText  isEqual: @"Both"])
        {[self.primaryButton setTitleColor: [UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
            [self.buttonPrimary setTitleColor: [UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
            
            self.imageViewHorizontal.backgroundColor =[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0];
            self.imageViewVerticalLine1.backgroundColor =[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0];
            self.imageViewVerticalLine2.backgroundColor =[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0];
}
    }
    else {
        self.primaryButton.hidden=YES;
    }
    if (self.secondaryButtonText != nil && [self.secondaryButtonText isEqualToString:@""]==NO) {
        [self setButtonTitle:self.secondaryButton title:self.secondaryButtonText];
         [self setButtonTitle:self.buttonSecondry title:self.secondaryButtonText];
        if([self.secondaryButtonText  isEqual: @"Inbound"])
        {[self.secondaryButton setTitleColor:[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
         [self.buttonSecondry setTitleColor:[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0] forState:UIControlStateNormal];}
    }
    else {
        self.secondaryButton.hidden=YES;
    }
    if (self.tertiaryButtonText != nil && [self.tertiaryButtonText isEqualToString:@""]==NO) {
        [self setButtonTitle:self.tertiaryButton title:self.tertiaryButtonText];
        
        if([self.tertiaryButtonText  isEqual: @"Outbound"])
        {[self.tertiaryButton setTitleColor:[UIColor colorWithRed:0 green:0.478431 blue:1.0 alpha:1.0] forState:UIControlStateNormal];}
    }
    else {
        self.tertiaryButton.hidden=YES;
        self.imageViewVerticalLine3.hidden=YES;

    }
    
    self.imageViewVerticalLine1.hidden=YES;
    self.imageViewVerticalLine2.hidden=YES;
    self.primaryButton.hidden=YES;
    self.secondaryButton.hidden=YES;

    if (self.okButtonText != nil && [self.okButtonText isEqualToString:@""]==NO) {}
    else
    {
        if (self.tertiaryButtonText != nil && [self.tertiaryButtonText isEqualToString:@""]==NO) {
            self.imageViewVerticalLine1.hidden=NO;
            self.imageViewVerticalLine2.hidden=NO;
            self.imageViewVerticalLine3.hidden=YES;
            self.secondaryButton.hidden=NO;
            self.primaryButton.hidden=NO;
            
        }
        else
        {
            self.imageViewVerticalLine3.hidden=NO;
            self.buttonSecondry.hidden=NO;
            self.buttonPrimary.hidden=NO;
            
            

        }
       
       

        
    }
    
  
    
    if (self.showBack==YES)
    { self.backView.hidden=NO;}
        
}

- (void)didReceiveMemoryWarning {
	
	[super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)primaryAction:(UIButton *)sender {
	
	if(self.delegate)
	{
		[self.delegate alertView:self primaryAction:sender info:self.primaryInfo];
	}
}

- (IBAction)backButton:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(alertView:backAction:info:)])
    {
     
        [self dismissViewControllerAnimated:TRUE completion:NULL];
        [self.delegate alertView:self backAction:sender info:self.backInfo];
            }
    else
    {

    [self dismissViewControllerAnimated:TRUE completion:NULL];
    }
    
}
- (IBAction)secondaryAction:(UIButton *)sender {
	
	/// If secondaryAction is not implemented, then it dismisses the alert view.
	if ([self.delegate respondsToSelector:@selector(alertView:secondaryAction:info:)])
	{
		
        [self.delegate alertView:self secondaryAction:sender info:self.secondaryInfo];

	}
	else
	{
		[self dismissViewControllerAnimated:TRUE completion:NULL];
	}
}

- (IBAction)tertiaryAction:(UIButton *)sender {
    
    /// If tertiaryAction is not implemented, then it dismisses the alert view.
    if ([self.delegate respondsToSelector:@selector(alertView:tertiaryAction:info:)])
    {
        [self.delegate alertView:self tertiaryAction:sender info:self.tertiaryInfo];
    }
    else
    {
        [self dismissViewControllerAnimated:TRUE completion:NULL];
    }
}

#pragma mark - Methods

- (void)setButtonTitle:(UIButton*)button title:(NSString*)title {
	
	// Disabling the AttributeTitle as we need to change the color of buttons
    //NSDictionary* attributes = [button.titleLabel.attributedText attributesAtIndex:0
	//																effectiveRange:NULL];
	//NSAttributedString* primaryTitle = [[NSAttributedString alloc] initWithString:title
	//																   attributes:attributes];
	//[button setAttributedTitle:primaryTitle forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
	[button layoutIfNeeded];
}
- (void)presentAlert {

	[self presentAlert:NULL];
}
- (void)presentAlert:(UIViewController *)viewController {
	
	if(viewController == NULL)
	{
		viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
	}
	
    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
	
	[viewController presentViewController:self animated:YES completion:NULL];
}

// For hiding the Notification Center /Control Center
- (void)close:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        [self dismissViewControllerAnimated:TRUE completion:NULL];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}


- (IBAction)weightErrorOkButton:(id)sender {
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.isPopOverDismissOnTouch == YES) {
        UITouch *touch = [[event allTouches] anyObject];
        if (touch.view == self.mainView){}
        else
        {[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
        
        
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.isPopOverDismissOnTouch == YES) {
        UITouch *touch = [[event allTouches] anyObject];
        if (touch.view == self.mainView){}
        else
        {[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.isPopOverDismissOnTouch == YES) {
        UITouch *touch = [[event allTouches] anyObject];
        if (touch.view == self.mainView){}
        else
        {[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (self.isPopOverDismissOnTouch == YES) {
        UITouch *touch = [[event allTouches] anyObject];
        if (touch.view == self.mainView){}
        else
        {[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end
