//
//  MyTableViewCell.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Transaction;

@interface MyTableViewCell : UITableViewCell

@property (strong) Transaction* transaction;

@end
