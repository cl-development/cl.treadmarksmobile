//
//  ImagePickerContainerController.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/18/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Container for UIImagePickerController so that we can change the supportedinterfaceOrientation
@interface ImagePickerContainerController : UIImagePickerController

@property NSUInteger supportedInterfaceOrientations;
@property BOOL shouldAutorotate;

@end
