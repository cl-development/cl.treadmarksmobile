//
//  CalendarPopOverViewController.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarPopoverViewControllerDelegate <NSObject>

@optional

- (void)tableDidSelectRow:(NSNotification*)notification;

@end

@interface CalendarPopOverViewController : UIViewController

@property (strong) NSArray* transactionsArray;
@property (strong) NSDate* date;
@property (strong) id<CalendarPopoverViewControllerDelegate> delegate;

@end

