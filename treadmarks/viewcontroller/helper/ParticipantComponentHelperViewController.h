//
//  ParticipantComponentHelperViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 10/29/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ParticipantComponentView;

@interface ParticipantComponentHelperViewController : UIViewController

//link back to the parent
@property (weak,nonatomic) ParticipantComponentView *parentParticipantComponentView;

@end
