//
//  MyTableViewCell.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "MyTableViewCell.h"
#import "Transaction.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionSyncStatusType.h"

@interface MyTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *transactionIdLabel;
@property (weak, nonatomic) IBOutlet UIImageView *transactionStatusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *transactionSyncStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *transactionSyncStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;

@end

@implementation MyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];

    self.transactionIdLabel.text =[self.transaction.friendlyId stringValue];
    self.transactionTypeLabel.text=self.transaction.transactionType.shortNameKey;
	if (self.transaction.transactionStatusType != NULL) {
        
		self.transactionStatusImageView.image = [UIImage imageNamed:self.transaction.transactionStatusType.fileName];
        self.transactionSyncStatusImageView.image = [UIImage imageNamed:self.transaction.transactionSyncStatusType.fileName];
        
        self.transactionSyncStatusLabel.text = self.transaction.transactionSyncStatusType.userMessage;
        self.transactionStatusLabel.text = self.transaction.transactionStatusType.nameKey;
        
		if ([self.transaction.transactionSyncStatusType.userMessage isEqualToString:@"Not Synced"]) {
            [self.transactionSyncStatusLabel setTextColor:[UIColor colorWithRed:(227/255.f) green:(215/255.f) blue:(31/255.f) alpha:1.0f]];
        }  else if ([self.transaction.transactionSyncStatusType.userMessage isEqualToString:@"Synced"]) {
            [self.transactionSyncStatusLabel setTextColor:[UIColor colorWithRed:(90/255.f) green:(147/255.f) blue:(33/255.f) alpha:1.0f]];
        }  else if ([self.transaction.transactionSyncStatusType.userMessage isEqualToString:@"Error During Sync"]) {
            [self.transactionSyncStatusLabel setTextColor:[UIColor colorWithRed:(237/255.f) green:(28/255.f) blue:(42/255.f) alpha:1.0f]];
        }
        
        if ([self.transaction.transactionStatusType.nameKey isEqualToString:@"Incomplete Transaction"]) {
           [self.transactionStatusLabel setTextColor:[UIColor colorWithRed:(255/255.f) green:(238/255.f) blue:(31/255.f) alpha:1.0f]];
            //[self.transactionSyncStatusLabel setTextColor:[UIColor colorWithRed:(227/255.f) green:(215/255.f) blue:(31/255.f) alpha:1.0f]];
            
        }  else if ([self.transaction.transactionStatusType.nameKey isEqualToString:@"Complete Transaction"]) {
            [self.transactionStatusLabel setTextColor:[UIColor colorWithRed:(90/255.f) green:(147/255.f) blue:(33/255.f) alpha:1.0f]];
        }  else if ([self.transaction.transactionStatusType.nameKey isEqualToString:@"Error In Transaction"]) {
            [self.transactionStatusLabel setTextColor:[UIColor colorWithRed:(237/255.f) green:(28/255.f) blue:(42/255.f) alpha:1.0f]];
        }
       
	}
}

@end
