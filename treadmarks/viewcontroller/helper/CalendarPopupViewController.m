//
//  CalendarPopupViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-20.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "CalendarPopupViewController.h"
#import "MyTableViewCell.h"
#import "Transaction.h"
#import "CalendarViewController.h"

@interface CalendarPopupViewController ()
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@end

@implementation CalendarPopupViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*[self.tableView registerNib:[UINib nibWithNibName:@"CalendarPopupViewController" bundle:nil]
         forCellReuseIdentifier:@"CalendarTableViewCell"];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self dateToString:self.date];
}

// TBD: Utility
-(NSString *) dateToString:(NSDate *)theDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateStyle = NSDateFormatterLongStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    return[formatter stringFromDate:theDate];
}


-(CGFloat)tableView: (UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)path {
    return 92;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.transactionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CalendarTableViewCell";
    Transaction *transaction = [self.transactionsArray objectAtIndex:indexPath.row];
    
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[MyTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CalendarTableViewCell"];
    }
   cell.transaction = transaction;
    
    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Transaction *transaction = [self.transactionsArray objectAtIndex:indexPath.row];
    // notify CalendarPopupViewControllerDelegate
    NSNotification *notification = [NSNotification notificationWithName:@"row selected" object:transaction];
    
    if([[self delegate] respondsToSelector:@selector(tableDidSelectRow:)]) {
        [[self delegate] tableDidSelectRow:notification];
    }

    [self.dayCellPopoverController dismissPopoverAnimated:YES];
}

 


@end
