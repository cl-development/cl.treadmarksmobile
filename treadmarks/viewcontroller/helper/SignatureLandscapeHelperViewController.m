//
//  SignatureLandscapeViewController.m
//  SignaturePad
//
//  Created by Capris Group on 2013-08-23.
//  Copyright (c) 2013 Capris Group. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SignatureLandscapeHelperViewController.h"
#import "AppDelegate.h"

#define kOFFSET_FOR_KEYBOARD 480.0

@interface SignatureLandscapeHelperViewController ()
{
    UIButton *clearButton;
}
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet NICSignatureView *signatureView;
@property (strong, nonatomic) IBOutlet UIButton *signatureAcceptButton;
@property (strong, nonatomic) IBOutlet UIButton *keyboardButton;
@property (nonatomic, strong) UIImage *myImage;
@property (nonatomic, strong) UITextField *field;

@property (nonatomic, strong) UIAlertController *alertController;

- (IBAction)signatureCapture:(id)sender;

- (IBAction)displayKeyboard:(id)sender;

@end

@implementation SignatureLandscapeHelperViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custominitialization
      
        UIView *containerViewRight = [[UIView alloc] initWithFrame:CGRectMake(632,0,136,80)];
        UIImage *image1 = [UIImage imageNamed:@"trans-sign-btn-clear.png"];
        clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [clearButton setFrame:CGRectMake(0,17.5,136,40)];
        [clearButton setImage:image1 forState:UIControlStateNormal];
        [clearButton setContentMode:UIViewContentModeCenter];
        [clearButton addTarget:self action:@selector(clearSignature) forControlEvents:UIControlEventTouchUpInside];
        [containerViewRight addSubview:clearButton];
         
        UIBarButtonItem *rightNavigationBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerViewRight];
        [[self navigationItem] setRightBarButtonItem:rightNavigationBarButtonItem];
        
        
        UIView *containerViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0,0,100,80)];
        UIImage *image2 = [UIImage imageNamed:@"gen-hdr-btn-back.png"];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setFrame:CGRectMake(0,17,100,40)];
        [backButton setImage:image2 forState:UIControlStateNormal];
        [backButton setContentMode:UIViewContentModeCenter];
        [backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [containerViewLeft addSubview:backButton];
        
        UIBarButtonItem *leftNavigationBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:containerViewLeft];
        [[self navigationItem] setLeftBarButtonItem:leftNavigationBarButtonItem];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeScreen:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeScreen:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        

    }
    
    return self;
}
- (void)closeScreen:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        [self restrictRotation:NO];
        [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
   
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage imageNamed:@"gen-hdr-landscape.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIView *customTitleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 900.0f, 90.0f)];
    
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(-55.0f, 20.0f, 900.0f, 40.0f)];
    [customLabel setBackgroundColor:[UIColor clearColor]];
    [customLabel setTextColor:[UIColor whiteColor]];
    [customLabel setFont:[UIFont systemFontOfSize:30.0f]];
    [customLabel setTextAlignment:NSTextAlignmentCenter];
    [customLabel setText:@"Draw Your Signature Slowly Using Your Finger"];
    [customTitleView addSubview:customLabel];
    
    [[self navigationItem] setTitleView:customTitleView];
    
    [[NSNotificationCenter defaultCenter]
     addObserverForName:UIKeyboardDidChangeFrameNotification
     object:nil
     queue:[NSOperationQueue mainQueue]
     usingBlock:^(NSNotification * notification)
     {
         CGRect keyboardEndFrame =
         [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
         
         CGRect screenRect = [[UIScreen mainScreen] bounds];
         
         if (CGRectIntersectsRect(keyboardEndFrame, screenRect))
         {
             // Keyboard is visible
         }
         else
         {
             // Keyboard is hidden
             NSLog(@"%f",self.view.frame.origin.y);
             if  (self.view.frame.origin.y >= 64.0)
             {
             }
else
{
    clearButton.hidden=NO;
             [self keyboardWillHide];
}
             
         }
     }];
//   // [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
    
//   // [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillHide)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //setting attributes for textField that receives user's first and last name
    self.field = [[UITextField alloc] initWithFrame:CGRectMake(190, 540, 644, 64)];
    self.field.delegate = (id)self;
   // [self.field becomeFirstResponder];
    [self.field setBackgroundColor:[UIColor darkGrayColor]];
    [self.field setTextColor:[UIColor whiteColor]];
    [self.field setPlaceholder:@"Enter First and Last Name"];
    [self.field setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [[self.field layer] setMasksToBounds:8.0f];
    [[self.field layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.field layer] setBorderWidth:1.0f];
    [self.field setFont:[UIFont fontWithName:@"Arial" size:30]];
    [self.field setReturnKeyType:UIReturnKeyDone];
    [self.field setTextAlignment:NSTextAlignmentCenter];
    [self.field setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.field setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    self.field.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.mainView addSubview:self.field];
 //   [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

    //[self keyboardWillShow];
    
}
/*
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    //self.currentResponder = textField;
    
    UITextInputAssistantItem* item = [textField inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
}
 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self keyboardWillHide];
    clearButton.hidden=NO;
    if ([textField.text length] > 0) {
        
        NSString *name = [[textField text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSArray *trimmedArray = [name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSArray *words = [trimmedArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
        NSMutableArray *trimmedNames = [NSMutableArray array];
        
        for (NSString *cleanString in words) {
            
            NSString *trimmedString = [cleanString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [trimmedNames addObject:trimmedString];
        }
        
        NSString *alertMessage = @"Please Enter Full First and Last Name";
        
        self.alertController = [UIAlertController alertControllerWithTitle:@"Error" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        
        [self.alertController addAction:okAction];
        
        //self.alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Full First and Last Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        if(trimmedNames.count > 1) {
            
            NSNumber *lengthOfShortestWord = [trimmedNames valueForKeyPath:@"@min.length"];
            
            if (lengthOfShortestWord.intValue > 1) {
                
                [textField resignFirstResponder];
                
            }
            
            else if (lengthOfShortestWord.intValue <= 1) {
                
                //[self.alert show];
                [self presentViewController:self.alertController animated:YES completion:nil];
                
            }
            
        }
        
        else if (trimmedNames.count < 2) {
            
            //[self.alert show];
            [self presentViewController:self.alertController animated:YES completion:nil];
        }
        
        
    }
    else
    {
         [textField resignFirstResponder];
    }
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) restrictRotation:(BOOL) restriction{
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}

- (IBAction)signatureCapture:(id)sender {
    
    self.myImage = [self.signatureView signatureImage];
    
    if (self.myImage == nil) {
        
        NSString *alertMessage = @"Please Enter Your Signature";
        NSString *errorMessage = @"Error";
        
        self.alertController = [UIAlertController
                                alertControllerWithTitle:errorMessage
                                message:alertMessage
                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
        
        [self.alertController addAction:okAction];
        [self presentViewController:self.alertController animated:YES completion:nil];
        
//        self.alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Your Signature" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [self.alert show];
        
    }
    //signature is valid, now perform name check
    else {
        
        if ([self.field.text length] == 0) {
            
            NSString *alertMessage = @"Please Enter Full First and Last Name";
            
            self.alertController = [UIAlertController alertControllerWithTitle:@"Error" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            
            [self.alertController addAction:okAction];
            [self presentViewController:self.alertController animated:YES completion:nil];
//            self.alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Full First and Last Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [self.alert show];
            
        }
        
        else if ([self.field.text length] > 0) {
            
            NSString *name = [[self.field text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSArray *trimmedArray = [name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            NSArray *words = [trimmedArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
            NSMutableArray *trimmedNames = [NSMutableArray array];
            
            for (NSString *cleanString in words) {
                
                NSString *trimmedString = [cleanString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                [trimmedNames addObject:trimmedString];
            }
            
            //self.alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Enter Full First and Last Name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            NSString *alertMessage = @"Please Enter Full First and Last Name";
            NSString *errorMessage = @"Error";
            
            self.alertController = [UIAlertController alertControllerWithTitle:errorMessage message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:nil];
            
            [self.alertController addAction:okAction];
            
            
            if(trimmedNames.count > 1) {
                
                NSNumber *lengthOfShortestWord = [trimmedNames valueForKeyPath:@"@min.length"];
                
                if (lengthOfShortestWord.intValue > 1) {
                    
                    NSInteger participantType = self.buttonTag;
                    NSString *participantName = self.field.text;
                    [self.signatureDelegate imageResult:self.myImage participant:participantName participantType:participantType];
                    [self.signatureView erase];
                    [self restrictRotation:NO];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    
                }
                
                else if (lengthOfShortestWord.intValue <= 1) {
                    
                    //[self.alert show];
                    [self presentViewController:self.alertController animated:YES completion:nil];
                    
                }
                
            }
            
            else if (trimmedNames.count < 2) {
                
                //[self.alert show];
                [self presentViewController:self.alertController animated:YES completion:nil];
                
            }
        
        
        }
    
    }
    
}

- (IBAction)displayKeyboard:(id)sender {
    [self.field becomeFirstResponder];
    
}

- (void)clearSignature {
    [self.signatureView erase];
    
}


- (void)backButtonPressed {
    // Dennis - fix for 3rd issue:
    [self.field resignFirstResponder];
    
    [self restrictRotation:NO];
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
   
}

-(BOOL)shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeLeft;
}


- (void)viewDidUnload {
    [self setKeyboardButton:nil];
   
    [super viewDidUnload];
}
//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    CGRect textFieldRect =
//    [self.view.window convertRect:textField.bounds fromView:textField];
//    CGRect viewRect =
//    [self.view.window convertRect:self.view.bounds fromView:self.view];
//    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
//    CGFloat numerator =
//    midline - viewRect.origin.y
//    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
//    CGFloat denominator =
//    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
//    * viewRect.size.height;
//    CGFloat heightFraction = numerator / denominator;
//    if (heightFraction < 0.0)
//    {
//        heightFraction = 0.0;
//    }
//    else if (heightFraction > 1.0)
//    {
//        heightFraction = 1.0;
//    }
//    UIInterfaceOrientation orientation =
//    [[UIApplication sharedApplication] statusBarOrientation];
//    if (orientation == UIInterfaceOrientationPortrait ||
//        orientation == UIInterfaceOrientationPortraitUpsideDown)
//    {
//        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
//    }
//    else
//    {
//        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
//    }
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y -= animatedDistance;
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
//    
//    [self.view setFrame:viewFrame];
//    
//    [UIView commitAnimations];
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textfield{
//    
//    CGRect viewFrame = self.view.frame;
//    viewFrame.origin.y += animatedDistance;
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationBeginsFromCurrentState:YES];
//    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
//    
//    [self.view setFrame:viewFrame];
//    
//    [UIView commitAnimations];
//}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    UITextInputAssistantItem* item = [sender inputAssistantItem];
    item.leadingBarButtonGroups = @[];
    item.trailingBarButtonGroups = @[];
   
    NSLog(@"%f",self.view.frame.origin.y);
    
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 64.0)
        {
            clearButton.hidden=YES;
            [self setViewMovedUp:YES];
        }

}




@end
