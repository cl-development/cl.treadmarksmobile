//
//  AlertViewController.h
//  TreadMarks
//
//  Created by Capris Group on 1/28/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertViewDelegate.h"
#import "UIView+CustomView.h"

@class ConfirmationPageViewController;


@interface AlertViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIButton *secondaryButton;
@property (weak, nonatomic) IBOutlet UIButton *primaryButton;
@property (weak, nonatomic) IBOutlet UIButton *tertiaryButton;
@property (strong, nonatomic) IBOutlet UIButton *buttonPrimary;
@property (strong, nonatomic) IBOutlet UIButton *buttonSecondry;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *weightErrorLabel;
- (IBAction)weightErrorOkButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *weightErrorView;
@property BOOL isPopOverDismissOnTouch;

@property (strong) NSString* heading;
@property (strong) NSString* message;
@property (strong) NSString* okButtonText;
@property (strong) NSString* primaryButtonText;
@property (strong) NSDictionary* primaryInfo;
@property (strong) NSDictionary* secondaryInfo;
@property (strong) NSString* secondaryButtonText;
@property (strong) NSDictionary* tertiaryInfo;
@property (strong) NSDictionary* backInfo;
@property (strong) NSString* tertiaryButtonText;
@property BOOL showBack;
@property (strong) id<AlertViewDelegate> delegate;

- (void)presentAlert;
- (void)presentAlert:(UIViewController*)viewController;

@end
