//
//  CalendarPopOverViewController.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-14.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "CalendarPopOverViewController.h"
#import "Transaction.h"
#import "MyTableViewCell.h"
#import "TransactionStatusType.h"
#import "UIView+CustomView.h"


@interface CalendarPopOverViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@property (nonatomic, strong) IBOutlet UIView *viewBackground;


@end

@implementation CalendarPopOverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    return self;
}

- (void)closeScreen:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }

   	
}


- (void)viewDidLoad {
	
    [super viewDidLoad];
    [self.viewBackground createCornerRadius:self.viewBackground];
    
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"EEEEdMMM"
															 options:0
                                                              locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];

	self.headerLabel.text = [dateFormatter stringFromDate:self.date];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)dismiss:(UIButton *)sender {

	[self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return [self.transactionsArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSString* identifier = @"MyTableViewCell";
	MyTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	
	if (cell == NULL)
	{
		[tableView registerNib:[UINib nibWithNibName:identifier bundle:NULL] forCellReuseIdentifier:identifier];
		
		cell = [tableView dequeueReusableCellWithIdentifier:identifier];
	}
	
	cell.transaction = self.transactionsArray[indexPath.row];
	
	if ([cell.transaction.transactionStatusType.transactionStatusTypeId isEqualToNumber:TRANSACTION_STATUS_TYPE_ID_DELETED])
	{
		cell.hidden = YES;
	}
	
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    Transaction* transaction = [self.transactionsArray objectAtIndex:indexPath.row];
	NSNotification* notification = [NSNotification notificationWithName:@"row selected" object:transaction];
	
	if ([self.delegate respondsToSelector:@selector(tableDidSelectRow:)])
	{
		[self.delegate tableDidSelectRow:notification];
	}
	
    self.view.userInteractionEnabled=NO;
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];

}

@end
