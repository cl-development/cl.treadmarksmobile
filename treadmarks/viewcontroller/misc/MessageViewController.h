//
//  MessageViewController.h
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-17.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"


typedef void(^VoidBlock)(void);

@interface MessageViewController : UIViewController

#pragma mark - Properties

@property (nonatomic,strong) NSString *header;
@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) NSString *singlelinetext;
@property (nonatomic,strong) NSString *singlegreenlinetext;
@property (nonatomic,strong) NSString *okbuttontext;
@property (nonatomic,strong) NSString *boldtext;
@property (nonatomic) NSTimeInterval hideDelay;
@property (nonatomic) BOOL closeOnTap;
@property (strong) VoidBlock hideCompletion;

#pragma mark - IBOutlets

@property (nonatomic,strong) IBOutlet UILabel *singlelineLabel;
@property (nonatomic,strong) IBOutlet UILabel *singlegreenLabel;
@property (nonatomic,strong) IBOutlet UIButton *okButton;
@property (nonatomic,strong) IBOutlet UILabel *messageLabel;
@property (nonatomic,strong) IBOutlet UILabel *boldLabel;

@end
