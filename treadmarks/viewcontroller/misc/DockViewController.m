//
//  DockViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-07-17.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "DockViewController.h"
#import "Utils.h"

@interface DockViewController ()

@property (weak, nonatomic) IBOutlet UIView *docView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@end

@implementation DockViewController

BOOL  animating;

- (void)viewDidLoad {
    [super viewDidLoad];
    animating = NO;
    [self updateUI:NO];
    CGFloat height = 125;//self.view.frame.size.height;
    self.view.frame = CGRectMake(0, 1024-height, 768, height);
}

-(void)viewWillAppear:(BOOL)animated{
    [self updateUI:NO];
}

-(void)updateUI:(BOOL)animation{
    CGRect newFrame;
    CGFloat height = 125;//self.view.frame.size.height;
    BOOL status;
    if ([Utils isDocOpen]) {
        newFrame = CGRectMake(0, 1024-height, 768, height);
        status=NO;
    } else {
        newFrame = CGRectMake(0, 1024-height+80, 768, height);
        status=YES;
    }
    
    if (animation) {
        [UIView animateWithDuration:.5
                         animations:^{
                             self.view.frame = newFrame;
                             self.arrowImageView.alpha=0;
                         } completion:^(BOOL finished) {
                             animating=NO;
                             self.arrowImageView.highlighted=status;
                             [UIView animateWithDuration:.2
                                              animations:^{
                                                  self.arrowImageView.alpha=1;
                                              }];
                         }];
    } else {
        self.view.frame = newFrame;
        self.arrowImageView.highlighted=status;
    }
}

- (IBAction)tapRecognized:(id)sender {
    if (animating) {
        //do nothing
        return;
    }
    animating=YES;
    [Utils setDocOpen:1-[Utils isDocOpen]];
    [self updateUI:YES];
}

- (void)viewDidUnload {
    [self setDocView:nil];
    [self setArrowImageView:nil];
    [super viewDidUnload];
}

-(IBAction)backToDock:(UIStoryboardSegue*)segue{
}

@end
