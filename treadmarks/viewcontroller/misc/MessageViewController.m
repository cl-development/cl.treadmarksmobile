//
//  MessageViewController.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-17.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "MessageViewController.h"

@interface MessageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (nonatomic, strong)IBOutlet UIView *viewBackground;
@property (nonatomic, weak)IBOutlet UIImageView *imageView;


- (IBAction)okButton:(id)sender;

@end

@implementation MessageViewController

-(id)init{
    self = [super init];
    if (self) {
        self.closeOnTap=YES;
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.headerLabel.text = self.header;
    self.messageLabel.text = self.message;
    self.singlegreenLabel.text=self.singlegreenlinetext;
    self.singlelineLabel.text=self.singlelinetext;
    self.boldLabel.text=self.boldtext;
    [self.viewBackground createCornerRadius:self.viewBackground];

    [self.viewBackground sizeToFit];
    if ([self.okbuttontext length]==0 ) {
        self.imageView.hidden=YES;
    }
    else
    {
        self.imageView.hidden=NO;
    }

    [self.okButton setTitle: self.okbuttontext forState: UIControlStateNormal];
    if(self.hideDelay>0){
    [self performSelector:@selector(hide) withObject:nil afterDelay:self.hideDelay];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapRecognized:(id)sender {
    
    if ([self.headerLabel.text isEqualToString:@"Thank You"]) {
        self.closeOnTap = NO;
    }
    
    if (self.closeOnTap) {
        //[self dismissViewControllerAnimated:YES completion:nil];
       [self hide];
    }
}

-(void)hide{
	
	//NSLog(@"hide");
    [self dismissViewControllerAnimated:YES completion:self.hideCompletion];
}

- (void)viewDidUnload {
    [self setHeaderLabel:nil];
    [self setMessageLabel:nil];
    [super viewDidUnload];
}
- (IBAction)okButton:(id)sender {
    [self performSelector:@selector(hide) withObject:nil afterDelay:0];
}
@end
