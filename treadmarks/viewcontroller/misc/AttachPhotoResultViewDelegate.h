//
//  AttachPhotoResultViewDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/19/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Photo;

@protocol AttachPhotoResultViewDelegate <NSObject>

- (void)updatePhoto:(Photo*)photo;
- (Photo*)photo;

@end
