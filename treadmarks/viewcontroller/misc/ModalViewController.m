//
//  ModalViewController.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

#pragma mark - Properties

@property (weak) UIView* currentView;

@end

@implementation ModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

+ (ModalViewController *)sharedModalViewController {
	
	static ModalViewController* sharedModalViewController = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
				  {
					  sharedModalViewController = [[self alloc] init];
				  });
	return sharedModalViewController;
}
- (void)showView:(UIView *)view {
	
	[self showView:view hideBackground:NO];
}

- (void)dismissView {
    if (self.currentView != nil)
    {
       
                                 [self.currentView removeFromSuperview];
                                 self.currentView = nil;
        self.containerView.alpha=0;
                            }

    [self dismissViewControllerAnimated:YES completion:nil];
   
}



-(void)removeview
{


}
- (void)showView:(UIView *)view hideBackground:(BOOL)hideBackground {
	
	
    
   // UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    //view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
   // [navigationController presentViewController:view animated:YES completion:nil];
    
    //self.backgroundView.hidden = hideBackground;
	
	self.currentView = view;
	[self.containerView insertSubview:self.currentView aboveSubview:self.backgroundView];
    //[self.containerView addSubview:self.currentView];
    
//	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^
//	 {
		 self.containerView.alpha = 1;
	// }];
}
- (void)hideView {
	
	if (self.currentView != nil)
	{
//		[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
//						 animations:^{
							 
							 self.containerView.alpha = 0;
//						 }
//						 completion:^(BOOL finished){
//							 
//							 if (finished)
//							 {
								 [self.currentView removeFromSuperview];
								 self.currentView = nil;
//							 }
//						 }];
	}
}

@end
