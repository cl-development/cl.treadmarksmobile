//
//  AttachScaleTicketViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-04.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ImagePickerContainerController.h"
#import "ScaleTicket.h"
#import "PageViewController.h"
#import "TOCPhotoComponentView.h"
#import "TOCScaleTicketComponentViewDelegate.h"
#import "AlertViewController.h"
#import "UIView+CustomView.h"


@interface AttachScaleTicketViewController : UIViewController <UIScrollViewDelegate, UITextFieldDelegate, AlertViewDelegate>

@property (strong) id<AlertViewDelegate, TOCScaleTicketComponentViewDelegate> delegate;
@property (strong) ScaleTicket* scaleTicket;
@property (strong) PageViewController* containerViewController;
@property (strong) id resultView;


@end
