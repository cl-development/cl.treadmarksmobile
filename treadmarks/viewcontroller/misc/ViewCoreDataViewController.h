//
//  ViewCoreDataViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-10-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataEngine.h"

@interface ViewCoreDataViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) DataEngine *dataEngine;
@property (nonatomic, strong) NSArray *arrayOfRecords;

@end
