//
//  ModalViewController.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalViewController : UIViewController

+ (ModalViewController*)sharedModalViewController;
- (void)showView:(UIView *)view;
- (void)showView:(UIView *)view hideBackground:(BOOL)hideBackground;
- (void)hideView;
- (void)dismissView;
-(void)removeview;

@end
