//
//  AttachPhotoViewController.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-04.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ImagePickerContainerController.h"
#import "Photo.h"
#import "PageViewController.h"
#import "AttachPhotoResultViewDelegate.h"
#import "TOCPhotoComponentView.h"
#import "UIView+CustomView.h"

@interface AttachPhotoViewController : UIViewController<UIGestureRecognizerDelegate>

@property (weak) id<AttachPhotoResultViewDelegate> resultView;
@property (weak) id<AlertViewDelegate, TOCPhotoComponentViewDelegate> delegate;
@property (weak) Photo* photo;
@property (weak) PageViewController* containerViewController;
@property (strong) ImagePickerContainerController* cameraUI;
@property (strong) CLLocationManager *locationManager;

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;

@end
/*
#import <UIKit/UIKit.h>
#import "Photo.h"
#import "AlertViewDelegate.h"

@interface AttachPhotoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UITextViewDelegate, AlertViewDelegate>

@property (nonatomic, strong) Photo *photo;
@property (nonatomic, weak) id delegate;
@property (nonatomic, assign) BOOL isEditing;

- (id)initWithPhoto:(Photo *)thePhoto;

@end
*/
