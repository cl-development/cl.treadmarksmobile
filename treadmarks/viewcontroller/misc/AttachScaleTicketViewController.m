//
//  AttachScaleTicketViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-03-04.
//  Copyright (c) 2014 Capris. All rights reserved.
//

/* One idea: if the scaleTicket is outbound, show the net weight, and alert if zero. */

#import "AttachScaleTicketViewController.h"
#import "DataEngine.h"
#import "ScaleTicket.h"
#import "PhotoPreselectComment.h"
#import "ImagePickerContainerController.h"
#import "Utils.h"
#import "OpenSansRegularTextField.h"
#import "NSManagedObject+NSManagedObject.h"
#import "AppDelegate.h"
#import "NewAlertView.h"
#import "ErrorAlertViewController.h"
#import "ModalViewController.h"

#define SCALE_TICKET_WGT_ERR_NONE 0
#define SCALE_TICKET_WGT_ERR_GROSS_ZERO 1
#define SCALE_TICKET_WGT_ERR_TARE_ZERO 2
#define SCALE_TICKET_WGT_ERR_GROSS_EQS_TARE 3
#define SCALE_TICKET_WGT_ERR_GROSS_NOT_GREATER_THAN_TARE 4
#define SCALE_TICKET_NET_WGT_ZERO 5
#define LBS_TO_KILOGRAMS 0.45359237
@interface AttachScaleTicketViewController () <ErrorAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomMargin;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *showError;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *scaleTicketNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *inboundWeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *inboundWeightTextField;
@property (weak, nonatomic) IBOutlet UILabel *outboundWeightLabel;
@property (weak, nonatomic) IBOutlet UITextField *outboundWeightTextField;
@property (weak, nonatomic) IBOutlet UILabel *weightUnitsLabel;
@property (weak, nonatomic) IBOutlet UISwitch *weightUnitsSwitch;
@property (weak, nonatomic) IBOutlet UIView *retakeButtonView;
@property (weak, nonatomic) NSNumber *scaleTicketType;
@property (weak, nonatomic) IBOutlet UIImageView *middleDivider;
@property (weak, nonatomic) IBOutlet UIImageView *lastDivider;
@property (nonatomic, strong) IBOutlet UIView *subView;

@property (strong) UIImage* image;
@property BOOL isChanged;
@property BOOL isImageChanged;

@end

@implementation AttachScaleTicketViewController


#pragma mark - Initializers

- (id)init {
	
	self = [super init];
	
	if(self)
	{
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:NULL];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:NULL];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeCamera:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeCamera:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];

        
       // self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		self.isImageChanged = NO;
      //  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];

	}
	
	return self;
}
- (void)closeCamera:(NSNotification*)notification {
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
    {
        [self.delegate closeCamera];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }

    
   // [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIView methods

- (void)viewDidLoad {
	
	[super viewDidLoad];
    [self.scrollView createCornerRadius:self.scrollView];

    [self.backgroundView createCornerRadius:self.backgroundView];
    [self.subView createCornerRadius:self.subView];
    
    self.scrollView.delegate = self;
	self.scrollView.contentSize=CGSizeMake(688, 1024);
    self.scaleTicketNumberTextField.delegate = self;
    self.inboundWeightTextField.delegate = self;
    self.outboundWeightTextField.delegate = self;
	self.isChanged = YES;

	// determine record type
    if (self.scaleTicket != nil) { // TBD: exception or NSError here.
        self.scaleTicketType = self.scaleTicket.scaleTicketType.scaleTicketTypeId;
    }
    
    if ([self.scaleTicketType integerValue]==SCALE_TICKET_TYPE_OUTBOUND) {
        self.inboundWeightTextField.hidden=YES;
        self.inboundWeightLabel.hidden=YES;
        self.outboundWeightTextField.hidden=NO;
        self.outboundWeightLabel.hidden=NO;
        //self.titleLabel.text = @"Outbound";
        // move the switch button
        CGRect switchRect = self.weightUnitsSwitch.frame;
        switchRect.origin.x = 588;
        switchRect.origin.y = 99;
        self.weightUnitsSwitch.frame = switchRect;
        
        CGRect labelRect = self.weightUnitsLabel.frame;
        labelRect.origin.x = 536;
        labelRect.origin.y = 95;
        self.weightUnitsLabel.frame = labelRect;
        
        CGRect inboundRect = self.inboundWeightLabel.frame;
        self.outboundWeightLabel.frame = inboundRect;
        self.outboundWeightTextField.frame = self.inboundWeightTextField.frame;
        self.lastDivider.hidden=YES;
    }
    else if ([self.scaleTicketType integerValue]==SCALE_TICKET_TYPE_INBOUND) {
        self.inboundWeightTextField.hidden=NO;
        self.inboundWeightLabel.hidden=NO;
        self.outboundWeightTextField.hidden=YES;
        self.outboundWeightLabel.hidden=YES;
       // self.titleLabel.text = @"Inbound";
        self.lastDivider.hidden=YES;
        CGRect switchRect = self.weightUnitsSwitch.frame;
        switchRect.origin.x = 588;
        switchRect.origin.y = 99;
        self.weightUnitsSwitch.frame = switchRect;
        
        CGRect labelRect = self.weightUnitsLabel.frame;
        labelRect.origin.x = 536;
        labelRect.origin.y = 95;
        self.weightUnitsLabel.frame = labelRect;
    }
    else {
        //self.titleLabel.text = @"Inbound/Outbound";
        
        // adjust the units controls up a bit:
        CGRect dividerRect = self.middleDivider.frame;
        dividerRect.size.width -=90;
        self.middleDivider.frame = dividerRect;
        
        CGRect switchRect = self.weightUnitsSwitch.frame;
        switchRect.origin.y = dividerRect.origin.y-switchRect.size.height/2;
        self.weightUnitsSwitch.frame = switchRect;
        
        CGRect labelRect = self.weightUnitsLabel.frame;
        labelRect.origin.y = dividerRect.origin.y-labelRect.size.height/2;
        self.weightUnitsLabel.frame = labelRect;
    }

    self.image = [Utils findSmallImage:self.scaleTicket.photo.fileName];
    self.imageView.image = self.image;
    self.isImageChanged = YES;
    
    if (self.scaleTicket.ticketNumber != nil) {
        self.scaleTicketNumberTextField.text = self.scaleTicket.ticketNumber;
    }
    
    if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
        if (self.scaleTicket.inboundWeight != nil) {
            if (self.scaleTicket.inboundWeight != [NSNumber numberWithInteger:0]) {
                self.inboundWeightTextField.text = [self.scaleTicket.inboundWeight stringValue];
            }
        }
    }
    else if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
        if (self.scaleTicket.outboundWeight != nil) {
            if (self.scaleTicket.outboundWeight != [NSNumber numberWithInteger:0]) {
                self.outboundWeightTextField.text = [self.scaleTicket.outboundWeight stringValue];
            }
        }
    }
    else if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
        if (self.scaleTicket.inboundWeight != nil) {
            if (self.scaleTicket.inboundWeight != [NSNumber numberWithInteger:0]) {
                self.inboundWeightTextField.text = [self.scaleTicket.inboundWeight stringValue];
            }
        }
        if (self.scaleTicket.outboundWeight != nil) {
            if (self.scaleTicket.outboundWeight != [NSNumber numberWithInteger:0]) {
                self.outboundWeightTextField.text = [self.scaleTicket.outboundWeight stringValue];
            }
        }
    }

    [self.datePicker setDate:self.scaleTicket.date];
    
    UnitType *theType = self.scaleTicket.unitType;
    if (theType==Nil) { // default is KGs
        self.weightUnitsLabel.text = @"KG";
        self.weightUnitsSwitch.on = YES;
    }
    else {
        if (theType.unitTypeId == UNIT_TYPE_ID_KILOGRAM) {
            self.weightUnitsLabel.text = @"KG";
            self.weightUnitsSwitch.on = YES;
        }
        else {
            self.weightUnitsLabel.text = @"LB";
            self.weightUnitsSwitch.on = NO;
        }
    }
    
    self.isChanged = NO;
    
    [self updateDoneButton:self.scaleTicketNumberTextField];
    [self.scaleTicketNumberTextField becomeFirstResponder];

    [self.scaleTicketNumberTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.inboundWeightTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.outboundWeightTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [[self.outboundWeightTextField valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1] forKey:@"insertionPointColor"];
    [[self.inboundWeightTextField valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1] forKey:@"insertionPointColor"];
    [[self.scaleTicketNumberTextField valueForKey:@"textInputTraits"] setValue:[UIColor colorWithRed:0/255.0 green:108/255.0 blue:255/255.0 alpha:1] forKey:@"insertionPointColor"];
}

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	self.view.hidden = self.image == NULL;
}

#pragma mark - AlertViewDelegate methods

/*
NewAlertView *newAlertView = [[NewAlertView alloc] init];
newAlertView.frame = CGRectMake(floor((self.view.frame.size.width - newAlertView.frame.size.width) / 2),
                                floor((self.view.frame.size.height - newAlertView.frame.size.height) / 2),
                                newAlertView.frame.size.width,
                                newAlertView.frame.size.height);
newAlertView.title = @"Attention";
newAlertView.text = self.alertMessage;
newAlertView.primaryButtonText = @"OK";
newAlertView.delegate = self;

[[ModalViewController sharedModalViewController] showView:newAlertView];

*/

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    
    
    /*AlertViewController* alert = [[AlertViewController alloc] init];
    
   // alert.title = @"Weight Error";
    alert.delegate = self;*/
    
    NSNumber *error = info[@"data"];
   
    ErrorAlertViewController *alert=[[ErrorAlertViewController alloc]init];

      alert.errorMessageText=  [self setAlertProperties:alert error:error];
    
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    
    [viewController presentViewController:alert animated:YES completion:NULL];
}
-(void)okButton:(UIButton *)sender
{
[self dismissViewControllerAnimated:TRUE completion:NULL];
    //[self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)alertView:(AlertViewController *)alertView
    primaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)alertView:(AlertViewController *)alertView
    secondaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// helper
-(NSString *) setAlertProperties:(ErrorAlertViewController *)alert error:(NSNumber *)error {
    NSString *errorMessage;
    if ( [error isEqualToNumber:@SCALE_TICKET_WGT_ERR_GROSS_ZERO]==YES) {
       // alert.message = @"The GROSS weight must be more than zero. Please try again.";
       errorMessage=   @"The GROSS weight must be more than zero. Please try again.";
    }
    else if ( [error isEqualToNumber:@SCALE_TICKET_WGT_ERR_TARE_ZERO]==YES) {
        //alert.message
        errorMessage=  @"The TARE weight must be more than zero. Please try again.";
    }
    else if ( [error isEqualToNumber:@SCALE_TICKET_WGT_ERR_GROSS_EQS_TARE]==YES) {
        //alert.message =
        errorMessage =@"You can't enter the same number for both the GROSS and TARE weights. They must be different.\n Please try again.";
        
    }
    else if ( [error isEqualToNumber:@SCALE_TICKET_WGT_ERR_GROSS_NOT_GREATER_THAN_TARE]==YES) {
        //alert.message =
         errorMessage = @"The GROSS weight must be greater than the TARE weight. Please try again.";

    }
    else if ( [error isEqualToNumber:@SCALE_TICKET_NET_WGT_ZERO]==YES) {
        //alert.message =
        errorMessage = @"The Net Weight must be more than zero. Please try again.";
        
    }
    return errorMessage;
}

#pragma mark - IBAction methods

// check the weight values are both non-zero
// check the weight values (gross and tare) are not equal to each other
// i.e. net weight not zero.
// check the inbound weight is greater than the outbound.

-(NSNumber *)fieldsAreValid {
    NSNumber *result = @SCALE_TICKET_WGT_ERR_NONE;
    NSNumber* unitTypeId = self.weightUnitsSwitch.on ? UNIT_TYPE_ID_KILOGRAM : UNIT_TYPE_ID_POUND;
    float inboundWeightValue = [self.inboundWeightTextField.text floatValue];
    float outboundWeightValue = [self.outboundWeightTextField.text floatValue];
    
    //net wight0.00 message
        if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND])
        {
        if (inboundWeightValue==0) {
            result = @SCALE_TICKET_WGT_ERR_GROSS_ZERO;
            
        }
        else {
            
            if (unitTypeId  == UNIT_TYPE_ID_POUND) {
                inboundWeightValue *= LBS_TO_KILOGRAMS;
            }
            result = [self grossTareCompare:inboundWeightValue inbound:YES];
        }
    }
    else  if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
        if (outboundWeightValue==0) {
            result = @SCALE_TICKET_WGT_ERR_TARE_ZERO;
        }
        else {
            if (unitTypeId  == UNIT_TYPE_ID_POUND) {
                outboundWeightValue *= LBS_TO_KILOGRAMS;
            }

            result = [self grossTareCompare:outboundWeightValue inbound:NO];
        }
    }
   

    else if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
        
        int netWeight=(int)(inboundWeightValue - outboundWeightValue);
        if(netWeight==0||netWeight==-0)
        {
            result = @ SCALE_TICKET_NET_WGT_ZERO;
            
        }
        
        if (inboundWeightValue==0) {
            result = @SCALE_TICKET_WGT_ERR_GROSS_ZERO;
           
        }
        else if (outboundWeightValue==0) {
            result = @SCALE_TICKET_WGT_ERR_TARE_ZERO;
        }
        else if (outboundWeightValue > inboundWeightValue) {
            result = @SCALE_TICKET_WGT_ERR_GROSS_NOT_GREATER_THAN_TARE;
        }
        else if (outboundWeightValue == inboundWeightValue) {
            result = @SCALE_TICKET_WGT_ERR_GROSS_EQS_TARE;
        }
    }
    
    return result;
}

-(NSNumber *) grossTareCompare:(float)weight inbound:(BOOL) inbound{
    NSNumber *result = @SCALE_TICKET_WGT_ERR_NONE;
    float compareWeight = INVALID_WEIGHT_VALUE;
    compareWeight = [self getCompareWeight:inbound];
    
    if (compareWeight!=INVALID_WEIGHT_VALUE) {
        BOOL okay = NO;
        
        int netWeight=(int)(weight - compareWeight);
                if(netWeight==0||netWeight==-0)
        {
            result = @ SCALE_TICKET_NET_WGT_ZERO;
            
        }
        if (weight==compareWeight) {
            result = @SCALE_TICKET_WGT_ERR_GROSS_EQS_TARE;
        }
        else {
            okay = inbound? weight > compareWeight : compareWeight > weight;
            if (okay==NO) {
                result = @SCALE_TICKET_WGT_ERR_GROSS_NOT_GREATER_THAN_TARE;
            }
        }
            //check for 0.000
        
    
    }
    return result;
}


-(float) getCompareWeight:(BOOL)inbound {
    float compareWeight = INVALID_WEIGHT_VALUE;
    NSArray *scaleTickets =[[DataEngine sharedInstance] scaleTicketsForTransaction:self.scaleTicket.transaction andPhotoType:PHOTOTYPE_ID_SCALE_TICKET];

    NSNumber *oppositeType = inbound? @SCALE_TICKET_TYPE_OUTBOUND : @SCALE_TICKET_TYPE_INBOUND;
    //check unit type id
    
    for (ScaleTicket *ticket in scaleTickets) {
        UnitType *ticketUnit = ticket.unitType;
         if ([ticket.scaleTicketType.scaleTicketTypeId isEqualToNumber:oppositeType]) {
             if (inbound)
             {  compareWeight = [ticket.outboundWeight floatValue];
                 if (ticketUnit.unitTypeId == UNIT_TYPE_ID_POUND)
                {compareWeight *=  LBS_TO_KILOGRAMS;}
             }
             else {
                 compareWeight = [ticket.inboundWeight floatValue];
                 if (ticketUnit.unitTypeId == UNIT_TYPE_ID_POUND)
                 {compareWeight *=  LBS_TO_KILOGRAMS;}
             }
             break;
         }
    }
    return compareWeight;
}
- (IBAction)retake:(UITapGestureRecognizer *)sender {
    
    [self.delegate popoverRequestsRetake:self.scaleTicket
                             resultView:self.resultView];
}


- (IBAction)done:(UIButton *)sender {
	
    if (self.scaleTicket==nil) { //TBD: generate an NSError.
        
    }
    /// When entering data is done -- trim any white spaces
    NSString *scaleNumber = [self.scaleTicketNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.scaleTicket.ticketNumber = scaleNumber;
    NSNumber *error = [self fieldsAreValid];
    
    if ([error integerValue] != SCALE_TICKET_WGT_ERR_NONE) {
        [self presentAlert:self info:@{@"data": error}];
        //[self showErrorMessage:error];
        return;
    }
    
    NSString *inBoundWeight = self.inboundWeightTextField.text;
    NSString *outBoundWeight = self.outboundWeightTextField.text;
    
    self.scaleTicket.outboundWeight = [NSDecimalNumber decimalNumberWithString:outBoundWeight];
    self.scaleTicket.inboundWeight = [NSDecimalNumber decimalNumberWithString:inBoundWeight];
    //1346- issue
    
    if ([[NSDecimalNumber decimalNumberWithString:outBoundWeight] isEqualToNumber:[NSDecimalNumber notANumber]]) {
        self.scaleTicket.outboundWeight = nil;
    }  else {
        self.scaleTicket.outboundWeight = [NSDecimalNumber decimalNumberWithString:outBoundWeight];
    }

    if ([[NSDecimalNumber decimalNumberWithString:inBoundWeight] isEqualToNumber:[NSDecimalNumber notANumber]]) {
        self.scaleTicket.inboundWeight = nil;
    }  else {
        self.scaleTicket.inboundWeight = [NSDecimalNumber decimalNumberWithString:inBoundWeight];
    }
   //-------

    NSNumber* unitTypeId = self.weightUnitsSwitch.on ? UNIT_TYPE_ID_KILOGRAM : UNIT_TYPE_ID_POUND;
	self.scaleTicket.unitType = [UnitType singleById:unitTypeId];

    [self.delegate popoverDone:self.scaleTicket
                         image:self.image
                    resultView:self.resultView];
}

-(IBAction) cancel:(id) sender {
    [self.delegate cancelAnimated:YES resultView:self.scaleTicket];
}

- (IBAction)dateChanged:(UIDatePicker*)sender {
    self.scaleTicket.date = sender.date;
}

-(IBAction)changeWeightUnits:(id)sender {
    if (self.weightUnitsSwitch.on) {
        self.weightUnitsLabel.text = @"KG";
    }
    else {
        self.weightUnitsLabel.text = @"LB";
    }
    self.scaleTicket.unitType = self.weightUnitsSwitch.on ? [[DataEngine sharedInstance] unitTypeForId:UNIT_TYPE_ID_KILOGRAM]:[[DataEngine sharedInstance] unitTypeForId:UNIT_TYPE_ID_POUND];
}

#pragma mark - Keyboard notification methods

- (void)keyboardWillShow:(NSNotification*)notification {
	
	NSDictionary* userInfo = notification.userInfo;
	CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^
     {
         CGSize size =CGSizeMake(688, 1024);
         
         size.height = 985+ keyboardFrame.size.height;
         
         self.scrollView.contentSize = size;
         self.scrollView.contentOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.frame.size.height);
     }];
}
- (void)keyboardWillHide:(NSNotification*)notification {
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^
     {
         CGSize size =CGSizeMake(688, 1024);
         
         size.height = 1024;
         
         self.scrollView.contentSize = size;
     }];
}


#pragma mark - Other Methods

- (void)updateDoneButton:(UITextField *)textField {
	
	NSString* scaleTicketNumberText = [self.scaleTicketNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* inboundWeightText = [self.inboundWeightTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* outboundWeightText = [self.outboundWeightTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    BOOL weightTextOk = NO;
    
    if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_INBOUND]) {
        weightTextOk = [inboundWeightText length] > 0;
    }
    else  if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_OUTBOUND]) {
        weightTextOk = [outboundWeightText length] > 0;
    }
    else if ([self.scaleTicketType isEqualToNumber:@SCALE_TICKET_TYPE_BOTH]) {
        weightTextOk = [inboundWeightText length] > 0 && [outboundWeightText length] > 0;
    }

	self.doneButton.enabled = [scaleTicketNumberText length] > 0 && weightTextOk;
}


#pragma mark - UITextFieldDelegate

// validate the entered numbers
// dissallow: chars other than decimals
// dissallow: overall length > 14
// restrict to max integer and decimal parts
// dissallow decimal point twice
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet *invalidCharSet = (textField==self.inboundWeightTextField || textField==self.outboundWeightTextField) ? [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField==self.scaleTicketNumberTextField) {
        if (newLength > SCALE_TICKET_NUMBER_MAX_LEN) {
            return NO;
        }
        else return YES;
    }
    if (newLength > SCALE_TICKET_WEIGHT_MAX_INTEGER_LEN + SCALE_TICKET_WEIGHT_MAX_DECIMAL_LEN + 1) {
        return NO;
    }
    NSRange decRange = [textField.text rangeOfString:@"." options:NSLiteralSearch];
    if (decRange.location != NSNotFound) {
        if ([filtered isEqualToString:@"."]==YES) {
            return NO;
        }
         NSString *integerPart = [textField.text componentsSeparatedByString:@"."][0];
         NSString *decimalPart = [textField.text componentsSeparatedByString:@"."][1];
         if (([integerPart length] > SCALE_TICKET_WEIGHT_MAX_INTEGER_LEN || [decimalPart length] >= SCALE_TICKET_WEIGHT_MAX_DECIMAL_LEN) && [string length] != 0) {
             return NO;
         }
    }
    else {
        if ([filtered isEqualToString:@"."]==NO) {
            if (newLength > SCALE_TICKET_WEIGHT_MAX_INTEGER_LEN) {
                return NO;
            }
        }
    }
    return [string isEqualToString:filtered];
}



-(void) textFieldDidChange:(UITextField *)textField {
    self.isChanged = YES;
	
	[self updateDoneButton:textField];
}

/*-(void) showErrorMessage:(NSError *)error {

    [Utils showMessage:self header:@"Weight Error" message:@"The gross weight is wrong." hideDelay:0 hideCompletion:^
     {
         NSLog(@"test");
     }];
}*/

@end
