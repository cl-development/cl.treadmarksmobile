//
//  ViewCoreDataViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-10-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "ViewCoreDataViewController.h"
#import "DataEngine.h"
#import "Registrant.h"
#import "Location.h"
#import "RegistrantType.h"
#import "Constants.h"

@interface ViewCoreDataViewController ()

@property (nonatomic, retain) NSArray *transactions;
@property (nonatomic, weak) IBOutlet UITableView *theTableView;
@property (nonatomic, weak) IBOutlet UITextView *dataDisplayTextView;
@property (nonatomic, retain) IBOutlet UITextField *userNumberTextField;

-(IBAction) viewUsers:(id) sender;
-(IBAction) viewRegistrants:(id) sender;
-(IBAction) viewRegistrantTypes:(id) sender;

-(IBAction) viewTransactions:(id) sender;
-(IBAction) viewTransactionTypes:(id) sender;
-(IBAction) viewTransactionType_RegistrantTypes:(id) sender;
-(IBAction) viewTransactionType_TireTypes:(id) sender;

-(IBAction) viewTransaction_TireTypes:(id) sender;
-(IBAction) viewTireTypes:(id) sender;

-(IBAction) viewTransactionEligibilities:(id) sender;
-(IBAction) viewEligibilities:(id) sender;

-(IBAction) viewTransactionSyncStatuses:(id) sender;
-(IBAction) viewTransactionStatusTypes:(id) sender;

-(IBAction) viewLocations:(id) sender;
-(IBAction) viewSTCAuthorizations:(id) sender;
-(IBAction) viewAuthorizations:(id) sender;

-(IBAction) viewComments:(id) sender;
-(IBAction) viewDocuments:(id) sender;
-(IBAction) viewDocumentTypes:(id) sender;

-(IBAction) viewPhotos:(id) sender;
-(IBAction) viewScaleTickets:(id) sender;
-(IBAction) viewScaleTicketTypes:(id) sender;
-(IBAction) viewUnitTypes:(id) sender;
-(IBAction) viewTransactionTypeModules:(id) sender;

-(void) displayRecords;
@end

@implementation ViewCoreDataViewController

#pragma mark - View

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dataEngine = [DataEngine sharedInstance];
    self.theTableView.hidden=YES;
    [self.theTableView registerClass:[UITableViewCell class] forCellReuseIdentifier: @"TransactionNameCell"];
    
    self.arrayOfRecords = [NSMutableArray array];
    self.transactions = [self.dataEngine entities:TRANSACTION_ENTITY_NAME];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

-(IBAction) viewRegistrants:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Registrant"]; //[self.dataEngine registrantForId:[NSNumber numberWithInt:1000005]];   //
    if ([self.arrayOfRecords count]==0)
        return;
    
    for (Registrant *reg in self.arrayOfRecords) {
        if ([reg.businessName isEqualToString:@"test"]==YES) {
            //NSLog(@"Found%@",reg.businessName);
        }
        if ([reg.registrationNumber integerValue]==2000009 || [reg.registrationNumber integerValue]==2000023) {
             //NSLog(@"Found %d", [reg.registrationNumber integerValue]);
        }
    }
    [self displayRecords];
}

-(IBAction) viewUsers:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"User"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
    
}

-(IBAction) viewRegistrantTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"RegistrantType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransactions:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Transaction"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewMessages:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Message"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}


-(IBAction) viewTransactionTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"TransactionType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    NSMutableString *currentText = [NSMutableString string];
    //[self displayRecords];
    [currentText appendString:@"\n"];

    for (TransactionType *type in self.arrayOfRecords) {
        [currentText appendString:@"\n"];
        [currentText appendString:type.nameKey];
        [currentText appendString:@"\n"];
        [currentText appendString:type.shortNameKey];
        [currentText appendString:@"\n"];
        [currentText appendString:[type.transactionTypeId stringValue]];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }
    
    self.dataDisplayTextView.text = currentText;
}

-(IBAction) viewTransactionType_RegistrantTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"TransactionType_RegistrantType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransactionType_TireTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"TransactionType_TireType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransaction_TireTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Transaction_TireType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTireTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"TireType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransactionEligibilities:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Transaction_Eligibility"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewEligibilities:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Eligibility"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];

}

-(IBAction) viewTransactionStatusTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransactionSyncStatuses:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}


-(IBAction) viewLocations:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Location"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];    
}

-(IBAction) viewSTCAuthorizations:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"STCAuthorization"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];

}

-(IBAction) viewAuthorizations:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Authorization"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewComments:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Comment"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewDocuments:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Document"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewDocumentTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"DocumentType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewPhotos:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"Photo"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewPhotoComments:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}


-(IBAction) viewScaleTickets:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"ScaleTicket"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewScaleTicketTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"ScaleTicketType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewUnitTypes:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"UnitType"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewTransactionTypeModules:(id) sender {
    self.arrayOfRecords = [self.dataEngine entities:@"TransactionType_Module"];
    if ([self.arrayOfRecords count]==0)
        return;
    
    [self displayRecords];
}

-(IBAction) viewUserDetail:(id) sender {
    [self viewUserDetailCC:self.userNumberTextField.text];
}

//load previous page in the flow
-(IBAction)backButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)viewTransactionButtonPressed:(id)sender {
    self.theTableView.hidden=NO;
}

#pragma mark - Private API


-(void) viewTransactionDetail:(Transaction *)theTransaction {
    User *incomingUser = (User *)[self.dataEngine userForId:theTransaction.incomingUser.userId];
    User *outgoingUser = (User *)[self.dataEngine userForId:theTransaction.outgoingUser.userId];
    TransactionStatusType *transactionStatusType = (TransactionStatusType *)[self.dataEngine transactionStatusTypeForId:theTransaction.transactionStatusType.transactionStatusTypeId];
    
    NSMutableString *currentText = [NSMutableString string];
    NSString *transDescription = [theTransaction description];
    
    [currentText appendString:NSStringFromClass([theTransaction class])];
    [currentText appendString:@"\n"];
    [currentText appendString:transDescription];
    [currentText appendString:@"\n"];
    [currentText appendString:@"--------------------------------------------\n"];
    
    if (incomingUser != nil) {
         [currentText appendString:NSStringFromClass([incomingUser class])];
         [currentText appendString:@"\n"];
         [currentText appendString:[incomingUser description]];
         [currentText appendString:@"\n"];
         [currentText appendString:@"--------------------------------------------\n"];
    }
    if (outgoingUser != nil) {
         [currentText appendString:NSStringFromClass([outgoingUser class])];
         [currentText appendString:@"\n"];
         [currentText appendString:[outgoingUser description]];
         [currentText appendString:@"\n"];
         [currentText appendString:@"--------------------------------------------\n"];
    }
    if (transactionStatusType != nil) {
        [currentText appendString:NSStringFromClass([transactionStatusType class])];
        [currentText appendString:@"\n"];
        //[currentText appendString:[transactionStatusType description]];
        [currentText appendString:transactionStatusType.nameKey];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }
    if (theTransaction.incomingGpsLog != nil) {
        [currentText appendString:NSStringFromClass([theTransaction.incomingGpsLog class])];
        [currentText appendString:@"\n"];
        [currentText appendString:[theTransaction.incomingGpsLog description]];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }
    if (theTransaction.outgoingGPSLog != nil) {
        [currentText appendString:NSStringFromClass([theTransaction.outgoingGPSLog class])];
        [currentText appendString:@"\n"];
        [currentText appendString:[theTransaction.outgoingGPSLog description]];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }

    self.dataDisplayTextView.text = currentText;
}

-(void) viewUserDetailCC:(NSString *)userId {
    User *user = (User *)[self.dataEngine userForId:[NSNumber numberWithInteger:[userId integerValue]]];
    Registrant *registant = (Registrant *)[self.dataEngine registrantForId:user.registrant.registrationNumber];
    Location *location = (Location *)[self.dataEngine locationForId:registant.location.locationId];
    
    NSMutableString *currentText = [NSMutableString string];
    NSString *description = [user description];
    
    [currentText appendString:NSStringFromClass([user class])];
    [currentText appendString:@"\n"];
    [currentText appendString:description];
    [currentText appendString:@"\n"];
    [currentText appendString:@"--------------------------------------------\n"];
    
    if (registant != nil) {
        [currentText appendString:NSStringFromClass([registant class])];
        [currentText appendString:@"\n"];
        [currentText appendString:[registant description]];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }
    if (location != nil) {
        [currentText appendString:NSStringFromClass([location class])];
        [currentText appendString:@"\n"];
        [currentText appendString:[location description]];
        [currentText appendString:@"\n"];
        [currentText appendString:@"--------------------------------------------\n"];
    }
    
    self.dataDisplayTextView.text = currentText;
}


-(void) displayRecords {
    NSMutableString *currentList = [NSMutableString string];
    for (NSManagedObject *theObject in self.arrayOfRecords) {
        NSString *fields = [theObject description];
        //NSString *fields = [NSString stringWithFormat:@"%@",theObject];
        
        [currentList appendString:fields];
        [currentList appendString:@"\n"];
    }
    self.dataDisplayTextView.text = currentList;
}
                         
#pragma mark - Table view data source

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Transactions";
}

/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(0,0,220,60)];
    tempView.backgroundColor=[UIColor blackColor];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,0,200,44)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = [UIColor redColor]; //here you can change the text color of header.
    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0];
    tempLabel.font = [UIFont boldSystemFontOfSize:17.0];
    tempLabel.text= @"Transactions";
    [tempView addSubview:tempLabel];
    
    return tempView;
}*/

 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     return 1;
 }
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     return [self.transactions count];
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *CellIdentifier = @"TransactionNameCell";
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
     
     if (cell == nil) {
         cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
     }
     
     Transaction *trans = [self.transactions objectAtIndex:[indexPath row]];
     cell.textLabel.text = [trans.friendlyId stringValue];
     
     return cell;
 }
 
 
#pragma mark - Table view delegate
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     Transaction *trans = [self.transactions objectAtIndex:[indexPath row]];
     
     self.theTableView.hidden=YES;
     [self viewTransactionDetail:trans];
}





@end
