//
//  AttachPhotoViewController.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2013-11-04.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "AttachPhotoViewController.h"
#import "DataEngine.h"
#import "PhotoPreselectComment.h"
#import "ImagePickerContainerController.h"
#import "Utils.h"
#import "OpenSansTextView.h"
#import "OverlayView.h"
#import "AlertViewController.h"

@interface AttachPhotoViewController () <
	UITableViewDataSource,
	UITableViewDelegate,
	UINavigationControllerDelegate,
	UIImagePickerControllerDelegate,
	UITextViewDelegate,
	CLLocationManagerDelegate,
    AlertViewDelegate>

#pragma mark - IBOutlet properties

@property (weak, nonatomic) IBOutlet UITextView *commentsTextView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomMargin;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *quickCommentTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *dialogBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *dialogView;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak,nonatomic) IBOutlet UIView *subView;

#pragma mark - Properties

@property (strong) UIImage* image;
@property (strong) NSArray* photoPreselectComments;
@property CGFloat originalContentViewHeight;
@property BOOL imagePickerCancel;
@property BOOL isChanged;
@property BOOL isImageChanged;
@property BOOL willDismiss;
@property (strong) CLLocation* location;
@property (weak, nonatomic) IBOutlet UIView *commetsSection;

@end

@implementation AttachPhotoViewController

#pragma mark - Initializers

- (id)init {
	
	self = [super init];
	
	if(self)
	{
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:NULL];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:NULL];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeCamera:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeCamera:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];

		self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		self.photoPreselectComments = [[DataEngine sharedInstance] entities:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
		self.imagePickerCancel = NO;
		self.willDismiss = NO;
		self.isImageChanged = NO;
	}
    
    

	return self;
}

#pragma mark - Override methods
- (void)closeCamera:(NSNotification*)notification {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"Unlocked"])
        {
            [self dismissViewControllerAnimated:YES completion:nil];
            [self.delegate closeCamera];
            return;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
       
}
    

- (void)viewDidLoad {
	
	[super viewDidLoad];
	
    
    //self.backgroundView.alpha = MODAL_BACKGROUND_ALPHA;
    [self.backgroundView createCornerRadius:self.backgroundView];
    [self.scrollView createCornerRadius:self.scrollView];
    [self.subView createCornerRadius:self.subView];
    [self.contentView createCornerRadius:self.contentView];
    
    //[self.buttonsView createCornerRadius:self.buttonsView];
    self.scrollView.contentSize = self.contentView.frame.size;
	self.quickCommentTableView.layer.borderWidth = 1;
	self.quickCommentTableView.layer.borderColor = [[UIColor colorWithRed:(90./255)
																	green:(147./255)
																	 blue:(33./255)
																	alpha:1] CGColor];
	self.quickCommentTableView.delegate = self;
	self.originalContentViewHeight = self.contentView.frame.size.height;
	self.commentsTextView.delegate = self;
	self.isChanged = YES;
	self.dialogBackgroundView.layer.cornerRadius = 10;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    
    singleTap.cancelsTouchesInView = NO;
    
    [self.scrollView addGestureRecognizer:singleTap];
    
    
	if(self.photo)
	{
        //NSLog(@"photo image is: %@", self.photo.description);
        //NSLog(@"The photo file name is: %@", self.photo.fileName);
		//self.image = [UIImage imageWithContentsOfFile:self.photo.fileName];
        self.image = [Utils findSmallImage:self.photo.fileName];
        //NSLog(@"The image object is: %@", self.image.description);
		self.imageView.image = self.image;
		self.commentsTextView.text = self.photo.comments;
		self.titleLabel.text = @"Edit Photo";
		self.isChanged = NO;
		self.removeButton.hidden = NO;
		self.cancelButton.hidden = YES;
		
		[self updateDoneButton:self.commentsTextView];
		
		[self.commentsTextView becomeFirstResponder];
	}
}

-(IBAction) singleTapGestureCaptured:(UITapGestureRecognizer*)recognizer
{
    self.quickCommentTableView.hidden = !self.quickCommentTableView.hidden;

}
- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	self.view.hidden = self.image == NULL;
}
- (void)viewDidAppear:(BOOL)animated {
	
	[super viewDidAppear:animated];
	
	if(self.willDismiss)
	{
		/// The view controller will be dismissed so do not do anything
		return;
	}
	
	if(self.image == NULL && self.imagePickerCancel == NO)
	{
		[self startCameraControllerFromViewController:self
										usingDelegate:self];
	}
}

#pragma mark - IBAction methods

- (IBAction)cancel:(UIButton *)sender {
	
	[self cancelAndDismissAnimated:YES];
}
- (IBAction)done:(UIButton *)sender {
	
	self.quickCommentTableView.hidden=YES;
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^
	 {
		 self.dialogView.alpha = 1;
		 
	 } completion:^(BOOL finished)
	 {
		 if(finished)
		 {
			 /// When entering data is done -- trim any white spaces
			 NSString* comment = [self.commentsTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet   ]];
			 
			 [self.delegate popoverDone:self.photo
								comment:comment
								  image:self.image
							 resultView:self.resultView
                              didRetake:self.isImageChanged
							   location:self.location];
		 }
	 }];
}
- (IBAction)retake:(UITapGestureRecognizer *)sender {
	
	[self startCameraControllerFromViewController:self
									usingDelegate:self];
}
- (IBAction)quickComment:(UITapGestureRecognizer *)sender {
	
	self.quickCommentTableView.hidden = !self.quickCommentTableView.hidden;
}
- (IBAction)back:(UIButton *)sender {
	
	if(self.isChanged || self.isImageChanged)
	{
		[self done:sender];
	}
	else
	{
		[self cancel:sender];
	}
}


- (IBAction)remove:(UIButton *)sender {
    
    
    [self presentAlert:self info:@{@"data": self.photo}];
    
    //Old way of calling the  Alertview from TOCPhoto
    //[self.delegate presentAlert:self info:@{@"data": self.photo}];
}


#pragma mark - AlertViewDelegate
- (void)alertView:(AlertViewController *)alertView
    primaryAction:(UIButton *)button
             info:(NSDictionary*)info {
    //Call TOCPhoto to remove ticket!
    [self.delegate alertView:alertView primaryAction:button info:info];
  
}


- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
    
    AlertViewController* alert = [[AlertViewController alloc] init];
    
    alert.title = @"Warning";
    alert.heading = @"Are you sure you want to remove this photo?";
    alert.message = @"You will lose the photo and comments that were recorded for this transaction.";
    alert.primaryButtonText = @"Remove";
    alert.primaryInfo = info;
    alert.secondaryButtonText = @"Cancel";
    alert.delegate = self;
    alert.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [viewController presentViewController:alert animated:YES completion:NULL];
    
}


#pragma mark - Methods

- (void)cancelAndDismissAnimated:(BOOL)animated {

	//NSLog(@"cancelAndDismiss");
	
	[self.delegate cancelAnimated:animated];
}
- (BOOL)startCameraControllerFromViewController: (UIViewController*) controller
								   usingDelegate: (id <UIImagePickerControllerDelegate,
												   UINavigationControllerDelegate>) delegate {
	
	//NSLog(@"startCameraControllerFromViewController:usingDelegate:");

    if (([UIImagePickerController isSourceTypeAvailable:
		  UIImagePickerControllerSourceTypeCamera] == NO)
		|| (delegate == nil)
		|| (controller == nil))
	{
		self.willDismiss = YES;
		
		[Utils showMessage:controller
					header:@"Error"
				   message:@"Camera not available."
				 hideDelay:1
			hideCompletion:^
		 {
			 [self cancelAndDismissAnimated:NO];
		 }];
		
        return NO;
	}
	
    self.cameraUI = [[ImagePickerContainerController alloc] init];
    self.cameraUI.supportedInterfaceOrientations = UIImageOrientationUp;
	self.cameraUI.shouldAutorotate = NO;
	self.cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
	
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    self.cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
	
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    self.cameraUI.allowsEditing = NO;

	// Load the overlay view from the OverlayView nib file. Self is the File's Owner for the nib file, so the overlayView outlet is set to the main view in the nib. Pass that view to the image picker controller to use as its overlay view, and set self's reference to the view to nil.
	OverlayView* overlay = [[OverlayView alloc] initWithFrame:self.cameraUI.cameraOverlayView.frame];
	
	overlay.viewController = self;
	
    self.cameraUI.delegate = overlay;
	self.cameraUI.showsCameraControls = NO;
	self.cameraUI.cameraOverlayView = overlay;
	
	[controller presentViewController:self.cameraUI animated:YES completion:NULL];
	
	//NSLog(@"self.locationManager = [[CLLocationManager alloc] init];");

	self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
	
	//NSLog(@"[self.locationManager startUpdatingLocation];");

    return YES;
}
- (void)updateDoneButton:(UITextView *)textView {
	
	NSString* trimmedText = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	self.doneButton.enabled = [trimmedText length] > 0;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return [self.photoPreselectComments count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSString* cellIdentifier = @"Cell";
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell == NULL)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	
	PhotoPreselectComment* comment = self.photoPreselectComments[indexPath.row];

	cell.textLabel.text = comment.nameKey;
	cell.textLabel.font = [UIFont fontWithName:DEFAULT_REGULAR_FONT size:22];
	
	return cell;
}

#pragma mark - Keyboard notification methods

- (void)keyboardWillShow:(NSNotification*)notification {
	
	NSDictionary* userInfo = notification.userInfo;
	CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^
	{
		CGSize size = self.contentView.frame.size;
		
		size.height = self.originalContentViewHeight + keyboardFrame.size.height-39;
		
		self.scrollView.contentSize = size;
		self.scrollView.contentOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.frame.size.height);
	}];
}
- (void)keyboardWillHide:(NSNotification*)notification {
	
	[UIView animateWithDuration:DEFAULT_ANIMATION_DURATION
					 animations:^
	{
		CGSize size = self.contentView.frame.size;
		
		size.height = self.originalContentViewHeight;
		
		self.scrollView.contentSize = size;
	}];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	self.image = [info objectForKey:UIImagePickerControllerOriginalImage];
	self.imageView.image = self.image;
	self.isImageChanged = YES;
	self.cameraUI = nil;
	
	//NSLog(@"self.locationManager = nil;");
	
	[self.locationManager stopUpdatingLocation];
	self.locationManager = nil;
	
	[self dismissViewControllerAnimated:YES completion:^
	{
		[self.commentsTextView becomeFirstResponder];
	}];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
	self.imagePickerCancel = YES;
	self.cameraUI = nil;
	
	//NSLog(@"self.locationManager = nil;");

	[self.locationManager stopUpdatingLocation];
	self.locationManager = nil;
	
	[self dismissViewControllerAnimated:YES completion:^
	 {
		 if(self.image == NULL)
		 {
			 [self cancelAndDismissAnimated:YES];
		 }
	 }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(OpenSansTextView *)textView {
	self.quickCommentTableView.hidden=YES;
	[textView textViewDidChange:textView];
	
	self.isChanged = YES;
	
	[self updateDoneButton:textView];
}


/*
 #pragma mark - UITableViewDataSource
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return [self.photoPreselectComments count];
 }
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
	NSString* cellIdentifier = @"Cell";
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell == NULL)
	{
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	
	PhotoPreselectComment* comment = self.photoPreselectComments[indexPath.row];
 
	cell.textLabel.text = comment.nameKey;
	cell.textLabel.font = [UIFont fontWithName:DEFAULT_REGULAR_FONT size:22];
	
	return cell;
 }

 */


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	PhotoPreselectComment* comment = self.photoPreselectComments[indexPath.row];
    

	
    [self.commentsTextView insertText:[NSString stringWithFormat:@"%@\r\n", comment.nameKey]];
	
	[self quickComment:NULL];
    
    self.quickCommentTableView.hidden = !self.quickCommentTableView.hidden;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	
	//NSLog(@"locationManager:didUpdateLocations:");
	
	self.location = [locations lastObject];

	//NSLog(@"self.location = [locations lastObject];");
}

@end
