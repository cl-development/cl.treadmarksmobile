//
//  HelpViewController.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2014-04-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UILabel *helpViewTopicLabel;
- (IBAction)backButton:(id)sender;

- (IBAction)printButton:(id)sender;

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadPdfFile];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)printButton:(id)sender {
    /*
     TBD  As Suggested by Gabe we will  activate print when PDF file come in.
     
     UIPrintInteractionController *pc = [UIPrintInteractionController
     sharedPrintController];
     UIPrintInfo *printInfo = [UIPrintInfo printInfo];
     printInfo.outputType = UIPrintInfoOutputGeneral;
     // printInfo.jobName = self.query;
     pc.printInfo = printInfo;
     pc.showsPageRange = YES;
     */
}

-(void)loadPdfFile
{
    //TBD  when we get the PDF file replace Path for source with self.helpPdfFileName and
    //we have make sure the PDF file name is exactly what is in the Table cell .
    self.helpViewTopicLabel.text=self.helpPdfFileName;
    NSString *path = [[NSBundle mainBundle] pathForResource:self.helpPdfFileName ofType:@"pdf"];
    
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    UIWebView *pdfWebView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 90, 768, 934)];
    [[pdfWebView scrollView] setContentOffset:CGPointMake(0,500) animated:YES];
    [pdfWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0, 50.0)"]];
    [pdfWebView loadRequest:request];
    [pdfWebView setBackgroundColor:[UIColor lightGrayColor]];
    pdfWebView.scalesPageToFit = YES;
    [self.view addSubview:pdfWebView];
    
}
@end
