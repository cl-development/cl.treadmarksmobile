//
//  MaterialTypeTableViewCell.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-06-17.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation


class TOCMaterialTypeTableViewCell : UITableViewCell {
    @IBOutlet weak var myLabel:UILabel!
    var myCellLabel:UILabel
    var descriptionLabel:UILabel
    var customImage:UIImageView
    
    var labelText: String? {
        didSet {
            self.myCellLabel.text = labelText
        }
    }
    
    var descriptionText: String? {
        didSet {
            self.myCellLabel.text = labelText
          //  self.descriptionLabel = UILabel(frame: CGRectMake(19,21,500,36))
            let label2Font = UIFont(name: "Helvetica-Light", size: 16.0)
            self.descriptionLabel.font = label2Font
            self.contentView.addSubview(self.descriptionLabel)
            
            self.descriptionLabel.text = descriptionText
            
            let detailHeight = heightForLabel(descriptionText!, font: label2Font!, width: 550)
            self.descriptionLabel.frame = CGRect(x: 19, y: 35, width: 550, height: detailHeight);
            
            //return detailHeight+25;//Choose your custom row height

            
           // self.descriptionLabel.numberOfLines = 0
          // self.descriptionLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
           // self.descriptionLabel.frame.width = 200
          //  self.descriptionLabel.frame.height = CGFloat(80)
          //  self.descriptionLabel.sizeToFit()
          //  CGSize size = [self.descriptionLabel sizeThatFits:CGSizeMake(width, MAXFLOAT)];
            
            
           // float height        = size.height;
           // label.frame         = CGRectMake(x, y, width, height);
        }
    }

    // not called
    required init?(coder aDecoder: NSCoder) {
        self.myCellLabel = UILabel(frame:CGRect.zero)
        self.descriptionLabel = UILabel(frame:CGRect.zero)
        self.customImage = UIImageView (frame: CGRect.zero)
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        self.myCellLabel = UILabel(frame: CGRect(x: 19,y: 0,width: 550,height: 36))
        let label1Font = UIFont.boldSystemFont(ofSize: 28)
        self.myCellLabel.font = label1Font
        self.descriptionLabel = UILabel(frame: CGRect(x: 19,y: 35,width: 550,height: 21))
        let label2Font = UIFont(name: "Helvetica-Light", size: 16.0)
        self.descriptionLabel.font = label2Font
        self.customImage = UIImageView(frame: CGRect(x: 0, y: 60, width: 600, height: 2))
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.addSubview(self.myCellLabel)
        self.contentView.addSubview(self.descriptionLabel)
        self.labelText = "Not set"
        self.descriptionText = "Not set"

        
        
       

        
           }
    
    // this is not called
    /*override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }*/
    
    
    func heightForLabel(_ text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    
}
