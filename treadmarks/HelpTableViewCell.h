//
//  HelpTableViewCell.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2014-04-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpTableViewCell : UITableViewCell

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UILabel *helpHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *helpTopicLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImageView;

@end
