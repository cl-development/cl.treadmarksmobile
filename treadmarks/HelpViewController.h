//
//  HelpViewController.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2014-04-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
@property (strong) NSString     *helpPdfFileName;
@end
