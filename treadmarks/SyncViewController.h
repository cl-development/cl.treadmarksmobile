//
//  SyncViewController.h
//  TreadMarks
//
//  Created by Adi on 2015-10-12.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMSyncEngine.h"
#import "KAProgressLabel.h"
@interface SyncViewController : UIViewController<TMSyncEngineDelegate>
@property (strong, nonatomic) IBOutlet UIView *syncInProgressView;
@end
