//
//  main.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-16.
//  Copyright (c) 2013 Capris. All rights reserved.
//



#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
