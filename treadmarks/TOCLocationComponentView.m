//
//  TOCLocationComponentView.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2/13/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TOCLocationComponentView.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionType.h"
#import "Constants.h"
#import "OpenSansRegularLabel.h"
#import "OpenSansLightLabel.h"
#import "OpenSansRegularTextField.h"
#import "OpenSansLightTextField.h"
#import "OpenSansRegularButton.h"
#import "DataEngine.h"
#import "TOCLocationPopOverView.h"
#import "Transaction.h"
#import "TOCPageViewController.h"
#import "Location.h"
#import "Utils.h"
//#import "DockViewController.h"
#import "NSManagedObject+NSManagedObject.h"

#define TOC_LOCATION_COMPONENT_HEIGHT_ON 355
#define TOC_LOCATION_COMPONENT_HEIGHT_OFF 80
#define COMPONENT_VIEW_POSTION_IN_STC 3


@interface TOCLocationComponentView ()
//@property (weak, nonatomic) IBOutlet UIImageView *validateImageView;
@property (weak, nonatomic) IBOutlet UIView *tapToEnterView;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIImageView *taptoenterplusImage;
@property (weak, nonatomic) IBOutlet UIButton *toggleButton;
@property (nonatomic,strong) IBOutlet UILabel *groupnameLabel;
@property (nonatomic,strong) IBOutlet UILabel *phonetextLabel;

@property (nonatomic,strong) IBOutlet UILabel *latitudeLabel;
@property (nonatomic,strong) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationError;
@property (weak, nonatomic) IBOutlet UIButton *tapToEnterOutlet;

@property (nonatomic,strong) Location *locationData;
@property (nonatomic, assign) float componentHeight;
@property (nonatomic, assign) BOOL isLocationExist ;
@property (strong)UIViewController* locationpopoverVC;
@property (weak, nonatomic) Utils *util;
@end

@implementation TOCLocationComponentView
- (IBAction)editButton:(id)sender {
    
    TOCLocationPopOverView *locationpopover =[[ TOCLocationPopOverView alloc]init];
    locationpopover.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    locationpopover.transactionId = self.transaction.transactionId;
	locationpopover.tocLocationComponentView = self;
      //NSLog(@"Location ID sent to popover %@",self.tocTransaction.friendlyId);
    
    
    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;

	//UINavigationController *navigationController = self.parentPageViewController.dockViewController.navigationController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    locationpopover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:locationpopover animated:YES completion:nil];
}
- (IBAction)editbuttonPressed:(id)sender {
    
    
    TOCLocationPopOverView *locationpopover =[[ TOCLocationPopOverView alloc]init];
    locationpopover.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    locationpopover.transactionId = self.transaction.transactionId;
	locationpopover.tocLocationComponentView = self;
     //NSLog(@"Location ID sent to popover %@",self.tocTransaction.friendlyId);
    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;

	//UINavigationController *navigationController = self.parentPageViewController.dockViewController.navigationController;
	//navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    locationpopover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:locationpopover animated:YES completion:nil];
}


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH,TOC_LOCATION_COMPONENT_HEIGHT_OFF)];
    if (self) {
        [self connectNib];
    }
    self.status=OFF_SCREEN;
    self.heightOn  = TOC_LOCATION_COMPONENT_HEIGHT_ON;
    self.heightOff = TOC_LOCATION_COMPONENT_HEIGHT_OFF;
   
	/*
   
    
  // //NSLog(@"GET TRANS %@",self.util.getTransactionId);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishedGrabingLocationNotification:)
                                                 name:@"Location Grabed"
                                               object:nil];
	 */

    [self updateUI];
    
    return self;
}

#pragma mark - ComponentView

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
    //Register notification
    self.tocTransaction=self.transaction;
     }


- (void) updateEditButton {
	
	if (self.status == ON_SCREEN)
	{
		self.editButton.alpha = 1;
	}
	else
	{
		self.editButton.alpha = 0;
	}
}

-(void)setViewUI
{
    self.status=OFF_SCREEN;
   self.toggleButton.selected = self.status;
   
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                   animations:^{
                     [self updateEditButton];
               }];
}
-(void)finishedGrabingLocationNotification {
	
	if (self.editButton.alpha == 1)
		self.status=ON_SCREEN;
	else
		self.status=OFF_SCREEN;
	
    [self updateUI];
    
    self.tapToEnterView.alpha=0;
    self.taptoenterplusImage.alpha=0;
	self.toggleButton.alpha=1;
    
    self.transaction.transactionSyncStatusType = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    ////NSLog(@"Notification removed" );
}

-(void)updateUI{
    
 self.tocTransaction=self.transaction;
 //NSLog(@"Location ID created at UPDATE UI %@",self.tocTransaction.friendlyId);
    
   // if (self.readOnly==YES)//for confirmation Page Testing
    //{self.status=ON_SCREEN;}
    
    CGRect frame = self.view.frame, newFrame;
    if (self.status==ON_SCREEN)//ON_SCREEN
    {
        newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_LOCATION_COMPONENT_HEIGHT_ON);
        self.toggleButton.alpha=1;
        self.tapToEnterView.alpha=0;
        self.taptoenterplusImage.alpha=0;
        
        
    } else {
        newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_LOCATION_COMPONENT_HEIGHT_OFF);
        self.editButton.alpha=0;
    }
        if (self.doLocationDataExist==YES && self.status==OFF_SCREEN) {
            self.toggleButton.alpha=1;
          self.editButton.alpha=0;
            self.tapToEnterView.alpha=0;
            self.taptoenterplusImage.alpha=0;
            self.tapToEnterOutlet.alpha=0;
        }
        else {
            self.toggleButton.alpha=0;
            //self.editButton.alpha=1;
            self.tapToEnterView.alpha=1;
            self.taptoenterplusImage.alpha=1;
        }
    
    if([self.latitudeLabel.text isEqualToString:@"0.000000"])
    {
        self.locationError.alpha=1;
        self.locationError.text=@"Location Data Not Available";
        self.latitudeLabel.alpha=0;
        self.longitudeLabel.alpha=0;
    }
    else
    {self.locationError.alpha=0;
        self.latitudeLabel.alpha=1;
        self.longitudeLabel.alpha=1;

    }

    
  if(self.readOnly==YES)
  {
  
      self.toggleButton.alpha=0;
      self.editButton.alpha=0;
      self.tapToEnterView.alpha=0;
      self.taptoenterplusImage.alpha=0;
      self.tapToEnterOutlet.alpha=0;
  
  }
   // [self Sampledata];
    self.view.frame = newFrame;
    
    [self validate];
}

- (BOOL) doLocationDataExist //Check for location data and if exists pouplate the Label.
{
    //TOCPageViewController *tocTransaction;
    //TOCPageViewController *tocTransaction =[[TOCPageViewController alloc]init];
    
	self.locationData = (Location*)[Location singleById:self.tocTransaction.transactionId];
    //NSLog(@"Location ID for search in db %@",self.tocTransaction.friendlyId);
    //NSLog(@"Location ID from db %@",self.locationData.locationId);
    
    if (self.locationData.locationId==NULL)
        
    {  self.isLocationExist = NO;
   }
    else
    { self.isLocationExist = YES;
        //if exist preload the data form Location Table
        if(self.locationData.latitude==NULL)
        {self.latitudeLabel.text=@"0.000000";
            self.longitudeLabel.text=@"-0.000000";}
        else
        {self.latitudeLabel.text=self.locationData.latitude;
            self.longitudeLabel.text=self.locationData.longitude;}
        self.groupnameLabel.text=self.locationData.name;
        self.phonetextLabel.text=self.locationData.phone;
        }
    
    return self.isLocationExist;
}

-(void)confirmationView
{
    self.status = ON_SCREEN;
    [self updateUI];
    
    self.toggleButton.alpha=0;
    self.editButton.alpha=0;
    self.tapToEnterView.alpha=0;
    self.taptoenterplusImage.alpha=0;
    /*CGRect frame = self.view.frame, newFrame;
    newFrame = CGRectMake(frame.origin.x, frame.origin.y, COMPONENT_WIDTH, TOC_LOCATION_COMPONENT_HEIGHT_ON);
    self.locationData = [[DataEngine sharedInstance] locationForId:self.tocTransaction.friendlyId];
    self.view.frame = newFrame;
    self.latitudeLabel.text=self.locationData.latitude;
    self.longitudeLabel.text=self.locationData.longitude;
    self.groupnameLabel.text=self.locationData.name;
    self.phonetextLabel.text=self.locationData.phone;
    */

}

- (IBAction)toggleButtonPressed:(id)sender {
    self.status = 1-self.status;
    self.toggleButton.selected = self.status;
    [UIView animateWithDuration:COMPONENTS_ANIMATION_DURATION
                     animations:^{
                         [self updateEditButton];
                     }];
    [self.parentPageViewController getComponentsView:COMPONENT_VIEW_POSTION_IN_STC];
    [self.parentPageViewController updateUI];
}

-(void)validate{
    
    if ([self.groupnameLabel.text length]>0&&[self.phonetextLabel.text length]>=12)
        self.valid=YES;
    else
        self.valid=NO;
    [self updateValidateImage];
    [self.parentPageViewController validate];
}

-(void)updateValidateImage{
    if (self.valid) {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_OK_IMAGE];
    } else {
        self.validateImageView.image = [UIImage imageNamed:COMPONENT_VALIDATION_NOT_OK_IMAGE];
    }
}


- (IBAction)tapToEnterAction:(id)sender {
    TOCLocationPopOverView *locationpopover =[[ TOCLocationPopOverView alloc]init];
    locationpopover.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    locationpopover.transactionId = self.transaction.transactionId;
    locationpopover.tocLocationComponentView = self;
    //NSLog(@"Location ID sent to popover %@",self.tocTransaction.friendlyId);
    UIViewController* navigationController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    
    //UINavigationController *navigationController = self.parentPageViewController.dockViewController.navigationController;
    //navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    locationpopover.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [navigationController presentViewController:locationpopover animated:YES completion:nil];
}

@end
