//
//  AppDelegate.m
//  TreadMarks
//
//  Created by Dragos Ionel on 2013-10-16.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "AppDelegate.h"
#import "StartViewController.h"
#import "TMAFClient.h"
#import "TMSyncEngine.h"
#import "Location.h"
#import "Registrant.h"
#import "User.h"
#import "RegistrantType.h"
#import "TireType.h"
#import "TransactionType.h"
#import "TransactionSyncStatusType.h"
#import "TransactionType_TireType.h"
#import "TransactionType_RegistrantType.h"
#import "TransactionStatusType.h"
#import "DocumentType.h"
#import "ScaleTicketType.h"
#import "UnitType.h"
#import "PhotoPreselectComment.h"
#import "TransactionType_RegistrantType.h"
#import "Eligibility.h"
#import "PhotoType.h"
#import "TMSyncEngine.h"
#import "MHWCoreDataController.h"
#import "ModalViewController.h"
#import "SendFTP.h"
#import "LoginViewController.h"
#import "DataEngine.h"

#import "TreadMarks-Swift.h"
// Touchpose
#import "QTouchposeApplication.h"


@interface AppDelegate ()

@property (strong) UINavigationController *mainNavigationViewController;

@end

@implementation AppDelegate

static void displayStatusChanged(CFNotificationCenterRef center,
                                 void *observer,
                                 CFStringRef name,
                                 const void *object,
                                 CFDictionaryRef userInfo) {
	
	NSLog(@"displayStatusChanged: %@", name);
	
	if (name == CFSTR("com.apple.springboard.lockcomplete"))
	{
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"kDisplayStatusLocked"];
		[[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];

	}
    
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Unlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //application.statusBarHidden = YES;
    //TouchPose.
   // QTouchposeApplication *touchposeApplication = (QTouchposeApplication *)application;
    //touchposeApplication.alwaysShowTouches = YES;
    //touchposeApplication.touchColor=[UIColor colorWithRed:136.0f/255.0f green:195.0f/255.0f blue:79.0f/255.0f alpha:1.0f];

    
    
    
	/// Preload the fixed database, if necessary
	NSURL* sourceUrl = [[MHWCoreDataController sharedInstance] sourceStoreURL];
	NSString* sourcePath = sourceUrl.path;
	BOOL storeExists = [[NSFileManager defaultManager] fileExistsAtPath:sourcePath];
	if (storeExists == NO)
	{
		NSString* fixedStorePath = [[NSBundle mainBundle] pathForResource:FIXED_STORE_PATH ofType:@"sqlite"];
		NSError* error;
		if ([[NSFileManager defaultManager] copyItemAtPath:fixedStorePath toPath:sourcePath error:&error] == NO)
		{
			@throw [NSException exceptionWithName:@"Error copying fixed database."
										   reason:error.localizedDescription
										 userInfo:error.userInfo];
		}
		
		NSString* shmPath = [fixedStorePath stringByAppendingString:@"-shm"];
		NSString* walPath = [fixedStorePath stringByAppendingString:@"-wal"];
		NSString* shmSourcePath = [sourcePath stringByAppendingString:@"-shm"];
		NSString* walSourcePath = [sourcePath stringByAppendingString:@"-wal"];
		if ([[NSFileManager defaultManager] copyItemAtPath:shmPath toPath:shmSourcePath error:&error] == NO)
		{
			@throw [NSException exceptionWithName:@"Error copying fixed database."
										   reason:error.localizedDescription
										 userInfo:error.userInfo];
		}
		if ([[NSFileManager defaultManager] copyItemAtPath:walPath toPath:walSourcePath error:&error] == NO)
		{
			@throw [NSException exceptionWithName:@"Error copying fixed database."
										   reason:error.localizedDescription
										 userInfo:error.userInfo];
		}
        self.restrictRotation=NO;
    }


	NSMutableArray* classesToSync = [[NSMutableArray alloc] init];
	[classesToSync addObject:[Location class]];
	[classesToSync addObject:[Registrant class]];
	[classesToSync addObject:[User class]];
	[classesToSync addObject:[PhotoPreselectComment class]];
	[classesToSync addObject:[DocumentType class]];
	[classesToSync addObject:[ScaleTicketType class]];
	[classesToSync addObject:[TransactionSyncStatusType class]];
    [classesToSync addObject:[TireType class]];
	[classesToSync addObject:[TransactionStatusType class]];
	[classesToSync addObject:[TransactionType class]];
	[classesToSync addObject:[RegistrantType class]];
	[classesToSync addObject:[UnitType class]];
	[classesToSync addObject:[Eligibility class]];
    [classesToSync addObject:[PhotoType class]];
    [classesToSync addObject:[TransactionType_TireType class]];
    [classesToSync addObject:[TransactionType_RegistrantType class]];
    
    [self registerManagedObjectClassesToSync:classesToSync];
    
    NSString *dateString = APP_STATIC_DATA_EXTRACT_DATE;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dataExtractDate = [[NSDate alloc] init];
    dataExtractDate = [dateFormatter dateFromString:dateString];
    
    for (id aClass in classesToSync)
	{
		NSString *className = NSStringFromClass(aClass);
		
		if ([[TMSyncEngine sharedEngine] getLastUpdatedDate:className] == nil)
		{
			[[TMSyncEngine sharedEngine] setLastUpdatedDate:dataExtractDate forClass:NSStringFromClass(aClass)];
		}
    }
    

    // create log on device:
	if (NSLOG_TO_FILE)
	{
		// create log on device if needed:
		SendFTP *sftp = [[SendFTP alloc] init];
		NSString *pathToLogFile = [sftp logFilePath];
		
		if ([[NSFileManager defaultManager] fileExistsAtPath:pathToLogFile]==NO) {
			freopen([pathToLogFile cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
		}
	}
    
	/// The view controller layers will be in the following order from top to bottom:
	/// 1.  ModalViewController
	/// 2.  UINavigationController
	/// 2.1.  StartViewController / LoginViewController / DockViewController
	/// 2.2.  UINavigationController
	/// 2.2.1.  ListViewController ...
	
//	StartViewController* startViewController = [[StartViewController alloc] init];
//	
//	UINavigationController* navigationViewController = [[UINavigationController alloc] initWithRootViewController:startViewController];
//	navigationViewController.navigationBarHidden = YES;
    
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
	
	UINavigationController* navigationViewController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
	navigationViewController.navigationBarHidden = YES;
	
	self.mainNavigationViewController = navigationViewController;

	ModalViewController* modalViewController = [ModalViewController sharedModalViewController];
	[modalViewController addChildViewController:navigationViewController];
	[modalViewController.view addSubview:navigationViewController.view];
	[modalViewController.view sendSubviewToBack:navigationViewController.view];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = modalViewController;
    [self.window makeKeyAndVisible];
    self.window.backgroundColor =[UIColor whiteColor];

    
	/// Logout when device locks
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),
									nil,
									displayStatusChanged,
									CFSTR("com.apple.springboard.lockcomplete"),
									nil,
									CFNotificationSuspensionBehaviorDeliverImmediately);
	
   /* CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), //center
                                    NULL, // observer
                                    displayStatusChanged, // callback
                                    CFSTR("com.apple.springboard.lockstate"), // event name
                                    NULL, // object
                                    CFNotificationSuspensionBehaviorDeliverImmediately);*/


    
    return YES;
}
- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kDisplayStatusLocked"])
    {
        [self logout:NO];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"kDisplayStatusLocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
   //setenv("CFNETWORK_DIAGNOSTICS", "3", 1);
   // self.restrictRotation=NO;
	NSLog(@"applicationWillResignActive");
}
- (void)applicationDidEnterBackground:(UIApplication *)application{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	
	NSLog(@"applicationDidEnterBackground");
	UIApplicationState state = [[UIApplication sharedApplication] applicationState];
	if (state == UIApplicationStateInactive)
	{
		NSLog(@"Sent to background by locking screen");
	}
	else if (state == UIApplicationStateBackground)
	{
		if (![[NSUserDefaults standardUserDefaults] boolForKey:@"kDisplayStatusLocked"])
		{
			NSLog(@"Sent to background by home button/switching to other app");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Unlocked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
		else
		{
			NSLog(@"Sent to background by locking screen");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Unlocked"];
            [[NSUserDefaults standardUserDefaults] synchronize];

		}
	}
}
- (void)applicationWillEnterForeground:(UIApplication *)application{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	NSLog(@"applicationWillEnterForeground");
	
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"kDisplayStatusLocked"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	NSLog(@"applicationDidBecomeActive");

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"kDisplayStatusLocked"]) {
		[self logout:NO];

		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"kDisplayStatusLocked"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application{
    // Saves changes in the application's managed object context before the application terminates.
//    [self saveContext];
}

-(void) registerManagedObjectClassesToSync:(NSArray *)classesToSync {
    [[TMSyncEngine sharedEngine] registerNSManagedObjectClassesToSync:classesToSync];
}
- (void)logout:(BOOL)animated {
	
	/// Pop until the navigation is at the login screen
	UIViewController* viewController;
	UINavigationController *navigationController = self.mainNavigationViewController;
	for (viewController in navigationController.childViewControllers)
	{
		if ([viewController class] == [LoginViewController class])
		{
			LoginViewController* loginViewController = (LoginViewController*)viewController;
			[loginViewController reset];
			
			break;
		}
	}
	
	[navigationController popToViewController:viewController animated:animated];
	
	[[ModalViewController sharedModalViewController] hideView];
	[[ModalViewController sharedModalViewController] dismissViewControllerAnimated:NO completion:nil];
	
	/*
	/// Dismiss any other modal views
	UIViewController *rootViewController = [[UIApplication sharedApplication] keyWindow].rootViewController;
	[rootViewController dismissViewControllerAnimated:NO completion:nil];
	 */
}
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if(self.restrictRotation)
        return UIInterfaceOrientationMaskLandscapeLeft;
    else
        return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Helper

-(void)deleteScaleTicket:(ScaleTicket *)ticket {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    [context deleteObject:ticket];
}

@end
