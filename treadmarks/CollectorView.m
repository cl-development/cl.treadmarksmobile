//
//  CollectorView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "CollectorView.h"
#import "ModalViewController.h"

@implementation CollectorView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.collectorView createCornerRadius:self.collectorView];
        
    }
    
    return self;
}

#pragma mark - Button Actions
- (IBAction)primaryButton:(id)sender
{
    [self.delegate primaryAction:sender];
}
- (IBAction)secondryButton:(id)sender
{
    [self.delegate secondryAction:sender];
}

- (void) touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (touch.view == self.collectorView){}
    else
    {
        [[ModalViewController sharedModalViewController] hideView];
        
    }
    
}
- (void) touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if (touch.view == self.collectorView){}
    else
    {
        [[ModalViewController sharedModalViewController] hideView];
        
    }
    
}


@end
