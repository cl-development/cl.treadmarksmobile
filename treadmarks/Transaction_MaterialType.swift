//
//  Transaction_MaterialType.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-06-22.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation
import CoreData

@objc(Transaction_MaterialType)
class Transaction_MaterialType: NSManagedObject {

    @NSManaged var syncDate: Date
    @NSManaged var transactionMaterialTypeId: String
    @NSManaged var materialType: MaterialType
    @NSManaged var transaction: Transaction

}
