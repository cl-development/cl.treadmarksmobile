//
//  TransactionType_MaterialType.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-06-22.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation
import CoreData

@objc(TransactionType_MaterialType)
class TransactionType_MaterialType: NSManagedObject {

    @NSManaged var syncDate: Date
    @NSManaged var transactionTypeMaterialTypeId: NSNumber
    @NSManaged var materialType: NSManagedObject
    @NSManaged var transactionType: TransactionType
    
   @objc func setValuesWithDictionarySwift(_ dict:NSDictionary)->(Bool) {
        let myKeys = dict.allKeys as! [NSString]
        for key:NSString in myKeys {
            let theValue = dict.value(forKey: key as String)
            if (key.isEqual(to: "effectiveStartDate") || key.isEqual(to: "syncDate") || key.isEqual(to: "effectiveEndDate")) {
                let theDate:Date = TMSyncEngine.shared().unixDate(toNSDate: theValue as! String)
                self.syncDate = theDate
            }
            else if (key.isEqual(to: "transactionTypeId")) {
                let transType = DataEngine.sharedInstance().transactionType(forId: theValue as! NSNumber)
                let transT = self.managedObjectContext?.object(with: (transType?.objectID)!)
                self.transactionType = transT as! TransactionType
            }
            else if (key.isEqual(to: "MaterialTypeId")) {
                let matType = DataEngine.sharedInstance().materialType(forId: theValue as! NSNumber)
                let matT = self.managedObjectContext?.object(with: (matType?.objectID)!)
                self.materialType = matT as! MaterialType
            }
            else {
                self.setValue(theValue, forKey:key as String)
            }
        }
        return true
    }

}

/*
}  else if ([key isEqualToString:@"transactionTypeId"]) {
TransactionType *transT = [[DataEngine sharedInstance] transactionTypeForId:theValue];
TransactionType *transType = (TransactionType *)[self.managedObjectContext objectWithID:[transT objectID]];
self.transactionType = transType;
*/
