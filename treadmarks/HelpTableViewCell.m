//
//  HelpTableViewCell.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2014-04-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "HelpTableViewCell.h"

@implementation HelpTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
