//
//  UIView+CustomView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 29/07/15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import "UIView+CustomView.h"

@implementation UIView (CustomView)
-(UIView *)createCornerRadius:(UIView *)view
{
    //view.layer.borderWidth = 2;
    view.layer.cornerRadius = 20;
    view.layer.masksToBounds = YES;
   // view.layer.borderColor =[[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0]CGColor];
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:3.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    return view;
    
}
@end
