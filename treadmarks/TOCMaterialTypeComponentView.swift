//
//  TOCMaterialTypeComponentView.swift
//  TreadMarks
//
//  Created by Dennis on 2015-06-15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

import Foundation

class TOCMaterialTypeComponentView : TOCComponentView {
    
    @IBOutlet weak var editButton:UIButton!
    @IBOutlet weak var tapToEnterView:UIView!
    @IBOutlet weak var toggleButton:UIButton!
    @IBOutlet weak var materialTypeLabel:UILabel!
    @IBOutlet weak var materialselectedName:UILabel!
    @IBOutlet weak var materialselectedDescription :UILabel!
    var materialTypes:[Transaction_MaterialType]? = []

    
    fileprivate let COMPONENT_WIDTH =   CGFloat(680)    //NSNumber(float:680)
    fileprivate let TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON =   CGFloat(170)  //NSNumber(float:150)
    fileprivate let TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF =   CGFloat(80)  //NSNumber(float:80)
    fileprivate let OFF_SCREEN = false
    fileprivate let ON_SCREEN = true
    fileprivate var selectedType:Transaction_MaterialType? = nil
    
    var First_Time_Load = true
    
    override init(frame:CGRect) {
        super.init(frame:CGRect(x: frame.origin.x, y: frame.origin.y, width: COMPONENT_WIDTH, height: TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF))
        self.connectNib()
        self.status = OFF_SCREEN
        self.heightOn  = TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON
        self.heightOff = TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func connectNib() {
        let name:String = self.nameOfClass
        let test:[AnyObject] = Bundle.main.loadNibNamed(name, owner: self, options: nil)! as [AnyObject]
        let theView: UIView = test[0] as! UIView
        First_Time_Load = true
        self.addSubview(theView)
    }

    override func updateUI() ->() {
        self.initData()
        
        if let selType = self.selectedType {
            self.status=ON_SCREEN
            let matType:MaterialType = selType.materialType 
            self.materialTypeLabel.text = matType.shortNameKey
            self.materialselectedName.text = matType.shortNameKey
            self.materialselectedDescription.text = matType.nameKey
            self.materialTypeLabel.font = UIFont.boldSystemFont(ofSize: 28.0)
            let label2Font = UIFont(name: "Helvetica-Light", size: 23.0)
            self.materialselectedDescription!.font = label2Font
            self.tapToEnterView.isHidden=true
            self.toggleButton.isHidden=false
            self.editButton.isHidden=false
        }
        else {
            self.status=OFF_SCREEN
            self.tapToEnterView.isHidden=false
            self.toggleButton.isHidden=true
            self.editButton.isHidden=true
        }
        
        if (self.status==ON_SCREEN) {
            self.frame = CGRect(x: self.frame.origin.x,y: self.frame.origin.y,width: self.frame.size.width,height: TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_ON)
            self.heightOn = self.frame.size.height
            if First_Time_Load == true
            {
                First_Time_Load = false
                self.status=OFF_SCREEN
                self.toggleButton.isSelected = self.status
                self.frame = CGRect(x: self.frame.origin.x,y: self.frame.origin.y,width: self.frame.size.width,height: TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF)
                self.editButton.isHidden=true

                //self.heightOff = self.frame.size.height
            }
        }

        
        if (self.readOnly==true) {
            self.tapToEnterView.isHidden=true
            self.toggleButton.isHidden=true
            self.editButton.isHidden=true
        }
        else {
            self.toggleButton.isSelected = self.status;
            self.validate()
           // self.parentPageViewController!.updateUI()
        }
        
    }
    
    
    
    override func validate()->() {
        self.valid = self.selectedType != nil
        self.updateValidateImage()
        
    }
    
    
    @objc func setViewUI()->() {
        self.status=OFF_SCREEN
        self.toggleButton.isSelected = self.status
        self.frame = CGRect(x: self.frame.origin.x,y: self.frame.origin.y,width: self.frame.size.width,height: TOC_MATERIAL_TYPE_COMPONENT_HEIGHT_OFF)
       // self.updateUI()
    }
    
    @IBAction func toggleAction(_ sender:AnyObject?) {
    
        self.status = !self.status
        self.toggleButton.isSelected = self.status;
        UIView.animate(withDuration: 1, animations: ({
            self.updateEditButton()
        }))
        
        if(self.parentPageViewController!.transaction!.transactionType.transactionTypeId ==  8)
        {
            self.parentPageViewController!.getComponentsView(3);
        }
        self.parentPageViewController!.updateUI()
    }
    
    func updateUIAnimated()->(){
        UIView.animate(withDuration: 1,animations: ({
            self.updateNewUI()
        }))
    }
    
    func updateNewUI()->()
    {
        if (self.status == ON_SCREEN)
        {
            self.materialselectedName.alpha = 0
        }
        else
        {
            self.materialselectedName.alpha = 1
            
        }
    }
    
    func updateEditButton()->() {
        if (self.status == ON_SCREEN)
        {
            self.editButton.alpha = 1
            self.editButton.isHidden = false

        }
        else
        {
            self.editButton.alpha = 0
            self.editButton.isHidden = true

        }
        self.updateUIAnimated()

    }

    @IBAction func editButtonpressedAction(_ sender:AnyObject?) {
        self.showPopover()
        
    }
    @IBAction func tapToEnterAction(_ sender:AnyObject?) {
        self.showPopover()
    }
    
    func showPopover()->() {
        let popover : TOCMaterialPopoverViewController = TOCMaterialPopoverViewController.init(nibName: "TOCMaterialPopoverViewController", bundle: nil)
        popover.delegate = self
        popover.transaction = self.transaction
        
        popover.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        let navigationController:UINavigationController = self.parentPageViewController!.navigationController!
        
        popover.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController.present(popover, animated: true, completion: nil)
    }

   @objc func materialTypeComponentUpdate()->() {
        self.updateUI()
        self.parentPageViewController!.validate()
    }
    
    // MARK: - data initialization
    func initData () {
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        if let test : Transaction_MaterialType = engineInstance.transactionMaterialType(forTransactionId: self.transaction?.transactionId)  {                     self.selectedType = test;
        }
    }

}


public extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    public var nameOfClass: String{
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}

