//
//  Authorization.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class STCAuthorization, Transaction, TransactionType, User;

@interface Authorization : NSManagedObject

@property (nonatomic, retain) NSString * authorizationId;
@property (nonatomic, retain) NSString * authorizationNumber;
@property (nonatomic, retain) NSDate * expiryDate;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) STCAuthorization *stcauthorization;
@property (nonatomic, retain) Transaction *transaction;
@property (nonatomic, retain) TransactionType *transactionType;
@property (nonatomic, retain) User *user;

@end
