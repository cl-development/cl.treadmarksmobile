//
//  Registrant.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, RegistrantType;

@interface Registrant : NSManagedObject

@property (nonatomic, retain) NSDate * activationDate;
@property (nonatomic, retain) NSDate * activeStateChangeDate;
@property (nonatomic, retain) NSString * businessName;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSDate * lastUpdatedDate;
@property (nonatomic, retain) NSString * metaData;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSNumber * registrationNumber;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) RegistrantType *registrantType;

@end
