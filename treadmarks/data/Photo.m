//
//  Photo.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Photo.h"
#import "PhotoType.h"
#import "Transaction.h"


@implementation Photo

@dynamic comments;
@dynamic createdDate;
@dynamic date;
@dynamic fileName;
@dynamic imageSyncDate;
@dynamic latitude;
@dynamic longitude;
@dynamic photoId;
@dynamic syncDate;
@dynamic photoType;
@dynamic transaction;

@end
