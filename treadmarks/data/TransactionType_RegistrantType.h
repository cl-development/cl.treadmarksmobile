//
//  TransactionType_RegistrantType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RegistrantType, TransactionType;

@interface TransactionType_RegistrantType : NSManagedObject

@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * transactionTypeRegistrantTypeId;
@property (nonatomic, retain) RegistrantType *registrantType;
@property (nonatomic, retain) TransactionType *transactionType;

@end
