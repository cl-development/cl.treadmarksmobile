//
//  TransactionType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionType_TireType;
@class TransactionType_MaterialType;

@interface TransactionType : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * incomingSignatureKey;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSString * outgoingSignatureKey;
@property (nonatomic, retain) NSString * shortNameKey;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSString * tireCountMessageKey;
@property (nonatomic, retain) NSNumber * transactionTypeId;
@property (nonatomic, retain) NSSet *transactionType_TireTypes;
@property (nonatomic, retain) TransactionType_MaterialType *transactionType_MaterialType;
@end

@interface TransactionType (CoreDataGeneratedAccessors)

- (void)addTransactionType_TireTypesObject:(TransactionType_TireType *)value;
- (void)removeTransactionType_TireTypesObject:(TransactionType_TireType *)value;
- (void)addTransactionType_TireTypes:(NSSet *)values;
- (void)removeTransactionType_TireTypes:(NSSet *)values;

@end
