//
//  ScaleTicketType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ScaleTicketType : NSManagedObject

@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSNumber * scaleTicketTypeId;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;

@end
