//
//  TransactionType_Module.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Module, TransactionType;

@interface TransactionType_Module : NSManagedObject

@property (nonatomic, retain) NSNumber * isRequired;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSNumber * step;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * transactionTypeModuleId;
@property (nonatomic, retain) Module *module;
@property (nonatomic, retain) TransactionType *transactionType;

@end
