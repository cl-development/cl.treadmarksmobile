//
//  Message.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic messageId;
@dynamic nameKey;
@dynamic syncDate;

@end
