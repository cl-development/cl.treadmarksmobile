//
//  Transaction.m
//  TreadMarks
//
//  Created by Capris Group on 2014-07-09.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction.h"
#import "GPSLog.h"
#import "Location.h"
#import "Photo.h"
#import "Registrant.h"
#import "TransactionStatusType.h"
#import "TransactionSyncStatusType.h"
#import "TransactionType.h"
#import "Transaction_TireType.h"
#import "User.h"


@implementation Transaction

@dynamic createdDate;
@dynamic deviceName;
//@dynamic deviceIDFV;
@dynamic friendlyId;
@dynamic incomingSignatureName;
@dynamic modifiedDate;
@dynamic outgoingSignatureName;
@dynamic postalCode1;
@dynamic postalCode2;
@dynamic syncDate;
@dynamic trailerNumber;
@dynamic transactionDate;
@dynamic transactionId;
@dynamic versionBuild;
@dynamic createdUser;
@dynamic incomingGpsLog;
@dynamic incomingRegistrant;
@dynamic incomingSignaturePhoto;
@dynamic incomingUser;
@dynamic modifiedUser;
@dynamic outgoingGPSLog;
@dynamic outgoingRegistrant;
@dynamic outgoingSignaturePhoto;
@dynamic outgoingUser;
@dynamic trailerLocation;
@dynamic transaction_TireTypes;
@dynamic transactionStatusType;
@dynamic transactionSyncStatusType;
@dynamic transactionType;
@dynamic transaction_MaterialType;

@end
