//
//  UnitType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "UnitType.h"
#import "TMSyncEngine.h"


@implementation UnitType

@dynamic kgMultiplier;
@dynamic nameKey;
@dynamic syncDate;
@dynamic unitTypeId;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]==YES) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}

@end
