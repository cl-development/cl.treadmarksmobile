//
//  Document.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Document.h"
#import "DocumentType.h"
#import "Photo.h"
#import "Transaction.h"


@implementation Document

@dynamic date;
@dynamic documentId;
@dynamic documentName;
@dynamic documentNumber;
@dynamic documentTypeOther;
@dynamic sortIndex;
@dynamic syncDate;
@dynamic documentType;
@dynamic photo;
@dynamic transaction;

@end
