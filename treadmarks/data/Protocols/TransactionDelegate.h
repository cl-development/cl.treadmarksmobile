//
//  TransactionDelegate.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/22/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TransactionDelegate <NSObject>

- (void)transactionDidVoid;

@optional

- (void)cancelVoidProcess;

@end
