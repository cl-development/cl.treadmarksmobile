//
//  PhotoType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "PhotoType.h"
#import "Photo.h"


@implementation PhotoType

@dynamic name;
@dynamic photoTypeId;
@dynamic syncDate;
@dynamic photos;

@end
