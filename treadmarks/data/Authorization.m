//
//  Authorization.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Authorization.h"
#import "STCAuthorization.h"
#import "Transaction.h"
#import "TransactionType.h"
#import "User.h"


@implementation Authorization

@dynamic authorizationId;
@dynamic authorizationNumber;
@dynamic expiryDate;
@dynamic syncDate;
@dynamic stcauthorization;
@dynamic transaction;
@dynamic transactionType;
@dynamic user;

@end
