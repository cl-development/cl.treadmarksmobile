//
//  Transaction_TireType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_TireType.h"
#import "TireType.h"
#import "Transaction.h"


@implementation Transaction_TireType

@dynamic quantity;
@dynamic syncDate;
@dynamic transactionTireTypeId;
@dynamic tireType;
@dynamic transaction;

@end
