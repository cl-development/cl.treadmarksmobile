//
//  UnitType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UnitType : NSManagedObject

@property (nonatomic, retain) NSNumber * kgMultiplier;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * unitTypeId;

@end
