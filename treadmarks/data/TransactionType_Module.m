//
//  TransactionType_Module.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionType_Module.h"
#import "Module.h"
#import "TransactionType.h"


@implementation TransactionType_Module

@dynamic isRequired;
@dynamic sortIndex;
@dynamic step;
@dynamic syncDate;
@dynamic transactionTypeModuleId;
@dynamic module;
@dynamic transactionType;

@end
