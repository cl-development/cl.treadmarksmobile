//
//  Registrant.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Registrant.h"
#import "Location.h"
#import "RegistrantType.h"
#import "DataEngine.h"
#import "TMSyncEngine.h"


@implementation Registrant

@dynamic activationDate;
@dynamic activeStateChangeDate;
@dynamic businessName;
@dynamic createdDate;
@dynamic isActive;
@dynamic lastUpdatedDate;
@dynamic metaData;
@dynamic modifiedDate;
@dynamic registrationNumber;
@dynamic syncDate;
@dynamic location;
@dynamic registrantType;



-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *dateMembers = [NSArray arrayWithObjects:@"syncDate",@"createdDate",@"lastUpdatedDate",@"activationDate",
                            @"activeStateChangeDate",@"modifiedDate", nil];
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"registrantTypeId"]==YES) {
            RegistrantType *regType = [[DataEngine sharedInstance] registrantTypeForId:theValue];
            RegistrantType *obj = (RegistrantType *)[self.managedObjectContext objectWithID:[regType objectID]];
            self.registrantType = obj;
        }
        else if ([key isEqualToString:@"locationId"]==YES) {
            Location *loc = [[DataEngine sharedInstance] locationForId:theValue];
            if (loc==nil) {
                return NO;
            }
            Location *obj = (Location *)[self.managedObjectContext objectWithID:[loc objectID]];
            self.location = obj;
        }
        else if ([dateMembers containsObject:key]==YES)  {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
          //  dispatch_sync(dispatch_get_main_queue(), ^{
                [self setValue:theDate forKey:key];
        //    });
        }
        else {
          //  dispatch_sync(dispatch_get_main_queue(), ^{
                [self setValue:theValue forKey:key];
         //   });
        }
    }
    return YES;
}

@end
