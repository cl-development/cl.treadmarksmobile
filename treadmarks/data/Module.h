//
//  Module.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Module : NSManagedObject

@property (nonatomic, retain) NSNumber * moduleId;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSDate * syncDate;

@end
