//
//  Photo+Photo.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-26.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Photo+Photo.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Utils.h"
#import "PhotoType.h"
#import "GPSLog.h"
#import "Transaction.h"
#import "UIImage+Resize.h"

@implementation Photo (Photo)

+ (id)insertNewObjectForTransaction:(Transaction*)transaction
							 ofType:(PhotoType*)photoType
					   withFileName:(NSString*)fileName
						andLocation:(GPSLog*)gpsLog {

	Photo* photo = [super insertNewObject:[transaction managedObjectContext]];
	photo.photoId = [[NSUUID UUID] UUIDString];
	photo.createdDate = [NSDate date];
	photo.date = photo.createdDate;
	photo.fileName = fileName;
	photo.photoType = photoType;
	photo.transaction = transaction;
	photo.latitude = [gpsLog.latitude stringValue];
	photo.longitude = [gpsLog.longitude stringValue];
	
	return photo;
}
+ (Photo *)savePhoto:(UIImage *)photoImage
			  ofType:(PhotoType*)photoType
	  forTransaction:(Transaction*)transaction
		withFileName:(NSString *)fileName
		 andLocation:(GPSLog*)gpsLog {
	
	if (fileName == nil)
	{
		fileName = [Utils nameForPhoto];
	}
	
	NSString* filePath = [Utils pathForPhotoWithPhotoName:fileName];
	
	@autoreleasepool
	{
		/// Only save signatures as original size
		if ([photoType.photoTypeId isEqualToNumber:PHOTO_TYPE_ID_SIGNATURE])
		{
			if ([UIImagePNGRepresentation(photoImage) writeToFile:filePath atomically:YES] == NO)
			{
                @throw [NSException exceptionWithName:@"" reason:nil userInfo:nil];
			}
		}
		/// For photos or scale tickets, save smaller version
		else if ([photoType.photoTypeId isEqualToNumber:PHOTO_TYPE_ID_PHOTO] ||
			[photoType.photoTypeId isEqualToNumber:PHOTO_TYPE_ID_SCALE_TICKET])
		{
			CGSize newSize = CGSizeMake(photoImage.size.width / 4, photoImage.size.height / 4);
			UIImage* resizedImage = [photoImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit
																	 bounds:newSize
													   interpolationQuality:kCGInterpolationHigh];
			filePath = [filePath stringByReplacingOccurrencesOfString:@".png" withString:@"_small.png"];
			
			if ([UIImagePNGRepresentation(resizedImage) writeToFile:filePath atomically:YES] == NO)
			{
				@throw [NSException exceptionWithName:@"" reason: nil userInfo: nil];
			}
		}
	}
	
	Photo* photo = [Photo insertNewObjectForTransaction:transaction
												 ofType:photoType
										   withFileName:fileName
											andLocation:gpsLog];
	
	return photo;
}

@end
