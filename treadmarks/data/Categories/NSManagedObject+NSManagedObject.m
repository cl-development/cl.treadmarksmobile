//
//  NSManagedObject+NSManagedObject.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-06.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "NSManagedObject+NSManagedObject.h"
#import "DataEngine.h"

@implementation NSManagedObject (NSManagedObject)

+ (NSUInteger)count {
	
	NSError* error;
	NSArray* result = [[[DataEngine sharedInstance] managedObjectContext] executeFetchRequest:[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])]
																						error:&error];
	if (error != NULL)
	{
#ifdef DEBUG
		@throw [[NSException alloc] initWithName:@"Exception"
										  reason:[error description]
										userInfo:[error userInfo]];
#endif
	}
	
	return [result count];
}
+ (id)insertNewObject:(NSManagedObjectContext *)context {
	
	return [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
										 inManagedObjectContext:context];
}
+ (void)prePopulateData {
	
	/// Override
}
+ (id)singleById:(id)recordId {
	
	return [self context:[[DataEngine sharedInstance] managedObjectContext]
			  singleById:recordId];
}
+ (id)single:(NSString *)predicateFormat, ... {
	
	va_list args;
	va_start(args, predicateFormat);
	
	NSManagedObject* result = [self context:[[DataEngine sharedInstance] managedObjectContext]
									 single:predicateFormat
								  arguments:args];
	
	va_end(args);
	
	return result;
}
+ (id)context:(NSManagedObjectContext*)context single:(NSString *)predicateFormat, ... {
	
	va_list args;
	va_start(args, predicateFormat);
	
	NSManagedObject* result = [self context:context single:predicateFormat arguments:args];
	
	va_end(args);
	
	return result;
}
+ (id)context:(NSManagedObjectContext*)context single:(NSString *)predicateFormat arguments:(va_list)argList {
	
	NSArray* results = [self context:context where:predicateFormat arguments:argList];

	NSUInteger count = [results count];
	switch (count)
	{
		case 0: return NULL;
		case 1:	return results[0];
		default:
#ifdef DEBUG
			@throw [NSException exceptionWithName:@"Exception"
										   reason:@"More than one record returned."
										 userInfo:NULL];
#endif
			return NULL;
	}
}
+ (id)context:(NSManagedObjectContext*)context singleById:(id)recordId {
	
	return [self context:context single:@"%K == %@", [self recordIdAttributeName], recordId];
}
+ (NSArray*)where:(NSString *)predicateFormat, ... {
	
	va_list args;
	va_start(args, predicateFormat);
	
	NSArray* array = [self context:[[DataEngine sharedInstance] managedObjectContext]
							 where:predicateFormat
						 arguments:args];
	
	va_end(args);
	
	return array;
}
+ (NSArray*)where:(NSString *)predicateFormat arguments:(va_list)argList {
	
	return [self context:[[DataEngine sharedInstance] managedObjectContext]
				   where:predicateFormat
			   arguments:argList];
}
+ (NSArray*)context:(NSManagedObjectContext*)context where:(NSString *)predicateFormat, ... {
	
	va_list args;
	va_start(args, predicateFormat);
	
	NSArray* array = [self context:context where:predicateFormat arguments:args];
	
	va_end(args);
	
	return array;
}
+ (NSArray*)context:(NSManagedObjectContext*)context where:(NSString *)predicateFormat arguments:(va_list)argList {
	
	NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([self class])];
	
	if (predicateFormat != nil)
	{
		[request setPredicate:[NSPredicate predicateWithFormat:predicateFormat arguments:argList]];
	}
	
	NSError* error;
	NSArray* results = [context executeFetchRequest:request error:&error];
	if (results == NULL)
	{
#ifdef DEBUG
		@throw [NSException exceptionWithName:@"Exception"
									   reason:[error description]
									 userInfo:[error userInfo]];
#endif
	}
	
	return results;
}
+ (NSString *)recordIdAttributeName {

	NSMutableString* attributeName = [[NSMutableString alloc] initWithString:NSStringFromClass([self class])];
	[attributeName replaceCharactersInRange:NSMakeRange(0, 1) withString:[[attributeName substringToIndex:1] lowercaseString]];
	[attributeName appendString:@"Id"];
	
	return attributeName;
}
+ (NSArray*)toArray {
	
	return [self where:nil];
}

@end
