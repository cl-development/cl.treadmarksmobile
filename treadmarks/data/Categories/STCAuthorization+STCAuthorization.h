//
//  STCAuthorization+STCAuthorization.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "STCAuthorization.h"

@interface STCAuthorization (STCAuthorization)

+ (id)insertNewObjectWithAuthorization:(Authorization*)authorization andLocation:(Location*)location;

@end
