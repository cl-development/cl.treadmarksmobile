//
//  Transaction_Eligibility+Transaction_Eligibility.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-23.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_Eligibility.h"

@interface Transaction_Eligibility (Transaction_Eligibility)

+ (id)insertNewObject:(NSManagedObjectContext *)context forTransaction:(Transaction*)transaction andEligibility:(Eligibility*)eligibility;

@end
