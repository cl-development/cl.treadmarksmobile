//
//  Transaction_Eligibility+Transaction_Eligibility.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-23.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_Eligibility+Transaction_Eligibility.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation Transaction_Eligibility (Transaction_Eligibility)

+ (id)insertNewObject:(NSManagedObjectContext *)context forTransaction:(Transaction*)transaction andEligibility:(Eligibility*)eligibility {
	
	if (eligibility == nil)
	{
#if DEBUG
		@throw [NSException exceptionWithName:@"Null Argument Exception"
									   reason:@"eligibility cannot be nil"
									 userInfo:nil];
#endif
	}
	
	Transaction_Eligibility* transactionEligibility = [super insertNewObject:context];
	transactionEligibility.transactionEligibilityId = [[NSUUID UUID] UUIDString];
	transactionEligibility.transaction = transaction;
	transactionEligibility.eligibility = eligibility;
	
	return transactionEligibility;
}

@end
