//
//  Transaction_TireType+Transaction_TireType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_TireType.h"

@interface Transaction_TireType (Transaction_TireType)

+ (id)insertNewObject:(NSManagedObjectContext *)context
	   forTransaction:(Transaction*)transaction
		 withTireType:(TireType*)tireType;

@end
