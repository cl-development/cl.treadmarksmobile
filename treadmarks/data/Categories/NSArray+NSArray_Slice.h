//
//  NSArray+NSArray_Slice.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-06-12.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (NSArray_Slice)
- (NSArray *)arrayBySlicingFrom:(NSInteger)start to:(NSInteger)stop;
@end
