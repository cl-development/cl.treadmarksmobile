//
//  TransactionType+TransactionType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-04-01.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionType+TransactionType.h"
#import "NSManagedObject+NSManagedObject.h"
#import "DataEngine.h"

@implementation TransactionType (TransactionType)

+ (void)prePopulateData {
	
	[super prePopulateData];
	
	/// The initial data population is taken care by the old way.
	/// Only need to apply patch here:
	TransactionType* ptr = (TransactionType*)[self singleById:TRANSACTION_TYPE_ID_PTR];
	NSString* incomingSignatureKey = @"I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector,_an_unregistered_collection_site_or_an_eligible_OTS_Special_Collection_Event_after_August_31,_2009.";
	if ([ptr.incomingSignatureKey isEqualToString:incomingSignatureKey] == NO)
	{
		ptr.incomingSignatureKey = incomingSignatureKey;
		ptr.outgoingSignatureKey = @"I_acknowledge_receipt_of_the_tires_above_for_processing_/_recycling_on_the_delivery_date_indicated.";
		
		NSManagedObjectContext* context = [[DataEngine sharedInstance] managedObjectContext];
		NSError* error;
		if ([context save:&error] == NO)
		{
#ifdef DEBUG
			@throw [NSException exceptionWithName:@"Exception"
										   reason:[error description]
										 userInfo:[error userInfo]];
#endif
		}
	}
}

@end
