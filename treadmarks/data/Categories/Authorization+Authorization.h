//
//  Authorization+Authorization.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Authorization.h"

@interface Authorization (Authorization)

+ (id)insertNewObjectForTransaction:(Transaction*)transaction;

@end
