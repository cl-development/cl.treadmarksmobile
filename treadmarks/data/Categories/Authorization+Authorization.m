//
//  Authorization+Authorization.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-22.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Authorization+Authorization.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Utils.h"
#import "Transaction+Transaction.h"

@implementation Authorization (Authorization)

+ (id)insertNewObjectForTransaction:(Transaction*)transaction {
	
	Authorization* authorization = [super insertNewObject:[transaction managedObjectContext]];
	authorization.authorizationId = [[NSUUID UUID] UUIDString];
	authorization.user = [Utils getIncomingUser];
	authorization.transaction = transaction;
	authorization.transactionType = transaction.transactionType;
	
	return authorization;
}

@end
