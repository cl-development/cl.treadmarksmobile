//
//  Comment+Comment.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Comment.h"
#import "NSManagedObject+NSManagedObject.h"

@interface Comment (Comment)

+ (id)insertNewObject:(NSManagedObjectContext *)context
	   forTransaction:(Transaction*)transaction
			   byUser:(User*)user
			 withText:(NSString*)text;

@end
