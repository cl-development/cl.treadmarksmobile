//
//  Location+Location.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-04-15.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Location.h"

@interface Location (Location)

+ (NSString *)formatLocationId:(NSNumber *)number;

@end
