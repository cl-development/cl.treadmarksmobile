//
//  Transaction+Transaction.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-20.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction.h"

enum ParticipantType {
	IncomingParticipant = INCOMING_USER,
	OutgoingParticipant = OUTGOING_USER
};

@interface Transaction (Transaction)

- (void)clearSyncStatus;
+ (id)insertNewObject:(NSManagedObjectContext *)context
   forTransactionType:(TransactionType*)transactionType;
- (void)setSignature:(UIImage *)signatureImage
	  forParticipant:(enum ParticipantType)participantType
			withName:(NSString *)name
		 andLocation:(GPSLog*)gpsLog;
- (void)addPhoto:(UIImage *)photoImage withLocation:(GPSLog*)gpsLog andComment:(NSString*)comment;
+ (void)loadTestTransactions:(User*)hauler;

@end
