//
//  GPSLog+GPSLog.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-26.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "GPSLog+GPSLog.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation GPSLog (GPSLog)

+ (NSString *)recordIdAttributeName {
	
	return @"gpsLogId";
}
+ (id)insertNewObject:(NSManagedObjectContext *)context {
	
	GPSLog* gpsLog = [super insertNewObject:context];
	gpsLog.gpsLogId = [[NSUUID UUID] UUIDString];
	gpsLog.timestamp = [NSDate date];
	
	return gpsLog;
}

@end
