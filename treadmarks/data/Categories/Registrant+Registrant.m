//
//  Registrant+Registrant.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-20.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Registrant+Registrant.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation Registrant (Registrant)

+ (NSString *)recordIdAttributeName {
	
	return @"registrationNumber";
}

@end
