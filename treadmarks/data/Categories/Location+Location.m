//
//  Location+Location.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-04-15.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Location+Location.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation Location (Location)

+ (NSString *)formatLocationId:(NSNumber *)number {
	
	NSString* locationID = [number stringValue];
	NSString* uuid = @"00000000-0000-0000-0000-000000000000";
	locationID = [uuid stringByReplacingCharactersInRange:NSMakeRange([uuid length] - [locationID length], [locationID length]) withString:locationID];
	
	return locationID;
}
+ (id)insertNewObject:(NSManagedObjectContext *)context {
	
	Location* location = [super insertNewObject:context];
	location.locationId = [[NSUUID UUID] UUIDString];
	
	return location;
}

@end
