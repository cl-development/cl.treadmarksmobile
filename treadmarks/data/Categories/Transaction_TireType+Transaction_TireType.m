//
//  Transaction_TireType+Transaction_TireType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_TireType+Transaction_TireType.h"
#import "NSManagedObject+NSManagedObject.h"

@implementation Transaction_TireType (Transaction_TireType)

+ (id)insertNewObject:(NSManagedObjectContext *)context
	   forTransaction:(Transaction*)transaction
		 withTireType:(TireType*)tireType {
	
	Transaction_TireType* transaction_TireType = [super insertNewObject:context];
	transaction_TireType.transactionTireTypeId = [[NSUUID UUID] UUIDString];
	transaction_TireType.transaction = transaction;
	transaction_TireType.tireType = tireType;
	
	return transaction_TireType;
}


@end
