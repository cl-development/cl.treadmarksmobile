//
//  Transaction+Transaction.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-20.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction+Transaction.h"
#import "TransactionSyncStatusType.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Utils.h"
#import "User.h"
#import "GPSLog.h"
#import "TransactionStatusType.h"
#import "Photo+Photo.h"
#import "DataEngine.h"
#import "PhotoType.h"
#import "TireType.h"
#import "TransactionType_TireType.h"
#import "Transaction_TireType+Transaction_TireType.h"
#import "Comment+Comment.h"
#import "Location+Location.h"
#import "Transaction_Eligibility+Transaction_Eligibility.h"
#import "Authorization+Authorization.h"
#import "STCAuthorization+STCAuthorization.h"
#import "ScaleTicket+ScaleTicket.h"


@implementation Transaction (Transaction)

- (void)clearSyncStatus {
	
	self.transactionSyncStatusType = (TransactionSyncStatusType*)[TransactionSyncStatusType singleById:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
}
+ (id)insertNewObject:(NSManagedObjectContext *)context
   forTransactionType:(TransactionType*)transactionType {

	Transaction* transaction = [super insertNewObject:context];
	transaction.transactionId = [[NSUUID UUID] UUIDString];
	transaction.friendlyId = [Utils createFriendlyTransactionId:transactionType];
	transaction.transactionDate = [NSDate date];
	transaction.transactionType = transactionType;
	transaction.incomingUser = [Utils getIncomingUser];
	transaction.incomingGpsLog = [GPSLog singleById:[Utils getGPSLogId]];
	transaction.incomingRegistrant = [Utils getIncomingRegistrant];
	transaction.createdUser = [Utils getIncomingUser];//[Utils getIncomingUser]
	transaction.createdDate = transaction.transactionDate;
	transaction.deviceName = [[UIDevice currentDevice] name];
//    transaction.deviceIDFV = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
	transaction.transactionStatusType = [TransactionStatusType singleById:TRANSACTION_STATUS_TYPE_ID_INCOMPLETE];
	transaction.transactionSyncStatusType = [TransactionSyncStatusType singleById:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
	
	/// Create Transaction_TireType records as well
	for (TransactionType_TireType* transactionType_TireType in transactionType.transactionType_TireTypes)
	{
		TireType* tireType = transactionType_TireType.tireType;
		[Transaction_TireType insertNewObject:context
							   forTransaction:transaction
								 withTireType:tireType];
	}
	
	return transaction;
}
- (void)setSignature:(UIImage *)signatureImage
	  forParticipant:(enum ParticipantType)participantType
			withName:(NSString *)name
		 andLocation:(GPSLog*)gpsLog {

	NSString* fileName = [NSString stringWithFormat:@"%@-%u.png", self.transactionId, participantType];
	Photo *photo = [Photo savePhoto:signatureImage
							 ofType:[PhotoType singleById:PHOTO_TYPE_ID_SIGNATURE]
					 forTransaction:self
					   withFileName:fileName
						andLocation:gpsLog];
	
	if (participantType == IncomingParticipant)
	{
		self.incomingSignaturePhoto = photo;
		self.incomingSignatureName = name;
	}
	else
	{
		self.outgoingSignaturePhoto = photo;
		self.outgoingSignatureName = name;
	}
}
- (void)addPhoto:(UIImage *)photoImage withLocation:(GPSLog *)gpsLog andComment:(NSString *)comment {

	NSString* fileName = [Utils nameForPhoto];
	Photo* photo = [Photo savePhoto:photoImage
							 ofType:[PhotoType singleById:PHOTOTYPE_ID_PHOTO]
					 forTransaction:self
					   withFileName:fileName
						andLocation:gpsLog];
	photo.comments = comment;
}
+ (void)loadTestTransactions:(User*)hauler {
	
    
    
	NSManagedObjectContext* context = [[DataEngine sharedInstance] managedObjectContext];
	
	/// Create 50 TCR transactions
	NSArray* haulers = [User where:@"registrant.registrantType == %@", [RegistrantType singleById:REGISTRANT_TYPE_ID_HAULER]];
	NSArray* collectors = [User where:@"registrant.registrantType == %@", [RegistrantType singleById:REGISTRANT_TYPE_ID_COLLECTOR]];
	NSArray* processors = [User where:@"registrant.registrationNumber BEGINSWITH %@", @"40"];
	
	/// Refactored objects used in the for-loop
	UIImage* signatureImage = [UIImage imageNamed:@"signature"];
	TransactionStatusType* transactionStatusTypeComplete = [TransactionStatusType singleById:TRANSACTION_STATUS_TYPE_ID_COMPLETE];
	Eligibility* eligibilityEligible = [Eligibility singleById:ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE];
	UIImage* photo = [UIImage imageNamed:@"photo"];
	NSArray* photoPreselectComments = [PhotoPreselectComment toArray];
	GPSLog* gpsLog;
	Transaction* transaction;
	PhotoPreselectComment* photoPreselectComment;
	
	for (int i = 0; i < TEST_TCR_TRANSACTIONS + TEST_STC_TRANSACTIONS + TEST_PTR_TRANSACTIONS; i++)
	{
		int k = (IS_RANDOM) ? arc4random() % (TEST_TCR_TRANSACTIONS + TEST_STC_TRANSACTIONS + TEST_PTR_TRANSACTIONS) : i;
		
		TransactionType* transactionType;
		
		if (k < TEST_TCR_TRANSACTIONS)
		{
			if ([transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_TCR] == NO)
			{
				transactionType = [TransactionType singleById:TRANSACTION_TYPE_ID_TCR];
			}
		}
		else if (k < TEST_TCR_TRANSACTIONS + TEST_STC_TRANSACTIONS)
		{
			if ([transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_STC] == NO)
			{
				transactionType = [TransactionType singleById:TRANSACTION_TYPE_ID_STC];
			}
		}
		else
		{
			if ([transactionType.transactionTypeId isEqualToNumber:TRANSACTION_TYPE_ID_PTR] == NO)
			{
				transactionType = [TransactionType singleById:TRANSACTION_TYPE_ID_PTR];
			}
		}
		
		/// Random GPS log
		gpsLog = [GPSLog insertNewObject:context];
		gpsLog.latitude = [NSNumber numberWithDouble:((double)arc4random() / 0x100000000) * 180.0f - 90.0f];
		gpsLog.longitude = [NSNumber numberWithDouble:((double)arc4random() / 0x100000000) * 360.0f - 180.0f];
		
		/// Set random hauler
		if (hauler == nil)
		{
			hauler = haulers[arc4random() % [haulers count]];
			[Utils setUserId:hauler.userId];
			[Utils setRegistrantId:hauler.registrant.registrationNumber];
		}
		
		transaction = [Transaction insertNewObject:context
								forTransactionType:transactionType];
		
		/// Add random tire counts
		for (Transaction_TireType* transaction_TireType in transaction.transaction_TireTypes)
		{
			transaction_TireType.quantity = [NSNumber numberWithUnsignedInteger:arc4random() % RANDOM_MAX_TIRE_QUANTITY / [transaction_TireType.tireType.estimatedWeight integerValue]];
		}
		
		/// Add 3 photos
		for (int j = 0; j < TEST_NUMBER_OF_PHOTOS; j++)
		{
			photoPreselectComment = photoPreselectComments[arc4random() % [photoPreselectComments count]];
			[transaction addPhoto:photo
					 withLocation:gpsLog
					   andComment:photoPreselectComment.nameKey];
		}
		
		/// Add comments
		[Comment insertNewObject:context
				  forTransaction:transaction
						  byUser:hauler
						withText:[NSString stringWithFormat:@"This is a %@ transaction created at: %@",
								  transactionType.shortNameKey,
								  [NSDate date]]];
        
        
        
		
	//	UNComment for photo
        //Magic code do not touch
        /// Create signature photo file
		[transaction setSignature:signatureImage
				   forParticipant:IncomingParticipant
						 withName:hauler.registrant.businessName
					  andLocation:transaction.incomingGpsLog];
		
		transaction.transactionStatusType = transactionStatusTypeComplete;
		
		/// Transaction specific components
		if (k < TEST_TCR_TRANSACTIONS)
		{
			/// TCR transaction
			/// Set random collector
			User* collectorUser = collectors[arc4random() % [collectors count]];
			transaction.outgoingUser = collectorUser;
			transaction.outgoingRegistrant = collectorUser.registrant;
			
			if ([collectorUser.registrant.location.postalCode length] < 3)
			{
				transaction.postalCode1 = collectorUser.registrant.location.postalCode;
				transaction.postalCode2 = @"";
			}
			else
			{
				transaction.postalCode1 = [collectorUser.registrant.location.postalCode substringToIndex:3];
				transaction.postalCode2 = [collectorUser.registrant.location.postalCode substringWithRange:NSMakeRange(3, 3)];
			}
			
			/// Add eligibility
			Transaction_Eligibility* transaction_Eligibility = [Transaction_Eligibility insertNewObject:context
																						 forTransaction:transaction
																						 andEligibility:eligibilityEligible];
			transaction_Eligibility.value = @YES;

			transaction.outgoingGPSLog = gpsLog;
			[transaction setSignature:signatureImage
					   forParticipant:OutgoingParticipant
							 withName:collectorUser.registrant.businessName
						  andLocation:transaction.outgoingGPSLog];
		}
		else if (k < TEST_TCR_TRANSACTIONS + TEST_STC_TRANSACTIONS)
		{
			/// STC transaction
			Authorization* authorization = [Authorization insertNewObjectForTransaction:transaction];
			authorization.authorizationNumber = [NSString stringWithFormat:@"%u", arc4random() % 100000];
			
			Location* location = [Location insertNewObject:context];
			location.locationId = transaction.transactionId;
			location.latitude = [[NSNumber numberWithDouble:((double)arc4random() / 0x100000000) * 180.0f - 90.0f] stringValue];
			location.longitude = [[NSNumber numberWithDouble:((double)arc4random() / 0x100000000) * 360.0f - 180.0f] stringValue];
			location.name = hauler.registrant.businessName;
			location.phone = hauler.registrant.location.phone;
			
			[STCAuthorization insertNewObjectWithAuthorization:authorization
												   andLocation:location];

			transaction.outgoingGPSLog = gpsLog;
		[transaction setSignature:signatureImage
					   forParticipant:OutgoingParticipant
							 withName:hauler.registrant.businessName
						  andLocation:gpsLog];
		}
		else
		{
			/// PTR transaction
			User* processorUser = processors[arc4random() % [processors count]];
			transaction.outgoingUser = processorUser;
			transaction.outgoingRegistrant = processorUser.registrant;
			
			ScaleTicket* inboundScaleTicket = [ScaleTicket insertNewObjectForTransaction:transaction
																			   withPhoto:photo
																	  andScaleTicketType:InboundScaleTicket
																			 andLocation:gpsLog];
			inboundScaleTicket.ticketNumber = [NSString stringWithFormat:@"%u", arc4random() % 10000];
			inboundScaleTicket.inboundWeight = [NSDecimalNumber decimalNumberWithMantissa:arc4random()
																				 exponent:-4
																			   isNegative:NO];

			ScaleTicket* outboundScaleTicket = [ScaleTicket insertNewObjectForTransaction:transaction
																				withPhoto:photo
																	   andScaleTicketType:OutboundScaleTicket
																			  andLocation:gpsLog];
			outboundScaleTicket.ticketNumber = [NSString stringWithFormat:@"%u", arc4random() % 10000];
			outboundScaleTicket.outboundWeight = [NSDecimalNumber decimalNumberWithMantissa:arc4random()
																				   exponent:-4
																				 isNegative:NO];

			transaction.outgoingGPSLog = gpsLog;
			[transaction setSignature:signatureImage
					   forParticipant:OutgoingParticipant
							 withName:processorUser.registrant.businessName
						  andLocation:gpsLog];
		}

		[context save:nil];
	}
	
    //Refresh the  list View using Notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Sync Complete" object:nil];
    
	//UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Transactions created." delegate:nil cancelButtonTitle:@"Done" otherButtonTitles:nil, nil];
	//[alertView show];
}

@end
