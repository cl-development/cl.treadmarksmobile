//
//  PhotoType+PhotoType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-06.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "PhotoType+PhotoType.h"
#import "NSManagedObject+NSManagedObject.h"
#import "DataEngine.h"

@implementation PhotoType (PhotoType)

#pragma mark - Override methods

+ (void)prePopulateData {
	
	[super prePopulateData];
	
	/* 
	 2014-03-06
	 Expecting 4 records 
	 */
	if ([PhotoType count] == 0)
	{
		//NSLog(@"Inserting PhotoType records");
		
		NSManagedObjectContext* context = [[DataEngine sharedInstance] managedObjectContext];
		
		PhotoType* photoType = (PhotoType*)[PhotoType insertNewObject:context];
		photoType.photoTypeId = PHOTOTYPE_ID_PHOTO;
		photoType.name = @"Photo";
		
		photoType = (PhotoType*)[PhotoType insertNewObject:context];
		photoType.photoTypeId = PHOTOTYPE_ID_SIGNATURE;
		photoType.name = @"Signature";
		
		photoType = (PhotoType*)[PhotoType insertNewObject:context];
		photoType.photoTypeId = PHOTOTYPE_ID_SCALE_TICKET;
		photoType.name = @"Scale Ticket";
		
		photoType = (PhotoType*)[PhotoType insertNewObject:context];
		photoType.photoTypeId = PHOTOTYPE_ID_DOCUMENT;
		photoType.name = @"Document";
		
		NSError* error;
		if ([context save:&error] == NO)
		{
#ifdef DEBUG
			@throw [NSException exceptionWithName:@"Exception"
										   reason:[error description]
										 userInfo:[error userInfo]];
#endif
		}
	}
}

@end
