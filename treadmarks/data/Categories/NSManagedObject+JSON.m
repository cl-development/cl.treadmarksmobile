//
//  NSManagedObject+JSON.m
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-05-12.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "NSManagedObject+JSON.h"
#import "DataEngine.h"

// must be overridden
@implementation NSManagedObject (JSON)
-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    
    return YES;
}

@end
