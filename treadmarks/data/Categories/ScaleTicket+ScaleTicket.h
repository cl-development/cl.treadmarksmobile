//
//  ScaleTicket+ScaleTicket.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-16.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ScaleTicket.h"

enum ScaleTicketType {
	InboundScaleTicket = SCALE_TICKET_TYPE_INBOUND,
	OutboundScaleTicket = SCALE_TICKET_TYPE_OUTBOUND,
	Both = SCALE_TICKET_TYPE_BOTH
};

@class GPSLog;

@interface ScaleTicket (ScaleTicket)

+ (id)insertNewObjectForTransaction:transaction
						  withPhoto:(UIImage*)photo
				 andScaleTicketType:(enum ScaleTicketType)scaleTicketType
						andLocation:(GPSLog*)gpsLog;

@end
