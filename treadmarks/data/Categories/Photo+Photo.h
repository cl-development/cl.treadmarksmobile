//
//  Photo+Photo.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-26.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Photo.h"

@class GPSLog;

@interface Photo (Photo)

+ (Photo *)savePhoto:(UIImage *)photoImage
			  ofType:(PhotoType*)photoType
	  forTransaction:(Transaction*)transaction
		withFileName:(NSString *)fileName
		 andLocation:(GPSLog*)gpsLog;

@end
