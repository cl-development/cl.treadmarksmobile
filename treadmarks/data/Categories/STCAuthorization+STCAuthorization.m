//
//  STCAuthorization+STCAuthorization.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "STCAuthorization+STCAuthorization.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Authorization.h"

@implementation STCAuthorization (STCAuthorization)

+ (id)insertNewObjectWithAuthorization:(Authorization*)authorization andLocation:(Location*)location {
	
	STCAuthorization* stcAuthorization = [super insertNewObject:[authorization managedObjectContext]];
	stcAuthorization.authorization = authorization;
	stcAuthorization.location = location;
	
	return stcAuthorization;
}

@end
