//
//  NSManagedObject+NSManagedObject.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-03-06.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (NSManagedObject)

+ (NSUInteger)count;
+ (id)insertNewObject:(NSManagedObjectContext*)context;
+ (void)prePopulateData;
+ (id)singleById:(id)recordId;
+ (id)single:(NSString *)predicateFormat, ...;
+ (id)context:(NSManagedObjectContext*)context singleById:(id)recordId;
+ (id)context:(NSManagedObjectContext*)context single:(NSString *)predicateFormat, ...;
+ (NSArray*)where:(NSString *)predicateFormat, ...;
+ (NSArray*)context:(NSManagedObjectContext*)context where:(NSString *)predicateFormat, ...;
+ (NSString*)recordIdAttributeName;
+ (NSArray*)toArray;

@end
