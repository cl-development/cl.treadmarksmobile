//
//  ScaleTicket+ScaleTicket.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-16.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ScaleTicket+ScaleTicket.h"
#import "NSManagedObject+NSManagedObject.h"
#import "Photo+Photo.h"
#import "PhotoType.h"
#import "UnitType.h"
#import "ScaleTicketType.h"

@implementation ScaleTicket (ScaleTicket)

+ (id)insertNewObjectForTransaction:transaction
						  withPhoto:(UIImage*)photo
				 andScaleTicketType:(enum ScaleTicketType)scaleTicketType
						andLocation:(GPSLog*)gpsLog {
	
	ScaleTicket* scaleTicket = [super insertNewObject:[transaction managedObjectContext]];
	scaleTicket.scaleTicketId = [[NSUUID UUID] UUIDString];
	
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
																   fromDate:[NSDate date]];
	scaleTicket.date = [[NSCalendar currentCalendar] dateFromComponents:components];
	scaleTicket.photo = [Photo savePhoto:photo
								  ofType:[PhotoType singleById:PHOTO_TYPE_ID_SCALE_TICKET]
						  forTransaction:transaction
							withFileName:nil
							 andLocation:gpsLog];
	scaleTicket.scaleTicketType = [ScaleTicketType singleById:[NSNumber numberWithInteger:scaleTicketType]];
	scaleTicket.transaction = transaction;
	scaleTicket.unitType = [UnitType singleById:UNIT_TYPE_ID_KILOGRAM];
	
	return scaleTicket;
}

@end
