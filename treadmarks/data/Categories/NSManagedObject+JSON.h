//
//  NSManagedObject+JSON.h
//  TreadMarks
//
//  Created by Dennis Christopher on 2014-05-12.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <CoreData/CoreData.h>


@interface NSManagedObject (JSON)
//-(void)setValuesWithDictionary:(NSDictionary *)theDict;
-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict;
@end
