//
//  Comment+Comment.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Comment+Comment.h"

@implementation Comment (Comment)

+ (id)insertNewObject:(NSManagedObjectContext *)context forTransaction:(Transaction *)transaction byUser:(User *)user withText:(NSString *)text {

	Comment* comment = [super insertNewObject:context];
	comment.transaction = transaction;
	comment.user = user;
	comment.text = text;
	comment.commentId = [[NSUUID UUID] UUIDString];
	comment.createdDate = [NSDate date];
	return comment;
}

@end
