//
//  TransactionSyncStatusType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TransactionSyncStatusType : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * transactionSyncStatusTypeId;
@property (nonatomic, retain) NSString * internalDescription;
@property (nonatomic, retain) NSString * userMessage;

@end
