//
//  DataEngine.h
//  TreadMarks
//
//  Created by Capris Group on 2013-10-02.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Registrant.h"
#import "RegistrantType.h"
#import "Transaction.h"
#import "Transaction_TireType.h"
#import "TransactionType_Module.h"
#import "ScaleTicketType.h"
#import "UnitType.h"
#import "Document.h"
#import "DocumentType.h"
#import "Module.h"
#import "Eligibility.h"
#import "STCAuthorization.h"
#import "TransactionStatusType.h"
#import "TransactionSyncStatusType.h"
#import "PhotoPreselectComment.h"
#import "ScaleTicket.h"
#import "Comment.h"
#import "TransactionType.h"
#import "Photo.h"
#import "GPSLog.h"

@class MaterialType;
@class Transaction_MaterialType;


#import "Transaction_Eligibility.h"

@interface DataEngine : NSObject

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
//@property (strong, nonatomic) NSManagedObjectContext *backgroundManagedObjectContext;

+ (DataEngine *) sharedInstance;

@property (nonatomic, strong)NSMutableArray *signaturePathArray;

- (NSManagedObject*) newEntity:(NSString*)entityName;
- (NSArray *)entities:(NSString*)entityName;
- (NSArray *)entities:(NSString*)entityName sort:(NSString *)sortIndex ascending:(BOOL)ascending;
- (void) deleteAllEntities:(NSString*)entityName;
- (void)saveContext;
- (void)saveSignaturePath:(NSString *)path;
- (NSArray *)getArrayContents;
//- (NSManagedObject *)backgroundManagedObjectForClass:(NSString *)className withId:(id)ID;

- (Registrant *)registrantForId:(NSNumber *)registrantId;

- (Registrant *)registrantIdFromMetaData:(NSString *)metaData;

- (RegistrantType *)registrantTypeForId:(NSNumber *)registrantTypeId;

- (Location *)locationForId:(NSString *)locationId;

- (TransactionSyncStatusType*)transactionSyncStatusTypeForId:(NSNumber *)transactionSyncStatusId;

- (TransactionStatusType*)transactionStatusTypeForId:(NSNumber *)transactionStatusId;

- (Transaction *)transactionForId:(NSString *)transactionId;
- (Transaction *)transactionForFriendlyId:(NSNumber *)transactionId;

- (NSArray *)transactionsNotDeletedSort:(NSString *)sortIndex ascending:(BOOL)ascending;
- (NSArray *)transactionsNotSynced;
- (NSArray *)transactionsNotSyncedLimit:(NSUInteger)limit offSet:(NSUInteger)offset;
- (NSArray *)transactionsLimit:(NSUInteger)limit offSet:(NSUInteger)offset;
- (NSArray *)transactionsOfType:(NSNumber *)type;


- (NSArray *)transactionsForRegistrant:(NSNumber *)registrantId AndUser:(NSNumber *)userId NotDeletedSort:(NSString *)sortIndex ascending:(BOOL)ascending filterSync:(BOOL)sync;

- (Authorization *)authorizationForId:(NSNumber *)authorizationId;

- (Comment *)commentForId:(NSNumber *)commentId;

- (Photo *)photoForId:(NSNumber *)photoId;

- (Document *)documentForId:(NSNumber *)documentId;

- (ScaleTicket *)scaleTicketForId:(NSNumber *)scaleTicketId;

- (NSArray *)scaleTicketsForTicketNumber:(NSString *)scaleTicketNumber;

- (Transaction_Eligibility *)transEligForId:(NSNumber *)transEligibilityId;

- (Transaction_TireType *)transactionTireTypeForId:(NSNumber *)transactionTireTypeId;

- (NSArray *)transactionTireTypeNotSyncedForTransactionId:(NSString *)transactionId;

- (TransactionType *)transactionTypeForId:(NSNumber *)transactionTypeId;

- (User *)userForId:(NSNumber *)userId;
- (User *)userForId:(NSNumber *)userId andRegistrantId:(NSNumber *)registrantId;

- (TireType *)tireTypeForId:(NSNumber *)tireTypeId;

- (TireType *)tireTypeForName:(NSString *)tireName;

- (ScaleTicketType *)scaleTicketTypeForId:(NSNumber *)scaleTicketTypeId;

- (UnitType *)unitTypeForId:(NSNumber *)unitTypeId;

- (TransactionType_Module *)transactionTypeModuleForId:(NSNumber *)transactionTypeModuleId;

- (DocumentType *)documentTypeForId:(NSNumber *)documentTypeId;

- (Module *)moduleForId:(NSNumber *)moduleId;

- (Eligibility *)eligibilityForId:(NSNumber *)eligibilityId;

- (STCAuthorization *)stcAuthorizationForAuthorization:(Authorization *)theAuthorization;

- (Transaction_Eligibility *)transactionEligibilityForId:(NSNumber *)transactionEligibilityId;

- (NSArray *)tireTypesForTransactionTypeId:(NSNumber *)transactionTypeId;

- (MaterialType *)materialTypeForId:(NSNumber *)materialTypeId;
- (NSArray *)materialTypesForTransactionTypeId:(NSNumber *)transactionTypeId;

- (NSArray *)eligibilitiesOfType:(NSNumber *)theType;
- (NSArray *)transactionEligibilitiesForEligibility:(Eligibility *)theEligibility;
- (NSArray *)transactionEligibilitiesForTransaction:(Transaction *)theTransaction;
- (NSArray *)transactionEligibilitiesNotSyncedForTransaction:(Transaction *)theTransaction;

- (NSArray *)photosForTransaction:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType;

- (NSArray *)photosForSync:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType;

- (Comment *)commentForTransaction:(Transaction *)theTransaction;
- (NSArray *)commentsForTransaction:(Transaction *)theTransaction;

- (Comment *)commentNotSyncedForTransactionId:(Transaction *)theTransaction;
- (NSArray *)commentsNotSyncedForTransactionId:(Transaction *)theTransaction;

- (Authorization *)authorizationForTransaction:(Transaction *)theTransaction;

- (NSArray *)scaleTicketsForTransaction:(Transaction *)theTransaction;

- (NSArray *)scaleTicketsForTransaction:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType;

- (GPSLog *)gpsLogForId:(NSNumber *)gpsLogId;

- (NSArray *)users ;

- (NSArray *) searchAllRegistrants;

- (void) deleteAllUsers;
- (void) deleteAllRegistrants;
- (void) deleteAllRegistrantTypes;

- (void) deletePhoto:(Photo *)thePhoto;
- (void) deleteScaleTicket:(ScaleTicket*)scaleTicket;
- (void) deleteComment:(Comment *)comment;

- (Transaction_MaterialType *)transactionMaterialTypeForTransactionId:(NSString *)transactionId;
- (NSArray *)transactionTireTypeForTransactionId:(NSString *)transactionId;
- (Transaction *)singleTransactionForFriendlyId:(NSNumber *)transactionId :(NSString*)transType;


- (NSArray *)managedObjectsForClass:(NSString *)className sortedByKey:(NSString *)key usingArrayOfIds:(NSArray *)idArray inArrayOfIds:(BOOL)inIds;

- (User *)userManagedObject:(NSNumber *)userId WithRegistrant:(Registrant *)registrant;
- (TransactionStatusType *)transactionStatusTypeEntitiesToSwap: (NSString *)attributeFilter;
- (NSArray *)transactionSyncStatusTypeEntitiesToSwap;

@end
