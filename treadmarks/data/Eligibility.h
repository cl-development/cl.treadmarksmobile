//
//  Eligibility.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-23.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionType, Transaction_Eligibility;

@interface Eligibility : NSManagedObject

@property (nonatomic, retain) NSNumber * eligibilityId;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSSet *transactionEligibility;
@property (nonatomic, retain) TransactionType *transactionType;
@end

@interface Eligibility (CoreDataGeneratedAccessors)

- (void)addTransactionEligibilityObject:(Transaction_Eligibility *)value;
- (void)removeTransactionEligibilityObject:(Transaction_Eligibility *)value;
- (void)addTransactionEligibility:(NSSet *)values;
- (void)removeTransactionEligibility:(NSSet *)values;

@end
