//
//  TransactionStatusType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TransactionStatusType : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * transactionStatusTypeId;

@end
