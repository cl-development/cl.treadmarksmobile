//
//  CSVData.h
//  TreadMarks
//
//  Created by Capris Group on 11/21/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataEngine.h"
#import "User.h"
#import "Location.h"
#import "Registrant.h"
#import "RegistrantType.h"
#import "CSVParser.h"
#import "Constants.h"

@interface CSVData : NSObject

-(void) addDataNew;
-(void) addDataExtra;
//-(void) addData;
//-(void) addUser;

@end
