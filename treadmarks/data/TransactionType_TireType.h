//
//  TransactionType_TireType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TireType, TransactionType;

@interface TransactionType_TireType : NSManagedObject

@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * transactionTypeTireTypeId;
@property (nonatomic, retain) TireType *tireType;
@property (nonatomic, retain) TransactionType *transactionType;

@end
