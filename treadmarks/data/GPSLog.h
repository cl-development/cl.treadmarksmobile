//
//  GPSLog.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GPSLog : NSManagedObject

@property (nonatomic, retain) NSString * gpsLogId;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSDate * timestamp;

@end
