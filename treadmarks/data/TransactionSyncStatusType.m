//
//  TransactionSyncStatusType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionSyncStatusType.h"
#import "TMSyncEngine.h"

@implementation TransactionSyncStatusType

@dynamic fileName;
@dynamic nameKey;
@dynamic syncDate;
@dynamic transactionSyncStatusTypeId;
@dynamic internalDescription;
@dynamic userMessage;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}


@end
