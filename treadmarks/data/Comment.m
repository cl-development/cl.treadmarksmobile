//
//  Comment.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Comment.h"
#import "Transaction.h"
#import "User.h"


@implementation Comment

@dynamic commentId;
@dynamic createdDate;
@dynamic syncDate;
@dynamic text;
@dynamic transaction;
@dynamic user;

@end
