//
//  Comment.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Transaction, User;

@interface Comment : NSManagedObject

@property (nonatomic, retain) NSString * commentId;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Transaction *transaction;
@property (nonatomic, retain) User *user;

@end
