//
//  Location.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Location.h"
#import "TMSyncEngine.h"


@implementation Location

@dynamic address1;
@dynamic address2;
@dynamic address3;
@dynamic city;
@dynamic country;
@dynamic fax;
@dynamic latitude;
@dynamic locationId;
@dynamic longitude;
@dynamic name;
@dynamic phone;
@dynamic postalCode;
@dynamic province;
@dynamic syncDate;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]==YES) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else if ([key isEqualToString:@"latitude"] || [key isEqualToString:@"longitude"] ) {
            NSNumber *temp = theValue;
            [self setValue:[temp stringValue] forKey:key];
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}

@end
