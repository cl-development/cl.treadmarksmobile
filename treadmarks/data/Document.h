//
//  Document.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DocumentType, Photo, Transaction;

@interface Document : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * documentId;
@property (nonatomic, retain) NSString * documentName;
@property (nonatomic, retain) NSString * documentNumber;
@property (nonatomic, retain) NSString * documentTypeOther;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) DocumentType *documentType;
@property (nonatomic, retain) Photo *photo;
@property (nonatomic, retain) Transaction *transaction;

@end
