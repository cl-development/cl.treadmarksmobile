//
//  TransactionType_RegistrantType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionType_RegistrantType.h"
#import "RegistrantType.h"
#import "TransactionType.h"
#import "TMSyncEngine.h"
#import "DataEngine.h"

@implementation TransactionType_RegistrantType

@dynamic syncDate;
@dynamic transactionTypeRegistrantTypeId;
@dynamic registrantType;
@dynamic transactionType;

-(void)setValuesWithDictionary:(NSDictionary *)theDict {
    
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }  else if ([key isEqualToString:@"registrantTypeId"]) {
            RegistrantType *regT = [[DataEngine sharedInstance] registrantTypeForId:theValue];
            RegistrantType *rType = (RegistrantType *)[self.managedObjectContext objectWithID:[regT objectID]];
            self.registrantType = rType;
        }  else if ([key isEqualToString:@"transactionTypeId"]) {
            TransactionType *transT = [[DataEngine sharedInstance] transactionTypeForId:theValue];
            TransactionType *transType = (TransactionType *)[self.managedObjectContext objectWithID:[transT objectID]];
            self.transactionType = transType;
        }  else {
            [self setValue:theValue forKey:key];
        }
        
    }

}

@end
