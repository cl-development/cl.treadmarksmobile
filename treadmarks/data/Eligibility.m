//
//  Eligibility.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-23.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Eligibility.h"
#import "TransactionType.h"
#import "Transaction_Eligibility.h"
#import "TMSyncEngine.h"
#import "DataEngine.h"

@implementation Eligibility

@dynamic eligibilityId;
@dynamic nameKey;
@dynamic sortIndex;
@dynamic syncDate;
@dynamic transactionEligibility;
@dynamic transactionType;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else if ([key isEqualToString:@"transactionTypeId"]) {
            TransactionType *tType = [[DataEngine sharedInstance] transactionTypeForId:theValue];
            TransactionType *transType = (TransactionType *)[self.managedObjectContext objectWithID:[tType objectID]];
            self.transactionType = transType;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}


@end
