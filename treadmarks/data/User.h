//
//  User.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Registrant;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * accessTypeId;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * lastAccessDate;
@property (nonatomic, retain) NSString * metaData;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSDate * termsAcceptedDate;
@property (nonatomic, retain) Registrant *registrant;

@end
