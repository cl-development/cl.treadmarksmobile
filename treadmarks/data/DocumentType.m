//
//  DocumentType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "DocumentType.h"
#import "TMSyncEngine.h"

@implementation DocumentType

@dynamic documentTypeId;
@dynamic nameKey;
@dynamic sortIndex;
@dynamic syncDate;


-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}


@end
