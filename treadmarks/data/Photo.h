//
//  Photo.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PhotoType, Transaction;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSDate * imageSyncDate;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * photoId;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) PhotoType *photoType;
@property (nonatomic, retain) Transaction *transaction;

@end
