//
//  Module.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Module.h"


@implementation Module

@dynamic moduleId;
@dynamic nameKey;
@dynamic syncDate;

@end
