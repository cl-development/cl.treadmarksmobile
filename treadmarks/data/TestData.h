//
//  TestData.h
//  TreadMarks
//
//  Created by Capris Group on 2013-10-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TestData : NSObject

+(void)populateDatabase;

@end
