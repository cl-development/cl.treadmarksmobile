//
//  User.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-06-03.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "User.h"
#import "Registrant.h"
#import "TMSyncEngine.h"
#import "DataEngine.h"


@implementation User

@dynamic accessTypeId;
@dynamic createdDate;
@dynamic email;
@dynamic lastAccessDate;
@dynamic metaData;
@dynamic modifiedDate;
@dynamic name;
@dynamic syncDate;
@dynamic userId;
@dynamic termsAcceptedDate;
@dynamic registrant;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *dateMembers = [NSArray arrayWithObjects:@"syncDate",@"createdDate",@"lastAccessDate",@"modifiedDate", nil];
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"registrationNumber"]==YES) {
            Registrant *reg = [[DataEngine sharedInstance] registrantForId:theValue];
            if (reg==nil) {
                return NO;
            }
            Registrant *obj = (Registrant *)[self.managedObjectContext objectWithID:[reg objectID]];
            self.registrant = obj;
        }
        else if ([dateMembers containsObject:key]==YES)  {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
           // dispatch_async(dispatch_get_main_queue(), ^{
                 [self setValue:theDate forKey:key];
            //});
        }
        else {
          //  dispatch_async(dispatch_get_main_queue(), ^{
                [self setValue:theValue forKey:key];
          //  });
        }
    }
    return YES;
}

@end
