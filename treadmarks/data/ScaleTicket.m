//
//  ScaleTicket.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "ScaleTicket.h"
#import "Photo.h"
#import "ScaleTicketType.h"
#import "Transaction.h"
#import "UnitType.h"


@implementation ScaleTicket

@dynamic date;
@dynamic inboundWeight;
@dynamic outboundWeight;
@dynamic scaleTicketId;
@dynamic sortIndex;
@dynamic syncDate;
@dynamic ticketNumber;
@dynamic photo;
@dynamic scaleTicketType;
@dynamic transaction;
@dynamic unitType;

@end
