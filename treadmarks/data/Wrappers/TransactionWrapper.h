//
//  TransactionWrapper.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/24/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionDelegate.h"

@class Transaction;

@interface TransactionWrapper : NSObject 

- (id)initWithTransaction:(Transaction*)transaction;

- (void)voidTransaction:(UIViewController<TransactionDelegate>*)viewController
				   info:(NSDictionary*)info;

@end
