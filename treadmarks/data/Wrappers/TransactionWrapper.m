//
//  TransactionWrapper.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2/24/2014.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionWrapper.h"
#import "AlertViewDelegate.h"
#import "AlertViewController.h"
#import "DataEngine.h"
#import "NewAlertView.h"
#import "ModalViewController.h"
#import "TOCPageViewController.h"
#import "ListViewController.h"

@interface TransactionWrapper() <NewAlertViewDelegate>

@property (strong) Transaction* transaction;
@property (strong) UIViewController<TransactionDelegate>* delegate;

@end

@implementation TransactionWrapper

@synthesize delegate;

#pragma mark - Initializers

- (id)initWithTransaction:(Transaction *)transaction {
	
	self = [super init];
	
	if(self)
	{
		self.transaction = transaction;
	}
	
	return self;
}

#pragma mark - Methods

- (void)voidTransaction:(UIViewController<TransactionDelegate> *)viewController info:(NSDictionary *)info {
	
	self.delegate = viewController;
	
	[self presentAlert:viewController info:info];
}

#pragma mark - AlertViewDelegate

- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {

	NewAlertView *alert = [[NewAlertView alloc] init];
	alert.title = @"Void Transaction";
	alert.heading = @"Are you sure you want to void this transaction?";
	alert.text = @"You will lose all your data including documents, photos, or notes that are recorded as part of this transaction";
	alert.primaryButtonText = @"Void";
	alert.secondaryButtonText = @"Cancel";
	alert.delegate = self;
    [alert setUI];
	[[ModalViewController sharedModalViewController] showView:alert];
}

#pragma mark - NewAlertViewDelegate

- (void)primaryAction:(UIButton *)sender {

	DataEngine* dataEngine = [DataEngine sharedInstance];
    
    self.transaction.transactionStatusType = [dataEngine transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_DELETED];
    self.transaction.transactionSyncStatusType = [dataEngine transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
	
	[dataEngine saveContext];
    
    if([self.delegate conformsToProtocol:@protocol(TransactionDelegate)] && [self.delegate respondsToSelector:@selector(cancelVoidProcess)]) {
        
        [self.delegate cancelVoidProcess];
    }
    
	[self.delegate transactionDidVoid];
/*
    ListViewController *listview = [[ListViewController alloc]init];

    if ([listview getMonthFromDate:self.transaction.createdDate] == [listview getClaimMonth:2]){
        countforLastMonth--;
    }
*/
	[[ModalViewController sharedModalViewController] hideView];
}
- (void)secondaryAction:(UIButton *)sender {
    
    if([self.delegate conformsToProtocol:@protocol(TransactionDelegate)] && [self.delegate respondsToSelector:@selector(cancelVoidProcess)]) {
        
        [self.delegate cancelVoidProcess];
    }
    
	[[ModalViewController sharedModalViewController] hideView];
}

@end
