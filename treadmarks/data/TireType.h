//
//  TireType.h
//  TreadMarks
//
//  Created by Capris Group on 2014-07-09.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionType_TireType;

@interface TireType : NSManagedObject

@property (nonatomic, retain) NSDate * effectiveEndDate;
@property (nonatomic, retain) NSDate * effectiveStartDate;
@property (nonatomic, retain) NSDecimalNumber * estimatedWeight;
@property (nonatomic, retain) NSString * nameKey;
@property (nonatomic, retain) NSString * shortNameKey;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * tireTypeId;
@property (nonatomic, retain) NSSet *transactionType_TireTypes;
@end

@interface TireType (CoreDataGeneratedAccessors)

- (void)addTransactionType_TireTypesObject:(TransactionType_TireType *)value;
- (void)removeTransactionType_TireTypesObject:(TransactionType_TireType *)value;
- (void)addTransactionType_TireTypes:(NSSet *)values;
- (void)removeTransactionType_TireTypes:(NSSet *)values;

@end
