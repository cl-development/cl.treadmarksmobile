//
//  GPSLog.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "GPSLog.h"


@implementation GPSLog

@dynamic gpsLogId;
@dynamic latitude;
@dynamic longitude;
@dynamic syncDate;
@dynamic timestamp;

@end
