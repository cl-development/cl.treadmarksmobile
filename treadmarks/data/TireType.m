//
//  TireType.m
//  TreadMarks
//
//  Created by Capris Group on 2014-07-09.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TireType.h"
#import "TransactionType_TireType.h"
#import "TMSyncEngine.h"

@implementation TireType

@dynamic effectiveEndDate;
@dynamic effectiveStartDate;
@dynamic estimatedWeight;
@dynamic nameKey;
@dynamic shortNameKey;
@dynamic sortIndex;
@dynamic syncDate;
@dynamic tireTypeId;
@dynamic transactionType_TireTypes;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        NSDate *theDate = nil;
        if ([key isEqualToString:@"effectiveEndDate"]==YES) {
            theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.effectiveEndDate = theDate;
        }
        else if ([key isEqualToString:@"effectiveStartDate"]==YES) {
            theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.effectiveStartDate = theDate;
        }
        else if ([key isEqualToString:@"syncDate"]==YES) {
            theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}

@end
