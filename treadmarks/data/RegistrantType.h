//
//  RegistrantType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RegistrantType : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * descriptionKey;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * metaData;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSNumber * registrantTypeId;
@property (nonatomic, retain) NSString * shortDescriptionKey;
@property (nonatomic, retain) NSDate * syncDate;


@end
