//
//  TransactionType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionType.h"
#import "TransactionType_TireType.h"
#import "TMSyncEngine.h"


@implementation TransactionType

@dynamic fileName;
@dynamic incomingSignatureKey;
@dynamic nameKey;
@dynamic outgoingSignatureKey;
@dynamic shortNameKey;
@dynamic sortIndex;
@dynamic syncDate;
@dynamic tireCountMessageKey;
@dynamic transactionTypeId;
@dynamic transactionType_TireTypes;
@dynamic transactionType_MaterialType;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        NSDate *theDate = nil;
        if ([key isEqualToString:@"syncDate"]==YES) {
            theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }
        else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}

@end
