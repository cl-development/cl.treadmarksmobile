//
//  STCAuthorization.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "STCAuthorization.h"
#import "Authorization.h"
#import "Location.h"


@implementation STCAuthorization

@dynamic authorization;
@dynamic location;

@end
