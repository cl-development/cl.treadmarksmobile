//
//  RegistrantType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "RegistrantType.h"
#import "TMSyncEngine.h"


@implementation RegistrantType

@dynamic createdDate;
@dynamic descriptionKey;
@dynamic fileName;
@dynamic metaData;
@dynamic modifiedDate;
@dynamic registrantTypeId;
@dynamic shortDescriptionKey;
@dynamic syncDate;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
         theValue = [theDict valueForKey:key];
         NSDate *theDate = nil;
         if ([key isEqualToString:@"createdDate"]==YES) {
             theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
             self.createdDate = theDate;
         }
         else if ([key isEqualToString:@"modifiedDate"]==YES) {
             theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
             self.modifiedDate = theDate;
         }
         else if ([key isEqualToString:@"syncDate"]==YES) {
             theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
             self.syncDate = theDate;
         }
         else {
             [self setValue:theValue forKey:key];
         }
     }
    return YES;
 }

@end
