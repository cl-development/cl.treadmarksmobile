//
//  TransactionType_TireType.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "TransactionType_TireType.h"
#import "TireType.h"
#import "TransactionType.h"
#import "TMSyncEngine.h"
#import "DataEngine.h"

@implementation TransactionType_TireType

@dynamic syncDate;
@dynamic transactionTypeTireTypeId;
@dynamic tireType;
@dynamic transactionType;

-(BOOL)setValuesWithDictionary:(NSDictionary *)theDict {
    
    NSArray *keys = [theDict allKeys];
    id theValue;
    for (NSString *key in keys) {
        theValue = [theDict valueForKey:key];
        if ([key isEqualToString:@"syncDate"]) {
            NSDate *theDate = [[TMSyncEngine sharedEngine] unixDateToNSDate:theValue];
            self.syncDate = theDate;
        }  else if ([key isEqualToString:@"tireTypeId"]) {
            TireType *tireT = [[DataEngine sharedInstance] tireTypeForId:theValue];
            TireType *tType = (TireType *)[self.managedObjectContext objectWithID:[tireT objectID]];
            self.tireType = tType;
        }  else if ([key isEqualToString:@"transactionTypeId"]) {
            TransactionType *transT = [[DataEngine sharedInstance] transactionTypeForId:theValue];
            TransactionType *transType = (TransactionType *)[self.managedObjectContext objectWithID:[transT objectID]];
            self.transactionType = transType;
        }  else {
            [self setValue:theValue forKey:key];
        }
    }
    return YES;
}


@end
