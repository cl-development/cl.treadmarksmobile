//
//  Transaction_Eligibility.m
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import "Transaction_Eligibility.h"
#import "Eligibility.h"
#import "Transaction.h"


@implementation Transaction_Eligibility

@dynamic syncDate;
@dynamic transactionEligibilityId;
@dynamic value;
@dynamic eligibility;
@dynamic transaction;

@end
