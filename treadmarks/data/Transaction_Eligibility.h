//
//  Transaction_Eligibility.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Eligibility, Transaction;

@interface Transaction_Eligibility : NSManagedObject

@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSString * transactionEligibilityId;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) Eligibility *eligibility;
@property (nonatomic, retain) Transaction *transaction;

@end
