//
//  Transaction_TireType.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-27.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TireType, Transaction;

@interface Transaction_TireType : NSManagedObject

@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSString * transactionTireTypeId;
@property (nonatomic, retain) TireType *tireType;
@property (nonatomic, retain) Transaction *transaction;

@end
