//
//  CSVData.m
//  TreadMarks
//
//  Created by Capris Group on 11/21/2013.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "CSVData.h"
#import "Utils.h"
#import "Location+Location.h"
#import "NSManagedObject+NSManagedObject.h"

@interface CSVData ()

@property (nonatomic, strong) NSArray *array;

@end

@implementation CSVData


-(void)addDataNew {
    [self addLocationsNew];
}


- (void) addLocationsNew {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"location" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self printLocation];
                                           [self addRegistrantsNew];
                                       }];
}

- (void) addRegistrantsNew {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"registrants" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self print];
                                           [self addUserNew];
                                       }];
}

- (void) addUserNew {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"users" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self printUser];
                                       }];
}


//- (void) addData {
//    
//    [self addLocations];
//    
//    NSString *file = [[NSBundle mainBundle] pathForResource:@"registrants" ofType:@"csv"];
//    
//    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
//                    withSeparatedCharacterString:@","
//                            quoteCharacterString:nil
//                                       withBlock:^(NSArray *array, NSError *error) {
//                                           self.array = array;
//                                           [self print];
//                                           
//                                       }];
//    
//    
//}

//- (void) addUser {
//    
//    NSString *file = [[NSBundle mainBundle] pathForResource:@"users" ofType:@"csv"];
//    
//    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
//                    withSeparatedCharacterString:@","
//                            quoteCharacterString:nil
//                                       withBlock:^(NSArray *array, NSError *error) {
//                                           self.array = array;
//                                           [self printUser];
//                                           
//                                       }];
//    
//    
//}

//- (void) addLocations {
//    
//    NSString *file = [[NSBundle mainBundle] pathForResource:@"location" ofType:@"csv"];
//    
//    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
//                    withSeparatedCharacterString:@","
//                            quoteCharacterString:nil
//                                       withBlock:^(NSArray *array, NSError *error) {
//                                           self.array = array;
//                                           [self printLocation];
//                                       }];
//    
//    
//}

//"locationId","address1","address2","address3","city","province","postalCode","country","phone","fax"
-(void)printLocation {
    
    for (int i = 0; i < [self.array count]; i++) {
        NSArray *locations = [self.array objectAtIndex:i];
        
        // why is the last record garbage?
        if ([locations count] < 10) {
            continue;
        }
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSNumber *number = [f numberFromString:[locations objectAtIndex:0]];
		
		if (number == nil)
		{
			continue;
		}
		////NSLog(@"The location id is: %@", number);
		NSString *locationID = [Location formatLocationId:number];
		
		/// Check if location already exists in database
		if ([Location singleById:locationID] != nil)
		{
			/// If it exists, then bypass it
			continue;
		}
		
        NSString *address1 = [[locations objectAtIndex:1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *address2 = [[locations objectAtIndex:2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *address3 = [[locations objectAtIndex:3] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *city = [[locations objectAtIndex:4] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *province = [[locations objectAtIndex:5] stringByReplacingOccurrencesOfString:@"\"" withString:@""];;
        NSString *postalCode = [[locations objectAtIndex:6] stringByReplacingOccurrencesOfString:@"\"" withString:@""];;
        NSString *country = [[locations objectAtIndex:7] stringByReplacingOccurrencesOfString:@"\"" withString:@""];;
        NSString *phone = [[locations objectAtIndex:8] stringByReplacingOccurrencesOfString:@"\"" withString:@""];;
        NSString *fax = [[locations objectAtIndex:9] stringByReplacingOccurrencesOfString:@"\"" withString:@""];;

        Location *location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
        location.locationId = locationID;
        location.address1 = address1;
        location.address2 = address2;
        location.address3 = address3;
        location.city = city;
        location.province = province;
        location.postalCode = postalCode;
        location.country = country;
        location.phone = phone;
        location.fax = fax;
        
        ////NSLog(@"The each location is: %d, %@", i, location);
        
        [[DataEngine sharedInstance]saveContext];
    }
    
    //NSLog(@"%lu Location objects were stored", (unsigned long)[self.array count]);

}

//"registration_number","registrant_type_id","business_name","created_date_ts","last_updated_date"
- (void)print {
    
    ////NSLog(@"%@", self.array);
    
    for (int i = 1; i < [self.array count]; i++) {
        ////NSLog(@"Adding object %d", i);
        NSArray *registrants = [self.array objectAtIndex:i];
        
		if ([registrants count] < 5)
		{
			continue;
		}
		
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *myNumber = [f numberFromString:[registrants objectAtIndex:0]];
		
		/// Check if the registrant already exists
		if ([Registrant singleById:myNumber] != nil)
		{
			continue;
		}
		
        NSNumber *regNumberType = [f numberFromString:[registrants objectAtIndex:1]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *businessName = [[registrants objectAtIndex:2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        NSString *create = [[registrants objectAtIndex:3] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *newCreatedString = [create stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSDate *createdDate = [dateFormatter dateFromString:newCreatedString];
        
        NSString *lastUpdated = [registrants objectAtIndex:4];
        NSString *newLast = [lastUpdated stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSDate *modifiedDate = [dateFormatter dateFromString:newLast];
   
        Location *location = (Location *) [[DataEngine sharedInstance] locationForId:[Location formatLocationId:myNumber]];
        
        Registrant *registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
        registrant.registrationNumber = myNumber;
        registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:regNumberType];
        registrant.businessName = businessName;
        registrant.location = location;
        registrant.createdDate = createdDate;
        registrant.modifiedDate = modifiedDate;
        registrant.metaData = nil;
        
        [[DataEngine sharedInstance]saveContext];
    }
    
   /* NSArray * array = [[DataEngine sharedInstance] entities:REGISTRANT_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        //NSLog(@"%@", [entity description]);
    }*/
    
    NSArray *testRegistrants = [[DataEngine sharedInstance] searchAllRegistrants];
    
    if ([testRegistrants count] == 0) {
        
        //NSLog(@"All registrants have been deleted ???");
    }
    
    else {
        //NSLog(@"%lu Registrant objects were stored", (unsigned long)[testRegistrants count]);
    }
    
}

- (void)printUser {
    ////NSLog(@"%@", self.array);
    
    for (int i = 1; i < [self.array count]; i++) {
        ////NSLog(@"Adding object %d", i);
        NSArray *user = [self.array objectAtIndex:i];
		
		if ([user count] < 7)
		{
			continue;
		}
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *idNumber = [f numberFromString:[user objectAtIndex:0]];
        NSString *userName = [[user objectAtIndex:1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *userEmail = [[user objectAtIndex:2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSNumber *access = [f numberFromString:[user objectAtIndex:3]];
        NSNumber *rNumber = [f numberFromString:[user objectAtIndex:4]];
		
		/// Check if the user/registrans number already exists
		if ([User single:@"userId == %@ && registrant.registrationNumber == %@", idNumber, rNumber] != nil)
		{
			continue;
		}
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *create = [user objectAtIndex:5];
        NSString *newCreatedString = [create stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        NSDate *createdDate = [dateFormatter dateFromString:newCreatedString];
        
        NSString *modif = [user objectAtIndex:6];
        NSString *newModif = [modif stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSDate *modifiedDate = [dateFormatter dateFromString:newModif];
        
        User *entity = (User *) [[DataEngine sharedInstance] newEntity:USER_ENTITY_NAME];
        entity.userId       = idNumber;
        entity.name         = userName;
        entity.email        = userEmail;
        entity.accessTypeId = access;
        entity.registrant   = [[DataEngine sharedInstance] registrantForId:rNumber];
        entity.createdDate = createdDate;
        entity.modifiedDate = modifiedDate;
        entity.metaData     = nil;
		
		//NSLog(@"Inserting [userId:%@, registrationNumber:%@", idNumber, rNumber);
        
        [[DataEngine sharedInstance]saveContext];
    }
    
    //NSArray * array = [[DataEngine sharedInstance] entities:USER_ENTITY_NAME];
    /*for (NSManagedObject * entity in array) {
        //NSLog(@"%@", [entity description]);
    }*/
    
    NSArray *testUsers = [[DataEngine sharedInstance] users];
    
    if ([testUsers count] == 0) {
        //NSLog(@"All users have been deleted ????");
    }
    else {
        //NSLog(@"%lu User objects were stored", (unsigned long)[testUsers count]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Fixed Data Complete" object:nil];
    }
}

-(void)addDataExtra {
    [self addLocationsExtra];
}

- (void) addLocationsExtra {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"NewLocations" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self printLocation];
                                           [self addRegistrantsExtra];
                                       }];
}


- (void) addRegistrantsExtra {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"NewRegistrants" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self print];
                                           [self addUserExtra];
                                       }];
}

- (void) addUserExtra {
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"NewUsers" ofType:@"csv"];
    
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *array, NSError *error) {
                                           self.array = array;
                                           [self printUser];
                                       }];
}

@end
