//
//  DataEngine.m
//  TreadMarks
//
//  Created by Capris Group on 2013-10-02.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "DataEngine.h"
#import "Utils.h"
#import "Constants.h"
#import "TransactionType_TireType.h"
#import "NSManagedObject+NSManagedObject.h"
#import "PhotoType.h"
#import "NSFileManager+MHWAdditions.h"

#import "TreadMarks-Swift.h"

@interface DataEngine ()

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (strong, nonatomic) NSManagedObjectContext *masterManagedObjectContext;



@end

@implementation DataEngine

#pragma mark - Core Data code

static DataEngine *sharedSingleton = nil;

/*returns shared instance of DataEngine*/
+ (DataEngine *) sharedInstance {
    
    if (sharedSingleton == nil) {
        sharedSingleton = [[super alloc] init];
        sharedSingleton.signaturePathArray = [[NSMutableArray alloc] init];
    }
    return sharedSingleton;
}

/*returns managed object context*/
- (NSManagedObjectContext *) managedObjectContext{
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator != nil) {
        //_managedObjectContext = [[NSManagedObjectContext alloc] init];
        // try:
        _managedObjectContext = [[NSManagedObjectContext alloc]initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        
        //Undo Support
        NSUndoManager *anUndoManager = [[NSUndoManager alloc] init];
        [self.managedObjectContext setUndoManager:anUndoManager];
    }
    return _managedObjectContext;
}

/*the persistence store coordinator*/
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
							 nil];
    
    NSURL * storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:SQLITE_STORE_URL];
    
    NSError * error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
												   configuration:nil
															 URL:storeURL
														 options:options
														   error:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    NSDictionary * fileAttributes=[NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
    
    if(![[ NSFileManager defaultManager] setAttributes:fileAttributes ofItemAtPath:[storeURL path] error:&error]) {
        //NSLog(@"Cannot encrypt core data file %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

/*managed object model*/
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:MANAGED_OBJECT_MODEL_PATH withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSURL *)applicationDocumentsDirectory; {
	
    return [NSFileManager urlToDocumentDirectory];
}

/*save context*/
- (void)saveContext {
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    [context save:&error];
    if (![context save:&error]) {
        
        //NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
		if(detailedErrors != nil && [detailedErrors count] > 0) {
			
			for(NSError* detailedError in detailedErrors) {
				NSLog(@"  DetailedError: %@", [detailedError userInfo]);
			}
			 
		}
		else {
			NSLog(@"  %@", [error userInfo]);
		}
        
    }
}
/*  Removed backgroundContext
// Return the NSManagedObjectContext to be used in the background during sync
- (NSManagedObjectContext *)backgroundManagedObjectContext {
    if (_backgroundManagedObjectContext != nil) {
        return _backgroundManagedObjectContext;
    }
    
    NSManagedObjectContext *masterContext = [self managedObjectContext];
    if (masterContext != nil) {
        _backgroundManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_backgroundManagedObjectContext performBlockAndWait:^{
            [_backgroundManagedObjectContext setParentContext:masterContext];
        }];
    }
    
    return _backgroundManagedObjectContext;
}
 */

#pragma mark - General core data code

/*create a new entity*/
- (NSManagedObject*) newEntity:(NSString*)entityName {
    //[Utils log:@"newEntity:%@",entityName];

    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    NSManagedObject *managedObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    return managedObject;
}

/*return all entities*/
- (NSArray *)entities:(NSString*)entityName  {
   // [Utils log:@"entities for:%@",entityName];
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        //NSLog(@"Whoops, couldn't search: %@", [error localizedDescription]);
    }
    
    return items;
}

- (TransactionStatusType *)transactionStatusTypeEntitiesToSwap: (NSString *)attributeFilter {
    // [Utils log:@"entities for:%@",entityName];
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_STATUS_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nameKey == %@", attributeFilter];
    [fetchRequest setPredicate:predicate];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
   
}

- (NSArray *)transactionSyncStatusTypeEntitiesToSwap {
    // [Utils log:@"entities for:%@",entityName];
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSString *userMessageText = @"Error during Sync";
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userMessage == %@", userMessageText];
    [fetchRequest setPredicate:predicate];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        //NSLog(@"Whoops, couldn't search: %@", [error localizedDescription]);
    }
    
    return items;
}



/*return all sorted entities*/
- (NSArray *)entities:(NSString*)entityName sort:(NSString *)sortIndex ascending:(BOOL)ascending {
    
    [Utils log:@"entities for:%@",entityName];
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortIndex ascending:ascending];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        //NSLog(@"Whoops, couldn't search: %@", [error localizedDescription]);
    }
    
    return items;
}

/*delete all entities*/
- (void) deleteAllEntities:(NSString*)entityName  {
    
    [Utils log:@"deleteAllEntities:%@",entityName];
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        // handle fetch error
    } else {
        
        for (NSManagedObject *recordToDelete in results) {
            [context deleteObject:recordToDelete];
        }
    }
    
    if (![context save:&error]) {
        //NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}

// sole purpose is to delete a defective scaleticket and its photo
- (void) deleteScaleTicket:(ScaleTicket*)scaleTicket  {
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    [context deleteObject:scaleTicket];
    
    if (scaleTicket.photo != nil) {
        [context deleteObject:scaleTicket.photo];
    }
    
    if (![context save:&error]) {
        NSLog(@"scale ticket delete failed: %@", [error localizedDescription]);
    }
}

-(void) deletePhoto:(Photo *)thePhoto {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    if (thePhoto.syncDate == nil) {
        [context deleteObject:thePhoto];
        //NSLog(@"Deleting photo object");
    } else {
        thePhoto.syncDate = nil;
        thePhoto.fileName = @"DELETE";
        //NSLog(@"Photo marked for deletion");
    }
    
    if (![context save:&error]) {
        //NSLog(@"Delete photo failed: %@", [error localizedDescription]);
    }
}

- (void) deleteComment:(Comment *)comment {
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    [context deleteObject:comment];
    
    if (![context save:&error]) {
        NSLog(@"Comment delete failed: %@", [error localizedDescription]);
    }
}


/*
- (NSManagedObject *)backgroundManagedObjectForClass:(NSString *)className withId:(id)ID {
    NSManagedObjectContext *managedObjectContext = [[DataEngine sharedInstance] backgroundManagedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:className];
    NSPredicate *predicate;
    NSError *error;
    
    // Create predicate
    if ([className isEqualToString:LOCATION_ENTITY_NAME]==YES) {
         predicate = [NSPredicate predicateWithFormat:@"locationId == %@", ID];
    }
    else if ([className isEqualToString:REGISTRANT_ENTITY_NAME]==YES) {
         predicate = [NSPredicate predicateWithFormat:@"registrationNumber == %@", ID];
    }
    // throw an error here if not one of the above
    else {
        return nil;
    }
    
    NSArray *items = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}
*/

#pragma mark - Registrant

- (Registrant *)registrantForId:(NSNumber *)registrantId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:REGISTRANT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"registrationNumber == %@", registrantId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}
- (Registrant *)registrantIdFromMetaData:(NSString *)metaData {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:REGISTRANT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"metaData == %@", metaData];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}


- (RegistrantType *)registrantTypeForId:(NSNumber *)registrantTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:REGISTRANT_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"registrantTypeId == %@", registrantTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Location *)locationForId:(NSString *)locationId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:LOCATION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"locationId == %@", locationId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
   // //NSLog(@"the number of location objects are: %d", [items count]);
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Transaction *)transactionForId:(NSString *)transactionId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionId == %@", transactionId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}
- (Transaction *)transactionForFriendlyId:(NSNumber *)friendlyId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"friendlyId == %@ ||friendlyId != nil", friendlyId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

//returnArray of FriendlyID
- (Transaction *)singleTransactionForFriendlyId:(NSNumber *)transactionId :(NSString*)transType{
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"friendlyId == %@", transactionId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
    
}

//return all the transactions that are not deleted
- (NSArray *)transactionsNotDeletedSort:(NSString *)sortIndex ascending:(BOOL)ascending {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionStatusType.transactionStatusTypeId != %@", TRANSACTION_STATUS_TYPE_ID_DELETED ];
    [fetchRequest setPredicate:pred];
    
    //Create sort descriptor
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortIndex ascending:ascending];
//    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    return items;
}
/// If filterSync is NO, then the method returns transactions for the specified registrant and user.
/// Else if filterSync is YES, then the method returns all transactions on the device.
- (NSArray *)transactionsForRegistrant:(NSNumber *)registrantId AndUser:(NSNumber *)userId NotDeletedSort:(NSString *)sortIndex ascending:(BOOL)ascending filterSync:(BOOL)sync{
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred;
	/// If sync, we do not care about the registrant
    if (sync) {
        pred = [NSPredicate predicateWithFormat:@"transactionStatusType.transactionStatusTypeId == %@ AND transactionSyncStatusType.transactionSyncStatusTypeId == %@ || transactionStatusType.transactionStatusTypeId == %@ AND transactionSyncStatusType.transactionSyncStatusTypeId == %@ || transactionStatusType.transactionStatusTypeId == %@ AND transactionSyncStatusType.transactionSyncStatusTypeId != %@ || transactionStatusType.transactionStatusTypeId == %@ AND transactionSyncStatusType.transactionSyncStatusTypeId != %@", TRANSACTION_STATUS_TYPE_ID_ERROR, TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SYNC_FAIL_TM, TRANSACTION_STATUS_TYPE_ID_COMPLETE, TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED, TRANSACTION_STATUS_TYPE_ID_INCOMPLETE, TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED, TRANSACTION_STATUS_TYPE_ID_INCOMPLETE, TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SYNC_TM];
      /* Need All the trnsaction
       NSPredicate *regPred = [NSPredicate predicateWithFormat:@"(incomingRegistrant.registrationNumber == %@  OR outgoingRegistrant.registrationNumber == %@ )", registrantId, registrantId];
         //NSPredicate *regPred = [NSPredicate predicateWithFormat:@"(incomingRegistrant.registrationNumber == %@ )", registrantId];
        pred = [NSCompoundPredicate andPredicateWithSubpredicates:@[delPred, regPred]];*/
        //pred=delPred;
    }
	/// It not sync, get only the transactions related to the specified registrant & user.
	else {
        pred = [NSPredicate predicateWithFormat:@"transactionStatusType.transactionStatusTypeId != %@ AND (incomingRegistrant.registrationNumber == %@ AND incomingUser.userId == %@ OR outgoingRegistrant.registrationNumber == %@ AND outgoingUser.userId == %@)", TRANSACTION_STATUS_TYPE_ID_DELETED, registrantId, userId,registrantId, userId];
    }
    
    [fetchRequest setPredicate:pred];
    
    //create sort descriptor
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"transactionStatusType.transactionStatusTypeId" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:sortIndex ascending:ascending];
    [fetchRequest setSortDescriptors:@[sortDescriptor1, sortDescriptor2]];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
//    for (Transaction *trans in items) {
//        NSLog(@"the number of transactions are: %lu", (unsigned long)[items count]);
//        NSLog(@"The friendly id is: %@, the status is: %@, and the syncStatus is: %@", trans.friendlyId, trans.transactionStatusType.transactionStatusTypeId, trans.transactionSyncStatusType.transactionSyncStatusTypeId);
//    }
    return items;
}

- (NSArray *)transactionsNotSynced {
	
	return [self transactionsNotSyncedLimit:NSUIntegerMax offSet:0];
}
- (NSArray *)transactionsNotSyncedLimit:(NSUInteger)limit offSet:(NSUInteger)offset
{
	
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionSyncStatusType.transactionSyncStatusTypeId != %@ && transactionSyncStatusType.transactionSyncStatusTypeId != %@", TRANSACTION_SYNC_STATUS_TYPE_ID_SYNCHRONIZED, TRANSACTION_SYNC_STATUS_TYPE_ID_REPOSITORY_SYNC_TM];
    [fetchRequest setPredicate:pred];
    
    fetchRequest.fetchLimit = limit;
    fetchRequest.fetchOffset = offset;
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
	
    return items;
}

/*- (NSArray *)transactionsOfType:(NSNumber *)type
{
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionType.transactionTypeId == %@", type];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return items;
}*/


- (NSArray *)transactionsLimit:(NSUInteger)limit offSet:(NSUInteger)offset {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    fetchRequest.fetchLimit = limit;
    fetchRequest.fetchOffset = offset;
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    return items;
}

- (NSArray *)transactionsOfType:(NSNumber *)type
{
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionType.transactionTypeId == %@", type];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return items;
}


- (Authorization *)authorizationForId:(NSNumber *)authorizationId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:AUTHORIZATION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"authorizationId == %@", authorizationId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (PhotoPreselectComment *)photoPreselectCommentForId:(NSNumber *)photoPreselectCommentId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:PHOTO_PRESELECT_COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"photoPreselectCommentId == %@", photoPreselectCommentId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Comment *)commentForId:(NSNumber *)commentId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"commentId == %@", commentId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Photo *)photoForId:(NSNumber *)photoId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:PHOTO_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"photoId == %@", photoId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Document *)documentForId:(NSNumber *)documentId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:DOCUMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"documentId == %@", documentId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (ScaleTicket *)scaleTicketForId:(NSNumber *)scaleTicketId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SCALE_TICKET_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"scaleTicketId == %@", scaleTicketId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (NSArray *)scaleTicketsForTicketNumber:(NSString *)scaleTicketNumber {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SCALE_TICKET_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"ticketNumber == %@", scaleTicketNumber];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}


- (Transaction_Eligibility *)transEligForId:(NSNumber *)transEligibilityId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transEligibilityId == %@", transEligibilityId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Transaction_TireType *)transactionTireTypeForId:(NSNumber *)transactionTireTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionTireTypeId == %@", transactionTireTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (TransactionType *)transactionTypeForId:(NSNumber *)transactionTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionTypeId == %@", transactionTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}


- (User *)userForId:(NSNumber *)userId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:USER_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %@", userId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (User *)userForId:(NSNumber *)userId andRegistrantId:(NSNumber *)registrantId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:USER_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"userId == %@ AND registrant.registrationNumber == %@", userId, registrantId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (TireType *)tireTypeForId:(NSNumber *)tireTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"tireTypeId == %@", tireTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (MaterialType *)materialTypeForId:(NSNumber *)materialTypeId {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:MATERIAL_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"materialTypeId == %@", materialTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (TireType *)tireTypeForName:(NSString *)tireName {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"nameKey == %@", tireName];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}


- (TransactionStatusType *)transactionStatusTypeForId:(NSNumber *)transactionStatusTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_STATUS_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionStatusTypeId == %@", transactionStatusTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (TransactionSyncStatusType *)transactionSyncStatusTypeForId:(NSNumber *)transactionSyncStatusTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionSyncStatusTypeId == %@", transactionSyncStatusTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (ScaleTicketType *)scaleTicketTypeForId:(NSNumber *)scaleTicketTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SCALE_TICKET_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"scaleTicketTypeId == %@", scaleTicketTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (UnitType *)unitTypeForId:(NSNumber *)unitTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:UNIT_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"unitTypeId == %@", unitTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (TransactionType_Module *)transactionTypeModuleForId:(NSNumber *)transactionTypeModuleId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TYPE_MODULE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transTypeModuleId == %@", transactionTypeModuleId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}


- (DocumentType *)documentTypeForId:(NSNumber *)documentTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:DOCUMENT_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"documentTypeId == %@", documentTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Module *)moduleForId:(NSNumber *)moduleId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:MODULE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"moduleId == %@", moduleId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

#pragma mark - Eligibility

- (Transaction_Eligibility *)transactionEligibilityForId:(NSNumber *)transactionEligibilityId {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transEligibilityId == %@", transactionEligibilityId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (Eligibility *)eligibilityForId:(NSNumber *)eligibilityId {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"eligibilityId == %@", eligibilityId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (NSArray *)eligibilitiesOfType:(NSNumber *)theType {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    TransactionType *transactionType = (TransactionType *)[[DataEngine sharedInstance] transactionTypeForId:theType];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionType == %@", transactionType];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (NSArray *)transactionEligibilitiesForEligibility:(Eligibility *)theEligibility {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"eligibility == %@", theEligibility];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (NSArray *)transactionEligibilitiesForTransaction:(Transaction *)theTransaction {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (NSArray *)transactionEligibilitiesNotSyncedForTransaction:(Transaction *)theTransaction {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_ELIGIBILITY_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@ AND syncDate == nil", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

#pragma mark - related documents

- (NSArray *)photosForTransaction:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType{
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:PHOTO_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
	/// Signature piece temporary -- need to figure out better way
    NSPredicate* photoTypePredicate = [NSPredicate predicateWithFormat:@"transaction == %@ && photoType.%K == %@ && fileName != %@", theTransaction, [PhotoType recordIdAttributeName], photoType, @"DELETE"];

    /// Need to make it backward compatible before phototype was implemented
	NSPredicate* nullPhotoType = [NSPredicate predicateWithFormat:@"transaction == %@ && photoType == nil", theTransaction];
	NSPredicate* signature = [NSPredicate predicateWithFormat:@"fileName CONTAINS[cd] %@", @"Signature"];
	
	NSPredicate* predicate;
    
	if([photoType isEqualToNumber:PHOTO_TYPE_ID_PHOTO])
	{
		predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[photoTypePredicate,
																		[NSCompoundPredicate andPredicateWithSubpredicates:@[nullPhotoType, [NSCompoundPredicate notPredicateWithSubpredicate:signature]]]]];
	}
	else if([photoType isEqualToNumber:PHOTO_TYPE_ID_SIGNATURE])
	{
		predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[photoTypePredicate,
																		[NSCompoundPredicate andPredicateWithSubpredicates:@[nullPhotoType, signature]]]];
	}
	else
	{
		predicate = photoTypePredicate;
	}
	
    [fetchRequest setPredicate:predicate];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (NSArray *)photosForSync:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:PHOTO_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
	/// Signature piece temporary -- need to figure out better way
    NSPredicate* photoTypePredicate = [NSPredicate predicateWithFormat:@"transaction == %@ && photoType.%K == %@ && syncDate == nil", theTransaction, [PhotoType recordIdAttributeName], photoType];
    
    /// Need to make it backward compatible before phototype was implemented
	NSPredicate* nullPhotoType = [NSPredicate predicateWithFormat:@"transaction == %@ && photoType == nil", theTransaction];
	NSPredicate* signature = [NSPredicate predicateWithFormat:@"fileName CONTAINS[cd] %@", @"Signature"];
	
	NSPredicate* predicate;
    
	if([photoType isEqualToNumber:PHOTO_TYPE_ID_PHOTO])
	{
		predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[photoTypePredicate,
																		[NSCompoundPredicate andPredicateWithSubpredicates:@[nullPhotoType, [NSCompoundPredicate notPredicateWithSubpredicate:signature]]]]];
	}
	else if([photoType isEqualToNumber:PHOTO_TYPE_ID_SIGNATURE])
	{
		predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[photoTypePredicate,
																		[NSCompoundPredicate andPredicateWithSubpredicates:@[nullPhotoType, signature]]]];
	}
	else
	{
		predicate = photoTypePredicate;
	}
	
    [fetchRequest setPredicate:predicate];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}


- (Comment *)commentForTransaction:(Transaction *)theTransaction {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return [items objectAtIndex:0];
    } else {
        return nil;
    }
}

- (NSArray *)commentsForTransaction:(Transaction *)theTransaction {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}


- (Comment *)commentNotSyncedForTransactionId:(Transaction *)theTransaction {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@ AND syncDate == nil", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return [items objectAtIndex:0];
    } else {
        return nil;
    }
}

- (NSArray *)commentsNotSyncedForTransactionId:(Transaction *)theTransaction {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:COMMENT_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@ AND syncDate == nil", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (Authorization *)authorizationForTransaction:(Transaction *)theTransaction {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:AUTHORIZATION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@", theTransaction];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return [items objectAtIndex:0];
    } else {
        return nil;
    }
}

- (NSArray *)scaleTicketsForTransaction:(Transaction *)theTransaction {
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SCALE_TICKET_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@ AND photo.fileName != %@", theTransaction, @"DELETE"];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}


- (NSArray *)scaleTicketsForTransaction:(Transaction *)theTransaction andPhotoType:(NSNumber *)photoType {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:SCALE_TICKET_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction == %@ AND photo.photoType.%K == %@ AND photo.fileName != %@", theTransaction, [PhotoType recordIdAttributeName], photoType, @"DELETE"];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items;
    } else {
        return nil;
    }
}

- (STCAuthorization *)stcAuthorizationForAuthorization:(Authorization *)theAuthorization {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:STCAUTHORIZATION_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"authorization == %@", theAuthorization];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}

- (GPSLog *)gpsLogForId:(NSNumber *)gpsLogId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:GPS_LOG_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"gpsLogId == %@", gpsLogId];
    [fetchRequest setPredicate:pred];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    if ([items count]>0) {
        return items[0];
    } else {
        return nil;
    }
}


/*return all users*/
- (NSArray *)users {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return items;
}


- (NSArray *)registrantTypes {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegistrantType" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return items;
}


#pragma mark - Registrant
- (void) deleteAllUsers {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        // handle fetch error
    } else {
        
        for (User *recordToDelete in results) {
            
            [context deleteObject:recordToDelete];
            
        }
        
        [context save:&error];
        
    }
    
    
}

- (NSArray *)searchAllRegistrants {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Registrant" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];
    
    return items;
}



- (void) deleteAllRegistrants {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Registrant" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        // handle fetch error
    } else {
        
        for (Registrant *recordToDelete in results) {
            
            [context deleteObject:recordToDelete];
            
        }
        
        [context save:&error];
        
    }
    
    
}

- (void) deleteAllRegistrantTypes {
    
    NSError *error;
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RegistrantType" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        // handle fetch error
    } else {
        
        for (RegistrantType *recordToDelete in results) {
            
            [context deleteObject:recordToDelete];
            
        }
        
        [context save:&error];
    }
}

- (NSArray *)materialTypesForTransactionTypeId:(NSNumber *)transactionTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TYPE_MATERIAL_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionType.transactionTypeId == %@", transactionTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *transactionTypeMaterialTypeArray = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *materialTypeArray = [NSMutableArray new];
    
    for (TransactionType_MaterialType *transactionTypeMatType in transactionTypeMaterialTypeArray) {
        [materialTypeArray addObject:transactionTypeMatType.materialType];
    }
    
    return materialTypeArray;
}


- (NSArray *)tireTypesForTransactionTypeId:(NSNumber *)transactionTypeId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transactionType.transactionTypeId == %@", transactionTypeId];
    [fetchRequest setPredicate:pred];
    
    NSArray *transactionTypeTireTypeArray = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *tireTypeArray = [NSMutableArray new];
    
    for (TransactionType_TireType *transactionTypeTireType in transactionTypeTireTypeArray) {
        [tireTypeArray addObject:transactionTypeTireType.tireType];
    }
    
    return tireTypeArray;
}

- (Transaction_MaterialType *)transactionMaterialTypeForTransactionId:(NSString *)transactionId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_MATERIAL_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction.transactionId == %@", transactionId];
    [fetchRequest setPredicate:pred];
        
    NSArray *transMatTypeArray = [context executeFetchRequest:fetchRequest error:&error];
    if (transMatTypeArray != nil && [transMatTypeArray count]>0) {
        return transMatTypeArray[0];
    }
    return nil;
}

- (NSArray *)transactionTireTypeForTransactionId:(NSString *)transactionId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction.transactionId == %@", transactionId];
    [fetchRequest setPredicate:pred];
    
    // Create sort descriptor
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"tireType.sortIndex" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortOrder]];
    
    NSArray *transactionTireTypeArray = [context executeFetchRequest:fetchRequest error:&error];
    
    return transactionTireTypeArray;
}

- (NSArray *)transactionTireTypeNotSyncedForTransactionId:(NSString *)transactionId {
    
    NSManagedObjectContext *context = [[DataEngine sharedInstance] managedObjectContext];
    NSError *error;
    
    // Create fetch request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:TRANSACTION_TIRE_TYPE_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    // Create predicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"transaction.transactionId == %@ AND syncDate==nil", transactionId];
    [fetchRequest setPredicate:pred];
    
    NSArray *transactionTireTypeArray = [context executeFetchRequest:fetchRequest error:&error];
    
    return transactionTireTypeArray;
}


#pragma mark - Signature Path

- (void)saveSignaturePath:(NSString *)path {
    
    [[[DataEngine sharedInstance] signaturePathArray] addObject:path];
}

- (NSArray *)getArrayContents {
    
    return [[DataEngine sharedInstance] signaturePathArray];
}

// NOTE: Location has a GUID id so has to be special cased.
- (NSArray *)managedObjectsForClass:(NSString *)className sortedByKey:(NSString *)key usingArrayOfIds:(NSArray *)idArray inArrayOfIds:(BOOL)inIds {
    NSArray *results = nil;
     NSManagedObjectContext *managedObjectContext = [[DataEngine sharedInstance] managedObjectContext];
     NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:className];
     NSPredicate *predicate;
     if (inIds) {
         if ([className isEqualToString:LOCATION_ENTITY_NAME]==YES) {
             //predicate = [NSPredicate predicateWithFormat:@"%@ IN %@", key, idArray];
             predicate = [NSPredicate predicateWithFormat:@"locationId IN %@", idArray];
         }
         else {
             predicate = [NSPredicate predicateWithFormat:@"%K IN %@", key, idArray];
         }
     } else {
         predicate = [NSPredicate predicateWithFormat:@"NOT (%K IN %@)", key, idArray];
     }
     [fetchRequest setPredicate:predicate];
     [fetchRequest setSortDescriptors:[NSArray arrayWithObject:
     [NSSortDescriptor sortDescriptorWithKey:key ascending:YES]]];

     NSError *error = nil;
     results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return results;
}

- (User *)userManagedObject:(NSNumber *)userId WithRegistrant:(Registrant *)registrant {
    NSArray *results = nil;
    NSManagedObjectContext *managedObjectContext = [[DataEngine sharedInstance] managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:USER_ENTITY_NAME];
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithFormat:@"registrant == %@ AND userId ==%@", registrant, userId];
    
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([results count]>0) {
        return results[0];
    } else {
        return nil;
    }

}




@end
