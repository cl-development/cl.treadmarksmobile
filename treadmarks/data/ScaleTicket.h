//
//  ScaleTicket.h
//  TreadMarks
//
//  Created by Bobby Yasumura on 2014-05-07.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo, ScaleTicketType, Transaction, UnitType;

#define INVALID_WEIGHT_VALUE 0.0//-1.0//0.0

@interface ScaleTicket : NSManagedObject


@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDecimalNumber * inboundWeight;
@property (nonatomic, retain) NSDecimalNumber * outboundWeight;
@property (nonatomic, retain) NSString * scaleTicketId;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSString * ticketNumber;
@property (nonatomic, retain) Photo *photo;
@property (nonatomic, retain) ScaleTicketType *scaleTicketType;
@property (nonatomic, retain) Transaction *transaction;
@property (nonatomic, retain) UnitType *unitType;

@end
