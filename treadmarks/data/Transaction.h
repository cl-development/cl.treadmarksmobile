//
//  Transaction.h
//  TreadMarks
//
//  Created by Capris Group on 2014-07-09.
//  Copyright (c) 2014 Capris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GPSLog, Location, Photo, Registrant, TransactionStatusType, TransactionSyncStatusType, TransactionType, Transaction_TireType, User, Transaction_MaterialType;

@interface Transaction : NSManagedObject

@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * deviceName;
//@property (nonatomic, retain) NSString * deviceIDFV;
@property (nonatomic, retain) NSNumber * friendlyId;
@property (nonatomic, retain) NSString * incomingSignatureName;
@property (nonatomic, retain) NSDate * modifiedDate;
@property (nonatomic, retain) NSString * outgoingSignatureName;
@property (nonatomic, retain) NSString * postalCode1;
@property (nonatomic, retain) NSString * postalCode2;
@property (nonatomic, retain) NSDate * syncDate;
@property (nonatomic, retain) NSNumber * trailerNumber;
@property (nonatomic, retain) NSDate * transactionDate;
@property (nonatomic, retain) NSString * transactionId;
@property (nonatomic, retain) NSString * versionBuild;
@property (nonatomic, retain) User *createdUser;
@property (nonatomic, retain) GPSLog *incomingGpsLog;
@property (nonatomic, retain) Registrant *incomingRegistrant;
@property (nonatomic, retain) Photo *incomingSignaturePhoto;
@property (nonatomic, retain) User *incomingUser;
@property (nonatomic, retain) User *modifiedUser;
@property (nonatomic, retain) GPSLog *outgoingGPSLog;
@property (nonatomic, retain) Registrant *outgoingRegistrant;
@property (nonatomic, retain) Photo *outgoingSignaturePhoto;
@property (nonatomic, retain) User *outgoingUser;
@property (nonatomic, retain) Location *trailerLocation;
@property (nonatomic, retain) NSSet *transaction_TireTypes;
@property (nonatomic, retain) TransactionStatusType *transactionStatusType;
@property (nonatomic, retain) TransactionSyncStatusType *transactionSyncStatusType;
@property (nonatomic, retain) TransactionType *transactionType;
@property (nonatomic, retain) Transaction_MaterialType *transaction_MaterialType;
@end

@interface Transaction (CoreDataGeneratedAccessors)

- (void)addTransaction_TireTypesObject:(Transaction_TireType *)value;
- (void)removeTransaction_TireTypesObject:(Transaction_TireType *)value;
- (void)addTransaction_TireTypes:(NSSet *)values;
- (void)removeTransaction_TireTypes:(NSSet *)values;

@end
