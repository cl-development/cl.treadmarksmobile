//
//  TestData.m
//  TreadMarks
//
//  Created by Capris Group on 2013-10-21.
//  Copyright (c) 2013 Capris. All rights reserved.
//

#import "TestData.h"
#import "DataEngine.h"
#import "User.h"
#import "Registrant.h"
#import "RegistrantType.h"
#import "TransactionType.h"
#import "TransactionStatusType.h"
#import "TransactionSyncStatusType.h"
#import "Transaction_TireType.h"
#import "Transaction.h"
#import "TransactionType_Module.h"
#import "TransactionType_RegistrantType.h"
#import "TransactionType_TireType.h"
#import "Transaction_Eligibility.h"
#import "Authorization.h"
#import "ScaleTicketType.h"
#import "Message.h"
#import "Eligibility.h"
#import "STCAuthorization.h"
#import "ScaleTicket.h"
#import "Comment.h"
#import "Module.h"
#import "Document.h"
#import "DocumentType.h"
#import "TireType.h"
#import "UnitType.h"
#import "Location.h"
#import "Photo.h"
#import "PhotoPreselectComment.h"
#import "CSVParser.h"
#import "Constants.h"
#import "Utils.h"

@interface TestData ()

@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) NSArray *myArray;

@end

@implementation TestData

+(void)populateDatabase{
    
    [Utils log:@"populateDatabase"];
    [self clearDatabase];
    
    //populate the independet entities
    
    [self populateTireType];
    [self populateTransactionType];
    [self populateTransactionStatusType];
    [self populateTransactionSyncStatusType];
    [self populateRegistrantType];
    [self populateDocumentType];
    [self populateScaleTicketType];
    [self populateMessage];
    [self populateUnitType];
    //[self populateStatusType];
    [self populateLocation];
    [self populatePhoto];
    [self populatePhotoPreselectComment];
    
    //populate first level of entities depended on the above ones or the ones behind them
    [self populateRegistrant];
    //[self addRegistrants];
    //[self printMe];
    [self populateUser];
    [self populateScaleTicket];
    
    [self populateTransaction];
    [self populateDocument];
    [self populateTransaction_TireType];
    [self populateComment];
    [self populateSTCAuthorization];
    [self populateTransactionTypeRegistrantType];
    [self populateTransactionEligibility];
    [self populateAuthorization];
    [self populateTransactionTypeTireType];
    [self populateEligibility];
    [self populateModule];
    [self populateTransactionTypeModule];
}

+(void)clearDatabase {
    [[DataEngine sharedInstance] deleteAllEntities:TIRE_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:REGISTRANT_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:DOCUMENT_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:SCALE_TICKET_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:MESSAGE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:UNIT_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:LOCATION_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:PHOTO_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:REGISTRANT_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:USER_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:SCALE_TICKET_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:DOCUMENT_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:COMMENT_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:STCAUTHORIZATION_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:AUTHORIZATION_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:ELIGIBILITY_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:MODULE_ENTITY_NAME];
    [[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
}

//end

+(void)populateRegistrantType{
    [Utils log:@"populateRegistrantType"];

    //delete the registrant types from database
    //[[DataEngine sharedInstance] deleteAllEntities:REGISTRANT_TYPE_ENTITY_NAME];
    
    int index=1;
    RegistrantType *registrantType = (RegistrantType *) [[DataEngine sharedInstance] newEntity:REGISTRANT_TYPE_ENTITY_NAME];
    registrantType.registrantTypeId = [NSNumber numberWithInteger:index++];
    registrantType.descriptionKey  = registrantType.shortDescriptionKey = @"Steward";
    registrantType.fileName = @"icon-steward.png";
    
    registrantType = (RegistrantType *) [[DataEngine sharedInstance] newEntity:REGISTRANT_TYPE_ENTITY_NAME];
    registrantType.registrantTypeId = [NSNumber numberWithInteger:index++];
    registrantType.descriptionKey  = registrantType.shortDescriptionKey = @"Collector";
    registrantType.fileName = @"icon-collector.png";
    
    registrantType = (RegistrantType *) [[DataEngine sharedInstance] newEntity:REGISTRANT_TYPE_ENTITY_NAME];
    registrantType.registrantTypeId = [NSNumber numberWithInteger:index++];
    registrantType.descriptionKey  = registrantType.shortDescriptionKey = @"Hauler";
    registrantType.fileName = @"icon-hauler.png";
    
    registrantType = (RegistrantType *) [[DataEngine sharedInstance] newEntity:REGISTRANT_TYPE_ENTITY_NAME];
    registrantType.registrantTypeId = [NSNumber numberWithInteger:index++];
    registrantType.descriptionKey  = registrantType.shortDescriptionKey = @"Processor";
    registrantType.fileName = @"icon-processor.png";
    
    registrantType = (RegistrantType *) [[DataEngine sharedInstance] newEntity:REGISTRANT_TYPE_ENTITY_NAME];
    registrantType.registrantTypeId = [NSNumber numberWithInteger:index++];
    registrantType.descriptionKey = @"Recycled Product Manufacturer";
    registrantType.shortDescriptionKey = @"RPM";
    registrantType.fileName = @"icon-rpm.png";
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:REGISTRANT_TYPE_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}

+(void)populateLocation{
    [Utils log:@"populateLocation"];

    //[[DataEngine sharedInstance] deleteAllEntities:LOCATION_ENTITY_NAME];
    
    int index=1;
    Location *location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInteger:index++];
    location.name = @"Liberty";
    location.address1 = @"215 Traders Blvd E";
    location.address2 = @"Unit 11";
    location.city = @"Mississauga";
    location.province = @"ON";
    location.postalCode = @"L4Z 3K5";
    location.country = @"CANADA";
    location.phone = @"905-507-8151";
    location.fax = @"905-507-8683";
    location.latitude = @"43.627875";
    location.longitude = @"-79.669397";
    
    
    location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInteger:index++];
    location.name = @"Collector A";
    location.address1 = @"300 The East Mall";
    location.address2 = @"Unit 100";
    location.address3 = @"Attn: Zach";
    location.city = @"Etobicoke";
    location.province = @"ON";
    location.postalCode = @"M2A 3J3";
    location.country = @"CANADA";
    location.phone = @"905-555-9804";
    location.latitude = @"43.805525";
    location.longitude = @"-80.444567";
    
    location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInteger:index++];
    location.name = @"Collector B";
    location.address1 = @"100 Yonge Street";
    location.city = @"Toronto";
    location.province = @"ON";
    location.postalCode = @"M9D 2X5";
    location.country = @"Canada";
    location.phone = @"416-897-0034";
    location.fax = @"416-980-0013";
    location.latitude = @"43.805525";
    location.longitude = @"-80.444567";
    
    
    location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInteger:index++];
    location.name = @"Processor Z";
    location.address1 = @"9 Parkway";
    location.city = @"Waterloo";
    location.province = @"ON";
    location.postalCode = @"N1C 9P0";
    location.country = @"Canada";
    location.phone = @"519-807-0012";
    location.latitude = @"43.627875";
    location.longitude = @"-79.669397";
    
    location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInteger:index++];
    location.name = @"Some staging area";
    location.address1 = @"1 Street";
    location.city = @"Nowhere";
    location.province = @"ON";
    location.postalCode = @"M1C 3B3";
    location.country = @"Canada";
    location.phone = @"905-888-1234";
    location.fax = @"905-887-1234";
    location.latitude = @"43.805525";
    location.longitude = @"-80.444567";
    
    location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
    location.locationId = [NSNumber numberWithInt:index++];
    location.address1 = @"R R 5 STATION MAIN, 202391 SUNSET STRIP";
    location.city = @"OWEN SOUND";
    location.province = @"ON";
    location.postalCode = @"N4K5N7";
    location.country = @"Canada";
    location.phone = @"519-376-0974";
    location.fax = @"519-376-5711";
    
    /* to be added
     1	2013-10-04 15:12:00	Liberty	215 Traders Blvd E	Unit 11		Mississauga	ON	L4Z 3K5	CANADA	905-507-8151	905-507-8683	43.627875	-79.669397
     2	2013-10-04 15:12:00	Collector A	300 The East Mall	Unit 100	Attn: Zach	Etobicoke	ON	M2A 3J3	CANADA	905-555-9804		43.805525	-80.444567
     3	2013-10-04 15:12:00	Collector B	100 Yonge Street			Toronto	ON	M9D 2X5	CANADA	416-897-0034	416-980-0013	44.990514	-75.414679
     4	2013-10-04 15:12:00	Processor Z	9 Parkway			Waterloo	ON	N1C 9P0	CANADA	519-807-0012		43.627875	-79.669397
     5	2013-10-04 15:12:00	Some staging area	1 Street			Nowhere	ON	M1C 3B3	CANADA	905-888-1234	905-887-1234	43.805525	-80.444567
     
     */

    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:LOCATION_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }


}

+(void)populateRegistrant{
    [Utils log:@"populateRegistrant"];
    
    //delete the registrant types from database
    //[[DataEngine sharedInstance] deleteAllEntities:REGISTRANT_ENTITY_NAME];
    
    Registrant *registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
    registrant.registrationNumber = [NSNumber numberWithInteger:3000180];
    registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:3]];
    registrant.businessName = @"Liberty";
    registrant.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:1]];
    registrant.metaData = @"{\"additional_info\":\"aaa\", \"another_field\":\"etc\"}";

    registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
    registrant.registrationNumber = [NSNumber numberWithInteger:2001044];
    registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:2]];
    registrant.businessName = @"Collector A";
    registrant.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:2]];

    registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
    registrant.registrationNumber = [NSNumber numberWithInteger:2000454];
    registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:2]];
    registrant.businessName = @"Collector B";
    registrant.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:3]];

    registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
    registrant.registrationNumber = [NSNumber numberWithInteger:4000027];
    registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:4]];
    registrant.businessName = @"Collector Z";
    registrant.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:4]];
    
    registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
    registrant.registrationNumber = [NSNumber numberWithInteger:2008500];
    registrant.registrantType = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:1]];
    registrant.businessName = @"451455 Ontario LTD";
    registrant.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:5]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:REGISTRANT_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}


+ (void) addRegistrants {
    
//    NSNumber *userNumber = [NSNumber numberWithInt:12345];
//    
//    NSLog(@"Initial check to see what objects are in Core Data store...");
//    
//    User *result = [[DataEngine sharedInstance] userForId:userNumber];
//    
//    if (result == nil) {
//        
//        NSLog(@"Store is empty");
//        
//    }
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"registrants" ofType:@"csv"];
    TestData *tData;
    [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                    withSeparatedCharacterString:@","
                            quoteCharacterString:nil
                                       withBlock:^(NSArray *theArray, NSError *error) {
                                           tData.myArray = theArray;
                                           //[self printMe];
                                           
                                       }];
    
    
}

+ (void)printMe {
    
    TestData *aData;
    //NSLog(@"%@", aData.myArray);
    
    for (int i = 1; i < [aData.myArray count]-1; i++) {
        
        NSLog(@"Adding object %d", i);
        NSArray *registrants = [aData.myArray objectAtIndex:i];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *myNumber = [f numberFromString:[registrants objectAtIndex:0]];
        NSNumber *regNumberType = [f numberFromString:[registrants objectAtIndex:1]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *businessName = [[registrants objectAtIndex:2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *address1 = [[registrants objectAtIndex:3] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *city = [[registrants objectAtIndex:6] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSString *create = [registrants objectAtIndex:12];
        NSString *newCreatedString = [create stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        NSDate *createdDate = [dateFormatter dateFromString:newCreatedString];
        
        NSString *modif = [registrants objectAtIndex:13];
        NSString *newModif = [modif stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        NSDate *modifiedDate = [dateFormatter dateFromString:newModif];
        
        Location *location = (Location *) [[DataEngine sharedInstance] newEntity:LOCATION_ENTITY_NAME];
        location.locationId = [NSNumber numberWithInt:i];
        location.address1 = address1;
        location.address2 = [registrants objectAtIndex:4];
        location.address3 = [registrants objectAtIndex:5];
        location.city = city;
        location.province = [registrants objectAtIndex:7];
        location.postalCode = [registrants objectAtIndex:8];
        location.country = [registrants objectAtIndex:9];
        location.phone = [registrants objectAtIndex:10];
        location.fax = [registrants objectAtIndex:11];
        
        Registrant *registrant = (Registrant *) [[DataEngine sharedInstance] newEntity:REGISTRANT_ENTITY_NAME];
        registrant.registrationNumber = myNumber;
        registrant.registrantType  = [[DataEngine sharedInstance] registrantTypeForId:regNumberType];
        //registrant.businessName = [registrants objectAtIndex:2];
        registrant.businessName = businessName;
        registrant.location = location;
        registrant.createdDate = createdDate;
        registrant.modifiedDate = modifiedDate;
        registrant.metaData = nil;
        
        [[DataEngine sharedInstance]saveContext];
        
        
    }
    
    
    NSArray * array = [[DataEngine sharedInstance] entities:REGISTRANT_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
    
    NSLog(@"what about here?");
    
    //[[DataEngine sharedInstance] deleteAllRegistrants];
    
    NSArray *testRegistrants = [[DataEngine sharedInstance] searchAllRegistrants];
    
    if ([testRegistrants count] == 0) {
        
        NSLog(@"All registrants have been deleted");
        
    }
    
    else {
        
        NSLog(@"%d objects were stored", [testRegistrants count]);
    }
    
}


+(void)populateTransactionType{
    [Utils log:@"populateTransactionType"];
    
    //delete the TransactionTypes from database
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_ENTITY_NAME];
    
    int index=1;
    
    TransactionType *transType = (TransactionType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_ENTITY_NAME];
    transType.transactionTypeId = [NSNumber numberWithInt:index++];
    transType.nameKey = @"Tire_Collection_Receipt";
    transType.shortNameKey = @"TCR";
    transType.fileName = @"tcr.png";
    transType.sortIndex = [NSNumber numberWithInt:0];
    transType.outgoingSignatureKey = @"I_certify_that_these_used_tires_were_accumulated_in_Ontario_after_Aug_31_2009";
    transType.incomingSignatureKey = @"I_certify_picking_up_the_quantities_of_tires_noted_above";
    transType.tireCountMessageKey = @"Enter_the_number_of_tires_you_are_collecting_for_each_category.__Enter_zero_if_none.__To_enter_tire_counts,_tap_the_begin_button.";
    
    
    transType = (TransactionType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_ENTITY_NAME];
    transType.transactionTypeId = [NSNumber numberWithInt:index++];
    transType.nameKey = @"Dedicated_Off-the-Road_Tire_Collection";
    transType.shortNameKey = @"DOT";
    transType.fileName = @"dot.png";
    transType.sortIndex = [NSNumber numberWithInt:10];
    transType.outgoingSignatureKey = @"I_certify_that_these_used_tires_were_accumulated_in_Ontario_after_Aug_31_2009";
    transType.incomingSignatureKey = @"I_certify_picking_up_the_quantities_of_tires_and_the_total_weight_noted_above";
    transType.tireCountMessageKey = @"Enter_the_number_of_tires_you_are_collecting_for_each_category.__Enter_zero_if_none.__To_enter_tire_counts,_tap_the_begin_button.";
    
    
    transType = (TransactionType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_ENTITY_NAME];
    transType.transactionTypeId = [NSNumber numberWithInt:index++];
    transType.nameKey = @"Hauler_Inventory_Transfer";
    transType.shortNameKey = @"HIT";
    transType.fileName = @"hit.png";
    transType.sortIndex = [NSNumber numberWithInt:20];
    transType.outgoingSignatureKey = @"I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector_or_through_an_eligible_OTS_Special_Collection_Event_and_were_collected_after_August_31_2009";
    transType.incomingSignatureKey = @"I_certify_receiving_the_quantities_of_tires_noted_above_and_will_deliver_the_tires_to_an_appropriate_OTS_end_use";
    transType.tireCountMessageKey = @"Enter_the_number_of_tires_you_are_transferring_for_each_category.__Enter_zero_if_none.__To_enter_tire_counts,_tap_the_begin_button.";
    
    transType = (TransactionType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_ENTITY_NAME];
    transType.transactionTypeId = [NSNumber numberWithInt:index++];
    transType.nameKey = @"Special_Tire_Collection";
    transType.shortNameKey = @"STC";
    transType.fileName = @"stc.png";
    transType.sortIndex = [NSNumber numberWithInt:40];
    transType.outgoingSignatureKey = @"I_certify_that_these_used_tires_were_accumulated_in_Ontario";
    transType.incomingSignatureKey = @"I_certify_picking_up_the_quantities_of_tires_noted_above";
    transType.tireCountMessageKey = @"Enter_the_number_of_tires_you_are_collecting_for_each_category.__Enter_zero_if_none.__To_enter_tire_counts,_tap_the_begin_button.";
    
    transType = (TransactionType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_ENTITY_NAME];
    transType.transactionTypeId = [NSNumber numberWithInt:index++];
    transType.nameKey = @"Processor_Tire_Receipt";
    transType.shortNameKey = @"PTR";
    transType.fileName = @"ptr.png";
    transType.sortIndex = [NSNumber numberWithInt:60];
    transType.outgoingSignatureKey = @"I_certify_that_these_used_tires_were_picked_up_from_an_OTS_registered_Collector_an_unregistered_collection_site_or_an_eligible_OTS_Special_Collection_Event_after_August_31_2009";
    transType.incomingSignatureKey = @"I_acknowledge_receipt_of_the_tires_in_Part_1_for_processing_/_recycling_on_the_delivery_date_indicated_on_this_form";
    transType.tireCountMessageKey = @"Enter_the_number_of_tires_you_are_delivering_for_each_category.__Enter_zero_if_none.__To_enter_tire_counts,_tap_the_begin_button.";
    
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_TYPE_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}


+(void)populateTransaction_TireType{
    
    [Utils log:@"populateTransactionTireType"];
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    
    int index=1;
    
    Transaction_TireType *transTireType = (Transaction_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    transTireType.transactionTireTypeId = [NSNumber numberWithInt:index++];
    transTireType.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    transTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:1]];
    transTireType.quantity = [NSNumber numberWithInt:12];
    transTireType.syncDate = [NSDate date];
    
    
    transTireType = (Transaction_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    transTireType.transactionTireTypeId = [NSNumber numberWithInt:index++];
    transTireType.quantity = [NSNumber numberWithInt:4];
    transTireType.syncDate = [NSDate date];
    transTireType.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    transTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:2]];
    
    transTireType = (Transaction_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    
    transTireType.transactionTireTypeId = [NSNumber numberWithInt:index++];
    transTireType.quantity = [NSNumber numberWithInt:0];
    transTireType.syncDate = [NSDate date];
    transTireType.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    transTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:3]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateScaleTicket{
    
    [Utils log:@"populateScaleTicket"];
    
    //delete the TransactionStatus from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:SCALE_TICKET_ENTITY_NAME];
  
    int index=1;
    
    ScaleTicket *scaleTicket = (ScaleTicket *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_ENTITY_NAME];
    
    scaleTicket.scaleTicketId = [NSNumber numberWithInt:index];
    scaleTicket.sortIndex = [NSNumber numberWithInt:1];
    scaleTicket.ticketNumber = [NSNumber numberWithInt:5701];
    scaleTicket.inboundWeight = [NSNumber numberWithInt:80655];
    scaleTicket.outboundWeight = NULL;
    scaleTicket.syncDate = NULL;
    scaleTicket.date = [NSDate date];
    scaleTicket.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:1]];
    scaleTicket.scaleTicketType = [[DataEngine sharedInstance] scaleTicketTypeForId:[NSNumber numberWithInt:1]];
    scaleTicket.transaction = [[DataEngine sharedInstance] transactionForId:@"372EF412-7F3E-4736-910F-64B96F99A0D7"];
    scaleTicket.unitType = [[DataEngine sharedInstance] unitTypeForId:[NSNumber numberWithInt:1]];
    
    index++;
    scaleTicket = (ScaleTicket *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_ENTITY_NAME];
    
    scaleTicket.scaleTicketId = [NSNumber numberWithInt:index];
    scaleTicket.sortIndex = [NSNumber numberWithInt:2];
    scaleTicket.ticketNumber = [NSNumber numberWithInt:5702];
    scaleTicket.inboundWeight = NULL;
    scaleTicket.outboundWeight = [NSNumber numberWithInt:72600];
    scaleTicket.syncDate = NULL;
    scaleTicket.date = [NSDate date];
    scaleTicket.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:2]];
    scaleTicket.scaleTicketType = [[DataEngine sharedInstance] scaleTicketTypeForId:[NSNumber numberWithInt:2]];
    scaleTicket.transaction = [[DataEngine sharedInstance] transactionForId:@"372EF412-7F3E-4736-910F-64B96F99A0D7"];
    scaleTicket.unitType = [[DataEngine sharedInstance] unitTypeForId:[NSNumber numberWithInt:1]];
    
    index++;
    scaleTicket = (ScaleTicket *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_ENTITY_NAME];
    
    scaleTicket.scaleTicketId = [NSNumber numberWithInt:index++];
    scaleTicket.sortIndex = [NSNumber numberWithInt:1];
    scaleTicket.ticketNumber = [NSNumber numberWithInt:4099];
    scaleTicket.inboundWeight = [NSNumber numberWithInt:40699];
    scaleTicket.outboundWeight = [NSNumber numberWithInt:37200];
    scaleTicket.syncDate = [NSDate date];
    scaleTicket.date = [NSDate date];
    scaleTicket.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:3]];
    scaleTicket.scaleTicketType = [[DataEngine sharedInstance] scaleTicketTypeForId:[NSNumber numberWithInt:3]];
    scaleTicket.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    scaleTicket.unitType = [[DataEngine sharedInstance] unitTypeForId:[NSNumber numberWithInt:1]];
    
    [[DataEngine sharedInstance]saveContext];
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:SCALE_TICKET_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateComment{
    
    [Utils log:@"populateComment"];
    
    //[[DataEngine sharedInstance] deleteAllEntities:COMMENT_ENTITY_NAME];
   
    int index=1;
    
    Comment *comment = (Comment *) [[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    
    comment.commentId = [NSNumber numberWithInt:index++];
    comment.text = @"Lorem Ipsum";
    comment.createdDate = [NSDate date];
    comment.syncDate = [NSDate date];
    comment.transaction = [[DataEngine sharedInstance] transactionForId:@"B945D138-BE52-4914-95DC-8317173146A6"];
    comment.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    

    comment = (Comment *) [[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    
    comment.commentId = [NSNumber numberWithInt:index++];
    comment.text = @"Fusce eget mauris sed ligula convallis interdum eget id odio. Integer ut arcu commodo, congue turpis et, consequat tortor.";
    comment.createdDate = [NSDate date];
    comment.syncDate = [NSDate date];
    comment.transaction = [[DataEngine sharedInstance] transactionForId:@"372EF412-7F3E-4736-910F-64B96F99A0D7"];
    comment.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];;
    
    
    comment = (Comment *) [[DataEngine sharedInstance] newEntity:COMMENT_ENTITY_NAME];
    
    comment.commentId = [NSNumber numberWithInt:index++];
    comment.text = @"Curabitur non purus quis";
    comment.createdDate = [NSDate date];
    comment.syncDate = [NSDate date];
    comment.transaction = [[DataEngine sharedInstance] transactionForId:@"5B55A179-383A-426E-A650-7D7F32CB7713"];
    comment.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    
    [[DataEngine sharedInstance]saveContext];
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:COMMENT_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populatePhoto{
    
    [Utils log:@"populatePhoto"];
    
    //[[DataEngine sharedInstance] deleteAllEntities:PHOTO_ENTITY_NAME];
    
    int index=1;
  
    Photo *photo = (Photo *) [[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
    
    photo.photoId = [NSNumber numberWithInt:index++];
    photo.fileName = @"IMG_0758.JPG";
    photo.comments = @"Dirty Tires";
    photo.latitude = @"43.627875";
    photo.longitude = @"-79.669397";
    photo.date = [NSDate date];
    photo.createdDate = [NSDate date];
    photo.syncDate = [NSDate date];
    
    photo = (Photo *) [[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
   
    photo.photoId = [NSNumber numberWithInt:index++];
    photo.fileName = @"IMG_0800.JPG";
    photo.comments = @"Tires covered in snow.";
    photo.latitude = @"43.805525";
    photo.longitude = @"-80.444567";
    photo.date = [NSDate date];
    photo.createdDate = [NSDate date];
    photo.syncDate = [NSDate date];
    
    photo = (Photo *) [[DataEngine sharedInstance] newEntity:PHOTO_ENTITY_NAME];
    
    photo.photoId = [NSNumber numberWithInt:index++];
    photo.fileName = @"IMG_0841.JPG";
    photo.comments = @"Lorem Ipsum";
    photo.latitude = @"44.990514";
    photo.longitude = @"-75.414679";
    photo.date = [NSDate date];
    photo.createdDate = [NSDate date];
    photo.syncDate = [NSDate date];
    
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:PHOTO_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateModule{
    
    [Utils log:@"populateModule"];
    
    //[[DataEngine sharedInstance] deleteAllEntities:MODULE_ENTITY_NAME];
    
    int index=1;
   
    Module *module = (Module *) [[DataEngine sharedInstance] newEntity:MODULE_ENTITY_NAME];
    
    module.moduleId = [NSNumber numberWithInt:index++];
    module.nameKey = @"file1";
    
    module = (Module *) [[DataEngine sharedInstance] newEntity:MODULE_ENTITY_NAME];
    
    module.moduleId = [NSNumber numberWithInt:index++];
    module.nameKey = @"file2";
    
    module = (Module *) [[DataEngine sharedInstance] newEntity:MODULE_ENTITY_NAME];
    
    module.moduleId = [NSNumber numberWithInt:index++];
    module.nameKey = @"file3";
   
    [[DataEngine sharedInstance]saveContext];
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:MODULE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateDocument{
    
    [Utils log:@"populateDocument"];
    
    //delete the Document entities from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:DOCUMENT_ENTITY_NAME];
  
    int index=1;
   
    Document *document = (Document *) [[DataEngine sharedInstance] newEntity:DOCUMENT_ENTITY_NAME];
    document.documentId = [NSNumber numberWithInt:index];
    document.documentName = @"Invoice #8044";
    document.documentNumber = @"8044";
    document.documentTypeOther = @"";
    document.sortIndex = [NSNumber numberWithInt:index];
    document.date = [NSDate date];
    document.documentType = [[DataEngine sharedInstance] documentTypeForId:[NSNumber numberWithInt:index]];
    document.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    document.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:4]];
    
    document = (Document *) [[DataEngine sharedInstance] newEntity:DOCUMENT_ENTITY_NAME];
    document.documentId = [NSNumber numberWithInt:++index];
    document.documentName = @"BOL 1/2";
    document.documentNumber = @"BOL-0001";
    document.documentTypeOther = @"";
    document.sortIndex = [NSNumber numberWithInt:index];
    document.date = [NSDate date];
    document.documentType = [[DataEngine sharedInstance] documentTypeForId:[NSNumber numberWithInt:index]];
    document.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    document.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:5]];
    
    document = (Document *) [[DataEngine sharedInstance] newEntity:DOCUMENT_ENTITY_NAME];
    document.documentId = [NSNumber numberWithInt:++index];
    document.documentName = @"BOL 2/2";
    document.documentNumber = @"BOL-0002";
    document.documentTypeOther = @"";
    document.sortIndex = [NSNumber numberWithInt:index];
    document.date = [NSDate date];
    document.documentType = [[DataEngine sharedInstance] documentTypeForId:[NSNumber numberWithInt:index]];
    document.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    document.photo = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:6]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:DOCUMENT_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populatePhotoPreselectComment{
    
    [Utils log:@"populatePhotoComment"];
    
    //delete the PhotoComment from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    
    int index=1;
   
    PhotoPreselectComment *photoPreselectComment = (PhotoPreselectComment *) [[DataEngine sharedInstance] newEntity:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    photoPreselectComment.photoPreselectCommentId = [NSNumber numberWithInt:index++];
    photoPreselectComment.nameKey = @"Dirty_Tires_Noted";
    photoPreselectComment.sortIndex = [NSNumber numberWithInt:10];
    photoPreselectComment.syncDate = [NSDate date];
    
    photoPreselectComment = (PhotoPreselectComment *) [[DataEngine sharedInstance] newEntity:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    photoPreselectComment.photoPreselectCommentId = [NSNumber numberWithInt:index++];
    photoPreselectComment.nameKey = @"Snowy_Tires_Noted";
    photoPreselectComment.sortIndex = [NSNumber numberWithInt:20];
    photoPreselectComment.syncDate = [NSDate date];
    
    photoPreselectComment = (PhotoPreselectComment *) [[DataEngine sharedInstance] newEntity:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    photoPreselectComment.photoPreselectCommentId = [NSNumber numberWithInt:index++];
    photoPreselectComment.nameKey = @"Rims_Indicated";
    photoPreselectComment.sortIndex = [NSNumber numberWithInt:30];
    photoPreselectComment.syncDate = [NSDate date];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:PHOTO_PRESELECT_COMMENT_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
    
}



+(void)populateSTCAuthorization{
    
    [Utils log:@"populateSTCAuthorization"];
    
    //delete the STCAuthorization from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:STCAUTHORIZATION_ENTITY_NAME];
    
    int index=1;
    
    STCAuthorization *stc = (STCAuthorization *) [[DataEngine sharedInstance] newEntity:STCAUTHORIZATION_ENTITY_NAME];
    stc.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:index]];
    stc.authorization = [[DataEngine sharedInstance] authorizationForId:[NSNumber numberWithInt:index++]];
    
    stc = (STCAuthorization *) [[DataEngine sharedInstance] newEntity:STCAUTHORIZATION_ENTITY_NAME];
    stc.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:index]];
    stc.authorization = [[DataEngine sharedInstance] authorizationForId:[NSNumber numberWithInt:index++]];
    
    stc = (STCAuthorization *) [[DataEngine sharedInstance] newEntity:STCAUTHORIZATION_ENTITY_NAME];
    stc.location = [[DataEngine sharedInstance] locationForId:[NSNumber numberWithInt:index]];
    stc.authorization = [[DataEngine sharedInstance] authorizationForId:[NSNumber numberWithInt:index++]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:STCAUTHORIZATION_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateDocumentType{
    
    [Utils log:@"populateDocumentType"];
    
    //delete the DocumentType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:DOCUMENT_TYPE_ENTITY_NAME];
    
    int index=1;
    
    DocumentType *docType = (DocumentType *) [[DataEngine sharedInstance] newEntity:DOCUMENT_TYPE_ENTITY_NAME];
    
    docType.documentTypeId = [NSNumber numberWithInt:index++];
    docType.nameKey = @"Invoice";
    docType.sortIndex = [NSNumber numberWithInt:0];
    docType.syncDate = [NSDate date];
    
    docType = (DocumentType *) [[DataEngine sharedInstance] newEntity:DOCUMENT_TYPE_ENTITY_NAME];
    
    docType.documentTypeId = [NSNumber numberWithInt:index++];
    docType.nameKey = @"Bill_of_Lading";
    docType.sortIndex = [NSNumber numberWithInt:10];
    docType.syncDate = [NSDate date];
    
    docType = (DocumentType *) [[DataEngine sharedInstance] newEntity:DOCUMENT_TYPE_ENTITY_NAME];
    
    docType.documentTypeId = [NSNumber numberWithInt:index++];
    docType.nameKey = @"Other";
    docType.sortIndex = [NSNumber numberWithInt:20];
    docType.syncDate = [NSDate date];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:DOCUMENT_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}


+(void)populateTransactionTypeModule{
    
    [Utils log:@"populateTransactionTypeModule"];
    
    //delete the TransactionTypeModule from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
    
    int index=1;
    
    TransactionType_Module *transTypeModule = (TransactionType_Module *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
    
    transTypeModule.transactionTypeModuleId = [NSNumber numberWithInt:index];
    transTypeModule.step = [NSNumber numberWithInt:index];
    transTypeModule.sortIndex = [NSNumber numberWithInt:0];
    transTypeModule.isRequired = [NSNumber numberWithInt:index];
    transTypeModule.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeModule.module = [[DataEngine sharedInstance] moduleForId:[NSNumber numberWithInt:1]];
    index++;
    
    transTypeModule = (TransactionType_Module *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
    
    transTypeModule.transactionTypeModuleId = [NSNumber numberWithInt:index];
    transTypeModule.step = [NSNumber numberWithInt:index];
    transTypeModule.sortIndex = [NSNumber numberWithInt:10];
    transTypeModule.isRequired = [NSNumber numberWithInt:index];
    transTypeModule.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeModule.module = [[DataEngine sharedInstance] moduleForId:[NSNumber numberWithInt:1]];
    index++;
    
    transTypeModule = (TransactionType_Module *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
    
    transTypeModule.transactionTypeModuleId = [NSNumber numberWithInt:index];
    transTypeModule.step = [NSNumber numberWithInt:index];
    transTypeModule.sortIndex = [NSNumber numberWithInt:20];
    transTypeModule.isRequired = [NSNumber numberWithInt:index];
    transTypeModule.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeModule.module = [[DataEngine sharedInstance] moduleForId:[NSNumber numberWithInt:3]];
    index++;
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_TYPE_MODULE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateTransactionTypeRegistrantType{
    
    [Utils log:@"populateTransactionTypeRegistrantType"];
    
    //delete the TransactionTypeRegistrantType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    
     int index=1;
    
    TransactionType_RegistrantType *transTypeRegType = (TransactionType_RegistrantType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    transTypeRegType.transactionTypeRegistrantTypeId = [NSNumber numberWithInt:index];
    transTypeRegType.syncDate = [NSDate date];
    transTypeRegType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index++]];
    transTypeRegType.registrantType = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:3]];
    
    transTypeRegType = (TransactionType_RegistrantType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    transTypeRegType.transactionTypeRegistrantTypeId = [NSNumber numberWithInt:index];
    transTypeRegType.syncDate = [NSDate date];
    transTypeRegType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index++]];
    transTypeRegType.registrantType = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:3]];
    
    transTypeRegType = (TransactionType_RegistrantType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    transTypeRegType.transactionTypeRegistrantTypeId = [NSNumber numberWithInt:index];
    transTypeRegType.syncDate = [NSDate date];
    transTypeRegType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index++]];
    transTypeRegType.registrantType = [[DataEngine sharedInstance] registrantTypeForId:[NSNumber numberWithInt:3]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_TYPE_REGISTRANT_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}


+(void)populateTransactionEligibility{
    
    [Utils log:@"populateTransactionEligibility"];
    
    //delete the TransactionElibility from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    
    int index=1;
    
    Transaction_Eligibility *transElig = (Transaction_Eligibility *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    transElig.transactionEligibilityId = [NSNumber numberWithInt:index++];
    transElig.value = [NSNumber numberWithInt:1];
    transElig.syncDate = [NSDate date];
    transElig.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    transElig.eligibility = [[DataEngine sharedInstance] eligibilityForId:[NSNumber numberWithInt:1]];
    
    transElig = (Transaction_Eligibility *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    transElig.transactionEligibilityId = [NSNumber numberWithInt:index++];
    transElig.value = [NSNumber numberWithInt:2];
    transElig.syncDate = [NSDate date];
    transElig.transaction = [[DataEngine sharedInstance] transactionForId:@"97E51BA5-09DD-4F99-A9FF-C55F08CA7F0C"];
    transElig.eligibility = [[DataEngine sharedInstance] eligibilityForId:[NSNumber numberWithInt:1]];

    transElig = (Transaction_Eligibility *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    transElig.transactionEligibilityId = [NSNumber numberWithInt:index++];
    transElig.value = [NSNumber numberWithInt:2];
    transElig.syncDate = [NSDate date];
    transElig.transaction = [[DataEngine sharedInstance] transactionForId:@"87D552BE-A796-4AE1-A9A8-6B98501FF02C"];
    transElig.eligibility = [[DataEngine sharedInstance] eligibilityForId:[NSNumber numberWithInt:2]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_ELIGIBILITY_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}


+(void)populateAuthorization{
    
    [Utils log:@"populateAuthorization"];
    
    //delete the Authorization from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:AUTHORIZATION_ENTITY_NAME];
    
    int index=1;
    
    Authorization *auth = (Authorization *) [[DataEngine sharedInstance] newEntity:AUTHORIZATION_ENTITY_NAME];
    auth.authorizationId = [NSNumber numberWithInt:index];
    auth.authorizationNumber = [NSNumber numberWithInt:index];
    auth.syncDate = [NSDate date];
    auth.expiryDate = [NSDate date];
    auth.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:index]];
    auth.transaction = [[DataEngine sharedInstance] transactionForId:@"39B06EBB-01F6-401E-9B7B-69D10EC19DDF"];
    auth.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index]];
    
    auth = (Authorization *) [[DataEngine sharedInstance] newEntity:AUTHORIZATION_ENTITY_NAME];
    auth.authorizationId = [NSNumber numberWithInt:++index];
    auth.authorizationNumber = [NSNumber numberWithInt:index];
    auth.syncDate = [NSDate date];
    auth.expiryDate = [NSDate date];
    auth.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:index]];
    auth.transaction = [[DataEngine sharedInstance] transactionForId:@"B945D138-BE52-4914-95DC-8317173146A6"];
    auth.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index]];
    
    auth = (Authorization *) [[DataEngine sharedInstance] newEntity:AUTHORIZATION_ENTITY_NAME];
    auth.authorizationId = [NSNumber numberWithInt:++index];
    auth.authorizationNumber = [NSNumber numberWithInt:index];
    auth.syncDate = [NSDate date];
    auth.expiryDate = [NSDate date];
    auth.user = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:index]];
    auth.transaction = [[DataEngine sharedInstance] transactionForId:@"372EF412-7F3E-4736-910F-64B96F99A0D7"];
    auth.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:index]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:AUTHORIZATION_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}



+(void)populateScaleTicketType{
    
    [Utils log:@"populateScaleTicketType"];
    
    //[[DataEngine sharedInstance] deleteAllEntities:SCALE_TICKET_TYPE_ENTITY_NAME];
   
    int index=1;
    
    ScaleTicketType *scaleTicketType = (ScaleTicketType *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_TYPE_ENTITY_NAME];
    
    scaleTicketType.scaleTicketTypeId = [NSNumber numberWithInt:index++];
    scaleTicketType.nameKey = @"Inbound";
    scaleTicketType.syncDate = [NSDate date];
    scaleTicketType.sortIndex = [NSNumber numberWithInt:0];
    
    scaleTicketType = (ScaleTicketType *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_TYPE_ENTITY_NAME];
    scaleTicketType.scaleTicketTypeId = [NSNumber numberWithInt:index++];
    scaleTicketType.nameKey = @"Outbound";
    scaleTicketType.syncDate = [NSDate date];
    scaleTicketType.sortIndex = [NSNumber numberWithInt:10];
    
    scaleTicketType = (ScaleTicketType *) [[DataEngine sharedInstance] newEntity:SCALE_TICKET_TYPE_ENTITY_NAME];
    scaleTicketType.scaleTicketTypeId = [NSNumber numberWithInt:index++];
    scaleTicketType.nameKey = @"Both";
    scaleTicketType.syncDate = [NSDate date];
    scaleTicketType.sortIndex = [NSNumber numberWithInt:20];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:SCALE_TICKET_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}


+(void)populateMessage{
    
    [Utils log:@"populateMessage"];
    
    //delete the Message entity from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:MESSAGE_ENTITY_NAME];
    
    int index=1;
    
    Message *message = (Message *) [[DataEngine sharedInstance] newEntity:MESSAGE_ENTITY_NAME];
    message.messageId = [NSNumber numberWithInt:index++];
    message.nameKey = @"abc123";
    message.syncDate = [NSDate date];
    
    message = (Message *) [[DataEngine sharedInstance] newEntity:MESSAGE_ENTITY_NAME];
    message.messageId = [NSNumber numberWithInt:index++];
    message.nameKey = @"bcd234";
    message.syncDate = [NSDate date];
    
    message = (Message *) [[DataEngine sharedInstance] newEntity:MESSAGE_ENTITY_NAME];
    message.messageId = [NSNumber numberWithInt:index++];
    message.nameKey = @"cde345";
    message.syncDate = [NSDate date];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:MESSAGE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}

+(void)populateUnitType{
    
    [Utils log:@"populateUnitType"];
    
    //delete the UnitType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:UNIT_TYPE_ENTITY_NAME];
    
    int index=1;
    
    UnitType *unitType = (UnitType *) [[DataEngine sharedInstance] newEntity:UNIT_TYPE_ENTITY_NAME];
    unitType.unitTypeId = [NSNumber numberWithInt:index];
    unitType.nameKey = @"kg";
    unitType.syncDate = [NSDate date];
    unitType.kgMultiplier = [NSNumber numberWithInt:index];
    index++;
    
    unitType = (UnitType *) [[DataEngine sharedInstance] newEntity:UNIT_TYPE_ENTITY_NAME];
    unitType.unitTypeId = [NSNumber numberWithInt:index];
    unitType.nameKey = @"lbs";
    unitType.syncDate = [NSDate date];
    unitType.kgMultiplier = [NSNumber numberWithInt:index];
    index++;

    unitType = (UnitType *) [[DataEngine sharedInstance] newEntity:UNIT_TYPE_ENTITY_NAME];
    unitType.unitTypeId = [NSNumber numberWithInt:index];
    unitType.nameKey = @"tonne";
    unitType.syncDate = [NSDate date];
    unitType.kgMultiplier = [NSNumber numberWithInt:index];
    index++;
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:UNIT_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
    
}



+(void)populateTransactionTypeTireType{
    
    [Utils log:@"populateTransactionTypeTireType"];
    
    //delete the TransactionTypeTireType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    
    int index=1;
    
    TransactionType_TireType *transTypeTireType = (TransactionType_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    transTypeTireType.transactionTypeTireTypeId = [NSNumber numberWithInt:index];
    transTypeTireType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:index++]];
    
    transTypeTireType = (TransactionType_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    transTypeTireType.transactionTypeTireTypeId = [NSNumber numberWithInt:index];
    transTypeTireType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:index++]];
    
    transTypeTireType = (TransactionType_TireType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    transTypeTireType.transactionTypeTireTypeId = [NSNumber numberWithInt:index];
    transTypeTireType.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    transTypeTireType.tireType = [[DataEngine sharedInstance] tireTypeForId:[NSNumber numberWithInt:index++]];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_TYPE_TIRE_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateTransaction {
    
    [Utils log:@"populateTransaction"];
    
    //delete the Transaction from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_ENTITY_NAME];
    
    //int index=1;
    
    Transaction *trans = (Transaction *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
    User *createdUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    User *modifiedUser = NULL;
    User *incomingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    User *outgoingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    Photo *incomingSignaturePhoto = NULL;
    Photo *outgoingSignaturePhoto = NULL;
    TransactionStatusType     *transactionStatusType = [[DataEngine sharedInstance] transactionStatusTypeForId:[NSNumber numberWithInt:3]];
    TransactionSyncStatusType *transactionSyncStatusType  = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:[NSNumber numberWithInt:2]];
    
    trans.transactionId = @"39B06EBB-01F6-401E-9B7B-69D10EC19DDF";
    trans.friendlyId = [NSNumber numberWithInt:10001238];
    trans.createdDate = [NSDate date];
    trans.modifiedDate = [NSDate date];
    trans.syncDate = [NSDate date];
    trans.transactionDate = [NSDate date];
    trans.trailerNumber = NULL;
    trans.outgoingSignatureName = NULL;
    trans.incomingSignatureName = NULL;
    trans.trailerLocation = NULL;
    trans.incomingSignaturePhoto = incomingSignaturePhoto;
    trans.outgoingSignaturePhoto = outgoingSignaturePhoto;
    trans.incomingRegistrant = NULL;
    trans.transactionStatusType     = transactionStatusType;
    trans.transactionSyncStatusType = transactionSyncStatusType;
    trans.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    trans.createdUser = createdUser;
    trans.modifiedUser = modifiedUser;
    trans.incomingUser = incomingUser;
    trans.outgoingUser = outgoingUser;
    

    trans = (Transaction *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
    createdUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    modifiedUser = NULL;
    incomingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    outgoingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    outgoingSignaturePhoto = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:8]];
    incomingSignaturePhoto = NULL;
    transactionStatusType = [[DataEngine sharedInstance] transactionStatusTypeForId:[NSNumber numberWithInt:2]];
    transactionSyncStatusType  = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:[NSNumber numberWithInt:2]];
    
    trans.transactionId = @"B945D138-BE52-4914-95DC-8317173146A6";
    trans.friendlyId = [NSNumber numberWithInt:10001246];
    trans.createdDate = [NSDate date];
    trans.modifiedDate = [NSDate date];
    trans.syncDate = [NSDate date];
    trans.transactionDate = [NSDate date];
    trans.trailerNumber = NULL;
    trans.outgoingSignatureName = NULL;
    trans.incomingSignatureName = NULL;
    
    trans.trailerLocation = NULL;
    trans.incomingSignaturePhoto = incomingSignaturePhoto;
    trans.outgoingSignaturePhoto = outgoingSignaturePhoto;
    trans.incomingRegistrant = NULL;
    trans.transactionStatusType     = transactionStatusType;
    trans.transactionSyncStatusType = transactionSyncStatusType;
    trans.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    trans.createdUser = createdUser;
    trans.modifiedUser = modifiedUser;
    trans.incomingUser = incomingUser;
    trans.outgoingUser = outgoingUser;
    
    trans = (Transaction *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
    createdUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    modifiedUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    incomingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    outgoingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    outgoingSignaturePhoto = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:9]];
    incomingSignaturePhoto = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:12]];
    transactionStatusType = [[DataEngine sharedInstance] transactionStatusTypeForId:[NSNumber numberWithInt:2]];
    transactionSyncStatusType  = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:[NSNumber numberWithInt:1]];
    
    trans.transactionId = @"372EF412-7F3E-4736-910F-64B96F99A0D7";
    trans.friendlyId = [NSNumber numberWithInt:10001253];
    trans.createdDate = [NSDate date];
    trans.modifiedDate = [NSDate date];
    trans.syncDate = [NSDate date];
    trans.transactionDate = [NSDate date];
    trans.trailerNumber = NULL;
    trans.outgoingSignatureName = NULL;
    trans.incomingSignatureName = NULL;
    
    trans.trailerLocation = NULL;
    trans.incomingSignaturePhoto = incomingSignaturePhoto;
    trans.outgoingSignaturePhoto = outgoingSignaturePhoto;
    trans.incomingRegistrant = NULL;
    trans.transactionStatusType     = transactionStatusType;
    trans.transactionSyncStatusType = transactionSyncStatusType;
    trans.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    trans.createdUser = createdUser;
    trans.modifiedUser = modifiedUser;
    trans.incomingUser = incomingUser;
    trans.outgoingUser = outgoingUser;
    
    //new transaction without outgoing user
    trans = (Transaction *) [[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
    createdUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    modifiedUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    incomingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:1]];
    //outgoingUser = [[DataEngine sharedInstance] userForId:[NSNumber numberWithInt:2]];
    //outgoingSignaturePhoto = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:9]];
    //incomingSignaturePhoto = [[DataEngine sharedInstance] photoForId:[NSNumber numberWithInt:12]];
    transactionStatusType = [[DataEngine sharedInstance] transactionStatusTypeForId:[NSNumber numberWithInt:2]];
    transactionSyncStatusType  = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:[NSNumber numberWithInt:1]];
    
    trans.transactionId = @"1234567898";
    trans.friendlyId = [NSNumber numberWithInt:33333333];
    trans.createdDate = [NSDate date];
    trans.modifiedDate = [NSDate date];
    trans.syncDate = [NSDate date];
    trans.transactionDate = [NSDate date];
    trans.trailerNumber = NULL;
    trans.outgoingSignatureName = NULL;
    trans.incomingSignatureName = NULL;
    
    trans.trailerLocation = NULL;
    //trans.incomingSignaturePhoto = incomingSignaturePhoto;
    //trans.outgoingSignaturePhoto = outgoingSignaturePhoto;
    trans.incomingRegistrant = NULL;
    trans.transactionStatusType     = transactionStatusType;
    trans.transactionSyncStatusType = transactionSyncStatusType;
    trans.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:1]];
    trans.createdUser = createdUser;
    trans.modifiedUser = modifiedUser;
    trans.incomingUser = incomingUser;
    //trans.outgoingUser = outgoingUser;
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}



+(void)populateEligibility{
    
    [Utils log:@"populateElibility"];
   
    //delete the Eligibility entities from database
    
    [[DataEngine sharedInstance] deleteAllEntities:ELIGIBILITY_ENTITY_NAME];
    
    
    Eligibility *elig = (Eligibility *) [[DataEngine sharedInstance] newEntity:ELIGIBILITY_ENTITY_NAME];
    elig.eligibilityId = ELIGIBILITY_TYPE_ID_TIRES_ELIGIBLE;
    elig.nameKey = @"These_tires_are_not_eligible_for_payment_of_the_Collector_Allowance_by_OTS";
    elig.sortIndex = [NSNumber numberWithInt:0];
    elig.syncDate = [NSDate date];
    elig.transactionType = [[DataEngine sharedInstance] transactionTypeForId:TRANSACTION_TYPE_ID_TCR];
    
    elig = (Eligibility *) [[DataEngine sharedInstance] newEntity:ELIGIBILITY_ENTITY_NAME];
    elig.eligibilityId = ELIGIBILITY_TYPE_ID_TIRES_GENERATED;
    elig.nameKey = @"These_tires_are_Generated_and_therefore_not_eligible_for_payment_of_the_Collection_Allowance_by_OTS";
    elig.sortIndex = [NSNumber numberWithInt:0];
    elig.syncDate = [NSDate date];
    elig.transactionType = [[DataEngine sharedInstance] transactionTypeForId:TRANSACTION_TYPE_ID_TCR];
    
    elig = (Eligibility *) [[DataEngine sharedInstance] newEntity:ELIGIBILITY_ENTITY_NAME];
    elig.eligibilityId = ELIGIBILITY_TYPE_ID_TIRES_CHARGED;
    elig.nameKey = @"Charged_a_disposal_fee";
    elig.sortIndex = [NSNumber numberWithInt:0];
    elig.syncDate = [NSDate date];
    elig.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:6]];

    elig = (Eligibility *) [[DataEngine sharedInstance] newEntity:ELIGIBILITY_ENTITY_NAME];
    elig.eligibilityId = ELIGIBILITY_TYPE_ID_TIRES_GENERATED_PRIOR;
    elig.nameKey = @"Generated prior to Sept. 1st, 2009";
    elig.sortIndex = [NSNumber numberWithInt:0];
    elig.syncDate = [NSDate date];
    elig.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:6]];

    elig = (Eligibility *) [[DataEngine sharedInstance] newEntity:ELIGIBILITY_ENTITY_NAME];
    elig.eligibilityId = ELIGIBILITY_TYPE_ID_TIRES_NON_REGISTERED;
    elig.nameKey = @"Picked up from a non-registered collector";
    elig.sortIndex = [NSNumber numberWithInt:0];
    elig.syncDate = [NSDate date];
    elig.transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInt:6]];

    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:ELIGIBILITY_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        
        NSLog(@"%@", [entity description]);
        
    }
    
}


+(void)populateTransactionStatusType{
    
    [Utils log:@"populateTransactionStatusType"];
    
    //delete the TransactionStatusType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    
    int index=1;
    
    TransactionStatusType *transStatusType = (TransactionStatusType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    transStatusType.transactionStatusTypeId = [NSNumber numberWithInt:index++];
    transStatusType.nameKey = @"Issues_in_transaction";
    transStatusType.fileName = @"list-flag-red.png";
    transStatusType.syncDate = [NSDate date];
    
    transStatusType = (TransactionStatusType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    transStatusType.transactionStatusTypeId = [NSNumber numberWithInt:index++];
    transStatusType.nameKey = @"Incomplete_transaction";
    transStatusType.fileName = @"list-flag-yellow.png";
    transStatusType.syncDate = [NSDate date];
    
    transStatusType = (TransactionStatusType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    transStatusType.transactionStatusTypeId = [NSNumber numberWithInt:index++];
    transStatusType.nameKey = @"Complete_Transaction";
    transStatusType.fileName = @"list-flag-green.png";
    transStatusType.syncDate = [NSDate date];
    
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_STATUS_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}

+(void)populateTransactionSyncStatusType{
    
    [Utils log:@"populateTransactionSyncStatusType"];
    
    //delete the TransactionSyncStatusType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    
    int index=1;
    
    TransactionSyncStatusType *transSyncStatusType = (TransactionSyncStatusType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    transSyncStatusType.transactionSyncStatusTypeId = [NSNumber numberWithInt:index++];
    transSyncStatusType.nameKey = @"Not_Synchronized_Transaction";
    transSyncStatusType.fileName = @"list-dot-red.png";
    transSyncStatusType.syncDate = [NSDate date];
    
    transSyncStatusType = (TransactionSyncStatusType *) [[DataEngine sharedInstance] newEntity:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    transSyncStatusType.transactionSyncStatusTypeId = [NSNumber numberWithInt:index++];
    transSyncStatusType.nameKey = @"Synchronized_Transaction";
    transSyncStatusType.fileName = @"list-dot-green.png";
    transSyncStatusType.syncDate = [NSDate date];
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TRANSACTION_SYNC_STATUS_TYPE_ENTITY_NAME];
    
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
    
}

+(void)populateTireType {
    
    [Utils log:@"populateTireType"];
    
    //delete the TireType from database
    
    //[[DataEngine sharedInstance] deleteAllEntities:TIRE_TYPE_ENTITY_NAME];
    
    int index=1;
    
    TireType *tireType = (TireType *) [[DataEngine sharedInstance] newEntity:TIRE_TYPE_ENTITY_NAME];
    tireType.tireTypeId = [NSNumber numberWithInt:index++];
    tireType.nameKey = @"Passenger_and_Light_Truck_Tires";
    tireType.shortNameKey = @"PLT";
    tireType.sortIndex = [NSNumber numberWithInt:0];
    tireType.estimatedWeight = [NSNumber numberWithInt:10];
    tireType.effectiveStartDate = [NSDate date];
    tireType.effectiveEndDate = [NSDate date];
    tireType.syncDate = [NSDate date];
    
    tireType = (TireType *) [[DataEngine sharedInstance] newEntity:TIRE_TYPE_ENTITY_NAME];
    tireType.tireTypeId = [NSNumber numberWithInt:index++];
    tireType.nameKey = @"Medium_Truck_Tires";
    tireType.shortNameKey = @"MT";
    tireType.sortIndex = [NSNumber numberWithInt:10];
    tireType.estimatedWeight = [NSNumber numberWithInt:50];
    tireType.effectiveStartDate = [NSDate date];
    tireType.effectiveEndDate = [NSDate date];
    tireType.syncDate = [NSDate date];
    
    tireType = (TireType *) [[DataEngine sharedInstance] newEntity:TIRE_TYPE_ENTITY_NAME];
    tireType.tireTypeId = [NSNumber numberWithInt:index++];
    tireType.nameKey = @"Agricultural_Drive_and_Logger_Skidder_Tires";
    tireType.shortNameKey = @"AGLS";
    tireType.sortIndex = [NSNumber numberWithInt:20];
    tireType.estimatedWeight = [NSNumber numberWithInt:60];
    tireType.effectiveStartDate = [NSDate date];
    tireType.effectiveEndDate = [NSDate date];
    tireType.syncDate = [NSDate date];
    
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:TIRE_TYPE_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}

+(void)populateUser {
    [Utils log:@"populateUser"];
    
    //delete the registrant types from database
    //[[DataEngine sharedInstance] deleteAllEntities:USER_ENTITY_NAME];

    User *entity = (User *) [[DataEngine sharedInstance] newEntity:USER_ENTITY_NAME];
    entity.userId       = [NSNumber numberWithInteger:1];
    entity.name         = @"Bobby Yasumura";
    entity.email        = @"bobby@capris.com";
    entity.accessTypeId = [NSNumber numberWithInteger:1];
    entity.registrant   = [[DataEngine sharedInstance] registrantForId:[NSNumber numberWithInt:3000180]];
    entity.metaData     = @"{\"company\":\"Capris\"}";

    
    entity = (User *) [[DataEngine sharedInstance] newEntity:USER_ENTITY_NAME];
    entity.userId       = [NSNumber numberWithInteger:2];
    entity.name         = @"Jill Watts";
    entity.email        = @"jillw@capris.com";
    entity.accessTypeId = [NSNumber numberWithInteger:2];
    entity.registrant   = [[DataEngine sharedInstance] registrantForId:[NSNumber numberWithInt:2001044]];
    entity.metaData     = NULL;
    
    entity = (User *) [[DataEngine sharedInstance] newEntity:USER_ENTITY_NAME];
    entity.userId       = [NSNumber numberWithInteger:3];
    entity.name         = @"Shivangi Mankad";
    entity.email        = @"shivangim@capris.com";
    entity.accessTypeId = [NSNumber numberWithInteger:3];
    entity.registrant   = [[DataEngine sharedInstance] registrantForId:[NSNumber numberWithInt:4000027]];
    entity.metaData     = NULL;
    
    /*
     1	Bobby Yasumura	bobby@capris.com	1	3000180	2013-10-01 13:50:33	NULL	NULL	{"company":"Capris"}
     2	Jill Watts	jillw@capris.com	2	2001044	2013-10-02 04:33:10	2013-10-03 02:33:00	NULL	NULL
     3	Shivangi Mankad	shivangim@capris.com	3	4000027	2013-09-05 08:10:00	NULL	2013-10-04 10:55:00	NULL
     */
    
    [[DataEngine sharedInstance]saveContext];
    
    [Utils log:@"here is what we added"];
    
    NSArray * array = [[DataEngine sharedInstance] entities:USER_ENTITY_NAME];
    for (NSManagedObject * entity in array) {
        NSLog(@"%@", [entity description]);
    }
}

@end
