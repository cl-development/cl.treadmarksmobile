//
//  TOCParticipantPopoverViewController.swift
//  TreadMarks
//
//  Created by Dennis Christopher on 2015-05-20.
//  Copyright (c) 2015 Capris. All rights reserved.
//
// TBD: check input values from user are complete
// NOTE: at dialog close time, the argument to proceed is a dummy user object
// set up with Registrant pointing to the Registrant created here and saved.
// or better: set up a base class protocol popoverDidComplete, popoverDidCancel
// and call these on the delegate at dialog close time

import Foundation

class TOCParticipantPopoverViewController : TOCPopoverViewController {
     @IBOutlet weak var registrantIDTextField:UITextField!
     @IBOutlet weak var registrantBusinessNameTextField:UITextField!
     @IBOutlet weak var registrantPhoneTextField:UITextField!
     @IBOutlet weak var registrantAddressTextField:UITextField!
     @IBOutlet weak var registrantCityTextField:UITextField!
     @IBOutlet weak var registrantPostalTextField:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registrantIDTextField.becomeFirstResponder()
    }
    
    func addManagedObject(dict : NSMutableDictionary, typeName:String) ->NSManagedObject? {
        var ok : Bool = false
        let engineInstance : DataEngine = DataEngine.sharedInstance()
        let newEntity : NSManagedObject? = engineInstance.newEntity(typeName)
        if let entity : NSManagedObject = newEntity {
            ok = entity.setValuesWithDictionary(dict as [NSObject : AnyObject])
            let context : NSManagedObjectContext = engineInstance.managedObjectContext
            if (ok==false) {
                context.deleteObject(entity)
                NSLog("Insert new object of type \(typeName) failed.")
            }
            else {
                /*var error : NSError?
                do {
                    try context.save()
                } catch let error1 as NSError {
                    error = error1
                }
                if (error != nil) {
                    NSException(name: "Core Data save failed", reason: error?.localizedDescription, userInfo: error?.userInfo)
                }
                else {
                    NSLog("\(typeName) object added")
                }*/
                return entity
            }
        }
        return nil
    }
    
    @IBAction override func doneButtonPressed(sender: AnyObject?) {
        super.doneButtonPressed(sender)
        let locDict : NSMutableDictionary = self.getLocationAsDictionary()
        let uuid : String = NSUUID().UUIDString
        locDict.setValue(uuid, forKey: "locationId")
        if self.addManagedObject(locDict, typeName:LOCATION_ENTITY_NAME) != nil {
            let regDict : NSMutableDictionary = self.getRegistrantAsDictionary()
            regDict.setValue(uuid, forKey: "locationId")
            if self.addManagedObject(regDict, typeName:REGISTRANT_ENTITY_NAME) != nil {
                let userDict : NSMutableDictionary = self.getUserAsDictionary()
                if let added : User = (self.addManagedObject(userDict, typeName:USER_ENTITY_NAME) as? User) {
                    if (self.delegate != nil)   {
                        if let view : TOCParticipantComponentView = self.delegate as? TOCParticipantComponentView{
                            let aSelector : Selector = "proceed:"
                            if view.respondsToSelector(aSelector) {
                                view.proceed(added)
                            }
                        }
                    }
                }
            }
        }
      }
 
    // MARK: - Helpers
    func getRegistrantAsDictionary ()->NSMutableDictionary {
        // why needed? constants.h in bridging header
        let REGISTRANT_TYPE_ID_COLLECTOR : NSNumber =  NSNumber(integer:1)
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(self.registrantBusinessNameTextField?.text, forKey: "businessName")
        if let fldString : NSString = self.registrantIDTextField.text {
            let regNumber:NSNumber = fldString.integerValue
            dict.setValue(regNumber, forKey:"registrationNumber")
        }
        dict.setValue(REGISTRANT_TYPE_ID_COLLECTOR, forKey:"registrantTypeId")
        
        return dict
    }
    
    func getUserAsDictionary ()->NSMutableDictionary {
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(1, forKey: "userId")
        if let fldString : NSString = self.registrantIDTextField.text {
            let regNumber:NSNumber = fldString.integerValue
            dict.setValue(regNumber, forKey:"registrationNumber")
        }
        
        return dict
    }
    
    func getLocationAsDictionary ()->NSMutableDictionary {
        let dict : NSMutableDictionary = NSMutableDictionary()
        if let fldString : NSString = self.registrantAddressTextField.text {
            dict.setValue(fldString, forKey:"address1")
        }
        if let fldString : NSString = self.registrantPhoneTextField.text {
            dict.setValue(fldString, forKey:"phone")
        }
        if let fldString : NSString = self.registrantPostalTextField.text {
            dict.setValue(fldString, forKey:"postalCode")
        }
        if let fldString : NSString = self.registrantCityTextField.text {
            dict.setValue(fldString, forKey:"city")
        }
        
        return dict
    }

}
