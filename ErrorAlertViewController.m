//
//  ErrorAlertViewController.m
//  TreadMarks
//
//  Created by Aditya Tandon on 2015-01-12.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import "ErrorAlertViewController.h"

@interface ErrorAlertViewController ()
@property (nonatomic, retain)IBOutlet UIView *viewBackground;



@end
@implementation ErrorAlertViewController
- (IBAction)okButton:(id)sender {
   [self.delegate okButton:sender];
    
    [self dismissViewControllerAnimated:TRUE completion:NULL];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.viewBackground createCornerRadius:self.viewBackground];

    // Do any additional setup after loading the view from its nib.
    self.errorMessageLabel.text=self.errorMessageText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
