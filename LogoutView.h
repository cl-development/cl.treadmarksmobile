//
//  LogoutView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"

@protocol LogoutViewControllerDelegate <NSObject>

@required


- (void)logoutcancelAction:(UIButton *)sender;
-(void)okAction:(UIButton *)sender;
@end
@interface LogoutView : UIView
@property id<LogoutViewControllerDelegate> delegate;
@property (nonatomic, weak)IBOutlet UIView *logoutView;

//LOGOUT VIEW'S BUTTON
- (IBAction)logoutOkButton:(id)sender;
- (IBAction)logoutCancelButton:(id)sender;

@end
