//
//  HelpView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HelpViewControllerDelegate <NSObject>

@required


- (void)helpCancelAction:(UIButton *)sender;
@end
@interface HelpView : UIView
@property (strong) NSString     *helpPdfFileName;
@property id<HelpViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *helpViewTopicLabel;
- (IBAction)backButton:(id)sender;

-(void)loadPdfFile;

@end
