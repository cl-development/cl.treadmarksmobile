//
//  TOCParticipantComponentView
//
//  Created by Dragos Ionel on 2013-10-12.
//  Copyright (c) 2013 Adelante. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCComponentView.h"
#import "RegistrantType.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import "TOCTransactionInformationComponentView.h"
#import "TOCTireCountComponentView.h"
#import "TOCPhotoComponentView.h"
#import "TOCNotesComponentView.h"
#import "TOCEligibilityComponentView.h"
#import "TOCOutgoingComponentView.h"
@interface TOCOutgoingComponentView : TOCComponentView <CLLocationManagerDelegate>

@property (nonatomic, assign) BOOL isIncomingUser;
@property (nonatomic, strong) User *user;
@property (strong, nonatomic) RegistrantType *expectedRegistrantType;

-(void)setViewUI;

- (id)initWithUser:(User *)user expectedRegistrantType:(RegistrantType *)expectedRegType;

@end
