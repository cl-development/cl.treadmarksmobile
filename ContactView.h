//
//  ContactView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"

@protocol ContactViewControllerDelegate <NSObject>

@required


- (void)contactCancelAction:(UIButton *)sender;
@end



@interface ContactView : UIView
@property id<ContactViewControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIView *headerView;
@property (nonatomic, weak) IBOutlet UIView *contactView;
- (IBAction)BackButton:(id)sender;


@end
