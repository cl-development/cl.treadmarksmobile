//
//  PopUpView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "PopUpView.h"
#import "Utils.h"
#import "Transaction+Transaction.h"
#import "DataEngine.h"
#import "TOCPageViewController.h"
#import "ModalViewController.h"
//#import "ListViewController.h"
//#import "NewAlertView.h"

//@interface PopUpView () <NewAlertViewDelegate>
//@property (nonatomic,strong) NSString *alertmessage;
//
//@end

@implementation PopUpView 

- (id)init {
    
    
//    ListViewController *listview = [[ListViewController alloc]init];
//    NSInteger *UnsyncLastMonth = listview.countUnsyncedTransactions;
//    NSString *LastMonth = [listview getClaimMonth:2];
//    NSString *Month = [listview getClaimMonth:1];
//    NSNumber *Year = [listview getClaimCurrentYear];
//    NSNumber *LastYear;
//    NSString *alertmessage;
    
    self = [super init];

    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        
//        if ([LastMonth isEqualToString:@"Dec"]){
//            LastYear = [NSNumber numberWithInt:([Year intValue]-1)];
//        } else {
//            LastYear = Year;
//        }
//        
////        UnsyncLastMonth = 2;
//        
//        if (UnsyncLastMonth > 0) {
//
//            NewAlertView *newAlertView = [[NewAlertView alloc] init];
//            
//            self.alertmessage = [NSString stringWithFormat:@"You must complete, void, or sync any incomplete or unsynced transactions in %@. %@ before you can create new transactions in %@. %@", LastMonth, LastYear, Month, Year];
//           NSLog(@"show alert!!!!!!!!!!!!!");
//            [self presentAlert:nil info:NULL];
//        } else {
        
            [self showBtSimplePopUp];
            
 //       }
    }
 
    return self;
}

//- (void)presentAlert:(UIViewController *)viewController info:(NSDictionary *)info {
//    
//    NewAlertView *newAlertView = [[NewAlertView alloc] init];
//    //    newAlertView.frame = CGRectMake(floor((self.view.frame.size.width - newAlertView.frame.size.width) / 2),
//    //                                    floor((self.view.frame.size.height - newAlertView.frame.size.height) / 2),
//    //                                    newAlertView.frame.size.width,
//    //                                    newAlertView.frame.size.height);
///*
//    NSRange boldedRange = NSMakeRange(10, 24);
//    
//    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:self.alertmessage];
//    
//    UIFont *font_regular = [UIFont fontWithName:@"Helvetica" size:20.0f];
//    UIFont *font_bold = [UIFont fontWithName:@"Helvetica-Bold" size:20.0f];
//    
//    [attrString addAttribute:NSFontAttributeName value:font_bold range:boldedRange];
//*/
////    [newAlertView setText: self.alertmessage];
//    newAlertView.title = @"Warning";
//    newAlertView.text = self.alertmessage;
//    newAlertView.primaryButtonText = @"OK";
//    newAlertView.delegate = self;
//    newAlertView.singlelineforOkay.hidden=YES;
//    
//    newAlertView.setColour =YES;
//    [newAlertView setUI];
//    [self addSubview:newAlertView];
////    [[ModalViewController sharedModalViewController] showView:newAlertView];
//    
////    NSLog(@"alert!!!!!!!!!!");
//}
//
//- (void)primaryAction:(UIButton *)sender {
//    
//    [[ModalViewController sharedModalViewController] hideView];
//}
// 
-(void)showBtSimplePopUp
{
    
    Registrant *currentRegistrant = [Utils getIncomingRegistrant];
    BOOL activeRegistrant = [currentRegistrant.isActive integerValue]==1 ? YES:NO;
    if ([Utils getProcessorDidLogin] == NO)
    {
        
        if([Utils getFlipServer]==YES)
        {
            self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?
                                                                    @"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//TCR
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//STC
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//DOT
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//HIT
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//UCR
                                                                   
                                                                   ]
                                                       andTitles:    @[
                                                                       @"TCR", @"STC",@"DOT",@"HIT",
                                                                       @"UCR"
                                                                       ]
                          
                                                  andActionArray:@[
                                                                   ^{
                
                activeRegistrant?[self tcrAction]:[self showAlert];//TCR
            },
                                                                    ^{
                activeRegistrant?[self stcAction]:[self showAlert];//STC
            },
                                                                    ^{
                activeRegistrant?[self dotAction]:[self showAlert];
                
                
            },
                                                                    
                                                                    ^{
                activeRegistrant?[self hitAction]:[self showAlert];
            },
                                                                    
                                                                    ^{
                activeRegistrant?[self ucrAction]:[self showAlert];
            },
                                                                    
                                                                    ]
                                             addToViewController:nil];
        }
        else
        {
            
            self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?
                                                                    @"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//TCR
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//STC
                                                                   
                                                                   
                                                                   ]
                                                       andTitles:    @[
                                                                       @"TCR", @"STC"                                                                           ]
                          
                                                  andActionArray:@[
                                                                   ^{
                
                activeRegistrant?[self tcrAction]:[self showAlert];//TCR
            },
                                                                    ^{
                activeRegistrant?[self stcAction]:[self showAlert];//STC
            },
                                                                    
                                                                    ]
                                             addToViewController:nil];
            
        }
        
    }
    else
    {
        //Enable Transaction.
        if([Utils getFlipServer]==YES)
        {
            
            self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[
                                                                   
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//PTR
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//PIT
                                                                   ]
                                                       andTitles:    @[
                                                                       @"PTR",@"PIT"                                                                       ]
                          
                                                  andActionArray:@[
                                                                   
                                                                   ^{
                activeRegistrant?[self ptrAction]:[self showAlert];//PTR
                
            },
                                                                    ^{
                activeRegistrant?[self pitAction]:[self showAlert];//PIT
            },                                                                    //
                                                                    ]
                                             addToViewController:nil];
            
        }
        else{
            
            
            self.popUp = [[btSimplePopUP alloc]initWithItemImage:@[
                                                                   
                                                                   
                                                                   [UIImage imageNamed:activeRegistrant?@"dock-icon-new-trans.png":@"dock-icon-new-trans-grey.png"],//PTR
                                                                   
                                                                   
                                                                   ]
                                                       andTitles:    @[
                                                                       @"PTR"                                                                       ]
                          
                                                  andActionArray:@[
                                                                   
                                                                   ^{
                activeRegistrant?[self ptrAction]:[self showAlert];//PTR
                
            },                                                               //
                                                                    ]
                                             addToViewController:nil];
            
            
        }
        
        
        
    }
    
    [self addSubview: self.popUp];
    [self.popUp setPopUpStyle:BTPopUpStyleDefault];
    [self.popUp setPopUpBorderStyle:BTPopUpBorderStyleDefaultNone];
    [self.popUp setPopUpBackgroundColor:[Utils colorWithRed:245 green:245 blue:245]];
    [self.popUp show: BTPopUPAnimateWithFade];
    self.popUp.backgroundColor =[UIColor clearColor];
    [[ModalViewController sharedModalViewController]hideView];
    
    
    
}



-(void)showAlert{
    [self.delegate extraAlertAction];
    
}
-(void)ptrAction
{
    [self.delegate ptrPerformAction];
    
}
-(void)tcrAction
{
    [self.delegate tcrPerformAction];
}
-(void)stcAction
{
    [self.delegate stcPerformAction];
}
-(void)pitAction
{
    [self.delegate PitPerformAction];
}

-(void)hitAction
{
    [self.delegate hitPerformAction];
    
}
-(void)ucrAction
{
    [self.delegate ucrPerformAction];
    
}
-(void)dotAction
{
    [self.delegate dotPerformAction];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ModalViewController sharedModalViewController]hideView];
}




/*
- (void)newTransaction:(NSInteger)transType {
    self.newTransactionCreated=YES;
    //create a new transaction based on transType, which is the tagValue of the button that was selected by the user.  The tag value corresponds to the TransactionTypeId, which allows for the creation of the correct TransactionType dynamically.
    [Utils log:@"New Transaction creation"];
    
    TransactionType *transactionType = [[DataEngine sharedInstance] transactionTypeForId:[NSNumber numberWithInteger:transType]];
    //create a new transaction
    Transaction *transaction     = (Transaction*)[[DataEngine sharedInstance] newEntity:TRANSACTION_ENTITY_NAME];
    transaction.deviceName = [[UIDevice currentDevice] name];
    
    // if a processor logs in, the user values are reversed:
    User *loggedInUser = [Utils getIncomingUser];
    BOOL processorDidLogin = loggedInUser.registrant.registrantType.registrantTypeId==REGISTRANT_TYPE_ID_PROCESSOR;
    
    if (processorDidLogin==YES) {
        transaction.incomingUser     = nil;
        transaction.incomingRegistrant = nil;
        transaction.outgoingUser     = [Utils getIncomingUser];
        // transaction.createdUser      = [Utils  getIncomingRegistrant];
        transaction.outgoingRegistrant = [Utils getIncomingRegistrant];
    }
    else {
        transaction.incomingUser     = [Utils getIncomingUser];
        transaction.incomingRegistrant = [Utils getIncomingRegistrant];
        
        transaction.outgoingUser     = nil;
        transaction.outgoingRegistrant = nil;
    }
    transaction.transactionType  = transactionType;
    transaction.transactionId    = [Utils createTransactionId];
    transaction.friendlyId       = [Utils createFriendlyTransactionId:transactionType];
    transaction.createdDate      = [NSDate date];
    transaction.transactionDate  = [NSDate date];
    transaction.createdUser = [Utils getIncomingUser];
    NSString *appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *versionBuildString = [NSString stringWithFormat:@"Version %@(%@)", appVersionString, appBuildString];
    transaction.versionBuild = versionBuildString;
    transaction.transactionStatusType       = [[DataEngine sharedInstance] transactionStatusTypeForId:TRANSACTION_STATUS_TYPE_ID_INCOMPLETE];
    
    transaction.transactionSyncStatusType   = [[DataEngine sharedInstance] transactionSyncStatusTypeForId:TRANSACTION_SYNC_STATUS_TYPE_ID_NOT_SYNCHRONIZED];
    
    //find the gps location of the login
    NSNumber *gpsLogId = [Utils getGPSLogId];
    GPSLog *incomingGPSLog = [[DataEngine sharedInstance] gpsLogForId:gpsLogId];
    transaction.incomingGpsLog = incomingGPSLog;
    
    //create the transaction_transactiontype objects
    NSArray *tireTypeArray = [[DataEngine sharedInstance] tireTypesForTransactionTypeId:transactionType.transactionTypeId];
    for (TireType *tireType in tireTypeArray) {
        Transaction_TireType *transactionTireType = (Transaction_TireType*)[[DataEngine sharedInstance] newEntity:TRANSACTION_TIRE_TYPE_ENTITY_NAME];
        transactionTireType.transaction = transaction;
        transactionTireType.tireType = tireType;
        transactionTireType.transactionTireTypeId = [[NSUUID UUID] UUIDString];
    }
    
    [Utils setTransactionId:transaction.transactionId];
    
    [[DataEngine sharedInstance] saveContext];
    
    TOCPageViewController *viewController = [[TOCPageViewController alloc] init];
    self.TOCView.newTransaction=OFF_SCREEN;
 
     if(self.delegate == NULL)
     {
     @throw [[NSException alloc] initWithName:@"Exception"
     reason:@"delegate must be specified."
     userInfo:NULL];
     }
     */
    
    // viewController.rootViewController = self.delegate;
    //self.TOCView.newTransaction=OFF_SCREEN;
    // [self.delegate.navigationController pushViewController:viewController animated:YES];
    // viewController.rootViewController = self.delegate;
/*
    [self.navigationController pushViewController:viewController animated:YES];
}
*/


@end
