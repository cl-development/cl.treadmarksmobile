//
//  InfoView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 30/09/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "InfoView.h"
#import "Utils.h"
#import "Location.h"
#import "User.h"
#import "Registrant.h"
#import "DataEngine.h"
#import "ModalViewController.h"


@implementation InfoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.infoView createCornerRadius:self.infoView];
        [self populateMyInfo];

    }
    
    return self;
}

#pragma mark - Button Action
- (IBAction)infoCancelButton:(id)sender {
    [self.delegate cancelAction:sender];
    
}
#pragma mark - Private Methods

-(void)populateMyInfo
{
    User        *userInfo=[[DataEngine sharedInstance] userForId:[Utils getUserId] andRegistrantId:[Utils getRegistrantId]];
    
    
    Registrant  *registrantInfo=[[DataEngine sharedInstance]  registrantForId:[Utils getRegistrantId]];
    Location    *locationInfo=registrantInfo.location;
    
    self.nameLabel.text=    userInfo.name;
    //NSLog(@"%@",self.nameLabel.text);
    self.emailLabel.text = userInfo.email==NULL?@"":userInfo.email;
    self.businessLabel.text=registrantInfo.businessName;
    self.registrantIdLabel.text= [NSString stringWithFormat:@"%@ - %@",[registrantInfo.registrationNumber stringValue],[userInfo.userId stringValue]];
    self.bizPhone.text=locationInfo.phone==NULL?@"":locationInfo.phone;
    
    self.addressLabel.text=locationInfo.address1==NULL?@"":locationInfo.address1 ;
    self.bizAddress2Label.text=[[NSString stringWithFormat:@ "%@ %@ %@",locationInfo.address2==NULL?@"":locationInfo.address2,
                                 locationInfo.address3==NULL?@"":locationInfo.address3,
                                 locationInfo.city==NULL?@"":locationInfo.city ] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    self.bizAddress3Label.text=[[NSString stringWithFormat:@ "%@ %@",locationInfo.province==NULL?@"":locationInfo.province,
                                 locationInfo.postalCode==NULL?@"":locationInfo.postalCode
                                 ]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.infoView || touch.view == self.headerView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.infoView || touch.view == self.headerView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.infoView || touch.view == self.headerView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }}
@end
