### Fixing the iPad mini 'crash' on initial sync bug
##### Why does it happen? 
Parsing and saving the 12mb of updates is done on the main thread and takes so long that the iOS watchdog will kill the app
##### How to fix it? 
1) Launch the app in the iOS simulator and sync it

2) After that is done go to *`/Users/<username>/Library/Developer/CoreSimulator/Devices/<device uuid>/data/Containers/Data/Application/<app uuid>/Documents`* and rename the follwing files:
    >TreadMarks -> TreadMarks-*\<current date>*.sqlite  
    >TreadMarks-shm -> TreadMarks-*\<current date\>*.sqlite-shm  
    >TreadMarks-wal -> TreadMarks-*\<current date\>*.sqlite-wal  
    
    *Note: format for *\<current date\>* is currently `dd-mm-yyyy`*

3) Then copy these files into *`<proj root>/TreadMarks`* and remove the old sqlite files

4) Go into xCode and add these new files into the project under *`treadmarks/TreadMarks/Supporting Files`* and make sure they are added to target Treadmarks.  

5) Now go to Constants.h and change :
    >**FIXED_STORE_PATH** to @"TreadMarks-*\<current date\>*"  
    >**APP_STATIC_DATA_EXTRACT_DATE** to @"*\<current date\>*"
