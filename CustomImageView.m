//
//  CustomImageView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/09/15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import "CustomImageView.h"

@implementation CustomImageView


- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self connectNib];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if ((self = [super initWithCoder:aDecoder])){
    }
    return self;
}

-(void)connectNib{
    [self addSubview:
     [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:nil] objectAtIndex:0]
     ];
    self.scrollView.maximumZoomScale=6.0;
    self.scrollView.contentSize=CGSizeMake(600, 600);
    self.scrollView.delegate=self;
}
-(void)setImage:(UIImage*)image
{
    self.imageView.image=image;
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [touch locationInView:self.view];
    if([touch.view isKindOfClass:[UIImageView class]]){}
    else if([touch.view isKindOfClass:[UIView class]]){
        [[ModalViewController sharedModalViewController] hideView];

    }
    
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [touch locationInView:self.view];
    if([touch.view isKindOfClass:[UIImageView class]]){}
    else if([touch.view isKindOfClass:[UIView class]]){
        [[ModalViewController sharedModalViewController] hideView];
        
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [touch locationInView:self.view];
    if([touch.view isKindOfClass:[UIImageView class]]){}
    else if([touch.view isKindOfClass:[UIView class]]){
        [[ModalViewController sharedModalViewController] hideView];
        
    }
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [touch locationInView:self.view];
    if([touch.view isKindOfClass:[UIImageView class]]){}
    else if([touch.view isKindOfClass:[UIView class]]){
        [[ModalViewController sharedModalViewController] hideView];
        
    }
}
-(IBAction)buttonCancelAction:(id)sender;
{
     [[ModalViewController sharedModalViewController] hideView];
}
- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    zoomRect.size.height = scrollView.frame.size.height / scale;
    zoomRect.size.width  = scrollView.frame.size.width  / scale;
    zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}
@end
