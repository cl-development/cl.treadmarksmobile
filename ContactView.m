//
//  ContactView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "ContactView.h"
#import "ModalViewController.h"
@implementation ContactView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self.contactView createCornerRadius:self.contactView];
        
    }
    
    return self;
}


- (IBAction)BackButton:(id)sender
{
    [self.delegate contactCancelAction:sender];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.contactView || touch.view == self.headerView) {
        
    }
    else
    {[[ModalViewController sharedModalViewController]hideView];}
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    if (touch.view == self.contactView || touch.view == self.headerView) {
        
    }
    else
    {[[ModalViewController sharedModalViewController]hideView];}
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[[event allTouches]anyObject];
    
    if (touch.view == self.contactView || touch.view == self.headerView) {
        
    }
    else
    {
        [[ModalViewController sharedModalViewController]hideView];
        
    }
    
}


@end
