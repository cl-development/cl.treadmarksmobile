//
//  ErrorAlertViewController.h
//  TreadMarks
//
//  Created by Aditya Tandon on 2015-01-12.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+CustomView.h"

@protocol ErrorAlertViewDelegate <NSObject>

@required

- (void)okButton:(UIButton *)sender;


@end

@interface ErrorAlertViewController : UIViewController
@property (strong, nonatomic) NSString *errorMessageText;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageHeaderLabel;
@property (strong, nonatomic) IBOutlet UILabel *errorMessageLabel;

@property id<ErrorAlertViewDelegate> delegate;
@end
