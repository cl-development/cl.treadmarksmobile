//
//  CustomImageView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/09/15.
//  Copyright (c) 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
#import "Utils.h"
#import "ModalViewController.h"
@interface CustomImageView : UIView<UIScrollViewDelegate>
@property (nonatomic, weak) IBOutlet UIView *view;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

-(void)setImage:(UIImage*)image;

@end
