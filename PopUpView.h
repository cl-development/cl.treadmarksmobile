//
//  PopUpView.h
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "btSimplePopUP.h"
@protocol PopUpViewControllerDelegate <NSObject>
-(void)ptrPerformAction;
-(void)stcPerformAction;
-(void)tcrPerformAction;
-(void)extraAlertAction;
-(void)PitPerformAction;
-(void)hitPerformAction;
-(void)ucrPerformAction;
-(void)dotPerformAction;
@required

@end


@interface PopUpView : UIView
@property (weak, nonatomic) IBOutlet UIView *Alertview;

@property id<PopUpViewControllerDelegate> delegate;
@property (nonatomic,retain)IBOutlet UILabel *alertlabel;
@property (weak, nonatomic) IBOutlet UIImageView *alertImage;
@property(nonatomic, strong) btSimplePopUP *popUp;


@end

