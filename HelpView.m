//
//  HelpView.m
//  TreadMarks
//
//  Created by Dinesh Garg on 01/10/15.
//  Copyright © 2015 Capris. All rights reserved.
//

#import "HelpView.h"

@implementation HelpView

- (id)init {
    
    self = [super init];
    
    if (self)
    {
        UIView* view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class)
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
        self.frame = view.frame;
        [self addSubview:view];
        [self loadPdfFile];
    }
    
    return self;
}
    
-(void)loadPdfFile
{
    //TBD  when we get the PDF file replace Path for source with self.helpPdfFileName and
    //we have make sure the PDF file name is exactly what is in the Table cell .
    self.helpViewTopicLabel.text=self.helpPdfFileName;
    NSLog(@"%@",self.helpPdfFileName);
    
    NSString *path = [[NSBundle mainBundle] pathForResource:self.helpPdfFileName ofType:@"pdf"];
    
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    UIWebView *pdfWebView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 90, 768, 934)];
    [[pdfWebView scrollView] setContentOffset:CGPointMake(0,500) animated:YES];
    [pdfWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0, 50.0)"]];
    [pdfWebView loadRequest:request];
    [pdfWebView setBackgroundColor:[UIColor lightGrayColor]];
    pdfWebView.scalesPageToFit = YES;
    [self addSubview:pdfWebView];
    
}
- (IBAction)backButton:(id)sender {
   [self.delegate helpCancelAction:sender];
//[self dismissViewControllerAnimated:YES completion:NULL];
  //   [self.navigationController popViewControllerAnimated:YES];
}


@end
